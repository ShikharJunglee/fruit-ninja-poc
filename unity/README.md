## Cloning the repository
* This repository is only for the core game play, Framework will be added as a sub-module. While cloning, enable Recursive Clone
* development is the default branch to start with.

## Setup
* Switch to either iOS or Android platform
* Build asset bundles by going to XcelerateGames/AssetBundles/Build

## Run the game
* Open "startup" scene & run or press "ctrl+shft+L"

## Naming convention
* All prefabs to start with "Pf". Ex **PfUiHUD**, **PfBall**
* All Ui classes to start with "Ui". Ex **UiHUD**, **UiSettings**, **UiDialogBox**
* No file to be kept **Laawaris**, All files must be under respective folders.

## Coding convention
* Public variable must start with "_" & follow camel casing. Ex **_Health**, **_DestroyTime**
* Private variables must start with "m" & follow camelcasing. Ex **mIdReady**, **mScore**
* Variables that are exposed in unity can be (preferably) set to Serializable & private.

## NO CHNAGES TO BE MADE IN FRAMEWORK PROJECT WITHOUT CONSULTING.
## ZERO WARNINGS
## No extra lines in code, code must be beautiful to look at :)