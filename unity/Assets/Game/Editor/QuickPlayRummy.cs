﻿using UnityEditor;
using XcelerateGames;
using XcelerateGames.Editor;

namespace JungleeGames.FruitNinja.Editor
{
    public class QuickPlayRummy : QuickPlay
    {
        private const string Startup = "Assets/Game/Scenes/startup.unity";
        private const string Game = "Assets/Game/Scenes/game.unity";
        private const string UiTest = "Assets/Game/Scenes/uitest.unity";

        [MenuItem(Utilities.MenuName + "Scene/Run From Startup %#l")]
        static void QuickRun()
        {
            if (QuickOpenStartupScene())
                EditorApplication.ExecuteMenuItem("Edit/Play");
        }

        static bool QuickOpenStartupScene()
        {
            return OpenScene(Startup);
        }

        [MenuItem(Utilities.MenuName + "Scene/Ui Test %#t")]
        static void QuickOpenUiTest()
        {
            OpenScene(UiTest);
        }

        [MenuItem(Utilities.MenuName + "Scene/Game %#m")]
        static void QuickOpenGame()
        {
            OpenScene(Game);
        }
    }
}
