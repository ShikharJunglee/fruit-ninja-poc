using System;
using System.Collections.Generic;
using XcelerateGames;
using XcelerateGames.IOC;

namespace JungleeGames.FruitNinja
{
    #region Fruits Related
    //Load cards by asset name
    public class SigLoadFruits : Signal<string> { };
    public class SigOnFruitCut : Signal { };
    public class SigOnFruitMiss : Signal { };
    public class SigFruitsLoaded : Signal { };
    #endregion Fruits Related


    //player data signals
    public class SigPlayerDataLoaded : Signal { };
    public class SigPlayerDataUpdated : Signal { };
    public class SigSavePlayerData : Signal { };

    #region Game Related
    public class SigOnStartGame : Signal { };
    public class SigOnGameQuit : Signal { };
    public class SigOnStartTimer : Signal<int> { };
    public class SigOnTimerFinished : Signal { };
    public class SigOnGameFinished : Signal<int> { };
    public class SigOnShowFinishScreen : Signal<GameFinishData> { };
    public class SigOnRestartGame : Signal { };
    #endregion Game Related
}
