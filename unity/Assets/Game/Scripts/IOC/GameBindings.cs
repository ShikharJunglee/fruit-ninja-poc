using XcelerateGames;
using XcelerateGames.IOC;
using JungleeGames.Keyboard;
using System.Diagnostics;
using JungleeGames.GoldRush;

namespace JungleeGames.FruitNinja
{
    public class GameBindings : BindingManager
    {
        protected override void SetBindings()
        {
            base.SetBindings();

            #region Framework
            BindSignal<SigLoadAssetFromBundle>();
            BindSignal<SigEngineReady>();
            BindSignal<SigFruitsLoaded>();
            BindSignal<SigLoadFruits>();
            // BindSignal<SigShowContactUs>();
            #endregion

            #region Fruits Related
            BindSignal<SigOnFruitCut>();
            BindSignal<SigOnFruitMiss>();

            //BindModel<CardModel>();
            #endregion Fruits Related


            #region Player Related
            BindSignal<SigPlayerDataLoaded>();
            BindSignal<SigPlayerDataUpdated>();
            BindSignal<SigSavePlayerData>();

            BindModel<PlayerModel>();
            #endregion Player Related

            #region Game Related
            //BindSignal<SigShowSettings>();
            BindSignal<SigOnStartGame>();
            BindSignal<SigOnStartTimer>();
            BindSignal<SigOnTimerFinished>();

            BindSignal<SigOnGameQuit>();
            BindSignal<SigOnGameFinished>();
            BindSignal<SigOnShowFinishScreen>();
            BindSignal<SigOnRestartGame>();
            #endregion
        }

        protected override void SetFlow()
        {
            base.SetFlow();

            #region Table Related
            On<SigOnStartGame>().Do<CmdCreateGameplayArea>();
            #endregion Table Related

            On<SigLoadAssetFromBundle>().Do<CmdLoadAssetFromBundle>();

            /*
            #region Player Related
            On<SigEngineReady>().Do<CmdLoadPlayerData>();
            On<SigSavePlayerData>().Do<CmdSavePlayerData>();
            //On<SigLoadThemes>().Do<CmdLoadThemes>();
            On<SigPlayerDataLoaded>().Do<CmdPreloadAssets>();
            #endregion Player Related
            */
        }
    }
}
