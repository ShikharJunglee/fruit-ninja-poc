﻿using UnityEngine;

public class AnimEvents : MonoBehaviour
{
    #region Public Methods
    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
    #endregion
}