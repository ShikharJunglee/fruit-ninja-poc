using JungleeGames.GoldRush;
using System.Collections.Generic;
using UnityEngine;
using XcelerateGames;
using XcelerateGames.Analytics;
using XcelerateGames.AssetLoading;
using XcelerateGames.Audio;
using XcelerateGames.IOC;

namespace JungleeGames.Blitz21
{
    public class GameManager : BaseBehaviour
    {
        #region Properties
        //This is temp, will get this from some config
        [SerializeField] private AudioVars _BGMusic = null;
        #endregion //Properties

        #region Signals
        #endregion //Signals

        #region UI Callbacks
        #endregion //UI Callbacks

        #region Private Methods
        protected override void Awake()
        {
            base.Awake();
#if !LIVE_BUILD && !BETA_BUILD
            XDebug.AddMask(XDebug.Mask.WebService);
            XDebug.AddMask(XDebug.Mask.Game);
#endif
            if (AudioController.pIsReady)
                PlayBgMusic(true);
            else
                AudioController.OnReady += PlayBgMusic;

            /*
            mWSDataModel.initData = new InitData()
            {
                userId = 432137,
                avatarUrl = "https://d2cbroser6kssl.cloudfront.net/images/solitaire_gold/avatars/avatar_",
                context = "02984A26BE6DF68C582AC5F04E97B99890349DC209ECC0DB8FC5501E83B851CC",
                sessionId = "s%3AuEb_o1ou2-.aaE7lpcapvYr6Zxu%2BPkRVzi4QXkL8bt0VfbkEo3bjb0",
                cookie = "pids=s%3AuVIJVjD_2QaPtms83HY8MrlVKxC4hLAl.%2BxZgJWXQjA1MnrB9aYshXDWimAcUlIo1yx3qHQKQA88; Path=/; Expires=Fri, 04 Dec 2020 11:28:07 GMT",
            };

            GRWebRequest.cookie = mWSDataModel.initData.cookie;
            */
            OnTableLoaded(true);

            //mSigOnStartGame.AddListener(OnStartGame);
        }

        private void PlayBgMusic(bool ready)
        {
            _BGMusic.Play();
        }


        void OnStartGame()
        {
            //  mSigLoadAssetFromBundle.Dispatch("table/PfUiTable", null, false, null);
            //  mSigLoadAssetFromBundle.Dispatch("table/PfUiTable", null, false, null);
        }

        private void OnTableLoaded(bool obj)
        {
            ResourceManager.DestroyLoadingScreen();
        }
        #endregion //Private Methods

        #region Public Methods
        #endregion //Public Methods
    }
}
