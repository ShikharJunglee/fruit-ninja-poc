﻿using UnityEngine;
using XcelerateGames.AssetLoading;
using XcelerateGames.Locale;

namespace XcelerateGames.UI
{
    public enum HeaderType
    {
        None,
        Disconnected,
    }

    public class UiGRDialogBox : UiDialogBox
    {
        [SerializeField] protected UiItem _Header = null;

        public static UiGRDialogBox ShowKey(string messageKey, HeaderType headerType, DialogBoxType inType)
        {
            UiGRDialogBox dialogBox = Show(Localization.Get(messageKey), string.Empty, inType);
            dialogBox._Header.SetTexture(dialogBox.GetHeaderPath(headerType), null);
            return dialogBox;
        }

        public new static UiGRDialogBox Show(string inMessage, string inHeader, DialogBoxType inType)
        {
            UiGRDialogBox dlgBox = Create();
            dlgBox.ShowDB(inMessage, inHeader, inType);
            return dlgBox;
        }

        private new static UiGRDialogBox Create()
        {
            string prefabName = "PfUiGRDialogBox";
            GameObject go = ResourceManager.LoadFromResources(prefabName) as GameObject;
            if (go != null)
                return GameObject.Instantiate(go).GetComponent<UiGRDialogBox>();
            else
                Debug.LogError("Could not find : " + prefabName + " under resources.");
            return null;
        }

        protected string GetHeaderPath(HeaderType headerType)
        {
            string headerName = null;
            switch(headerType)
            {
                case HeaderType.Disconnected:
                    headerName = "Disconnected";
                    break;
            }

            return $"titles/{headerName}";
        }
    }
}
