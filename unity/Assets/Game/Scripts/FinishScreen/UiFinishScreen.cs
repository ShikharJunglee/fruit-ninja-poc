﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using XcelerateGames;
using XcelerateGames.IOC;
using XcelerateGames.Timer;
using XcelerateGames.UI;

namespace JungleeGames.FruitNinja
{
    public class UiFinishScreen : UiBase
    {
        #region Properties
        [SerializeField] private UiItem mScore = null;
        [SerializeField] private UiItem mTimeBonus = null;
        [SerializeField] private UiItem mTotalScore = null;

        [SerializeField] private Transform mPanel = null;
        #endregion //Properties

        #region Signals
        [InjectSignal] private SigOnShowFinishScreen mSigOnShowFinishScreen = null;
        [InjectSignal] private SigOnRestartGame mSigOnRestartGame = null;
        #endregion //Signals

        #region Private Methods
        protected override void Start()
        {
            base.Start();
            mSigOnShowFinishScreen.AddListener(ShowFinishWindow);
        }

        private void ShowFinishWindow(GameFinishData GameFinishData)
        {
            mPanel.SetActive(true);
            mScore.SetText(GameFinishData.score, true);
            mTimeBonus.SetText(GameFinishData.time_bonus, true);
            mTotalScore.SetText(GameFinishData.total_score, true);
        }
        #endregion //Private Methods

        #region Public Methods
        public void OnRestartButtonClick()
        {
            mPanel.SetActive(false);
            mSigOnRestartGame.Dispatch();
        }

        public void OnQuitButtonClick()
        {
            Application.Quit();
        }
        #endregion
    }
}
