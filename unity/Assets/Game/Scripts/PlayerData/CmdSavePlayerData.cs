﻿using UnityEngine;
using XcelerateGames.IOC;
using XcelerateGames;

namespace JungleeGames.FruitNinja
{
    public class CmdSavePlayerData : Command
    {
        [InjectModel] private PlayerModel mPlayerModel = null;

        public override void Execute()
        {
            PlayerPrefs.SetString("PlayerData", mPlayerModel.playerData.ToJson());
            base.Execute();
        }
    }
}
