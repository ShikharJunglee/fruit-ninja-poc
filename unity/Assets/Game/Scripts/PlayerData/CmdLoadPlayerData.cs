using UnityEngine;
using XcelerateGames.IOC;
using XcelerateGames;
using System.Collections;

namespace JungleeGames.FruitNinja
{
    public class CmdLoadPlayerData : Command
    {
        [InjectModel] private PlayerModel mPlayerModel = null;

        [InjectSignal] private SigPlayerDataLoaded mSigPlayerDataLoaded = null;
        [InjectSignal] private SigSavePlayerData mSigSavePlayerData = null;

        public override void Execute()
        {
            if (PlayerPrefs.HasKey("PlayerData"))
            {
                //Debug.Log("fetching cached player data");
                mPlayerModel.playerData = PlayerPrefs.GetString("PlayerData").FromJson<PlayerData>();
            }
            else
            {
                //Debug.Log("caching player data");
                mPlayerModel.playerData = new PlayerData(id: "mo95895b", balance: 50, userName: "John Doe", mobileNumber: "9876543210");
                mSigSavePlayerData.Dispatch();
            }
            mSigPlayerDataLoaded.Dispatch();
            base.Execute();
        }
    }
}
