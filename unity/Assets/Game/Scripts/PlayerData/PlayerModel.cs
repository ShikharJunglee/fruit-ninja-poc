using XcelerateGames.IOC;

namespace JungleeGames.FruitNinja
{
    public class PlayerModel : XGModel
    {
        public PlayerData playerData = null;
        public bool IsReady => playerData != null;
    }
}
