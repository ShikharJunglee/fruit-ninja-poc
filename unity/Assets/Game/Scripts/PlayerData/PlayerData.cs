using Newtonsoft.Json;

namespace JungleeGames.FruitNinja
{
    public class PlayerData
    {
        [JsonProperty] public string id { get; private set; }
        [JsonProperty] public string mobileNumber { get; private set; }
        [JsonProperty] public int balance { get; set; }
        [JsonProperty] public string userName { get; set; }
        [JsonProperty] public int themeId { get; set; } = 1;

        public PlayerData(string id, int balance, string userName, string mobileNumber)
        {
            this.id = id;
            this.mobileNumber = mobileNumber;
            this.userName = userName;
            this.balance = balance;
        }

        public void UpdateID(int inId)
        {
            this.themeId = inId;
        }
    }
}
