using JungleeGames.GoldRush;
using UnityEngine;
using XcelerateGames;
using XcelerateGames.AssetLoading;
using XcelerateGames.Audio;
using XcelerateGames.IOC;
using XcelerateGames.Locale;
using XcelerateGames.Timer;

namespace JungleeGames.FruitNinja
{
    public class Startup : BaseBehaviour
    {
        #region Properties
        [SerializeField] private string _SceneName = "game";
        #endregion //Properties

        #region Signals
        [InjectSignal] private SigEngineReady mSigEngineReady = null;
        #endregion //Signals

        #region UI Callbacks
        #endregion //UI Callbacks

        #region Private Methods
        protected override void Awake()
        {
            base.Awake();
            ServerTime.Init(0);
            ResourceManager.Init(OnResourceManagerReady);
        }

        private void OnDestroy()
        {
        }

        private void OnResourceManagerReady()
        {
            ResourceManager.UnregisterOnReadyCallback(OnResourceManagerReady);
            Localization.Init(OnLocalizationLoaded);
        }

        private void OnLocalizationLoaded(bool loaded)
        {
            AudioController.Init();
            mSigEngineReady.Dispatch();
            ResourceManager.LoadScene(_SceneName);
        }
        #endregion //Private Methods

        #region Public Methods
        #endregion //Public Methods
    }
}
