﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using XcelerateGames.IOC;
using XcelerateGames.UI;

namespace JungleeGames.Keyboard
{
    public class KeyBase : UiItem
    {
        [HideInInspector] public bool capitalized;

        #region Signals
        [InjectSignal] private SigOnShiftKeyPress mSigOnShiftKeyPress = null;
        [InjectSignal] private SigOnShowKeyboard mSigOnShowKeyboard = null;
        [InjectSignal] private SigOnKeyPress mSigOnKeyPress = null;
        #endregion //Signals

        protected override void Start()
        {
            base.Start();
            Reset();
            mSigOnShowKeyboard.AddListener(Reset);
            mSigOnShiftKeyPress.AddListener(ToggleShiftEvent);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            mSigOnShowKeyboard.RemoveListener(Reset);
            mSigOnShiftKeyPress.RemoveListener(ToggleShiftEvent);
        }

        public override void OnClicked()
        {
            base.OnClicked();
            ReactivateInputField(JGKeyboard.mlastSelected);
            Invoke("AfterKeyPress", .01f);
        }

        public virtual void ToggleShiftEvent(bool shiftOn)
        {
            capitalized = shiftOn;
        }

        public virtual void Reset()
        {
            capitalized = true;
        }

        void ReactivateInputField(JGInputField inputField)
        {
            if (inputField != null)
            {
                StartCoroutine(ActivateInputFieldWithoutSelection(inputField));
            }
        }

        void AfterKeyPress()
        {
            mSigOnKeyPress.Dispatch();
        }

        IEnumerator ActivateInputFieldWithoutSelection(JGInputField inputField)
        {
            inputField.ActivateInputField();
            // wait for the activation to occur in a lateupdate
            yield return new WaitForEndOfFrame();
            // make sure we're still the active ui
            if (EventSystem.current.currentSelectedGameObject == inputField.gameObject)
            {
                // To remove hilight we'll just show the caret at the end of the line
                inputField.MoveTextEnd(false);

            }
        }
    }
}
