﻿using UnityEngine;
using UnityEngine.UI;
using XcelerateGames.IOC;
using XcelerateGames.UI;

namespace JungleeGames.Keyboard
{
    public enum KeyboardType
    {
        Standard,
        Num,
        Alphabet
    }

    public enum KeyboarPos
    {
        Left,
        Right,
        Center
    }

    public class JGKeyboard : UiMenu
    {
        #region Signals
        [InjectSignal] private SigShowKeyboard mSigShowKeyboard = null;
        [InjectSignal] private SigHideKeyboard mSigHideKeyboard = null;
        [InjectSignal] private SigOnShowKeyboard mSigOnShowKeyboard = null;
        #endregion //Signals

        #region Properties
        [SerializeField] private GameObject[] mKeyboardPanel = null;
        #endregion //Properties

        #region Static Variables
        public static JGInputField mlastSelected;
        public static bool _IsKeyboardOpen;
       #endregion // Static Variables

        private GameObject mActiveKeyboard;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            HideAllKeyboard();
            mSigShowKeyboard.AddListener(ShowKeyboard);
            mSigHideKeyboard.AddListener(Hidekeyboard);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            mSigShowKeyboard.RemoveListener(ShowKeyboard);
            mSigHideKeyboard.RemoveListener(Hidekeyboard);
        }

        private void ShowKeyboard(KeyboardType type, TextAnchor Pos)
        {
            HideAllKeyboard();
            int ktype = (int)type;
            mKeyboardPanel[ktype].SetActive(true);
            mKeyboardPanel[ktype].GetComponent<GridLayoutGroup>().childAlignment = Pos;
            mSigOnShowKeyboard.Dispatch();
            _IsKeyboardOpen = true;
            mActiveKeyboard = mKeyboardPanel[ktype];
        }

        void Hidekeyboard()
        {
            mlastSelected = null;
            mActiveKeyboard.SetActive(false);
            _IsKeyboardOpen = false;
            mActiveKeyboard = null;
        }
        void HideAllKeyboard()
        {
            for (int i = 0; i < mKeyboardPanel.Length; i++)
            {
                mKeyboardPanel[i].SetActive(false);
            }
            _IsKeyboardOpen = false;
            mActiveKeyboard = null;
        }
    }
}
