﻿using UnityEngine;

namespace JungleeGames.Keyboard
{
    public class SpecialKey : KeyBase
    {

        public override void OnClicked()
        {
            base.OnClicked();
            JGInputField input = JGKeyboard.mlastSelected;

            Event keyPress = Event.KeyboardEvent(transform.name);

            //if (capitalized) keyPress.character = char.ToUpper(keyPress.character);
            Debug.Log(keyPress.ToString());
            input.ProcessEvent(keyPress);

            input.ForceLabelUpdate();
        }
    }
}
