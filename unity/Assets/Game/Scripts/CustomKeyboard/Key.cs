﻿using UnityEngine;
using TMPro;

namespace JungleeGames.Keyboard
{
    public class Key : KeyBase
    {
        private string mChildText = null;

        protected override void Start()
        {
            base.Start();
            TMP_Text mText = transform.GetComponentInChildren<TMP_Text>();
            mChildText = mText.text.ToString();
        }

        public override void OnClicked()
        {
            base.OnClicked();
            JGInputField input = JGKeyboard.mlastSelected;

            Event keyPress = Event.KeyboardEvent(mChildText);

            if (capitalized) keyPress.character = char.ToUpper(keyPress.character);

            input.ProcessEvent(keyPress);

            input.ForceLabelUpdate();
        }

        public override void ToggleShiftEvent(bool shiftOn)
        {
            base.ToggleShiftEvent(shiftOn);
            SetTextValue();
        }

        public override void Reset()
        {
            base.Reset();
            SetTextValue();
        }

        void SetTextValue()
        {
            TMP_Text mText = transform.GetComponentInChildren<TMP_Text>();
            mChildText = mText.text.ToString();
            if (capitalized)
            {
                mText.text = mChildText.ToUpper();
            }
            else
            {
                mText.text = mChildText.ToLower();
            }
        }

    }
}
