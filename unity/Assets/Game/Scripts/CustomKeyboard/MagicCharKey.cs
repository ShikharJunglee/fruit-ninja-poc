﻿using TMPro;
using UnityEngine;

namespace JungleeGames.Keyboard
{
    public class MagicCharKey : KeyBase
    {
        private string mChildText = null;

        protected override void Start()
        {
            base.Start();
            TMP_Text mText = transform.GetComponentInChildren<TMP_Text>();
            mChildText = mText.text.ToString();
        }

        public override void OnClicked()
        {
            base.OnClicked();
            JGInputField input = JGKeyboard.mlastSelected;
            Event keyPress = null;
            switch (mChildText)
            {
                case "&":
                    keyPress = Event.KeyboardEvent("a");
                    keyPress.keyCode = KeyCode.Ampersand;
                    keyPress.character = mChildText[0];
                    break;
                case "^":
                    keyPress = Event.KeyboardEvent("a");
                    keyPress.keyCode = KeyCode.Caret;
                    keyPress.character = mChildText[0];
                    break;
                case "%":
                    keyPress = Event.KeyboardEvent("a");
                    keyPress.keyCode = KeyCode.Percent;
                    keyPress.character = mChildText[0];
                    break;
                case "#":
                    keyPress = Event.KeyboardEvent("a");
                    keyPress.keyCode = KeyCode.Hash;
                    keyPress.character = mChildText[0];
                    break;
                default:
                    if (mChildText.Length != 1)
                    {
                        Debug.LogError("Ignoring spurious multi-character key value: " + mChildText);
                        return;
                    }
                    keyPress = Event.KeyboardEvent(mChildText);
                    if (capitalized) keyPress.character = char.ToUpper(keyPress.character);
                    break;
            }

            input.ProcessEvent(keyPress);
            input.ForceLabelUpdate();
        }

    }
}

