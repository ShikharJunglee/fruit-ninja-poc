﻿using XcelerateGames.UI;
using UnityEngine;
using XcelerateGames.IOC;

namespace JungleeGames.Keyboard
{
    public class UiJGInputFieldItem : UiItem
    {
        [Header("Custom JG Field")]
        public KeyboardType _keyboardType;
        public TextAnchor _keyboardpos;
       [InjectSignal] private SigShowKeyboard mSigShowKeyboard = null;

        protected override void Start()
        {
            base.Start();
            transform.GetComponent<JGInputField>()._SigShowKeyboard = mSigShowKeyboard;
        }

    }
}
