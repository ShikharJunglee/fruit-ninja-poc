﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JungleeGames.Keyboard
{
    [RequireComponent(typeof(UiJGInputFieldItem))]
    public class JGInputField : TMP_InputField
    {
        #region Signals
        public SigShowKeyboard _SigShowKeyboard = null;
        #endregion //Signals

        #region private variables
        private KeyboardType mkeyboardType = KeyboardType.Standard;
        private TextAnchor mkeyboardpos = TextAnchor.LowerCenter;
        #endregion /private variables

        protected override void Start()
        {
            base.Start();
            mkeyboardType = transform.GetComponent<UiJGInputFieldItem>()._keyboardType;
            mkeyboardpos = transform.GetComponent<UiJGInputFieldItem>()._keyboardpos;
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);
            if (!JGKeyboard._IsKeyboardOpen || JGKeyboard.mlastSelected != this)
            {
                _SigShowKeyboard.Dispatch(mkeyboardType, mkeyboardpos);
            }
            JGKeyboard.mlastSelected = this;
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);
        }
    }
}
