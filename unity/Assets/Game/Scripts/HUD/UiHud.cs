﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using XcelerateGames;
using XcelerateGames.IOC;
using XcelerateGames.Timer;
using XcelerateGames.UI;

namespace JungleeGames.FruitNinja
{
    public class UiHud : UiBase
    {
        #region Properties
        [SerializeField] private UiItem _UserName = null;
        [SerializeField] private UiItem _Balance = null;
        [SerializeField] private Transform _StartGamePanel = null;
        [SerializeField] private UiItem mTimerText = null;
        #endregion //Properties

        #region Signals
        [InjectModel] private PlayerModel mPlayerModel = null;
        [InjectSignal] private SigPlayerDataUpdated mSigPlayerDataUpdated = null;
        [InjectSignal] private SigOnStartGame mSigOnStartGame = null;
        [InjectSignal] private SigOnGameQuit mSigOnGameQuit = null;
        [InjectSignal] private SigOnStartTimer mSigOnStartTimer = null;

        [InjectSignal] private SigOnTimerFinished mSigOnTimerFinished = null;
        // [InjectSignal] private SigShowNoNetwork mSigShowNoNetwork = null;

        //[InjectSignal] private SigLoadAssetFromBundle mSigLoadAssetFromBundle = null;
        #endregion //Signals

        #region Private Methods
        protected override void Start()
        {
            base.Start();
            if (mPlayerModel.IsReady)
                OnPlayerDataUpdated();

            mSigPlayerDataUpdated.AddListener(OnPlayerDataUpdated);
            mSigOnGameQuit.AddListener(OnGameQuitClicked);
            mSigOnStartTimer.AddListener(OnStartTimer);
        }



        protected override void OnDestroy()
        {
            base.OnDestroy();
            mSigPlayerDataUpdated.RemoveListener(OnPlayerDataUpdated);
            mSigOnGameQuit.RemoveListener(OnGameQuitClicked);
        }

        private void OnPlayerDataUpdated()
        {
            _UserName.text = mPlayerModel.playerData.userName;
            _Balance.text = "Balance: " + mPlayerModel.playerData.balance.ToString();
            //Debug.Log($"user name is{mPlayerModel.playerData.userName}");
        }

        private void OnGameQuitClicked()
        {
            _StartGamePanel.SetActive(true);
        }
        #endregion //Private Methods

        #region Public Methods
        public void OnHamburgerIconClick()
        {
            //mSigShowSettings.Dispatch();
        }

        public void OnStartBtnClicked()
        {
            //if (ConnectivityMonitor.pIsInternetAvailable)
            //{
            _StartGamePanel.SetActive(false);
            mSigOnStartGame.Dispatch();
            //}
            // else
            //{
            // mSigShowNoNetwork.Dispatch(true);
            //}
        }

        private void OnStartTimer(int timer)
        {
            ServerTime.Init(0);
            mTimerText.GetComponent<UiTimer>().ShowExpiryTime(timer, OnTimerFinished);
        }

        private void OnTimerFinished(UiTimer UiTimer)
        {
            mSigOnTimerFinished.Dispatch();
        }
        #endregion
    }
}
