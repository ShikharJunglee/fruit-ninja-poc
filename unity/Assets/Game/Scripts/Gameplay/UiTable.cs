using DG.Tweening;
using JungleeGames.FruitNinja;
using NoSuchStudio.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using XcelerateGames.IOC;
using XcelerateGames.Timer;
using XcelerateGames.UI;

namespace XcelerateGames
{
    public class UiTable : UiBase
    {
        #region Properties        
        public int mTotalScore { get; private set; } = 0;
        [SerializeField] private UiItem mTotalScoreText = null;
        //[SerializeField] private Canvas mTableCanvas = null;
        //[SerializeField] private GameObject mFruitGo = null;

        #endregion //Properties

        #region Signals
        //Fruits related
        //[InjectSignal] private SigOnStartGame mSigOnStartGame = null;
        //Group related
        [InjectSignal] private SigOnFruitMiss mSigOnFruitMiss = null;
        [InjectSignal] private SigOnFruitCut mSigOnFruitCut = null;

        //FinishScreen related
        [InjectSignal] private SigOnGameFinished mSigOnGameFinished = null;
        [InjectSignal] private SigOnRestartGame mSigOnRestartGame = null;
        // [InjectSignal] private SigOnRegenerateCards mSigOnRegenerateCards = null;


        //Timer related
        [InjectSignal] private SigOnTimerFinished mSigOnTimerFinished = null;

        //Models
        //[InjectModel] private CardModel mCardModel = null;
        //[InjectModel] private GroupModel mGroupModel = null;

        //GamePlay Signals
        // [InjectSignal] private SigOnStartTimer mSigOnStartTimer = null;

        //[InjectSignal] private SigAddCard mSigAddCard = null;
        #endregion //Signals

        #region UI Callbacks
        #endregion //UI Callbacks

        #region Private Methods

        protected override void Awake()
        {
            base.Awake();
            //signals
            mSigOnTimerFinished.AddListener(OnTimerFinished);
            //mSigOnStartGame.AddListener(OnRestartGame);
            mSigOnFruitMiss.AddListener(OnFruitMiss);
            mSigOnFruitCut.AddListener(OnFruitCut);
        }


        protected override void Start()
        {
            base.Start();
            //mSigOnStartGame.Dispatch();
        }

        protected override void OnDestroy()
        {
            mSigOnTimerFinished.RemoveListener(OnTimerFinished);
            mSigOnRestartGame.RemoveListener(OnRestartGame);
            base.OnDestroy();
        }

        private void OnTimerFinished()
        {
            mSigOnGameFinished.Dispatch(mTotalScore);
        }

        private void OnFruitCut()
        {

        }
        private void OnFruitMiss()
        {

        }

        //update total score of all perfect matches so far
        private void UpdateTotalScoreUI()
        {
            mTotalScoreText.SetText(mTotalScore, true);
        }

        private void OnRestartGame()
        {
            //destroy all instantiated fruits

            //remove all discarded cards
            //foreach (Transform child in mDiscardedDeck)
            //{
            //    Destroy(child.gameObject);
            //}

            //mSigOnStartGame.Dispatch("cards1/CardAtlas", 1);
            // ResetTable();
        }

        #endregion //Private Methods

        #region Public Methods
        public void InitTable()
        {

        }

        public void ResetTable()
        {
            mTotalScore = 0;

            UpdateTotalScoreUI();
            //mSigOnStartTimer.Dispatch((mCardModel.mTableData.time));
        }

        private void DisableButton(Button input)
        {
            input.interactable = false;
        }

        private void EnableButton(Button input)
        {
            input.interactable = true;
        }
        #endregion
    }
}
