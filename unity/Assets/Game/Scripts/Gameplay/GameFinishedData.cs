using System.Collections.Generic;
using JungleeGames.GoldRush;
using Newtonsoft.Json;

namespace JungleeGames.FruitNinja
{
    public class GameFinishData
    {
        [JsonProperty] public int score { get; private set; } = 0;
        [JsonProperty] public int time_bonus { get; private set; } = 0;
        [JsonProperty] public int total_score { get; private set; } = 0;
    }
}
