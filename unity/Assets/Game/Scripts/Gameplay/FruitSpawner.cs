using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitSpawner : MonoBehaviour
{
    public GameObject mFruitPrefab;
    public float minDelay = .1f;
    public float maxDelay = 1f;

    // Use this for initialization
    private void Start()
    {
        StartCoroutine(SpawnFruits());
    }

    private IEnumerator SpawnFruits()
    {
        while (true)
        {
            float delay = Random.Range(minDelay, maxDelay);
            yield return new WaitForSeconds(delay);

            float xPos = Random.Range(0, Screen.width);
            float yPos = -50;//Random.Range(0, Screen.height / 2);
            var screenBottomCenter = new Vector3(xPos, yPos, 0);
            var inWorld = Camera.main.ScreenToWorldPoint(screenBottomCenter);

            GameObject spawnedFruit = null;
            if (xPos < Screen.width / 2)
            {
                spawnedFruit = Instantiate(mFruitPrefab, new Vector3(inWorld.x, inWorld.y, 0), Quaternion.Euler(0, 0, Random.Range(85, 90)));
            }
            else
            {
                spawnedFruit = Instantiate(mFruitPrefab, new Vector3(inWorld.x, inWorld.y, 0), Quaternion.Euler(0, 0, Random.Range(90, 135)));
            }

            Destroy(spawnedFruit, 5f);
        }
    }

}
