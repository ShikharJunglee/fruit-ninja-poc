Build Number: 3, Version: 1.0, Environment: Dev. Date: Nov 2, 2020
-------------------------------------------------------------------
1. Implemented Card drag and drop on new group
2. Declare Ui implemented as per received packet
3. Results screen implemented with actual data and timers

Version: 1.0 Build Number: 2, Environment: Dev. Date: Oct 27, 2020
------------------------------------------------------------------
1. Updated Unity from 2019.4.11f1 to 2019.4.13f1
2. Gameplay flow 2P and 6P mode
3. Implemented Discard UI functionality
4. Disconnection game table message
5. Network Strength Bar
6. Setting Menu UI Update
7. Report A Problem UI Update
8. Game Table Message on Finish or Open Deck Click
9. Cards group validation logic updated
10. Game information screen(with fake data for now)
11. Results screen opened on press of "O" key(with fake data for now)

Version: 1.0 Build Number: 1, Environment: Dev. Date: ??
------------------------------------------------------------------
1. Integrated Firebase Crashlytics