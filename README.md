# web socket URL
ws://192.168.225.203:12000/path?pid=


####################################################
##################  Native code  ###################
#################################################### 


---->add debugShowCheckedModeBanner: false, in main Material App to remove the ribbon

to format dart code
shift+option+F for MAc
Shift + Alt + F  for windows

# To run app
flutter run
flutter run --flavor indiaprod
flutter run --flavor usaprod
flutter run --flavor usastage
flutter run --flavor indiastage

# To run in VS debug
Add the flavor in configurations in .vscode/launch.json
 Eg:
  "configurations": [
        {
            "name": "Flutter",
            "program": "lib/main.dart",
            "request": "launch",
            "type": "dart",
            "args": [
                "--flavor",
                "indiastage"
            ]
        }
    ]
####################################################
##################  Android      ###################
#################################################### 
# To give release build
1.grs_android_unityexport. Export this repo into Android Folder
2.Copy SolitaireGold.keystore to Android/App folder.Get this file from developer.
Please note the file Android/App/SolitaireGold.keystore is in git ignore.
3.Run this command
flutter build apk --release
flutter build apk --release --flavor usastage
flutter build apk --release --flavor usaprod
flutter build apk --release --flavor indiastage
flutter build apk --release --flavor indiaprod

# local.properties
flutter.channelId=10
flutter.buildType=prod

# To format code on android 

mac=Option +command+l
windows= ctrl +alt +l
ubuntu = ctrl + shift + alt + l




####################################################
##################  iOS          ###################
#################################################### 
#  To run app
flutter run
#  To build
flutter build ios --release
or --profile or --release or --debug)
..................................
to run on particular device 
flutter run -d ZX1PC2JHXH
.................................
#  To add ios platform:swift
flutter create -i swift . 
#  Cocopods
pod init
pod update
pod install

#  Refresh
flutter clean
rm -rf Pods directory
#  To open simulator
open -a Simulator

#  Keyboard Shortcuts
to open ios keyboard in simulator=⇧(Option) + ⌘(Command) +K
                                 =⌘(Command) +K

Format code = Ctrl+I                                 