import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:provider/provider.dart';

import 'event.dart';

class AnalyticsEvents {
  void onAddCashLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "choose_amount_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onDepositTileClicked(
    BuildContext context, {
    @required String source,
    double amount,
    String promoCode,
    double instantCash,
    double bonus,
    double lockedBonus,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "deposit_tile_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "iCash": instantCash,
      "bonus": bonus,
      "lkdBonus": lockedBonus,
    });

    AnalyticsManager().addEvent(event);
  }

  void onHavePromoClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "have_promo_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRepeat(
    BuildContext context, {
    @required String source,
    double amount,
    String promoCode,
    int paymentMode,
    bool checked,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "repeat", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "modeOpId": paymentMode,
      "check": checked.toString(),
    });

    AnalyticsManager().addEvent(event);
  }

  void onRepeatTransaction(
    BuildContext context, {
    @required String source,
    double amount,
    String promoCode,
    int paymentMode,
    bool checked,
    String gateway,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "repeat_transaction", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "modeOpId": paymentMode,
      "check": checked.toString(),
      "gwayId": gateway,
    });

    AnalyticsManager().addEvent(event);
  }

  void onAddCashProceed(
    BuildContext context, {
    @required String source,
    double amount,
    String promoCode,
    bool isCustomAmount,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "proceed_req", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "isCustom": isCustomAmount.toString(),
    });

    AnalyticsManager().addEvent(event);
  }

  void onAddCashProceedError(
    BuildContext context, {
    @required String source,
    double amount,
    String promoCode,
    bool error,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "proceed_validation_error", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "err": error.toString(),
    });

    AnalyticsManager().addEvent(event);
  }

  void onPayNowScreen(
    BuildContext context, {
    @required String source,
    double amount,
    String promoCode,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "proceed_res", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
    });

    AnalyticsManager().addEvent(event);
  }

  // void onLaunchAddCash({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  // }) {
  //   Event event = Event(name: "addcash");
  //   event.setFirstDeposit(firstDeposit);
  //   if (amount != null) {
  //     event.setDepositAmount(amount.round());
  //   }
  //   event.setPromoCode(promoCode);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onDepositTile({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  // }) {
  //   Event event = Event(name: "deposit_tile");
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onEnterCodeButton({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  // }) {
  //   Event event = Event(name: "enter_code_button");
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onRepeat({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  //   Map<String, dynamic> paymentDetails,
  //   bool repeat,
  // }) {
  //   Event event = Event(name: "repeat");
  //   event.setFirstDeposit(false);
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setPaymentRepeatChecked(repeat);
  //   event.setGatewayId(paymentDetails["gatewayId"]);
  //   event.setModeOptionId(paymentDetails["modeOptionId"]);

  //   AnalyticsManager().addEvent(event);
  // }

  // void proceedValidationError({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  //   Map<String, dynamic> validationResponse,
  // }) {
  //   Event event = Event(name: "proceed_validation_error");
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);
  //   event.setErrorMessage(validationResponse["msg"]);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onProceedRequest({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  // }) {
  //   Event event = Event(name: "proceed_req");
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onProceedResponse({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  //   bool success,
  //   Map<String, dynamic> validationResponse,
  // }) {
  //   Event event = Event(name: "proceed_res");
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setPaymentSuccess(success);
  //   event.setFirstDeposit(firstDeposit);
  //   event.setErrorMessage(validationResponse["msg"]);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onApplyPromo({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  // }) {
  //   Event event = Event(name: "have_promo_code");
  //   event.setIsOpening(true);
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onRemovePromo({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  // }) {
  //   Event event = Event(name: "remove_promo_code");
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);

  //   AnalyticsManager().addEvent(event);
  // }

  // void onRepeatTransaction(
  //     {@required int amount,
  //     @required bool firstDeposit,
  //     bool isTile,
  //     String promoCode,
  //     Map<String, dynamic> paymentDetails,
  //     String clientPaymentType}) {
  //   Event event = Event(name: "repeat_transaction");
  //   event.setFirstDeposit(false);
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setModeOptionId(paymentDetails["modeOptionId"]);
  //   event.setGatewayId(int.parse(paymentDetails["gatewayId"].toString()));
  //   event.setClientPaymentType(clientPaymentType);
  //   event.setTile(isTile);
  //   AnalyticsManager().addEvent(event);
  // }

  void paymentFailed({
    @required String source,
    @required int amount,
    @required bool firstDeposit,
    String promoCode,
    Map<String, dynamic> paymentPayload,
    Map<String, dynamic> paymentResponse,
  }) {
    Event event = Event(name: "pay_failed", eventMetadata: {
      "fu": firstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "modeOptId": paymentPayload["modeOptionId"],
      "ordrId": (paymentResponse == null || paymentResponse["orderId"] == null)
          ? paymentPayload["orderId"]
          : paymentResponse["orderId"],
      "gwayId": paymentPayload["gatewayId"] != null
          ? int.parse(paymentPayload["gatewayId"].toString())
          : 0,
      "pmntType": paymentPayload["paymentType"],
    });

    AnalyticsManager().addEvent(event);
  }

  void paymentSuccess({
    @required int amount,
    @required bool firstDeposit,
    String promoCode,
    Map<String, dynamic> paymentPayload,
    Map<String, dynamic> paymentResponse,
  }) {
    Event event = Event(name: "pay_success", eventMetadata: {
      "fu": firstDeposit,
      "amount": amount,
      "promocode": promoCode,
      "updtBal": double.tryParse(paymentResponse["withdrawable"].toString())
              .toDouble() +
          double.tryParse(paymentResponse["depositBucket"].toString())
              .toDouble() +
          double.tryParse(paymentResponse["bonusAmount"].toString()).toDouble(),
      "ordrId": paymentResponse["orderId"],
      "pmntType": paymentPayload["paymentType"],
      "modeOptId": paymentPayload["modeOptionId"],
      if (paymentResponse["gatewayId"] != null)
        "gwayId": int.parse(paymentResponse["gatewayId"].toString()),
    });

    AnalyticsManager().addEvent(event);
  }

  // void applyPromoError({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   String promoCode,
  //   Map<String, dynamic> promoResponse,
  // }) {
  //   Event event = Event(name: "apply_promo_error");
  //   event.setPromoCode(promoCode);
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);
  //   event.setErrorMessage(promoResponse["msg"]);

  //   AnalyticsManager().addEvent(event);
  // }

  // void applyPromoSuccess({
  //   @required int amount,
  //   @required bool firstDeposit,
  //   Map<String, dynamic> promoCode,
  //   Map<String, dynamic> promoResponse,
  // }) {
  //   Event event = Event(name: "apply_promo_sucess");
  //   event.setDepositAmount(amount);
  //   event.setFirstDeposit(firstDeposit);
  //   event.setErrorMessage(promoResponse["msg"]);
  //   event.setPromoCode(promoCode == null ? "" : promoCode["promoCode"]);
  //   event.setInstantCash(
  //     promoCode == null
  //         ? 0
  //         : getPercentageAmount(
  //             amount,
  //             double.tryParse(promoCode["instantCashPercentage"].toString()),
  //             promoCode,
  //           ).toString(),
  //   );
  //   event.setBonusCash(
  //     promoCode == null
  //         ? 0
  //         : getPercentageAmount(
  //             amount,
  //             double.tryParse(promoCode["playablePercentage"].toString()),
  //             promoCode,
  //           ).toString(),
  //   );
  //   event.setLockedBonusCash(
  //     promoCode == null
  //         ? 0
  //         : getPercentageAmount(
  //             amount,
  //             double.tryParse(promoCode["nonPlayablePercentage"].toString()),
  //             promoCode,
  //           ).toString(),
  //   );

  //   AnalyticsManager().addEvent(event);
  // }

  void paymentFailedRetry({
    @required String source,
    @required int amount,
    String promoCode,
    Map<String, dynamic> transactionResult,
  }) {
    Event event = Event(name: "pay_failed_retry", eventMetadata: {
      "fu": transactionResult["firstDepositor"] != "false",
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "modeOptId": transactionResult["modeOptionId"],
      "ordrId": transactionResult["orderId"],
    });

    AnalyticsManager().addEvent(event);
  }

  void paymentFailedCancel({
    @required String source,
    @required int amount,
    String promoCode,
    Map<String, dynamic> transactionResult,
  }) {
    Event event = Event(name: "pay_failed_cancel", eventMetadata: {
      "fu": transactionResult["firstDepositor"] != "false",
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "modeOptId": transactionResult["modeOptionId"],
      "ordrId": transactionResult["orderId"],
    });

    AnalyticsManager().addEvent(event);
  }

  void paymentModeLaunch({
    @required String source,
    @required int amount,
    @required bool firstDeposit,
    String promoCode,
    int flem,
  }) {
    Event event = Event(name: "payment_mode_screen", eventMetadata: {
      "fu": firstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void validatePaySecurely({
    @required String source,
    int flem,
    int gatewayId,
    int modeOptionId,
    @required int amount,
    @required bool firstDeposit,
    String promoCode,
    String message,
  }) {
    Event event = Event(name: "pay_securely_validation", eventMetadata: {
      "src": source,
      "fu": firstDeposit,
      "amount": amount,
      "promocode": promoCode,
      "err": message,
    });

    AnalyticsManager().addEvent(event);
  }

  void paySecurely({
    @required String source,
    int flem,
    int gatewayId,
    int modeOptionId,
    @required int amount,
    @required bool firstDeposit,
    String promoCode,
    String clientPaymentType,
  }) {
    Event event = Event(name: "pay_securely", eventMetadata: {
      "fu": firstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "modeOptId": modeOptionId,
      "gwayId": gatewayId,
      "pmntType": clientPaymentType,
    });

    AnalyticsManager().addEvent(event);
  }

  void payModeSelect({
    @required String source,
    int flem,
    bool expanded,
    String paymentType,
    @required int amount,
    @required bool firstDeposit,
    String promoCode,
  }) {
    Event event = Event(name: "pay_mode_select", eventMetadata: {
      "fu": firstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "pmntType": paymentType,
    });

    AnalyticsManager().addEvent(event);
  }

  void payOptionSelect({
    String source,
    int flem,
    int gatewayId,
    int modeOptionId,
    @required int amount,
    String paymentOptionType,
    @required bool firstDeposit,
    String promoCode,
  }) {
    Event event = Event(name: "pay_option_select", eventMetadata: {
      "fu": firstDeposit,
      "src": source,
      "amount": amount,
      "promocode": promoCode,
      "modeOptId": modeOptionId,
      "optionType": paymentOptionType,
      "gwayId": gatewayId,
    });

    AnalyticsManager().addEvent(event);
  }
}
