import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/event.dart';
import 'package:solitaire_gold/models/profile/user.dart';

class ReferAndEarnEvents {
  ReferAndEarnEvents._internal();
  factory ReferAndEarnEvents() => referAndEarnEvents;
  static final ReferAndEarnEvents referAndEarnEvents =
      ReferAndEarnEvents._internal();

  void onRAFClicked(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_button_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFLoaded(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_page_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFBackClicked(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_back_button_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFHelpClicked(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_help_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFHelpPopupLoaded(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_help_popup_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFHelpPopupClosed(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_help_close_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFCopyCodeClicked(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_copy_code_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFWhatsAppClicked(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_whatsapp_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFTelegramClicked(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_telegram_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFMoreClicked(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_more_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRAFSmsClicked(BuildContext context,
      {@required int amount, @required String referalCode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "raf_freesms_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "rfa": amount,
      "rfc": referalCode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSmsContactLoaded(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "sms_contact_access_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSmsContactDenied(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "sms_contact_access_denied", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSmsContactAllowed(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "sms_contact_access_allowed", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onDoNotAskAgain(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "dont_ask_again_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFreeSmsLoaded(
    BuildContext context, {
    @required int noOfContatcs,
    @required int totalBonus,
    @required bool selectedAll,
    @required int existingHzContacts,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "freesms_page_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "ncl": noOfContatcs,
      "tb": totalBonus,
      "sac": selectedAll,
      "ehcc": existingHzContacts,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFreeInviteClicked(
    BuildContext context, {
    @required int noOfContatcs,
    @required int totalBonus,
    @required bool selectedAll,
    @required int existingHzContacts,
    @required int selectedContactCount,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "free_invite_button_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "ncl": noOfContatcs,
      "tb": totalBonus,
      "sac": selectedAll,
      "ehcc": existingHzContacts,
      "scc": selectedContactCount,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSearchButtonClicked(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "search_button_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSingleCheckBoxSelected(
    BuildContext context, {
    @required int noOfContatcs,
    @required int totalBonus,
    @required bool selectedAll,
    @required int existingHzContacts,
    @required int selectedContactCount,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "single_checkbox_selected", eventMetadata: {
      "fu": user.isFirstDeposit,
      "ncl": noOfContatcs,
      "tb": totalBonus,
      "sac": selectedAll,
      "ehcc": existingHzContacts,
      "scc": selectedContactCount,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSingleCheckBoxDeSelected(
    BuildContext context, {
    @required int noOfContatcs,
    @required int totalBonus,
    @required bool selectedAll,
    @required int existingHzContacts,
    @required int selectedContactCount,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "single_checkbox_deselected", eventMetadata: {
      "fu": user.isFirstDeposit,
      "ncl": noOfContatcs,
      "tb": totalBonus,
      "sac": selectedAll,
      "ehcc": existingHzContacts,
      "scc": selectedContactCount,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSelectAll(
    BuildContext context, {
    @required int noOfContatcs,
    @required int totalBonus,
    @required int existingHzContacts,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "select_all_checkbox_selected", eventMetadata: {
      "fu": user.isFirstDeposit,
      "ncl": noOfContatcs,
      "tb": totalBonus,
      "ehcc": existingHzContacts,
    });

    AnalyticsManager().addEvent(event);
  }

  void onDeSelectAll(
    BuildContext context, {
    @required int noOfContatcs,
    @required int totalBonus,
    @required int existingHzContacts,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "select_all_checkbox_deselected", eventMetadata: {
      "fu": user.isFirstDeposit,
      "ncl": noOfContatcs,
      "tb": totalBonus,
      "ehcc": existingHzContacts,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFreeInviteBackButton(
    BuildContext context, {
    @required int noOfContatcs,
    @required int totalBonus,
    @required bool selectedAll,
    @required int existingHzContacts,
    @required int selectedContactCount,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "freeinvite_back_button_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "ncl": noOfContatcs,
      "tb": totalBonus,
      "sac": selectedAll,
      "ehcc": existingHzContacts,
      "scc": selectedContactCount,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFreeInviteBackLoaded(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event =
        Event(name: "freeinvite_back_confirmation_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFreeInviteNoClicked(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "freeinvite_no_button_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFreeInviteYesClicked(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "freeinvite_yes_button_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }
}
