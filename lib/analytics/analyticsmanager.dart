import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:solitaire_gold/analytics/event.dart';
import 'package:solitaire_gold/analytics/visit.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:http/http.dart' as http;
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedprefkeys.dart';

class AnalyticsManager {
  String _url;
  String source;
  String journey;
  static int userId;
  static Timer _timer;
  static Visit _visit;
  static int _duration;
  static int _timeout;
  bool _isEnabled;
  String analyticsCookie;
  static DateTime _lastBatchUploadTime;
  static List<Event> analyticsEvents = [];
  static DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  AnalyticsManager._internal();
  static final AnalyticsManager _analyticsManager =
      AnalyticsManager._internal();
  factory AnalyticsManager() => _analyticsManager;

  init({
    String url,
    bool enabled,
    int timeout = 30,
    int duration = 5,
    @required AppConfig config,
  }) async {
    _url = url;
    _timeout = timeout;
    _duration = duration;
    _isEnabled = enabled;

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    AndroidDeviceInfo androidInfo;
    IosDeviceInfo iosDeviceInfo;
    if (!Platform.isIOS) {
      androidInfo = await deviceInfo.androidInfo;
    } else {
      iosDeviceInfo = await deviceInfo.iosInfo;
    }

    _visit = Visit(
      productId: 1,
      userId: userId,
      deviceId: config.firebaseToken,
      domain: Uri.parse(config.apiUrl).host,
      channelId: int.parse(HttpManager.channelId),
      model: Platform.isIOS ? iosDeviceInfo.model : androidInfo.model,
      manufacturer: Platform.isIOS ? "Apple" : androidInfo.manufacturer,
      osVersion: Platform.isIOS
          ? iosDeviceInfo.systemVersion
          : androidInfo.version.release,
      osName: Platform.isIOS ? iosDeviceInfo.systemName : androidInfo.host,
      refURL: config.branchInviteUrl,
      refCode: config.branchReferralCode,
      serial: Platform.isIOS
          ? iosDeviceInfo.identifierForVendor
          : androidInfo.androidId,
      appVersion: double.parse(packageInfo.version.split(".")[0] +
          "." +
          packageInfo.version.split(".")[1]),
    );
  }

  statAnalytics() {
    if (_isEnabled == true) {
      _timer = Timer(Duration(seconds: _duration), () async {
        if (_lastBatchUploadTime == null ||
            _lastBatchUploadTime.difference(DateTime.now()) >
                Duration(minutes: _timeout)) {
          await uploadEventBatch();
        } else {
          uploadEventBatch();
        }
        AnalyticsManager().statAnalytics();
      });
    }
  }

  setUser(User user) async {
    try {
      final value = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
          .invokeMethod('getAppLaunchSource');
      if (value != null && value.length > 0) {
        setSource(value);
      }
    } catch (e) {}
    userId = user.id;
    _visit.userId = user.id;
  }

  logout() {
    userId = null;
    _visit.userId = null;
    analyticsCookie = null;
  }

  setContext(Map<String, dynamic> context) {
    try {
      _visit.utmTerm = context["utm_term"] != null ? context["utm_term"] : "";
      _visit.utmMedium =
          context["utm_medium"] != null ? context["utm_medium"] : "";
      _visit.utmSource =
          context["utm_source"] != null ? context["utm_source"] : "";
      _visit.utmContent =
          context["utm_content"] != null ? context["utm_content"] : "";
      _visit.utmCampaign =
          context["utm_campaign"] != null ? context["utm_campaign"] : "";
    } catch (e) {}
  }

  Future<bool> uploadEventBatch() async {
    if (analyticsCookie == null || analyticsCookie == "") {
      await SharedPrefHelper()
          .getFromSharedPref(SharedPrefKey.analyticsCookie)
          .then((onValue) {
        analyticsCookie = onValue;
      });
    }

    if (analyticsEvents.length > 0) {
      _visit.clientTimestamp = DateTime.now().millisecondsSinceEpoch;
      Map<String, dynamic> payload = {
        "events": analyticsEvents.getRange(0, analyticsEvents.length).toList(),
        "visit": _visit
      };
      analyticsEvents.removeRange(0, analyticsEvents.length);

      bool analyticsSent;
      try {
        analyticsSent = await http.Client()
            .post(
          _url,
          headers: {
            'cookie': analyticsCookie,
            'Content-type': 'application/json',
          },
          body: json.encode(payload),
        )
            .then((http.Response res) {
          if (res.statusCode >= 200 && res.statusCode <= 299) {
            analyticsCookie = res.headers["set-cookie"];
            SharedPrefHelper().saveToSharedPref(
                SharedPrefKey.analyticsCookie, analyticsCookie);
            _lastBatchUploadTime = DateTime.now();
            return true;
          }
          return false;
        });
      } on SocketException {
        analyticsSent = false;
      }

      return Future.value(analyticsSent);
    } else {
      return Future.value(false);
    }
  }

  addEvent(Event event) {
    if (_visit != null) {
      event.source = source;
      event.journey = journey;
      event.userId = _visit.userId;
      event.appVersion = _visit.appVersion;
      event.clientTimestamp = DateTime.now().millisecondsSinceEpoch;
      analyticsEvents.add(event);
      if (_timer == null) {
        statAnalytics();
      }
    }
  }

  setJourney(String journey) {
    this.journey = journey;
  }

  setSource(String source) {
    this.source = source;
  }
}
