class Event {
  int userId;
  final int id;
  String source;
  String journey;
  final String name;
  double appVersion;
  int clientTimestamp;
  final String network;
  int v1;
  int v2;
  int v3;
  int v4;
  int v5;
  int v6;
  int v7;
  String s1;
  String s2;
  String s3;
  String s4;
  String s5;
  final Map<String, dynamic> eventMetadata;

  Event({
    this.id = 0,
    this.name = "",
    this.userId = 0,
    this.source = "",
    this.network = "",
    this.journey = "",
    this.appVersion = 0,
    this.clientTimestamp = 0,
    this.eventMetadata = const {},
  });

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "source": source,
        "userId": userId,
        "journey": journey,
        "network": network,
        "appVersion": appVersion,
        "clientTimestamp": clientTimestamp,
        "v1": v1,
        "v2": v2,
        "v3": v3,
        "v4": v4,
        "v5": v5,
        "v6": v6,
        "v7": v7,
        "s1": s1,
        "s2": s2,
        "s3": s3,
        "s4": s4,
        "s5": s5,
        "eventMetadata": eventMetadata,
      };

  setDepositAmount(int amount) {
    this.v1 = amount;
  }

  setModeOptionId(int id) {
    this.v2 = id;
  }

  setFirstDeposit(bool isFirstDeposit) {
    this.v3 = isFirstDeposit == true ? 0 : 1;
  }

  setUserBalance(double amount) {
    this.v4 = amount.toInt();
  }

  setIsOpening(bool bOpen) {
    this.v5 = bOpen ? 0 : 1;
  }

  setPaymentRepeatChecked(bool isChecked) {
    this.v5 = isChecked ? 1 : 0;
  }

  setPayModeExpanded(bool isExpanded) {
    this.v5 = isExpanded ? 0 : 1;
  }

  setPaymentSuccess(bool isSuccess) {
    this.v5 = isSuccess ? 0 : 1;
  }

  setTile(bool isTile) {
    this.v5 = isTile ? 0 : 1;
  }

  setGatewayId(int id) {
    this.v6 = id;
  }

  setFLEM(int value) {
    this.v7 = value;
  }

  setPromoCode(String promoCode) {
    this.s1 = promoCode == null ? "" : promoCode;
  }

  setInstantCash(String instantCash) {
    this.s2 = instantCash;
  }

  setBonusCash(String bonus) {
    this.s3 = bonus;
  }

  setLockedBonusCash(String lockedBonus) {
    this.s4 = lockedBonus;
  }

  setOrderId(String orderId) {
    this.s2 = orderId;
  }

  setErrorMessage(String msg) {
    this.s5 = msg == null ? "" : msg;
  }

  setPaymentType(String type) {
    this.s3 = type;
  }

  setPaymentOptionType(String type) {
    this.s3 = type;
  }

  setClientPaymentType(String type) {
    this.s4 = type;
  }

  setMatchStartTime(int matchStartTime) {
    this.v1 = matchStartTime;
  }

  setMatchLeagueId(int leagueId) {
    this.v2 = leagueId;
  }

  setMatchFirstOrRepeatDepositor(int isFirstDeposit) {
    this.v3 = isFirstDeposit;
  }

  setMatchSportType(int sportType) {
    this.v4 = sportType;
  }

  setMatchBrandId(String brandId) {
    this.s1 = brandId;
  }
}
