import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/event.dart';
import 'package:solitaire_gold/models/databasemanager.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/lobby_data.dart';
import 'package:solitaire_gold/models/lobby/prizestructure_details.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/models/profile/user.dart';

class LobbyAnalytics {
  LobbyAnalytics._internal();
  factory LobbyAnalytics() => _gamePlayAnalytics;
  static final LobbyAnalytics _gamePlayAnalytics = LobbyAnalytics._internal();

  void onSignUp(
    BuildContext context, {
    @required String source,
    String type,
    String refCode,
    @required User user,
  }) {
    Event event = Event(name: "signup", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "signupType": type,
      "refCode": refCode,
      "mobile": user.mobile,
      "email": user.email,
    });

    AnalyticsManager().addEvent(event);
  }

  List<int> getTemplateIds(BuildContext context, int selectedTab) {
    DatabaseManager _databaseManager =
        Provider.of<DatabaseManager>(context, listen: false);
    LobbyData _lobby = _databaseManager.getLobbyData();
    List<int> templates = new List<int>();

    if (selectedTab == 0) {
      if (_lobby.asyncH2H != null) {
        _lobby.asyncH2H.forEach((element) {
          templates.add(element.templateId);
        });
      }
    } else {
      if (_lobby.smallLeagues != null) {
        _lobby.smallLeagues.forEach((element) {
          templates.add(element.templateId);
        });
      }
    }
    return templates;
  }

  void onLogin(
    BuildContext context, {
    @required String source,
    String type,
    String refCode,
    @required User user,
  }) {
    Event event = Event(name: "login", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "signupType": type,
      "refCode": refCode,
      "mobile": user.mobile,
      "email": user.email,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLobbyLoaded(
    BuildContext context, {
    @required String source,
    int selectedTab,
    List<String> banners,
  }) {
    User user = Provider.of(context, listen: false);
    List<int> templates = getTemplateIds(context, selectedTab);
    Event event = Event(name: "lobby_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "slt": selectedTab,
      "wbal": user.userBalance.getTotalBalance(),
      "gbal": user.userBalance.goldBars,
      "ltemp": templates,
      "bnlst": banners,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLobbyTabSwitch(
    BuildContext context, {
    @required String source,
    int selectedTab,
    List<String> banners,
  }) {
    User user = Provider.of(context, listen: false);
    List<int> templates = getTemplateIds(context, selectedTab);
    Event event = Event(name: "lobby_tab_switch", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "slt": selectedTab,
      "wbal": user.userBalance.getTotalBalance(),
      "gbal": user.userBalance.goldBars,
      "ltemp": templates,
      "bnlst": banners,
    });

    AnalyticsManager().addEvent(event);
  }

  void onContestCardClicked(
    BuildContext context, {
    @required String source,
    int selectedTab,
    List<String> banners,
    AsyncHeadsUp headsUp,
    SmallLeague smallLeague,
  }) {
    User user = Provider.of(context, listen: false);
    List<int> templates = getTemplateIds(context, selectedTab);
    Event event = Event(name: "contest_card_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "slt": selectedTab,
      "wbal": user.userBalance.getTotalBalance(),
      "gbal": user.userBalance.goldBars,
      "ltemp": templates,
      "bnlst": banners,
      "cef": selectedTab == 0 ? headsUp.realCurrency : smallLeague.realCurrency,
      "gbef": selectedTab == 0
          ? headsUp.virtualCurrency
          : smallLeague.virtualCurrency,
      "ppool": selectedTab == 0 ? headsUp.prizePool : smallLeague.prizePool,
      "tid": selectedTab == 0 ? headsUp.templateId : smallLeague.templateId,
      "cid":
          selectedTab == 0 ? headsUp.h2hTemplateId : smallLeague.tournamentId,
      "frmtype": selectedTab == 0 ? headsUp.formatType : smallLeague.formatType,
      "size": selectedTab == 0 ? headsUp.maxPlayers : smallLeague.maxPlayers,
      "fsize": selectedTab == 0 ? headsUp.joinedCount : smallLeague.joinedCount,
      "eftype": selectedTab == 0 ? headsUp.entryType : smallLeague.entryType,
    });

    AnalyticsManager().addEvent(event);
  }

  void onPrizeInfoLoaded(
    BuildContext context, {
    @required String source,
    List<String> banners,
    PrizeStrucutreDetails prizeStrucutreDetails,
    int maxSize,
    Map<String, dynamic> anaylticData,
  }) {
    User user = Provider.of(context, listen: false);
    int selectedTab = 1;
    List<int> templates = getTemplateIds(context, selectedTab);
    Event event = Event(name: "prize_info_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "slt": 1,
      "wbal": user.userBalance.getTotalBalance(),
      "gbal": user.userBalance.goldBars,
      "ltemp": templates,
      "bnlst": anaylticData == null ? [] : anaylticData["banners"],
      "cef": prizeStrucutreDetails.entryFee,
      "gbef": prizeStrucutreDetails.goldBarFee,
      "frmtype": 3,
      "ppool": prizeStrucutreDetails.goldBarsAmount != null &&
              prizeStrucutreDetails.goldBarsAmount != 0
          ? prizeStrucutreDetails.goldBarsAmount
          : prizeStrucutreDetails.prizeAmount,
      "tid": anaylticData == null ? null : anaylticData["tid"],
      "cid": anaylticData == null ? null : anaylticData["cid"],
      "mid": anaylticData == null ? null : anaylticData["mid"],
      "size": maxSize,
      "wnnrs": prizeStrucutreDetails.prizeCount,
    });

    AnalyticsManager().addEvent(event);
  }

  void onPlayClicked(
    BuildContext context, {
    @required String source,
    int selectedTab,
    List<String> banners,
    AsyncHeadsUp headsUp,
    SmallLeague smallLeague,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "play_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "slt": selectedTab,
      "wbal": user.userBalance.bonusAmount +
          user.userBalance.withdrawable +
          user.userBalance.depositBucket,
      "gbal": user.userBalance.goldBars,
      "bnlst": banners,
      "cef": selectedTab == 0 ? headsUp.realCurrency : smallLeague.realCurrency,
      "gbef": selectedTab == 0
          ? headsUp.virtualCurrency
          : smallLeague.virtualCurrency,
      "ppool": selectedTab == 0 ? headsUp.prizePool : smallLeague.prizePool,
      "tid": selectedTab == 0 ? headsUp.templateId : smallLeague.templateId,
      "cid": selectedTab == 0 ? headsUp.h2hTemplateId : smallLeague.matchId,
      "frmtype": selectedTab == 0 ? headsUp.formatType : smallLeague.formatType,
      "size": selectedTab == 0 ? headsUp.maxPlayers : smallLeague.maxPlayers,
      "fsize": selectedTab == 0 ? headsUp.joinedCount : smallLeague.joinedCount,
      "eftype": selectedTab == 0 ? headsUp.entryType : smallLeague.entryType,
    });

    AnalyticsManager().addEvent(event);
  }

  void onPlayResponse(
    BuildContext context, {
    @required String source,
    List<String> banners,
    AsyncHeadsUp headsUp,
    SmallLeague smallLeague,
    List<int> error,
  }) {
    User user = Provider.of(context, listen: false);
    int selectedTab = headsUp != null ? 0 : 1;
    Event event = Event(name: "play_button_response", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "slt": selectedTab,
      "wbal": user.userBalance.bonusAmount +
          user.userBalance.withdrawable +
          user.userBalance.depositBucket,
      "gbal": user.userBalance.goldBars,
      "bnlst": banners,
      "cef": selectedTab == 0 ? headsUp.realCurrency : smallLeague.realCurrency,
      "gbef": selectedTab == 0
          ? headsUp.virtualCurrency ?? 0
          : smallLeague.virtualCurrency ?? 0,
      "ppool": selectedTab == 0 ? headsUp.prizePool : smallLeague.prizePool,
      "tid": selectedTab == 0 ? headsUp.templateId : smallLeague.templateId,
      "cid": selectedTab == 0 ? headsUp.h2hTemplateId : smallLeague.matchId,
      "frmtype": selectedTab == 0 ? headsUp.formatType : smallLeague.formatType,
      "size": selectedTab == 0 ? headsUp.maxPlayers : smallLeague.maxPlayers,
      "fsize": selectedTab == 0 ? headsUp.joinedCount : smallLeague.joinedCount,
      "err": error,
      "eftype": selectedTab == 0 ? headsUp.entryType : smallLeague.entryType,
    });

    AnalyticsManager().addEvent(event);
  }

  void onJoinContest(
    BuildContext context, {
    @required String source,
    int selectedTab,
    List<String> banners,
    AsyncHeadsUp headsUp,
    SmallLeague smallLeague,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "join_contest_successful", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "slt": selectedTab,
      "wbal": user.userBalance.bonusAmount +
          user.userBalance.withdrawable +
          user.userBalance.depositBucket,
      "gbal": user.userBalance.goldBars,
      "bnlst": banners,
      "cef": selectedTab == 0 ? headsUp.realCurrency : smallLeague.realCurrency,
      "gbef": selectedTab == 0
          ? headsUp.virtualCurrency
          : smallLeague.virtualCurrency,
      "ppool": selectedTab == 0 ? headsUp.prizePool : smallLeague.prizePool,
      "tid": selectedTab == 0 ? headsUp.templateId : smallLeague.templateId,
      "cid": selectedTab == 0 ? headsUp.h2hTemplateId : smallLeague.matchId,
      "frmtype": selectedTab == 0 ? headsUp.formatType : smallLeague.formatType,
      "size": selectedTab == 0 ? headsUp.maxPlayers : smallLeague.maxPlayers,
      "fsize": selectedTab == 0 ? headsUp.joinedCount : smallLeague.joinedCount,
      "eftype": selectedTab == 0 ? headsUp.entryType : smallLeague.entryType,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRapLoaded(
    BuildContext context, {
    @required String source,
    String email,
    int matchId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "rap_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mobile": user.mobile,
      "email": email,
      "mid": matchId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRapCategorySelect(
    BuildContext context, {
    @required String source,
    String category,
    int matchId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "rap_category_choose", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "cat": category,
      "mid": matchId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRapSubCategorySelect(
    BuildContext context, {
    @required String source,
    String subCategory,
    int matchId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "rapsub_category_choose", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "scat": subCategory,
      "mid": matchId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRapSubmit(
    BuildContext context, {
    @required String source,
    String email,
    int matchId,
    String category,
    String subCategory,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "rap_submit_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mobile": user.mobile,
      "email": email,
      "mid": matchId,
      "cat": category,
      "scat": subCategory,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRapClosed(
    BuildContext context, {
    @required String source,
    String email,
    int matchId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "rap_closed", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mobile": user.mobile,
      "email": email,
      "mid": matchId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onMenuItemClicked(
    BuildContext context, {
    @required String source,
    String title,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "menu_item_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mtitle": title,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLobbyFooterItemClicked(BuildContext context,
      {@required String source, int index}) {
    String item;
    switch (index) {
      case 0:
        item = "add_cash";
        break;
      case 1:
        item = "result";
        break;
      case 2:
        item = "lobby";
        break;
      case 3:
        item = "promos";
        break;
      case 4:
        item = "account";
        break;
    }
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "lobby_" + item + "_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "bal": user.userBalance.getTotalBalance(),
      "gbal": user.userBalance.goldBars,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLobbyAccountLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "lobby_account_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "bal": user.userBalance.getTotalBalance(),
      "gbal": user.userBalance.goldBars,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLobbyResultsLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "lobby_results_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "bal": user.userBalance.getTotalBalance(),
      "gbal": user.userBalance.goldBars,
    });

    AnalyticsManager().addEvent(event);
  }

  void onBannerClicked(
    BuildContext context, {
    @required String source,
    String cta,
    String bannerId,
    List<dynamic> zoneId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "banner_clicked", eventMetadata: {
      "fu": user.userBalance == null ? null : user.isFirstDeposit,
      "src": source,
      "bal":
          user.userBalance == null ? null : user.userBalance.getTotalBalance(),
      "gbal": user.userBalance == null ? null : user.userBalance.goldBars,
      "ctype": cta,
      "bannerId": bannerId,
      "zoneId": zoneId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLoginPopupLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "login_popup_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "email": user.email,
      "mobile": user.mobile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLoginPopupClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "login_popup_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "email": user.email,
      "mobile": user.mobile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onLoginPopupClosed(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "login_popup_closed", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "email": user.email,
      "mobile": user.mobile,
    });

    AnalyticsManager().addEvent(event);
  }
}
