import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/event.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/leaderboard.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:provider/provider.dart';

class GamePlayAnalytics {
  GamePlayAnalytics._internal();
  factory GamePlayAnalytics() => _gamePlayAnalytics;
  static final GamePlayAnalytics _gamePlayAnalytics =
      GamePlayAnalytics._internal();

  void onProfileScreenLoaded(
    BuildContext context, {
    @required String source,
    String teamName,
    String fname,
    String lname,
    String gender,
    String dob,
    String email,
    String mobile,
    String state,
    String city,
    String pincode,
    int avatarId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "profile_screen_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "tn": teamName,
      "bal": (user.userBalance.withdrawable +
              user.userBalance.depositBucket +
              user.userBalance.bonusAmount)
          .toInt(),
      "n": fname,
      "ln": lname,
      "g": gender,
      "dob": dob,
      "e": email,
      "m": mobile,
      "s": state,
      "c": city,
      "pin": pincode,
      "avatarId": avatarId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onProfileUpdated(
    BuildContext context, {
    @required String source,
    String teamName,
    String fname,
    String lname,
    String gender,
    String dob,
    String email,
    String mobile,
    String state,
    String city,
    String pincode,
    int avatarId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "profile_updated", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "tn": teamName,
      "bal": (user.userBalance.withdrawable +
              user.userBalance.depositBucket +
              user.userBalance.bonusAmount)
          .toInt(),
      "n": fname,
      "ln": lname,
      "g": gender,
      "dob": dob,
      "e": email,
      "m": mobile,
      "s": state,
      "c": city,
      "pin": pincode,
      "avtrId": avatarId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onDisplayPicClicked(
    BuildContext context, {
    int avatarId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "display_pic_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "profile_page",
      "bal": user.userBalance.getTotalBalance(),
      "avtrId": avatarId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onAvatarPopUpLoaded(
    BuildContext context, {
    int avatarId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "avatar_popup_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "profile_page",
      "bal": user.userBalance.getTotalBalance(),
      "avtrId": avatarId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onAvatarClicked(
    BuildContext context, {
    int avatarId,
    int newAvatarId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "avatar_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "profile_page",
      "bal": user.userBalance.getTotalBalance(),
      "avtrId": avatarId,
      "nwAvtrId": newAvatarId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onUpdateAvatarClicked(
    BuildContext context, {
    int avatarId,
    int newAvatarId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "update_avatar_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "profile_page",
      "bal": user.userBalance.getTotalBalance(),
      "avtrId": avatarId,
      "nwAvtrId": newAvatarId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onCloseAvatarClicked(
    BuildContext context, {
    int avatarId,
    int newAvatarId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "close_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "profile_page",
      "bal": user.userBalance.getTotalBalance(),
      "avtrId": avatarId,
      "nwAvtrId": newAvatarId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onAccountSummaryLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "account_summary_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "app_drawer",
      "tbal": (user.userBalance.withdrawable +
          user.userBalance.depositBucket +
          user.userBalance.bonusAmount),
      "dep": user.userBalance.depositBucket,
      "bns": user.userBalance.bonusAmount,
      "wdrwl": user.userBalance.withdrawable,
      "lkdb": user.userBalance.lockedBonusAmount
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawLoaded(
    BuildContext context, {
    @required String source,
    String ifsc,
    String accountNo,
    String upiId,
    String mode,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_page_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wdrwlbal": user.userBalance.withdrawable,
      "wtyp": mode,
      "fname": user.address.firstName != null ? user.address.firstName : "",
      "lnmae": user.address.lastName != null ? user.address.lastName : "",
      "ifsc": ifsc,
      "acno": accountNo,
      "upiid": upiId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawTypeSelected(BuildContext context,
      {@required String source, String withdrawMode}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_type_sel", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wtyp": withdrawMode,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRequestWithdrawClicked(
    BuildContext context, {
    @required String source,
    String fName,
    String lName,
    String withdrawMode,
    String ifsc,
    String accountNo,
    String upiId,
    double withdrawAmount,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "request_withdraw_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wdrwlbal": user.userBalance.withdrawable,
      "wtyp": withdrawMode,
      "fname": fName,
      "lnmae": lName,
      "ifsc": ifsc,
      "acno": accountNo,
      "upiid": upiId,
      "wamnt": withdrawAmount
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawRaised(
    BuildContext context, {
    @required String source,
    String fName,
    String lName,
    String withdrawMode,
    String ifsc,
    String accountNo,
    String upiId,
    double withdrawAmount,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_raised_successfully", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wdrwlbal": user.userBalance.withdrawable,
      "wtyp": withdrawMode,
      "fname": fName,
      "lnmae": lName,
      "ifsc": ifsc,
      "acno": accountNo,
      "upiid": upiId,
      "wamnt": withdrawAmount
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawHistoryClicked(
    BuildContext context, {
    @required String source,
    String fName,
    String lName,
    String withdrawMode,
    String ifsc,
    String accountNo,
    String upiId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_history_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wdrwlbal": user.userBalance.withdrawable,
      "wtyp": withdrawMode,
      "fname": fName,
      "lnmae": lName,
      "ifsc": ifsc,
      "acno": accountNo,
      "upiid": upiId,
    });
    AnalyticsManager().addEvent(event);
  }

  void onWithdrawHistoryUSClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_history_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawHistoryLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_history_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onStateDobPopupLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "dob_state_popup_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onStateDobCancel(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "dob_state_cancel_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onStateDobSubmitClicked(
    BuildContext context, {
    @required String source,
    String state,
    String dob,
    bool error,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "dob_submit clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onStateDobSubmitted(
    BuildContext context, {
    @required String source,
    String state,
    String dob,
    bool error,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "dob_submitted", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "s": state,
      "dob": dob,
      "error": error.toString(),
    });

    AnalyticsManager().addEvent(event);
  }

  void stateDobCancelClicked(BuildContext context, {@required String source}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "state_dob_cancel_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycAddTypeSelected(
    BuildContext context, {
    @required String source,
    String addType,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_add_type_selected", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "atyp": addType,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycAddFrontClicked(
    BuildContext context, {
    @required String source,
    String addType,
    String frontFile,
    String backFile,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_add_front_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "atyp": addType,
      "fcpy": frontFile,
      "bcpy": backFile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycAddBackClicked(
    BuildContext context, {
    @required String source,
    String addType,
    String frontFile,
    String backFile,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_add_back_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "atyp": addType,
      "fcpy": frontFile,
      "bcpy": backFile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycAddFrontSelected(
    BuildContext context, {
    @required String source,
    String addType,
    String frontFile,
    String backFile,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_add_front_selected", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "atyp": addType,
      "fcpy": frontFile,
      "bcpy": backFile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycAddBackSelected(
    BuildContext context, {
    @required String source,
    String addType,
    String frontFile,
    String backFile,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_add_back_selected", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "atyp": addType,
      "fcpy": frontFile,
      "bcpy": backFile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycAddUploadClicked(
    BuildContext context, {
    @required String source,
    String addType,
    String frontFile,
    String backFile,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_add_upload_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "atyp": addType,
      "fcpy": frontFile,
      "bcpy": backFile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycAddUploaded(
    BuildContext context, {
    @required String source,
    String addType,
    String frontFile,
    String backFile,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "kyc_add_uploaded_successfully", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "atyp": addType,
      "fcpy": frontFile,
      "bcpy": backFile,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycPanClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "pan_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycPanSubmit(
    BuildContext context, {
    @required String source,
    String pan,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "pan_submit_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "pan": pan,
    });

    AnalyticsManager().addEvent(event);
  }

  void onKycPanSubmitted(
    BuildContext context, {
    @required String source,
    String pan,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "pan_submitted_successfully", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "es": user.verificationStatus.emailVerified ? "verified" : "notverified",
      "ms": user.verificationStatus.mobileVerified ? "verified" : "notverified",
      "ks": user.verificationStatus.addressVerificationStatus,
      "ps": user.verificationStatus.panVerificationStatus,
      "pan": pan,
    });

    AnalyticsManager().addEvent(event);
  }

  void onContactUsPageLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "contact_us_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onContactUsCategorySelected(
    BuildContext context, {
    @required String source,
    String category,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "contact_us_category_choose", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "cat": category,
    });

    AnalyticsManager().addEvent(event);
  }

  void onContactUsSubcategorySelected(
    BuildContext context, {
    @required String source,
    String subcategory,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "contact_us_sub_category_choose", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "scat": subcategory,
    });

    AnalyticsManager().addEvent(event);
  }

  void onContactUsSubmit(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "contact_us_submit_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onAddCashClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "addcash_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wbal": user.userBalance.withdrawable,
      "gbal": user.userBalance.goldBars
    });

    AnalyticsManager().addEvent(event);
  }

  void onChargeBackRequestClicked(
    BuildContext context, {
    @required String source,
    int minAmount,
    int maxAmount,
    int processingFee,
    double amountEntered,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "refund_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "chrgbck_page",
      "rfndbal": user.userBalance.depositBucket,
      "totalbal": user.userBalance.getTotalBalance(),
      "minamt": minAmount,
      "maxamt": maxAmount,
      "pfee": processingFee,
      "amtentered": amountEntered,
      "rfndreq": amountEntered - processingFee,
    });

    AnalyticsManager().addEvent(event);
  }

  void onChargeBackRaised(
    BuildContext context, {
    @required String source,
    int minAmount,
    int maxAmount,
    int processingFee,
    double amountEntered,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "request_raised_successfully", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "chrgbck_page",
      "rfndbal": user.userBalance.depositBucket - amountEntered,
      "totalbal": user.userBalance.getTotalBalance(),
      "minamt": minAmount,
      "maxamt": maxAmount,
      "pfee": processingFee,
      "amtentered": amountEntered,
      "rfndreq": amountEntered - processingFee,
    });

    AnalyticsManager().addEvent(event);
  }

  void onChargeBackLoaded(
    BuildContext context, {
    @required String source,
    int minAmount,
    int maxAmount,
    int processingFee,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "chargeback_page_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": "app_drawer",
      "rfndbal": user.userBalance.depositBucket,
      "totalbal": user.userBalance.getTotalBalance(),
      "minamt": minAmount,
      "maxamt": maxAmount,
      "pfee": processingFee,
    });

    AnalyticsManager().addEvent(event);
  }

  void onChargebackHistoryClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "refund_history_clicked ", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onChargebackHistoryLoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "refund_history_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawUSLoaded(
    BuildContext context, {
    @required String source,
    String routingNumber,
    String accountNo,
    bool bAddressPresent,
    String mode,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_page_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wdrwlbal": user.userBalance.withdrawable,
      "add": bAddressPresent,
      "totalbal": user.userBalance.getTotalBalance(),
      "wtyp": mode,
      "routingno": routingNumber,
      "acno": accountNo,
    });

    AnalyticsManager().addEvent(event);
  }

  void onRequestWithdrawUSClicked(
    BuildContext context, {
    @required String source,
    String fName,
    String lName,
    String withdrawMode,
    String routingNumber,
    String accountNo,
    double processingFee,
    double withdrawAmount,
    bool bAddressPresent,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "request_withdraw_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wdrwlbal": user.userBalance.withdrawable,
      "add": bAddressPresent,
      "totalbal": user.userBalance.getTotalBalance(),
      "wtyp": withdrawMode,
      "routingno": routingNumber,
      "pfee": processingFee,
      "acno": accountNo,
      "wamt": withdrawAmount,
      "fname": lName,
      "lname": fName,
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawUSRaised(
    BuildContext context, {
    @required String source,
    String fName,
    String lName,
    String withdrawMode,
    String routingNumber,
    String accountNo,
    double withdrawAmount,
    double processingFee,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "withdraw_raised_successfully", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "wdrwlbal": user.userBalance.withdrawable,
      "wtyp": withdrawMode,
      "fname": fName,
      "lnmae": lName,
      "routingno": routingNumber,
      "acno": accountNo,
      "wamnt": withdrawAmount,
      "pfee": processingFee,
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawAddAddressClicked(
    BuildContext context, {
    @required String source,
    bool bAdressPresent,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "add_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "add": bAdressPresent
    });

    AnalyticsManager().addEvent(event);
  }

  void onWithdrawUpdateAddressClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "update_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onTotalBalanceInfoClicked(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "total_bal_info_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "winnings": user.userBalance.withdrawable,
      "deposit": user.userBalance.depositBucket,
      "bonus": user.userBalance.bonusAmount,
      "totalbal": user.userBalance.getTotalBalance(),
    });

    AnalyticsManager().addEvent(event);
  }

  void onEndGameLoaded(
    BuildContext context, {
    @required String source,
    AsyncHeadsUp headsUp,
    SmallLeague league,
    Leaderboard leaderboard,
    int matchId,
    int gameId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "end_game_pop_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "tid": headsUp != null
          ? headsUp.templateId
          : league != null ? league.templateId : 0,
      "tblid": headsUp != null ? 0 : league != null ? league.tableId : 0,
      "cid": headsUp != null
          ? headsUp.h2hTemplateId
          : league != null ? league.tournamentId : 0,
      "mid": leaderboard != null ? leaderboard.matchId : matchId,
      "eftype": headsUp != null
          ? headsUp.entryType
          : league != null ? league.entryType : null,
      "gid": gameId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onEndNowClicked(
    BuildContext context, {
    @required String source,
    AsyncHeadsUp headsUp,
    SmallLeague league,
    Leaderboard leaderboard,
    int matchId,
    int gameId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "end_now_cilcked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "tid": headsUp != null
          ? headsUp.templateId
          : league != null ? league.templateId : 0,
      "tblid": headsUp != null ? 0 : league != null ? league.tableId : 0,
      "cid": headsUp != null
          ? headsUp.h2hTemplateId
          : league != null ? league.tournamentId : 0,
      "mid": leaderboard != null ? leaderboard.matchId : matchId,
      "eftype": headsUp != null
          ? headsUp.entryType
          : league != null ? league.entryType : null,
      "gid": gameId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onPlayOnClicked(
    BuildContext context, {
    @required String source,
    AsyncHeadsUp headsUp,
    Leaderboard leaderboard,
    SmallLeague league,
    int matchId,
    int gameId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "play_on_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "tid": headsUp != null
          ? headsUp.templateId
          : league != null ? league.templateId : 0,
      "tblid": headsUp != null ? 0 : league != null ? league.tableId : 0,
      "cid": headsUp != null
          ? headsUp.h2hTemplateId
          : league != null ? league.tournamentId : 0,
      "mid": leaderboard != null ? leaderboard.matchId : matchId,
      "eftype": headsUp != null
          ? headsUp.entryType
          : league != null ? league.entryType : null,
      "gid": gameId,
    });

    AnalyticsManager().addEvent(event);
  }

  void onGameOverLoaded(
    BuildContext context, {
    @required String source,
    AsyncHeadsUp headsUp,
    Leaderboard leaderboard,
    SmallLeague league,
    int matchId,
    int score,
    int timeBonus,
    int gameId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "game_over_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "tid": headsUp != null
          ? headsUp.templateId
          : league != null ? league.templateId : 0,
      "tblid": headsUp != null ? 0 : league != null ? league.tableId : 0,
      "cid": headsUp != null
          ? headsUp.h2hTemplateId
          : league != null ? league.tournamentId : 0,
      "mid": leaderboard != null ? leaderboard.matchId : matchId,
      "eftype": headsUp != null
          ? headsUp.entryType
          : league != null ? league.entryType : null,
      "gid": gameId,
      "score": score,
      "tbonus": timeBonus,
      "tscore": score + timeBonus,
    });

    AnalyticsManager().addEvent(event);
  }

  void onSubmitScoreClicked(
    BuildContext context, {
    @required String source,
    AsyncHeadsUp headsUp,
    Leaderboard leaderboard,
    SmallLeague league,
    int matchId,
    int score,
    int timeBonus,
    int gameId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "game_over_submit_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "tid": headsUp != null
          ? headsUp.templateId
          : league != null ? league.templateId : 0,
      "tblid": headsUp != null ? 0 : league != null ? league.tableId : 0,
      "cid": headsUp != null
          ? headsUp.h2hTemplateId
          : league != null ? league.tournamentId : 0,
      "mid": leaderboard != null ? leaderboard.matchId : matchId,
      "eftype": headsUp != null
          ? headsUp.entryType
          : league != null ? league.entryType : null,
      "gid": gameId,
      "score": score,
      "tbonus": timeBonus,
      "tscore": score + timeBonus,
    });

    AnalyticsManager().addEvent(event);
  }

  void onResultScreenLoaded(
    BuildContext context, {
    @required String source,
    Leaderboard leaderboardData,
    int matchId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "result_screen_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mid": leaderboardData.matchId,
      "cef": leaderboardData.entryFeeRealCurrency,
      "gbef": leaderboardData.entryFeevirtualCurrency,
      "jnct": leaderboardData.joinedCount,
      "size": leaderboardData.size,
      "ppool": leaderboardData.prizePool,
      "etype": leaderboardData.rewardType,
      "frmtype": leaderboardData.formatType,
      "mstatus": leaderboardData.matchStatus,
      "allowtie": leaderboardData.allowTiebreaker,
    });

    AnalyticsManager().addEvent(event);
  }

  void onResultInfoClicked(
    BuildContext context, {
    @required String source,
    Leaderboard leaderboardData,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "result_info_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mid": leaderboardData.matchId,
      "cef": leaderboardData.entryFeeRealCurrency,
      "gbef": leaderboardData.entryFeevirtualCurrency,
      "jnct": leaderboardData.joinedCount,
      "size": leaderboardData.size,
      "ppool": leaderboardData.prizePool,
      "etype": leaderboardData.rewardType,
      "frmtype": leaderboardData.formatType,
      "mstatus": leaderboardData.matchStatus,
      "allowtie": leaderboardData.allowTiebreaker,
    });

    AnalyticsManager().addEvent(event);
  }

  void onOkClicked(
    BuildContext context, {
    @required String source,
    Leaderboard leaderboardData,
    int matchId,
    double score,
    double winAmountGems,
    double winAmountCash,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "result_ok_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mid": leaderboardData.matchId,
      "cef": leaderboardData.entryFeeRealCurrency,
      "gbef": leaderboardData.entryFeevirtualCurrency,
      "jnct": leaderboardData.joinedCount,
      "size": leaderboardData.size,
      "ppool": leaderboardData.prizePool,
      "etype": leaderboardData.rewardType,
      "frmtype": leaderboardData.formatType,
      "mstatus": leaderboardData.matchStatus,
      "allowtie": leaderboardData.allowTiebreaker,
      "tscore": score,
      "wamntc": winAmountCash,
      "wamntgb": winAmountGems,
    });

    AnalyticsManager().addEvent(event);
  }

  void onResultCloseClicked(
    BuildContext context, {
    @required String source,
    Leaderboard leaderboardData,
    int gameId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "result_close_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "mid": leaderboardData.matchId,
      "cef": leaderboardData.entryFeeRealCurrency,
      "gbef": leaderboardData.entryFeevirtualCurrency,
      "gid": gameId,
      "jnct": leaderboardData.joinedCount,
      "size": leaderboardData.size,
      "ppool": leaderboardData.prizePool,
      "etype": leaderboardData.rewardType,
      "frmtype": leaderboardData.formatType,
      "mstatus": leaderboardData.matchStatus,
      "allowtie": leaderboardData.allowTiebreaker,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFTUELoaded(
    BuildContext context, {
    @required String source,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "ftue_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFTUENextClicked(
    BuildContext context, {
    @required String source,
    int screenNumber,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "ftue_next_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "scrno": screenNumber,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFTUECloseClicked(
    BuildContext context, {
    @required String source,
    int screenNumber,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "ftue_close_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "scrno": screenNumber,
    });

    AnalyticsManager().addEvent(event);
  }

  void onFTUESwiped(
    BuildContext context, {
    @required String source,
    int screenNumber,
    String direction,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "ftue_swiped", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "scrno": screenNumber,
      "swipe": direction,
    });

    AnalyticsManager().addEvent(event);
  }

  void onReplayClicked(
    BuildContext context, {
    @required String source,
    int matchId,
  }) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "replay_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
      "bal": user.userBalance.getTotalBalance(),
      "mid": matchId,
      "src": source,
      "uid": user.id,
    });

    AnalyticsManager().addEvent(event);
  }

  void onGeoLocationPopupRequested(BuildContext context,
      {@required String source}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "loc_popup_requested", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onGeoLocationAccessGiven(BuildContext context,
      {@required String access}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "loc_access_given", eventMetadata: {
      "fu": user.isFirstDeposit,
      "access": access,
    });

    AnalyticsManager().addEvent(event);
  }

  void onGeoLocationAccessDeniedPopup(BuildContext context,
      {@required String source}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "loc_access_denied_popup_loaded", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
    });

    AnalyticsManager().addEvent(event);
  }

  void onGeoLocationAccessDeniedPopupSettingsClicked(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "loc_settings_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onGeoLocationAccessDeniedPopupCancelClicked(BuildContext context) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "loc_cancel_clicked", eventMetadata: {
      "fu": user.isFirstDeposit,
    });

    AnalyticsManager().addEvent(event);
  }

  void onGeoLocationStatus(BuildContext context,
      {@required String source, @required bool status}) {
    User user = Provider.of(context, listen: false);
    Event event = Event(name: "loc_gps_status", eventMetadata: {
      "fu": user.isFirstDeposit,
      "src": source,
      "st": status,
    });

    AnalyticsManager().addEvent(event);
  }

  void onAppLaunch(
    BuildContext context,
  ) {
    Event event = Event(
      name: "app_launch",
    );

    AnalyticsManager().addEvent(event);
  }
}
