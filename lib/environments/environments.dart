class Environments {
  final int _value;

  const Environments._internal(this._value);

  int get value => _value;

  static Environments from(int value) => Environments._internal(value);

  static const STAGE_ANDROID = Environments._internal(1);
  static const STAGE_IOS = Environments._internal(2);
  static const PROD_ANDROID = Environments._internal(3);
  static const PROD_IOS = Environments._internal(4);
  static const STAGE_ANDROID_US = Environments._internal(5);
  static const STAGE_IOS_US = Environments._internal(6);
  static const PROD_ANDROID_US = Environments._internal(7);
  static const PROD_IOS_US = Environments._internal(8);
  static const LOCAL_ANDROID = Environments._internal(9);
  static const MI_INDIA = Environments._internal(15);
  static const OPPO_INDIA = Environments._internal(16);
  static const VIVO_INDIA = Environments._internal(17);

}
