class RequestType {
  final int _value;
  const RequestType._internal(this._value);

  int get value => _value;

  get hashCode => _value.hashCode;

  operator ==(status) => status._value == this._value;

  toString() => _value.toString();

  static RequestType from(int value) => RequestType._internal(value);

  static const REQ_PING = const RequestType._internal(0);
  static const RES_PING = const RequestType._internal(0);
  static const READY = const RequestType._internal(1);

  static const UNAUTHORISED = const RequestType._internal(201);
  static const INVALID_MESSAGE = const RequestType._internal(202);
  static const UNKNOWN_EXCEPTION = const RequestType._internal(203);

  static const LOBBY_DATA = const RequestType._internal(301);
  static const TOURNAMENT_UPDATE = const RequestType._internal(302);
  static const ASYNC_HEADS_UP_UPDATE = const RequestType._internal(303);
  static const HEADS_UP_UPDATE = const RequestType._internal(304);
  static const LEADERBOARD = const RequestType._internal(305);
  static const MY_MATCHES = const RequestType._internal(306);
  static const LEAGUES_UPDATE = const RequestType._internal(307);
}
