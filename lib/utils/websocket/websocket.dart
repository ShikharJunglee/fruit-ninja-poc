import 'dart:async';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/websocket/requesttype.dart';
import 'package:web_socket_channel/io.dart';

const int PING_PONG_TIME = 2;

class WebSocket {
  String _url;
  bool _isConnected = false;
  IOWebSocketChannel _channel;
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  StreamController<dynamic> _streamController = StreamController.broadcast();

  StreamSubscription<dynamic> _messageListener;

  WebSocket._internal();
  factory WebSocket() => socket;
  static final WebSocket socket = WebSocket._internal();

  int iPingCount = 0;
  int retryCount = 0;
  Timer _pingPongTimer;
  int pingPongTimeInSeconds = PING_PONG_TIME;
  bool bConnectionProcessInProgress = false;

  connect(String url) async {
    _url = url;
    bConnectionProcessInProgress = true;

    if (_messageListener != null) {
      _messageListener.cancel();
      _channel.sink.close();
    }

    _channel = null;
    iPingCount = 0;
    pingPongTimeInSeconds = PING_PONG_TIME;

    _channel = IOWebSocketChannel.connect(_url, headers: {
      "cookie": HttpManager.cookie,
    });

    //setConnectionStatus(true);

    if (_pingPongTimer == null || !_pingPongTimer.isActive) {
      _startPingPong();
    }
    _listenWSMsg();

    addConnectivityListeners();
  }

  void setConnectionStatus(bool connected) {
    _isConnected = connected;
    // EventManager().publish(EventName.WEBSOCKET_CONNECTION, data: connected);
  }

  void addConnectivityListeners() {
    if (_connectivitySubscription == null) {
      _connectivitySubscription = Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        if (result == ConnectivityResult.none) {
          setConnectionStatus(false);
        } else if (!_isConnected) {
          setConnectionStatus(false);
          connect(_url);
        }
      });
    }
  }

  bool isConnected() {
    return _isConnected;
  }

  _listenWSMsg() async {
    if (_channel != null && _channel.stream != null) {
      _channel.changeStream((stream) {
        _messageListener = stream.listen(
          (onData) {
            var response = json.decode(onData);
            RequestType requestType = RequestType.from(response["type"]);

            iPingCount = 0;
            if (requestType != RequestType.RES_PING) {
              _streamController.add(response);
            }
            if (requestType == RequestType.READY) {
              setConnectionStatus(true);
            }
            bConnectionProcessInProgress = false;
            pingPongTimeInSeconds = PING_PONG_TIME;
          },
          onError: (error, StackTrace stackTrace) {
            pingPongTimeInSeconds = PING_PONG_TIME;
          },
          onDone: () {
            setConnectionStatus(false);
            bConnectionProcessInProgress = false;
            pingPongTimeInSeconds = PING_PONG_TIME;
          },
        );
        return null;
      });
    }
  }

  _startPingPong() {
    _pingPongTimer = Timer(Duration(seconds: pingPongTimeInSeconds), () {
      if (!bConnectionProcessInProgress) {
        iPingCount++;
        if (iPingCount > 3 || !_isConnected) {
          setConnectionStatus(false);
          connect(_url);
        } else {
          this.sendMessage({"type": RequestType.REQ_PING.value});
        }
      }
      _pingPongTimer.cancel();
      _startPingPong();
    });
  }

  stopPingPong() {
    _pingPongTimer.cancel();
    reset();
  }

  StreamController<dynamic> subscriber() {
    return _streamController;
  }

  sendMessage(Map<String, dynamic> msg) async {
    if (_channel != null && _isConnected) {
      _channel.sink.add(json.encode(msg));
    }
  }

  reset() {
    if (_channel != null && _channel.sink != null) {
      _channel.sink.close();
      _messageListener.cancel();
      _connectivitySubscription.cancel();
    }
  }
}
