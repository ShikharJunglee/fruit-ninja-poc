import 'package:solitaire_gold/utils/websocket/requesttype.dart';

class LobbyRequest {
  RequestType _type;

  LobbyRequest() {
    this._type = RequestType.LOBBY_DATA;
  }

  Map<String, dynamic> toJson() => {
        "type": this._type.toString(),
      };
}

class MyMatchesRequest {
  RequestType _type;

  MyMatchesRequest() {
    this._type = RequestType.MY_MATCHES;
  }

  Map<String, dynamic> toJson() => {
        "type": this._type.toString(),
      };
}

class LeaderboardRequest {
  RequestType _type;
  int _matchId;
  int _prizeStructureId;

  LeaderboardRequest(int matchId, {int pId}) {
    this._type = RequestType.LEADERBOARD;
    this._matchId = matchId;
    this._prizeStructureId = pId;
  }

  Map<String, dynamic> toJson() => {
        "type": this._type.toString(),
        "matchId": this._matchId,
        "prizeStructureId": this._prizeStructureId,
      };
}
