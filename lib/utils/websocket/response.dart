import 'package:solitaire_gold/utils/websocket/requesttype.dart';

class Response {
  final bool error;
  final RequestType type;
  final Map<String, dynamic> data;

  Response({this.error, this.type, this.data});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      data: json["data"],
      error: json["error"],
      type: RequestType.from(json["type"]),
    );
  }
}
