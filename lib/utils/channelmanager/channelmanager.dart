import 'package:flutter/services.dart';

class ChannelManager {
  static const MethodChannel BROWSER_LAUNCHER =
      const MethodChannel('com.silvercenturion.browser');
  static const WEBENGAGE = const MethodChannel('com.silvercenturion.webengage');
  static const FLUTTER_TO_NATIVE_CHANNEL =
      const MethodChannel('com.silvercenturion.utils');
  static const LOCATION_MANAGER_CHANNEL =
      const MethodChannel('com.silvercenturion.locationmanager');
  static const FIREBASE_FCM = const MethodChannel('com.silvercenturion.fcm');
  static const BRANCH_IO = const MethodChannel('com.silvercenturion.branch');
  static const SOCIAL_SHARE =
      const MethodChannel('com.silvercenturion.socialshare');
  static const RAZORPAY = const MethodChannel('com.silvercenturion.razorpay');
  static const TECHPROCESS =
      const MethodChannel('com.silvercenturion.techprocess');
  static const PAYTMNATIVE =
      const MethodChannel('com.silvercenturion.paytmnativechannel');
  static const LOGINSERVICE =
      const MethodChannel('com.silvercenturion.loginservice');
  static const AUTH_EVENTS_STREAM_CHANNEL =
      const EventChannel('com.silvercenturion.authevents');
  static const NATIVE_TO_FLUTTER_EVENT_STREAM_CHANNEL =
      const EventChannel('com.silvercenturion.eventalertsstream');
  static const GPS_STREAM_CHANNEL =
      const MethodChannel('com.silvercenturion.gps');
}
