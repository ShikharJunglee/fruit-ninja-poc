import 'dart:io';
import 'package:permission/permission.dart';

class StoragePermission {
  PermissionStatus permissionStatus = PermissionStatus.allow;

  Future<PermissionStatus> checkForPermission() async {
    if (!Platform.isIOS) {
      List<Permissions> permissions =
          await Permission.getPermissionsStatus([PermissionName.WriteStorage]);

      if (permissions[0].permissionStatus != PermissionStatus.allow) {
        return await askForPermission();
      }
      return permissions[0].permissionStatus;
    }
    return null;
  }

  Future<PermissionStatus> askForPermission() async {
    final result =
        await Permission.requestPermissions([PermissionName.WriteStorage]);
    if (result != null) {
      return result[0].permissionStatus;
    }
    return null;
  }
}
