import 'package:intl/intl.dart';

class CurrencyFormat {
  static NumberFormat formatCurrency;

  static String format(
    double amount, {
    int decimalDigits = 0,
    String name = "INR",
    bool showDelimeters = true,
  }) {
    if (formatCurrency == null) {
      formatCurrency = NumberFormat.compactSimpleCurrency(
        locale: "hi_IN",
        name: name,
        decimalDigits: decimalDigits,
      );
    } else {
      formatCurrency.maximumFractionDigits = decimalDigits;
    }
    if (!showDelimeters) {
      return formatCurrency.format(amount).replaceAll(",", "");
    }
    return formatCurrency.format(amount);
  }
}
