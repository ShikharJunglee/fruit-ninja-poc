class AppUrl {
  final String _value;
  const AppUrl._internal(this._value);

  String get value => _value;

  // String get toString => _value;

  static AppUrl from(String value) => AppUrl._internal(value);

  static const INIT_DATA = const AppUrl._internal("app/initdata");
  static const LOGIN_URL = const AppUrl._internal("auth/login");
  static const LOGOUT_URL = const AppUrl._internal("auth/logout");
  static const AUTH_CHECK_URL = const AppUrl._internal("user/status");
  static const SIGN_UP = const AppUrl._internal("auth/signup/2");
  static const REQUEST_OTP = const AppUrl._internal("auth/send-otp");
  static const RESEND_OTP = const AppUrl._internal("auth/resend-otp");
  static const OTP_SIGNIN = const AppUrl._internal("auth/authenticate-otp/2");
  static const PASSSWORD_SIGNIN = const AppUrl._internal("auth/login");
  static const GET_SIGNIN_BANNERS = const AppUrl._internal("app/banners");
  static const FORGOT_PASSWORD = const AppUrl._internal("ups/forgotPassword");
  static const SOCIAL_SIGN_IN = const AppUrl._internal("auth/social-signin/2");
  static const APPLE_NATIVE_LOGIN_URL =
      const AppUrl._internal("auth/apple/nativelogin");
  static const APPLE_WEB_LOGIN_URL =
      const AppUrl._internal("auth/apple/weblogin?");
  static const GET_BANNERS = const AppUrl._internal("app/banners/");
  static const CHANGE_TEAM_NAME =
      const AppUrl._internal("user/update-teamname");
  static const UPDATE_USER_PROFILE =
      const AppUrl._internal("user/update-profile");
  static const GET_STATE_LIST =
      const AppUrl._internal("verification/states-list");
  static const UPDATE_STATE_DOB = const AppUrl._internal("user/state-dob");
  static const GET_AVATAR_LIST = const AppUrl._internal("user/get-avatars");
  static const UPADATE_AVATAR = const AppUrl._internal("user/update-avatar");

  static const GET_STATE_FROM_LAT_LONG =
      const AppUrl._internal("withdraw/latlongstate");

  static const GET_USER_PROFILE =
      const AppUrl._internal("verification/user-info");
  static const AUTH_WITHDRAW = const AppUrl._internal("withdraw/auth");
  static const AUTH_WITHDRAW_US = const AppUrl._internal("withdraw/auth-us");
  static const WITHDRAW = const AppUrl._internal("withdraw");
  static const WITHDRAW_US = const AppUrl._internal("withdraw/us");
  static const WITHDRAW_TSEVO = const AppUrl._internal("withdraw/tsevo");
  static const WITHDRAW_TSEVO_INIT_PAYOUT =
      const AppUrl._internal("withdraw/tsevo-init?");

  static const WITHDRAW_HISTORY = const AppUrl._internal("withdraw/history");
  static const CANCEL_WITHDRAW = const AppUrl._internal("withdraw/cancel/");
  static const SEND_VERIFICATION_OTP =
      const AppUrl._internal("verification/activate/mobile");
  static const VERIFY_OTP = const AppUrl._internal("verification/mobile");
  static const SEND_VERIFICATION_MAIL =
      const AppUrl._internal("verification/activate/email");
  static const KYC_DOC_LIST =
      const AppUrl._internal("verification/kyc-doc-list");
  static const UPLOAD_DOC_ADDRESS =
      const AppUrl._internal("verification/upload-doc-address/");
  static const RESEND_VERIFICATION_OTP =
      const AppUrl._internal("verification/otp/resend");
  static const SUBMIT_PAN_NUMBER =
      const AppUrl._internal("verification/submit-pan-number/");
  static const VERIFICATION_STATUS = const AppUrl._internal("ups/user/verify");

  static const CONTACTUS_FORM = const AppUrl._internal("app/contact-us");
  static const CONTACTUS_SUBMIT = const AppUrl._internal("app/submit-problem");
  static const REPORT_PROBLEM_SUBMIT =
      const AppUrl._internal("app/submit-problem-attachment");

  static const ACCOUNT_DETAILS = const AppUrl._internal("user/account-summary");

  static const GET_RAF_DATA = const AppUrl._internal("user/raf-data");
  static const GET_EXISTING_USERS =
      const AppUrl._internal("app/get-existing-app-users");
  static const SAVE_INSTALLED_APPS =
      const AppUrl._internal("app/save-installed-apps");
  static const SAVE_CONTACT_BOOK =
      const AppUrl._internal("user/save-contact-book");
  static const SEND_INVITE_VIA_SMS =
      const AppUrl._internal("user/inviteviasms");

  static const GET_PRIZESTRUCTURE =
      const AppUrl._internal("lobby/prize-structure/");
  static const GET_LEADERBOARD_DATA_WITH_MATCHID =
      const AppUrl._internal("lobby/leaderboard/");
  static const JOIN_CONTEST = const AppUrl._internal("lobby/join-contest-auth");

  static const GET_AMOUNT_TILES =
      const AppUrl._internal("payments/amount-tiles");
  static const PAYMENT_MODE = const AppUrl._internal("payments/proceed");
  static const VALIDATE_PROMO =
      const AppUrl._internal("payments/validate-promo");
  static const INIT_PAYMENT_PAYTM =
      const AppUrl._internal("payments/init-payment/paytm?");
  static const INIT_PAYMENT_CASHFREE =
      const AppUrl._internal("payments/init-payment/cashfree?");
  static const INIT_PAYMENT_RAZORPAY =
      const AppUrl._internal("payments/init-payment/razorpay?");
  static const INIT_PAYMENT_STRIPE =
      const AppUrl._internal("payments/init-payment/stripe?");
  static const INIT_PAYMENT_TSEVO =
      const AppUrl._internal("payments/init-payment/tsevo?");
  static const SUCCESS_PAY =
      const AppUrl._internal("payments/process-payment-response?");
  static const STRIPE_PAYMENT_STATUS = const AppUrl._internal(
      "payments/stripe-payment-success?type_id=13&source=STRIPE");

  static const TSEVO_PAYMENT_STATUS = const AppUrl._internal(
      "payments/stripe-payment-success?type_id=12&source=TSEVO");

  static const PAYMENT_SUCCESS =
      const AppUrl._internal("assets/payments/payment-response.html");
  static const TSEVO_PAYMENT_SUCCESS = const AppUrl._internal("success");
  static const COOKIE_PAGE =
      const AppUrl._internal("assets/payments/cookie.html");

  static const CHARGEBACK_DATA = const AppUrl._internal("chargeback/get-data");
  static const CHARGEBACK_REQUEST =
      const AppUrl._internal("chargeback/request-chargeback");
  static const CHARGEBACK_HISTORY =
      const AppUrl._internal("chargeback/history");
  static const CANCEL_CHARGEBACK = const AppUrl._internal("chargeback/cancel/");
  static const SUBMIT_SSN = const AppUrl._internal("withdraw/add-ssn");
  static const TOTAL_WITHDRAW =
      const AppUrl._internal("withdraw/total-withdraw");
  static const TSEVO_WITHDRAW_STATUS =
      const AppUrl._internal("withdraw/tsevo-withdraw-status");
  static const GET_GEOLOCATION = const AppUrl._internal(
      "https://www.googleapis.com/geolocation/v1/geolocate?key=");
  static const GET_STAGE_REPLAY_DATA = const AppUrl._internal(
      "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/moves/");
  static const GET_PROD_REPLAY_DATA = const AppUrl._internal(
      "https://khbwjhebfchbwjqhdjehbd67878dewdcwec-gamereplay.solitairegold.in/moves/");

  @override
  String toString() {
    return _value;
  }
}
