import 'dart:async';

import 'package:flash/flash.dart';

class EventManager {
  EventManager._internal();
  factory EventManager() => eventManager;
  static final EventManager eventManager = EventManager._internal();
  Map<EventName, StreamController<dynamic>> events = Map();
  StreamSubscription<dynamic> nativeToFlutterStreamSubscription;
  FlashController<dynamic> pushNotifflashController;

  void publish(EventName eventName, {dynamic data}) {
    if (events[eventName] != null) {
      events[eventName].sink.add(data ?? null);
    }
  }

  StreamSubscription<dynamic> subscribe(
      EventName eventName, Function callback) {
    if (events[eventName] == null) {
      events[eventName] = StreamController.broadcast();
    }
    final subscription = events[eventName].stream.listen(callback);
    return subscription;
  }

  void pauseNativeToFlutterStreamSubscriptionEvent() {
    if (nativeToFlutterStreamSubscription != null) {
      nativeToFlutterStreamSubscription.pause();
    }
  }

  void resumeNativeToFlutterStreamSubscriptionEvent() {
    if (nativeToFlutterStreamSubscription != null) {
      nativeToFlutterStreamSubscription.resume();
    }
  }

  void cancleNativeToFlutterStreamSubscriptionEvent() {
    if (nativeToFlutterStreamSubscription != null) {
      nativeToFlutterStreamSubscription.cancel();
    }
  }

    closePushNotifFlash() {
    if (EventManager().pushNotifflashController != null){
           EventManager().pushNotifflashController.dismiss();
    }
    EventManager().pushNotifflashController = null;
  }
}

enum EventName {
  WEBENGAGE_EVENT_FIRED,
  ANALYTICS_EVENT,
  CLOSING_PERIPHERALS,
  MYMATCH_ADDED,
  LEADERBOARD_LOADED,
  UNITY_MESSAGE,
  VERIFY_OTP_ERROR,
  WEBSOCKET_CONNECTION,
  SOLITAIRE_LOADING,
}
