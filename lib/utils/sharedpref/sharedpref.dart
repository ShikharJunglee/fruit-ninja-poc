import 'dart:async';
import 'package:solitaire_gold/utils/sharedpref/sharedprefkeys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefHelper {
  static String wsCookie;

  SharedPrefHelper.internal();
  factory SharedPrefHelper() => _instance;
  static SharedPrefHelper _instance = SharedPrefHelper.internal();

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  saveCookieToStorage(String cookie) async {
    return this.saveToSharedPref(SharedPrefKey.cookie, cookie);
  }

  saveWSCookieToStorage(String cookie) async {
    wsCookie = cookie;
    final pref = await _prefs;
    pref.setString(SharedPrefKey.websocketKey, cookie);
  }

  getCookie() async {
    return this.getFromSharedPref(SharedPrefKey.cookie);
  }

  Future<bool> removeCookie() async {
    return this.removeFromSharedPref(SharedPrefKey.cookie);
  }

  Future<bool> saveToSharedPref(String key, String value) async {
    final pref = await _prefs;
    return pref.setString(key, value);
  }

  getFromSharedPref(String key) async {
    final pref = await _prefs;
    return pref.get(key);
  }

  Future<bool> removeFromSharedPref(String key) async {
    final pref = await _prefs;
    return pref.remove(key);
  }
}
