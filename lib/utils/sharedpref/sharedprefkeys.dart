class SharedPrefKey {
  SharedPrefKey.internal();
  factory SharedPrefKey() => _instance;
  static SharedPrefKey _instance = new SharedPrefKey.internal();

  static String cookie = "gr_cookie";
  static String analyticsCookie = "analytics_cookie";
  static String websocketKey = "ws_fantasy_cookie";
  static String isRegisteredUser = "registered_user";

  static String leagueVisited = "league_visited";
  static String requestedLocation = "requested_location";

  static String geoloc = "geoloc";
  static String accountUpdateTimestamp = "account_update_ts";

  static String interactiveDemo = "interactive_demo";
}
