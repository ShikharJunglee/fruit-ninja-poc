import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';

class HttpManager extends http.BaseClient {
  static String cookie;
  static String channelId;
  static String appVersion;
  final http.Client _inner;

  HttpManager(this._inner);

  Future<http.StreamedResponse> send(http.BaseRequest request) {
    request.headers["cookie"] = cookie;
    request.headers['channelId'] = channelId;
    request.headers['appVersion'] = appVersion;
    request.headers['Content-type'] = 'application/json';

    return _inner.send(request);
  }

  Future<http.Response> sendRequest(http.BaseRequest request) async {
    if (cookie == null || cookie == "") {
      Future<dynamic> futureCookie = SharedPrefHelper.internal().getCookie();
      await futureCookie.then((value) {
        cookie = value;
      });
    }

    return await send(request).then((onValue) {
      var newCookie = onValue.headers["set-cookie"];
      if (newCookie != null && newCookie != "") {
        cookie = newCookie;
        SharedPrefHelper.internal().saveCookieToStorage(cookie);
      }
      return http.Response.fromStream(onValue);
    });
  }

  Future<bool> isInternetConnected() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      // connected
      return true;
    }

    return false;
  }
}
