import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';

class PaytmNativePayment {
  AddCashAPI api = AddCashAPI();

  Map<String, dynamic> getPaymentPayload({
    String promoCode,
    @required int amount,
    bool isFirstDeposit = false,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
  }) {
    return {
      "channelId": HttpManager.channelId,
      "orderId": null,
      "promoCode": promoCode,
      "depositAmount": amount,
      "paymentOption": paymentModeDetails["paymentOption"],
      "paymentType": paymentModeDetails["paymentType"],
      "gateway": paymentModeDetails["gateway"],
      "gatewayName": paymentModeDetails["gateway"],
      "gatewayId": paymentModeDetails["gatewayId"],
      "accessToken": paymentModeDetails["accessToken"],
      "requestType": paymentModeDetails["requestType"],
      "modeOptionId": paymentModeDetails["modeOptionId"],
      "bankCode": paymentModeDetails["processorBankCode"],
      "detailRequired": paymentModeDetails["detailRequired"],
      "processorBankCode": paymentModeDetails["processorBankCode"],
      "cvv": paymentModeDetails["cvv"],
      "label": paymentModeDetails["label"],
      "expireYear": paymentModeDetails["expireYear"],
      "expireMonth": paymentModeDetails["expireMonth"],
      "nameOnTheCard": paymentModeDetails["nameOnTheCard"],
      "saveCardDetails": paymentModeDetails["saveCardDetails"],
      "email": userDetails["email"],
      "phone": userDetails["phone"],
      "last_name": userDetails["lastName"],
      "first_name": userDetails["firstName"],
      "updateName": userDetails["updateName"],
      "updateEmail": userDetails["updateEmail"],
      "updateMobile": userDetails["updateMobile"],
      "isFirstDeposit": isFirstDeposit,
      "native": true,
    };
  }

  openPaytmNativePayment(
    BuildContext context, {
    @required int amount,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
    @required Function(Map<String, dynamic>, Map<String, dynamic>) onComplete,
    bool isFirstDeposit = false,
    String promoCode,
  }) async {
    Map<String, dynamic> payload = {};
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    Loader().showLoader(true);

    var response = await api.initPaytmNativePayment(
      context,
      payload: getPaymentPayload(
        amount: amount,
        promoCode: promoCode,
        userDetails: userDetails,
        isFirstDeposit: isFirstDeposit,
        paymentModeDetails: paymentModeDetails,
      ),
    );

    Loader().showLoader(false);

    payload = {
      "name": config.appName,
      "email": userDetails["email"],
      "phone": userDetails["phone"],
      "amount": (amount).toString(),
      "orderid": response["orderid"],
      "mid": response["mid"],
      "txyToken": response["txyToken"],
      "callbackurl": response["callbackurl"],
      "customerID": response["customerID"],
      "checkSumHash": response["checkSumHash"],
      "website": response["website"],
      "industrialTypeID": response["industrialTypeID"],
      "method":
          (paymentModeDetails["paymentType"] as String).indexOf("CARD") == -1
              ? paymentModeDetails["paymentType"].toLowerCase()
              : "card",
      "image":
          "https://d2cbroser6kssl.cloudfront.net/images/howzat/logo/howzat-logo-red-bg-v1.png",
    };

    Map<dynamic, dynamic> paymentResponseFromPaytm =
        await _openPaytmNativePayment(context, payload);
    if (paymentResponseFromPaytm["error"]) {
      onComplete(payload, paymentResponseFromPaytm);
      addWebEngagePaytmNativeDropOffEvent(
          amount.toString(), isFirstDeposit, "");
    } else {
      Loader().showLoader(true);
      var paymentResponseFromHowzat = await processPaytmNativePaymentResponse(
          context, json.decode(paymentResponseFromPaytm["data"]));
      onComplete(payload, paymentResponseFromHowzat);
      Loader().showLoader(false);
    }
  }

  addWebEngagePaytmNativeDropOffEvent(
      String amount, bool isFirstDeposit, String errorMessage) {
    Map<String, dynamic> data = new Map<String, dynamic>();
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = isFirstDeposit;
    data["pageSource"] = "paytm_page";
    data["userSelectedAmount"] = amount.toString();
    data["errorMessage"] = errorMessage;
    MyWebEngageService().webengageAddPaymentDropOffEvent(data);
  }

  Future<Map<dynamic, dynamic>> _openPaytmNativePayment(
      BuildContext context, Map<String, dynamic> payload) async {
    Map<String, dynamic> dataToSend = new Map();
    String failedMessage =
        "Payment cancelled please retry transaction. In case your money has been deducted, please contact support team!";
    try {
      Map<dynamic, dynamic> paymentResult = await ChannelManager.PAYTMNATIVE
          .invokeMethod('openPayTmNativePayment', payload);
      if (paymentResult["status"] == "success") {
        dataToSend["error"] = false;
        dataToSend["data"] = paymentResult["data"];
        return dataToSend;
      } else {
        dataToSend["error"] = true;
        dataToSend["message"] = failedMessage;
        return dataToSend;
      }
    } catch (e) {
      dataToSend["error"] = true;
      dataToSend["message"] = failedMessage;
      return dataToSend;
    }
  }

  Future<bool> isPaytmAppInstalled(BuildContext context) async {
    bool isPaytmAppInstalled = false;
    try {
      String result =
          await ChannelManager.PAYTMNATIVE.invokeMethod('isPayTmAppInstalled');
      if (result == "true") {
        isPaytmAppInstalled = true;
      }
      return isPaytmAppInstalled;
    } catch (e) {
      return isPaytmAppInstalled;
    }
  }

  Future<Map<String, dynamic>> processPaytmNativePaymentResponse(
      BuildContext context, Map<dynamic, dynamic> payload) async {
    Loader().showLoader(true, immediate: true);

    var response = await api.checkForPaymetStatus(context,
        payload: payload, payMode: "paytm");

    Loader().showLoader(false);

    return response;
  }
}
