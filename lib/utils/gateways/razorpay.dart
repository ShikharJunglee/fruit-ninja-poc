import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/screens/lobby/addcash/razorpaypaymentui.dart';
import 'package:solitaire_gold/services/myrazorpaycustomservice.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';

import '../appconfig.dart';
import '../httpmanager.dart';
import '../loader.dart';

class Razorpay {
  Map<String, dynamic> razorpayPayload;
  Map<String, dynamic> paymentPayload;

  Map<String, dynamic> getPaymentPayload({
    String promoCode,
    @required int amount,
    bool isFirstDeposit = false,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
  }) {
    return {
      "channelId": HttpManager.channelId,
      "orderId": null,
      "promoCode": promoCode,
      "depositAmount": amount,
      "paymentOption": paymentModeDetails["paymentOption"],
      "paymentType": paymentModeDetails["paymentType"],
      "gateway": paymentModeDetails["gateway"],
      "gatewayName": paymentModeDetails["gateway"],
      "gatewayId": paymentModeDetails["gatewayId"],
      "accessToken": paymentModeDetails["accessToken"],
      "requestType": paymentModeDetails["requestType"],
      "modeOptionId": paymentModeDetails["modeOptionId"],
      "bankCode": paymentModeDetails["processorBankCode"],
      "detailRequired": paymentModeDetails["detailRequired"],
      "processorBankCode": paymentModeDetails["processorBankCode"],
      "cvv": paymentModeDetails["cvv"],
      "label": paymentModeDetails["label"],
      "expireYear": paymentModeDetails["expireYear"],
      "expireMonth": paymentModeDetails["expireMonth"],
      "nameOnTheCard": paymentModeDetails["nameOnTheCard"],
      "saveCardDetails": paymentModeDetails["saveCardDetails"],
      "email": userDetails["email"],
      "phone": userDetails["phone"],
      "last_name": userDetails["lastName"],
      "first_name": userDetails["firstName"],
      "updateName": userDetails["updateName"],
      "updateEmail": userDetails["updateEmail"],
      "updateMobile": userDetails["updateMobile"],
      "isFirstDeposit": isFirstDeposit,
      "native": true,
    };
  }

  openRazorpay(
    BuildContext context, {
    @required int amount,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
    @required Function(Map<String, dynamic>, Map<String, dynamic>) onComplete,
    bool isFirstDeposit = false,
    String promoCode,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    razorpayPayload = new Map();
    paymentPayload = getPaymentPayload(
      amount: amount,
      promoCode: promoCode,
      userDetails: userDetails,
      isFirstDeposit: isFirstDeposit,
      paymentModeDetails: paymentModeDetails,
    );
    String paymentMethod =
        (paymentModeDetails["paymentType"] as String).indexOf("CARD") == -1
            ? paymentModeDetails["paymentType"].toLowerCase()
            : "card";
    bool openCustomeRazorpay = true;
    razorpayPayload["name"] = config.appName;
    razorpayPayload["amount"] = (amount * 100).toString();
    razorpayPayload["image"] =
        "https://d2cbroser6kssl.cloudfront.net/images/howzat/logo/howzat-logo-red-bg-v1.png";
    razorpayPayload["method"] = paymentMethod;
    /*Init the Razorapay payment to get ordet ID and to get razorpayRegId */
    Loader().showLoader(true, immediate: true);
    var responseFromInit = await AddCashAPI()
        .initRazorpayPayment(context, payload: paymentPayload);

    if (responseFromInit != null &&
        responseFromInit["action"] != null &&
        responseFromInit["action"]["value"] != null) {
      razorpayPayload["orderId"] = responseFromInit["action"]["value"];
      razorpayPayload["razorpayRegId"] = responseFromInit["razorpayApiKey"];
      razorpayPayload["email"] = userDetails["email"] != null
          ? userDetails["email"].toString()
          : responseFromInit["data"]["hCodedEmail"].toString();
      razorpayPayload["phone"] = userDetails["phone"] != null
          ? userDetails["phone"].toString()
          : responseFromInit["data"]["hCodedPhone"].toString();
      razorpayPayload["razorayUPIIntentEnabledVersion"] =
          responseFromInit["razorayUPIIntentEnabledVersion"];
      bool bPaymetDataSet = false;
      if (Platform.isAndroid) {
        bPaymetDataSet = await setTheSelectedPaymentModeData(
            context, paymentModeDetails, paymentMethod, openCustomeRazorpay);
      } else {
        bPaymetDataSet = true;
      }
      if (bPaymetDataSet) {
        Map<dynamic, dynamic> paymentResponseFromRazorpay =
            await MyRazorpayCustomeService()
                .openRazorpayNative(context, razorpayPayload);
        if (paymentResponseFromRazorpay["error"]) {
          onComplete(razorpayPayload, paymentResponseFromRazorpay);
          addWebEngageRazorpayDropOffEvent(amount.toString(), isFirstDeposit,
              paymentResponseFromRazorpay["message"]);
        } else {
          Loader().showLoader(true);
          var paymentResponseFromHowzat = await processRazorpayResponse(
              context, paymentResponseFromRazorpay["data"]);
          onComplete(razorpayPayload, paymentResponseFromHowzat);
          Loader().showLoader(false);
        }
      } else {
        razorpayPaymentCancledOrFailed(onComplete);
      }
    } else {
      razorpayPaymentCancledOrFailed(onComplete);
    }
    Loader().showLoader(false);
  }

  razorpayPaymentCancledOrFailed(Function onComplete) {
    /*If the Order ID is null,cancel the payment */
    Map<String, dynamic> dataToSend = new Map();
    String failedMessage =
        "Payment cancelled please retry transaction. In case your money has been deducted, please contact support team!";
    dataToSend["error"] = true;
    dataToSend["message"] = failedMessage;
    onComplete(razorpayPayload, dataToSend);
  }

  Future<bool> setTheSelectedPaymentModeData(
      BuildContext context,
      Map<String, dynamic> paymentModeDetails,
      String paymentMethod,
      bool openCustomeRazorpay) async {
    if (paymentMethod == "upi") {
      bool bPaymetDataSet = await updateUPIPaymentPayload(context);
      return bPaymetDataSet;
    } else if (paymentMethod == "card") {
      if (paymentModeDetails["detailRequired"] == true) {
        razorpayPayload["tp_cardNumber"] = paymentModeDetails["cardNumber"];
        razorpayPayload["tp_nameOnTheCard"] =
            paymentModeDetails["cardHolderName"];
        razorpayPayload["tp_expireMonth"] =
            paymentModeDetails["expiry"].split("/")[0];
        razorpayPayload["tp_expireYear"] =
            "20" + paymentModeDetails["expiry"].split("/")[1];
        razorpayPayload["tp_cvv"] = paymentModeDetails["cvv"];
        razorpayPayload["cardDataCapturingRequired"] = true;
        return true;
      } else {
        bool bPaymetDataSet = await updateCardPaymentPayload(context);
        return bPaymetDataSet;
      }
    } else if (paymentMethod == "netbanking") {
      bool bPaymetDataSet = await updateNetBankingPayload(context);
      return bPaymetDataSet;
    } else if (paymentMethod == "wallet") {
      bool bPaymetDataSet = await updateMobileWalletPaymentPayload(context);
      return bPaymetDataSet;
    } else {
      return false;
    }
  }

  Future<bool> updateMobileWalletPaymentPayload(BuildContext context) async {
    Map<String, dynamic> getPaymentMethodPayload = {};
    getPaymentMethodPayload["paymentMethod"] = "wallet";
    getPaymentMethodPayload["razorpayRegId"] = razorpayPayload["razorpayRegId"];
    Loader().showLoader(true);
    Map<dynamic, dynamic> razorpayPaymentCodesMap =
        await MyRazorpayCustomeService()
            .getPaymentMethodData(getPaymentMethodPayload);
    Loader().showLoader(false);
    if (razorpayPaymentCodesMap != null) {
      Map<String, dynamic> razorpayPaymentModeSelectData =
          await openRazorpayPaymentSelectBottomSheet(
              "wallet", razorpayPaymentCodesMap, context);
      if (razorpayPaymentModeSelectData != null &&
          razorpayPaymentModeSelectData["statusCode"] != null) {
        if (razorpayPaymentModeSelectData["statusCode"] == 1) {
          razorpayPayload["wallet"] =
              razorpayPaymentModeSelectData["selectedWallet"];
        } else if (razorpayPaymentModeSelectData["statusCode"] == 2) {}
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  addWebEngageRazorpayDropOffEvent(
      String amount, bool isFirstDeposit, String errorMessage) {
    /* Analytics data */
    Map<String, dynamic> data = new Map<String, dynamic>();
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = isFirstDeposit;
    data["pageSource"] = "razorpay_page";
    data["userSelectedAmount"] = amount.toString();
    data["errorMessage"] = errorMessage;
    MyWebEngageService().webengageAddPaymentDropOffEvent(data);
  }

  Future<bool> updateUPIPaymentPayload(BuildContext context) async {
    String userSelectedUPIPaymentPkg = "";
    String upiPaymentFlow = "";
    String upiID = "";
    Map<String, dynamic> getPaymentMethodPayload = {};
    getPaymentMethodPayload["paymentMethod"] = "upi";
    getPaymentMethodPayload["razorpayRegId"] = razorpayPayload["razorpayRegId"];
    getPaymentMethodPayload["razorayUPIIntentEnabledVersion"] =
        razorpayPayload["razorayUPIIntentEnabledVersion"];
    Loader().showLoader(true);
    Map<dynamic, dynamic> razorpayPaymentCodesMap =
        await MyRazorpayCustomeService()
            .getPaymentMethodData(getPaymentMethodPayload);
    Loader().showLoader(false);
    if (razorpayPaymentCodesMap != null) {
      Map<String, dynamic> razorpayPaymentModeSelectData =
          await openRazorpayPaymentSelectBottomSheet(
              "upi", razorpayPaymentCodesMap, context);
      if (razorpayPaymentModeSelectData != null &&
          razorpayPaymentModeSelectData["statusCode"] != null) {
        if (razorpayPaymentModeSelectData["statusCode"] == 1) {
          userSelectedUPIPaymentPkg =
              razorpayPaymentModeSelectData["packagename"];
          upiPaymentFlow = "intent";
        } else if (razorpayPaymentModeSelectData["statusCode"] == 2) {
          upiID = razorpayPaymentModeSelectData["upiid"];
          upiPaymentFlow = "collect";
        }
        razorpayPayload["upiPaymentFlow"] = upiPaymentFlow;
        razorpayPayload["userSelectedUPIPaymentPkg"] =
            userSelectedUPIPaymentPkg;
        razorpayPayload["upiID"] = upiID;
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<bool> updateCardPaymentPayload(BuildContext context) async {
    Map<String, dynamic> getPaymentMethodPayload = {};
    getPaymentMethodPayload["paymentMethod"] = "card";
    getPaymentMethodPayload["razorpayRegId"] = razorpayPayload["razorpayRegId"];
    Map<dynamic, dynamic> razorpayPaymentCodesMap = new Map();
    razorpayPaymentCodesMap["razorpayRegId"] = razorpayPayload["razorpayRegId"];
    razorpayPaymentCodesMap["paymentMethod"] = "card";
    Loader().showLoader(false);
    if (razorpayPaymentCodesMap != null) {
      Map<String, dynamic> razorpayPaymentModeSelectData =
          await openRazorpayPaymentSelectBottomSheet(
              "card", razorpayPaymentCodesMap, context);
      if (razorpayPaymentModeSelectData != null &&
          razorpayPaymentModeSelectData["statusCode"] != null) {
        if (razorpayPaymentModeSelectData["statusCode"] == 1) {
        } else if (razorpayPaymentModeSelectData["statusCode"] == 2) {
          razorpayPayload["tp_cardNumber"] =
              razorpayPaymentModeSelectData["cardNumber"];
          razorpayPayload["tp_nameOnTheCard"] =
              razorpayPaymentModeSelectData["cardHolderName"];
          razorpayPayload["tp_expireMonth"] =
              razorpayPaymentModeSelectData["expiry"].split("/")[0];
          razorpayPayload["tp_expireYear"] =
              "20" + razorpayPaymentModeSelectData["expiry"].split("/")[1];
          razorpayPayload["tp_cvv"] = razorpayPaymentModeSelectData["cvv"];
          razorpayPayload["cardDataCapturingRequired"] = true;
        }
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<bool> updateNetBankingPayload(BuildContext context) async {
    Map<String, dynamic> getPaymentMethodPayload = {};
    getPaymentMethodPayload["paymentMethod"] = "netbanking";
    getPaymentMethodPayload["razorpayRegId"] = razorpayPayload["razorpayRegId"];
    Loader().showLoader(true);
    Map<dynamic, dynamic> razorpayPaymentCodesMap =
        await MyRazorpayCustomeService()
            .getPaymentMethodData(getPaymentMethodPayload);
    Loader().showLoader(false);
    if (razorpayPaymentCodesMap != null) {
      Map<String, dynamic> razorpayPaymentModeSelectData =
          await openRazorpayPaymentSelectBottomSheet(
              "netbanking", razorpayPaymentCodesMap, context);
      if (razorpayPaymentModeSelectData != null &&
          razorpayPaymentModeSelectData["statusCode"] != null) {
        if (razorpayPaymentModeSelectData["statusCode"] == 1) {
          razorpayPayload["bank"] =
              razorpayPaymentModeSelectData["selectedNetbankingBank"];
        } else if (razorpayPaymentModeSelectData["statusCode"] == 2) {
          razorpayPayload["bank"] =
              razorpayPaymentModeSelectData["selectedNetbankingBank"];
        }
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<Map<String, dynamic>> openRazorpayPaymentSelectBottomSheet(
      String paymentType,
      Map<dynamic, dynamic> razorpayPaymentCodesMap,
      BuildContext context) async {
    /*This will open popup to select payment banks,card details and to choose UPI apps */
    Map<String, dynamic> responseFromPage = await showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        backgroundColor: Colors.white,
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  RazorpayPaymentUI(
                      paymentType: paymentType,
                      razorpayPaymentCodesMap: razorpayPaymentCodesMap,
                      razorpayRegId: razorpayPayload["razorpayRegId"],
                      paymentPayload: paymentPayload,
                      onBackPressed: (response) {}),
                  SizedBox(
                    height: 2.0,
                  ),
                  Padding(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      child: SizedBox(height: 1)),
                ],
              ),
            ));

    if (responseFromPage == null) {
      try {
        // AnalyticsEvents analyticsEvents = new AnalyticsEvents();
        // analyticsEvents.payModeOptionsClosed(
        //     expanded: false,
        //     amount: paymentPayload["depositAmount"],
        //     promoCode: paymentPayload["promoCode"],
        //     firstDeposit: paymentPayload["isFirstDeposit"],
        //     paymentType: paymentType,
        //     gateWayID: int.parse(paymentPayload["gatewayId"].toString()),
        //     modeOptionId: paymentPayload["modeOptionId"]);
      } catch (e) {
        print(e);
      }
    }

    return responseFromPage;
  }

  Future<Map<String, dynamic>> processRazorpayResponse(
      BuildContext context, Map<dynamic, dynamic> payload) async {
    var response = await AddCashAPI()
        .checkForPaymetStatus(context, payload: payload, payMode: "razorpay");
    return response;
  }
}
