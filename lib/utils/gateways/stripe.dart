import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:stripe_payment/stripe_payment.dart';

class StripePay {
  AddCashAPI api = AddCashAPI();

  Map<String, dynamic> getPaymentPayload({
    String promoCode,
    @required int amount,
    bool isFirstDeposit = false,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
  }) {
    return {
      "channelId": HttpManager.channelId,
      "orderId": null,
      "promoCode": promoCode,
      "depositAmount": amount,
      "paymentOption": paymentModeDetails["paymentOption"],
      "paymentType": paymentModeDetails["paymentType"],
      "gateway": paymentModeDetails["gateway"],
      "gatewayName": paymentModeDetails["gateway"],
      "gatewayId": paymentModeDetails["gatewayId"],
      "accessToken": paymentModeDetails["accessToken"],
      "requestType": paymentModeDetails["requestType"],
      "modeOptionId": paymentModeDetails["modeOptionId"],
      "bankCode": paymentModeDetails["processorBankCode"],
      "detailRequired": paymentModeDetails["detailRequired"],
      "processorBankCode": paymentModeDetails["processorBankCode"],
      "cvv": paymentModeDetails["cvv"],
      "label": paymentModeDetails["label"],
      "expireYear": paymentModeDetails["expireYear"],
      "expireMonth": paymentModeDetails["expireMonth"],
      "nameOnTheCard": paymentModeDetails["nameOnTheCard"],
      "saveCardDetails": paymentModeDetails["saveCardDetails"],
      "email": userDetails["email"],
      "phone": userDetails["phone"],
      "last_name": userDetails["lastName"],
      "first_name": userDetails["firstName"],
      "updateName": userDetails["updateName"],
      "updateEmail": userDetails["updateEmail"],
      "updateMobile": userDetails["updateMobile"],
      "isFirstDeposit": isFirstDeposit,
      "native": true,
    };
  }

  openStripe(
    BuildContext context, {
    @required int amount,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
    bool isFirstDeposit = false,
    @required Function(Map<String, dynamic>) onComplete,
    String promoCode,
    Map<String, dynamic> stripeConfig,
  }) async {
    int expiryMonth =
        int.tryParse((paymentModeDetails["expiry"] as String).split("/")[0]);
    int expiryYear =
        int.tryParse((paymentModeDetails["expiry"] as String).split("/")[1]);

    final CreditCard cardDetails = CreditCard(
      number: paymentModeDetails["cardNumber"],
      expMonth: expiryMonth,
      expYear: expiryYear,
      cvc: paymentModeDetails["cvv"],
    );

    Loader().showLoader(true, immediate: true);

    var response = await api.initStripePayment(
      context,
      payload: getPaymentPayload(
        amount: amount,
        promoCode: promoCode,
        userDetails: userDetails,
        isFirstDeposit: isFirstDeposit,
        paymentModeDetails: paymentModeDetails,
      ),
    );

    StripePayment.setOptions(
      StripeOptions(
        publishableKey: stripeConfig["publishableKey"].toString(),
        merchantId: stripeConfig["merchantId"].toString(),
        androidPayMode: stripeConfig["environment"].toString(),
      ),
    );

    // StripePayment.setOptions(
    //   StripeOptions(
    //     publishableKey:
    //         "pk_test_51HBwciLZvJTk6DTSQPBegBDFz7fLGkZBcbCshBiEGFFTIST9pdPZx6sbNd7MdWqRYh9ILnE3m6sUf9cwM4WAQanI00ZfejZWPA",
    //     merchantId: "acct_1HBwciLZvJTk6DTS",
    //     androidPayMode: 'test',
    //   ),
    // );

    StripePayment.confirmPaymentIntent(
      PaymentIntent(
        paymentMethod: PaymentMethodRequest(
          card: cardDetails,
        ),
        clientSecret: response["paymentClientSecret"],
      ),
    ).then((paymentIntent) async {
      var statusResponse = await api.checkStripePaymentStatus(
        context,
        orderId: response["paymentId"],
        amount: amount,
        status: paymentIntent.status,
      );

      Loader().showLoader(false);
      onComplete(statusResponse);
    }).catchError((err) {
      Loader().showLoader(false);
      onComplete({"error": true, "message": err.message});
      addWebEngageStripeNativeDropOffEvent(
          amount.toString(), isFirstDeposit, err.message);
    });
  }

  addWebEngageStripeNativeDropOffEvent(
      String amount, bool isFirstDeposit, String errorMessage) {
    try {
      Map<String, dynamic> data = new Map<String, dynamic>();
      data["channelId"] = HttpManager.channelId;
      data["isFirstDeposit"] = isFirstDeposit;
      data["pageSource"] = "strip_page";
      data["userSelectedAmount"] = amount.toString();
      data["errorMessage"] = errorMessage != null ? errorMessage : "";
      MyWebEngageService().webengageAddPaymentDropOffEvent(data);
    } catch (e) {}
  }
}
