import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/screens/paymentmode/initpay.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class Tsevo {
  openTsevoPayment(
    BuildContext context, {
    @required int amount,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
    bool isFirstDeposit = false,
    String promoCode,
    double latitude,
    double longitude,
    double radius,
    double altitude,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    String querParamString = getQueryParams(
      promoCode: promoCode,
      amount: amount,
      userDetails: userDetails,
      isFirstDeposit: isFirstDeposit,
      paymentModeDetails: paymentModeDetails,
      latitude: latitude,
      longitude: longitude,
      radius: radius,
      altitude: altitude,
    );

    final result = await Navigator.of(context).push(
      MaterialPageRoute(
        settings: RouteSettings(name: "InitPay"),
        builder: (context) => InitPay(
          url: config.apiUrl +
              AppUrl.INIT_PAYMENT_TSEVO.toString() +
              querParamString,
        ),
      ),
    );

    if (result != null) {
      Map<String, dynamic> response = json.decode(result);
      if ((response["authStatus"] as String).toLowerCase() ==
          "Declined".toLowerCase()) {
        return {
          "error": true,
          "data": response,
        };
      } else {
        var statusResponse = await AddCashAPI().checkTsevoStatus(
          context,
          orderId: response["paymentId"],
          amount: amount,
          status: "succeeded",
        );

        return statusResponse;
      }
    }
  }

  openTsevoPayout(
    BuildContext context, {
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> withdrawRequest,
    bool isFirstDeposit = false,
    double latitude,
    double longitude,
    double radius,
    double altitude,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    String querParamString = getPayoutQueryParams(
      withdrawId: double.parse(withdrawRequest["id"].toString()).toInt(),
      latitude: latitude,
      longitude: longitude,
      radius: radius,
      altitude: altitude,
    );

    String url = config.apiUrl +
        AppUrl.WITHDRAW_TSEVO_INIT_PAYOUT.toString() +
        querParamString;

    final result = await Navigator.of(context).push(
      MaterialPageRoute(
        settings: RouteSettings(name: "Payout"),
        builder: (context) => InitPay(
          url: url,
        ),
      ),
    );

    if (result != null) {
      Map<String, dynamic> response = json.decode(result);
      if ((response["authStatus"] as String).toLowerCase() ==
              "Declined".toLowerCase() ||
          (response["authStatus"] as String).toLowerCase() ==
              "fail".toLowerCase()) {
        String message = response["error"] as String;
        if (response["error"] != null) {
          if ((response["error"] as String).toLowerCase() == "pending") {
            message =
                "Your transaction is in pending state. Please wait for some time and check.";
          } else if ((response["error"] as String).toLowerCase() ==
              "complete") {
            message = "Your transaction is already processed.";
          }
        }
        return {
          "error": true,
          "data": response,
          "message": message,
        };
      } else {
        return {
          "error": false,
          "data": response,
        };
      }
    }
  }

  String getPayoutQueryParams({
    int withdrawId,
    double latitude,
    double longitude,
    double radius,
    double altitude,
  }) {
    Map<String, dynamic> payload = {
      "channelId": HttpManager.channelId,
      "withdrawid": withdrawId,
      "latitude": latitude,
      "longitude": longitude,
      "radius": radius,
      "altitude": altitude,
    };

    bool shouldAppend = false;
    String querParamString = "";
    payload.forEach((key, value) {
      if (shouldAppend) {
        querParamString += '&';
      }
      querParamString += key + '=' + value.toString();
      shouldAppend = true;
    });

    return querParamString;
  }

  String getQueryParams({
    String promoCode,
    @required int amount,
    bool isFirstDeposit = false,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
    double latitude,
    double longitude,
    double radius,
    double altitude,
  }) {
    Map<String, dynamic> payload = {
      "channelId": HttpManager.channelId,
      "orderId": null,
      "promoCode": promoCode,
      "depositAmount": amount,
      "paymentOption": paymentModeDetails["paymentOption"],
      "paymentType": paymentModeDetails["paymentType"],
      "gateway": paymentModeDetails["gateway"],
      "gatewayName": paymentModeDetails["gateway"],
      "gatewayId": paymentModeDetails["gatewayId"],
      "accessToken": paymentModeDetails["accessToken"],
      "requestType": paymentModeDetails["requestType"],
      "modeOptionId": paymentModeDetails["modeOptionId"],
      "bankCode": paymentModeDetails["processorBankCode"],
      "detailRequired": paymentModeDetails["detailRequired"],
      "processorBankCode": paymentModeDetails["processorBankCode"],
      "cvv": paymentModeDetails["cvv"],
      "label": paymentModeDetails["label"],
      "expireYear": paymentModeDetails["expireYear"],
      "expireMonth": paymentModeDetails["expireMonth"],
      "nameOnTheCard": paymentModeDetails["nameOnTheCard"],
      "saveCardDetails": paymentModeDetails["saveCardDetails"],
      if (shouldUseEmail(paymentModeDetails, userDetails["email"]))
        "email": userDetails["email"] == null ? "@" : userDetails["email"],
      if (shouldUseMobile(paymentModeDetails, userDetails["phone"]))
        "mobile":
            userDetails["phone"] == null ? "          " : userDetails["phone"],
      "last_name": userDetails["lastName"],
      "first_name": userDetails["firstName"],
      "updateName": userDetails["updateName"],
      "updateEmail": userDetails["updateEmail"],
      "updateMobile": userDetails["updateMobile"],
      "isFirstDeposit": isFirstDeposit,
      "native": true,
      "latitude": latitude,
      "longitude": longitude,
      "radius": radius,
      "altitude": altitude,
    };

    bool shouldAppend = false;
    String querParamString = "";
    payload.forEach((key, value) {
      if (shouldAppend) {
        querParamString += '&';
      }
      querParamString += key + '=' + value.toString();
      shouldAppend = true;
    });

    return querParamString;
  }

  bool shouldUseMobile(paymentModeDetails, String mobile) {
    bool bAllow = paymentModeDetails["gatewayId"].toString() != "166" ||
        (paymentModeDetails["gatewayId"].toString() == "166" && mobile != null);
    return bAllow;
  }

  bool shouldUseEmail(paymentModeDetails, String email) {
    bool bAllow = paymentModeDetails["gatewayId"].toString() != "166" ||
        (paymentModeDetails["gatewayId"].toString() == "166" && email != null);
    return bAllow;
  }
}
