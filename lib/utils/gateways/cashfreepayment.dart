import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/screens/paymentmode/initpay.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class CashFreePayment {
  openCashFreePayment(
    BuildContext context, {
    @required int amount,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
    bool isFirstDeposit = false,
    String promoCode,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    String querParamString = getQueryParams(
      promoCode: promoCode,
      amount: amount,
      userDetails: userDetails,
      isFirstDeposit: isFirstDeposit,
      paymentModeDetails: paymentModeDetails,
    );

    final result = await Navigator.of(context).push(
      MaterialPageRoute(
        settings: RouteSettings(name: "InitPay"),
        builder: (context) => InitPay(
          url: config.apiUrl +
              AppUrl.INIT_PAYMENT_CASHFREE.toString() +
              querParamString,
        ),
      ),
    );

    if (result != null) {
      Map<String, dynamic> response = json.decode(result);
      if ((response["authStatus"] as String).toLowerCase() ==
          "Declined".toLowerCase()) {
        return {
          "error": true,
          "data": response,
        };
      } else {
        return {
          "error": false,
          "data": response,
        };
      }
    }
  }

  String getQueryParams({
    String promoCode,
    @required int amount,
    bool isFirstDeposit = false,
    @required Map<String, dynamic> userDetails,
    @required Map<String, dynamic> paymentModeDetails,
  }) {
    Map<String, dynamic> payload = {
      "channelId": HttpManager.channelId,
      "orderId": null,
      "promoCode": promoCode,
      "depositAmount": amount,
      "paymentOption": paymentModeDetails["paymentOption"],
      "paymentType": paymentModeDetails["paymentType"],
      "gateway": paymentModeDetails["gateway"],
      "gatewayName": paymentModeDetails["gateway"],
      "gatewayId": paymentModeDetails["gatewayId"],
      "accessToken": paymentModeDetails["accessToken"],
      "requestType": paymentModeDetails["requestType"],
      "modeOptionId": paymentModeDetails["modeOptionId"],
      "bankCode": paymentModeDetails["processorBankCode"],
      "detailRequired": paymentModeDetails["detailRequired"],
      "processorBankCode": paymentModeDetails["processorBankCode"],
      "cvv": paymentModeDetails["cvv"],
      "label": paymentModeDetails["label"],
      "expireYear": paymentModeDetails["expireYear"],
      "expireMonth": paymentModeDetails["expireMonth"],
      "nameOnTheCard": paymentModeDetails["nameOnTheCard"],
      "saveCardDetails": paymentModeDetails["saveCardDetails"],
      if (shouldUseEmail(paymentModeDetails, userDetails["email"]))
        "email": userDetails["email"] == null ? "@" : userDetails["email"],
      if (shouldUseMobile(paymentModeDetails, userDetails["phone"]))
        "mobile":
            userDetails["phone"] == null ? "          " : userDetails["phone"],
      "last_name": userDetails["lastName"],
      "first_name": userDetails["firstName"],
      "updateName": userDetails["updateName"],
      "updateEmail": userDetails["updateEmail"],
      "updateMobile": userDetails["updateMobile"],
      "isFirstDeposit": isFirstDeposit,
      "native": true,
    };

    bool shouldAppend = false;
    String querParamString = "";
    payload.forEach((key, value) {
      if (shouldAppend) {
        querParamString += '&';
      }
      querParamString += key + '=' + value.toString();
      shouldAppend = true;
    });

    return querParamString;
  }

  bool shouldUseMobile(paymentModeDetails, String mobile) {
    bool bAllow = paymentModeDetails["gatewayId"].toString() != "166" ||
        (paymentModeDetails["gatewayId"].toString() == "166" && mobile != null);
    return bAllow;
  }

  bool shouldUseEmail(paymentModeDetails, String email) {
    bool bAllow = paymentModeDetails["gatewayId"].toString() != "166" ||
        (paymentModeDetails["gatewayId"].toString() == "166" && email != null);
    return bAllow;
  }
}
