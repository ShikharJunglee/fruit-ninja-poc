import 'package:solitaire_gold/models/banners/banners.dart';
import 'package:solitaire_gold/models/databasemanager.dart';
import 'package:solitaire_gold/models/dataloader.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/providers/deeplinkroute.dart';
import 'package:solitaire_gold/providers/fcmDataRoute.dart';

import 'appconfig.dart';
import 'package:provider/provider.dart';

class Providers {
  List<ChangeNotifierProvider> _items;

  Providers(AppConfig config) {
    _items = [
      ChangeNotifierProvider<AppConfig>.value(
        value: config,
      ),
      ChangeNotifierProvider<User>.value(
        value: User(),
      ),
      ChangeNotifierProvider<DataLoader>.value(
        value: DataLoader(),
      ),
      ChangeNotifierProvider<DatabaseManager>.value(
        value: DatabaseManager(),
      ),
      ChangeNotifierProvider<BannerList>.value(
        value: BannerList(),
      ),
      ChangeNotifierProvider<InitData>.value(
        value: InitData(),
      ),
      ChangeNotifierProvider<DeepLinkRoute>.value(
        value: DeepLinkRoute(),
      ),
      ChangeNotifierProvider<FCMDataRoute>.value(
        value: FCMDataRoute(),
      )
    ];
  }

  List<ChangeNotifierProvider> getAll() {
    return _items;
  }
}
