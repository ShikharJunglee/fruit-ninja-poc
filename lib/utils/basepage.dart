import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class BasePage {
  showMessageOnTop(BuildContext context, {String msg}) {
    Flushbar flushbar;
    flushbar = Flushbar(
      boxShadows: [
        BoxShadow(
          blurRadius: 15.0,
          spreadRadius: 15.0,
          color: Colors.black12,
        )
      ],
      flushbarStyle: FlushbarStyle.FLOATING,
      messageText: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              msg,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.black,
                  ),
            ),
          ),
          InkWell(
            child: Icon(
              Icons.close,
              color: Colors.black,
            ),
            onTap: () {
              flushbar.dismiss(true);
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
    );
    flushbar.show(context);
  }

  showAlertOnTop(BuildContext context, {String title, String msg}) {
    return showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      builder: (BuildContext context) {
        return CustomDialog(
          padding: EdgeInsets.symmetric(horizontal: 32.0),
          dialog: Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: <Widget>[
                      if (title != null)
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  vertical: 8.0,
                                ),
                                child: Text(
                                  "$title",
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .headline5
                                      .copyWith(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              msg,
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .headline6
                                  .copyWith(
                                    color: Colors.grey.shade600,
                                    height: 1.7,
                                    fontWeight: FontWeight.w500,
                                  ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 140.0,
                        child: Button(
                          size: ButtonSize.medium,
                          text: "Ok".toUpperCase(),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
