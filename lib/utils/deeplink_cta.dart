import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/api/auth/auth.dart';
import 'package:solitaire_gold/api/kyc/kycverificationAPI.dart';
import 'package:solitaire_gold/api/raf/earncashAPI.dart';
import 'package:solitaire_gold/api/user/accountAPI.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/generic_dialog.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/chargeback/chargebackhelper.dart';
import 'package:solitaire_gold/screens/lobby/addcash/add_cash_helper.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw_helper.dart';
import 'package:solitaire_gold/services/mybranchioservice.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';

class DeeplinkCTA {
  static const String DEPOSIT = "DEPOSIT";
  static const String PROFILE = "PROFILE";
  static const String KYC = "KYC";
  static const String ACCOUNT = "ACCOUNT";
  static const String WITHDRAW = "WITHDRAW";
  static const String RAF = "REFERRAL";
  static const String HOW_TO_PLAY = "HOW_TO_PLAY";
  static const String SCORING = "SCORING";
  static const String HELP = "HELP";
  static const String LOGOUT = "LOGOUT";
  static const String CHARGE_BACK = "CHARGE_BACK";
  static const String LINK = "LINK";
}

class DeeplinkLaunch {
  DeeplinkLaunch._internal();
  factory DeeplinkLaunch() => deeplinkLaunch;
  static final DeeplinkLaunch deeplinkLaunch = DeeplinkLaunch._internal();

  AuthAPI _authAPI = AuthAPI();
  AccountAPI _accountAPI = AccountAPI();
  EarnCashAPI _earnCashAPI = EarnCashAPI();
  KYCVerificationAPI _kycVerificationAPI = KYCVerificationAPI();

  onFTUE(BuildContext context) {
    routeManager.launchFTUE(context, "account");
  }

  onScoring(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    config.staticPageConfig.forEach((conf) {
      if (conf["name"].toLowerCase() == "ScoringSystem".toLowerCase()) {
        routeManager.launchStaticPage(context,
            url: conf["data"]["url"], title: conf["data"]["title"]);
      }
    });
  }

  onAccountSummary(BuildContext context) async {
    Loader().showLoader(true, immediate: true);
    Map<String, dynamic> response =
        await _accountAPI.getAccountDetails(context);

    Loader().showLoader(false);
    routeManager.launchAccountSummary(context, accountData: response["data"]);
  }

  Future<Map<String, dynamic>> onAddCash(
    BuildContext context, {
    @required String source,
    Function onPaymentComplete,
    int prefilledAmount,
    String prefilledPromoCode,
  }) async {
    Loader().showLoader(true);
    AnalyticsManager().setJourney("Deposit");

    GamePlayAnalytics().onAddCashClicked(context, source: source);
    Map<String, dynamic> result = await AddCashHelper().authAddcash(context);

    Map<String, dynamic> paymentResponse;
    if (result["error"] == true) {
      if (result["message"] != null) {
        await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) => GenericDialog(
            letterSpacing: 2.2,
            contentText: Text(
              "${result["message"]}",
              textAlign: TextAlign.center,
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.white,
                  ),
            ),
            buttonText: "Ok".toUpperCase(),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        );
      }
    } else {
      paymentResponse = await routeManager.launchAddCash(
        context,
        data: result["data"],
        onPaymentComplete: onPaymentComplete,
        prefilledAmount: prefilledAmount,
        prefilledPromoCode: prefilledPromoCode,
        source: source,
      );
      AnalyticsManager().setJourney(null);
    }

    return paymentResponse;
  }

  onWithdraw(
    BuildContext context, {
    String source,
    Function(String msg) onError,
  }) async {
    WithdrawHelper().launchWithdraw(
      context,
      source: source,
      onErrorMessage: onError,
    );
  }

  onRAF(BuildContext context) async {
    Loader().showLoader(true, immediate: true);

    final result = await _earnCashAPI.getReferalData(context);

    if (result["error"] != true) {
      User user = Provider.of(context, listen: false);
      user.copyFrom(result["data"]["user"]);

      if (result["data"]["rafData"]["refDetails"]["screenIndex"] == 1) {
        return routeManager.launchRAF(context,
            rafData: result["data"]["rafData"]);
      } else {
        return routeManager.launchRAFV2(context,
            rafData: result["data"]["rafData"]);
      }
    }
  }

  onProfile(BuildContext context) async {
    Loader().showLoader(true, immediate: true);
    Map<String, dynamic> response =
        await _kycVerificationAPI.getUserData(context);

    User _user = Provider.of(context, listen: false);
    _user.copyFrom(response["data"]["user"]);

    Loader().showLoader(false);
    routeManager.launchProfile(context, response["data"]["url"]);
  }

  onKYC(BuildContext context) async {
    Loader().showLoader(true);
    Map<String, dynamic> response =
        await _kycVerificationAPI.getUserData(context);

    User _user = Provider.of(context, listen: false);
    _user.copyFrom(response["data"]["user"]);

    Loader().showLoader(false);
    routeManager.launchKYC(context);
  }

  onLogout(BuildContext context, {Function(String msg) onMessage}) async {
    Loader().showLoader(true, immediate: true);
    bool result = await _authAPI.logout(context);
    if (result == true) {
      routeManager.launchSigninScreen(context);
      AnalyticsManager().logout();
      Loader().showLoader(false);
      try {
        MyWebEngageService().doWebEngageLogOut();
        MyBranchIoService().doBranchIoLogOut();
      } catch (e) {}
    } else {
      onMessage("Getting error while logout.");
    }
  }

  onChargeBack(BuildContext context, {String source}) async {
    ChargebackHelper().launchChargeback(
      context,
      source: "app_drawer",
    );
  }
}
