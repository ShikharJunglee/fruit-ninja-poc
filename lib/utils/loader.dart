import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:solitaire_gold/models/dataloader.dart';

class Loader {
  Loader._internal();
  factory Loader() => loader;
  static final Loader loader = Loader._internal();

  Timer _timer;
  Timer _timerToShowLoader;

  GlobalKey<NavigatorState> navigatorkey;

  showLoader(bool bShow, {bool immediate = false}) {
    if (bShow && immediate) {
      _resetShowLoaderTimers();
      _show(true);
      return;
    }
    if (bShow) {
      _startTimerToShowLoader();
    }

    if (!bShow) {
      _show(false);
      _resetShowLoaderTimers();
    }
  }

  setGlobalContextKey(GlobalKey<NavigatorState> navigatorkey) {
    this.navigatorkey = navigatorkey;
  }

  _startTimerToShowLoader() {
    if (_timerToShowLoader != null) {
      _resetAllTimers();
    }

    _timerToShowLoader = Timer(Duration(milliseconds: 500), () {
      _show(true);
    });
  }

  _show(bool bShow) {
    DataLoader dataLoader =
        Provider.of(navigatorkey.currentContext, listen: false);

    dataLoader.setLoadingStatus(bShow);

    if (bShow) {
      _timer = Timer(Duration(seconds: 10), () {
        _resetAllTimers();
        _show(false);
      });
    } else if (_timer != null) {
      _timer.cancel();
    }
  }

  _resetShowLoaderTimers() {
    if (_timerToShowLoader != null) {
      _timerToShowLoader.cancel();
    }
    _timerToShowLoader = null;
  }

  _resetAllTimers() {
    if (_timer != null) {
      _timer.cancel();
    }
    _timer = null;
    _resetShowLoaderTimers();
  }
}
