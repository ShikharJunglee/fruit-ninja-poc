import 'package:flutter/material.dart';

class AppConfig with ChangeNotifier {
  String wsUrl;
  String apiUrl;
  String gameUrl;
  String replyURL;
  int channelId;
  String appName;
  String firebaseToken;
  String fcmSubscribeId;
  String branchInviteUrl = "";
  String branchReferralCode = "";
  String googleaddid = "";
  int otpResendTime = 0;
  int resendPasswordCount = 0;
  int maxOtpResendCount = 0;
  int maxOtpResendTime = 0;
  int emailSwitch = 1;
  List<dynamic> staticPageConfig;
  Map<dynamic, dynamic> branchReferringParams;
  bool bShowLeaderBoardOnResultPageOnStart = false;
  bool disableBranchIOAttribution = false;
  int attributionNumber = 0;
  bool disableSocialLogin = false;
  bool enableAppUpdate = true;

  void setApiUrl(String apiUrl) {
    this.apiUrl = apiUrl;
  }

  void setGameUrl(String gameUrl) {
    this.gameUrl = gameUrl;
  }

  void setAppName(String name) {
    appName = name;
  }

  void setReplyURL(String url) {
    this.replyURL = url;
  }

  void setWSUrl(String url) {
    wsUrl = url;
  }

  void setChannelId(int channelId) {
    this.channelId = channelId;
  }

  void setOtpResendTime(int value) {
    otpResendTime = value;
  }

  void setResendPasswordCount(int value) {
    resendPasswordCount = value;
  }

  void setOtpMaxResendCount(int value) {
    maxOtpResendCount = value;
  }

  void setOtpMaxResendTime(int value) {
    maxOtpResendTime = value;
  }

  void setEmailSwitch(int value) {
    emailSwitch = value;
  }

  void setFirebaseToken(String token) {
    firebaseToken = token;
  }

  void setBranchUrl(String url) {
    branchInviteUrl = url;
  }

  void setBranchReferralCode(String referralCode) {
    branchReferralCode = referralCode;
  }

  void setStaticPageConfig(List<dynamic> config) {
    this.staticPageConfig = config;
  }

  void setFCMSubscribeId(String id) {
    fcmSubscribeId = id;
  }

  void setGoogleAddId(String id) {
    googleaddid = id;
  }

  void setBranchResponse(Map<dynamic, dynamic> data) {
    this.branchReferringParams = data;
  }

  void setbShowLeaderBoardOnResultPageOnStart(bool value) {
    this.bShowLeaderBoardOnResultPageOnStart = value;
  }

  void setDisableBranchIOAttributionValue(bool value) {
    this.disableBranchIOAttribution = value;
  }

  void setAttributionNumber(int value) {
    this.attributionNumber = value;
  }

  void setDisableSocialLoginValue(bool value) {
    this.disableSocialLogin = value;
  }

  void setEnableAppUpdate(bool value) {
    this.enableAppUpdate = true;
  }
}
