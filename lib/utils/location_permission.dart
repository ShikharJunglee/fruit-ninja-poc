import 'dart:io';
import 'package:flutter/material.dart';
import 'package:permission/permission.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/popups/common/locationsettings.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedprefkeys.dart';

class LocationPermissionHandler {
  Future<PermissionStatus> checkForPermission(BuildContext context,
      {@required String source}) async {
    if (!Platform.isIOS) {
      List<Permissions> permissions =
          await Permission.getPermissionsStatus([PermissionName.Location]);

      String result = await SharedPrefHelper()
          .getFromSharedPref(SharedPrefKey.requestedLocation);

      if (result != null &&
          permissions[0].permissionStatus == PermissionStatus.notAgain) {
        openSettings(context, source);
      } else if (permissions[0].permissionStatus != PermissionStatus.allow) {
        SharedPrefHelper()
            .saveToSharedPref(SharedPrefKey.requestedLocation, "true");
        GamePlayAnalytics()
            .onGeoLocationPopupRequested(context, source: source);

        return await askForPermission();
      }
      return permissions[0].permissionStatus;
    }
    return null;
  }

  openSettings(BuildContext context, String source) async {
    await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      barrierDismissible: true,
      builder: (prizestructurecontext) {
        return LocationSettingsDialog(
          source: source,
        );
      },
    );
  }

  Future<PermissionStatus> askForPermission() async {
    final result =
        await Permission.requestPermissions([PermissionName.Location]);
    if (result != null) {
      return result[0].permissionStatus;
    }
    return null;
  }
}
