import 'package:flutter/material.dart';

class DeepLinkRoute with ChangeNotifier {
  int promoamount;
  String pageRoute;
  String promocode;
  String pageLocation;
  String pageTitle;
  String uniqueId;
  String sg_body;
  int sg_matchId;
  String sg_title;
  String sg_type;
  String gameSource;

  DeepLinkRoute(
      {this.pageRoute,
      this.promocode,
      this.promoamount,
      this.pageLocation,
      this.pageTitle,
      this.uniqueId,
      this.sg_body,
      this.sg_matchId,
      this.sg_title,
      this.sg_type});

  setData(DeepLinkRoute routeData) {
    this.pageRoute = routeData.pageRoute;
    this.promocode = routeData.promocode;
    this.pageLocation = routeData.pageLocation;
    this.pageTitle = routeData.pageTitle;
    this.uniqueId = routeData.uniqueId;
    this.promoamount = int.tryParse(routeData.promoamount.toString());
    this.sg_body = routeData.sg_body;
    this.sg_matchId = int.tryParse(routeData.sg_matchId.toString());
    this.sg_title = routeData.sg_title;
    this.sg_type = routeData.sg_type;
    notifyListeners();
  }

  setToDefault() {
    this.pageRoute = " ";
  }

  factory DeepLinkRoute.fromJson(Map<dynamic, dynamic> json) {
    return DeepLinkRoute(
      pageRoute: json["dl_page_route"],
      promocode: json["dl_ac_promocode"],
      promoamount: json["dl_ac_promoamount"] == null ||
              json["dl_ac_promoamount"].trim() == ""
          ? null
          : int.tryParse(json["dl_ac_promoamount"]),
      sg_matchId: json["sg_matchId"] == null || json["sg_matchId"].trim() == ""
          ? null
          : int.parse(json["sg_matchId"]),
      pageLocation: json["dl_sp_pageLocation"],
      pageTitle: json["dl_sp_pageTitle"],
      uniqueId: json["dl_unique_id"],
      sg_title: json["sg_title"],
      sg_type: json["sg_type"],
      sg_body: json["sg_body"],
    );
  }
}
