import 'package:flutter/material.dart';

class FCMDataRoute with ChangeNotifier {
  String sg_body;
  int sg_matchId;
  String sg_title;
  String sg_type;
  String gameSource;


  FCMDataRoute(
      {
      this.sg_body,
      this.sg_matchId,
      this.sg_title,
      this.sg_type});

  setData(FCMDataRoute routeData) {
    this.sg_body = routeData.sg_body;
    this.sg_matchId = int.tryParse(routeData.sg_matchId.toString());
    this.sg_title = routeData.sg_title;
    this.sg_type = routeData.sg_type;
    notifyListeners();
  }
  
  setToDefault() {
    this.sg_matchId = null;
  }

  factory FCMDataRoute.fromJson(Map<dynamic, dynamic> json) {
    return FCMDataRoute(
      sg_matchId: json["matchId"] == null || json["matchId"].trim() == ""
          ? null
          : int.parse(json["matchId"]),
      sg_title: json["title"],
      sg_type: json["type"],
      sg_body: json["body"],
    );
  }
}
