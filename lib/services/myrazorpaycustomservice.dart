import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';

class MyRazorpayCustomeService {
  Future<Map<dynamic, dynamic>> getPaymentMethodData(
      Map<String, dynamic> getPaymentMethodPayload) async {
    /*This channel call will init the payment methods information from Razorpay like 
    supported payment methods,netbanking codes ,supported wallets */
    try {
      final value = await ChannelManager.RAZORPAY
          .invokeMethod('getPaymentMethodData', getPaymentMethodPayload);
      return value;
    } catch (e) {
      print("Error is .......");
      print(e);
      return Future.value(null);
    }
  }

  Future<Map<String, dynamic>> openRazorpayNative(
      BuildContext context, Map<String, dynamic> payload) async {
    String failedMessage =
        "Payment cancelled please retry transaction. In case your money has been deducted, please contact support team!";
    Map<String, dynamic> dataToSend = new Map();
    try {
      Map<dynamic, dynamic> paymentResult = await ChannelManager.RAZORPAY
          .invokeMethod('_openRazorpayNative', payload);
      if (paymentResult["status"] == "success") {
        dataToSend["error"] = false;
        Map<dynamic, dynamic> data = new Map();
        data = paymentResult["data"];
        dataToSend["data"] = data;
        return dataToSend;
      } else {
        dataToSend["error"] = true;
        dataToSend["message"] = failedMessage;
        return dataToSend;
      }
    } catch (e) {
      dataToSend["error"] = true;
      dataToSend["message"] = failedMessage;
      return dataToSend;
    }
  }

  Future<bool> bIsValidVPA(String vpa, String razorpayRegId) async {
    /*This channel call will init the payment methods information from Razorpay like 
    supported payment methods,netbanking codes ,supported wallets */
    Map<String, dynamic> arguments = new Map();
    arguments["razorpayRegId"] = razorpayRegId;
    arguments["vpa"] = vpa;
    try {
      final value =
          await ChannelManager.RAZORPAY.invokeMethod('isValidVpa', arguments);
      return value;
    } catch (e) {
      print("Error is .......");
      print(e);
      return Future.value(false);
    }
  }
}
