import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/utils/MyConstants.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class MyHelperClass {
  static bool isEmailValid(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  static getScreenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static getScreenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static String getMobileCountryCode() {
    if (HttpManager.channelId.toString() == "201" ||
        HttpManager.channelId.toString() == "202") {
      return MyConstants.TELE_COUNTRY_CODE_USA;
    } else {
      return MyConstants.TELE_COUNTRY_CODE_INDIA;
    }
  }

  static void copyTextToClipBoard(String text) {
    Clipboard.setData(
      ClipboardData(text: text),
    );
  }

  static String getReadableDateFromTimeStamp(String timeStamp) {
    String convertedDate = "";
    try {
      if (timeStamp.length > 0) {
        var iTimeStamp = int.tryParse(timeStamp);
        DateTime date = DateTime.fromMillisecondsSinceEpoch(
            iTimeStamp == null ? 0 : iTimeStamp);
        convertedDate = date.day.toString() +
            "-" +
            date.month.toString() +
            "-" +
            date.year.toString();
      }
    } catch (e) {}
    return convertedDate;
  }

  static double checkDouble(dynamic value) {
    if (value is String) {
      return double.parse(value);
    } else {
      return value;
    }
  }

  Future<void> showDialogWithTitleAndMessage(
      BuildContext context, String title, String message) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: <Widget>[],
        );
      },
    );
  }

  static Future<bool> isTablet(BuildContext context) async {
    if (Platform.isIOS) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      return iosInfo.model.toLowerCase() == "ipad";
    } else {
      var shortestSide = MediaQuery.of(context).size.shortestSide;
      return shortestSide > 600;
    }
  }

  static Future<bool> isIpad() async {
    if (Platform.isIOS) {
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      return iosInfo.model.toLowerCase() == "ipad";
    }
    return Future.value(false);
  }

  static showAlertDialog(BuildContext context, String text, String title) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {},
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(text),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
