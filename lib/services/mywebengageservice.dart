import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/services/myhelperclass.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class MyWebEngageService {
  MyWebEngageService._internal();
  static final MyWebEngageService _webEngage = MyWebEngageService._internal();
  factory MyWebEngageService() => _webEngage;

  static Future<String> doWebEngageLogin(
      BuildContext context, String userId) async {
    /*This method will login to Web Engage */
    String channelResponse = "";
    try {
      channelResponse =
          await ChannelManager.WEBENGAGE.invokeMethod('webengageTrackUser', {
        "trackingType": "login",
        "value": userId,
      });
    } catch (e) {
      print(e);
    }
    return channelResponse;
  }

  Future<String> doWebEngageLogOut() async {
    /*This method will logout to Web Engage */
    String channelResponse = "";
    try {
      channelResponse =
          await ChannelManager.WEBENGAGE.invokeMethod('webengageTrackUser', {
        "trackingType": "logout",
        "value": "",
      });
    } catch (e) {
      print(e);
    }
    return channelResponse;
  }

  Future<String> webEngageEventLogin(BuildContext context,
      {Map<String, dynamic> data,
      String loginType,
      String phone,
      String email}) async {
    String channelResponse = "";
    try {
      Map<String, dynamic> modifiedData = new Map();
      Map<dynamic, dynamic> dataToSend = new Map();
      data.remove("email");
      data.remove("mobile");
      if (phone != null && phone.length > 5) {
        dataToSend["mobile"] =
            MyHelperClass.getMobileCountryCode() + phone.toString();
      } else {
        dataToSend["mobile"] = " ";
      }
      dataToSend["email"] = email;
      dataToSend["chosenloginTypeByUser"] = loginType.toString();
      dataToSend["registrationID"] = data["id"].toString();
      dataToSend["transactionID"] = data["id"].toString();
      dataToSend["description"] =
          "CHANNEL_" + HttpManager.channelId.toString() + "_LOGIN";
      dataToSend["channelId"] = HttpManager.channelId.toString();
      data.forEach((key, value) {
        if (key != null && value != null) {
          modifiedData[key] = value.toString();
        }
      });
      dataToSend["data"] = modifiedData;
      channelResponse = await ChannelManager.WEBENGAGE
          .invokeMethod('webEngageEventLogin', dataToSend);
      return channelResponse;
    } catch (e) {
      print("Error in WebEngage Signup Event....");
      print(e);
      return channelResponse;
    }
  }

  Future<String> webEngageEventSignUp(BuildContext context,
      {Map<String, dynamic> signupdata,
      String loginType,
      String email,
      String phone}) async {
    String channelResponse = "";
    try {
      Map<dynamic, dynamic> dataToSend = new Map();
      Map<String, dynamic> modifiedData = new Map();
      dataToSend["registrationID"] = signupdata["id"].toString();
      dataToSend["transactionID"] = signupdata["id"].toString();
      dataToSend["description"] =
          "CHANNEL" + signupdata["channelId"].toString() + "SIGNUP";
      if (phone != null && phone.length > 5) {
        dataToSend["mobile"] = MyHelperClass.getMobileCountryCode() + phone;
      } else {
        dataToSend["mobile"] = " ";
      }
      dataToSend["email"] = email;
      dataToSend["chosenloginTypeByUser"] = loginType;
      dataToSend["channelId"] = HttpManager.channelId.toString();
      signupdata.forEach((key, value) {
        if (key != null && value != null) {
          modifiedData[key] = value.toString();
        }
      });
      dataToSend["data"] = modifiedData;
      channelResponse = await ChannelManager.WEBENGAGE
          .invokeMethod('webEngageEventSigniup', dataToSend);
    } catch (e) {
      print("Error in WebEngage Signup Event....");
      print(e);
    }
    return channelResponse;
  }

  static Future<void> onUserInfoRefreshed(BuildContext context) async {
    try {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      User user = Provider.of(context, listen: false);
      Map<dynamic, dynamic> userInfo = new Map();
      userInfo["userId"] = user.id != null ? user.id.toString() : "0";
      userInfo["email"] = user.email != null ? user.email : "";
      userInfo["mobile"] = (user.mobile != null && user.mobile.length > 5)
          ? MyHelperClass.getMobileCountryCode() + user.mobile.toString()
          : "";
      userInfo["first_name"] =
          user.address.firstName != null ? user.address.firstName : "";
      userInfo["lastName"] =
          user.address.lastName != null ? user.address.lastName : "";
      userInfo["userName"] = user.loginName != null ? user.loginName : "";
      userInfo["channelId"] = HttpManager.channelId;
      userInfo["withdrawable"] = user.userBalance.withdrawable != null
          ? user.userBalance.withdrawable
          : 0.0;
      userInfo["depositBucket"] = user.userBalance.depositBucket != null
          ? user.userBalance.depositBucket
          : 0.0;
      userInfo["nonWithdrawable"] = user.userBalance.bonusAmount != null
          ? user.userBalance.bonusAmount
          : 0.0;
      if (user.userBalance.goldBars != null) {
        userInfo["chipBalance"] = user.userBalance.goldBars;
      }
      if (user.userBalance.depositBucket != null &&
          user.userBalance.bonusAmount != null &&
          user.userBalance.withdrawable != null) {
        userInfo["balance"] = user.userBalance.depositBucket +
            user.userBalance.bonusAmount +
            user.userBalance.withdrawable;
      }
      if (user.verificationStatus.panVerificationStatus != null) {
        userInfo["idProofStatus"] =
            user.verificationStatus.panVerificationStatus;
      }
      if (user.verificationStatus.mobileVerified != null) {
        userInfo["mobileVerified"] = user.verificationStatus.mobileVerified;
      }
      if (user.verificationStatus.emailVerified != null) {
        userInfo["emailVerified"] = user.verificationStatus.emailVerified;
      }
      if (user.verificationStatus.addressVerificationStatus != null) {
        userInfo["addressProofStatus"] =
            user.verificationStatus.addressVerificationStatus;
      }
      if (user.status != null) {
        userInfo["accountStatus"] = user.status;
      }
      if (packageInfo.version != null) {
        userInfo["appVersion"] = packageInfo.version.toString();
      }
      if (user.address.state != null) {
        userInfo["state"] = user.address.state.toString();
      }
      if (user.userBalance.goldBars != null) {
        userInfo["chipBalance"] = user.userBalance.goldBars;
      }

      String channelResponse = "";
      try {
        channelResponse = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
            .invokeMethod('onUserInfoRefreshed', userInfo);
      } catch (e) {
        print(e);
      }
    } catch (e) {
      print(e);
    }
  }

  Future<String> webengageCustomAttributeTrackUser(
      BuildContext context, Map<dynamic, dynamic> data) async {
    String channelResponse = "";
    try {
      channelResponse = await ChannelManager.WEBENGAGE
          .invokeMethod('webengageCustomAttributeTrackUser', data);
    } catch (e) {
      print(e);
    }
    return channelResponse;
  }

  Future<String> webengageTrackEventsWithAttributes(
      Map<dynamic, dynamic> data) async {
    String channelResponse = "";
    try {
      channelResponse = await ChannelManager.WEBENGAGE
          .invokeMethod('trackEventsWithAttributes', data);
    } catch (e) {
      print(e);
    }
    return channelResponse;
  }

  Future<String> webengageAddScreenData(
      {BuildContext context, Map<String, dynamic> data}) async {
    String channelResponse = "";
    try {
      channelResponse = await ChannelManager.WEBENGAGE
          .invokeMethod('webengageAddScreenData', data);
    } catch (e) {
      print(e);
    }
    return channelResponse;
  }

  webengageRAFClickedEvent(Map<String, dynamic> inputdata) async {
    if (inputdata != null) {
      inputdata.removeWhere((key, value) => value == null);
      Map<dynamic, dynamic> eventdata = new Map();
      eventdata["eventName"] = "RAF_CLICKED";
      eventdata["data"] = inputdata;
      webengageTrackEventsWithAttributes(eventdata);
    }
  }

  /***************************Add Cash and Payment Events*********** */

  webengageAddCashPageVisitEvent(Map<String, dynamic> inputdata) async {
    if (inputdata != null) {
      inputdata.removeWhere((key, value) => value == null);
      Map<dynamic, dynamic> eventdata = new Map();
      eventdata["eventName"] = "CHOOSEAMOUNT_PAGE";
      eventdata["data"] = inputdata;
      webengageTrackEventsWithAttributes(eventdata);
    }
  }

  webengagePaymentModePageVisitEvent(Map<String, dynamic> inputdata) async {
    if (inputdata != null) {
      inputdata.removeWhere((key, value) => value == null);
      Map<dynamic, dynamic> eventdata = new Map();
      eventdata["eventName"] = "PAYMENTMODE_PAGE";
      eventdata["data"] = inputdata;
      webengageTrackEventsWithAttributes(eventdata);
    }
  }

  webengageAddDepositIntiatedEvent(Map<String, dynamic> inputdata) async {
    if (inputdata != null) {
      inputdata.removeWhere((key, value) => value == null);
      Map<dynamic, dynamic> eventdata = new Map();
      eventdata["eventName"] = "DEPOSITE_INIT";
      eventdata["data"] = inputdata;
      webengageTrackEventsWithAttributes(eventdata);
    }
  }

  webengageAddTransactionFailedEvent(Map<String, dynamic> inputdata) async {
    if (inputdata != null) {
      inputdata.removeWhere((key, value) => value == null);
      Map<dynamic, dynamic> eventdata = new Map();
      eventdata["eventName"] = "DEPOSIT_FAIL";
      eventdata["data"] = inputdata;
      webengageTrackEventsWithAttributes(eventdata);
    }
  }

  webengageAddTransactionSuccessEvent(Map<String, dynamic> inputdata) async {
    if (inputdata != null) {
      inputdata.removeWhere((key, value) => value == null);
      Map<dynamic, dynamic> eventdata = new Map();
      eventdata["eventName"] = "DEPOSIT_SUCCESS";
      eventdata["data"] = inputdata;
      webengageTrackEventsWithAttributes(eventdata);
    }
  }

  webengageAddPaymentDropOffEvent(Map<String, dynamic> inputdata) async {
    if (inputdata != null) {
      inputdata.removeWhere((key, value) => value == null);
      Map<dynamic, dynamic> eventdata = new Map();
      eventdata["eventName"] = "PAYMENT_DROPOFF";
      eventdata["data"] = inputdata;
      webengageTrackEventsWithAttributes(eventdata);
    }
  }

  Map<String, dynamic> _getTransactionData(
      Map<String, dynamic> transactionData) {
    DateTime date = DateTime.fromMillisecondsSinceEpoch(
        int.tryParse(transactionData["date"].toString()));
    String dateinString = date.day.toString() +
        "-" +
        date.month.toString() +
        "-" +
        date.year.toString();
    String timeinString = date.hour.toString() +
        ":" +
        date.minute.toString() +
        ":" +
        date.second.toString();

    return {
      "txnDate": dateinString,
      "txnTime": timeinString,
      "txnId": transactionData["txnId"] == null ? "" : transactionData["txnId"],
      "appPage": "AddCashPage",
      "data": transactionData,
      "firstDepositor": transactionData["firstDepositor"],
    };
  }
}
