import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'dart:isolate';
import 'package:flutter/foundation.dart'
    show FlutterError, FlutterErrorDetails, kDebugMode;
import 'package:solitaire_gold/utils/httpmanager.dart';

class MyFirebaseCrashAnalytics {
  String userId;

  Future<void> initializeFlutterFire() async {
    try {
      await Firebase.initializeApp();

      await FirebaseCrashlytics.instance
          .setCrashlyticsCollectionEnabled(!kDebugMode);

      Function originalOnError = FlutterError.onError;
      FlutterError.onError = (FlutterErrorDetails errorDetails) async {
        await FirebaseCrashlytics.instance.recordFlutterError(errorDetails);
        originalOnError(errorDetails);
      };
      Isolate.current.addErrorListener(RawReceivePort((pair) async {
        final List<dynamic> errorAndStacktrace = pair;
        await FirebaseCrashlytics.instance.recordError(
          errorAndStacktrace.first,
          errorAndStacktrace.last,
        );
      }).sendPort);
      addCrashlyticsCustomeKeys("channel_id", HttpManager.channelId);
    } catch (e) {
      print(e);
    }
  }

  Future<void> setCrashlyticsUserIdentity(String _userId) async {
    try {
      this.userId = _userId;
      FirebaseCrashlytics.instance.setUserIdentifier(userId);
      if (_userId != null) addCrashlyticsCustomeKeys("userId", _userId);
    } catch (e) {
      print(e);
    }
  }

  Future<void> addCrashlyticsCustomeKeys(String key, dynamic value) async {
    try {
      FirebaseCrashlytics.instance.setCustomKey(key.toString(), value);
    } catch (e) {
      print(e);
    }
  }
}

MyFirebaseCrashAnalytics firebaseCrashAnalytics = MyFirebaseCrashAnalytics();
