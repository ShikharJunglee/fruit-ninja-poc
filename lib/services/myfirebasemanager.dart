import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';

class MyFirebaseMessagingService {
  static Future<String> getFirebaseToken(AppConfig config) async {
    String value = "";
    try {
      value =
          await ChannelManager.FIREBASE_FCM.invokeMethod('_getFirebaseToken');
      if (value != null && value.length > 3) {
        config.setFirebaseToken(value);
      }
      return value;
    } catch (e) {
      return value;
    }
  }

  static Future<String> setFirebaseToken(AppConfig config) async {
    String value;
    try {
      value =
          await ChannelManager.FIREBASE_FCM.invokeMethod('_getFirebaseToken');
      config.setFirebaseToken(value);
    } catch (e) {}
    return value;
  }

  static Future<void> subscribeToFirebaseTopic(String topicName) async {
    try {
      await ChannelManager.FIREBASE_FCM
          .invokeMethod('_subscribeToFirebaseTopic', topicName)
          .timeout(Duration(seconds: 10));
    } catch (e) {
      print(e);
    }
  }
}
