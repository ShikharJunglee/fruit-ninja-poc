import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/services/mybranchioservice.dart';
import 'package:solitaire_gold/services/myfirebasecrashlytics.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';

class MyUserLoginService {
  static Future<Map<dynamic, dynamic>> getFacebookLoginToken() async {
    Map<dynamic, dynamic> loginResult = new Map();
    var facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    var result = await facebookLogin.logIn(['email', 'public_profile']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        loginResult["status"] = "success";
        loginResult["token"] = result.accessToken.token;
        return loginResult;
        break;
      case FacebookLoginStatus.cancelledByUser:
        loginResult["status"] = "fail";
        return loginResult;
        break;
      case FacebookLoginStatus.error:
        loginResult["status"] = "fail";
        return loginResult;
        break;
      default:
        loginResult["status"] = "fail";
        return loginResult;
    }
  }

  static Future<Map<String, dynamic>> getGoogleLoginToken() async {
    if (Platform.isAndroid) {
      GoogleSignIn _googleSignIn = GoogleSignIn(
        scopes: [
          'email',
        ],
      );
      GoogleSignInAuthentication googleKey;
      Map<String, dynamic> loginResult = new Map();
      try {
        await _googleSignIn.signIn();
        googleKey = await _googleSignIn.currentUser.authentication;
        loginResult["status"] = "success";
        loginResult["token"] = googleKey.accessToken;
        return loginResult;
      } catch (error) {
        loginResult["status"] = "fail";
        return loginResult;
      }
    } else {
      Map<String, dynamic> loginResult = new Map();
      try {
        final value =
            await ChannelManager.LOGINSERVICE.invokeMethod('doGoogleLogin');
        if (value != null && value["status"] != null) {
          if (value["status"] == "success") {
            loginResult["status"] = "success";
            loginResult["token"] = value["token"];
          } else {
            loginResult["status"] = "fail";
          }
          return loginResult;
        } else {
          loginResult["status"] = "fail";
          return loginResult;
        }
      } catch (e) {
        loginResult["status"] = "fail";
        return loginResult;
      }
    }
  }

  static Future<Map<String, dynamic>> getAppleLoginToken() async {
    Map<String, dynamic> loginResult = new Map();
    final credential = await SignInWithApple.getAppleIDCredential(
      scopes: [AppleIDAuthorizationScopes.email],
    );
    if (credential.identityToken != null &&
        credential.authorizationCode != null &&
        credential.userIdentifier != null) {
      loginResult["status"] = "success";
      loginResult["authorizationCode"] = credential.authorizationCode;
      loginResult["userIdentifier"] = credential.userIdentifier;
      loginResult["identityToken"] = credential.identityToken;
      return loginResult;
    } else {
      loginResult["status"] = "fail";
      return loginResult;
    }
  }

  static Future<void> _getLocalStorageValues(BuildContext context) async {
    //AppConfig config = Provider.of<AppConfig>(context, listen: false);
    // deviceId = config.firebaseToken;
    // pfRefCode = config.branchReferralCode;
    // installReferringLink = config.branchInviteUrl;
    // if (deviceId.length < 3) {
    //   getFirebaseToken(config).then((String refcode) {
    //     deviceId = refcode;
    //   });
    // }
    return null;
  }

  static Future<void> onLoginAuthenticate(
    BuildContext context,
    Map<String, dynamic> loginData,
    String userSelectedLoginType,
    User user,
  ) async {
    try {
      if (user.id != null) {
        await firebaseCrashAnalytics
            .setCrashlyticsUserIdentity(user.id.toString());
        await MyBranchIoService.doBranchIoLogin(user.id.toString());
        await MyWebEngageService.doWebEngageLogin(context, user.id.toString());
      }
      String phoneStringValue = "";
      String emailStringValue = "";
      if (loginData["email"] != null) {
        emailStringValue = loginData["email"];
      }
      if (loginData["mobile"] != null) {
        phoneStringValue = loginData["mobile"];
      }
      bool isNewUser = false;
      if (loginData["newUser"] != null && loginData["newUser"]) {
        isNewUser = true;
      }
      if (loginData["isNewUser"] != null && loginData["isNewUser"]) {
        isNewUser = true;
      }
      if (!isNewUser) {
        try {
          String channelResult = await MyWebEngageService().webEngageEventLogin(
              context,
              data: loginData,
              loginType: userSelectedLoginType,
              phone: phoneStringValue,
              email: emailStringValue);
        } catch (e) {}
      } else {
        try {
          String channelResult = await MyWebEngageService()
              .webEngageEventSignUp(context,
                  signupdata: loginData,
                  loginType: userSelectedLoginType,
                  phone: phoneStringValue,
                  email: emailStringValue);
          String branchSignupREsult = await MyBranchIoService()
              .branchLifecycleEventSigniup(context,
                  loginData: loginData,
                  loginType: userSelectedLoginType,
                  phone: phoneStringValue,
                  email: emailStringValue);
        } catch (e) {}
      }
    } catch (e) {}
  }
}

class UserSelectedLoginType {
  static const APPLE_LOGIN = "APPLE_LOGIN";
  static const GOOGLE_LOGIN = "GOOGLE_LOGIN";
  static const FACEBOOK_LOGIN = "FACEBOOK_LOGIN";
  static const OTP_LOGIN = "OTP_LOGIN";
  static const EMAIL_LOGIN = "EMAIL_LOGIN";
  static const MOBILE_LOGIN = "MOBILE_LOGIN";
}
