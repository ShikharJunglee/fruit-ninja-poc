import 'dart:io';

import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/services/myhelperclass.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class FlutterToNativeService {
  static Future<String> getAdvertisingId(AppConfig config) async {
    String value = "";
    if (Platform.isAndroid) {
      try {
        value = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
            .invokeMethod('getAdvertisingId');
        if (value != null && value.length > 3) {
          config.setGoogleAddId(value);
        }
        return value;
      } catch (e) {
        return value;
      }
    } else {
      return "";
    }
  }

  static Future<Map<dynamic, dynamic>> getDeviceInfoFromNative(
    AppConfig config,
  ) async {
    Map<dynamic, dynamic> deviceInfoMap = new Map();
    try {
      final data = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
          .invokeMethod('getDeviceInfoFromNative');
      if (data != null) {
        deviceInfoMap = data;
      }
      if (deviceInfoMap["googleAdId"] != null &&
          deviceInfoMap["googleAdId"].length > 4) {
        config.setGoogleAddId(deviceInfoMap["googleAdId"]);
      }
      return deviceInfoMap;
    } catch (e) {
      return deviceInfoMap;
    }
  }

  static Future<String> onUserInfoRefreshed(BuildContext context) async {
    User user = Provider.of(context, listen: false);
    Map<dynamic, dynamic> userInfo = new Map();
    userInfo["email"] = user.email != null ? user.email : "";
    userInfo["mobile"] = (user.mobile != null && user.mobile.length > 5)
        ? MyHelperClass.getMobileCountryCode().toString() + user.mobile.toString()
        : "";
    userInfo["first_name"] =
        user.address.firstName != null ? user.address.firstName : "";
    userInfo["lastName"] =
        user.address.lastName != null ? user.address.lastName : "";
    userInfo["login_name"] = user.loginName != null ? user.loginName : "";
    userInfo["channelId"] = HttpManager.channelId;
    userInfo["withdrawable"] = user.userBalance.withdrawable;
    userInfo["depositBucket"] = user.userBalance.depositBucket;
    userInfo["nonWithdrawable"] = 0.0;
    userInfo["nonPlayableBucket"] = 0.0;
    userInfo["accountStatus"] = "";
    userInfo["user_balance_webengage"] =
        user.userBalance.depositBucket + user.userBalance.withdrawable;
    userInfo["pan_verification"] =
        user.verificationStatus.panVerificationStatus;
    userInfo["mobile_verification"] = user.verificationStatus.mobileVerified;
    userInfo["address_verification"] =
        user.verificationStatus.addressVerificationStatus;
    userInfo["email_verification"] = user.verificationStatus.emailVerified;
    String accountStatus = "";
    if (accountStatus == "ACTIVE") {
      userInfo["accountStatus"] = "1";
    } else if (accountStatus == "CLOSED") {
      userInfo["accountStatus"] = "2";
    } else if (accountStatus == "BLOCKED") {
      userInfo["accountStatus"] = "3";
    }
    userInfo["pincode"] = "";
    userInfo["dob"] = "";
    userInfo["state"] = "";
    String channelResponse = "";
    try {
      channelResponse = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
          .invokeMethod('onUserInfoRefreshed', userInfo);
    } catch (e) {
      print(e);
    }
    return channelResponse;
  }
}
