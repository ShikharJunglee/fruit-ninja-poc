import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class MyBranchIoService {
  MyBranchIoService._internal();
  static final MyBranchIoService branchIO = MyBranchIoService._internal();
  factory MyBranchIoService() => branchIO;

  static initBranchIoPlugin(AppConfig config, BuildContext context) async {
    try {
      final value = await ChannelManager.BRANCH_IO
          .invokeMethod('_initBranchIoPlugin')
          .timeout(Duration(seconds: 30));
      if (value != null) {
        Map<dynamic, dynamic> branchResponse = {};
        if (Platform.isAndroid) {
          branchResponse = json.decode(value);
        } else {
          branchResponse = value;
        }
        if (branchResponse != null) {
          config.branchReferringParams = branchResponse;
          String referalLink = branchResponse["~referring_link"];
          if (referalLink != null && referalLink.isNotEmpty) {
            config.setBranchUrl(referalLink.toString());
            Uri uri = Uri.parse(referalLink);
            String refCode = uri.queryParameters["refCode"];
            if (refCode != null && refCode.isNotEmpty) {
              config.setBranchReferralCode(refCode);
            }
          }
        }
      }
    } catch (e) {}
  }

  static Future<String> getInstallReferringLink(AppConfig config) async {
    String value;
    try {
      value = await ChannelManager.BRANCH_IO
          .invokeMethod('_getInstallReferringLink');
      if (value != null && value.length > 3) {
        return value;
      } else {
        return "";
      }
    } catch (e) {
      return "";
    }
  }

  static Future<Map<dynamic, dynamic>> getBranchInstallParams(
      AppConfig config) async {
    Map<dynamic, dynamic> installParma = new Map();
    try {
      final value =
          await ChannelManager.BRANCH_IO.invokeMethod('getBranchInstallParams');
      if (value != null) {
        if (Platform.isAndroid) {
          installParma = json.decode(value);
        } else {
          print(installParma);
          installParma = value;
        }
      }
      return installParma;
    } catch (e) {
      print(e);
      return installParma;
    }
  }

  static Future<Map<dynamic, dynamic>> getBranchSessionParams(
      AppConfig config) async {
    Map<dynamic, dynamic> installParma = new Map();
    try {
      final value = await ChannelManager.BRANCH_IO
          .invokeMethod('getLatestReferringParms');
      if (value != null) {
        if (Platform.isAndroid) {
          print(installParma);
          installParma = json.decode(value);
        } else {
          print(installParma);
          installParma = value;
        }
      }
      return installParma;
    } catch (e) {
      print(e);
      return installParma;
    }
  }

  Future<String> branchLifecycleEventSigniup(BuildContext context,
      {Map<String, dynamic> loginData,
      String loginType,
      String email,
      String phone}) async {
    String channelResponse = "";
    try {
      Map<dynamic, dynamic> signupdata = new Map();
      signupdata["registrationID"] = loginData["id"].toString();
      signupdata["transactionID"] = loginData["id"].toString();
      signupdata["description"] =
          "CHANNEL" + HttpManager.channelId.toString().toString() + "SIGNUP";
      signupdata["data"] = loginData;
      channelResponse = await ChannelManager.BRANCH_IO
          .invokeMethod('branchLifecycleEventSigniup', signupdata);
    } catch (e) {
      print("Error in Branch IO Signup Event....");
      print(e);
    }
    return channelResponse;
  }

  Future<void> branchEventTransactionFailed(
      Map<String, dynamic> transactionData) async {
    String channelResponse = "";
    try {
      Map<dynamic, dynamic> trackdata = new Map();
      DateTime date = DateTime.fromMillisecondsSinceEpoch(
          int.parse(transactionData["date"].toString()));
      String dateinString = date.day.toString() +
          "-" +
          date.month.toString() +
          "-" +
          date.year.toString();
      String timeinString = date.hour.toString() +
          ":" +
          date.minute.toString() +
          ":" +
          date.second.toString();
      trackdata["txnDate"] = dateinString;
      trackdata["txnTime"] = timeinString;
      trackdata["txnId"] = transactionData["txnId"];
      trackdata["appPage"] = "AddCashPage";
      trackdata["data"] = transactionData;
      trackdata["firstDepositor"] = transactionData["firstDepositor"];
      channelResponse = await ChannelManager.BRANCH_IO
          .invokeMethod('branchEventTransactionFailed', trackdata);
    } catch (e) {}
  }

  Future<String> branchEventTransactionSuccess(
      Map<String, dynamic> transactionData) async {
    String channelResponse = "";
    try {
      Map<dynamic, dynamic> trackdata = new Map();
      DateTime date = DateTime.fromMillisecondsSinceEpoch(
          int.parse(transactionData["date"].toString()));
      String dateinString = date.day.toString() +
          "-" +
          date.month.toString() +
          "-" +
          date.year.toString();
      String timeinString = date.hour.toString() +
          ":" +
          date.minute.toString() +
          ":" +
          date.second.toString();
      trackdata["txnDate"] = dateinString;
      trackdata["txnTime"] = timeinString;
      trackdata["appPage"] = "AddCashPage";
      trackdata["data"] = transactionData;
      trackdata["firstDepositor"] = transactionData["firstDepositor"];
      channelResponse = await ChannelManager.BRANCH_IO
          .invokeMethod('branchEventTransactionSuccess', trackdata);
    } catch (e) {}
    return channelResponse;
  }

  static Future<Map<dynamic, dynamic>> getAndroidDeviceInfo() async {
    Map<dynamic, dynamic> androidDeviceInfoMap = new Map();
    try {
      androidDeviceInfoMap =
          await ChannelManager.BRANCH_IO.invokeMethod('_getAndroidDeviceInfo');
      return androidDeviceInfoMap;
    } catch (e) {
      return androidDeviceInfoMap;
    }
  }

  static Future<String> doBranchIoLogin(String userId) async {
    String channelResponse = "";
    try {
      channelResponse = await ChannelManager.BRANCH_IO
          .invokeMethod('trackAndSetBranchUserIdentity', userId);
    } catch (e) {
      print("Error in Branch IO tracking.....");
      print(e);
    }
    return channelResponse;
  }

  Future<void> doBranchIoLogOut() async {
    try {
      await ChannelManager.BRANCH_IO.invokeMethod('branchUserLogout');
    } catch (e) {
      print("Error in Branch IO tracking.....");
      print(e);
    }
  }
}
