import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/providers/deeplinkroute.dart';

import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';

class MyDeepLinkingService {
  StreamSubscription _branchattributionSubscription;
  StreamSubscription _deeplinkingSubscription;

  static Future<Map<dynamic, dynamic>> getDeepLinkingRoutingData() async {
    Map<dynamic, dynamic> deepLinkingRoutingData = new Map();
    try {
      if (Platform.isIOS) {
        final value = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
            .invokeMethod('_deepLinkingRoutingHandler');
        if (value != null) {
          deepLinkingRoutingData = value;
        }
      } else {
        final value = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
            .invokeMethod('_deepLinkingRoutingHandler')
            .timeout(Duration(seconds: 10));
        if (value != null) {
          deepLinkingRoutingData = value;
        }
      }
      return deepLinkingRoutingData;
    } catch (e) {
      return deepLinkingRoutingData;
    }
  }

  static void setDeepLinkRouteProviderData(
      BuildContext context, Map<dynamic, dynamic> deepLinkingRoutingData) {
    try {
      if (deepLinkingRoutingData != null &&
          deepLinkingRoutingData["activateDeepLinkingNavigation"] != null &&
          deepLinkingRoutingData["activateDeepLinkingNavigation"]) {
        DeepLinkRoute routesData =
            Provider.of<DeepLinkRoute>(context, listen: false);
        routesData.setData(DeepLinkRoute.fromJson(deepLinkingRoutingData));
      }
    } catch (e) {}
  }

  @protected
  subscribeToBranchAttributionListener(
      AppConfig config, bool disableBranchIOAttribution) {
    if (_branchattributionSubscription == null) {
      _branchattributionSubscription = ChannelManager.AUTH_EVENTS_STREAM_CHANNEL
          .receiveBroadcastStream()
          .listen((dataFromListner) {
        if (dataFromListner != null) {
          try {
            if (dataFromListner["installReferring_link"] != null) {
              if (dataFromListner["installReferring_link"].length > 3) {
                config.setBranchUrl(dataFromListner["installReferring_link"]);
              }
            }
            if (disableBranchIOAttribution) {
              if (dataFromListner["refCodeFromBranch"] != null) {
                if (dataFromListner["refCodeFromBranch"].length > 3) {
                  config.setBranchReferralCode(
                      dataFromListner["refCodeFromBranch"]);
                }
              }
            }
          } catch (e) {}
        }
      });
    }
  }

  subscribeToDeepLinkingListener(
      AppConfig config, bool disableBranchIOAttribution, BuildContext context,
      {final Function onDataReceived}) {}

  unsubscribeToBranchAttributionListener() {
    if (_branchattributionSubscription != null) {
      _branchattributionSubscription.cancel();
      _branchattributionSubscription = null;
    }
  }

  unsubscribeToDeepLinkingListener() {
    if (_deeplinkingSubscription != null) {
      _deeplinkingSubscription.cancel();
    }
  }
}
