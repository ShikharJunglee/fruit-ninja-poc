import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class AuthAPI {
  Future<http.Response> login({
    @required BuildContext context,
    @required String userName,
    @required password,
    @required payloadContext,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.LOGIN_URL.toString()));

    var payload = {
      "value": {
        "auth_attribute": userName,
        "password": password,
      },
      "context": payloadContext,
    };

    req.body = json.encode(payload);

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        return res;
      },
    );
  }

  Future<Map<String, dynamic>> getLoginStatus(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
      "GET",
      Uri.parse(config.apiUrl + AppUrl.AUTH_CHECK_URL.toString()),
    );

    Map<String, dynamic> response;
    try {
      response = await HttpManager(http.Client()).sendRequest(req).then(
        (http.Response res) {
          Map<String, dynamic> result = json.decode(res.body);
          if (res.statusCode >= 200 && res.statusCode <= 299) {
            return {"error": false, "user": result["data"]};
          } else {
            Map<String, dynamic> response = json.decode(res.body);
            return {"error": true, "message": response["error"]};
          }
        },
      );
    } on SocketException {
      await Future.delayed(Duration(milliseconds: 500), () async {
        response = await getLoginStatus(context);
      });
    }
    return response;
  }

  Future<http.Response> signup({
    @required BuildContext context,
    @required String email,
    @required String mobile,
    @required password,
    @required payloadContext,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.SIGN_UP.toString()));

    Map<String, dynamic> payload = {
      "password": password,
      "context": payloadContext,
    };
    if (email == null) {
      payload["phone"] = mobile;
    } else {
      payload["email"] = email;
    }

    req.body = json.encode(payload);

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        return res;
      },
    );
  }

  requestOTPForLogin({
    @required BuildContext context,
    @required String mobile,
    String email = "",
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    Map<String, dynamic> _payload = {
      "mobile": mobile,
      "email": email,
      "context": {
        "channelId": HttpManager.channelId,
      }
    };
    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.REQUEST_OTP.value));

    req.body = json.encode(_payload);
    return await HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          return {
            "error": false,
            "data": json.decode(res.body),
          };
        } else {
          Map<String, dynamic> response = json.decode(res.body);
          return {
            "error": true,
            "message": response["message"],
          };
        }
      },
    );
  }

  resendOTPForLogin({
    @required BuildContext context,
    @required String mobile,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    Map<String, dynamic> _payload = {
      "mobile": mobile,
      "context": {
        "channelId": HttpManager.channelId,
      }
    };

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.RESEND_OTP.value));
    req.body = json.encode(_payload);
    return await HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          return {
            "error": false,
          };
        } else {
          return {
            "error": true,
            "message": "Unable to send OTP.",
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> doPasswordSignIn({
    @required BuildContext context,
    @required String mobile,
    @required String password,
    String email = "",
    @required Map<String, dynamic> payloadContext,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.PASSSWORD_SIGNIN.value));

    Map<String, dynamic> payload = {
      "password": password,
      "username": mobile,
      "email": email,
      "context": payloadContext,
    };

    req.body = json.encode(payload);

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          SharedPrefHelper().saveCookieToStorage(res.headers["set-cookie"]);
          return {
            "error": false,
            "data": response["data"],
          };
        } else {
          return {
            "error": true,
            "message": response["error"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> doPasswordSignUp({
    @required BuildContext context,
    @required String mobile,
    @required String password,
    String email = "",
    @required Map<String, dynamic> payloadContext,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req =
        http.Request("POST", Uri.parse(config.apiUrl + AppUrl.SIGN_UP.value));

    Map<String, dynamic> payload = {
      "password": password,
      "phone": mobile,
      "email": email,
      "context": payloadContext,
    };

    req.body = json.encode(payload);

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          SharedPrefHelper().saveCookieToStorage(res.headers["set-cookie"]);
          return {
            "error": false,
            "data": response["data"],
          };
        } else {
          return {
            "error": true,
            "message": response["error"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> socialNativeLogin({
    @required BuildContext context,
    @required String token,
    @required String loginType,
    @required payloadContext,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    String apiUrl = config.apiUrl + AppUrl.SOCIAL_SIGN_IN.toString();
    http.Request req = http.Request("POST", Uri.parse(apiUrl));
    var payload = {
      "accessToken": token,
      "loginType": loginType,
      "context": payloadContext,
    };
    req.body = json.encode(payload);
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          SharedPrefHelper().saveCookieToStorage(res.headers["set-cookie"]);
          return {
            "error": false,
            "data": response["data"],
          };
        } else {
          return {
            "error": true,
            "message": response["message"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> appleSocialNativeLogin({
    @required BuildContext context,
    @required String authorizationCode,
    @required String identityToken,
    @required String userIdentifier,
    @required payloadContext,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    String apiUrl = config.apiUrl + AppUrl.APPLE_NATIVE_LOGIN_URL.toString();
    http.Request req = http.Request("POST", Uri.parse(apiUrl));
    var payload = {
      "authorizationCode": authorizationCode,
      "identityToken": identityToken,
      "userIdentifier": userIdentifier,
      "context": payloadContext,
    };
    req.body = json.encode(payload);
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        print("Apple Response......");
        print(response);
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          SharedPrefHelper().saveCookieToStorage(res.headers["set-cookie"]);
          return {
            "error": false,
            "data": response["data"],
          };
        } else {
          return {
            "error": true,
            "message": response["message"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> doOTPSignin({
    @required BuildContext context,
    @required String mobile,
    @required String otp,
    String email = "",
    @required Map<String, dynamic> payloadContext,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.OTP_SIGNIN.value));

    Map<String, dynamic> payload = {
      "password": otp,
      "mobile": mobile,
      "email": email,
      "context": payloadContext,
    };

    req.body = json.encode(payload);

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          SharedPrefHelper().saveCookieToStorage(res.headers["set-cookie"]);
          return {
            "error": false,
            "data": response["data"],
          };
        } else {
          return {
            "error": true,
            "message": response["message"],
          };
        }
      },
    );
  }

  Future<http.Response> requestOTP(
      BuildContext context, String userName) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.FORGOT_PASSWORD.value));
    req.body = json.encode({
      "username": userName,
      "isEmail": userName.indexOf("@") != -1,
    });
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        return res;
      },
    );
  }

  Future<dynamic> getBanners(BuildContext context, {int zone}) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET",
        Uri.parse(
          "${config.apiUrl}${AppUrl.GET_SIGNIN_BANNERS.value}/$zone",
        ));
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        final banners = json.decode(res.body)["data"];

        return (banners as List).map((dynamic value) {
          return value;
        }).toList();
      }
      return null;
    });
  }

  Future<bool> logout(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET",
        Uri.parse(
          "${config.apiUrl}${AppUrl.LOGOUT_URL.value}",
        ));
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        return true;
      } else {
        return false;
      }
    });
  }
}
