import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class LobbyAPI {
  Future<Map<String, dynamic>> getPrizeStructure(BuildContext context,
      {int prizestructureId, int joinCount, int status}) {
    AppConfig config = Provider.of(context, listen: false);
    http.Request req;
    if (joinCount != null && status != null) {
      req = http.Request(
        "GET",
        Uri.parse(
          config.apiUrl +
              AppUrl.GET_PRIZESTRUCTURE.toString() +
              prizestructureId.toString() +
              "/$joinCount/$status",
        ),
      );
    } else {
      req = http.Request(
        "GET",
        Uri.parse(config.apiUrl +
            AppUrl.GET_PRIZESTRUCTURE.toString() +
            prizestructureId.toString()),
      );
    }

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "success") {
        return {
          "data": response["data"],
          "error": false,
        };
      } else {
        return {
          "error": true,
          "message": "Getting Error whille verifying email."
        };
      }
    });
  }

  Future<Map<String, dynamic>> joinContest(
    BuildContext context, {
    double bonusAllowed,
    double entryFee,
    int diamonds,
    Map<String, dynamic> locationData,
  }) async {
    AppConfig config = Provider.of(context, listen: false);

    http.Request req = http.Request(
      "POST",
      Uri.parse(config.apiUrl + AppUrl.JOIN_CONTEST.toString()),
    );

    req.body = json.encode({
      "entryFee": entryFee,
      "bonusAllowed": bonusAllowed,
      "goldBars": diamonds,
      "locationData": locationData != null ? locationData["data"] : null,
      "updateUserLocation": locationData != null
          ? locationData["updateUserLocationOnServer"]
          : false,
    });

    Map<String, dynamic> response;
    try {
      response = await HttpManager(http.Client())
          .sendRequest(req)
          .then((http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          return {
            "data": response["data"],
            "error": false,
          };
        } else {
          return {
            "error": true,
            "message": "Something went wrong! Please try again."
          };
        }
      });
    } on SocketException {
      return {
        "error": true,
        "isSocketError": true,
        "message": "Check your internet connection and retry."
      };
    } on http.ClientException {
      return {
        "error": true,
        "message": "Something went wrong! Please try again."
      };
    }

    return response;
  }

  Future<Map<String, dynamic>> getReplayData(
    BuildContext context, {
    int matchId,
  }) {
    InitData initData = Provider.of(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(initData.experiments.replayConfigs.url + matchId.toString()),
    );

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      return {
        "data": response,
        "error": false,
      };
    }).catchError((error) {});
  }

  Future<Map<String, dynamic>> getLeaderBoardDataWithMatchId(
      BuildContext context, int matchId) {
    AppConfig config = Provider.of(context, listen: false);
    http.Request req;
    req = http.Request(
      "GET",
      Uri.parse(config.apiUrl +
          AppUrl.GET_LEADERBOARD_DATA_WITH_MATCHID.toString() +
          matchId.toString()),
    );
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res != null) {
        try {
          Map<String, dynamic> response = json.decode(res.body);
          if (response["type"] == "success") {
            return {
              "data": response["data"],
              "error": false,
            };
          } else {
            return {"error": true, "message": "Getting Error"};
          }
        } catch (e) {
          return {"error": true, "message": "Getting Error"};
        }
      } else {
        return {"error": true, "message": "Getting Error"};
      }
    });
  }
}
