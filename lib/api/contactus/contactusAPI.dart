import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:http_parser/http_parser.dart';
import 'package:path/path.dart' as path;
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class ContactUsAPI {
  Future<Map<String, dynamic>> getContactUsData(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.CONTACTUS_FORM.toString(),
      ),
    );
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          return {
            "error": false,
            "data": response["data"],
          };
        } else {
          return {
            "error": true,
            "message": response["message"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> submitContactUs(
    BuildContext context, {
    String category,
    String subCategory,
    String comments,
    String email,
    String mobile,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.CONTACTUS_SUBMIT.toString()));
    req.body = json.encode({
      "category": category,
      "subCategory": subCategory,
      "description": comments,
      "email": email,
      "phone": mobile
    });
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "success") {
        return {"error": false};
      } else {
        return {"error": true};
      }
    });
  }

  Future<Map<String, dynamic>> submitReportProblem(
    BuildContext context, {
    String category,
    String subCategory,
    String description,
    String email,
    String mobile,
    int matchId,
    int gameId,
    File attachment,
  }) async {
    var attachmentLength;
    var attachmentStream;
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    var url =
        Uri.parse(config.apiUrl + AppUrl.REPORT_PROBLEM_SUBMIT.toString());
    var req = http.MultipartRequest("POST", url);
    if (attachment != null) {
      attachmentLength = await attachment.length();
      attachmentStream =
          http.ByteStream(DelegatingStream.typed(attachment.openRead()));
      req.files.add(http.MultipartFile(
          'fileAttachments', attachmentStream, attachmentLength,
          filename: path.basename(attachment.path),
          contentType: MediaType('image', 'png')));
    }

    req.fields.addAll({
      "category": category,
      "subCategory": subCategory,
      "description": description,
      "email": email,
      "phone": mobile,
      "logs": "",
      "gameId": gameId.toString(),
      "matchId": matchId.toString(),
    });

    Map<String, dynamic> response = {"error": true, "body": {}};
    await HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          // Map<String, dynamic> responseBody = json.decode(res.body);
          response["error"] = false;
        } else {
          response["error"] = true;
        }
      },
    );
    return response;
  }
}
