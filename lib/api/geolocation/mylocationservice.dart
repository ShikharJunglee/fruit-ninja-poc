import 'dart:io';
import 'package:flutter/material.dart';

import 'package:location_permissions/location_permissions.dart';
import 'package:solitaire_gold/popups/common/locationsettings_ios.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';

class MyLocationService {
  MyLocationService._internal();
  static final MyLocationService locationService =
      MyLocationService._internal();
  factory MyLocationService() => locationService;
  static Future<void> askForLocationPermission(
      BuildContext context, bool bForceUserForLocation, String source) async {
    try {
      if (Platform.isIOS) {
        await LocationPermissions().requestPermissions();
        PermissionStatus permissionStatus =
            await LocationPermissions().checkPermissionStatus();
        if (permissionStatus == PermissionStatus.granted) {
        } else if (permissionStatus == PermissionStatus.denied) {
          if (bForceUserForLocation) {
            await openIOSSettings(context, source);
          }
        }
      }
    } catch (e) {
      return "";
    }
  }

  static Future<Map<String, dynamic>> getLongLatValues(
      BuildContext context, bool bForceUserForLocation, String source) async {
    LocationPermissions _permissions = LocationPermissions();
    await _permissions.requestPermissions();
    Map<String, dynamic> locationData = new Map();
    PermissionStatus permissionStatus =
        await LocationPermissions().checkPermissionStatus();
    locationData["bAccessGiven"] = false;
    if (permissionStatus.toString() == PermissionStatus.granted.toString()) {
      try {
        final value = await ChannelManager.LOCATION_MANAGER_CHANNEL
            .invokeMethod('getLocationLongLat');

        if (value != null && value["bAccessGiven"] != null) {
          if (value["bAccessGiven"] == "true") {
            locationData["bAccessGiven"] = true;
            locationData["longitude"] = value["longitude"];
            locationData["latitude"] = value["latitude"];
          }
        }
      } catch (e) {}
    } else if (permissionStatus.toString() ==
        PermissionStatus.denied.toString()) {
      if (bForceUserForLocation) {
        await openIOSSettings(context, source);
      }
    } else {
      ServiceStatus _status = await _permissions.checkServiceStatus();
      if (_status == ServiceStatus.disabled) {
        await openIOSSettings(context, source);
      }
    }
    return locationData;
  }

  static openIOSSettings(BuildContext context, String source) async {
    var result = await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      barrierDismissible: true,
      builder: (prizestructurecontext) {
        return LocationSettingsDialogIOS(
          source: source,
        );
      },
    );

    if (result == true) {
      LocationPermissions _permissions = LocationPermissions();

      return _permissions.openAppSettings();
    }

    return false;
  }

  static void openSettingForGrantingPermissions(BuildContext context) async {
    await LocationPermissions().openAppSettings();
    PermissionStatus permissionStatus =
        await LocationPermissions().checkPermissionStatus();
    if (permissionStatus.toString() == PermissionStatus.granted.toString()) {
      Navigator.of(context).pop();
    }
  }
}
