import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:location_permissions/location_permissions.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/api/geolocation/mylocationservice.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:solitaire_gold/utils/location_permission.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:permission/permission.dart' as UserPermission;
import 'package:solitaire_gold/utils/sharedpref/sharedprefkeys.dart';

class LocationAPI {
  Completer<bool> gpsEnabled = Completer<bool>();

  static getStoredLocation(InitData config) async {
    Map<String, dynamic> locationResult;
    String storedLocation =
        await SharedPrefHelper().getFromSharedPref(SharedPrefKey.geoloc);
    if (storedLocation != null) {
      Map<String, dynamic> storedLocationMap = json.decode(storedLocation);
      int hoursSinceLastStored = DateTime.now()
          .difference(DateTime.fromMillisecondsSinceEpoch(
              storedLocationMap["timestamp"]))
          .inHours;
      if (hoursSinceLastStored <
          config.locationStateRestrictions["hoursUntilNewLocation"]) {
        locationResult = {
          "isError": false,
          "data": storedLocationMap["location"],
          "updateUserLocationOnServer": false
        };
        return locationResult;
      }
    }

    return null;
  }

  Future<Map<String, dynamic>> getUserLocation(
    BuildContext context, {
    isAddCash = false,
    isJoinContest = false,
  }) async {
    Loader().showLoader(true, immediate: true);

    String source = !isAddCash && !isJoinContest
        ? "lobby"
        : isAddCash ? "add_cash" : "play";

    User user = Provider.of<User>(context, listen: false);
    InitData initData = Provider.of(context, listen: false);

    if (user.isBlockedState != null && user.isBlockedState == true) {
      return {"isError": true, "data": null, "isBlockedState": true};
    }

    Map<String, dynamic> locationResult;
    Map<String, dynamic> locationConfig =
        initData.locationStateRestrictions["addCash"];
    bool useGeoLocation =
        locationConfig["getLocationUsing"].contains("geolocation");
    bool useGPS = locationConfig["getLocationUsing"].contains("gps");

    if (isJoinContest) {
      locationConfig = initData.locationStateRestrictions["joinCashContest"];
    }

    locationResult = await getStoredLocation(initData);

    if (locationResult == null) {
      if (useGeoLocation && useGPS) {
        locationResult = await getGeoLocationResult(context, locationConfig);

        if (locationResult["isError"] ||
            (locationResult["data"]["accuracy"]).toDouble() >
                (locationConfig["accuracyThresholdMetres"]).toDouble()) {
          if (Platform.isAndroid) {
            locationResult = await getGPSLocation(context, source: source);
          } else {
            locationResult = await getGPSLocationIOS(context, source: source);
          }
        }
      } else if (useGPS) {
        if (Platform.isAndroid) {
          locationResult = await getGPSLocation(context, source: source);
        } else {
          locationResult = await getGPSLocationIOS(context, source: source);
        }
      } else if (useGeoLocation) {
        locationResult = await getGeoLocationResult(context, locationConfig);
      }
    }

    if (locationResult != null && !locationResult["isError"]) {
      SharedPrefHelper().saveToSharedPref(
          SharedPrefKey.geoloc,
          json.encode({
            "timestamp": DateTime.now().millisecondsSinceEpoch,
            "location": locationResult["data"]
          }));
    }
    if (locationResult != null) {
      locationResult["updateUserLocationOnServer"] = true;
    }
    return locationResult;
  }

  getGeoLocationResult(
      BuildContext context, Map<String, dynamic> locationConfig) async {
    Map<String, dynamic> locationResult;
    try {
      locationResult = await getGeoLocation(context,
          timeoutSeconds: locationConfig["geolocationTimeoutSeconds"]);
    } on TimeoutException catch (e) {
      print("Geolocation request timed out!");
      print(e);
      locationResult = {"isError": true, "error": ""};
    }

    return locationResult;
  }

  Future<Map<String, dynamic>> getGeoLocation(BuildContext context,
      {int timeoutSeconds}) async {
    InitData initData = Provider.of(context, listen: false);

    http.Request req = http.Request("POST",
        Uri.parse(AppUrl.GET_GEOLOCATION.toString() + initData.geoLocationKey));
    req.body = json.encode({});
    return HttpManager(http.Client())
        .sendRequest(req)
        .timeout(Duration(seconds: timeoutSeconds))
        .then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["error"] != null) {
          return {"isError": true, "error": result["error"]};
        } else {
          return {"isError": false, "data": result};
        }
      },
    );
  }

  Future<Map<String, dynamic>> getGPSLocation(
    BuildContext context, {
    @required String source,
  }) async {
    Map<String, dynamic> position;
    try {
      LocationPermissionHandler locationPermission =
          LocationPermissionHandler();
      UserPermission.PermissionStatus status =
          await locationPermission.checkForPermission(context, source: source);
      if (status == null ||
          status == UserPermission.PermissionStatus.deny ||
          status == UserPermission.PermissionStatus.notAgain) {
        Loader().showLoader(false);

        GamePlayAnalytics().onGeoLocationAccessGiven(context, access: "deny");

        return {
          "isError": true,
          "data": null,
          "isBlockedState": false,
          "permissionStatus": status
        };
      }
      GamePlayAnalytics().onGeoLocationAccessGiven(context, access: "allow");

      bool isGPSON =
          await ChannelManager.GPS_STREAM_CHANNEL.invokeMethod('checkAndOnGPS');

      GamePlayAnalytics().onGeoLocationStatus(
        context,
        source: source,
        status: isGPSON,
      );

      if (isGPSON) {
        position = (await ChannelManager.GPS_STREAM_CHANNEL
                .invokeMethod('getGPSLocation'))
            .cast<String, dynamic>();
      }
    } catch (e) {}

    if (position != null) {
      return {
        "isError": false,
        "data": {
          "location": {
            "lat": position["latitude"],
            "lng": position["longitude"]
          },
          "accuracy": position["accuracy"]
        }
      };
    }

    return {"isError": true, "data": null};
  }

  Future<Map<String, dynamic>> getGPSLocationIOS(BuildContext context,
      {@required String source}) async {
    Map<String, dynamic> position;

    LocationPermissions _permissions = LocationPermissions();

    PermissionStatus permissionStatus =
        await _permissions.checkPermissionStatus();

    if (permissionStatus == null ||
        permissionStatus == PermissionStatus.denied) {
      GamePlayAnalytics().onGeoLocationAccessGiven(context, access: "deny");
      await MyLocationService.openIOSSettings(context, source);

      return {
        "isError": true,
        "data": null,
        "isBlockedState": false,
        "permissionStatus": permissionStatus
      };
    } else {
      GamePlayAnalytics().onGeoLocationAccessGiven(context, access: "allow");

      ServiceStatus _status = await _permissions.checkServiceStatus();
      if (_status == ServiceStatus.disabled) {
        GamePlayAnalytics()
            .onGeoLocationStatus(context, source: source, status: false);

        await MyLocationService.openIOSSettings(context, source);

        return {
          "isError": true,
          "data": null,
          "isBlockedState": false,
          "permissionStatus": permissionStatus
        };
      } else {
        GamePlayAnalytics()
            .onGeoLocationStatus(context, source: source, status: true);
      }
    }

    position = await MyLocationService.getLongLatValues(context, false, source);

    if (position != null && position["bAccessGiven"] != false) {
      return {
        "isError": false,
        "data": {
          "location": {
            "lat": position["latitude"],
            "lng": position["longitude"]
          },
          "accuracy": position["accuracy"]
        }
      };
    }

    Loader().showLoader(false);
    return {"isError": true, "data": null};
  }
}
