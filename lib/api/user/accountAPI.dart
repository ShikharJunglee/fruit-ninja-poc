import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class AccountAPI {
  getAccountDetails(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.ACCOUNT_DETAILS.toString(),
      ),
    );
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        Map<String, dynamic> response = json.decode(res.body);
        return {
          "error": false,
          "data": response["data"],
        };
      } else {
        return {
          "error": true,
        };
      }
    });
  }
}
