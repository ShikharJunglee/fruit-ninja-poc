import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class ProfileAPI {
  updateTeamName(BuildContext context, String teamName) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "PUT",
      Uri.parse(
        config.apiUrl + AppUrl.CHANGE_TEAM_NAME.toString(),
      ),
    );
    req.body = json.encode({
      "username": teamName,
    });
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        return {
          "error": false,
        };
      } else {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "error" && response["data"] != null) {
          return {
            "error": true,
            "message": response["data"]["errorMessage"],
          };
        } else {
          return {
            "error": true,
            "message": "Getting Error whille updating username."
          };
        }
      }
    });
  }

  updateProfile(BuildContext context, Map<String, dynamic> payload) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "PUT",
      Uri.parse(
        config.apiUrl + AppUrl.UPDATE_USER_PROFILE.toString(),
      ),
    );
    req.body = json.encode(payload);
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        return {
          "error": false,
          "data": response["data"],
        };
      } else {
        if (response["type"] == "error" && response["data"] != null) {
          return {
            "error": true,
            "message": response["data"]["errorMessage"],
          };
        } else {
          return {
            "error": true,
            "message": "Getting Error whille updating profile."
          };
        }
      }
    });
  }

  getStateList(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.GET_STATE_LIST.toString(),
      ),
    );
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        Map<String, dynamic> response = json.decode(res.body);
        return {
          "error": false,
          "data": response["data"],
        };
      } else {
        return {
          "error": true,
        };
      }
    });
  }

  updateStateDob(BuildContext context, {String dob, String state}) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "POST",
      Uri.parse(
        config.apiUrl + AppUrl.UPDATE_STATE_DOB.toString(),
      ),
    );
    req.body = json.encode({"dob": dob, "state": state});

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 300) {
        return {"success": true};
      } else {
        return {"success": false};
      }
    });
  }

  getAvatars(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.GET_AVATAR_LIST.toString(),
      ),
    );
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        Map<String, dynamic> response = json.decode(res.body);
        return {
          "error": false,
          "data": response["data"],
        };
      } else {
        return {
          "error": true,
        };
      }
    });
  }

  updateAvatar(BuildContext context, Map<String, dynamic> payload) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "POST",
      Uri.parse(
        config.apiUrl + AppUrl.UPADATE_AVATAR.toString(),
      ),
    );
    req.body = json.encode(payload);
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        Map<String, dynamic> response = json.decode(res.body);
        return {
          "error": false,
          "data": response["data"],
        };
      } else {
        return {
          "error": true,
        };
      }
    });
  }
}
