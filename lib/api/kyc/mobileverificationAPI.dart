import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class MobileVerificationAPI {
  Future<Map<String, dynamic>> sendOTP(BuildContext context,
      {String mobile, bool shouldUpdate}) {
    AppConfig config = Provider.of(context, listen: false);

    http.Request req = http.Request("POST",
        Uri.parse(config.apiUrl + AppUrl.SEND_VERIFICATION_OTP.toString()));
    req.body = json.encode({
      "phone": mobile,
      "isChanged": shouldUpdate,
    });
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "success") {
        return {"error": false};
      } else {
        return {
          "error": true,
          "message": response["data"]["errorMessage"],
        };
      }
    });
  }

  Future<Map<String, dynamic>> verifyOTP(BuildContext context, {String otp}) {
    AppConfig config = Provider.of(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.VERIFY_OTP.toString()));
    req.body = json.encode({
      "otp": otp,
    });
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "success") {
        return {
          "error": false,
        };
      } else {
        return {
          "error": true,
          "message": response["data"]["errorMessage"],
        };
      }
    });
  }

  Future<Map<String, dynamic>> resendOTP(BuildContext context,
      {@required String mobile}) async {
    AppConfig config = Provider.of(context, listen: false);

    Map<String, dynamic> _payload = {
      "mobile": mobile,
      "context": {
        "channelId": HttpManager.channelId,
      }
    };

    http.Request req = http.Request("POST",
        Uri.parse(config.apiUrl + AppUrl.RESEND_VERIFICATION_OTP.toString()));
    req.body = json.encode(_payload);
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          return {"error": false};
        } else {
          return {"error": true};
        }
      },
    );
  }
}
