import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class WithdrawAPI {
  Future<Map<String, dynamic>> authWithdraw(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET", Uri.parse(config.apiUrl + AppUrl.AUTH_WITHDRAW.toString()));
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> authWithdrawUS(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET", Uri.parse(config.apiUrl + AppUrl.AUTH_WITHDRAW_US.toString()));
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> makeWithdrawRequest(
    BuildContext context, {
    int withdrawType,
    @required double amount,
    @required String name,
    @required bool hasBankDetails,
    @required Map<String, dynamic> bankDetails,
    @required vpaID,
    @required bool hasVPAID,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.WITHDRAW.toString()));
    req.body = json.encode({
      "amount": amount,
      "name": name,
      "bankDetails": bankDetails,
      "hasBankDetails": hasBankDetails,
      "withdrawType": withdrawType,
      "hasVPAID": hasVPAID,
      "vpaID": vpaID
    });

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) async {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          response["error"] = false;
          return {
            "error": false,
            ...response["data"],
          };
        } else {
          Map<String, dynamic> response = json.decode(res.body);
          response["errorMessage"] = response["data"]["errorMessage"];
          response["error"] = true;

          return {
            "errorMessage": response["data"]["errorMessage"],
            "error": true,
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> makeWithdrawRequestUS(
    BuildContext context, {
    int withdrawType,
    @required double amount,
    @required bool hasBankDetails,
    @required Map<String, dynamic> bankDetails,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.WITHDRAW_US.toString()));
    req.body = json.encode({
      "amount": amount,
      "bankDetails": bankDetails,
      "hasBankDetails": hasBankDetails,
      "withdrawType": withdrawType,
    });

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) async {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          response["error"] = false;
          return {
            "error": false,
            ...response["data"],
          };
        } else {
          Map<String, dynamic> response = json.decode(res.body);
          response["errorMessage"] = response["data"]["errorMessage"];
          response["error"] = true;

          return {
            "errorMessage": response["data"]["errorMessage"],
            "error": true,
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> makeWithdrawRequestTsevo(
    BuildContext context, {
    int withdrawType,
    @required double amount,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.WITHDRAW_TSEVO.toString()));
    req.body = json.encode({
      "amount": amount,
      "withdrawType": withdrawType,
    });

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) async {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          response["error"] = false;
          return {
            "error": false,
            ...response["data"],
          };
        } else {
          Map<String, dynamic> response = json.decode(res.body);
          response["errorMessage"] = response["data"]["errorMessage"];
          response["error"] = true;

          return {
            "errorMessage": response["data"]["errorMessage"],
            "error": true,
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> submitSSN(BuildContext context, String ssn) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.SUBMIT_SSN.toString()));
    Map<String, dynamic> payload = {
      "ssn": ssn,
    };
    req.body = json.encode(payload);
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> getTotalWithdraw(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET", Uri.parse(config.apiUrl + AppUrl.TOTAL_WITHDRAW.toString()));

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> getTsevoWithdrawStatus(
      BuildContext context, int id) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request("GET",
        Uri.parse(config.apiUrl + AppUrl.TSEVO_WITHDRAW_STATUS.toString()));

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }
}

class AuthStatusCode {
  static const REG_CLOSED = -1;
  static const ACCOUNT_BLOCKED = 1;
  static const ACCOUNT_CLOSED = 2;
  static const MISSING_DOB = 3;
  static const AGE_UNDER_18 = 4;
  static const BLOCKED_STATE = 5;
  static const NOT_VERIFIED = 6;
  static const NOT_IN_INDIA = 11;
  static const INSUFFICIENT_FUND = 12;
  static const INSUFFICIENT_CASH_FUND = -7;
  static const NOT_ALLOWED = 13;
  static const CONTEST_ALREADY_JOINED = -10;
  static const PAN_REQUIRED = 14;
}
