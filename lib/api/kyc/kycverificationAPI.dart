import 'dart:io';
import 'dart:convert';
import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:path/path.dart' as path;
import 'package:http_parser/http_parser.dart';

class KYCVerificationAPI {
  Future<Map<String, dynamic>> getAddressList(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.KYC_DOC_LIST.toString(),
      ),
    );
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "success") {
        List<dynamic> addressList = response["data"];
        return {
          "error": false,
          "addressList": addressList,
        };
      } else {
        return {
          "error": true,
        };
      }
    });
  }

  onSendPan(BuildContext context, {@required String panNumber}) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST",
        Uri.parse(config.apiUrl +
            AppUrl.SUBMIT_PAN_NUMBER.toString() +
            "$panNumber"));
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "success") {
        return response["data"];
      } else {
        return {"error": true, "msg": "error submitting pan card"};
      }
    });
  }

  getVerificationStatus(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;

    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.VERIFICATION_STATUS.toString(),
      ),
    );
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      if (res.statusCode >= 200 && res.statusCode <= 299) {
        Map<String, dynamic> response = json.decode(res.body);
        var status = {
          'pan_verification': response["pan_verification"],
          'address_verification': response["address_verification"],
          'email_verification': response["email_verification"],
          'mobile_verification': response["mobile_verification"],
          'kyc_remark': response["kyc_remark"],
          'pan_remark': response["pan_remark"],
        };
        verificationStatus.setVerificationStatus(status);
      }
    });
  }

  onUploadDocuments(
    BuildContext context, {
    String addressDocType,
    @required File addressImage,
    File addressBackImage,
  }) async {
    var addressBackCopyLength;
    var addressBackCopyStream;

    AppConfig config = Provider.of(context, listen: false);

    var uriAddressUpload = Uri.parse(
        config.apiUrl + AppUrl.UPLOAD_DOC_ADDRESS.toString() + addressDocType);

    var addressLength = await addressImage.length();
    var addressStream =
        http.ByteStream(DelegatingStream.typed(addressImage.openRead()));

    var httpRequestForAddress = http.MultipartRequest("POST", uriAddressUpload);
    httpRequestForAddress.files.add(
      http.MultipartFile('kyc', addressStream, addressLength,
          filename: path.basename(addressImage.path),
          contentType: MediaType('image', 'jpg')),
    );

    if (addressBackImage != null) {
      addressBackCopyLength = await addressBackImage.length();
      addressBackCopyStream =
          http.ByteStream(DelegatingStream.typed(addressBackImage.openRead()));
      httpRequestForAddress.files.add(http.MultipartFile(
          'kyc_back_copy', addressBackCopyStream, addressBackCopyLength,
          filename: path.basename(addressBackImage.path),
          contentType: MediaType('image', 'jpg')));
    }

    httpRequestForAddress.headers["cookie"] = HttpManager.cookie;

    return await httpRequestForAddress.send().then((onValue) async {
      return await http.Response.fromStream(onValue);
    }).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        Map<String, dynamic> responseData = {"error": true, "body": {}};
        if (response["type"] == "success") {
          responseData["error"] = false;
          responseData["body"]["addressVerification"] =
              response["data"]["address_verification"];
          responseData["body"]["address_verification_remark"] =
              response["data"]["address_verification_remark"];
        } else {
          responseData["error"] = true;
          if (response["data"]["errorCode"] != null) {
            responseData["message"] = response["data"]["errorCode"];
          } else if (response["message"] != null) {
            responseData["message"] = response["message"];
          } else {
            responseData["message"] = "Getting error while performing request.";
          }
        }

        return responseData;
      },
    );
  }

  getUserData(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.GET_USER_PROFILE.toString(),
      ),
    );
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          return {"error": false, "data": response["data"]};
        } else {
          return {
            "error": false,
            "message": "Getting error while getting verification status."
          };
        }
      },
    );
  }
}
