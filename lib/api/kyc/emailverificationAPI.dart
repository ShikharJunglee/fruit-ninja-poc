import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class EmailVerificationAPI {
  Future<Map<String, dynamic>> sendVerificationEmail(BuildContext context,
      {String email, bool shouldUpdate}) {
    AppConfig config = Provider.of(context, listen: false);

    http.Request req = http.Request("POST",
        Uri.parse(config.apiUrl + AppUrl.SEND_VERIFICATION_MAIL.toString()));
    req.body = json.encode({
      "email": email,
      "isChanged": shouldUpdate,
    });
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "success") {
        return {"error": false};
      } else {
        if (response["type"] == "error" && response["data"] != null) {
          return {
            "error": true,
            "message": response["data"]["errorMessage"],
          };
        } else {
          return {
            "error": true,
            "message": "Getting Error whille verifying email."
          };
        }
      }
    });
  }
}
