import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class EarnCashAPI {
  Future<Map<String, dynamic>> getReferalData(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET", Uri.parse(config.apiUrl + AppUrl.GET_RAF_DATA.toString()));
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<List> getExistingContacts(
      BuildContext context, List<String> arrPhoneNumbersToFilter) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request("POST",
        Uri.parse(config.apiUrl + AppUrl.GET_EXISTING_USERS.toString()));

    req.body = json.encode(arrPhoneNumbersToFilter);
    return await HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          if (res.body != null) {
            return jsonDecode(res.body)["data"];
          }
        }
        return null;
      },
    );
  }

  void saveInstalledApps(BuildContext context, List packages) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request("POST",
        Uri.parse(config.apiUrl + AppUrl.SAVE_INSTALLED_APPS.toString()));

    req.body = json.encode(packages);

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        bool bSuccess = res.statusCode >= 200 && res.statusCode <= 299;

        bSuccess
            ? print("Installed apps saved.")
            : print(
                "Installed apps saving failed. " + res.statusCode.toString());
      },
    );
  }

  saveContactsApi(BuildContext context, List arrContact, String refCode) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
        "POST",
        Uri.parse(config.apiUrl +
            AppUrl.SAVE_CONTACT_BOOK.toString() +
            "/" +
            refCode));

    req.body = json.encode(arrContact);

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        bool bSuccess = res.statusCode >= 200 && res.statusCode <= 299;

        bSuccess
            ? print("Phonebook saved.")
            : print("Phonebook saving failed. " + res.statusCode.toString());
      },
    );
  }

  Future<bool> sendInvitesApi(
      BuildContext context, List<String> arrSelectedNumbers, String refCode) {
    User user = Provider.of(context, listen: false);

    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST",
        Uri.parse(config.apiUrl +
            AppUrl.SEND_INVITE_VIA_SMS.toString() +
            "/" +
            refCode));

    req.body = json.encode({
      "mobileNums": arrSelectedNumbers,
      "firstName": user.address.firstName,
      "lastName": user.address.lastName == null ? "" : user.address.lastName,
      "email": user.email,
      "mobile": user.mobile,
    });

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        return res.statusCode >= 200 && res.statusCode <= 299;
      },
    );
  }
}
