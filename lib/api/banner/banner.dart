import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/banner/banner_zone.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class BannerAPI {
  Future<Map<String, dynamic>> getBanners(BuildContext context,
      {@required BannerZone zone}) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET",
        Uri.parse(
            config.apiUrl + AppUrl.GET_BANNERS.toString() + zone.toString()));

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (res.statusCode >= 200 && res.statusCode < 299) {
          if (response["type"] == "success") {
            return {
              "error": false,
              "data": response["data"],
              "message": response["message"]
            };
          } else {
            return {"error": true, "message": response["message"]};
          }
        } else {
          return {
            "error": true,
            "message": "Getting error while getting banners."
          };
        }
      },
    );
  }
}
