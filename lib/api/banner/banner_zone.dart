class BannerZone {
  final int _value;
  const BannerZone._internal(this._value);

  int get value => _value;

  static BannerZone from(int value) => BannerZone._internal(value);

  static const LOGIN_BOTTOM = const BannerZone._internal(1);
  static const LOGIN_TOP = const BannerZone._internal(2);
  static const LOBBY_TOP = const BannerZone._internal(3);
  static const DEALS = const BannerZone._internal(4);
  static const RAF_BANNERS = const BannerZone._internal(5);
  static const RAF_TESTINONIAL_BANNERS = const BannerZone._internal(6);

  @override
  String toString() {
    return _value.toString();
  }
}
