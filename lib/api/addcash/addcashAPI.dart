import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:solitaire_gold/api/geolocation/locationAPI.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class AddCashAPI {
  Future<Map<String, dynamic>> getUserDetails(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET", Uri.parse(config.apiUrl + AppUrl.AUTH_CHECK_URL.toString()));
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> getLatLong(BuildContext context) async {
    Map<String, dynamic> locationResult =
        await LocationAPI().getGPSLocation(context, source: null);
    if (locationResult != null && !locationResult["isError"]) {
      return locationResult["data"];
    }
    return null;
  }

  Future<Map<String, dynamic>> getAmountTiles(
    BuildContext context, {
    Map<String, dynamic> locationData,
  }) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET", Uri.parse(config.apiUrl + AppUrl.GET_AMOUNT_TILES.toString()));
    req.body = json.encode({
      "locationData": locationData != null ? locationData["data"] : null,
      "updateUserLocation": locationData != null
          ? locationData["updateUserLocationOnServer"]
          : false,
    });
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> validatePromo(
    BuildContext context, {
    @required int amount,
    @required String promoCode,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.VALIDATE_PROMO.toString()));
    req.body = json.encode({
      "amount": amount,
      "promoCode": promoCode,
      "channelId": HttpManager.channelId,
      "transaction_amount_in_paise": amount * 100,
    });
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "message": result["message"],
            "details": result["data"]["promoDetails"]
          };
        }
      },
    );
  }

  proceedToPaymentMode(BuildContext context,
      {@required int amount, String promo}) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "POST", Uri.parse(config.apiUrl + AppUrl.PAYMENT_MODE.toString()));
    req.body = json.encode({
      "amount": amount,
      "promoCode": promo,
      "channelId": HttpManager.channelId,
    });
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["error"] != null || response["type"] == "error") {
          return {
            "error": true,
            "message": response["message"] != null
                ? response["message"]
                : "Getting error while processing request",
          };
        } else {
          return {"error": false, "data": response["data"]};
        }
      },
    );
  }

  Future<Map<String, dynamic>> initRazorpayPayment(BuildContext context,
      {@required Map<String, dynamic> payload}) {
    int index = 0;
    String querParamString = "";

    payload.forEach((key, value) {
      if (index != 0) {
        querParamString += '&';
      }
      querParamString += key + '=' + value.toString();
      index++;
    });

    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
      "GET",
      Uri.parse(config.apiUrl +
          AppUrl.INIT_PAYMENT_RAZORPAY.toString() +
          querParamString),
    );

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "error") {
        return null;
      } else {
        return response["data"];
      }
    });
  }

  Future<Map<String, dynamic>> checkForPaymetStatus(BuildContext context,
      {@required Map<dynamic, dynamic> payload, String payMode}) {
    AppConfig config = Provider.of(context, listen: false);

    String url = config.apiUrl +
        AppUrl.SUCCESS_PAY.toString() +
        (payMode == "razorpay"
            ? "type_id=4&source=RAZORPAY"
            : "type_id=1&source=PAYTMWALLET");

    http.Request req = http.Request("POST", Uri.parse(url));
    req.body = json.encode(payload);
    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      Map<String, dynamic> responseData = response["data"];
      if (response["type"] == "success") {
        String status = responseData["authStatus"].toLowerCase();
        if (status == "declined" || status == "failed" || status == "fail") {
          Map<String, dynamic> paymentResponse = {
            "error": true,
          };
          if (responseData["orderId"] == null) {
            paymentResponse["message"] =
                "Payment cancelled please retry transaction. In case your money has been deducted, please contact customer support team!";
          } else {
            paymentResponse["data"] = responseData;
          }

          return paymentResponse;
        } else {
          return {
            "error": false,
            "data": responseData,
          };
        }
      }
      return null;
    });
  }

  Future<Map<String, dynamic>> checkStripePaymentStatus(
    BuildContext context, {
    @required String orderId,
    @required int amount,
    @required String status,
  }) {
    AppConfig config = Provider.of(context, listen: false);

    String url = config.apiUrl + AppUrl.STRIPE_PAYMENT_STATUS.toString();

    http.Request req = http.Request("POST", Uri.parse(url));

    req.body =
        json.encode({"id": orderId, "orderAmount": amount, "status": status});

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);

      if (response["type"] == "success") {
        Map<String, dynamic> responseData = response["data"];
        String status = responseData["authStatus"].toLowerCase();

        if (status == "declined" || status == "failed" || status == "fail") {
          Map<String, dynamic> paymentResponse = {
            "error": true,
          };
          if (responseData["orderId"] == null) {
            paymentResponse["message"] =
                "Payment cancelled please retry transaction. In case your money has been deducted, please contact customer support team!";
          } else {
            paymentResponse["data"] = responseData;
          }

          return paymentResponse;
        } else {
          return {
            "error": false,
            "data": responseData,
          };
        }
      }
      return null;
    });
  }

  Future<Map<String, dynamic>> checkTsevoStatus(
    BuildContext context, {
    @required String orderId,
    @required int amount,
    @required String status,
  }) {
    AppConfig config = Provider.of(context, listen: false);

    String url = config.apiUrl + AppUrl.TSEVO_PAYMENT_STATUS.toString();

    http.Request req = http.Request("POST", Uri.parse(url));

    req.body =
        json.encode({"id": orderId, "orderAmount": amount, "status": status});

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);

      if (response["type"] == "success") {
        Map<String, dynamic> responseData = response["data"];

        if (responseData["authStatus"] != null) {
          String status = responseData["authStatus"].toLowerCase();

          if (status == "declined" || status == "failed" || status == "fail") {
            Map<String, dynamic> paymentResponse = {
              "error": true,
            };
            if (responseData["orderId"] == null) {
              paymentResponse["message"] =
                  "Payment cancelled please retry transaction. In case your money has been deducted, please contact customer support team!";
            } else {
              paymentResponse["data"] = responseData;
            }

            return paymentResponse;
          } else {
            return {
              "error": false,
              "data": responseData,
            };
          }
        } else {
          Map<String, dynamic> paymentResponse = {
            "error": true,
            "pending": true,
            "message":
                "Transaction is pending. Please wait for some time and then check again in transaction history. If money does not get added to your account, please contact customer support team!"
          };

          return paymentResponse;
        }
      }

      return null;
    });
  }

  Future<Map<String, dynamic>> initPaytmNativePayment(BuildContext context,
      {@required Map<String, dynamic> payload}) {
    int index = 0;
    String querParamString = "";

    payload.forEach((key, value) {
      if (index != 0) {
        querParamString += '&';
      }
      querParamString += key + '=' + value.toString();
      index++;
    });

    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
      "GET",
      Uri.parse(config.apiUrl +
          AppUrl.INIT_PAYMENT_PAYTM.toString() +
          querParamString),
    );

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      if (response["type"] == "error") {
        return null;
      } else {
        List lst = response["data"]["gateways"];
        Map<String, dynamic> dataToSend = {
          "txyToken": response["data"]["txnToken"]
        };

        for (var i = 0; i < lst.length; i++) {
          if (lst[i]["id"] == "MID") {
            dataToSend["mid"] = lst[i]["value"];
          }
          if (lst[i]["id"] == "CUST_ID") {
            dataToSend["customerID"] = lst[i]["value"];
          }
          if (lst[i]["id"] == "ORDER_ID") {
            dataToSend["orderid"] = lst[i]["value"];
          }

          if (lst[i]["id"] == "CALLBACK_URL") {
            dataToSend["callbackurl"] = lst[i]["value"];
          }

          if (lst[i]["id"] == "WEBSITE") {
            dataToSend["website"] = lst[i]["value"];
          }

          if (lst[i]["id"] == "INDUSTRY_TYPE_ID") {
            dataToSend["industrialTypeID"] = lst[i]["value"];
          }

          if (lst[i]["id"] == "CHECKSUMHASH") {
            dataToSend["checkSumHash"] = lst[i]["value"];
          }
        }

        return dataToSend;
      }
    });
  }

  Future<Map<String, dynamic>> initStripePayment(BuildContext context,
      {@required Map<String, dynamic> payload}) {
    int index = 0;
    String querParamString = "";

    payload.forEach((key, value) {
      if (index != 0) {
        querParamString += '&';
      }
      querParamString += key + '=' + value.toString();
      index++;
    });

    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
      "GET",
      Uri.parse(config.apiUrl +
          AppUrl.INIT_PAYMENT_STRIPE.toString() +
          querParamString),
    );

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      print(res.body);
      if (response["type"] == "error") {
        return null;
      } else {
        return response["data"];
      }
    });
  }

  Future<Map<String, dynamic>> initTsevoPayment(BuildContext context,
      {@required Map<String, dynamic> payload}) {
    int index = 0;
    String querParamString = "";

    payload.forEach((key, value) {
      if (index != 0) {
        querParamString += '&';
      }
      querParamString += key + '=' + value.toString();
      index++;
    });

    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
      "GET",
      Uri.parse(config.apiUrl +
          AppUrl.INIT_PAYMENT_STRIPE.toString() +
          querParamString),
    );

    return HttpManager(http.Client())
        .sendRequest(req)
        .then((http.Response res) {
      Map<String, dynamic> response = json.decode(res.body);
      print(res.body);
      if (response["type"] == "error") {
        return null;
      } else {
        return response["data"];
      }
    });
  }
}
