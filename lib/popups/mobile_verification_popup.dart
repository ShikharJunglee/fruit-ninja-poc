import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/api/kyc/mobileverificationAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/screens/withdraw/verification/mobileverification.dart';
import 'package:provider/provider.dart';

class MobileVerificationPopup extends StatefulWidget {
  @override
  _MobileVerificationPopupState createState() =>
      _MobileVerificationPopupState();
}

class _MobileVerificationPopupState extends State<MobileVerificationPopup> {
  Timer _timer;
  int _currentTimeLapse;
  bool _bIsOTPSent = false;

  String errorMessage;

  FocusNode _mobileFocusNode;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController _otpController = TextEditingController();
  final TextEditingController _mobileController = TextEditingController();

  MobileVerificationAPI _mobileVerificationAPI = MobileVerificationAPI();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
    _mobileFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _mobileFocusNode.dispose();
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    _currentTimeLapse = OTP_RESEND_TIME;
    const oneSec = const Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_currentTimeLapse < 1) {
            timer.cancel();
          } else {
            _currentTimeLapse = _currentTimeLapse - 1;
          }
        },
      ),
    );
  }

  void setData(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    if (user.mobile != null) {
      _mobileController.text = user.mobile;
    }
  }

  void onSendOTP(BuildContext ccontext, String mobile) async {
    Map<String, dynamic> result = await _mobileVerificationAPI.sendOTP(
      context,
      mobile: _mobileController.text,
      shouldUpdate: _mobileController.text != mobile,
    );

    if (!result["error"]) {
      startTimer();
      setState(() {
        _bIsOTPSent = true;
      });
    } else {
      setState(() {
        errorMessage = result["message"];
      });
    }
  }

  void onResendOTP(BuildContext context) async {
    Map<String, dynamic> result = await _mobileVerificationAPI
        .resendOTP(context, mobile: _mobileController.text);

    if (!result["error"]) {
      startTimer();
      setState(() {
        _bIsOTPSent = true;
      });
    } else {
      setState(() {
        errorMessage = "Getting error while resending OTP.";
      });
    }
  }

  void onVerifyOTP() async {
    User user = Provider.of<User>(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;

    Map<String, dynamic> result = await _mobileVerificationAPI
        .verifyOTP(context, otp: _otpController.text);

    if (!result["error"]) {
      user.mobile = _mobileController.text;
      verificationStatus.updateMobileVerificationStatus(true);
      Navigator.of(context).pop({"success": true});
    } else {
      setState(() {
        errorMessage = result["message"];
      });
    }
  }

  void editNumber() {
    setState(() {
      _bIsOTPSent = false;
      _otpController.clear();
      _currentTimeLapse = OTP_RESEND_TIME;
      _timer.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }
    return CustomDialog(
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: CommonDialogFrame(
          titleAsset: "images/titles/verification.png",
          child: Form(
            key: formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 50.0,
                ),
                Container(
                  height: 140.0,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        child: SimpleTextBox(
                          labelText: "Mobile Number",
                          labelStyle: Theme.of(context)
                              .primaryTextTheme
                              .bodyText1
                              .copyWith(
                                color: Color.fromRGBO(214, 123, 16, 1),
                              ),
                          controller: _mobileController,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline5
                              .copyWith(
                                color: Colors.white,
                              ),
                          maxLength: 10,
                          color: Colors.transparent,
                          focusedBorderColor: Color.fromRGBO(214, 123, 16, 1),
                          borderColor: Color.fromRGBO(214, 123, 16, 1),
                          validator: (value) {
                            if (value.isEmpty || value.length < 10) {
                              return "Provide valid mobile number for verification";
                            }
                            return null;
                          },
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      if (_bIsOTPSent)
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 24.0,
                            right: 24.0,
                            top: 16.0,
                          ),
                          child: SimpleTextBox(
                            labelText: "Enter OTP",
                            labelStyle: Theme.of(context)
                                .primaryTextTheme
                                .bodyText1
                                .copyWith(
                                  color: Color.fromRGBO(214, 123, 16, 1),
                                ),
                            controller: _otpController,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline5
                                .copyWith(
                                  color: Colors.white,
                                ),
                            maxLength: 4,
                            suffixIcon: CupertinoButton(
                              child: Text(
                                "Resend ${_currentTimeLapse > 0 ? _currentTimeLapse : ''}",
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline6
                                    .copyWith(
                                      color: Color.fromRGBO(214, 123, 16, 1),
                                      fontWeight: FontWeight.w300,
                                      fontStyle: FontStyle.italic,
                                      decoration: TextDecoration.underline,
                                      decorationStyle:
                                          TextDecorationStyle.solid,
                                    ),
                              ),
                              onPressed: _currentTimeLapse > 0
                                  ? null
                                  : () {
                                      onResendOTP(context);
                                    },
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter OTP.';
                              }
                              if (value.length < 4) {
                                return 'Invalid';
                              }
                              return null;
                            },
                            color: Colors.transparent,
                            focusedBorderColor: Color.fromRGBO(214, 123, 16, 1),
                            borderColor: Color.fromRGBO(214, 123, 16, 1),
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.number,
                          ),
                        ),
                    ],
                  ),
                ),
                if (errorMessage != null && errorMessage.length > 0)
                  Padding(
                    padding: const EdgeInsets.only(
                        bottom: 16.0, left: 28.0, right: 28.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            errorMessage,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .subtitle1
                                .copyWith(
                                  color: Colors.red.shade400,
                                ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "You will receive OTP on this number",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Colors.white,
                            ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            bottom: 16.0, top: 24.0, left: 80, right: 80),
                        child: Button(
                          text: _bIsOTPSent ? "SUBMIT" : "GET OTP",
                          style: ctaStyle,
                          onPressed: () {
                            User user =
                                Provider.of<User>(context, listen: false);
                            setState(() {
                              errorMessage = "";
                            });
                            if (formKey.currentState.validate()) {
                              if (_bIsOTPSent) {
                                onVerifyOTP();
                              } else {
                                onSendOTP(context, user.mobile);
                              }
                            }
                          },
                          size: ButtonSize.medium,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
