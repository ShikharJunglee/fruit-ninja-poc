import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/api/contactus/contactusAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class ReportDialog extends StatefulWidget {
  final int matchId;
  final int gameId;
  final String source;

  ReportDialog({
    @required this.matchId,
    @required this.gameId,
    @required this.source,
  });

  @override
  _ReportDialogState createState() => _ReportDialogState();
}

class _ReportDialogState extends State<ReportDialog> {
  final FocusNode emailFocusNode = FocusNode();
  final FocusNode descriptionFocusNode = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _description = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _categoryController = TextEditingController();
  final TextEditingController _subCategoryController = TextEditingController();

  bool categoryError = false;
  bool subCategoryError = false;

  ContactUsAPI _contactUsAPI = ContactUsAPI();

  List<dynamic> categoryData = [];
  List<DropdownMenuItem> categoryList = [];
  List<DropdownMenuItem> subCategoryList = [];
  List<dynamic> subCategoryData = [];

  String selectedCategoryIndex;
  String selectedSubCategoryIndex;

  String categoryLabel = "Select Category";
  String subCategoryLabel = "Select Subcategory";

  int descriptionMinChar = 10;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => setData());
    super.initState();
  }

  setData() {
    User user = Provider.of(context, listen: false);
    _emailController.text = user.email;
    _getFormDetails(context);
    LobbyAnalytics().onRapLoaded(
      context,
      source: widget.source,
      email: user.email,
      matchId: widget.matchId,
    );
  }

  _getFormDetails(BuildContext context) async {
    final result = await _contactUsAPI.getContactUsData(context);

    Loader().showLoader(false);

    if (result["error"] != true) {
      setState(() {
        categoryData = result['data'] != null ? result['data'] : [];
        categoryList =
            getCategories(result['data'] != null ? result['data'] : []);
      });
    }
  }

  @override
  void dispose() {
    _description.dispose();
    _emailController.dispose();
    _categoryController.dispose();
    _subCategoryController.dispose();
    emailFocusNode.dispose();
    descriptionFocusNode.dispose();
    subCategoryData = [];
    categoryData = [];
    categoryList = [];
    subCategoryList = [];
    super.dispose();
  }

  List<DropdownMenuItem> getCategories(List<dynamic> categoriesData) {
    List<DropdownMenuItem> listCategories = [];
    if (categoriesData != null && categoriesData.length > 0) {
      for (var i = 0; i < categoriesData.length; i++) {
        Map<String, dynamic> category = categoriesData[i];
        listCategories.add(
          DropdownMenuItem(
              child: Container(
                width: 140.0,
                child: Text(
                  category["id"],
                  style: TextStyle(fontSize: 16),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              value: i.toString()),
        );
      }
    }
    return listCategories;
  }

  setSubCategoriesListData(index) {
    Map<String, dynamic> selectedCategoryData = categoryData[int.parse(index)];
    List<dynamic> subCategoryData = selectedCategoryData["subcategories"];
    List<DropdownMenuItem> listSubCategories = [];
    for (var i = 0; i < subCategoryData.length; i++) {
      Map<String, dynamic> data = subCategoryData[i];
      listSubCategories.add(DropdownMenuItem(
        child: Text(data["id"]),
        value: i.toString(),
      ));
    }
    setState(() {
      this.subCategoryData = subCategoryData;
      subCategoryList = listSubCategories;
    });
  }

  bool isEmailValid(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  submitForm(BuildContext context) async {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }   
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }

    User user = Provider.of(context, listen: false);
    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> result = await _contactUsAPI.submitReportProblem(
      context,
      category: categoryData[int.parse(selectedCategoryIndex)]["id"],
      subCategory: subCategoryData[int.parse(selectedSubCategoryIndex)]["id"],
      description: _description.text,
      email: _emailController.text,
      mobile: user.mobile != null ? user.mobile : "",
      matchId: widget.matchId,
      gameId: widget.gameId,
    );

    Loader().showLoader(false);

    if (result["error"] == true) {
      // showMessageOnTop(context,
      //     msg: "Unable to process request. Please try again...!");

    } else {
      FocusScope.of(context).unfocus();
      await showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (context) => CustomDialog(
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          dialog: Dialog(
            backgroundColor: Colors.transparent,
            child: CommonDialogFrame(
              canClose: true,
              titleAsset: "images/titles/success.png",
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 48.0, left: 48.0, right: 48.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "Request received",
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Allow us to revert in 48 hrs",
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 24.0, bottom: 12.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            constraints: BoxConstraints(maxWidth: 180.0),
                            child: Button(
                              text: "OK",
                              style: ctaStyle,
                              size: ButtonSize.medium,
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );

      FocusScope.of(context).unfocus();
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }
    return CustomDialog(
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: CommonDialogFrame(
          onClose: () {
            LobbyAnalytics().onRapClosed(
              context,
              source: widget.source,
              matchId: widget.matchId,
              email: _emailController.text,
            );
          },
          titleAsset: "images/titles/report-a-problem.png",
          textPositionFromTop: 42,
          letterSpacing: 2,
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    height: 40.0,
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 24.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 80.0,
                          child: SimpleTextBox(
                            labelText: "Enter Email",
                            labelStyle: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Color.fromRGBO(214, 123, 16, 1),
                                ),
                            controller: _emailController,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Colors.white,
                                ),
                            errorStyle: Theme.of(context)
                                .primaryTextTheme
                                .subtitle1
                                .copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  height: 0.6,
                                ),
                            color: Colors.transparent,
                            focusedBorderColor: Color.fromRGBO(214, 123, 16, 1),
                            borderColor: Color.fromRGBO(214, 123, 16, 1),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Required";
                              } else if (!isEmailValid(value)) {
                                return "Enter a valid Email";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.emailAddress,
                          ),
                        ),
                        Container(
                          height: 80.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                decoration: _categoryController.text == ""
                                    ? BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(4.0),
                                        border: Border.all(
                                          width: 1.0,
                                          color:
                                              Color.fromRGBO(214, 123, 16, 1),
                                        ),
                                      )
                                    : null,
                                child: Stack(
                                  alignment: Alignment.centerRight,
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 4.0),
                                      child: Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.white,
                                        size: 28.0,
                                      ),
                                    ),
                                    DropdownButton(
                                      items: categoryList,
                                      isExpanded: true,
                                      value: selectedCategoryIndex,
                                      hint: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 14.0, horizontal: 12.0),
                                        child: Text(
                                          categoryLabel,
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline6
                                              .copyWith(
                                                color: Colors.white,
                                              ),
                                        ),
                                      ),
                                      dropdownColor:
                                          Color.fromRGBO(69, 4, 15, 1),
                                      icon: Icon(Icons.keyboard_arrow_down),
                                      iconSize: 0,
                                      underline: Container(),
                                      iconEnabledColor: Colors.white,
                                      selectedItemBuilder:
                                          (BuildContext context) {
                                        return categoryList.map<Widget>((item) {
                                          return SimpleTextBox(
                                            labelText: categoryLabel,
                                            floatingLabelBehavior:
                                                FloatingLabelBehavior.always,
                                            enabled: false,
                                            controller: _categoryController,
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .headline6
                                                .copyWith(
                                                  color: Colors.white,
                                                ),
                                            errorStyle: Theme.of(context)
                                                .primaryTextTheme
                                                .subtitle1
                                                .copyWith(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  height: 0.6,
                                                ),
                                            labelStyle: Theme.of(context)
                                                .primaryTextTheme
                                                .headline6
                                                .copyWith(color: Colors.white
                                                    // Color.fromRGBO(214, 123, 16, 1),
                                                    ),
                                            color: Colors.transparent,
                                            focusedBorderColor:
                                                Color.fromRGBO(214, 123, 16, 1),
                                            disabledBorderColor:
                                                Color.fromRGBO(214, 123, 16, 1),

                                            // item.value["id"],
                                          );
                                        }).toList();
                                      },
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6
                                          .copyWith(
                                            color: Colors.white,
                                          ),
                                      onChanged: (value) {
                                        LobbyAnalytics().onRapCategorySelect(
                                          context,
                                          source: widget.source,
                                          category:
                                              categoryData[int.parse(value)]
                                                  ["id"],
                                          matchId: widget.matchId,
                                        );
                                        setState(() {
                                          selectedCategoryIndex = value;
                                          _categoryController.text =
                                              categoryData[int.parse(value)]
                                                  ["id"];
                                          selectedSubCategoryIndex = null;
                                          _subCategoryController.text = "";
                                          setSubCategoriesListData(value);
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              if (categoryError)
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 12.0, top: 2.0),
                                  child: Text(
                                    "Required",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .subtitle1
                                        .copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          // height: 0.6,
                                        ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                        Container(
                          height: 80.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                decoration: _subCategoryController.text == ""
                                    ? BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(4.0),
                                        border: Border.all(
                                          width: 1.0,
                                          color:
                                              Color.fromRGBO(214, 123, 16, 1),
                                        ),
                                      )
                                    : null,
                                child: Stack(
                                  alignment: Alignment.centerRight,
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 4.0),
                                      child: Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.white,
                                        size: 28.0,
                                      ),
                                    ),
                                    DropdownButton(
                                      items: subCategoryList,
                                      isExpanded: true,
                                      value: selectedSubCategoryIndex,
                                      hint: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 14.0, horizontal: 12.0),
                                        child: Text(
                                          subCategoryLabel,
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline6
                                              .copyWith(
                                                color: Colors.white,
                                              ),
                                        ),
                                      ),
                                      dropdownColor:
                                          Color.fromRGBO(69, 4, 15, 1),
                                      icon: Icon(Icons.keyboard_arrow_down),
                                      iconSize: 0,
                                      underline: Container(),
                                      iconEnabledColor: Colors.white,
                                      selectedItemBuilder:
                                          (BuildContext context) {
                                        return subCategoryList
                                            .map<Widget>((item) {
                                          return SimpleTextBox(
                                            labelText: subCategoryLabel,
                                            floatingLabelBehavior:
                                                FloatingLabelBehavior.always,
                                            enabled: false,
                                            controller: _subCategoryController,
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .headline6
                                                .copyWith(
                                                  color: Colors.white,
                                                ),
                                            errorStyle: Theme.of(context)
                                                .primaryTextTheme
                                                .subtitle1
                                                .copyWith(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  height: 0.6,
                                                ),
                                            labelStyle: Theme.of(context)
                                                .primaryTextTheme
                                                .headline6
                                                .copyWith(color: Colors.white
                                                    // Color.fromRGBO(214, 123, 16, 1),
                                                    ),
                                            color: Colors.transparent,
                                            focusedBorderColor:
                                                Color.fromRGBO(214, 123, 16, 1),
                                            disabledBorderColor:
                                                Color.fromRGBO(214, 123, 16, 1),
                                          );
                                        }).toList();
                                      },
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6
                                          .copyWith(
                                            color: Colors.white,
                                          ),
                                      onChanged: (value) {
                                        LobbyAnalytics().onRapSubCategorySelect(
                                          context,
                                          source: widget.source,
                                          subCategory:
                                              subCategoryData[int.parse(value)]
                                                  ["id"],
                                          matchId: widget.matchId,
                                        );
                                        setState(() {
                                          selectedSubCategoryIndex = value;
                                          _subCategoryController.text =
                                              subCategoryData[int.parse(value)]
                                                  ["id"];
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              if (subCategoryError)
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 12.0, top: 2.0),
                                  child: Text(
                                    "Required",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .subtitle1
                                        .copyWith(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          // height: 0.6,
                                        ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                        Container(
                          height: 146.0,
                          child: SimpleTextBox(
                            controller: _description,
                            isDense: true,
                            minLines: 4,
                            maxLines: 4,
                            labelText: "Describe your issue",
                            labelStyle: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Color.fromRGBO(214, 123, 16, 1),
                                ),
                            errorStyle: Theme.of(context)
                                .primaryTextTheme
                                .subtitle2
                                .copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                ),
                            focusNode: descriptionFocusNode,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Colors.white,
                                ),
                            color: Colors.transparent,
                            floatingLabelBehavior: FloatingLabelBehavior.always,
                            focusedBorderColor: Color.fromRGBO(214, 123, 16, 1),
                            borderColor: Color.fromRGBO(214, 123, 16, 1),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Required";
                              }
                              if (value.length < descriptionMinChar) {
                                return "Description minimum length should be $descriptionMinChar characters";
                              }
                              return null;
                            },
                            keyboardType: TextInputType.multiline,
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 56),
                                child: Button(
                                  text: "Submit".toUpperCase(),
                                  style: ctaStyle,
                                  size: ButtonSize.medium,
                                  onPressed: () {
                                    if (_categoryController.text == null ||
                                        _categoryController.text.isEmpty) {
                                      setState(() {
                                        categoryError = true;
                                      });
                                    } else {
                                      setState(() {
                                        categoryError = false;
                                      });
                                    }
                                    if (_subCategoryController.text == null ||
                                        _subCategoryController.text.isEmpty) {
                                      setState(() {
                                        subCategoryError = true;
                                      });
                                    } else {
                                      setState(() {
                                        subCategoryError = false;
                                      });
                                    }
                                    if (_formKey.currentState.validate()) {
                                      LobbyAnalytics().onRapSubmit(
                                        context,
                                        source: widget.source,
                                        email: _emailController.text,
                                        matchId: widget.matchId,
                                        category: _categoryController.text,
                                        subCategory:
                                            _subCategoryController.text,
                                      );
                                      submitForm(context);
                                    }
                                  },
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
