import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class GenericDialog extends StatelessWidget {
  final String title;
  final Text contentText;
  final String buttonText;
  final Function onPressed;
  final double letterSpacing;

  GenericDialog({
    this.title,
    this.contentText,
    this.buttonText,
    this.onPressed,
    this.letterSpacing = 2.3,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }
    return CustomDialog(
      padding: EdgeInsets.all(0.0),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            CommonDialogFrame(
              titleAsset: title == null ? "images/titles/alert.png" : title,
              letterSpacing: letterSpacing,
              child: Column(
                children: <Widget>[
                  Container(
                    padding:
                        EdgeInsets.only(top: 48.0, left: 24.0, right: 24.0),
                    alignment: Alignment.center,
                    constraints: BoxConstraints(
                      minHeight: 200.0,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(child: contentText),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 80.0),
                            constraints: BoxConstraints(maxHeight: 60.0),
                            child: Button(
                              text: buttonText.toUpperCase(),
                              style: ctaStyle,
                              onPressed: () {
                                onPressed();
                              },
                              size: ButtonSize.medium,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
