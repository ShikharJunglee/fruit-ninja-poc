import 'package:flutter/material.dart';

class CommonDialogFrame extends StatelessWidget {
  final double letterSpacing;
  final String titleAsset;
  final Widget child;
  final Widget footer;
  final bool canClose;
  final double textPositionFromTop;
  final Function onClose;

  CommonDialogFrame({
    this.child,
    this.titleAsset,
    this.letterSpacing = 2.6,
    this.footer,
    this.canClose = true,
    this.textPositionFromTop = 50.0,
    this.onClose,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    return Container(
      padding: EdgeInsets.all(8.0),
      constraints: BoxConstraints(
        maxWidth: width,
      ),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                top: titleAsset == null
                    ? 0
                    : screenWidth < 350
                        ? 32
                        : 42.0),
            child: Stack(
              alignment: Alignment.topRight,
              overflow: Overflow.visible,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Stack(
                        children: <Widget>[
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Flexible(
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.0),
                                    border: Border.all(
                                      color: Color.fromRGBO(97, 55, 2, 1),
                                      width: 2.0,
                                    ),
                                    color: Color.fromRGBO(69, 4, 15, 1),
                                  ),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(13.0),
                                      border: Border.all(
                                        color: Color.fromRGBO(122, 75, 3, 1),
                                        width: 2.0,
                                      ),
                                      color: Color.fromRGBO(69, 4, 15, 1),
                                    ),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        border: Border.all(
                                          color: Color.fromRGBO(196, 120, 2, 1),
                                          width: 2.0,
                                        ),
                                        gradient: LinearGradient(
                                          colors: [
                                            Color.fromRGBO(32, 1, 5, 1),
                                            Color.fromRGBO(83, 7, 18, 1)
                                          ],
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                        ),
                                      ),
                                      child: Container(
                                        width: width - 16.0,
                                        child: child,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              if (footer != null)
                                Container(
                                  constraints:
                                      BoxConstraints(maxWidth: maxWidth),
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 4.0,
                                    horizontal: 16.0,
                                  ),
                                  child: footer,
                                ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                if (canClose)
                  Positioned(
                    right: -18.0,
                    top: -20.0,
                    width: 48.0,
                    child: IconButton(
                      padding: EdgeInsets.all(0.0),
                      icon: Image.asset(
                        "images/icons/close2.png",
                        height: 36.0,
                      ),
                      onPressed: () {
                        if (onClose != null) {
                          onClose();
                        }
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
              ],
            ),
          ),
          if (titleAsset != null)
            Positioned(
              left: 0.0,
              top: 0.0,
              width: width - 16.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: <Widget>[
                        Container(
                          width: width * 0.65,
                          child: Image.asset(
                            titleAsset,
                            fit: BoxFit.scaleDown,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
        ],
      ),
    );
  }
}
