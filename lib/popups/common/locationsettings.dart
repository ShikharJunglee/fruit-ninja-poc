import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission/permission.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class LocationSettingsDialog extends StatelessWidget {
  final String source;

  LocationSettingsDialog({@required this.source});

  @override
  Widget build(BuildContext context) {
    GamePlayAnalytics().onGeoLocationAccessDeniedPopup(context, source: source);
    return CustomDialog(
      padding: EdgeInsets.all(0.0),
      dialog: Dialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.hardEdge,
        child: CommonDialogFrame(
          titleAsset: "images/titles/location-access.png",
          letterSpacing: 2.4,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: Container(
                  constraints: BoxConstraints(maxHeight: 250),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 32.0, vertical: 32.0),
                                child: Text(
                                  "Cash games are not allowed in certain states. Please turn on your device's GPS to verify your location",
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .headline6
                                      .copyWith(
                                        color: Colors.white,
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Container(
                height: 48.0,
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                ),
                margin: const EdgeInsets.only(bottom: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Button(
                      text: "Settings".toUpperCase(),
                      size: ButtonSize.medium,
                      onPressed: () async {
                        GamePlayAnalytics()
                            .onGeoLocationAccessDeniedPopupSettingsClicked(
                                context);
                        bool permissionSettingsPageOpened =
                            await Permission.openSettings();
                        if (permissionSettingsPageOpened) {
                          Navigator.of(context).pop();
                        }
                      },
                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                    Button(
                      text: "Cancel".toUpperCase(),
                      size: ButtonSize.medium,
                      type: ButtonType.secondary,
                      onPressed: () {
                        GamePlayAnalytics()
                            .onGeoLocationAccessDeniedPopupCancelClicked(
                                context);
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
