import 'package:flutter/material.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class ScoringSystem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: EdgeInsets.all(0),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: CommonDialogFrame(
          titleAsset: "images/titles/scoring-system.png",
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      gradient: LinearGradient(colors: [
                        Color.fromRGBO(27, 61, 43, 1),
                        Color.fromRGBO(18, 43, 29, 1),
                      ])),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(padding: EdgeInsets.only(top: 20)),
                      Flexible(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                child: FittedBox(
                                  fit: BoxFit.fill,
                                  child: Image.asset(
                                      "images/scoringsystem/card-tabs.png"),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                child: FittedBox(
                                  fit: BoxFit.fill,
                                  child: Image.asset(
                                      "images/scoringsystem/actions.png"),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Expanded(
                                child: FittedBox(
                                  fit: BoxFit.fill,
                                  child: Image.asset(
                                      "images/scoringsystem/time-bonus.png"),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
