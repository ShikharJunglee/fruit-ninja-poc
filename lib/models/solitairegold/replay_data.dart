class ReplayGame {
  bool isReplay;
  int matchId;
  int gameId;

  ReplayGame({
    this.isReplay,
    this.matchId,
    this.gameId,
  });

  factory ReplayGame.fromJson(Map<String, dynamic> json) {
    return ReplayGame(
      isReplay: json["isReplay"],
      matchId: json["matchId"],
      gameId: json["gameId"],
    );
  }

  Map<String, dynamic> toJson() => {
        "isReplay": this.isReplay,
        "matchId": this.matchId,
        "gameId": this.gameId,
      };
}
