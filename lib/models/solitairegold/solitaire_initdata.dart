import 'package:solitaire_gold/screens/solitairegold/solitaire_messages.dart';

class SolitaireInitData {
  MessageType type;
  String context;
  String cookie;
  String avatarUrl;
  String csBaseUrl;
  String lobbyBaseUrl;
  String replayServerBaseUrl;
  int userId;
  int channelId;
  Map<String, dynamic> gameTableConfig;

  SolitaireInitData({
    this.type = MessageType.Init,
    this.context,
    this.cookie,
    this.userId,
    this.channelId,
    this.avatarUrl,
    this.csBaseUrl,
    this.lobbyBaseUrl,
    this.replayServerBaseUrl,
    this.gameTableConfig,
  });

  factory SolitaireInitData.fromJson(Map<String, dynamic> json) {
    return SolitaireInitData(
      type: MessageType.Init,
      context: json["context"],
      cookie: json["cookie"],
      userId: json["userId"],
      channelId: json["channelId"],
      avatarUrl: json["avatarUrl"],
      csBaseUrl: json["serverBaseUrl"],
      lobbyBaseUrl: json["lobbyBaseUrl"],
      replayServerBaseUrl: json["replayServerBaseUrl"],
      gameTableConfig: json["gameTableConfig"],
    );
  }

  Map<String, dynamic> toJson() => {
        "type": this.type.getString(),
        "context": this.context,
        "cookie": this.cookie,
        "userId": this.userId,
        "channelId": this.channelId,
        "avatarUrl": this.avatarUrl,
        "lobbyBaseUrl": this.lobbyBaseUrl,
        "serverBaseUrl": this.csBaseUrl,
        "replayServerBaseUrl": this.replayServerBaseUrl,
        "gameTableConfig": this.gameTableConfig,
      };
}
