class GameStartData {
  bool isFTUE;
  bool isReplay;
  int tieMatchId;
  int tournamentId;
  int gameType;
  bool musicEnabled;

  GameStartData({
    this.isFTUE,
    this.isReplay,
    this.tieMatchId,
    this.tournamentId,
    this.gameType,
    this.musicEnabled,
  });

  factory GameStartData.fromJson(Map<String, dynamic> json) {
    return GameStartData(
      isFTUE: json["isFTUE"],
      isReplay: json["isReplay"],
      tieMatchId: json["tieMatchId"],
      tournamentId: json["tournamentId"],
      gameType: json["gameType"],
      musicEnabled: json["musicEnabled"],
    );
  }

  Map<String, dynamic> toJson() => {
        "isFTUE": this.isFTUE,
        "isReplay": this.isReplay,
        "tieMatchId": this.tieMatchId,
        "tournamentId": this.tournamentId,
        "gameType": this.gameType,
        "musicEnabled": this.musicEnabled,
      };
}
