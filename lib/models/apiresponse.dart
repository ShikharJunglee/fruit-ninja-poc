class ApiResponse {
  final String type;
  final bool success;
  final Map<String, dynamic> data;
  ApiResponse({this.type, this.data, this.success});

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
      type: json["type"],
      data: json["data"],
      success: json["type"].toLowerCase() == "success",
    );
  }
}
