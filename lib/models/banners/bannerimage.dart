class BannerImage {
  final int id;
  final String title;
  final String url;
  final String bannersLocation;
  final int rotationOrder;
  final String href;
  final String details;
  final int targetLocation;
  final bool enabled;

  BannerImage({
    this.id,
    this.title,
    this.url,
    this.bannersLocation,
    this.rotationOrder,
    this.href,
    this.details,
    this.targetLocation,
    this.enabled,
  });

  factory BannerImage.fromJson(Map<String, dynamic> json) {
    return BannerImage(
      id: json["id"],
      title: json["title"],
      url: json["url"],
      bannersLocation: json["bannersLocation"],
      rotationOrder: json["rotationOrder"],
      href: json["href"],
      details: json["details"],
      targetLocation: json["targetLocation"],
      enabled: json["enabled"],
    );
  }
}
