import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/banner/banner_zone.dart';

class BannerList with ChangeNotifier {
  Map<int, List<dynamic>> items = {};

  setItems(BannerZone zone, List<dynamic> banners) {
    items[zone.value] = banners;

    notifyListeners();
  }

  List<dynamic> getBannersFor(BannerZone zone) {
    return items[zone.value] == null ? [] : items[zone.value];
  }
}
