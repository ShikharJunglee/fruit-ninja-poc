import 'package:flutter/material.dart';

class InitData with ChangeNotifier {
  String wsUrl;
  ServerData serverData = ServerData();
  UpdatePolicy updatePolicy = UpdatePolicy();
  LocalizationData localizationData = LocalizationData();
  Experiments experiments = Experiments();
  Map<String, dynamic> locationStateRestrictions = {};
  String geoLocationKey;
  AnalyticsConfig analyticsConfig;

  copyFrom(Map<String, dynamic> json) {
    this.wsUrl = json["wsUrl"];
    this.serverData.copyFrom(json["serverData"]);
    this.updatePolicy.copyFrom(json["updatePolicy"]);
    this.localizationData.copyFrom(json["localizationData"]);
    this.experiments.copyFrom(json["experiments"]);
    this.locationStateRestrictions = json["locationStateRestrictions"];
    this.geoLocationKey = json["geoLocationKey"];
    this.analyticsConfig = AnalyticsConfig.fromJson(json["analytics"]);

    notifyListeners();
  }
}

class ServerData {
  String time;
  String avatarUrl;

  copyFrom(Map<String, dynamic> json) {
    this.time = json["time"];
    this.avatarUrl = json["avatarUrl"];
  }
}

class UpdatePolicy {
  bool update;
  bool forcedUpdate;
  String updateUrl;
  UpdatePopup updatePopup = UpdatePopup();

  copyFrom(Map<String, dynamic> json) {
    this.update = json["update"];
    this.forcedUpdate = json["forcedUpdate"];
    this.updateUrl = json["updateUrl"];
    this.updatePopup.copyFrom(json["updatePopup"]);
  }
}

class UpdatePopup {
  int interval;
  int counter;
  String header;
  List<dynamic> logs;

  copyFrom(Map<String, dynamic> json) {
    this.interval = json["interval"];
    this.counter = json["counter"];
    this.header = json["header"];
    this.logs = json["logs"];
  }
}

class LocalizationData {
  String code;
  double version;
  String downloadLink;

  copyFrom(Map<String, dynamic> json) {
    this.code = json["code"];
    this.version = json["version"];
    this.downloadLink = json["downloadLink"];
  }
}

class Experiments {
  SignUpConfig signUpConfig = SignUpConfig();
  WelcomeBackConfig welcomeBackConfig = WelcomeBackConfig();
  List<StaticPageConfig> staticPages = List<StaticPageConfig>();
  List<FooterConfig> footers = List<FooterConfig>();
  List<MenuConfig> menuItems = List<MenuConfig>();
  MiscConfig miscConfig = MiscConfig();
  ReplayConfigs replayConfigs = ReplayConfigs();
  Map<String, dynamic> gameTableConfig = Map();

  copyFrom(Map<String, dynamic> json) {
    this.signUpConfig.copyFrom(json["signUpData"]);
    this.welcomeBackConfig.copyFrom(json["welcomeBonusConfig"]);
    this.staticPages = (json["staticPages"] as List<dynamic>)
        .map((f) => StaticPageConfig.fromJson(f))
        .toList();
    this.footers = (json["footerItemConfig"] as List<dynamic>)
        .map((f) => FooterConfig.fromJson(f))
        .toList();

    this.menuItems = (json["accountConfigs"] as List<dynamic>)
        .map((f) => MenuConfig.fromJson(f))
        .toList();
    this.miscConfig.copyFrom(json["miscConfigs"]);
    this.replayConfigs.copyFrom(json["replayConfigs"]);
    this.gameTableConfig = json["gameTableConfig"];
  }

  int getAddCashIndex() {
    for (FooterConfig footer in this.footers) {
      if (footer.name == "ADDCASH") {
        return footer.index - 1;
      }
    }
    return 0;
  }

  int getLobbyIndex() {
    for (FooterConfig footer in this.footers) {
      if (footer.name == "LOBBY") {
        return footer.index - 1;
      }
    }
    return 2;
  }
}

class SignUpConfig {
  int emailSwitch;
  int socialSwitch;
  int resendTime;
  int resendPasswordCount;
  int maxResendCount;
  int maxResendTime;
  bool appleLoginEnabled;

  copyFrom(Map<String, dynamic> json) {
    this.emailSwitch = json["emailSwitch"];
    this.socialSwitch = json["socialSwitch"];
    this.resendTime = json["resendTime"];
    this.resendPasswordCount = json["resendPasswordCount"];
    this.maxResendCount = json["maxResendCount"];
    this.maxResendTime = json["maxResendTime"];
    this.appleLoginEnabled = json["appleLoginEnabled"];
  }
}

class WelcomeBackConfig {
  bool welcomeBonusEnabled;

  copyFrom(Map<String, dynamic> json) {
    this.welcomeBonusEnabled = json["welcomeBonusEnabled"];
  }
}

class StaticPageConfig {
  String name;
  StaticPageData data = StaticPageData();

  StaticPageConfig();

  factory StaticPageConfig.fromJson(Map<String, dynamic> json) {
    StaticPageConfig staticPageConfig = StaticPageConfig();
    staticPageConfig.copyFrom(json);
    return staticPageConfig;
  }
  copyFrom(Map<String, dynamic> json) {
    this.name = json["name"];
    this.data.copyFrom(json["data"]);
  }
}

class StaticPageData {
  String title;
  String url;

  copyFrom(Map<String, dynamic> json) {
    this.title = json["title"];
    this.url = json["url"];
  }
}

class FooterConfig {
  String name;
  int index;

  FooterConfig();

  factory FooterConfig.fromJson(Map<String, dynamic> json) {
    FooterConfig footerConfig = FooterConfig();
    footerConfig.copyFrom(json);
    return footerConfig;
  }
  copyFrom(Map<String, dynamic> json) {
    this.name = json["name"];
    this.index = json["index"];
  }
}

class MenuConfig {
  String icon;
  String title;
  String cta;
  String staticLink;
  int type;
  int visibilityMod;
  List<int> visibilityRemainders;

  MenuConfig();

  factory MenuConfig.fromJson(Map<String, dynamic> json) {
    MenuConfig menuConfig = MenuConfig();
    menuConfig.copyFrom(json);
    return menuConfig;
  }
  copyFrom(Map<String, dynamic> json) {
    this.title = json["title"];
    this.icon = json["icon"];
    this.cta = json["cta"];
    this.staticLink = json["staticLink"];
    this.type = json["type"];
    this.visibilityMod = json["visibilityMod"];
    this.visibilityRemainders = json["visibilityRemainders"];
  }

  Map<String, dynamic> toJson() => {
        "title": title,
        "icon": icon,
        "cta": cta,
        "staticLink": staticLink,
        "type": type,
        "visibilityMod": visibilityMod,
        "visibilityRemainders": visibilityRemainders,
      };
}

class MiscConfig {
  bool vibrate;
  bool spotsLeft;
  int replayStepSkipper;
  int pingCount;
  int updateAccountDurationInMs;
  bool stateEnabled;
  int amountPrecision;
  bool musicEnabled;

  MiscConfig();

  factory MiscConfig.fromJson(Map<String, dynamic> json) {
    MiscConfig miscConfig = MiscConfig();
    miscConfig.copyFrom(json);
    return miscConfig;
  }

  copyFrom(Map<String, dynamic> json) {
    this.vibrate = json["vibrate"];
    this.spotsLeft = json["spotsLeft"];
    this.replayStepSkipper = json["replayStepSkipper"];
    this.pingCount = json["pingCount"];
    this.updateAccountDurationInMs = json["updateAccountDurationInMs"] ?? 30000;
    this.stateEnabled = json["stateEnabled"] ?? true;
    this.amountPrecision = json["amountPrecision"] ?? 2;
    this.musicEnabled = json["musicEnabled"] ?? true;
  }

  Map<String, dynamic> toJson() => {
        "vibrate": vibrate,
        "spotsLeft": spotsLeft,
        "replayStepSkipper": replayStepSkipper,
        "pingCount": pingCount,
        "updateAccountDurationInMs": updateAccountDurationInMs,
        "stateEnabled": stateEnabled,
        "amountPrecision": amountPrecision,
      };
}

class ReplayConfigs {
  String url;

  copyFrom(Map<String, dynamic> json) {
    if (json != null) {
      this.url = json["url"];
    }
  }
}

class AnalyticsConfig {
  final String analyticsURL;
  final int visitTimeout;
  final bool analyticsEnabled;
  final int analyticsSendInterval;

  AnalyticsConfig({
    this.analyticsURL,
    this.visitTimeout,
    this.analyticsEnabled,
    this.analyticsSendInterval,
  });

  factory AnalyticsConfig.fromJson(Map<String, dynamic> json) {
    return AnalyticsConfig(
      analyticsURL: json["analyticsURL"],
      visitTimeout: json["visitTimeout"],
      analyticsEnabled: json["analyticsEnabled"],
      analyticsSendInterval: json["analyticsSendInterval"],
    );
  }

  Map<String, dynamic> toJson() => {
        "analyticsURL": analyticsURL,
        "visitTimeout": visitTimeout,
        "analyticsEnabled": analyticsEnabled,
        "analyticsSendInterval": analyticsSendInterval,
      };
}
