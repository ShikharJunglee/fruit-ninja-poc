import 'package:solitaire_gold/models/profile/userbalance.dart';

class Account {
  UserBalance balance;
  List<Transaction> recentTransactions;

  Account({
    this.balance,
    this.recentTransactions,
  });

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
      balance: UserBalance.fromJson(json["balance"]),
      recentTransactions: (json["summary"] as List)
          .map((i) => Transaction.fromJson(i))
          .toList(),
    );
  }
}

class Transaction {
  int id;
  bool debit;
  String date;
  String type;
  String txnId;
  double amount;
  double userBalance;
  String bucket;
  List<String> filterTypes;
  bool isGoldbarTransaction;
  int txnHead;

  Transaction({
    this.id,
    this.date,
    this.type,
    this.debit,
    this.txnId,
    this.amount,
    this.userBalance,
    this.bucket,
    this.filterTypes,
    this.isGoldbarTransaction,
    this.txnHead,
  });

  factory Transaction.fromJson(Map<String, dynamic> json) {
    return Transaction(
      id: json["id"],
      date: json["date"],
      type: json["type"],
      debit: json["debit"],
      txnId: json["txnId"],
      bucket: json["bucket"],
      amount: (json["amount"]).toDouble(),
      userBalance: (json["userBalance"]).toDouble(),
      filterTypes: (json["transactionFilterType"] as List)
          .map((i) => i.toString())
          .toList(),
      isGoldbarTransaction: json["isGoldbarTransaction"],
      txnHead: json["txnHead"],
    );
  }
}
