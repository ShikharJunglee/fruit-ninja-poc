class PaymentDetails {
  int minAmount;
  int depositLimit;
  bool isFirstDeposit;
  List<int> amountTiles;
  List<int> hotTiles;
  List<int> bestTiles;
  List<dynamic> bonusArray;
  List<dynamic> lastPaymentArray;
  int defaultAmount;

  PaymentDetails({
    this.minAmount,
    this.bonusArray,
    this.amountTiles,
    this.hotTiles,
    this.bestTiles,
    this.depositLimit,
    this.isFirstDeposit,
    this.lastPaymentArray,
    this.defaultAmount,
  });

  factory PaymentDetails.fromJson(Map<String, dynamic> json) {
    bool isFirstDeposit =
        json["isFirstDeposit"] == null ? false : json["isFirstDeposit"];
    return PaymentDetails(
      minAmount: json["minAmount"],
      bonusArray: json["bonusArray"],
      depositLimit: json["depositLimit"],
      lastPaymentArray: json["lastPaymentArray"],
      isFirstDeposit: isFirstDeposit,
      amountTiles:
          (json["amountTiles"] as List).map((i) => (i as int).toInt()).toList(),
      hotTiles:
          (json["hotTiles"] as List).map((i) => (i as int).toInt()).toList(),
      bestTiles: json["bestTiles"] == null
          ? []
          : (json["bestTiles"] as List).map((i) => (i as int).toInt()).toList(),
      defaultAmount:
          json["defaultAmount"] == null ? 500 : json["defaultAmount"],
    );
  }
}
