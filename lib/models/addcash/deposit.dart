import 'package:solitaire_gold/models/addcash/paymentdetails.dart';

class Deposit {
  String bannerImage;
  bool bAllowRepeatDeposit;
  bool showLockedAmount;
  PaymentDetails paymentDetails;

  Deposit({
    this.bannerImage,
    this.paymentDetails,
    this.bAllowRepeatDeposit,
    this.showLockedAmount = false,
  });

  factory Deposit.fromJson(Map<String, dynamic> json) {
    bool isFirstDeposit = json["chooseAmountData"]["isFirstDeposit"] == null
        ? false
        : json["chooseAmountData"]["isFirstDeposit"];

    json["chooseAmountData"]["autoApplyPromo"] =
        json["chooseAmountData"]["addCashPromoAb"] == 0
            ? json["refreshData"]["user_id"] % 2 != 1
            : (json["chooseAmountData"]["addCashPromoAb"] == 2
                ? isFirstDeposit
                : false);

    return Deposit(
      paymentDetails: PaymentDetails.fromJson(json["chooseAmountData"]),
      bAllowRepeatDeposit: json["repeatAllowed"] == null ||
              json["chooseAmountData"]["lastPaymentArray"].length == 0
          ? false
          : json["repeatAllowed"],
      showLockedAmount:
          json["bShowLockedAmount"] == null ? false : json["bShowLockedAmount"],
    );
  }
}
