class ChargebackData {
  final int minChargeback;
  final int maxChargeback;
  final dynamic processingFee;

  ChargebackData({
    this.minChargeback,
    this.maxChargeback,
    this.processingFee,
  });

  factory ChargebackData.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return ChargebackData();
    } else {
      return ChargebackData(
        minChargeback: json["minimumChargebackAmt"],
        maxChargeback: json["maximumChargebackAmt"],
        processingFee: json["processingFee"],
      );
    }
  }
}
