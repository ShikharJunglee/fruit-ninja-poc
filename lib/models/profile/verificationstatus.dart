import 'package:flutter/material.dart';

class VerificationStatus with ChangeNotifier {
  bool emailVerified;
  bool mobileVerified;
  String panVerificationStatus;
  String addressVerificationStatus;
  String panRemark;
  String kycRemark;
  int kycAttempts;
  int panAttempts;
  Map<String, dynamic> messages;

  VerificationStatus({
    this.emailVerified,
    this.mobileVerified,
    this.panVerificationStatus,
    this.addressVerificationStatus,
    this.panRemark,
    this.kycRemark,
    this.kycAttempts,
    this.panAttempts,
    this.messages,
  });

  factory VerificationStatus.fromJson(Map<String, dynamic> json) {
    return VerificationStatus(
      emailVerified: json["emailVerification"],
      mobileVerified: json["mobileVerification"],
      panVerificationStatus: json["panVerification"],
      addressVerificationStatus: json["addressVerification"],
      panRemark: json["panRemark"],
      kycRemark: json["kycRemark"],
      kycAttempts: json["kycAttempts"],
      panAttempts: json["panAttempts"],
      messages: json["messages"],
    );
  }

  setEmailVerified(bool emailVerified) {
    this.emailVerified = emailVerified;
    notifyListeners();
  }

  setMobileVerified(bool mobileVerified) {
    this.mobileVerified = mobileVerified;
    notifyListeners();
  }

  setPanVerificationStatus(String panVerificationStatus) {
    this.panVerificationStatus = panVerificationStatus;
    notifyListeners();
  }

  setAddressVerificationStatus(String addressVerificationStatus) {
    this.addressVerificationStatus = addressVerificationStatus;
    notifyListeners();
  }

  setPanRemark(String panRemark) {
    this.panRemark = panRemark;
    notifyListeners();
  }

  setKYCemark(String kycRemark) {
    this.kycRemark = kycRemark;
    notifyListeners();
  }

  setKYCAttempts(int kycAttempts) {
    this.kycAttempts = kycAttempts;
    notifyListeners();
  }

  setPANAttempts(int panAttempts) {
    this.panAttempts = panAttempts;
    notifyListeners();
  }

  setMessages(Map<String, dynamic> messages) {
    this.messages = messages;
    notifyListeners();
  }

  Map<String, dynamic> toJson() => {
        "emailVerification": this.emailVerified,
        "mobileVerification": this.mobileVerified,
        "panVerification": this.panVerificationStatus,
        "addressVerification": this.addressVerificationStatus,
        "panRemark": this.panRemark,
        "kycRemark": this.kycRemark,
        "kycAttempts": this.kycAttempts,
        "panAttempts": this.panAttempts,
        "messages": this.messages,
      };

  setVerificationStatus(Map<String, dynamic> json) {
    this.addressVerificationStatus = json["address_verification"];
    this.panVerificationStatus = json["pan_verification"];
    this.emailVerified =
        json["email_verification"] == null ? false : json["email_verification"];
    this.mobileVerified = json["mobile_verification"] == null
        ? false
        : json["mobile_verification"];
    this.kycRemark = json["kyc_remark"];
    this.panRemark = json["pan_remark"];

    notifyListeners();
  }

  updateAddress(String addressStatus, String kycRemark) {
    this.addressVerificationStatus = addressStatus;
    this.kycRemark = kycRemark;

    notifyListeners();
  }

  updatePanStatus(String panStatus, String panRemark) {
    this.panVerificationStatus = panStatus;
    this.panRemark = panRemark;

    notifyListeners();
  }

  updateDocStatus(String kycStatus, String addressStatus) {
    this.panVerificationStatus = kycStatus;
    this.addressVerificationStatus = addressStatus;

    notifyListeners();
  }

  updateMobileVerificationStatus(bool isVerified) {
    this.mobileVerified = isVerified;

    notifyListeners();
  }

  updateEmailVerificationStatus(bool isVerified) {
    this.emailVerified = isVerified;

    notifyListeners();
  }
}
