import 'package:flutter/material.dart';

class UserBalance with ChangeNotifier {
  double withdrawable;
  double depositBucket;
  double bonusAmount;
  double lockedBonusAmount;
  int goldBars;

  UserBalance({
    this.withdrawable,
    this.depositBucket,
    this.bonusAmount,
    this.lockedBonusAmount,
    this.goldBars,
  });

  factory UserBalance.fromJson(Map<String, dynamic> json) {
    return UserBalance(
      withdrawable: json["withdrawable"].toDouble(),
      depositBucket: json["depositBucket"].toDouble(),
      bonusAmount: json["bonusAmount"].toDouble(),
      lockedBonusAmount: json["lockedBonusAmount"].toDouble(),
      goldBars: json["goldBars"] != null ? json["goldBars"] : 0,
    );
  }

  setWithdrawable(double withdrawable) {
    this.withdrawable = withdrawable;
    notifyListeners();
  }

  setDepositBucket(double depositBucket) {
    this.depositBucket = depositBucket;
    notifyListeners();
  }

  setBonusAmount(double bonusAmount) {
    this.bonusAmount = bonusAmount;
    notifyListeners();
  }

  setLockedBonusAmount(double lockedBonusAmount) {
    this.lockedBonusAmount = lockedBonusAmount;
    notifyListeners();
  }

  setGoldBars(int goldBarsCount) {
    this.goldBars = goldBarsCount;
    notifyListeners();
  }

  updateWithdrawable(double withdrawable) {
    this.withdrawable = withdrawable;

    notifyListeners();
  }

  getTotalBalance() {
    return this.depositBucket + this.bonusAmount + this.withdrawable;
  }

  Map<String, dynamic> toJson() => {
        "withdrawable": this.withdrawable,
        "depositBucket": this.depositBucket,
        "bonusAmount": this.bonusAmount,
        "lockedBonusAmount": this.lockedBonusAmount,
        "goldBars": this.goldBars,
      };
}
