import 'package:flutter/material.dart';

class Address with ChangeNotifier {
  String city;
  String state;
  int pincode;
  String firstName;
  String lastName;
  String address1;
  String address2;

  Address({
    this.city,
    this.state,
    this.pincode,
    this.firstName,
    this.lastName,
    this.address1,
    this.address2,
  });

  factory Address.fromJson(Map<String, dynamic> json) {
    return Address(
      city: json["city"],
      state: json["state"],
      pincode: json["pincode"],
      firstName: json["firstName"],
      lastName: json["lastName"],
      address1: json["address1"],
      address2: json["address2"],
    );
  }

  setCity(String city) {
    this.city = city;
    notifyListeners();
  }

  setState(String state) {
    this.state = state;
    notifyListeners();
  }

  setPincode(int pincode) {
    this.pincode = pincode;
    notifyListeners();
  }

  setFirstName(String firstName) {
    this.firstName = firstName;
    notifyListeners();
  }

  setlLstName(String lastName) {
    this.lastName = lastName;
    notifyListeners();
  }

  setAddress1(String address1) {
    this.address1 = address1;
    notifyListeners();
  }

  setAddress2(String address2) {
    this.address2 = address2;
    notifyListeners();
  }

  Map<String, dynamic> toJson() => {
        "city": city,
        "state": state,
        "pincode": pincode,
        "firstName": firstName,
        "lastName": lastName,
        "address1": address1,
        "address2": address2,
      };
}
