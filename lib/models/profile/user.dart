import 'package:flutter/material.dart';
import 'package:solitaire_gold/models/profile/address.dart';
import 'package:solitaire_gold/models/profile/userbalance.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';

class User with ChangeNotifier {
  int id;
  String email;
  String mobile;
  String dob;
  String gender;
  String status;
  bool otpSignUp;
  bool newUser;
  String loginName;
  Address address;
  UserBalance userBalance;
  bool isUserNameChangeAllowed = true;
  bool hasPassword = true;
  bool isFirstDeposit = false;
  VerificationStatus verificationStatus;
  int avatar;
  Map<String, dynamic> welcomePopup;
  bool isBlockedState;
  User();

  factory User.fromJson(Map<String, dynamic> json) {
    User user = User();
    user.copyFrom(json);

    return user;
  }

  copyFrom(Map<String, dynamic> json) {
    this.id = json["id"];
    this.email = json["email"];
    this.mobile = json["mobile"];
    this.dob = json["dob"];
    this.gender = json["gender"];
    this.status = json["status"];
    this.otpSignUp = json["otpSignUp"];
    this.newUser = json["newUser"];
    this.loginName = json["loginName"];
    this.isFirstDeposit = json["firstDeposit"];
    this.address = Address.fromJson(Map<String, dynamic>.from(json["address"]));
    this.userBalance = UserBalance.fromJson(json["userBalance"]);
    this.isUserNameChangeAllowed = json["isUserNameChangeAllowed"];
    this.verificationStatus =
        VerificationStatus.fromJson(json["verificationStatus"]);
    this.avatar = json["avatarId"];
    this.welcomePopup =
        json["welcomePopup"] != null ? json["welcomePopup"] : this.welcomePopup;
    this.isBlockedState = json["isBlockedState"];

    notifyListeners();
  }

  setId(int id) {
    this.id = id;
    notifyListeners();
  }

  setEmail(String email) {
    this.email = email;
    notifyListeners();
  }

  setMobile(String mobile) {
    this.mobile = mobile;
    notifyListeners();
  }

  setDob(String dob) {
    this.dob = dob;
    notifyListeners();
  }

  setGender(String gender) {
    this.gender = gender;
    notifyListeners();
  }

  setStatus(String status) {
    this.status = status;
    notifyListeners();
  }

  setOTPSignUp(bool otpSignUp) {
    this.otpSignUp = otpSignUp;
    notifyListeners();
  }

  setNewUser(bool newUser) {
    this.newUser = newUser;
    notifyListeners();
  }

  setLoginName(String loginName) {
    this.loginName = loginName;
    notifyListeners();
  }

  setAddress(Address address) {
    this.address = address;
    notifyListeners();
  }

  setUserBalance(UserBalance userBalance) {
    this.userBalance = userBalance;
    notifyListeners();
  }

  setIsUserNameChangeAllowed(bool isUserNameChangeAllowed) {
    this.isUserNameChangeAllowed = isUserNameChangeAllowed;
    notifyListeners();
  }

  setHasPassword(bool hasPassword) {
    this.hasPassword = hasPassword;
    notifyListeners();
  }

  setVerificationStatus(VerificationStatus verificationStatus) {
    this.verificationStatus = verificationStatus;
    notifyListeners();
  }

  setAvatar(int avatar) {
    this.avatar = avatar;
    notifyListeners();
  }

  setUserAllowedChangeName(bool allowed) {
    this.isUserNameChangeAllowed = allowed;
    notifyListeners();
  }

  Map<String, dynamic> toJson() => {
        "id": this.id,
        "email": this.email,
        "mobile": this.mobile,
        "dob": this.dob,
        "gender": this.gender,
        "status": this.status,
        "otpSignUp": this.otpSignUp,
        "newUser": this.newUser,
        "loginName": this.loginName,
        "isFirstDeposit": this.isFirstDeposit,
        "address": this.address.toJson(),
        "userBalance": this.userBalance.toJson(),
        "isUserNameChangeAllowed": this.isUserNameChangeAllowed,
        "verificationStatus": this.verificationStatus.toJson(),
        "avatar": this.avatar,
        "isBlockedState": this.isBlockedState,
      };
}
