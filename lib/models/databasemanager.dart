import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/models/lobby/lobby_data.dart';
import 'package:solitaire_gold/models/lobby/mymatches.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';
import 'package:solitaire_gold/utils/websocket/request.dart';
import 'package:solitaire_gold/utils/websocket/requesttype.dart';
import 'package:solitaire_gold/utils/websocket/response.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class DatabaseManager with ChangeNotifier {
  WebSocket _webSocket;
  StreamSubscription<dynamic> wsUpdateListener;

  LobbyData _lobbyData = LobbyData();
  MyMatches _myMatches = MyMatches();

  DatabaseManager() : super() {
    _webSocket = WebSocket();
    initDataHolders();
  }

  initDataHolders() {
    if (wsUpdateListener != null) {
      wsUpdateListener.cancel();
    }
  }

  connectToServerWith(String url) {
    if (wsUpdateListener != null) {
      wsUpdateListener.cancel();
    }
    _webSocket.connect(url);
    addDataListener();
  }

  void addDataListener() {
    wsUpdateListener = _webSocket.subscriber().stream.listen(onWsMessage);
  }

  LobbyData getLobbyData() {
    return _lobbyData;
  }

  MyMatches getMyMatches() {
    return _myMatches;
  }

  void onWsMessage(message) {
    Response response = Response.fromJson(message);

    if (response.type == RequestType.READY) {
      LobbyRequest tournamentRequest = LobbyRequest();
      WebSocket().sendMessage(tournamentRequest.toJson());
    } else if (response.type == RequestType.LOBBY_DATA) {
      _lobbyData.copyFrom(LobbyData.fromJson(response.data));
    } else if (response.type == RequestType.ASYNC_HEADS_UP_UPDATE) {
      _lobbyData.updateAsyncH2H(response.data);
    } else if (response.type == RequestType.LEAGUES_UPDATE) {
      _lobbyData.updateSmallLeagues(response.data);
    } else if (response.type == RequestType.MY_MATCHES) {
      _myMatches.setMyMatches(response.data);
      EventManager().publish(EventName.MYMATCH_ADDED, data: response.data);
    } else if (response.type == RequestType.LEADERBOARD) {
      EventManager().publish(EventName.LEADERBOARD_LOADED, data: response.data);
    }
  }
}
