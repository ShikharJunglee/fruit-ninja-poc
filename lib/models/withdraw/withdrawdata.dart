class WithdrawData {
  String accountNumber;
  String vpaID;
  String ifscCode;
  String routingNumber;
  final int numberOfFreeWithdraw;
  final int totalWithdraw;
  int withdrawCost;
  final int minWithdraw;
  final int maxWithdraw;
  final int paytmMaxWithdraw;
  final int paytmMinWithdraw;
  final Map<String, dynamic> withdrawModes;
  final int processingMin;
  final dynamic processingFee;
  final int ssnValue;
  final bool ssnPresent;
  final bool ssnEnabled;

  WithdrawData({
    this.accountNumber,
    this.ifscCode,
    this.routingNumber,
    this.numberOfFreeWithdraw,
    this.totalWithdraw,
    this.withdrawCost,
    this.minWithdraw,
    this.paytmMinWithdraw,
    this.maxWithdraw,
    this.paytmMaxWithdraw,
    this.withdrawModes,
    this.vpaID,
    this.processingFee,
    this.processingMin,
    this.ssnValue,
    this.ssnPresent,
    this.ssnEnabled,
  });

  factory WithdrawData.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return WithdrawData();
    } else {
      return WithdrawData(
        ifscCode: json["ifsc_code"],
        minWithdraw: json["minWithdraw"],
        maxWithdraw: json["maxWithdraw"],
        withdrawCost: json["withdrawCost"],
        totalWithdraw: json["totalWithdraw"],
        accountNumber: json["account_number"],
        paytmMaxWithdraw: json["paytmMaxWithdraw"],
        paytmMinWithdraw: json["paytmMinWithdraw"],
        numberOfFreeWithdraw: json["numberOfFreeWithdraw"],
        withdrawModes: json["withdrawModes"],
        vpaID: json["vpaID"],
        routingNumber: json["routingNumber"],
        processingMin: json["processingAmount"] ?? 0,
        processingFee: json["processingFee"],
        ssnValue: json["ssnValue"],
        ssnPresent: json["ssnPresent"],
        ssnEnabled: json["ssnEnabled"],
      );
    }
  }
}
