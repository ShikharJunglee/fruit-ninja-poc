import 'package:flutter/material.dart';

class AsyncHeadsUp with ChangeNotifier {
  int brandId;
  String displayName;
  DateTime endTime;
  bool entryType;
  int formatType;
  int gameConfigId;
  bool allowPause;
  int gameDuration;
  int h2hTemplateId;
  bool isDeleted;
  bool isGuaranteed;
  bool isRecommended;
  double prizePool;
  int maxPlayers;
  int joinedCount;
  int minPlayers;
  int noOfClones;
  int prizeStructureId;
  int prizeType;
  double realCurrency;
  int scoringId;
  int segmentId;
  DateTime startTime;
  int status;
  int priority;
  int templateId;
  int tiebreakerDuration;
  int undoCnt;
  int virtualCurrency;
  int wastPileCardCnt;
  double bounsPercentage;
  int rewardType;
  String ribbonText;
  int freeTrips;
  int tripPenalty;

  AsyncHeadsUp({
    this.brandId,
    this.displayName,
    this.endTime,
    this.entryType,
    this.formatType,
    this.gameConfigId,
    this.allowPause,
    this.gameDuration,
    this.h2hTemplateId,
    this.isDeleted,
    this.isGuaranteed,
    this.isRecommended,
    this.prizePool,
    this.maxPlayers,
    this.joinedCount,
    this.minPlayers,
    this.noOfClones,
    this.prizeStructureId,
    this.prizeType,
    this.realCurrency,
    this.scoringId,
    this.segmentId,
    this.startTime,
    this.status,
    this.priority,
    this.templateId,
    this.tiebreakerDuration,
    this.undoCnt,
    this.virtualCurrency,
    this.wastPileCardCnt,
    this.bounsPercentage,
    this.rewardType,
    this.ribbonText,
    this.freeTrips,
    this.tripPenalty,
  });

  copyFrom(Map<String, dynamic> json) {
    this.brandId = json["brandId"] == null
        ? this.brandId
        : double.parse(json["brandId"].toString()).toInt();
    this.displayName =
        json["displayName"] == null ? this.displayName : json["displayName"];
    this.endTime = json["endTime"] == null
        ? this.endTime
        : DateTime.tryParse(json["endTime"]);
    this.startTime = json["startTime"] == null
        ? this.startTime
        : DateTime.tryParse(json["startTime"]);

    this.entryType =
        json["entryType"] == null ? this.entryType : json["entryType"];
    this.formatType =
        json["formatType"] == null ? this.formatType : json["formatType"];
    this.gameConfigId =
        json["gameConfigId"] == null ? this.gameConfigId : json["gameConfigId"];
    this.allowPause =
        json["allowPause"] == null ? this.allowPause : json["allowPause"];
    this.gameDuration =
        json["gameDuration"] == null ? this.gameDuration : json["gameDuration"];
    this.h2hTemplateId = json["h2hTemplateId"] == null
        ? this.h2hTemplateId
        : json["h2hTemplateId"];
    this.isDeleted =
        json["isDeleted"] == null ? this.isDeleted : json["isDeleted"];
    this.isGuaranteed =
        json["isGuaranteed"] == null ? this.isGuaranteed : json["isGuaranteed"];
    this.isRecommended = json["isRecommended"] == null
        ? this.isRecommended
        : json["isRecommended"];
    this.prizePool = json["prizePool"] == null
        ? this.prizePool
        : double.tryParse(json["prizePool"].toString());
    this.maxPlayers =
        json["maxPlayers"] == null ? this.maxPlayers : json["maxPlayers"];
    this.joinedCount = json["joinedCount"] == null
        ? this.joinedCount
        : double.parse(json["joinedCount"].toString()).toInt();
    this.minPlayers =
        json["minPlayers"] == null ? this.minPlayers : json["minPlayers"];
    this.noOfClones =
        json["noOfClones"] == null ? this.noOfClones : json["noOfClones"];
    this.prizeStructureId = json["prizeStructureId"] == null
        ? this.prizeStructureId
        : json["prizeStructureId"];
    this.prizeType =
        json["prizeType"] == null ? this.prizeType : json["prizeType"];
    this.realCurrency =
        json["realCurrency"] == null ? this.realCurrency : json["realCurrency"];
    this.scoringId =
        json["scoringId"] == null ? this.scoringId : json["scoringId"];
    this.segmentId =
        json["segmentId"] == null ? this.segmentId : json["segmentId"];
    this.status = json["status"] == null ? this.status : json["status"];
    this.priority = json["priority"] == null ? this.priority : json["priority"];
    this.templateId =
        json["templateId"] == null ? this.templateId : json["templateId"];
    this.tiebreakerDuration = json["tiebreakerDuration"] == null
        ? this.tiebreakerDuration
        : json["tiebreakerDuration"];
    this.undoCnt = json["undoCnt"] == null ? this.undoCnt : json["undoCnt"];
    this.virtualCurrency = json["virtualCurrency"] == null
        ? this.virtualCurrency
        : json["virtualCurrency"];
    this.wastPileCardCnt = json["wastPileCardCnt"] == null
        ? this.wastPileCardCnt
        : json["wastPileCardCnt"];
    this.bounsPercentage = json["bounsPercentage"] == null
        ? this.bounsPercentage
        : double.tryParse(json["bounsPercentage"].toString());
    this.rewardType =
        json["rewardType"] == null ? this.rewardType : json["rewardType"];
    this.ribbonText =
        json["ribbonText"] == null ? this.ribbonText : json["ribbonText"];
    this.freeTrips =
        json["freeTrips"] == null ? this.freeTrips : json["freeTrips"];
    this.tripPenalty =
        json["tripPenalty"] == null ? this.tripPenalty : json["tripPenalty"];
  }

  factory AsyncHeadsUp.fromJson(Map<String, dynamic> json) {
    return AsyncHeadsUp(
      brandId: json["brandId"],
      displayName: json["displayName"],
      endTime:
          json["endTime"] == null ? null : DateTime.tryParse(json["endTime"]),
      entryType: json["entryType"],
      formatType: json["formatType"],
      gameConfigId: json["gameConfigId"],
      allowPause: json["allowPause"],
      gameDuration: json["gameDuration"],
      h2hTemplateId: json["h2hTemplateId"],
      isDeleted: json["isDeleted"],
      isGuaranteed: json["isGuaranteed"],
      isRecommended: json["isRecommended"],
      prizePool: double.tryParse(json["prizePool"].toString()),
      maxPlayers: json["maxPlayers"],
      joinedCount: json["joinedCount"],
      minPlayers: json["minPlayers"],
      noOfClones: json["noOfClones"],
      prizeStructureId: json["prizeStructureId"],
      prizeType: json["prizeType"],
      realCurrency: double.tryParse(json["realCurrency"].toString()),
      scoringId: json["scoringId"],
      segmentId: json["segmentId"],
      startTime: json["startTime"] == null
          ? null
          : DateTime.tryParse(json["startTime"]),
      status: json["status"],
      priority: json["priority"],
      templateId: json["templateId"],
      tiebreakerDuration: json["tiebreakerDuration"],
      undoCnt: json["undoCnt"],
      virtualCurrency: json["virtualCurrency"],
      wastPileCardCnt: json["wastPileCardCnt"],
      bounsPercentage: json["bounsPercentage"],
      rewardType: json["rewardType"],
      ribbonText: json["ribbonText"],
      freeTrips: json["freeTrips"],
      tripPenalty: json["tripPenalty"],
    );
  }
}
