class PrizeStrucutreDetails {
  double serviceFee;
  double prizeAmount;
  int prizeCount;
  int goldBarsAmount;
  int rewardType;
  double entryFee;
  int goldBarFee;
  List<PrizeDistribution> distribution;

  PrizeStrucutreDetails({
    this.serviceFee,
    this.prizeAmount,
    this.prizeCount,
    this.goldBarsAmount,
    this.rewardType,
    this.entryFee,
    this.goldBarFee,
    this.distribution,
  });

  factory PrizeStrucutreDetails.fromJson(Map<String, dynamic> json) {
    return PrizeStrucutreDetails(
      serviceFee: json["serviceFee"].toDouble(),
      prizeAmount: json["prizeAmount"].toDouble(),
      prizeCount: json["prizeCount"],
      goldBarsAmount:
          double.tryParse(json["goldBarsAmount"].toString()).toInt(),
      rewardType: json["rewardType"],
      entryFee: double.tryParse(json["entryFee"].toString()),
      goldBarFee: json["goldBarFee"],
      distribution: json["distribution"] == null
          ? []
          : (json["distribution"] as List<dynamic>)
              .map((f) => PrizeDistribution.fromJson(f))
              .toList(),
    );
  }
}

class PrizeDistribution {
  String rank;
  String amount;
  String goldBars;

  PrizeDistribution({
    this.rank,
    this.amount,
    this.goldBars,
  });

  factory PrizeDistribution.fromJson(Map<String, dynamic> json) {
    return PrizeDistribution(
      rank: json["rank"],
      amount: json["amount"] == null ? "0" : json["amount"],
      goldBars: json["goldBars"] == null ? "0" : json["goldBars"],
    );
  }
}
