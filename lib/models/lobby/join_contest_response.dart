class JoinContestResponse {
  final List<int> error;
  final JoinContestErrorDetails errorDetails;

  JoinContestResponse({this.error, this.errorDetails});

  factory JoinContestResponse.fromJson(Map<String, dynamic> json) {
    return JoinContestResponse(
      error: json["error"].cast<int>(),
      errorDetails: JoinContestErrorDetails.fromJson(json["errorDetails"]),
    );
  }
}

class JoinContestErrorDetails {
  final bool isBlockedUser;
  final String title;
  final String errorMessage;

  JoinContestErrorDetails({
    this.isBlockedUser,
    this.title,
    this.errorMessage,
  });

  factory JoinContestErrorDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return JoinContestErrorDetails(errorMessage: "", isBlockedUser: false, title: "");
    }
    return JoinContestErrorDetails(
      isBlockedUser: json["isBlockedUser"],
      title: json["title"],
      errorMessage: json["errorMessage"],
    );
  }
}
