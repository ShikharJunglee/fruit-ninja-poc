import 'package:flutter/material.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';

class LobbyData with ChangeNotifier {
  List<AsyncHeadsUp> asyncH2H = [];
  List<SmallLeague> smallLeagues = [];

  LobbyData({this.asyncH2H, this.smallLeagues});

  copyFrom(LobbyData _update) {
    asyncH2H = _update.asyncH2H;
    smallLeagues = _update.smallLeagues;

    _sort();

    notifyListeners();
  }

  updateAsyncH2H(Map<String, dynamic> _h2hData) {
    if ((_h2hData["lstAdded"] as List<dynamic>).length > 0) {
      List<AsyncHeadsUp> _newLeagues = (_h2hData["lstAdded"] as List<dynamic>)
          .map((f) => AsyncHeadsUp.fromJson(f))
          .toList();
      print(_newLeagues);
      _newLeagues.forEach((newLeague) {
        bool bMatchFound = false;
        if (asyncH2H != null) {
          asyncH2H.forEach((existingLeague) {
            if (existingLeague.h2hTemplateId == newLeague.h2hTemplateId) {
              bMatchFound = true;
            }
          });
        }

        if (!bMatchFound) {
          asyncH2H.add(newLeague);
          _sort();

          notifyListeners();
        }
      });
    } else if (asyncH2H != null &&
        (_h2hData["lstModified"] as List<dynamic>).length > 0) {
      (_h2hData["lstModified"] as List<dynamic>).forEach((updatedLeague) {
        for (int i = 0; i < asyncH2H.length; i++) {
          if (updatedLeague["h2hTemplateId"] == asyncH2H[i].h2hTemplateId) {
            asyncH2H[i].copyFrom(updatedLeague);
          }
        }
      });

      notifyListeners();
    } else if (asyncH2H != null &&
        (_h2hData["lstRemoved"] as List<dynamic>).length > 0) {
      (_h2hData["lstRemoved"]).forEach((h2hTemplateId) {
        asyncH2H.removeWhere(
            (oldLeague) => oldLeague.h2hTemplateId == h2hTemplateId);
        notifyListeners();
      });
    }
  }

  updateSmallLeagues(Map<String, dynamic> _smallLeagueData) {
    if ((_smallLeagueData["lstAdded"] as List<dynamic>).length > 0) {
      List<SmallLeague> _newLeagues =
          (_smallLeagueData["lstAdded"] as List<dynamic>)
              .map((f) => SmallLeague.fromJson(f))
              .toList();
      _newLeagues.forEach((newLeague) {
        bool bMatchFound = false;
        if (smallLeagues != null) {
          smallLeagues.forEach((existingLeague) {
            if (existingLeague.tournamentId == newLeague.tournamentId) {
              bMatchFound = true;
            }
          });
        }

        if (!bMatchFound) {
          smallLeagues.add(newLeague);

          _sort();
          notifyListeners();
        }
      });
    } else if (smallLeagues != null &&
        (_smallLeagueData["lstModified"] as List<dynamic>).length > 0) {
      (_smallLeagueData["lstModified"] as List<dynamic>)
          .forEach((updatedLeague) {
        for (int i = 0; i < smallLeagues.length; i++) {
          if (updatedLeague["tournamentId"] == smallLeagues[i].tournamentId) {
            smallLeagues[i].copyFrom(updatedLeague);
          }
        }
      });

      notifyListeners();
    } else if (smallLeagues != null &&
        (_smallLeagueData["lstRemoved"] as List<dynamic>).length > 0) {
      (_smallLeagueData["lstRemoved"]).forEach((tournamentId) {
        smallLeagues
            .removeWhere((oldLeague) => oldLeague.tournamentId == tournamentId);
        notifyListeners();
      });
    }
  }

  _sort() {
    smallLeagues.sort((a, b) {
      if (a.priority == b.priority) {
        if (a.realCurrency == b.realCurrency) {
          return a.virtualCurrency - b.virtualCurrency;
        }
        return (a.realCurrency - b.realCurrency).toInt();
      }
      return b.priority - a.priority;
    });

    asyncH2H.sort((a, b) {
      if (a.priority == b.priority) {
        if (a.realCurrency == b.realCurrency) {
          return a.virtualCurrency - b.virtualCurrency;
        }
        return (a.realCurrency - b.realCurrency).toInt();
      }
      return b.priority - a.priority;
    });
  }

  factory LobbyData.fromJson(Map<String, dynamic> json) {
    return LobbyData(
      asyncH2H: json["headsUps"] == null
          ? []
          : (json["headsUps"] as List<dynamic>)
              .map((f) => AsyncHeadsUp.fromJson(f))
              .toList(),
      smallLeagues: json["tournaments"] == null
          ? []
          : (json["tournaments"] as List<dynamic>)
              .map((f) => SmallLeague.fromJson(f))
              .toList(),
    );
  }
}
