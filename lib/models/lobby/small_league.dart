import 'package:flutter/material.dart';

class SmallLeague with ChangeNotifier {
  int brandId;
  String displayName;
  DateTime endTime;
  bool entryType;
  bool allowPause;
  int formatType;
  int gameConfigId;
  int gameDuration;
  bool isDeleted;
  bool isGuaranteed;
  bool isRecommended;
  int joinedCount;
  int matchId;
  int maxPlayers;
  int minPlayers;
  int noOfClones;
  int prizeStructureId;
  double prizePool;
  int prizeType;
  double realCurrency;
  int regCloseWindow;
  DateTime regStartTime;
  int scoringId;
  int segmentId;
  DateTime startTime;
  int status;
  int priority;
  int tableId;
  int templateId;
  int tiebreakerDuration;
  int tournamentId;
  int undoCnt;
  int virtualCurrency;
  int wastPileCardCnt;
  double bounsPercentage;
  int rewardType;
  int freeTrips;
  int tripPenalty;
  String ribbonText;

  SmallLeague({
    this.brandId,
    this.displayName,
    this.endTime,
    this.entryType,
    this.allowPause,
    this.formatType,
    this.gameConfigId,
    this.gameDuration,
    this.isDeleted,
    this.isGuaranteed,
    this.isRecommended,
    this.joinedCount,
    this.matchId,
    this.maxPlayers,
    this.minPlayers,
    this.noOfClones,
    this.prizeStructureId,
    this.prizePool,
    this.prizeType,
    this.realCurrency,
    this.regCloseWindow,
    this.regStartTime,
    this.scoringId,
    this.segmentId,
    this.startTime,
    this.status,
    this.priority,
    this.tableId,
    this.templateId,
    this.tiebreakerDuration,
    this.tournamentId,
    this.undoCnt,
    this.virtualCurrency,
    this.wastPileCardCnt,
    this.bounsPercentage,
    this.rewardType,
    this.freeTrips,
    this.tripPenalty,
    this.ribbonText,
  });

  copyFrom(Map<String, dynamic> json) {
    this.brandId = json["brandId"] == null ? this.brandId : json["brandId"];
    this.displayName =
        json["displayName"] == null ? this.displayName : json["displayName"];
    this.entryType =
        json["entryType"] == null ? this.entryType : json["entryType"];
    this.allowPause =
        json["allowPause"] == null ? this.allowPause : json["allowPause"];
    this.formatType = json["formatType"] == null
        ? this.formatType
        : (int.tryParse(json["formatType"].toString()));
    this.gameConfigId =
        json["gameConfigId"] == null ? this.gameConfigId : json["gameConfigId"];
    this.gameDuration =
        json["gameDuration"] == null ? this.gameDuration : json["gameDuration"];
    this.isDeleted =
        json["isDeleted"] == null ? this.isDeleted : json["isDeleted"];
    this.isGuaranteed =
        json["isGuaranteed"] == null ? this.isGuaranteed : json["isGuaranteed"];
    this.isRecommended = json["isRecommended"] == null
        ? this.isRecommended
        : json["isRecommended"];

    this.joinedCount = json["joinedCount"] == null
        ? this.joinedCount
        : double.parse(json["joinedCount"].toString()).toInt();

    this.matchId = json["matchId"] == null ? this.matchId : json["matchId"];
    this.maxPlayers =
        json["maxPlayers"] == null ? this.maxPlayers : json["maxPlayers"];
    this.minPlayers =
        json["minPlayers"] == null ? this.minPlayers : json["minPlayers"];
    this.noOfClones =
        json["noOfClones"] == null ? this.noOfClones : json["noOfClones"];
    this.prizeStructureId = json["prizeStructureId"] == null
        ? this.prizeStructureId
        : json["prizeStructureId"];
    this.prizeType =
        json["prizeType"] == null ? this.prizeType : json["prizeType"];
    this.realCurrency =
        json["realCurrency"] == null ? this.realCurrency : json["realCurrency"];
    this.regCloseWindow = json["regCloseWindow"] == null
        ? this.regCloseWindow
        : json["regCloseWindow"];

    this.scoringId =
        json["scoringId"] == null ? this.scoringId : json["scoringId"];
    this.segmentId =
        json["segmentId"] == null ? this.segmentId : json["segmentId"];
    this.status = json["status"] == null ? this.status : json["status"];
    this.priority = json["priority"] == null ? this.priority : json["priority"];
    this.tableId = json["tableId"] == null ? this.tableId : json["tableId"];
    this.templateId =
        json["templateId"] == null ? this.templateId : json["templateId"];
    this.tiebreakerDuration = json["tiebreakerDuration"] == null
        ? this.tiebreakerDuration
        : json["tiebreakerDuration"];
    this.tournamentId =
        json["tournamentId"] == null ? this.tournamentId : json["tournamentId"];
    this.undoCnt = json["undoCnt"] == null ? this.undoCnt : json["undoCnt"];
    this.virtualCurrency = json["virtualCurrency"] == null
        ? this.virtualCurrency
        : json["virtualCurrency"];
    this.wastPileCardCnt = json["wastPileCardCnt"] == null
        ? this.wastPileCardCnt
        : json["wastPileCardCnt"];
    this.bounsPercentage = json["bounsPercentage"] == null
        ? this.bounsPercentage
        : double.tryParse(json["bounsPercentage"].toString());
    this.rewardType =
        json["rewardType"] == null ? this.rewardType : json["rewardType"];
    this.freeTrips =
        json["freeTrips"] == null ? this.freeTrips : json["freeTrips"];
    this.tripPenalty =
        json["tripPenalty"] == null ? this.tripPenalty : json["tripPenalty"];
    this.ribbonText =
        json["ribbonText"] == null ? this.ribbonText : json["ribbonText"];

    this.prizePool = json["prizePool"] == null
        ? this.prizePool
        : double.parse(json["prizePool"].toString());
    this.endTime = json["endTime"] == null
        ? this.endTime
        : DateTime.tryParse(json["endTime"]);
    this.regStartTime = json["regStartTime"] == null
        ? this.regStartTime
        : DateTime.tryParse(json["regStartTime"]);
    this.startTime = json["startTime"] == null
        ? this.startTime
        : DateTime.tryParse(json["startTime"]);
  }

  factory SmallLeague.fromJson(Map<String, dynamic> json) {
    return SmallLeague(
      brandId: json["brandId"],
      displayName: json["displayName"],
      endTime:
          json["endTime"] == null ? null : DateTime.tryParse(json["endTime"]),
      entryType: json["entryType"],
      allowPause: json["allowPause"],
      formatType: json["formatType"],
      gameConfigId: json["gameConfigId"],
      gameDuration: json["gameDuration"],
      isDeleted: json["isDeleted"],
      isGuaranteed: json["isGuaranteed"],
      isRecommended: json["isRecommended"],
      joinedCount: json["joinedCount"],
      matchId: json["matchId"],
      maxPlayers: json["maxPlayers"],
      minPlayers: json["minPlayers"],
      noOfClones: json["noOfClones"],
      prizeStructureId: json["prizeStructureId"],
      prizePool: double.parse(json["prizePool"].toString()),
      prizeType: json["prizeType"],
      realCurrency: double.parse(json["realCurrency"].toString()),
      regCloseWindow: json["regCloseWindow"],
      regStartTime: json["regStartTime"] == null
          ? null
          : DateTime.tryParse(json["regStartTime"]),
      scoringId: json["scoringId"],
      segmentId: json["segmentId"],
      startTime: json["startTime"] == null
          ? null
          : DateTime.tryParse(json["startTime"]),
      status: json["status"],
      priority: json["priority"],
      tableId: json["tableId"],
      templateId: json["templateId"],
      tiebreakerDuration: json["tiebreakerDuration"],
      tournamentId: json["tournamentId"],
      undoCnt: json["undoCnt"],
      virtualCurrency: json["virtualCurrency"],
      wastPileCardCnt: json["wastPileCardCnt"],
      bounsPercentage: json["bounsPercentage"],
      rewardType: json["rewardType"],
      freeTrips: json["freeTrips"],
      tripPenalty: json["tripPenalty"],
      ribbonText: json["ribbonText"],
    );
  }
}
