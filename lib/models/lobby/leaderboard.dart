class LeaderboardUser {
  int userId;
  String userName;
  int rank;
  double score;
  int timeBonus;
  double realCurrency;
  double virtualCurrency;
  int prize;
  int gameStatus;
  int avatarId;

  LeaderboardUser({
    this.userId,
    this.userName,
    this.rank,
    this.score,
    this.timeBonus,
    this.realCurrency,
    this.virtualCurrency,
    this.prize,
    this.gameStatus,
    this.avatarId,
  });
  factory LeaderboardUser.fromJson(Map<String, dynamic> json) {
    return LeaderboardUser(
      userId: json["userId"],
      userName: json["userName"],
      rank: json["rank"],
      score: json["score"].toDouble(),
      timeBonus: json["timeBonus"],
      realCurrency: json["realCurrency"].toDouble(),
      virtualCurrency: json["virtualCurrency"].toDouble(),
      prize: json["prize"],
      gameStatus: json["gameStatus"],
      avatarId: json["avatarId"],
    );
  }
}

class Leaderboard {
  List<LeaderboardUser> leaderboard;
  int type;
  int matchId;
  int formatType;
  double prizePool;
  int rewardType;
  double entryFeeRealCurrency;
  int entryFeevirtualCurrency;
  int matchStatus;
  bool allowRematch;
  bool allowTiebreaker;
  int joinedCount;
  int size;
  int prizeStructureId;

  Leaderboard({
    this.leaderboard,
    this.type,
    this.matchId,
    this.formatType,
    this.prizePool,
    this.rewardType,
    this.entryFeeRealCurrency,
    this.entryFeevirtualCurrency,
    this.matchStatus,
    this.allowRematch,
    this.allowTiebreaker,
    this.joinedCount,
    this.size,
    this.prizeStructureId,
  });

  factory Leaderboard.fromJson(Map<String, dynamic> json) {
    return Leaderboard(
      leaderboard: json["leaderboard"] == null
          ? []
          : (json["leaderboard"] as List<dynamic>)
              .map((item) => LeaderboardUser.fromJson(item))
              .toList(),
      type: json["type"],
      matchId: json["matchId"],
      formatType: json["formatType"],
      prizePool: json["prizePool"].toDouble(),
      rewardType: json["rewardType"],
      entryFeeRealCurrency:
          double.tryParse(json["entryFeeRealCurrency"].toString()),
      entryFeevirtualCurrency: json["entryFeevirtualCurrency"],
      matchStatus: json["matchStatus"],
      allowRematch: json["allowRematch"],
      allowTiebreaker: json["allowTiebreaker"],
      joinedCount: json["joinedCount"],
      size: json["size"],
      prizeStructureId: json["prizeStructureId"],
    );
  }
}
