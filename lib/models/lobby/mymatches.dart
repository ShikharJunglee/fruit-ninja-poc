import 'package:flutter/material.dart';

class MatchStatus {
  static const int CREATED = 0;
  static const int ACTIVE = 1;
  static const int IN_PROGRESS = 3;
  static const int COMPLETED = 4;
  static const int REFUND = 5;
  static const int COMPLETED_BONUS = 6;
  static const int COMPLETED_REFERRAL_BONUS = 7;
}

class MyMatch {
  bool isTitle;
  String name;
  int matchId;
  int gameId;
  int id;
  int size;
  int joined;
  int minPlayers;
  int score;
  double realCurrency;
  int virtualCurrency;
  double entryRealCurrency;
  int entryVirtualCurrency;
  int rank;
  int status;
  int formatType;
  int prizeType;
  double bonusPercent;
  String endTime;
  String joinedTime;
  int prizeStructureId;
  int templateId;

  MyMatch({
    this.isTitle,
    this.name,
    this.matchId,
    this.gameId,
    this.id,
    this.size,
    this.joined,
    this.minPlayers,
    this.score,
    this.realCurrency,
    this.virtualCurrency,
    this.entryRealCurrency,
    this.entryVirtualCurrency,
    this.rank,
    this.status,
    this.formatType,
    this.prizeType,
    this.bonusPercent,
    this.endTime,
    this.joinedTime,
    this.prizeStructureId,
    this.templateId,
  });

  factory MyMatch.fromJson(Map<String, dynamic> json) {
    return MyMatch(
      isTitle: json["isTitle"],
      name: json["name"],
      matchId: json["matchId"],
      gameId: json["gameId"],
      id: json["id"],
      size: json["size"],
      joined: json["joined"],
      minPlayers: json["minPlayers"],
      score: double.tryParse(json["score"].toString()).toInt(),
      realCurrency: double.tryParse(json["realCurrency"].toString()),
      virtualCurrency: json["virtualCurrency"],
      entryRealCurrency: double.tryParse(json["entryRealCurrency"].toString()),
      entryVirtualCurrency: json["entryVirtualCurrency"],
      rank: json["rank"],
      status: json["status"],
      formatType: json["formatType"],
      prizeType: json["prizeType"],
      bonusPercent: double.tryParse(json["bonusPercent"].toString()),
      endTime: json["endTime"],
      joinedTime: json["joinedTime"],
      prizeStructureId: json["prizeStructureId"],
      templateId: json["templateId"],
    );
  }
}

class MyMatches with ChangeNotifier {
  List<MyMatch> _allMatches = [];
  List<MyMatch> _inProgressMatches = [];
  List<MyMatch> _completedMatches = [];
  bool loaded = false;

  setMyMatches(Map<String, dynamic> data) {
    _allMatches = [];
    _inProgressMatches = [];
    _completedMatches = [];
    loaded = true;
    data?.forEach((key, value) {
      value?.forEach((match) {
        _allMatches.add(MyMatch.fromJson(match));
      });
    });
    // allMatches = [...data["tournaments"], ...data["h2h"]];
    _allMatches?.forEach((MyMatch m) {
      if (m.status > MatchStatus.IN_PROGRESS) {
        _completedMatches.add(m);
      } else {
        _inProgressMatches.add(m);
      }
    });
    _inProgressMatches?.sort((a, b) => b.gameId - a.gameId);
    _completedMatches?.sort((a, b) =>
        DateTime.parse(b.endTime).millisecondsSinceEpoch -
        DateTime.parse(a.endTime).millisecondsSinceEpoch);
    notifyListeners();
  }

  List<MyMatch> getAllMatches() {
    return _allMatches;
  }

  List<MyMatch> getCompletedMatches() {
    return _completedMatches;
  }

  List<MyMatch> getInProgressMatches() {
    return _inProgressMatches;
  }
}
