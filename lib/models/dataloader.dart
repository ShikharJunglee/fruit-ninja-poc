import 'package:flutter/material.dart';

class DataLoader with ChangeNotifier {
  bool isLoading;

  DataLoader({this.isLoading = false});

  setLoadingStatus(bool loading) {
    this.isLoading = loading;

    notifyListeners();
  }
}
