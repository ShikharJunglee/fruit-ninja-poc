import 'package:flutter/material.dart';
import 'package:solitaire_gold/models/addcash/deposit.dart';
import 'package:solitaire_gold/models/addcash/paymentdetails.dart';

import 'package:solitaire_gold/routes/solitaire_gold_pageroute.dart';
import 'package:solitaire_gold/screens/auth/login/signin.dart';
import 'package:solitaire_gold/screens/chargeback/chargeback.dart';
import 'package:solitaire_gold/screens/chargeback/chargebackhistory.dart';
import 'package:solitaire_gold/screens/contactus/contactus.dart';
import 'package:solitaire_gold/screens/helpcenter/help_center.dart';
import 'package:solitaire_gold/screens/lobby/addcash/add_cash.dart';
import 'package:solitaire_gold/screens/lobby/lobby.dart';
import 'package:solitaire_gold/screens/myaccount/myaccounts.dart';
import 'package:solitaire_gold/screens/paymentmode/paymentmode.dart';
import 'package:solitaire_gold/screens/paymentmode/payoutmode.dart';
import 'package:solitaire_gold/screens/profile/profile.dart';
import 'package:solitaire_gold/screens/raf/raf.dart';
import 'package:solitaire_gold/screens/raf/rafV2.dart';
import 'package:solitaire_gold/screens/raf/selectcontacts.dart';
import 'package:solitaire_gold/screens/solitairegold/solitairegold.dart';
import 'package:solitaire_gold/screens/splashscreen/splashscreen.dart';
import 'package:solitaire_gold/screens/staticpage.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw.dart';
import 'package:solitaire_gold/screens/withdraw/verification/verification.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/withdrawhistory.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw_us.dart';
import 'package:solitaire_gold/screens/withdraw/withdrawaddress.dart';

class AppRoutes {
  SolitaireGoldPageRoute getRouteFor(RouteSettings settings) {
    Map<String, dynamic> arguments = settings.arguments;
    switch (settings.name.toLowerCase()) {
      case '/lobby':
        return SolitaireGoldPageRoute(pageBuilder: (context) => Lobby());
      case '/signin':
        return SolitaireGoldPageRoute(pageBuilder: (context) => Signin());
      case '/signup':
        return SolitaireGoldPageRoute(pageBuilder: (context) => Signin());
      case '/solitaire':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => SolitaireGold(
            asyncHeadsUp: arguments["asyncHeadsUp"],
            league: arguments["league"],
            isReplay: arguments["isReplay"],
            selectedTabIndex: arguments["selectedLobbyTabIndex"],
            isFTUE: arguments["isFTUE"],
            gameId: arguments["gameId"],
            matchId: arguments["matchId"],
            tieMatchId: arguments["tieMatchId"],
            formatType: arguments["formatType"],
          ),
        );
      case '/profile':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => Profile(
            url: arguments["url"],
          ),
        );
      case '/account':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => MyAccount(
            accountData: arguments["accountData"],
          ),
        );
      case '/contact-us':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => ContactUs(),
        );
      case '/withdraw':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => Withdraw(
            data: arguments["data"],
            source: arguments["source"],
          ),
        );
      case '/withdraw_us':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => WithdrawUS(
            data: arguments["data"],
            source: arguments["source"],
          ),
        );
      case '/kyc':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => Verification(),
        );
      case '/withdraw-history':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => WithdrawHistory(
            source: arguments["source"],
          ),
        );
      case '/add-cash':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => AddCash(
            isFullScreen: true,
            source: arguments["source"],
            promoCodes: arguments["data"]["promoCodes"],
            paymentConfig: arguments["data"]["paymentConfig"],
            depositData: Deposit(
              paymentDetails:
                  PaymentDetails.fromJson(arguments["data"]["paymentData"]),
              bAllowRepeatDeposit: true,
              showLockedAmount: true,
            ),
            paymentComplete: arguments["onPaymentComplete"],
            prefilledAmount: arguments["prefilledAmount"] != null
                ? arguments["prefilledAmount"]
                : 0,
            prefilledPromoCode: arguments["prefilledPromoCode"] != null
                ? arguments["prefilledPromoCode"]
                : "",
          ),
        );
      case '/select-contacts':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => SelectContacts(
            refCode: arguments["refCode"],
            bonusAmount: arguments["bonusAmount"],
            contacts: arguments["contacts"],
          ),
        );
      case '/refer':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => EarnCash(
            data: arguments["data"],
          ),
        );
      case '/refer-v2':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => EarnCashV2(
            data: arguments["data"],
          ),
        );
      case '/help-center':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => HelpCenter(),
        );
      case '/static-page':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => StaticPage(
            url: arguments["url"],
            title: arguments["title"],
          ),
        );
      case '/payment-mode':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => PaymentMode(
            source: arguments["source"],
            amount: arguments["amount"],
            goldCount: arguments["goldCount"],
            bonusAmount: arguments["bonusAmount"],
            promo: arguments["promo"],
            paymentModeDetails: arguments["paymentModeDetails"],
            disclaimerText: arguments["disclaimerText"],
            stripe: arguments["stripe"],
          ),
        );
      case '/payout-mode':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => PayoutMode(
            source: arguments["source"],
            withdrawRequest: arguments["withdrawRequest"],
          ),
        );
      case '/chargeback':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => Chargeback(
            source: arguments["source"],
            data: arguments["data"],
          ),
        );
        break;

      case '/withdraw-address':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => WithdrawAddress(
            source: arguments["source"],
          ),
        );
        break;
      case '/chargeback-history':
        return SolitaireGoldPageRoute(
          pageBuilder: (context) => ChargebackHistory(
            source: arguments["source"],
          ),
        );
        break;
      default:
        return SolitaireGoldPageRoute(pageBuilder: (context) => SplashScreen());
    }
  }
}
