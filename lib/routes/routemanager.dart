import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/leaderboard.dart';
import 'package:solitaire_gold/models/lobby/mymatches.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/screens/lobby/ftue/ftue_route.dart';
import 'package:solitaire_gold/screens/solitairegold/solitairegold.dart';
import 'package:solitaire_gold/utils/appconfig.dart';

class RouteManager {
  launchLobbyScreen(BuildContext context) {
    return Navigator.pushReplacementNamed(context, "/lobby");
  }

  launchSigninScreen(BuildContext context) {
    return Navigator.pushReplacementNamed(context, "/signin");
  }

  launchSignupScreen(BuildContext context) {
    return Navigator.pushReplacementNamed(context, "/signup");
  }

  launchFTUE(BuildContext context, String source) {
    return Navigator.of(context).push(FTUERoute(source));
  }

  launchProfile(BuildContext context, String avatarUrl) {
    return Navigator.pushNamed(context, "/profile", arguments: {
      "url": avatarUrl,
    });
  }

  launchAddCash(BuildContext context,
      {Map<String, dynamic> data,
      Function onPaymentComplete,
      int prefilledAmount,
      String prefilledPromoCode,
      String source}) {
    return Navigator.pushNamed(context, "/add-cash", arguments: {
      "data": data,
      "prefilledAmount": prefilledAmount,
      "prefilledPromoCode": prefilledPromoCode,
      "onPaymentComplete": onPaymentComplete,
      "source": source
    });
  }

  launchPayoutMode(BuildContext context,
      {String source, Map<String, dynamic> withdrawRequest}) {
    return Navigator.pushNamed(context, "/payout-mode", arguments: {
      "withdrawRequest": withdrawRequest,
      "source": source,
    });
  }

  launchPaymentMode(
    BuildContext context, {
    String source,
    double amount,
    int goldCount,
    double bonusAmount,
    Map<String, dynamic> promo,
    Map<String, dynamic> paymentModeDetails,
    @required String disclaimerText,
    Map<String, dynamic> stripe,
  }) {
    return Navigator.pushNamed(context, "/payment-mode", arguments: {
      "source": source,
      "amount": amount.toInt(),
      "goldCount": goldCount,
      "bonusAmount": bonusAmount.toInt(),
      "promo": promo,
      "paymentModeDetails": paymentModeDetails,
      "disclaimerText": disclaimerText,
      "stripe": stripe,
    });
  }

  launchSolitaire(
    BuildContext context, {
    AsyncHeadsUp asyncHeadsUp,
    SmallLeague league,
    Leaderboard leaderboard,
    int selectedLobbyTabIndex,
    int tieMatchId = 0,
    int formatType,
  }) {
    if (SolitaireGold.isLaunched == false) {
      SolitaireGold.isLaunched = true;
      return Navigator.pushNamed(context, "/solitaire", arguments: {
        "asyncHeadsUp": asyncHeadsUp,
        "league": league,
        "leaderboard": leaderboard,
        "isReplay": false,
        "selectedLobbyTabIndex": selectedLobbyTabIndex,
        "tieMatchId": tieMatchId,
        "formatType": formatType,
      });
    }
  }

  launchInteractiveFTUE(BuildContext context) {
    if (SolitaireGold.isLaunched == false) {
      SolitaireGold.isLaunched = true;
      return Navigator.pushNamed(context, "/solitaire", arguments: {
        "isReplay": false,
        "isFTUE": true,
      });
    }
  }

  launchWithdraw(
    BuildContext context, {
    @required Map<String, dynamic> withdrawData,
    @required String source,
  }) {
    AppConfig config = Provider.of(context, listen: false);
    if (config.channelId < 200) {
      return Navigator.pushNamed(context, "/withdraw",
          arguments: {"data": withdrawData});
    } else {
      return Navigator.pushNamed(context, "/withdraw_us",
          arguments: {"data": withdrawData, "source": source});
    }
  }

  launchRAF(BuildContext context, {@required Map<String, dynamic> rafData}) {
    return Navigator.pushNamed(context, "/refer", arguments: {
      "data": rafData,
    });
  }

  launchRAFV2(BuildContext context, {@required Map<String, dynamic> rafData}) {
    return Navigator.pushNamed(context, "/refer-v2", arguments: {
      "data": rafData,
    });
  }

  launchAccountSummary(BuildContext context,
      {Map<String, dynamic> accountData}) {
    return Navigator.pushNamed(context, "/account", arguments: {
      "accountData": accountData,
    });
  }

  launchKYC(BuildContext context) {
    return Navigator.pushNamed(
      context,
      "/kyc",
    );
  }

  launchContactUs(BuildContext context) {
    return Navigator.pushNamed(
      context,
      "/contact-us",
    );
  }

  launchWithdrawHistory(BuildContext context, {String source}) {
    return Navigator.pushNamed(
      context,
      "/withdraw-history",
      arguments: {"source": source},
    );
  }

  launchSelectContacts(BuildContext context,
      {String refCode, int refAmount, List<Contact> contacts}) {
    return Navigator.pushNamed(
      context,
      "/select-contacts",
      arguments: {
        "refCode": refCode,
        "bonusAmount": refAmount,
        "contacts": contacts,
      },
    );
  }

  launchHelpCenter(BuildContext context) {
    return Navigator.pushNamed(
      context,
      "/help-center",
    );
  }

  launchStaticPage(
    BuildContext context, {
    @required String url,
    @required String title,
  }) {
    return Navigator.pushNamed(context, "/static-page", arguments: {
      "url": url,
      "title": title,
    });
  }

  launchChargeback(
    BuildContext context, {
    @required String source,
    @required Map<String, dynamic> withdrawData,
  }) {
    return Navigator.pushNamed(
      context,
      "/chargeback",
      arguments: {
        "source": source,
        "data": withdrawData,
      },
    );
  }

  launchWithdrawAddressDetails(
    BuildContext context, {
    @required String source,
    User user,
  }) {
    return Navigator.pushNamed(
      context,
      "/withdraw-address",
      arguments: {
        "source": source,
      },
    );
  }

  launchChargebackHistory(BuildContext context, {String source}) {
    return Navigator.pushNamed(
      context,
      "/chargeback-history",
      arguments: {"source": source},
    );
  }

  launchSolitaireReplay(BuildContext context, {MyMatch match}) {
    return Navigator.pushNamed(context, "/solitaire", arguments: {
      "isReplay": true,
      "matchId": match.matchId,
      "gameId": match.gameId,
    });
  }
}

RouteManager routeManager = RouteManager();
