import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:solitaire_gold/api/user/profileAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/commonwidgets/overlaymanager.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/profile/widgets/avatar_dialog.dart';
import 'package:solitaire_gold/screens/profile/widgets/statelist.dart';
import 'package:solitaire_gold/screens/profile/widgets/team_name_dialog.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

const double _kPickerSheetHeight = 216.0;

class Profile extends StatefulWidget {
  final String url;

  Profile({this.url});

  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> with BasePage {
  List<dynamic> states = List();
  GlobalKey _key = GlobalKey();
  String source = "app_drawer";
  Map<String, dynamic> selectedState;
  OverlayManager _overlayManager = OverlayManager();

  ProfileAPI _profileAPI = ProfileAPI();

  bool bIsDatePicked = false;
  bool bIsProfileEditable = false;
  bool showStateError = false;
  DateTime _date = DateTime(DateTime.now().year - 18);

  String _gender;
  String _userName;

  final TextEditingController _birthdayController = TextEditingController();
  final TextEditingController _cityController = TextEditingController();
  final TextEditingController _fNameController = TextEditingController();
  final TextEditingController _lNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _address1Controller = TextEditingController();
  final TextEditingController _pincodeController = TextEditingController();
  final TextEditingController _teamNameController = TextEditingController();
  final TextEditingController _stateController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _setData(context));
  }

  getStates(BuildContext context) async {
    User user = Provider.of(context, listen: false);

    Map<String, dynamic> result = await _profileAPI.getStateList(context);
    if (result["error"] == false) {
      states = result["data"];
      selectedState = user.address.state == null
          ? null
          : getStateFromCode(user.address.state);
      _stateController.text =
          selectedState != null ? selectedState["value"] : "";
    }
  }

  getStateFromCode(String code) {
    Map<String, dynamic> state =
        states.firstWhere((state) => state["code"] == code, orElse: () => null);
    if (state != null) return state;
    return null;
  }

  getStateNameFromCode(String code) {
    Map<String, dynamic> state = getStateFromCode(code);
    if (state != null) return state["value"];
    return "";
  }

  _setData(BuildContext context) {
    User user = Provider.of(context, listen: false);

    GamePlayAnalytics().onProfileScreenLoaded(
      context,
      source: source,
      teamName: user.loginName == null ? "None" : user.loginName,
      fname: user.address.firstName != null ? user.address.firstName : "",
      lname: user.address.lastName != null ? user.address.lastName : "",
      gender: user.gender,
      dob: user.dob != null ? user.dob : "",
      email: user.email,
      mobile: user.mobile,
      state: user.address.state != null ? user.address.state : "",
      city: user.address.city,
      pincode: user.address.pincode.toString(),
      avatarId: user.avatar,
    );

    getStates(context);
    initTextControllers(user);
  }

  initTextControllers(User user) {
    setState(() {
      _userName = user.loginName;
      _gender = user.gender == null ? null : user.gender.toLowerCase();
      if (user.dob != null)
        _date = DateTime(
          int.tryParse(user.dob.split("-")[0]),
          int.tryParse(user.dob.split("-")[1]),
          int.tryParse(user.dob.split("-")[1]),
        );
    });
    if (user.dob != null) {
      _date = DateTime.parse(user.dob);
      _birthdayController.text = getDate();
    }
    _cityController.text = user.address.city;
    _fNameController.text = user.address.firstName;
    _lNameController.text = user.address.lastName;
    _phoneController.text = user.mobile;
    _emailController.text = user.email;
    _address1Controller.text = user.address.address1;
    _pincodeController.text =
        user.address.pincode == null ? "" : user.address.pincode.toString();
    _stateController.text = getStateNameFromCode(user.address.state);

    _fNameController.addListener(enableProfileUpdate);
    _lNameController.addListener(enableProfileUpdate);
    _birthdayController.addListener(enableProfileUpdate);
    _phoneController.addListener(enableProfileUpdate);
    _emailController.addListener(enableProfileUpdate);
    _address1Controller.addListener(enableProfileUpdate);
    _cityController.addListener(enableProfileUpdate);
    _pincodeController.addListener(enableProfileUpdate);
  }

  void enableProfileUpdate() {
    setState(() {
      bIsProfileEditable = true;
    });
  }

  allowNameDobEdit(User user) {
    return user.verificationStatus.addressVerificationStatus != "VERIFIED" &&
        user.verificationStatus.addressVerificationStatus != "DOC_REJECTED" &&
        user.verificationStatus.addressVerificationStatus != "UNDER_REVIEW" &&
        user.verificationStatus.addressVerificationStatus != "DOC_SUBMITTED";
  }

  _showChangeTeamNameDialog() {
    User user = Provider.of(context, listen: false);
    if (user.isUserNameChangeAllowed) {
      final _formKey = GlobalKey<FormState>();
      final FocusNode focusNode = FocusNode();
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (BuildContext context) {
          return TeamNameDialog(
            changeTeamName: (BuildContext context) {
              _changeTeamName(context);
            },
            formkey: _formKey,
            focusNode: focusNode,
            textController: _teamNameController,
          );
        },
      );
      FocusScope.of(context).requestFocus(focusNode);
    } else {
      showMessageOnTop(context, msg: "Username can not be changed again!!!");
    }
  }

  _changeTeamName(BuildContext context) async {
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }

    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> result =
        await _profileAPI.updateTeamName(context, _teamNameController.text);

    Loader().showLoader(false);

    if (result["error"]) {
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      result["message"],
                      style:
                          Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                                color: Colors.grey.shade700,
                              ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
    } else {
      Navigator.of(context).pop();
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Username changed successfully.",
                      style:
                          Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                                color: Colors.grey.shade700,
                              ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
      User user = Provider.of(context, listen: false);
      user.setIsUserNameChangeAllowed(false);
      setState(() {
        _userName = _teamNameController.text;
      });
    }
  }

  Future<Null> _selectDate(BuildContext context, Function onChange) async {
    FocusScope.of(context).unfocus();
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return _buildBottomPicker(
          CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            maximumDate: DateTime.now(),
            initialDateTime: _date,
            onDateTimeChanged: (DateTime newDateTime) {
              onChange(newDateTime);
            },
          ),
        );
      },
    );
  }

  Widget _buildBottomPicker(Widget picker) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(right: 8.0),
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "DONE",
                    style:
                        Theme.of(context).primaryTextTheme.headline6.copyWith(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                            ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: _kPickerSheetHeight,
            color: CupertinoColors.white,
            child: DefaultTextStyle(
              style: const TextStyle(
                color: CupertinoColors.black,
                fontSize: 22.0,
              ),
              child: GestureDetector(
                onTap: () {},
                child: SafeArea(
                  top: false,
                  child: picker,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  getDate() {
    return (_date.year.toString() +
        "/" +
        (_date.month >= 10
            ? _date.month.toString()
            : ("0" + _date.month.toString())) +
        "/" +
        (_date.day >= 10
            ? _date.day.toString()
            : ("0" + _date.day.toString())));
  }

  Map<String, dynamic> getUserProfileObject() {
    User user = Provider.of(context, listen: false);
    DateFormat dateFormat = DateFormat("yyyy-MM-dd");
    Map<String, dynamic> payload = {
      "address": {
        if (user.address.firstName != _fNameController.text)
          "first_name": _fNameController.text,
        if (user.address.lastName != _lNameController.text)
          "last_name": _lNameController.text,
        if (user.address.address1 != _address1Controller.text)
          "add_line_1": _address1Controller.text,
        if (user.address.city != _cityController.text)
          "city": _cityController.text,
        if (user.address.pincode.toString() != _pincodeController.text)
          "pin": _pincodeController.text,
      },
      "info": {
        if (user.email != _emailController.text) "email": _emailController.text,
        if (user.mobile != _phoneController.text)
          "phone": _phoneController.text,
        if (user.gender != _gender) "gender": _gender,
        if (bIsDatePicked) "dob": dateFormat.format(_date),
        if (user.address.state != selectedState["code"])
          "state": selectedState["code"],
      },
    };

    return payload;
  }

  onSaveProfile() async {
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }

    AppConfig config = Provider.of(context, listen: false);
    if (selectedState == null) {
      setState(() {
        showStateError = true;
      });
    }
    if (_pincodeController.text.indexOf(".") != -1 ||
        _pincodeController.text.indexOf(" ") != -1 ||
        _pincodeController.text.indexOf(",") != -1 ||
        _pincodeController.text.indexOf("-") != -1) {
      showMessageOnTop(context,
          msg: config.channelId < 200
              ? "Invalid pincode entered"
              : "Invalid zipcode entered");
      return null;
    }
    User user = Provider.of(context, listen: false);
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      FocusScope.of(context).unfocus();

      Loader().showLoader(true, immediate: true);

      Map<String, dynamic> result =
          await _profileAPI.updateProfile(context, getUserProfileObject());

      Loader().showLoader(false);

      if (!result["error"]) {
        user.copyFrom(result["data"]);
        GamePlayAnalytics().onProfileUpdated(
          context,
          source: source,
          teamName: user.loginName == null ? "None" : user.loginName,
          fname: user.address.firstName,
          lname: user.address.lastName,
          gender: user.gender,
          dob: user.dob != null ? user.dob : "",
          email: user.email,
          mobile: user.mobile,
          state: user.address.state != null ? user.address.state : "",
          city: user.address.city,
          pincode: user.address.pincode.toString(),
          avatarId: user.avatar,
        );
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) {
            return SimpleDialog(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Profile updated successfully.",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 78.0),
                  child: Button(
                    size: ButtonSize.medium,
                    text: "OK",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          },
        );

        initTextControllers(user);
        setState(() {
          bIsProfileEditable = false;
        });
      } else {
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) {
            return SimpleDialog(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        result["message"],
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 78.0),
                  child: Button(
                    size: ButtonSize.medium,
                    text: "OK",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          },
        );
      }
    }
  }

  showStateSelection() {
    FocusScope.of(context).unfocus();
    double height = 400.0;

    int delay = 0;
    if (FocusScope.of(context).focusedChild != null) {
      delay = 50;
    }

    Future.delayed(Duration(milliseconds: delay), () {
      double width = MediaQuery.of(context).size.width - 140;
      Offset offset = Offset(MediaQuery.of(context).size.width / 2 - width / 2,
          MediaQuery.of(context).size.height / 2 - height / 2);

      _overlayManager.showOverlay(
        context,
        offset,
        Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6.0),
            ),
            clipBehavior: Clip.hardEdge,
            child: StateList(
              stateData: states,
              onSelect: (Map<String, dynamic> state) {
                _overlayManager.closeAll();
                setState(() {
                  selectedState = state;
                  bIsProfileEditable = true;
                });
              },
            ),
          ),
        ),
      );
    });
  }

  Widget getDropdown({String label, bool enabled, String placeholder}) {
    Widget dropdown = enabled != true
        ? getListItemTextBox(
            label: label,
            placeholder: "State",
            controller: _stateController,
            suffixIconConstraints: BoxConstraints(maxHeight: 20.0),
            enabled: false)
        : Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: 120.0,
                  height: 32.0,
                  alignment: Alignment.centerLeft,
                  child: Text(
                    label,
                    style:
                        Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                              color: Colors.grey.shade700,
                              fontWeight: FontWeight.w600,
                            ),
                  ),
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            InkWell(
                              onTap: enabled
                                  ? () {
                                      showStateSelection();
                                    }
                                  : null,
                              child: Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Flexible(
                                      child: Text(
                                        selectedState != null
                                            ? selectedState["value"]
                                            : placeholder,
                                        overflow: TextOverflow.ellipsis,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .subtitle1
                                            .copyWith(
                                              color: Colors.black,
                                            ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_drop_down,
                                      color: enabled
                                          ? Colors.black
                                          : Colors.grey.shade300,
                                      key: _key,
                                    ),
                                  ],
                                ),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16.0, vertical: 3.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.0),
                                  border: Border.all(
                                    width: 1.0,
                                    color: enabled
                                        ? Colors.grey.shade500
                                        : Colors.grey.shade300,
                                  ),
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            showStateError
                                ? Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "  *Required",
                                      style: TextStyle(
                                          color: Theme.of(context).errorColor,
                                          fontSize: 12),
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );

    return dropdown;
  }

  Widget getListItemTextBox({
    String label,
    String placeholder,
    TextEditingController controller,
    TextInputType keyboardType = TextInputType.text,
    int maxLength,
    Widget suffixIcon,
    BoxConstraints suffixIconConstraints,
    bool enabled = true,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 120.0,
            height: 32.0,
            alignment: Alignment.centerLeft,
            child: Text(
              label,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.grey.shade700,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Expanded(
            child: SimpleTextBox(
              controller: controller,
              keyboardType: keyboardType,
              suffixIcon: suffixIcon,
              suffixIconConstraints: BoxConstraints(maxHeight: 20.0),
              labelText: placeholder != null ? placeholder : label,
              enabled: enabled,
              contentPadding: EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 8.0,
              ),
              borderColor: Colors.grey.shade500,
              focusedBorderColor: Colors.grey.shade700,
              isDense: true,
              validator: (value) {
                if (value.isEmpty) {
                  return "*Required";
                } else if (maxLength != null && value.length > maxLength) {
                  return "Maximum " +
                      maxLength.toString() +
                      " characters allowed";
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget getCheckBox({
    @required String label,
    @required bool isChecked,
    @required Function onPressed,
  }) {
    return InkWell(
      child: Row(
        children: <Widget>[
          Container(
            width: 20.0,
            height: 20.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                width: 1.0,
                color: Colors.grey.shade400,
              ),
            ),
            alignment: Alignment.center,
            child: Container(
              height: 16.0,
              width: 16.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isChecked ? Colors.green : Colors.transparent,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              label,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.grey.shade600,
                  ),
            ),
          ),
        ],
      ),
      onTap: onPressed != null
          ? () {
              onPressed();
            }
          : null,
    );
  }

  Widget getGenderWiget({@required String label}) {
    User user = Provider.of(context, listen: false);

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        children: <Widget>[
          Container(
            width: 120.0,
            child: Text(
              label,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.grey.shade700,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Expanded(
            child: Row(
              children: <Widget>[
                getCheckBox(
                  label: "Male",
                  isChecked: _gender == "male",
                  onPressed: user.gender == null
                      ? () {
                          setState(() {
                            _gender = "male";
                            bIsProfileEditable = true;
                          });
                        }
                      : null,
                ),
                SizedBox(
                  width: 16.0,
                ),
                getCheckBox(
                  label: "Female",
                  isChecked: _gender == "female",
                  onPressed: user.gender == null
                      ? () {
                          setState(() {
                            _gender = "female";
                            bIsProfileEditable = true;
                          });
                        }
                      : null,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget getListItemTextBoxWithSuffix(
      {String label,
      String placeholder,
      TextEditingController controller,
      Widget suffix,
      bool enabled = true}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 120.0,
            height: 32.0,
            alignment: Alignment.centerLeft,
            child: Text(
              label,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.grey.shade700,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Expanded(
            child: SimpleTextBox(
              controller: controller,
              labelText: placeholder != null ? placeholder : label,
              enabled: enabled,
              contentPadding: EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 8.0,
              ),
              borderColor: Colors.grey.shade500,
              focusedBorderColor: Colors.grey.shade700,
              isDense: true,
              validator: (String value) {
                if (value.isEmpty) {
                  return "*Required";
                }
                return null;
              },
            ),
          ),
          if (suffix != null) suffix
        ],
      ),
    );
  }

  Widget getCurrentAvatar({BuildContext context}) {
    User user = Provider.of(context, listen: false);
    return Container(
      margin: EdgeInsets.only(right: 16.0, left: 16.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        border: Border.all(
          width: 2.0,
          color: Color.fromRGBO(188, 185, 160, 1),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 1.0,
            spreadRadius: 0.5,
            offset: Offset(0.0, 2.0),
          )
        ],
      ),
      child: ClipRRect(
        clipBehavior: Clip.hardEdge,
        borderRadius: BorderRadius.circular(
          50,
        ),
        child: CachedNetworkImage(
          imageUrl: widget.url + user.avatar.toString() + ".png",
          fit: BoxFit.fitHeight,
          placeholder: (context, string) {
            return Container(
              padding: EdgeInsets.all(16.0),
              child: CircularProgressIndicator(
                strokeWidth: 2.0,
              ),
              width: 64,
              height: 64,
            );
          },
          height: 64,
          width: 64,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _overlayManager.closeAll();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    User userData = Provider.of(context, listen: false);
    return LoaderContainer(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CommonAppBar(
          children: <Widget>[
            Text(
              "Personal Info".toUpperCase(),
            ),
          ],
        ),
        body: ChangeNotifierProvider.value(
          value: userData,
          child: Consumer<User>(
            builder: (context, user, child) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.all(12.0),
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 4.0,
                            spreadRadius: 0,
                            offset: Offset(0, 3.0),
                          ),
                        ],
                      ),
                      child: SingleChildScrollView(
                        child: Form(
                          key: _formKey,
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 16.0),
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    InkWell(
                                        onTap: () {
                                          GamePlayAnalytics()
                                              .onDisplayPicClicked(
                                            context,
                                            avatarId: user.avatar,
                                          );
                                          showDialog(
                                            barrierColor:
                                                Color.fromARGB(175, 0, 0, 0),
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AvatarDialog(
                                                  url: widget.url);
                                            },
                                          );
                                        },
                                        child:
                                            getCurrentAvatar(context: context)),
                                    Expanded(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Text(
                                                "Username",
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .subtitle1
                                                    .copyWith(
                                                      color:
                                                          Colors.grey.shade700,
                                                    ),
                                              ),
                                              Container(
                                                width: 40.0,
                                                height: 24.0,
                                                child: FlatButton(
                                                  onPressed:
                                                      user.isUserNameChangeAllowed
                                                          ? () {
                                                              _showChangeTeamNameDialog();
                                                            }
                                                          : null,
                                                  padding: EdgeInsets.zero,
                                                  child: Text(
                                                    "Edit",
                                                    style: Theme.of(context)
                                                        .primaryTextTheme
                                                        .subtitle1
                                                        .copyWith(
                                                          color:
                                                              user.isUserNameChangeAllowed
                                                                  ? Colors.blue
                                                                  : Colors.grey,
                                                          fontStyle:
                                                              FontStyle.italic,
                                                        ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text(
                                                _userName == null
                                                    ? ""
                                                    : _userName,
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .headline6
                                                    .copyWith(
                                                      color: Colors.black,
                                                    ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 16.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: SimpleTextBox(
                                          controller: _fNameController,
                                          labelText: "First Name",
                                          enabled: allowNameDobEdit(user) ||
                                              user.address.firstName == null,
                                          contentPadding: EdgeInsets.symmetric(
                                            horizontal: 16.0,
                                            vertical: 8.0,
                                          ),
                                          borderColor: Colors.grey.shade500,
                                          focusedBorderColor:
                                              Colors.grey.shade700,
                                          isDense: true,
                                          validator: (String value) {
                                            if (value.isEmpty) {
                                              return "*Required";
                                            }
                                            return null;
                                          },
                                        ),
                                      ),
                                      SizedBox(
                                        width: 8.0,
                                      ),
                                      Expanded(
                                        child: SimpleTextBox(
                                          controller: _lNameController,
                                          labelText: "Last Name",
                                          enabled: allowNameDobEdit(user) ||
                                              user.address.lastName == null,
                                          contentPadding: EdgeInsets.symmetric(
                                            horizontal: 16.0,
                                            vertical: 8.0,
                                          ),
                                          borderColor: Colors.grey.shade500,
                                          focusedBorderColor:
                                              Colors.grey.shade700,
                                          isDense: true,
                                          validator: (String value) {
                                            if (value.isEmpty) {
                                              return "*Required";
                                            }
                                            return null;
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                getGenderWiget(label: "Gender*"),
                                InkWell(
                                  onTap: (config.channelId < 200 &&
                                              allowNameDobEdit(user)) ||
                                          user.dob == null
                                      ? () {
                                          _selectDate(
                                            context,
                                            (DateTime newDate) {
                                              _date = newDate;
                                              bIsDatePicked = true;
                                              _birthdayController.text =
                                                  getDate();
                                            },
                                          );
                                        }
                                      : null,
                                  child: getListItemTextBoxWithSuffix(
                                    label: "Birthday*",
                                    placeholder: "YYYY / MM / DD",
                                    controller: _birthdayController,
                                    enabled: false,
                                    suffix: Container(
                                      height: 32.0,
                                      padding: EdgeInsets.only(left: 12.0),
                                      child: Image.asset(
                                        "images/calendar-icon.png",
                                      ),
                                    ),
                                  ),
                                ),
                                getListItemTextBox(
                                  label: "Mobile*",
                                  placeholder: "Mobile Number",
                                  controller: _phoneController,
                                  keyboardType: TextInputType.phone,
                                  enabled:
                                      user.verificationStatus.mobileVerified !=
                                          true,
                                ),
                                getListItemTextBox(
                                  label: "Email*",
                                  placeholder: "Email",
                                  controller: _emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  enabled:
                                      user.verificationStatus.emailVerified !=
                                          true,
                                ),
                                getListItemTextBox(
                                  label: "Address Line 1*",
                                  placeholder: "Address",
                                  controller: _address1Controller,
                                  enabled: user.address.address1 == null,
                                ),
                                getDropdown(
                                  label: "State*",
                                  placeholder: user.address.state == null
                                      ? "State"
                                      : getStateNameFromCode(
                                          user.address.state),
                                  enabled: user.address.state == null,
                                ),
                                getListItemTextBox(
                                  label: "City*",
                                  placeholder: "City",
                                  controller: _cityController,
                                  enabled: user.address.city == null ||
                                      user.address.city.isEmpty,
                                ),
                                getListItemTextBox(
                                  label: config.channelId < 200
                                      ? "Pincode*"
                                      : "Zipcode*",
                                  placeholder: config.channelId < 200
                                      ? "Pincode"
                                      : "Zipcode",
                                  controller: _pincodeController,
                                  keyboardType: TextInputType.number,
                                  maxLength: 6,
                                  enabled: user.address.pincode == null ||
                                      user.address.pincode == 0,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Flexible(
                                        child: RichText(
                                          text: TextSpan(children: [
                                            TextSpan(
                                                text:
                                                    "Report A Problem (if any)",
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .subtitle1
                                                    .copyWith(
                                                      color: Colors.blue,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 14.0,
                                                    ),
                                                recognizer:
                                                    TapGestureRecognizer()
                                                      ..onTap = () {
                                                        FocusScope.of(context)
                                                            .unfocus();
                                                        routeManager
                                                            .launchContactUs(
                                                                context);
                                                      }),
                                          ]),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Button(
                                          onPressed: bIsProfileEditable
                                              ? () {
                                                  onSaveProfile();
                                                }
                                              : null,
                                          text: "UPDATE PROFILE",
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline5
                                              .copyWith(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
