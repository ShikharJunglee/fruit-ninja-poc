import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/api/user/profileAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:provider/provider.dart';

class AvatarDialog extends StatefulWidget {
  final String url;
  AvatarDialog({this.url});
  @override
  _AvatarDialogState createState() => _AvatarDialogState();
}

class _AvatarDialogState extends State<AvatarDialog> {
  int currentAvatar;
  List<dynamic> avatarMap = List<dynamic>();
  ProfileAPI _profileAPI = ProfileAPI();

  _AvatarDialogState();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _getAvatarList(context));
  }

  _getAvatarList(BuildContext context) async {
    User user = Provider.of(context, listen: false);
    Map<String, dynamic> result = await _profileAPI.getAvatars(context);
    setState(() {
      currentAvatar = user.avatar;
    });
    if (result["error"] == false) {
      setState(() {
        avatarMap = result["data"];
      });
    }
    GamePlayAnalytics().onAvatarPopUpLoaded(context, avatarId: user.avatar);
  }

  _updateAvatar() async {
    Map<String, dynamic> payload = {
      "address": {},
      "info": {"avatarId": currentAvatar}
    };
    var response = await _profileAPI.updateAvatar(context, payload);
    if (!response["error"]) {
      User user = Provider.of(context, listen: false);
      user.setAvatar(currentAvatar);
      Navigator.of(context).pop();
    }
  }

  Widget getCurrentAvatar({BuildContext context, String id}) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.white,
        border: Border.all(
          width: 2.0,
          color: Color.fromRGBO(188, 185, 160, 1),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 1.0,
            spreadRadius: 0.5,
            offset: Offset(0.0, 2.0),
          )
        ],
      ),
      child: ClipRRect(
          clipBehavior: Clip.hardEdge,
          borderRadius: BorderRadius.circular(
            50,
          ),
          child: CachedNetworkImage(
            imageUrl: widget.url + id + ".png",
            fit: BoxFit.fitHeight,
            placeholder: (context, string) {
              return Container(
                padding: EdgeInsets.all(4.0),
                child: Container(
                  padding: EdgeInsets.all(16.0),
                  child: CircularProgressIndicator(
                    strokeWidth: 2.0,
                  ),
                ),
                width: 50,
                height: 50,
              );
            },
            height: 100,
            width: 100,
          )),
    );
  }

  Widget getAvatar({BuildContext context, String url, int id}) {
    return Container(
      width: 75,
      height: 75,
      padding: EdgeInsets.all(8),
      child: Stack(
        fit: StackFit.expand,
        alignment: Alignment.topRight,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              border: Border.all(
                width: 2.0,
                color: Color.fromRGBO(188, 185, 160, 1),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black12,
                  blurRadius: 1.0,
                  spreadRadius: 0.5,
                  offset: Offset(0.0, 2.0),
                )
              ],
            ),
            child: InkWell(
              onTap: () {
                User user = Provider.of(context, listen: false);
                GamePlayAnalytics().onAvatarClicked(
                  context,
                  avatarId: user.avatar,
                  newAvatarId: id,
                );
                setState(() {
                  currentAvatar = id;
                });
              },
              child: ClipRRect(
                clipBehavior: Clip.hardEdge,
                borderRadius: BorderRadius.circular(
                  8,
                ),
                child: CachedNetworkImage(
                  imageUrl: widget.url + id.toString() + ".png",
                  fit: BoxFit.fitHeight,
                  placeholder: (context, string) {
                    return Container(
                      padding: EdgeInsets.all(16.0),
                      child: CircularProgressIndicator(
                        strokeWidth: 2.0,
                      ),
                      width: 50,
                      height: 50,
                    );
                  },
                  height: 100,
                  width: 100,
                ),
              ),
            ),
          ),
          currentAvatar == id
              ? Positioned(
                  bottom: 0,
                  right: 0,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(24.0),
                    ),
                    child: Icon(
                      Icons.check_circle,
                      color: Colors.green,
                      size: 16.0,
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }

  Widget getAvatarList() {
    List<Widget> avatarList = [];
    avatarMap.forEach((element) {
      avatarList.add(
          getAvatar(context: context, url: element["name"], id: element["id"]));
    });
    return Container(
      height: 200,
      constraints: BoxConstraints(minWidth: 300),
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 254, 246, 1),
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: Color.fromRGBO(211, 211, 211, 1),
        ),
      ),
      child: SingleChildScrollView(
        child: Wrap(
          children: avatarList,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: Text(
                          "Choose Your Avatar",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline6
                              .copyWith(
                                  color: Colors.grey.shade800,
                                  fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        height: 100,
                        width: 100,
                        child: getCurrentAvatar(
                            context: context, id: currentAvatar.toString()),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: getAvatarList(),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                            vertical: 20, horizontal: 26.0),
                        child: Button(
                          size: ButtonSize.large,
                          text: "Update Avatar".toUpperCase(),
                          onPressed: () {
                            User user = Provider.of(context, listen: false);
                            GamePlayAnalytics().onUpdateAvatarClicked(context,
                                avatarId: user.avatar,
                                newAvatarId: currentAvatar);
                            _updateAvatar();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            IconButton(
              padding: EdgeInsets.all(0.0),
              icon: Image.asset(
                "images/icons/close2.png",
                height: 32.0,
              ),
              onPressed: () {
                User user = Provider.of(context, listen: false);
                GamePlayAnalytics().onCloseAvatarClicked(
                  context,
                  avatarId: user.avatar,
                  newAvatarId: currentAvatar,
                );
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}
