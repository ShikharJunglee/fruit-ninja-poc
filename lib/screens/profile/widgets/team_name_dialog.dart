import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';

class TeamNameDialog extends StatelessWidget {
  final FocusNode focusNode;
  final Function changeTeamName;
  final GlobalKey<FormState> formkey;
  final TextEditingController textController;

  TeamNameDialog(
      {this.formkey, this.focusNode, this.textController, this.changeTeamName});

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Edit Username".toUpperCase(),
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Colors.grey.shade800,
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 32.0, vertical: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 16.0),
                            child: Row(
                              children: <Widget>[
                                Form(
                                  key: formkey,
                                  child: Expanded(
                                    child: SimpleTextBox(
                                      controller: textController,
                                      isDense: true,
                                      hintText: "Type your username",
                                      borderColor: Colors.grey.shade700,
                                      focusedBorderColor: Colors.grey.shade700,
                                      focusNode: focusNode,
                                      maxLines: 1,
                                      validator: (value) {
                                        if (value.isEmpty || value.length < 4) {
                                          return "Minimum 4 characters required.";
                                        } else if (value.length > 12) {
                                          return "Maximum 12 characters allowed.";
                                        } else if (value.contains(RegExp(
                                                '[@#\$?(),!/:{}><;`*~%^&+=]')) ==
                                            true) {
                                          return "Name must contains characters, numbers, '.', '-', and '_' symbols only";
                                        } else if (value.indexOf(" ") != -1) {
                                          return "Name should not contain space";
                                        } else if (!value
                                            .startsWith(RegExp('[A-Za-z]'))) {
                                          return "Name must starts with character.";
                                        }

                                        return null;
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                                "You can not change your username more than once"),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 140.0,
                            child: Button(
                              size: ButtonSize.medium,
                              text: "Save".toUpperCase(),
                              onPressed: () {
                                if (formkey.currentState.validate()) {
                                  changeTeamName(context);
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            IconButton(
              padding: EdgeInsets.all(0.0),
              icon: Image.asset(
                "images/icons/close2.png",
                height: 32.0,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}
