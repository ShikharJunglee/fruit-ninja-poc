import 'dart:math';

import 'package:flutter/material.dart';

class StateList extends StatefulWidget {
  final List<dynamic> stateData;
  final Function onSelect;

  StateList({this.stateData, this.onSelect});

  @override
  _StateListState createState() => _StateListState();
}

class _StateListState extends State<StateList> {
  ScrollController scrollController = ScrollController();

  List<Widget> getStateList() {
    int i = 0;
    List<Widget> stateWidgets = [];
    widget.stateData.forEach((state) {
      stateWidgets.add(
        InkWell(
          onTap: () {
            widget.onSelect(state);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 20.0),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey.shade300, width: 1.0),
                  top: i == 0
                      ? BorderSide(color: Colors.grey.shade300, width: 1.0)
                      : BorderSide(color: Colors.grey.shade300, width: 0.0),
                ),
              ),
              child: Text(
                state["value"],
              ),
            ),
          ),
        ),
      );
      i++;
    });
    return stateWidgets;
  }

  void scrollDropdownDown() {
    scrollController.animateTo(scrollController.offset + 55,
        curve: Curves.linear, duration: Duration(milliseconds: 300));
  }

  void scrollDropdownUp() {
    scrollController.animateTo(scrollController.offset - 55,
        curve: Curves.linear, duration: Duration(milliseconds: 300));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          FlatButton(
            onPressed: () {
              scrollDropdownUp();
            },
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Transform.rotate(
                    angle: pi * 1.5,
                    child: Container(
                      child: Icon(Icons.chevron_right),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: getStateList(),
              ),
            ),
          ),
          FlatButton(
            onPressed: () {
              scrollDropdownDown();
            },
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Transform.rotate(
                    angle: pi * 0.5,
                    child: Container(
                      child: Icon(Icons.chevron_right),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
