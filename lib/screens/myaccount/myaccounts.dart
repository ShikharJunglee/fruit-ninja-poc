import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/api/raf/earncashAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/myaccount/account.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/generic_dialog.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/screens/lobby/addcash/add_cash_helper.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw_helper.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';

class MyAccount extends StatefulWidget {
  final Map<String, dynamic> accountData;

  MyAccount({this.accountData});

  @override
  MyAccountState createState() => MyAccountState();
}

class MyAccountState extends State<MyAccount> with BasePage {
  String cookie = "";
  Account accountDetails = Account();
  EarnCashAPI _earnCashAPI = EarnCashAPI();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  List<dynamic> transactionTypes = [
    {
      "label": "All",
      "value": ["all"]
    },
    {
      "label": "Cash",
      "value": ["cash"]
    },
    {
      "label": "Gems",
      "value": ["gems"]
    },
    {
      "label": "Deposit/Withdrawals",
      "value": ["deposit", "withdraw"]
    },
  ];

  Map<String, dynamic> selectedTransactionType;

  @override
  void initState() {
    super.initState();
    accountDetails = Account.fromJson(widget.accountData);
    selectedTransactionType = transactionTypes[0];
    GamePlayAnalytics().onAccountSummaryLoaded(
      context,
      source: "app_drawer",
    );
  }

  _launchAddCash(BuildContext context, {String source}) async {
    AnalyticsManager().setJourney("Deposit");
    GamePlayAnalytics().onAddCashClicked(
      context,
      source: source,
    );
    Loader().showLoader(true);

    Map<String, dynamic> result = await AddCashHelper().authAddcash(context);

    Loader().showLoader(false);
    if (result["error"] == true) {
      if (result["message"] != null) {
        await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) => GenericDialog(
            letterSpacing: 2.2,
            contentText: Text(
              "${result["message"]}",
              textAlign: TextAlign.center,
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.white,
                  ),
            ),
            buttonText: "Ok".toUpperCase(),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        );
      }
    } else {
      await routeManager.launchAddCash(context,
          data: result["data"], source: "account_summary");
      AnalyticsManager().setJourney(null);
    }
  }

  _launchWithdraw(BuildContext context, String source) async {
    WithdrawHelper().launchWithdraw(
      context,
      source: "account_summary",
      onErrorMessage: (String message) {
        showDialog(
          context: context,
          builder: (context) {
            return GenericDialog(
              contentText: Text(
                message,
                textAlign: TextAlign.center,
                style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                      color: Colors.white,
                    ),
              ),
              buttonText: "Ok".toUpperCase(),
              onPressed: () {
                Navigator.of(context).pop();
              },
            );
          },
        );
      },
    );
  }

  _launchEarnCash(BuildContext context, String source) async {
    Loader().showLoader(true, immediate: true);

    final result = await _earnCashAPI.getReferalData(context);

    Loader().showLoader(false);

    if (result["error"] != true) {
      User user = Provider.of(context, listen: false);
      user.copyFrom(result["data"]["user"]);

      if (result["data"]["rafData"]["refDetails"]["screenIndex"] == 1) {
        return routeManager.launchRAF(context,
            rafData: result["data"]["rafData"]);
      } else {
        return routeManager.launchRAFV2(context,
            rafData: result["data"]["rafData"]);
      }
    }
  }

  getAbsoluteAmount(double amount) {
    String amountInString = amount.toStringAsFixed(2);
    try {
      var decimalAmount = amountInString.split('.');
      if (decimalAmount[1] == "00") {
        amountInString = amount.toStringAsFixed(0);
      }
    } catch (e) {}
    return amountInString;
  }

  Widget getDistributionButton(
    String title,
    double balance,
    String buttonText,
    Function onPressed, {
    String title1,
    double balance1,
  }) {
    InitData initData = Provider.of(context, listen: false);
    return Container(
      padding: EdgeInsets.only(top: 8.0),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey.shade300,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.0),
                      child: Text(
                        title,
                        style: TextStyle(
                          fontSize: Theme.of(context)
                              .primaryTextTheme
                              .bodyText2
                              .fontSize,
                        ),
                      ),
                    ),
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: CurrencyText(
                        showCurrencyDelimiters: false,
                        amount: balance,
                        decimalDigits:
                            initData.experiments.miscConfig.amountPrecision,
                        style: TextStyle(
                            fontSize: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .fontSize,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              if (balance1 != null && balance1 > 0)
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Text(
                          title1,
                          style: TextStyle(
                            fontSize: Theme.of(context)
                                .primaryTextTheme
                                .bodyText2
                                .fontSize,
                          ),
                        ),
                      ),
                      FittedBox(
                        fit: BoxFit.scaleDown,
                        child: CurrencyText(
                          showCurrencyDelimiters: false,
                          amount: balance1,
                          decimalDigits:
                              initData.experiments.miscConfig.amountPrecision,
                          style: TextStyle(
                              fontSize: Theme.of(context)
                                  .primaryTextTheme
                                  .headline6
                                  .fontSize,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Button(
              size: ButtonSize.medium,
              text: buttonText.toUpperCase(),
              style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w900,
                    letterSpacing: 1.2,
                  ),
              onPressed: () {
                onPressed();
              },
            ),
          ),
        ],
      ),
    );
  }

  List<DropdownMenuItem> _getTransactionTypes() {
    List<DropdownMenuItem> _lstMenuItems = [];
    if (transactionTypes != null) {
      for (Map<String, dynamic> transaction in transactionTypes) {
        _lstMenuItems.add(
          DropdownMenuItem(
            child: Container(
              width: 140.0,
              child: Text(
                transaction["label"],
                style: TextStyle(fontSize: 16),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            value: transaction,
          ),
        );
      }
    } else {
      _lstMenuItems.add(
        DropdownMenuItem(
          child: Container(
            width: 140.0,
            child: Text(
              "",
            ),
          ),
          value: "",
        ),
      );
    }
    return _lstMenuItems;
  }

  Widget getTransactionFilter() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              height: 40,
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 8.0),
              child: Text(
                "Transaction Type",
                style: TextStyle(fontSize: 16.0),
              )),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(left: 24.0),
              padding: const EdgeInsets.only(left: 24.0, right: 8.0),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey.shade300,
                ),
                borderRadius: BorderRadius.circular(6.0),
              ),
              child: DropdownButton(
                items: _getTransactionTypes(),
                underline: Container(),
                value: selectedTransactionType,
                onChanged: (value) {
                  setState(() {
                    selectedTransactionType = value;
                  });
                },
                isExpanded: true,
              ),
            ),
            // Dropdown(
            //   label: "All",
            //   valueKey: "label",
            //   dropDownItems: transactionTypes,
            //   onSelectCallback: (Map<String, dynamic> selectedItem) {
            //     setState(() {
            //       selectedTransactionType = selectedItem;
            //     });
            //   },
            // ),
          ),
        ],
      ),
    );
  }

  getFilteredTransactions() {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> transactionItems = [];
    for (Transaction transaction in accountDetails.recentTransactions) {
      if (selectedTransactionType == null ||
          (transaction.filterTypes.any((element) =>
              selectedTransactionType["value"].contains(element)))) {
        transactionItems.add(Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Text(
                  transaction.type.toString(),
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.grey.shade600,
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 4.0),
                  child: Text(
                    transaction.date,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.grey.shade600,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 4.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      if (transaction.isGoldbarTransaction)
                        Padding(
                          padding: const EdgeInsets.only(right: 1.0),
                          child: Image.asset(
                            "images/currency/diamond.png",
                            height: 14.0,
                          ),
                        ),
                      Text(
                        transaction.amount == 0 && transaction.txnHead == 100
                            ? "Free"
                            : ((transaction.debit ? "-" : "+") +
                                (transaction.isGoldbarTransaction
                                    ? transaction.amount.toInt().toString()
                                    : CurrencyFormat.format(transaction.amount,
                                        decimalDigits: initData.experiments
                                            .miscConfig.amountPrecision))),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey.shade600,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    if (transaction.isGoldbarTransaction)
                      Padding(
                        padding: const EdgeInsets.only(right: 1.0),
                        child: Image.asset(
                          "images/currency/diamond.png",
                          height: 14.0,
                        ),
                      ),
                    Text(
                      transaction.isGoldbarTransaction
                          ? transaction.userBalance.toInt().toString()
                          : CurrencyFormat.format(
                              transaction.userBalance,
                              decimalDigits: initData
                                  .experiments.miscConfig.amountPrecision,
                            ),
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.grey.shade600,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
      }
    }

    return transactionItems;
  }

  @override
  Widget build(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    InitData initData = Provider.of(context, listen: false);
    List<Widget> filteredTransactions = getFilteredTransactions();
    return LoaderContainer(
      child: Scaffold(
        key: scaffoldKey,
        appBar: CommonAppBar(
          children: <Widget>[
            Text(
              "Account Summary".toUpperCase(),
            )
          ],
        ),
        body: Container(
          margin: EdgeInsets.all(12.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12.0),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 4.0,
                spreadRadius: 0,
                offset: Offset(0, 3.0),
              ),
            ],
          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Total balance".toUpperCase(),
                      style: TextStyle(
                        fontSize: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .fontSize,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Image.asset(
                            config.channelId < 200
                                ? "images/currency/cash.png"
                                : "images/currency/cash-dollar.png",
                            height: 32.0,
                          ),
                        ),
                        CurrencyText(
                          showCurrencyDelimiters: false,
                          amount: accountDetails.balance.depositBucket +
                              accountDetails.balance.withdrawable +
                              accountDetails.balance.bonusAmount,
                          style: TextStyle(
                            fontSize: Theme.of(context)
                                .primaryTextTheme
                                .headline5
                                .fontSize,
                            fontWeight: FontWeight.bold,
                          ),
                          decimalDigits:
                              initData.experiments.miscConfig.amountPrecision,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0),
                          child: Image.asset(
                            "images/currency/diamond.png",
                            height: 26.0,
                          ),
                        ),
                        Text(
                          accountDetails.balance.goldBars.toString(),
                          style: TextStyle(
                              fontSize: Theme.of(context)
                                  .primaryTextTheme
                                  .headline5
                                  .fontSize,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 16.0),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: getDistributionButton(
                        "Deposits",
                        accountDetails.balance.depositBucket,
                        "Add Cash",
                        () {
                          _launchAddCash(context,
                              source: "transaction_history");
                        },
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Expanded(
                      child: getDistributionButton(
                        "Winnings",
                        accountDetails.balance.withdrawable,
                        "Withdraw",
                        () {
                          _launchWithdraw(context, "transaction_history");
                        },
                      ),
                    ),
                    SizedBox(
                      width: 8.0,
                    ),
                    Expanded(
                      child: getDistributionButton(
                        "Bonus",
                        accountDetails.balance.bonusAmount,
                        "Refer",
                        () {
                          _launchEarnCash(context, "transaction_history");
                        },
                        title1: "Locked",
                        balance1: accountDetails.balance.lockedBonusAmount,
                      ),
                    ),
                  ],
                ),
              ),
              getTransactionFilter(),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(8.0),
                        color: Colors.grey.shade200,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 0.0),
                                child: Text(
                                  "Event".toUpperCase(),
                                  style: TextStyle(fontSize: 16.0),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Date".toUpperCase(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16.0),
                                    ),
                                    Text(
                                      "(dd/mm/yy)",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(fontSize: 16.0),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Text(
                                  "+/-".toUpperCase(),
                                  textAlign: TextAlign.right,
                                  style: TextStyle(fontSize: 16.0),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding: const EdgeInsets.only(right: 0.0),
                                child: Text(
                                  "Balance".toUpperCase(),
                                  textAlign: TextAlign.right,
                                  style: TextStyle(fontSize: 16.0),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (filteredTransactions.length > 0)
                        Expanded(
                          child: SingleChildScrollView(
                            child: Column(children: filteredTransactions),
                          ),
                        ),
                      if (filteredTransactions.length == 0)
                        Expanded(
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(
                                "No Results Found",
                                style: TextStyle(
                                  fontSize: 20.0,
                                ),
                              ),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
