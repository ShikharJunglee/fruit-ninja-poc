import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class PrizeDistributionCard extends StatelessWidget {
  final String rank;
  final double prize;
  final double diamonds;

  PrizeDistributionCard({
    @required this.rank,
    @required this.prize,
    @required this.diamonds,
  });

  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline4.copyWith(
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
      gradient: LinearGradient(
        colors: [
          Colors.redAccent.shade100,
          Colors.redAccent.shade700,
          Colors.black,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  getPrizeWidget(
      BuildContext context, double realCurrency, double virtualCurrency) {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> _widgets = [];
    if (virtualCurrency != null && virtualCurrency > 0) {
      _widgets.add(
        Image.asset(
          "images/currency/diamond.png",
          height: 22.0,
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 2.0),
          child: getGradientText(
            context,
            virtualCurrency
                .toString()
                .replaceAll(".0", "")
                .replaceAll(".00", ""),
          ),
        ),
      );
    }

    if (virtualCurrency != null &&
        virtualCurrency > 0 &&
        realCurrency != null &&
        realCurrency > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: getGradientText(context, "+"),
        ),
      );
    }

    if (realCurrency != null && realCurrency > 0) {
      _widgets.add(
        getGradientText(
          context,
          CurrencyFormat.format(realCurrency.toDouble(),
                  decimalDigits:
                      initData.experiments.miscConfig.amountPrecision,
                  showDelimeters: false)
              .replaceAll(".00", ""),
        ),
      );
    }

    return Row(children: _widgets);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      shape: RoundedRectangleBorder(
        side: BorderSide(
          width: 2.0,
          color: Color.fromRGBO(255, 206, 21, 1),
        ),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Rank",
                  style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                        fontWeight: FontWeight.bold,
                        color: Colors.brown.shade700,
                      ),
                ),
                Text(
                  "#$rank",
                  style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                        color: Colors.brown.shade700,
                        fontWeight: FontWeight.bold,
                      ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  "Prize",
                  style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                        color: Colors.brown.shade700,
                        fontWeight: FontWeight.bold,
                      ),
                ),
                getPrizeWidget(context, prize, diamonds),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
