import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/lobby/prizestructure_details.dart';
import 'package:solitaire_gold/screens/prizestructure/distribution_card.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class PrizeStructure extends StatefulWidget {
  final Function onJoin;
  final PrizeStrucutreDetails prizestructureDetails;
  final int maxPlayers;
  final Map<String, dynamic> analyticData;
  final String source;
  final int prizeStructureId;

  PrizeStructure({
    @required this.prizestructureDetails,
    @required this.onJoin,
    @required this.maxPlayers,
    @required this.analyticData,
    @required this.source,
    @required this.prizeStructureId,
  });

  @override
  _PrizeStructureState createState() => _PrizeStructureState();
}

class _PrizeStructureState extends State<PrizeStructure> {
  @override
  void initState() {
    super.initState();
    LobbyAnalytics().onPrizeInfoLoaded(
      context,
      source: widget.source,
      prizeStrucutreDetails: widget.prizestructureDetails,
      maxSize: widget.maxPlayers,
      anaylticData: widget.analyticData,
    );
  }

  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline5.copyWith(
        fontWeight: FontWeight.w600,
        color: Colors.white,
        shadows: [
          Shadow(
            blurRadius: 0.0,
            color: Colors.grey.shade700,
            offset: Offset(0.0, 2.0),
          ),
        ],
      ),
      gradient: LinearGradient(
        colors: [
          Colors.yellow,
          Color.fromRGBO(253, 239, 171, 1),
          Color.fromRGBO(231, 181, 25, 1),
          Colors.red,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  getEntryFeeWidget(
      BuildContext context, double realCurrency, int virtualCurrency) {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> _widgets = [];
    if (virtualCurrency != null && virtualCurrency > 0) {
      _widgets.add(
        Image.asset(
          "images/currency/diamond.png",
          height: 18.0,
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 2.0),
          child: getGradientText(
            context,
            virtualCurrency.toString(),
          ),
        ),
      );
    }

    if (virtualCurrency != null &&
        virtualCurrency > 0 &&
        realCurrency != null &&
        realCurrency > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: getGradientText(
            context,
            "+",
          ),
        ),
      );
    }

    if (realCurrency != null && realCurrency > 0) {
      _widgets.add(
        getGradientText(
          context,
          CurrencyFormat.format(
            realCurrency.toDouble(),
            decimalDigits: initData.experiments.miscConfig.amountPrecision,
            showDelimeters: false,
          ),
        ),
      );
    }
    if ((realCurrency == null || realCurrency == 0) &&
        (virtualCurrency == null || virtualCurrency == 0)) {
      _widgets.add(
        getGradientText(
          context,
          "Free",
        ),
      );
    }

    return Container(
      height: 25,
      child: FittedBox(
        fit: BoxFit.fitWidth,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _widgets,
        ),
      ),
    );
  }

  getPrizePoolWidget(
      BuildContext context, double prizePool, int diamondPrizePool) {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> _widgets = [];
    if (diamondPrizePool > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(right: 2.0),
          child: Image.asset(
            "images/currency/diamond.png",
            height: 18.0,
          ),
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 2.0),
          child: getGradientText(
            context,
            diamondPrizePool.toString(),
          ),
        ),
      );
    }

    if (diamondPrizePool > 0 && prizePool > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4.0),
          child: getGradientText(
            context,
            "+",
          ),
        ),
      );
    }

    if (prizePool > 0) {
      _widgets.add(
        getGradientText(
          context,
          CurrencyFormat.format(
            prizePool.toDouble(),
            decimalDigits: initData.experiments.miscConfig.amountPrecision,
            showDelimeters: false,
          ),
        ),
      );
    }

    return Container(
      height: 25,
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _widgets,
        ),
      ),
    );
  }

  _getHeader() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("images/header.png"),
          fit: BoxFit.fill,
        ),
      ),
      padding: const EdgeInsets.only(top: 30.0, bottom: 24.0),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 8.0,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/stats_rectangle.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    "Winners",
                    style: Theme.of(context)
                        .primaryTextTheme
                        .headline6
                        .copyWith(fontSize: 19),
                  ),
                  Container(
                    height: 25,
                    child: getGradientText(
                      context,
                      widget.prizestructureDetails.prizeCount.toString() +
                          "/" +
                          widget.maxPlayers.toString(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 8.0,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/stats_rectangle.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    "Entry Fee",
                    style: Theme.of(context)
                        .primaryTextTheme
                        .headline6
                        .copyWith(fontSize: 19),
                  ),
                  getEntryFeeWidget(
                    context,
                    widget.prizestructureDetails.entryFee,
                    widget.prizestructureDetails.goldBarFee,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 8.0,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/stats_rectangle.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    "Prize",
                    style: Theme.of(context)
                        .primaryTextTheme
                        .headline6
                        .copyWith(fontSize: 19),
                  ),
                  getPrizePoolWidget(
                    context,
                    widget.prizestructureDetails.prizeAmount,
                    widget.prizestructureDetails.goldBarsAmount,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 8.0,
          ),
        ],
      ),
    );
  }

  _getFooter() {
    return widget.onJoin != null
        ? Padding(
            padding: const EdgeInsets.only(top: 12.0, bottom: 8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 80.0),
                            constraints: BoxConstraints(maxHeight: 60.0),
                            child: Button(
                              text: "PLAY",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .headline4
                                  .copyWith(
                                    fontSize: 32,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w800,
                                  ),
                              onPressed: () {
                                widget.onJoin();
                              },
                              size: ButtonSize.medium,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        : Container();
  }

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: EdgeInsets.all(0.0),
      dialog: Dialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CommonDialogFrame(
              titleAsset: "images/titles/prize-breakup.png",
              letterSpacing: 2.4,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _getHeader(),
                  SizedBox(
                    height: 4.0,
                  ),
                  Flexible(
                    child: Container(
                      constraints: BoxConstraints(maxHeight: 250),
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            for (PrizeDistribution _prize
                                in widget.prizestructureDetails.distribution)
                              PrizeDistributionCard(
                                rank: _prize.rank,
                                diamonds: double.tryParse(_prize.goldBars),
                                prize: double.tryParse(_prize.amount),
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  _getFooter(),
                ],
              ),
            ),
            Text(
              "ID# ${widget.prizeStructureId}",
              style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                    color: Colors.white,
                  ),
            )
          ],
        ),
      ),
    );
  }
}
