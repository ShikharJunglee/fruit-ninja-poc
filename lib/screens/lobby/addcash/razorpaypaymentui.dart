import 'dart:convert';
import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/analytics/analyticsevents.dart';
import 'package:solitaire_gold/commonwidgets/colorbutton.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/commonwidgets/textbox/MaskedTextInputFormatter.dart';
import 'package:solitaire_gold/screens/paymentmode/carddetails.dart';
import 'package:solitaire_gold/services/myrazorpaycustomservice.dart';

class RazorpayPaymentUI extends StatefulWidget {
  final Map<dynamic, dynamic> razorpayPaymentCodesMap;
  final Function onBackPressed;
  final String paymentType;
  final String razorpayRegId;
  final Map<String, dynamic> paymentPayload;
  RazorpayPaymentUI(
      {@required this.razorpayPaymentCodesMap,
      @required this.onBackPressed,
      @required this.paymentType,
      @required this.razorpayRegId,
      @required this.paymentPayload});
  @override
  RazorpayPaymentUIState createState() => RazorpayPaymentUIState();
}

class RazorpayPaymentUIState extends State<RazorpayPaymentUI> with CardDetails {
  Map<dynamic, dynamic> installedUPIAppsDataMap;
  Map<dynamic, dynamic> razorpayNetbankingDataMap;
  Map<dynamic, dynamic> razorpayWalletDataMap;
  String selectedNetbankingBank;
  String selectedWallet;
  final GlobalKey<FormState> _formState = GlobalKey<FormState>();
  final TextEditingController _cardNumberController = TextEditingController();
  TextEditingController _upiController = TextEditingController();
  String cvv;
  String expiry;
  String cardNumber;
  String cardHolderName;
  String cardIcon;
  bool bshowInvalidVpaError = false;

  @override
  void initState() {
    super.initState();
    addCardNumberListener();
    installedUPIAppsDataMap = new Map();
    razorpayNetbankingDataMap = new Map();
    razorpayWalletDataMap = new Map();
    if (widget.razorpayPaymentCodesMap["razorpayNetbankingDataMap"] != null) {
      razorpayNetbankingDataMap =
          widget.razorpayPaymentCodesMap["razorpayNetbankingDataMap"];
      selectedNetbankingBank = razorpayNetbankingDataMap.keys.toList()[0];
    }
    if (widget.razorpayPaymentCodesMap["installedUPIAppsDataMap"] != null) {
      installedUPIAppsDataMap =
          widget.razorpayPaymentCodesMap["installedUPIAppsDataMap"];
    }
    if (widget.razorpayPaymentCodesMap["razorpayWalletDataMap"] != null) {
      razorpayWalletDataMap =
          widget.razorpayPaymentCodesMap["razorpayWalletDataMap"];
      selectedWallet = razorpayWalletDataMap.keys.toList()[0];
    }
    try {
      // AnalyticsEvents analyticsEvents = new AnalyticsEvents();
      // analyticsEvents.payModeOptionsLoaded(
      //     expanded: false,
      //     amount: widget.paymentPayload["depositAmount"],
      //     promoCode: widget.paymentPayload["promoCode"],
      //     firstDeposit: widget.paymentPayload["isFirstDeposit"],
      //     paymentType: widget.paymentType,
      //     gateWayID: int.parse(widget.paymentPayload["gatewayId"].toString()),
      //     modeOptionId: widget.paymentPayload["modeOptionId"]);
    } catch (e) {
      print(e);
    }
  }

  getImageDataFromBase64Data(String base64String) {
    try {
      Uint8List bytes = base64.decode(base64String);
      return bytes;
    } catch (e) {
      print(e);
    }
  }

  onPaySecurely(Map<String, dynamic> response, String method) async {
    if (method == "vpa_collect") {
      bool isValidVpa = await MyRazorpayCustomeService()
          .bIsValidVPA(_upiController.text, widget.razorpayRegId);
      if (isValidVpa) {
        Navigator.of(context).pop(response);
      } else {
        this.setState(() {
          bshowInvalidVpaError = true;
        });
      }
    }
  }

/* ....................Card..................... */
  Widget getUPIWidget() {
    return Column(children: <Widget>[
      (installedUPIAppsDataMap != null && installedUPIAppsDataMap.length > 0)
          ? Column(children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 4.0, left: 4.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Tap on your favourite UPI app to  add cash",
                      style:
                          Theme.of(context).primaryTextTheme.headline4.copyWith(
                                color: Colors.black,
                                fontSize: 19,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ],
                ),
              ),
              getInstalledUPIAppsList(),
              Row(children: <Widget>[
                Expanded(
                  child: new Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                      child: Divider(
                        color: Colors.black45,
                        height: 36,
                      )),
                ),
                Text(
                  "OR",
                  style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                        color: Colors.black45,
                        fontSize: 16,
                        fontWeight: FontWeight.normal,
                      ),
                ),
                Expanded(
                  child: new Container(
                      margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                      child: Divider(
                        color: Colors.black45,
                        height: 36,
                      )),
                ),
              ]),
            ])
          : Container(),
      Padding(
        padding: EdgeInsets.only(top: 4.0, left: 0.0),
        child: Row(
          children: <Widget>[
            Text(
              "Enter UPI ID",
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.black,
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 16.0, left: 0.0, right: 16.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: SimpleTextBox(
                labelText: "Enter your UPI Id",
                controller: _upiController,
                contentPadding:
                    EdgeInsets.only(top: 0.5, bottom: 0.5, left: 3.7),
                borderColor: Colors.black38,
                validator: (value) {
                  if (value.isEmpty) {
                    return "Please enter UPI ID";
                  } else if (bshowInvalidVpaError) {
                    return "Pleas enter valid UPI  ID";
                  }
                  return null;
                },
                obscureText: false,
              ),
            ),
          ],
        ),
      ),
      bshowInvalidVpaError
          ? Padding(
              padding: EdgeInsets.only(top: 4.0, left: 0.0),
              child: Row(
                children: <Widget>[
                  Text(
                    "Please enter a valid UPI id",
                    style:
                        Theme.of(context).primaryTextTheme.headline4.copyWith(
                              color: Colors.red,
                              fontSize: 19,
                              fontWeight: FontWeight.normal,
                            ),
                  ),
                ],
              ),
            )
          : Container(),
      Container(
        padding:
            EdgeInsets.only(bottom: 12.0, top: 8.0, left: 12.0, right: 12.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: ColorButton(
                onPressed: () {
                  this.setState(() {
                    bshowInvalidVpaError = false;
                  });
                  Map<String, dynamic> response = new Map();
                  response["statusCode"] = 2;
                  response["upiid"] = _upiController.text;
                  onPaySecurely(response, "vpa_collect");

                  try {
                    // AnalyticsEvents analyticsEvents = new AnalyticsEvents();
                    // analyticsEvents.customeUPIIdEntered(
                    //     expanded: false,
                    //     amount: widget.paymentPayload["depositAmount"],
                    //     promoCode: widget.paymentPayload["promoCode"],
                    //     firstDeposit: widget.paymentPayload["isFirstDeposit"],
                    //     paymentType: widget.paymentType,
                    //     modeOptionId: widget.paymentPayload["modeOptionId"],
                    //     gateWayID: int.parse(
                    //         widget.paymentPayload["gatewayId"].toString()));
                  } catch (e) {
                    print(e);
                  }
                },
                child: Text(
                  "PAY SECURELY".toUpperCase(),
                  style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                        color: Colors.white,
                      ),
                ),
              ),
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 4.0, left: 0.0),
        child: Row(
          children: <Widget>[
            Text(
              "Collect request  will be  sent to your UPI app",
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.black,
                    fontSize: 19,
                    fontWeight: FontWeight.normal,
                  ),
            ),
          ],
        ),
      ),
    ]);
  }

  getInstalledUPIAppsList() {
    var keys = installedUPIAppsDataMap.keys.toList();
    return Padding(
        padding: EdgeInsets.only(top: 6.0, bottom: 6.0),
        child: Container(
            height: 80.0,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: keys.length,
                itemBuilder: (BuildContext context, int index) => Card(
                      child: new InkWell(
                        onTap: () {
                          var res = installedUPIAppsDataMap[keys[index]];

                          AnalyticsEvents analyticsEvents =
                              new AnalyticsEvents();
                          try {
                            analyticsEvents.payOptionSelect(
                              amount: widget.paymentPayload["depositAmount"],
                              promoCode: widget.paymentPayload["promoCode"],
                              firstDeposit:
                                  widget.paymentPayload["isFirstDeposit"],
                              modeOptionId:
                                  widget.paymentPayload["modeOptionId"],
                              paymentOptionType:
                                  keys[index].toString().toUpperCase(),
                            );
                          } catch (e) {
                            print(e);
                          }
                          Map<String, dynamic> response = new Map();
                          response["statusCode"] = 1;
                          response["packagename"] = res["packagename"];
                          Navigator.of(context).pop(response);
                        },
                        child: Container(
                          width: 80.0,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              new Image.memory(
                                getImageDataFromBase64Data(
                                    installedUPIAppsDataMap[keys[index]]
                                        ["icondata"]),
                                height: 28.0,
                              ),
                              new Text(keys[index]),
                            ],
                          ),
                        ),
                      ),
                    ))));
  }

  /* ....................NetBanking..................... */
  Widget getNetbankingWidget() {
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 8.0, left: 0.0),
        child: Row(
          children: <Widget>[
            Text(
              "Please select your bank",
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.black,
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 8.0, left: 0.0),
        child: Row(
          children: <Widget>[
            DropdownButton(
              onChanged: (newValue) {
                AnalyticsEvents analyticsEvents = new AnalyticsEvents();
                try {
                  analyticsEvents.payOptionSelect(
                    amount: widget.paymentPayload["depositAmount"],
                    promoCode: widget.paymentPayload["promoCode"],
                    firstDeposit: widget.paymentPayload["isFirstDeposit"],
                    modeOptionId: widget.paymentPayload["modeOptionId"],
                    paymentOptionType: newValue.toString().toUpperCase(),
                  );
                } catch (e) {
                  print(e);
                }
                setState(() {
                  selectedNetbankingBank = newValue;
                });
              },
              elevation: 16,
              value: selectedNetbankingBank,
              style: TextStyle(color: Colors.black),
              items: (razorpayNetbankingDataMap.keys.toList()).map((item) {
                return DropdownMenuItem(
                  child: Text(razorpayNetbankingDataMap[item]["label"]),
                  value: item,
                );
              }).toList(),
            ),
          ],
        ),
      ),
      Container(
        padding: EdgeInsets.only(bottom: 12.0, top: 8.0, left: 8.0, right: 8.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: ColorButton(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 12.0),
                  child: Text(
                    "Pay Securely".toUpperCase(),
                    style:
                        Theme.of(context).primaryTextTheme.headline6.copyWith(
                              color: Colors.white,
                            ),
                  ),
                ),
                onPressed: () {
                  Map<String, dynamic> response = new Map();
                  response["statusCode"] = 1;
                  response["selectedNetbankingBank"] = selectedNetbankingBank;
                  Navigator.of(context).pop(response);
                },
              ),
            ),
          ],
        ),
      )
    ]);
  }

  /* ....................Card..................... */
  addCardNumberListener() {
    cardIcon = otherCardIcon;
    _cardNumberController.addListener(() {
      setState(() {
        cardIcon = getCardIcon(_cardNumberController.text);
      });
    });
  }

  Widget getCardWidget() {
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 8.0, left: 0.0),
        child: Row(
          children: <Widget>[
            Text(
              "Use Card",
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.black,
                    fontSize: 19,
                    fontWeight: FontWeight.bold,
                  ),
            ),
          ],
        ),
      ),
      Padding(
          padding: EdgeInsets.only(top: 8.0, left: 0.0),
          child: getCardInputFieldForm()),
    ]);
  }

  Form getCardInputFieldForm() {
    return Form(
      key: _formState,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 16.0, top: 4.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: SimpleTextBox(
                    labelText: "Card number",
                    controller: _cardNumberController,
                    style:
                        Theme.of(context).primaryTextTheme.headline6.copyWith(
                              color: Colors.grey.shade700,
                            ),
                    labelStyle:
                        Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                              color: Colors.grey.shade600,
                            ),
                    suffixIcon: Padding(
                      padding: EdgeInsets.only(right: 4.0),
                      child: Image.network(
                        cardIcon,
                        width: 8.0,
                      ),
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.deny(
                        RegExp("[-,./#\$]"),
                      ),
                      MaskedTextInputFormatter(
                        mask: 'xxxx xxxx xxxx xxxx xxxx',
                        separator: ' ',
                      ),
                    ],
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Card number is required.";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      cardNumber = value;
                    },
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 16.0, top: 4.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(right: 8.0),
                    child: SimpleTextBox(
                      labelText: "Expiry Date",
                      style:
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                color: Colors.grey.shade700,
                              ),
                      labelStyle:
                          Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                                color: Colors.grey.shade600,
                              ),
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.deny(
                          RegExp("[-,.#\$]"),
                        ),
                        MaskedTextInputFormatter(
                          mask: 'xx/xx',
                          separator: '/',
                        ),
                      ],
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Expiry date required.";
                        } else {
                          List<String> expiryDate = value.split("/");
                          int month = int.tryParse(expiryDate[0]);
                          int year = int.tryParse(expiryDate.length == 1
                              ? ""
                              : "20" + expiryDate[1]);
                          if (month == null || !(month > 0 && month <= 12)) {
                            return "Invalid month";
                          }
                          if (year == null || year == 0) {
                            return "Invalid year";
                          } else {
                            int currentYear = DateTime.now().year;
                            int currentMonth = DateTime.now().month;
                            if (year < currentYear - 1 ||
                                (year == currentYear && currentMonth < month)) {
                              return "Invalid expiry date";
                            }
                          }
                        }
                        return null;
                      },
                      onSaved: (value) {
                        expiry = value;
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: SimpleTextBox(
                      labelText: "CVV",
                      maxLines: 1,
                      style:
                          Theme.of(context).primaryTextTheme.bodyText2.copyWith(
                                color: Colors.grey.shade700,
                              ),
                      labelStyle:
                          Theme.of(context).primaryTextTheme.bodyText2.copyWith(
                                color: Colors.grey.shade600,
                              ),
                      obscureText: true,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly,
                      ],
                      validator: (value) {
                        if (value.isEmpty) {
                          return "CVV number required.";
                        }
                        return null;
                      },
                      onSaved: (value) {
                        cvv = value;
                      },
                      maxLength: 4,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 16.0, top: 4.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: SimpleTextBox(
                    labelText: "Card Holder's Name",
                    style:
                        Theme.of(context).primaryTextTheme.headline6.copyWith(
                              color: Colors.grey.shade700,
                            ),
                    labelStyle:
                        Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                              color: Colors.grey.shade600,
                            ),
                    keyboardType: TextInputType.text,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Cardholder's name is required.";
                      }
                      return null;
                    },
                    onSaved: (value) {
                      cardHolderName = value;
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 12.0, top: 8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: ColorButton(
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 12.0),
                      child: Text(
                        "Pay Securely".toUpperCase(),
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Colors.white,
                            ),
                      ),
                    ),
                    onPressed: () {
                      if (_formState.currentState.validate()) {
                        _formState.currentState.save();
                        Map<String, dynamic> response = new Map();
                        response["statusCode"] = 2;
                        response["cardNumber"] =
                            cardNumber.replaceAll(RegExp(r"\s\b|\b\s"), "");
                        response["expiry"] = expiry;
                        response["cardHolderName"] = cardHolderName;
                        response["cvv"] = cvv;
                        response["cardDataCapturingRequired"] = true;
                        Navigator.of(context).pop(response);
                      }
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  /* ....................Mobile wallet ..................... */
  Widget getMobileWalletWidget() {
    return Column(children: <Widget>[
      (razorpayWalletDataMap != null && razorpayWalletDataMap.length > 0)
          ? Column(children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 4.0, left: 4.0, bottom: 4.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Tap on your favourite  mobile wallet to  add cash",
                      style:
                          Theme.of(context).primaryTextTheme.headline4.copyWith(
                                color: Colors.black,
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ],
                ),
              ),
              getMobileWalletAppsList(),
            ])
          : Container(),
      //getMobileWalletDropDownUI()
    ]);
  }

  getMobileWalletDropDownUI() {
    return Column(children: <Widget>[
      Row(children: <Widget>[
        Expanded(
          child: new Container(
              margin: const EdgeInsets.only(left: 10.0, right: 20.0),
              child: Divider(
                color: Colors.black,
                height: 36,
              )),
        ),
        Text("OR"),
        Expanded(
          child: new Container(
              margin: const EdgeInsets.only(left: 20.0, right: 10.0),
              child: Divider(
                color: Colors.black,
                height: 36,
              )),
        ),
      ]),
      DropdownButton(
        onChanged: (newValue) {
          setState(() {
            selectedWallet = newValue;
          });
        },
        elevation: 16,
        value: selectedWallet,
        style: TextStyle(color: Colors.black),
        items: (razorpayWalletDataMap.keys.toList()).map((item) {
          return DropdownMenuItem(
            child: Text(item),
            value: item,
          );
        }).toList(),
      ),
      ColorButton(
        onPressed: () {
          Map<String, dynamic> response = new Map();
          response["statusCode"] = 1;
          response["selectedWallet"] = selectedWallet;
          Navigator.of(context).pop(response);
        },
        child: Text(
          "PAY SECURELY".toUpperCase(),
          style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                color: Colors.white,
              ),
        ),
      ),
    ]);
  }

  getMobileWalletAppsList() {
    var keys = razorpayWalletDataMap.keys.toList();
    return Padding(
        padding: EdgeInsets.only(top: 6.0, bottom: 8.0),
        child: Container(
            height: 100.0,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: keys.length,
                itemBuilder: (BuildContext context, int index) => Card(
                      child: new InkWell(
                        onTap: () {
                          var res = razorpayWalletDataMap[keys[index]];
                          Map<String, dynamic> response = new Map();
                          response["statusCode"] = 1;
                          response["selectedWallet"] = keys[index];
                          Navigator.of(context).pop(response);
                          AnalyticsEvents analyticsEvents =
                              new AnalyticsEvents();
                          try {
                            analyticsEvents.payOptionSelect(
                                amount: widget.paymentPayload["depositAmount"],
                                promoCode: widget.paymentPayload["promoCode"],
                                firstDeposit:
                                    widget.paymentPayload["isFirstDeposit"],
                                modeOptionId:
                                    widget.paymentPayload["modeOptionId"],
                                paymentOptionType:
                                    keys[index].toString().toUpperCase());
                          } catch (e) {
                            print(e);
                          }
                        },
                        child: Container(
                          width: 80.0,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Column(children: <Widget>[
                                new CachedNetworkImage(
                                    imageUrl:
                                        razorpayWalletDataMap[keys[index]] !=
                                                null
                                            ? razorpayWalletDataMap[keys[index]]
                                            : "",
                                    height: 30.0,
                                    width: 60.0),
                                // new Text(keys[index]),
                              ])
                            ],
                          ),
                        ),
                      ),
                    ))));
  }

  Widget getRazorPayPaymentMethodUIbody() {
    if (widget.paymentType == "netbanking") {
      return getNetbankingWidget();
    } else if (widget.paymentType == "upi") {
      return getUPIWidget();
    } else if (widget.paymentType == "wallet") {
      return getMobileWalletWidget();
    } else {
      return getCardWidget();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 30.0,
            child: const DecoratedBox(
              decoration: const BoxDecoration(color: Colors.red),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 12.0, bottom: 16.0),
              child: getRazorPayPaymentMethodUIbody()),
        ],
      ),
    );
  }
}
