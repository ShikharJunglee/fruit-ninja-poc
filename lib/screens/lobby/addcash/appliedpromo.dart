import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';

class AppliedPromo extends StatefulWidget {
  final int amount;
  final Function onRemove;
  final bool showLockedBonus;
  final Map<String, dynamic> promoCode;
  final Function getPercentageAmount;

  AppliedPromo({
    this.promoCode,
    this.amount = 0,
    this.onRemove,
    this.showLockedBonus = false,
    this.getPercentageAmount,
  });

  @override
  _AppliedPromoState createState() => _AppliedPromoState();
}

class _AppliedPromoState extends State<AppliedPromo> {
  double height = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => onWidgetLoadFinished());
  }

  onWidgetLoadFinished() {
    setState(() {
      height = 52;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.promoCode == null) {
      return Container();
    }

    TextStyle style = Theme.of(context).primaryTextTheme.overline.copyWith(
          letterSpacing: 0.0,
          color: Colors.black,
        );

    double instantCash = widget.getPercentageAmount(
        widget.promoCode,
        widget.amount,
        double.tryParse(widget.promoCode["instantCashPercentage"].toString()));
    double bonusAmount = widget.getPercentageAmount(
        widget.promoCode,
        widget.amount,
        double.tryParse(widget.promoCode["playablePercentage"].toString()));
    double lockedBonus = widget.getPercentageAmount(
        widget.promoCode,
        widget.amount,
        double.tryParse(widget.promoCode["nonPlayablePercentage"].toString()));

    return AnimatedContainer(
      height: height,
      duration: Duration(milliseconds: 300),
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 2.0, bottom: 2.0),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 20.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: DottedBorder(
              color: Theme.of(context).buttonColor,
              dashPattern: [6.0, 2.0],
              borderType: BorderType.RRect,
              padding: EdgeInsets.all(0.0),
              radius: Radius.circular(4.0),
              strokeWidth: 1.0,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: 4.0),
                          child: Text(
                            widget.promoCode["promoCode"],
                            style: Theme.of(context)
                                .primaryTextTheme
                                .subtitle1
                                .copyWith(
                                  color: Theme.of(context).buttonColor,
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                        ),
                        Text(
                          "Applied",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .caption
                              .copyWith(
                                color: Theme.of(context).buttonColor,
                                fontStyle: FontStyle.italic,
                              ),
                        ),
                      ],
                    ),
                    if (instantCash > 0)
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              instantCash
                                  .toStringAsFixed(2)
                                  .replaceAll(".00", ""),
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ),
                          Text(
                            "Extra cash".toUpperCase(),
                            style: style,
                          ),
                        ],
                      ),
                    if (bonusAmount > 0)
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              bonusAmount
                                  .toStringAsFixed(2)
                                  .replaceAll(".00", ""),
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ),
                          Text(
                            "Bonus".toUpperCase(),
                            style: style,
                          ),
                        ],
                      ),
                    if (lockedBonus > 0 && widget.showLockedBonus)
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 4.0),
                            child: Text(
                              lockedBonus
                                  .toStringAsFixed(2)
                                  .replaceAll(".00", ""),
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ),
                          Text(
                            "Locked".toUpperCase(),
                            style: style,
                          ),
                        ],
                      ),
                    IconButton(
                      padding: EdgeInsets.all(0.0),
                      icon: Icon(Icons.close),
                      onPressed: () {
                        widget.onRemove();
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
