import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/api/geolocation/locationAPI.dart';
import 'package:solitaire_gold/api/kyc/withdrawAPI.dart';
import 'package:solitaire_gold/popups/common/generic_dialog.dart';
import 'package:solitaire_gold/screens/withdraw/statedobdialog.dart';
import 'package:solitaire_gold/utils/loader.dart';

class AddCashHelper {
  AddCashHelper._internal();
  factory AddCashHelper() => addCashHelper;
  static final AddCashHelper addCashHelper = AddCashHelper._internal();

  AddCashAPI _addCashAPI = AddCashAPI();

  Future<Map<String, dynamic>> authAddcash(BuildContext context) async {
    LocationAPI _locationAPI = LocationAPI();
    Map<String, dynamic> locationResult;

    locationResult = await _locationAPI.getUserLocation(
      context,
      isAddCash: true,
    );
    Loader().showLoader(false);

    if (locationResult != null && locationResult["isError"]) {
      if (locationResult["isBlockedState"] == true) {
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) => GenericDialog(
            contentText: Text(
              "Cash transactions are restricted in your State. Reach out to our support for further assistance.",
              textAlign: TextAlign.center,
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.white,
                  ),
            ),
            buttonText: "Ok".toUpperCase(),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        );

        return {
          "error": true,
          "message": null,
          "isBlockedState": locationResult["isBlockedState"]
        };
      }

      return {"error": true, "message": null};
    } else {
      Loader().showLoader(true, immediate: true);
      final result = await _addCashAPI.getAmountTiles(context,
          locationData: locationResult);

      Loader().showLoader(false);

      if (result["error"] != true && result["data"] != null) {
        if (result["data"]["error"].length > 0) {
          if (result["data"]["errorDetails"]["isBlockedUser"]) {
            return {
              "error": true,
              "message": result["data"]["errorDetails"]["errorMessage"],
            };
          } else {
            List errorCodes = result["data"]["error"];
            int errorCode = _getErrorCode(errorCodes);
            switch (errorCode) {
              case AuthStatusCode.MISSING_DOB:
                final result = await showDialog(
                  barrierColor: Color.fromARGB(175, 0, 0, 0),
                  context: context,
                  builder: (BuildContext context) {
                    return StateDobDialog(
                      source: "add_cash",
                    );
                  },
                );
                if (result != null && result["success"]) {
                  return authAddcash(context);
                } else if (result != null && !result["success"]) {
                  return {
                    "error": true,
                    "message": "Getting error while updating details.",
                  };
                }
                break;
              case AuthStatusCode.NOT_VERIFIED:
              case AuthStatusCode.REG_CLOSED:
              case AuthStatusCode.PAN_REQUIRED:
                return {
                  "error": false,
                  "data": result["data"],
                };
                break;
            }
          }
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      } else if (result["error"] == true && result["message"] != null) {
        return {
          "error": true,
          "message": result["message"],
        };
      }
    }
    return null;
  }

  int _getErrorCode(errorCodes) {
    if (errorCodes.indexOf(AuthStatusCode.INSUFFICIENT_FUND) != -1 ||
        errorCodes.indexOf(AuthStatusCode.INSUFFICIENT_CASH_FUND) != -1) {
      return AuthStatusCode.INSUFFICIENT_FUND;
    } else if (errorCodes.indexOf(AuthStatusCode.MISSING_DOB) != -1) {
      return AuthStatusCode.MISSING_DOB;
    } else if (errorCodes.indexOf(AuthStatusCode.NOT_VERIFIED) != -1 ||
        errorCodes.indexOf(7) != -1 ||
        errorCodes.indexOf(8) != -1 ||
        errorCodes.indexOf(9) != -1) {
      return AuthStatusCode.NOT_VERIFIED;
    } else if (errorCodes.indexOf(AuthStatusCode.CONTEST_ALREADY_JOINED) !=
        -1) {
      return AuthStatusCode.CONTEST_ALREADY_JOINED;
    } else {
      return AuthStatusCode.REG_CLOSED;
    }
  }
}
