import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import 'package:solitaire_gold/analytics/analyticsevents.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/addcash/deposit.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/lobby/addcash/appliedpromo.dart';
import 'package:solitaire_gold/screens/lobby/addcash/paymentpartners.dart';
import 'package:solitaire_gold/screens/lobby/addcash/promoinput.dart';
import 'package:solitaire_gold/screens/lobby/addcash/template.dart';
import 'package:solitaire_gold/screens/lobby/addcash/transactionfailed.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/services/mybranchioservice.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/gateways/cashfreepayment.dart';
import 'package:solitaire_gold/utils/gateways/razorpay.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class AddCash extends StatefulWidget {
  final String source;
  final Deposit depositData;
  final List<dynamic> promoCodes;
  final Map<String, dynamic> promoCode;
  final Map<String, dynamic> paymentConfig;
  final Function(Map<String, dynamic>) paymentComplete;
  final int prefilledAmount;
  final String prefilledPromoCode;
  final bool isFullScreen;
  final double availableHeight;

  AddCash({
    this.promoCode,
    this.paymentComplete,
    @required this.source,
    @required this.promoCodes,
    @required this.depositData,
    @required this.paymentConfig,
    this.prefilledAmount,
    this.prefilledPromoCode,
    this.isFullScreen = false,
    this.availableHeight,
  });

  @override
  State<StatefulWidget> createState() => AddCashState();
}

class AddCashState extends State<AddCash>
    with BasePage, CashFreePayment, Razorpay {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final FocusNode customAmountFocusNode = FocusNode();
  final FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();
  final TextEditingController customAmountController = TextEditingController();

  final AddCashAPI _addCashAPI = AddCashAPI();
  final AnalyticsEvents _analyticsEvents = AnalyticsEvents();

  double amount = 0;
  int goldCount = 0;
  bool isTile = true;
  int screenType = 1;
  double bonusAmount = 0;
  int baseAmountGoldbar = 1;
  bool bRepeatTransaction = true;
  Map<String, dynamic> promoCode;
  bool shouldAutoValidate = false;

  @override
  void initState() {
    super.initState();
    setData();
    initWebview(context);
    WidgetsBinding.instance
        .addPostFrameCallback((_) => onWidgetLoadFinished(context));

    if (widget.prefilledAmount != null && widget.prefilledAmount > 0) {
      customAmountController.text = widget.prefilledAmount.toString();
    }
  }

  initWebview(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    try {
      await flutterWebviewPlugin
          .launch(
            config.apiUrl + AppUrl.COOKIE_PAGE.toString(),
            hidden: true,
          )
          .timeout(Duration(seconds: 4));
    } catch (e) {}
  }

  void setData() {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    customAmountController.addListener(() {
      _onAmountChange();
    });

    promoCode = widget.promoCode;
    if (widget.paymentConfig != null) {
      screenType = widget.paymentConfig["screenType"] ?? 1;
      baseAmountGoldbar = widget.paymentConfig["baseAmountGoldbar"] ?? 1;
    }
    if (widget.depositData != null) {
      widget.depositData.bAllowRepeatDeposit =
          widget.depositData.bAllowRepeatDeposit &&
              (config.channelId != 201 && config.channelId != 202);

      if (!(widget.depositData.bAllowRepeatDeposit &&
          widget.depositData.paymentDetails.lastPaymentArray != null &&
          widget.depositData.paymentDetails.lastPaymentArray.length != 0)) {
        bRepeatTransaction = false;
      }

      if (widget.depositData.paymentDetails.lastPaymentArray.length > 0 &&
          widget.depositData.paymentDetails.lastPaymentArray[0]["amount"] > 0) {
        customAmountController.text = widget
            .depositData.paymentDetails.lastPaymentArray[0]["amount"]
            .toString();
        amount = widget.depositData.paymentDetails.lastPaymentArray[0]["amount"]
            .toDouble();
      }
    }
  }

  _onAmountChange() {
    if (customAmountController.text.isEmpty) {
      setState(() {
        amount = 0.0;
      });
    } else {
      if (amount.toInt().toString() != customAmountController.text) {
        var temp = int.tryParse(customAmountController.text);
        if (temp != null) {
          setState(() {
            amount = double.tryParse(customAmountController.text);
          });
          if (widget.paymentConfig["showBonusOnTile"]) {
            autoApplyPromo(amount.toInt());
          } else {
            promoCode = null;
            bonusAmount = 0;
            goldCount = 0;
          }
        }
      }
    }
    if (shouldAutoValidate) formKey.currentState.validate();
  }

  onWidgetLoadFinished(BuildContext context) {
    _analyticsEvents.onAddCashLoaded(
      context,
      source: widget.source,
    );
    AppConfig config = Provider.of(context, listen: false);
    HttpManager.channelId = config.channelId.toString();
    updateUserDetails();
    addWebEngageAddCashPageLoadedEvent(context);
    try {
      MyWebEngageService().webengageAddScreenData(context: context, data: {
        "screenName": "addcash",
        "data": {},
      });
    } catch (e) {}
  }

  addWebEngageAddCashPageLoadedEvent(BuildContext context) {
    User user = Provider.of(context, listen: false);
    try {
      Map<String, dynamic> data = new Map<String, dynamic>();
      data["channelId"] = HttpManager.channelId;
      data["isFirstDeposit"] = user.isFirstDeposit;
      MyWebEngageService().webengageAddCashPageVisitEvent(data);
    } catch (e) {}
  }

  updateUserDetails() async {
    User user = Provider.of(context, listen: false);
    final result = await _addCashAPI.getUserDetails(context);
    if (!result["error"]) {
      user.copyFrom(result["data"]);
    }
  }

  getLastPaymentOptionRow() {
    List<dynamic> lastPayment =
        widget.depositData.paymentDetails.lastPaymentArray;
    return widget.depositData.bAllowRepeatDeposit &&
            lastPayment != null &&
            lastPayment.length > 0
        ? Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(12.0, 2.0, 0.0, 2.0),
                child: Checkbox(
                    value: bRepeatTransaction,
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    activeColor: Theme.of(context).buttonColor,
                    onChanged: (bool checked) {
                      _analyticsEvents.onRepeat(
                        context,
                        source: widget.source,
                        amount: amount,
                        promoCode:
                            promoCode == null ? "" : promoCode["promoCode"],
                        checked: !bRepeatTransaction,
                        paymentMode: widget.depositData.paymentDetails
                            .lastPaymentArray[0]["modeOptionId"],
                      );
                      setState(() {
                        bRepeatTransaction = !bRepeatTransaction;
                      });
                    }),
              ),
              Text(
                "Proceed with ${widget.depositData.paymentDetails.lastPaymentArray[0]['label'].toString()}",
                style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                      fontSize: 16.0,
                    ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 8.0),
                child: lastPayment[0]["logoUrl"].endsWith(".svg")
                    ? SvgPicture.network(
                        lastPayment[0]["logoUrl"],
                        width: 24.0,
                      )
                    : CachedNetworkImage(
                        imageUrl: lastPayment[0]["logoUrl"],
                        width: 24.0,
                      ),
              ),
            ],
          )
        : Container();
  }

  addWebEnggaeInitPayEvent(Map<String, dynamic> paymentModeDetails) {
    Map<String, dynamic> data = paymentModeDetails;
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = (widget.depositData != null &&
            widget.depositData.paymentDetails != null)
        ? widget.depositData.paymentDetails.isFirstDeposit
        : false;
    data["pageSource"] = "add_cash_page";
    MyWebEngageService().webengageAddDepositIntiatedEvent(data);
  }

  double getPercentageAmount(
      Map<String, dynamic> promoCode, int amount, double percentage) {
    int maxDiscount = promoCode["maximum"];
    double uMaxExtra = (maxDiscount * percentage / 100.0);
    double uExtra = (amount * (promoCode["percentage"]) * percentage / 10000.0);

    uExtra = uExtra > uMaxExtra ? uMaxExtra : uExtra;
    return uExtra;
  }

  onInitPayment(BuildContext context) async {
    User user = Provider.of(context, listen: false);
    Map<String, dynamic> paymentModeDetails =
        widget.depositData.paymentDetails.lastPaymentArray[0];
    await validatePaymentMode(context);
    // bool bisPaytmAppInstalled = await isPaytmAppInstalled(context);
    // try {
    //   addWebEnggaeInitPayEvent(paymentModeDetails);
    // } catch (e) {}

    // onRepeatTransaction(
    //   context,
    //   source: widget.source,
    //   amount: amount,
    //   promoCode: promoCode == null ? "" : promoCode["promoCode"],
    //   paymentMode: widget.depositData.paymentDetails.lastPaymentArray[0]
    //       ["modeOptionId"],
    //   checked: true,
    //   gateway: widget
    //       .depositData.paymentDetails.lastPaymentArray[0]["gatewayId"]
    //       .toString(),
    // );

    // if (paymentModeDetails["gateway"] == "PAYTMWALLET") {
    //   if (bisPaytmAppInstalled) {
    //     openPaytmNativePayment(context,
    //         amount: amount.toInt(),
    //         paymentModeDetails: paymentModeDetails,
    //         promoCode: promoCode == null ? null : promoCode["promoCode"],
    //         userDetails: {
    //           "email": user.email,
    //           "phone": user.mobile,
    //           "lastName": user.address.lastName,
    //           "firstName": user.address.firstName,
    //         },
    //         isFirstDeposit: false, onComplete: (payload, response) {
    //       onPaymentResponse(
    //           {}..addAll(payload)..addAll(paymentModeDetails), response);
    //     });
    //   }
    // } else
    if (paymentModeDetails["isSeamless"]) {
      if (paymentModeDetails["gateway"] == "RAZORPAY") {
        openRazorpay(
          context,
          amount: amount.toInt(),
          paymentModeDetails: paymentModeDetails,
          promoCode: promoCode == null ? null : promoCode["promoCode"],
          userDetails: {
            "email": user.email,
            "phone": user.mobile,
            "lastName": user.address != null ? user.address.lastName : "",
            "firstName": user.address != null ? user.address.firstName : "",
          },
          isFirstDeposit: false,
          onComplete: (payload, response) {
            onPaymentResponse({}..addAll(payload)..addAll(paymentModeDetails),
                response["data"]);
          },
        );
      }
    } else {
      Map<String, dynamic> result = await openCashFreePayment(
        context,
        amount: amount.toInt(),
        paymentModeDetails: paymentModeDetails,
        promoCode: promoCode == null ? null : promoCode["promoCode"],
        userDetails: {
          "email": user.email,
          "phone": user.mobile,
          "lastName": user.address.lastName,
          "firstName": user.address.firstName,
        },
        isFirstDeposit: false,
      );
      onPaymentResponse({}..addAll(paymentModeDetails), result);
    }
  }

  Future<Map<String, dynamic>> validatePaymentMode(context) async {
    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> paymentModeData =
        await _addCashAPI.proceedToPaymentMode(
      context,
      amount: amount.toInt(),
      promo: promoCode == null ? "" : promoCode["promoCode"],
    );

    Loader().showLoader(false);

    return paymentModeData;
  }

  addWebEngagePaymentModePageVisitEvent() {
    Map<String, dynamic> data = new Map<String, dynamic>();
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = (widget.depositData != null &&
            widget.depositData.paymentDetails != null)
        ? widget.depositData.paymentDetails.isFirstDeposit
        : false;
    data["pageSource"] = "add_cash_page";
    data["userSelectedAmount"] = amount != null ? amount.toString() : "0.0";
    MyWebEngageService().webengagePaymentModePageVisitEvent(data);
  }

  addWebEngagePaymentDropOffEvent() {
    Map<String, dynamic> data = new Map<String, dynamic>();
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = widget.depositData.paymentDetails.isFirstDeposit;
    data["pageSource"] = "paymentmode_page";
    data["userSelectedAmount"] = amount.toString();
    MyWebEngageService().webengageAddPaymentDropOffEvent(data);
  }

  addWebEngagePaymentFailedEvent(Map<String, dynamic> response) {
    try {
      Map<String, dynamic> data = response;
      data["channelId"] = HttpManager.channelId;
      data["isFirstDeposit"] = widget.depositData.paymentDetails.isFirstDeposit;
      data["pageSource"] = "add_cash_page";
      MyWebEngageService().webengageAddTransactionFailedEvent(data);
    } catch (e) {
      print(e);
    }
  }

  addWebEngagePaymentSuccessEvent(Map<String, dynamic> response) {
    Map<String, dynamic> data = response;
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = (widget.depositData != null &&
            widget.depositData.paymentDetails != null)
        ? widget.depositData.paymentDetails.isFirstDeposit
        : false;
    data["pageSource"] = "add_cash_page";
    MyWebEngageService().webengageAddTransactionSuccessEvent(data);
  }

  void launchPaymentMode(context) async {
    Map<String, dynamic> paymentModeData = await validatePaymentMode(context);

    if (paymentModeData["error"] == true) {
      showMessageOnTop(context, msg: paymentModeData["message"]);
      return;
    }
    addWebEngagePaymentModePageVisitEvent();

    Map<String, dynamic> result = await routeManager.launchPaymentMode(context,
        source: widget.source,
        promo: promoCode,
        amount: amount,
        goldCount: goldCount,
        bonusAmount: bonusAmount,
        paymentModeDetails: paymentModeData["data"],
        disclaimerText: widget.paymentConfig["disclaimerText"],
        stripe: widget.paymentConfig["stripe"]);

    if (result != null) {
      if (result["cancel"] != null && result["cancel"] == true) {
        widget.paymentComplete(null);
        addWebEngagePaymentDropOffEvent();
      } else {
        onPaymentResponse(paymentModeData, result);
      }
    } else {
      FocusScope.of(context).unfocus();
    }
  }

  onPaymentResponse(
      Map<String, dynamic> payload, Map<String, dynamic> response) async {
    if (response["error"] != null && response["error"]) {
      addWebEngagePaymentFailedEvent(response["data"]);
      MyBranchIoService().branchEventTransactionFailed(response);
      if (response["message"] != null) {
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                  child: Text(
                    response["message"],
                    textAlign: TextAlign.center,
                    softWrap: true,
                    style:
                        Theme.of(context).primaryTextTheme.headline5.copyWith(
                              color: Colors.grey.shade700,
                            ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 78.0),
                  child: Button(
                    size: ButtonSize.medium,
                    text: "OK",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          },
        );
        _analyticsEvents.paymentFailed(
          source: widget.source,
          amount: amount.toInt(),
          paymentPayload: payload,
          paymentResponse: response["data"],
          promoCode: promoCode == null ? "" : promoCode["promoCode"],
          firstDeposit: widget.depositData.paymentDetails.isFirstDeposit,
        );
      } else {
        Map<String, dynamic> result = await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return TransactionFailed(response["data"]);
          },
        );

        if (result != null && result["cancel"]) {
          _analyticsEvents.paymentFailedCancel(
            source: widget.source,
            amount: amount.toInt(),
            transactionResult: response["data"],
            promoCode: promoCode == null ? "" : promoCode["promoCode"],
          );
          Navigator.of(context).pop();
        } else {
          _analyticsEvents.paymentFailedRetry(
            source: widget.source,
            amount: amount.toInt(),
            transactionResult: response["data"],
            promoCode: promoCode == null ? "" : promoCode["promoCode"],
          );
        }
      }
    } else {
      try {
        addWebEngagePaymentSuccessEvent(response["data"]);
      } catch (e) {}
      // _analyticsEvents.paymentSuccess(
      //   amount: amount.toInt(),
      //   paymentPayload: payload,
      //   paymentResponse: response,
      //   promoCode: promoCode == null ? "" : promoCode["promoCode"],
      //   firstDeposit: widget.depositData.paymentDetails.isFirstDeposit,
      // );
      if (widget.paymentComplete != null) {
        widget.paymentComplete(response);
      } else {
        if (response != null) {
          response["goldCount"] = goldCount;
          response["bonusAmount"] = bonusAmount;
        }
        Navigator.of(context).pop(response);
      }
    }
  }

  onAddCash() {
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }
    // to be added after we store card details on db
    if (bRepeatTransaction) {
      onInitPayment(context);
    } else {
      launchPaymentMode(context);
    }
  }

  onSubmitCustomAmount() {
    _analyticsEvents.onAddCashProceed(
      context,
      source: widget.source,
      amount: amount.toDouble(),
      promoCode: promoCode != null ? promoCode["promoCode"] : "",
      isCustomAmount: true,
    );
    shouldAutoValidate = true;
    if (formKey.currentState.validate()) {
      onAddCash();
      _analyticsEvents.onPayNowScreen(
        context,
        source: widget.source,
        amount: amount,
        promoCode: promoCode != null ? promoCode["promoCode"] : "",
      );
    } else {
      _analyticsEvents.onAddCashProceedError(
        context,
        source: widget.source,
        amount: amount,
        promoCode: promoCode != null ? promoCode["promoCode"] : "",
        error: true,
      );
    }
  }

  getMaximumDiscountOfferIndex(choosenAmount) {
    int offerIndex = -1;
    double discountAmount = 0;
    double instantCash = 0;
    double playableCash = 0;
    int i = 0;
    widget.promoCodes.forEach((promoCode) {
      if (choosenAmount >= promoCode["minimum"]) {
        double curDiscount = choosenAmount * promoCode["percentage"] / 100.0;
        double curInstantCash =
            curDiscount * promoCode["instantCashPercentage"] / 100.0;
        double curPlayableCash =
            curDiscount * promoCode["playablePercentage"] / 100.0;
        curDiscount = (curDiscount > promoCode["maximum"].toDouble()
            ? promoCode["maximum"].toDouble()
            : curDiscount);

        if (discountAmount < curDiscount ||
            (discountAmount == curDiscount && instantCash < curInstantCash) ||
            (discountAmount == curDiscount &&
                instantCash == curInstantCash &&
                playableCash < curPlayableCash)) {
          instantCash = curInstantCash;
          discountAmount = curDiscount;
          offerIndex = i;
        }
      }

      i++;
    });

    return offerIndex;
  }

  autoApplyPromo(int amount) {
    int offerIndex = getMaximumDiscountOfferIndex(amount);
    if (offerIndex > -1) {
      validateAndApplyPromo(widget.promoCodes[offerIndex]);
    } else {
      validateAndApplyPromo(null);
    }
  }

  validateAndApplyPromo(Map<String, dynamic> promoToApply) async {
    double newAmount = customAmountController.text.isNotEmpty
        ? double.tryParse(customAmountController.text)
        : 0;
    if (promoToApply != null) {
      if (promoToApply["minimum"] != null && promoToApply["minimum"] > amount) {
        customAmountController.text = promoToApply["minimum"].toString();
      }
      if (promoToApply["minimum"] != null) {
        newAmount = double.tryParse(customAmountController.text) ??
            promoToApply["minimum"].toDouble();
      }

      final result = await _addCashAPI.validatePromo(
        context,
        amount: newAmount.toInt(),
        promoCode: promoToApply["promoCode"],
      );

      if (result["error"]) {
        setState(() {
          promoCode = null;
          bonusAmount = 0;
          goldCount = 0;
        });
        if (result["message"] != null)
          showMessageOnTop(context, msg: result["message"]);
      } else {
        double calculatedBonus = 0.0;

        if (result["details"] != null) {
          calculatedBonus = newAmount * result["details"]["percentage"] / 100;
          if (newAmount < result["details"]["minimum"]) {
            calculatedBonus = 0;
          } else if (calculatedBonus > result["details"]["maximum"]) {
            calculatedBonus = (result["details"]["maximum"]).toDouble();
          }
        }

        setState(() {
          this.amount = newAmount;
          promoCode = result["details"];
          bonusAmount = calculatedBonus;
          goldCount = (newAmount.toInt() *
                  ((result["details"]["gemsPercentage"] ?? 0) / 100.0))
              .toInt();
        });
      }
    } else {
      setState(() {
        promoCode = null;
        bonusAmount = 0;
        goldCount = 0;
      });
    }
  }

  openPromoDialog() async {
    if (customAmountFocusNode != null) {
      customAmountFocusNode.unfocus();
    }
    Map<String, dynamic> promoToApply = await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      builder: (BuildContext context) {
        return PromoInput(
          amount: customAmountController.text == "" ? -1 : amount.toInt(),
          appliedPromo: promoCode,
          promoCodes: widget.promoCodes,
        );
      },
    );
    if (promoToApply != null) {
      validateAndApplyPromo(promoToApply);
    }
  }

  Widget getBody() {
    return Column(
      children: [
        Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                if (widget.promoCodes != null &&
                    widget.promoCodes.length > 0 &&
                    widget.paymentConfig["showGoldbar"])
                  Container(
                    padding:
                        EdgeInsets.only(left: 24.0, right: 24.0, top: 16.0),
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Image.asset(
                          "images/ribbon.png",
                          fit: BoxFit.fitWidth,
                          package: "payments",
                        ),
                        FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Get ",
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline6
                                    .copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 22.0,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 0.0,
                                      color: Colors.black,
                                      offset: Offset(0.0, 1.0),
                                    ),
                                  ],
                                ),
                              ),
                              Image.asset(
                                "images/currency/diamond.png",
                                height: 22.0,
                              ),
                              GradientText(
                                "${(((widget.promoCodes[0]["gemsPercentage"] ?? 0) / 100.0) * baseAmountGoldbar).toInt()}",
                                style: TextStyle(
                                  fontSize: 28.0,
                                  fontWeight: FontWeight.bold,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 0.0,
                                      color: Colors.black,
                                      offset: Offset(0.0, 1.0),
                                    ),
                                  ],
                                ),
                                gradient: LinearGradient(
                                  colors: [
                                    Colors.yellow,
                                    Colors.yellow,
                                    Colors.red,
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                              ),
                              Text(
                                "  for every  ",
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline6
                                    .copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 22.0,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 0.0,
                                      color: Colors.black,
                                      offset: Offset(0.0, 1.0),
                                    ),
                                  ],
                                ),
                              ),
                              GradientText(
                                CurrencyFormat.format(
                                    baseAmountGoldbar.toDouble()),
                                style: TextStyle(
                                  fontSize: 28.0,
                                  fontWeight: FontWeight.bold,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 0.0,
                                      color: Colors.black,
                                      offset: Offset(0.0, 1.0),
                                    ),
                                  ],
                                ),
                                gradient: LinearGradient(
                                  colors: [
                                    Colors.yellow,
                                    Colors.yellow,
                                    Colors.red,
                                  ],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                              ),
                              Text(
                                " you deposit",
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline6
                                    .copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 22.0,
                                  shadows: [
                                    Shadow(
                                      blurRadius: 0.0,
                                      color: Colors.black,
                                      offset: Offset(0.0, 1.0),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                Padding(
                  padding: EdgeInsets.only(
                      left: 18.0, right: 18.0, top: 12.0, bottom: 4.0),
                  child: AddCashTemplate(
                    screenType: screenType,
                    showBonusOnTile: widget.paymentConfig == null
                        ? false
                        : widget.paymentConfig["showBonusOnTile"],
                    onTileSelect: (int amount, double bonusAmount,
                        Map<String, dynamic> promoCode) {
                      _analyticsEvents.onAddCashProceed(
                        context,
                        source: widget.source,
                        amount: amount.toDouble(),
                        promoCode:
                            promoCode != null ? promoCode["promoCode"] : "",
                        isCustomAmount: false,
                      );
                      _analyticsEvents.onDepositTileClicked(
                        context,
                        source: widget.source,
                        amount: amount.toDouble(),
                        promoCode:
                            promoCode != null ? promoCode["promoCode"] : "",
                        instantCash: promoCode != null
                            ? getPercentageAmount(
                                promoCode,
                                amount,
                                double.tryParse(
                                    promoCode["instantCashPercentage"]
                                        .toString()))
                            : 0,
                        bonus: promoCode != null
                            ? getPercentageAmount(
                                promoCode,
                                amount,
                                double.tryParse(
                                    promoCode["playablePercentage"].toString()))
                            : 0,
                        lockedBonus: promoCode != null
                            ? getPercentageAmount(
                                promoCode,
                                amount,
                                double.tryParse(
                                    promoCode["nonPlayablePercentage"]
                                        .toString()))
                            : 0,
                      );

                      this.amount = amount.toDouble();
                      customAmountController.text = amount.toString();
                      this.promoCode = promoCode;
                      this.bonusAmount = bonusAmount;
                      this.goldCount = promoCode != null
                          ? (amount.toInt() *
                                  ((promoCode["gemsPercentage"] ?? 0) / 100.0))
                              .toInt()
                          : 0;
                      onAddCash();
                    },
                    selectedPromocode: promoCode,
                    selectedAmount: amount.toInt(),
                    chooseAmountData: widget.depositData.paymentDetails,
                    hotAmounts: widget.depositData.paymentDetails.hotTiles,
                    bestAmounts: widget.depositData.paymentDetails.bestTiles,
                    promoCodes: widget.promoCodes,
                  ),
                ),
              ],
            ),
          ],
        ),
        if (screenType == 1)
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
            child: Stack(
              children: <Widget>[
                Form(
                  key: formKey,
                  child: Stack(
                    alignment: Alignment.topRight,
                    children: <Widget>[
                      SimpleTextBox(
                        controller: customAmountController,
                        focusNode: customAmountFocusNode,
                        labelText: "Enter Amount",
                        labelStyle: TextStyle(color: Colors.grey.shade700),
                        isDense: true,
                        maxLength: widget
                            .depositData.paymentDetails.depositLimit
                            .toString()
                            .length,
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 12.0),
                        borderColor: Colors.grey.shade500,
                        focusedBorderColor: Colors.green,
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: true),
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        validator: (_) {
                          if (customAmountController.text.isEmpty) {
                            return "*Required";
                          }
                          if ((int.tryParse(customAmountController.text) ?? 0) <
                                  widget.depositData.paymentDetails.minAmount ||
                              (int.tryParse(customAmountController.text) ?? 0) >
                                  widget.depositData.paymentDetails
                                      .depositLimit) {
                            return "Enter amount between ${CurrencyFormat.format(widget.depositData.paymentDetails.minAmount.toDouble())} and ${CurrencyFormat.format(widget.depositData.paymentDetails.depositLimit.toDouble())}";
                          }
                          shouldAutoValidate = false;
                          return null;
                        },
                        onChanged: (amount) {
                          autoApplyPromo(int.parse(amount));
                        },
                      ),
                      FlatButton(
                        padding: EdgeInsets.only(top: 2.0, right: 12.0),
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        textColor: Color.fromRGBO(49, 145, 185, 1),
                        disabledTextColor: Colors.grey.shade500,
                        onPressed: () {
                          _analyticsEvents.onHavePromoClicked(context,
                              source: widget.source);
                          FocusScope.of(context).unfocus();
                          openPromoDialog();
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              promoCode == null
                                  ? "Have a Promocode"
                                  : "Change Promocode",
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              "?",
                              style: TextStyle(
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        if (screenType == 1 && promoCode != null)
          AppliedPromo(
            amount: amount.toInt(),
            promoCode: promoCode,
            showLockedBonus: widget.depositData.showLockedAmount,
            getPercentageAmount: getPercentageAmount,
            onRemove: () {
              setState(() {
                promoCode = null;
                bonusAmount = 0;
                goldCount = 0;
              });
            },
          ),
        if (widget.depositData != null) getLastPaymentOptionRow(),
        if (screenType == 1)
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 18.0, right: 18.0, top: 2.0),
                  child: amount > 0
                      ? Button(
                          isCurrencyText: true,
                          showCurrencyDelimiters: false,
                          amount: amount.toDouble(),
                          prefix: "Add Cash ".toUpperCase(),
                          onPressed: () {
                            onSubmitCustomAmount();
                          },
                        )
                      : Button(
                          text: "Add Cash".toUpperCase(),
                          onPressed: () {
                            onSubmitCustomAmount();
                          },
                        ),
                ),
              ),
            ],
          ),
        SizedBox(
          height: 60.0,
        ),
      ],
    );
  }

  Widget getFooter() {
    return Container(
      height: 104.0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          PaymentPartners(
            disclaimerText: widget.paymentConfig["disclaimerText"],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (widget.depositData == null) {
      return Container();
    }

    if (widget.isFullScreen) {
      return Scaffold(
        appBar: CommonAppBar(
          children: <Widget>[
            Text(
              "Enter Amount".toUpperCase(),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: getBody(),
        ),
        bottomNavigationBar: getFooter(),
      );
    }

    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: getBody(),
            ),
          ),
          getFooter(),
        ],
      ),
    );
  }
}
