import 'package:flutter/material.dart';

class PaymentPartners extends StatelessWidget {
  final String disclaimerText;

  PaymentPartners({
    @required this.disclaimerText,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 12.0, top: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Image.asset(
                    "images/payment/visa.png",
                    height: 36.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Image.asset(
                    "images/payment/master_card.png",
                    height: 36.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: Image.asset(
                    "images/payment/veri_sign.png",
                    height: 36.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(),
                  child: Image.asset(
                    "images/payment/ssl.png",
                    height: 36.0,
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "$disclaimerText",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).primaryTextTheme.caption.copyWith(
                        color: Colors.grey.shade700,
                      ),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 4.0, bottom: 8.0),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style:
                          Theme.of(context).primaryTextTheme.caption.copyWith(
                                color: Colors.grey.shade700,
                                fontSize: 10.0,
                              ),
                      children: [
                        TextSpan(
                          text: "Bonus credit is subject to ",
                        ),
                        TextSpan(
                          text: "Terms and Conditions*",
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
