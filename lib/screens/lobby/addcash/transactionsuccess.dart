import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class TransactionSuccess extends StatelessWidget {
  final Map<String, dynamic> transactionResult;

  final double bonusAmount;
  final int goldCount;

  TransactionSuccess(this.transactionResult, this.bonusAmount, this.goldCount);

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);
    DateTime date = DateTime.fromMillisecondsSinceEpoch(
        int.parse(transactionResult["date"].toString()));
    String dateString = date.day.toString() +
        "-" +
        date.month.toString() +
        "-" +
        date.year.toString();

    return CustomDialog(
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 4.0, vertical: 16.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 16.0),
                            child: Text(
                              "Transaction Successful",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .headline5
                                  .copyWith(
                                    color: Color.fromRGBO(0, 112, 0, 1),
                                    fontWeight: FontWeight.w600,
                                  ),
                            ),
                          ),
                          Container(
                            // color: Color.fromRGBO(250, 250, 235, 1),
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 8.0),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Amount",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                              color:
                                                  Color.fromRGBO(75, 75, 75, 1),
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                      CurrencyText(
                                        showCurrencyDelimiters: false,
                                        amount: double.tryParse(
                                            transactionResult["amount"]
                                                .toString()),
                                        decimalDigits: initData.experiments
                                            .miscConfig.amountPrecision,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Updated Balance",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                              color:
                                                  Color.fromRGBO(75, 75, 75, 1),
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                      Consumer<User>(
                                          builder: (context, user, child) {
                                        return CurrencyText(
                                          showCurrencyDelimiters: false,
                                          amount: user.userBalance
                                              .getTotalBalance(),
                                          decimalDigits: initData.experiments
                                              .miscConfig.amountPrecision,
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline6
                                              .copyWith(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.bold),
                                        );
                                      }),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Transaction Date",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                              color:
                                                  Color.fromRGBO(75, 75, 75, 1),
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                      Text(
                                        "$dateString",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Order Id",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                              color:
                                                  Color.fromRGBO(75, 75, 75, 1),
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                      Text(
                                        transactionResult["orderId"].toString(),
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          if (goldCount > 0)
                            Container(
                              padding: EdgeInsets.only(
                                  top: 8.0, left: 8.0, right: 8.0, bottom: 4.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "You got ",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Color.fromRGBO(0, 112, 0, 1),
                                          fontWeight: FontWeight.w500,
                                        ),
                                  ),
                                  Image.asset(
                                    "images/currency/diamond.png",
                                    height: 22.0,
                                  ),
                                  Text(
                                    "$goldCount Gems",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Color.fromRGBO(0, 112, 0, 1),
                                          fontWeight: FontWeight.w500,
                                        ),
                                  ),
                                ],
                              ),
                            ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 140.0,
                            child: Button(
                              size: ButtonSize.medium,
                              text: "Ok".toUpperCase(),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            IconButton(
              padding: EdgeInsets.all(0.0),
              icon: Image.asset(
                "images/icons/close2.png",
                height: 32.0,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}
