import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/screens/lobby/addcash/promotile.dart';

class PromoInput extends StatefulWidget {
  final int amount;
  final List<dynamic> promoCodes;
  final Map<String, dynamic> appliedPromo;

  PromoInput({this.promoCodes, this.amount = 0, this.appliedPromo});

  @override
  _PromoInputState createState() => _PromoInputState();
}

class _PromoInputState extends State<PromoInput> {
  final FocusNode promoCodeFocusNode = FocusNode();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController promoController = TextEditingController();
  Map<String, dynamic> selectedPromo;

  bool applyCustomPromo = false;

  @override
  void initState() {
    promoController.addListener(() {
      var promoMatch = widget.promoCodes.firstWhere(
          (element) => element["promoCode"] == promoController.text,
          orElse: () => null);
      if (promoMatch != null) {
        selectedPromo = promoMatch;
      } else {
        setState(() {
          selectedPromo = null;
        });
      }

      setState(() {
        applyCustomPromo = promoController.text.isNotEmpty;
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    promoCodeFocusNode.requestFocus(FocusNode());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: EdgeInsets.symmetric(
        horizontal: 4.0,
      ),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 16.0,
                        bottom: 8.0,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "Apply Promocode",
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Colors.grey.shade800,
                                  fontWeight: FontWeight.bold,
                                ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 16.0,
                                  right: 16.0,
                                  top: 20.0,
                                  bottom: 8.0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      margin:
                                          EdgeInsets.symmetric(vertical: 8.0),
                                      child: Form(
                                        key: formKey,
                                        child: SimpleTextBox(
                                          controller: promoController,
                                          focusNode: promoCodeFocusNode,
                                          isDense: true,
                                          maxLines: 1,
                                          hintText: "Enter Promocode",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0),
                                          textCapitalization:
                                              TextCapitalization.characters,
                                          focusedBorderColor: Colors.green,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return "Enter promo to apply.";
                                            }
                                            return null;
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 8.0),
                              child: Column(
                                children: <Widget>[
                                  for (var promo in widget.promoCodes)
                                    if ((promo["minimum"] <= widget.amount) ||
                                        widget.amount == -1)
                                      PromoTile(
                                        promoCode: promo,
                                        isSelected: selectedPromo != null &&
                                            selectedPromo["promoCode"] ==
                                                promo["promoCode"],
                                        onSelect: () {
                                          promoController.text =
                                              promo["promoCode"];
                                          setState(() {
                                            selectedPromo = promo;
                                          });
                                        },
                                      ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 56.0,
                              child: Button(
                                size: ButtonSize.large,
                                text: "Apply".toUpperCase(),
                                onPressed: () {
                                  if (selectedPromo != null) {
                                    Navigator.of(context).pop(selectedPromo);
                                  } else {
                                    if (formKey.currentState.validate()) {
                                      Navigator.of(context).pop({
                                        "promoCode": promoController.text,
                                      });
                                    }
                                  }
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            IconButton(
              padding: EdgeInsets.all(0.0),
              icon: Image.asset(
                "images/icons/close2.png",
                height: 32.0,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}
