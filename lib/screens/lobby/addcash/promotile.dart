import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class PromoTile extends StatefulWidget {
  final Function onSelect;
  final bool isSelected;
  final Map<String, dynamic> promoCode;

  PromoTile({
    this.promoCode,
    this.isSelected,
    this.onSelect,
  });

  @override
  _PromoTileState createState() => _PromoTileState();
}

class _PromoTileState extends State<PromoTile> {
  bool showMore = false;

  String getPromoText() {
    int maximum = widget.promoCode["maximum"];
    String percentage = widget.promoCode["percentage"].toString();
    switch (widget.promoCode['bonusVisibility']) {
      case 0:
        percentage += '%';
        break;
      case 1:
        percentage = 'upto ${CurrencyFormat.format(maximum.toDouble())} bonus';
        break;
      case 2:
        percentage = 'upto ' + percentage + '%';
        break;
      case 3:
        percentage = '${CurrencyFormat.format(maximum.toDouble())} bonus';
        break;
    }
    if (widget.promoCode['percentage'] == 0) return "";
    return "Get " +
        percentage +
        " on min. deposit of ${CurrencyFormat.format((widget.promoCode["minimum"] as int).toDouble())}";
  }

  double getLockedBonus() {
    return widget.promoCode["maximum"] *
        widget.promoCode["nonPlayablePercentage"] /
        100;
  }

  double getPlayableBonus() {
    return widget.promoCode["maximum"] *
        widget.promoCode["playablePercentage"] /
        100;
  }

  double getInstantCash() {
    return widget.promoCode["maximum"] *
        widget.promoCode["instantCashPercentage"] /
        100;
  }

  Widget getCheckBox({
    @required String label,
    @required bool isChecked,
    @required Function onPressed,
  }) {
    return InkWell(
      child: Row(
        children: <Widget>[
          Container(
            width: 20.0,
            height: 20.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(
                width: 1.0,
                color: Colors.grey.shade400,
              ),
            ),
            alignment: Alignment.center,
            child: Container(
              height: 16.0,
              width: 16.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isChecked ? Colors.green : Colors.transparent,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              label,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.grey.shade600,
                  ),
            ),
          ),
        ],
      ),
      onTap: onPressed != null
          ? () {
              onPressed();
            }
          : null,
    );
  }

  @override
  Widget build(BuildContext context) {
    TextStyle termsTextStyle =
        Theme.of(context).primaryTextTheme.bodyText2.copyWith(
              color: widget.isSelected
                  ? Theme.of(context).buttonColor
                  : Colors.grey.shade600,
              fontWeight: FontWeight.normal,
              fontSize: 13.0,
            );
    double lockedBonus = getLockedBonus().roundToDouble();
    double playableBonus = getPlayableBonus().roundToDouble();
    double instantCash = getInstantCash().roundToDouble();
    double bonusAmount = lockedBonus + playableBonus;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
      child: InkWell(
        onTap: () {
          widget.onSelect();
        },
        child: DottedBorder(
          color: widget.isSelected
              ? Theme.of(context).buttonColor
              : Colors.grey.shade600,
          dashPattern: [6.0, 2.0],
          borderType: BorderType.RRect,
          padding: EdgeInsets.all(0.0),
          radius: Radius.circular(4.0),
          strokeWidth: 1.0,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: getCheckBox(
                                  label: "",
                                  isChecked: widget.isSelected,
                                  onPressed: () {},
                                ),
                              ),
                              Text(
                                widget.promoCode["promoCode"],
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .subtitle1
                                    .copyWith(
                                      fontWeight: FontWeight.bold,
                                      color: widget.isSelected
                                          ? Theme.of(context).buttonColor
                                          : Colors.grey.shade800,
                                    ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 28.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding:
                                      EdgeInsets.only(top: 4.0, bottom: 4.0),
                                  child: Text(getPromoText(),
                                      textAlign: TextAlign.left,
                                      style: termsTextStyle),
                                ),
                                if (instantCash > 0 && bonusAmount > 0)
                                  Text(
                                    "(${CurrencyFormat.format(instantCash)} Instant Cash + ${CurrencyFormat.format(bonusAmount)} Bonus)",
                                    style: termsTextStyle,
                                  ),
                                if (instantCash <= 0 && bonusAmount > 0)
                                  Text(
                                    "(${CurrencyFormat.format(bonusAmount)} Bonus)",
                                    style: termsTextStyle,
                                  ),
                                if (instantCash > 0 && bonusAmount <= 0)
                                  Text(
                                    "(${CurrencyFormat.format(instantCash)} Instant Cash)",
                                    style: termsTextStyle,
                                  ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
