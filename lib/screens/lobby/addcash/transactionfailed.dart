import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class TransactionFailed extends StatelessWidget {
  final Map<String, dynamic> transactionResult;

  TransactionFailed(this.transactionResult);

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);
    DateTime date = DateTime.fromMillisecondsSinceEpoch(
        int.parse(transactionResult["date"].toString()));
    String dateString = date.day.toString() +
        "-" +
        date.month.toString() +
        "-" +
        date.year.toString();
    return CustomDialog(
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: Stack(
          alignment: Alignment.topRight,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 4.0, vertical: 16.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(bottom: 16.0),
                            child: Text(
                              "Transaction Failed",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .headline5
                                  .copyWith(
                                    color: Theme.of(context).errorColor,
                                    fontWeight: FontWeight.w600,
                                  ),
                            ),
                          ),
                          Container(
                            // color: Color.fromRGBO(250, 250, 235, 1),
                            padding: EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 8.0),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Amount",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                              color:
                                                  Color.fromRGBO(75, 75, 75, 1),
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                      CurrencyText(
                                        showCurrencyDelimiters: false,
                                        amount: double.parse(
                                            transactionResult["amount"]
                                                .toString()),
                                        decimalDigits: initData.experiments
                                            .miscConfig.amountPrecision,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Transaction Date",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                              color:
                                                  Color.fromRGBO(75, 75, 75, 1),
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                      Text(
                                        dateString,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Order Id",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                              color:
                                                  Color.fromRGBO(75, 75, 75, 1),
                                              fontWeight: FontWeight.w500,
                                            ),
                                      ),
                                      Text(
                                        transactionResult["orderId"].toString(),
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 140.0,
                            child: Button(
                              size: ButtonSize.medium,
                              text: "Retry".toUpperCase(),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            IconButton(
              padding: EdgeInsets.all(0.0),
              icon: Image.asset(
                "images/icons/close2.png",
                height: 32.0,
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      ),
    );
  }
}
