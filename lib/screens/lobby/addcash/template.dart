import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/models/addcash/paymentdetails.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class AddCashTemplate extends StatelessWidget {
  final int screenType;
  final int selectedAmount;
  final bool showBonusOnTile;
  final List<int> hotAmounts;
  final List<int> bestAmounts;

  final List<dynamic> promoCodes;
  final Map<String, dynamic> selectedPromocode;

  @required
  final PaymentDetails chooseAmountData;
  @required
  final Function(int, double, Map<String, dynamic>) onTileSelect;

  AddCashTemplate({
    this.selectedAmount,
    this.onTileSelect,
    this.chooseAmountData,
    this.hotAmounts = const [],
    this.bestAmounts = const [],
    this.promoCodes = const [],
    this.selectedPromocode,
    this.screenType,
    this.showBonusOnTile,
  });

  getMaximumDiscountOfferIndex(choosenAmount) {
    int offerIndex = -1;
    double discountAmount = 0;
    double instantCash = 0;
    double playableCash = 0;
    int i = 0;
    promoCodes.forEach((promoCode) {
      if (choosenAmount >= promoCode["minimum"]) {
        double curDiscount = choosenAmount * promoCode["percentage"] / 100.0;
        double curInstantCash =
            curDiscount * promoCode["instantCashPercentage"] / 100.0;
        double curPlayableCash =
            curDiscount * promoCode["playablePercentage"] / 100.0;
        curDiscount = (curDiscount > promoCode["maximum"].toDouble()
            ? promoCode["maximum"].toDouble()
            : curDiscount);

        if (discountAmount < curDiscount ||
            (discountAmount == curDiscount && instantCash < curInstantCash) ||
            (discountAmount == curDiscount &&
                instantCash == curInstantCash &&
                playableCash < curPlayableCash)) {
          instantCash = curInstantCash;
          discountAmount = curDiscount;
          offerIndex = i;
        }
      }

      i++;
    });

    return offerIndex;
  }

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.count(
      crossAxisCount: 2,
      mainAxisSpacing: 10.0,
      crossAxisSpacing: 10.0,
      staggeredTiles:
          List.generate(chooseAmountData.amountTiles.length, (index) {})
              .map<StaggeredTile>((_) => StaggeredTile.fit(1))
              .toList(),
      controller: ScrollController(keepScrollOffset: false),
      shrinkWrap: true,
      children: List.generate(chooseAmountData.amountTiles.length, (index) {
        double bonusGiven = 0;
        int amount = chooseAmountData.amountTiles[index];
        int offerIndex = getMaximumDiscountOfferIndex(amount);

        if (offerIndex != -1) {
          if (amount * promoCodes[offerIndex]["percentage"] / 100.0 >=
              promoCodes[offerIndex]["maximum"].toDouble()) {
            bonusGiven = promoCodes[offerIndex]["maximum"].toDouble();
          } else {
            bonusGiven =
                (amount * promoCodes[offerIndex]["percentage"] / 100.0);
          }
        }
        return Card(
          elevation: 4.0,
          clipBehavior: Clip.hardEdge,
          color: Colors.white,
          child: FlatButton(
            onPressed: () {
              onTileSelect(amount, bonusGiven,
                  offerIndex != -1 ? promoCodes[offerIndex] : null);
            },
            padding: EdgeInsets.all(8.0),
            child: Container(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      CurrencyFormat.format(amount.toDouble()),
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).primaryTextTheme.headline4.copyWith(
                                color: Colors.black,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: 20.0,
                          child: offerIndex > -1 && showBonusOnTile
                              ? FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: CurrencyText(
                                    showCurrencyDelimiters: false,
                                    amount: bonusGiven,
                                    decimalDigits: 0,
                                    prefix: "Get ",
                                    suffix: " Bonus",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline5
                                        .copyWith(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16.0),
                                  ),
                                )
                              : Container(),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Button(
                      size: ButtonSize.medium,
                      onPressed: () {
                        onTileSelect(amount, bonusGiven,
                            offerIndex != -1 ? promoCodes[offerIndex] : null);
                      },
                      text: "ADD CASH".toUpperCase(),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
