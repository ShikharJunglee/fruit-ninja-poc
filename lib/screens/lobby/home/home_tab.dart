import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedprefkeys.dart';

class HomeTabBar extends StatefulWidget {
  final List<String> tabs;
  final tabHeight;
  final TabController tabController;

  HomeTabBar({this.tabs, this.tabController, this.tabHeight});

  @override
  _HomeTabBarState createState() => _HomeTabBarState();
}

class _HomeTabBarState extends State<HomeTabBar> {
  int _selectedIndex = 0;
  double animationValue = 0.0;

  bool _isLeagueVisited = true;

  @override
  void initState() {
    setData();
    super.initState();
  }

  setData() async {
    widget.tabController.addListener(() {
      if (!widget.tabController.indexIsChanging) onTabChanged();
    });
    widget.tabController.animation.addListener(onAnimation);
    widget.tabController.animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        setState(() {
          _selectedIndex = widget.tabController.index;
        });
      }
    });

    String isVisited =
        await SharedPrefHelper().getFromSharedPref(SharedPrefKey.leagueVisited);
    if (mounted) {
      setState(() {
        _isLeagueVisited = isVisited == "true";
      });
    }
  }

  onAnimation() {
    setState(() {
      animationValue = widget.tabController.animation.value;
    });
  }

  onTabChanged() {
    setState(() {
      _selectedIndex = widget.tabController.index;
    });

    _isLeagueVisited = true;
    SharedPrefHelper().saveToSharedPref(SharedPrefKey.leagueVisited, "true");
  }

  Widget _getTabButton(String title, int _tabIndex) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Stack(
          children: [
            Container(
              height: widget.tabHeight + 3,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(74, 3, 11, 1),
                    Color.fromRGBO(27, 1, 4, 1),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(6.0),
                ),
                border: Border.fromBorderSide(
                  BorderSide(
                    color: Color.fromRGBO(167, 13, 31, 1),
                  ),
                ),
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Opacity(
                    opacity: _selectedIndex.toDouble() == animationValue
                        ? _tabIndex == _selectedIndex ? 1 : 0
                        : _tabIndex - animationValue <= 0
                            ? 1 + _tabIndex - animationValue
                            : 1 - (_tabIndex - animationValue),
                    child: Image.asset(
                      "images/lobby/tab_highlight.png",
                      fit: BoxFit.fill,
                      height: widget.tabHeight + 13.0,
                    ),
                  ),
                ),
              ],
            ),
            InkWell(
              onTap: () {
                if (!widget.tabController.indexIsChanging) {
                  setState(() {
                    _selectedIndex = _tabIndex;
                    widget.tabController.animateTo(_selectedIndex);
                  });
                }
              },
              child: Container(
                padding: EdgeInsets.only(left: 1.0, top: 1.0, right: 1.0),
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(6.0),
                  ),
                ),
                child: Container(
                  height: widget.tabHeight,
                  child: Center(
                    child: Text(
                      title,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'FagoCo',
                                letterSpacing: -0.4,
                              ),
                    ),
                  ),
                ),
              ),
            ),
            if (!_isLeagueVisited && _tabIndex == 1 && _selectedIndex != 1)
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    width: 32.0,
                    height: 32.0,
                    margin: EdgeInsets.only(top: 8.0, right: 8.0),
                    child: FlareActor(
                      "images/flare/ripple.flr",
                      alignment: Alignment.center,
                      animation: "ripple0",
                      fit: BoxFit.fitWidth,
                    ),
                  )
                ],
              ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    widget.tabController.animation.removeListener(onAnimation);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: List.generate(
        widget.tabs.length,
        (index) => _getTabButton(widget.tabs[index], index),
      ),
    );
  }
}
