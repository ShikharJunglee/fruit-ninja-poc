import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LeagueSkeletonCard extends StatelessWidget {
  Widget getTextShimmer({double width = 40.0, double height = 12.0}) {
    return Shimmer.fromColors(
      baseColor: Color.fromRGBO(102, 27, 32, 1),
      highlightColor: Color.fromRGBO(122, 27, 32, 1),
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Color.fromRGBO(102, 27, 32, 1),
          borderRadius: BorderRadius.circular(4.0),
        ),
      ),
    );
  }

  Widget getRibbon({double width = 40.0, double height = 12.0}) {
    return Stack(
      children: [
        Positioned(
          top: 0.5,
          left: -1.0,
          child: Shimmer.fromColors(
            baseColor: Color.fromRGBO(37, 13, 17, 1),
            highlightColor: Color.fromRGBO(37, 13, 17, 1),
            child: Image.asset(
              "images/template/middle_ribbon.png",
              height: 20.0,
            ),
          ),
        ),
        Positioned(
          top: 0.5,
          right: -1.0,
          child: Shimmer.fromColors(
            baseColor: Color.fromRGBO(37, 13, 17, 1),
            highlightColor: Color.fromRGBO(37, 13, 17, 1),
            child: Image.asset(
              "images/template/middle_ribbon.png",
              height: 20.0,
            ),
          ),
        ),
        Shimmer.fromColors(
          baseColor: Color.fromRGBO(102, 27, 32, 1),
          highlightColor: Color.fromRGBO(122, 27, 32, 1),
          child: Image.asset(
            "images/template/middle_ribbon.png",
            height: 20.0,
          ),
        ),
      ],
    );
  }

  Widget getLogoShimmer() {
    return Shimmer.fromColors(
      baseColor: Color.fromRGBO(102, 27, 32, 1),
      highlightColor: Color.fromRGBO(122, 27, 32, 1),
      child: Container(
        width: (86 * 347) / 222,
        height: 86.0,
        decoration: BoxDecoration(
          color: Color.fromRGBO(102, 27, 32, 1),
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      borderOnForeground: false,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          width: 3.0,
          color: Color.fromRGBO(99, 27, 31, 1),
        ),
        borderRadius: BorderRadius.circular(12.0),
      ),
      color: Color.fromRGBO(83, 19, 22, 1),
      margin: EdgeInsets.all(8.0),
      child: Stack(
        alignment: Alignment.topCenter,
        overflow: Overflow.visible,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(6.0),
            child: IntrinsicHeight(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  getLogoShimmer(),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        getTextShimmer(width: 60.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: getTextShimmer(width: 40.0),
                        ),
                        getTextShimmer(width: 80.0, height: 18.0),
                      ],
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      getTextShimmer(width: 96.0, height: 36.0),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: -8.0,
            child: getRibbon(
              width: MediaQuery.of(context).size.width * 0.6,
              height: 18.0,
            ),
          ),
        ],
      ),
    );
  }
}
