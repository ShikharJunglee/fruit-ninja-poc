import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/databasemanager.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/mymatches.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class AsyncH2HCard extends StatefulWidget {
  final AsyncHeadsUp asyncH2H;
  final Function onJoin;
  final Function onClick;

  AsyncH2HCard({this.asyncH2H, @required this.onJoin, @required this.onClick});

  @override
  _AsyncH2HCardState createState() => _AsyncH2HCardState();
}

class _AsyncH2HCardState extends State<AsyncH2HCard> {
  bool isButtonEnabled = true;

  getEntryFeeWidget(
      BuildContext context, double realCurrency, int virtualCurrency) {
    List<Widget> _widgets = [];
    if (virtualCurrency != null && virtualCurrency > 0) {
      _widgets.add(
        Image.asset(
          "images/currency/diamond.png",
          height: 20.0,
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: Text(
            virtualCurrency.toString(),
            style: Theme.of(context).primaryTextTheme.headline5.copyWith(
                  color: Color.fromRGBO(153, 101, 40, 1),
                  fontFamily: 'FagoCo',
                  fontWeight: FontWeight.normal,
                ),
          ),
        ),
      );
    }

    if (virtualCurrency != null &&
        virtualCurrency > 0 &&
        realCurrency != null &&
        realCurrency > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Text(
            "+",
            style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                  color: Color.fromRGBO(153, 101, 40, 1),
                  fontWeight: FontWeight.w900,
                ),
          ),
        ),
      );
    }

    if (realCurrency != null && realCurrency > 0) {
      _widgets.add(
        Row(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 2.0),
              child: Text(
                CurrencyFormat.format(0).replaceAll("0", ""),
                style: Theme.of(context).primaryTextTheme.headline5.copyWith(
                      color: Color.fromRGBO(153, 101, 40, 1),
                      fontFamily: 'FagoCo-bold',
                    ),
              ),
            ),
            Text(
              realCurrency.toStringAsFixed(2).replaceAll(".00", ""),
              style: Theme.of(context).primaryTextTheme.headline5.copyWith(
                    color: Color.fromRGBO(153, 101, 40, 1),
                    fontFamily: 'FagoCo',
                    fontWeight: FontWeight.normal,
                  ),
            ),
          ],
        ),
      );
    }

    if ((realCurrency == null || realCurrency == 0) &&
        (virtualCurrency == null || virtualCurrency == 0)) {
      _widgets.add(
        Text(
          "Free",
          style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                color: Color.fromRGBO(153, 101, 40, 1),
                fontWeight: FontWeight.w900,
              ),
        ),
      );
    }

    return Row(children: _widgets);
  }

  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline2.copyWith(
            fontWeight: FontWeight.w600,
            fontFamily: 'FagoCo-bold',
            color: Colors.white,
          ),
      gradient: LinearGradient(
        colors: [
          Colors.yellow,
          Color.fromRGBO(253, 239, 171, 1),
          Color.fromRGBO(231, 181, 25, 1),
          Colors.red,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  getPrizePoolWidget(BuildContext context, int rewardType, double prizePool) {
    InitData initData = Provider.of(context, listen: false);

    List<Widget> _widgets = [];
    if (rewardType == 2) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(top: 12.0),
          child: Image.asset(
            "images/currency/diamond.png",
            height: 35.0,
          ),
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 4.0, bottom: 3.0),
          child: getGradientText(
            context,
            prizePool.toInt().toString(),
          ),
        ),
      );
    }

    if (rewardType == 1) {
      _widgets.add(
        Padding(
          padding: EdgeInsets.only(bottom: 3),
          child: Row(
            children: [
              getGradientText(
                context,
                CurrencyFormat.format(
                  prizePool,
                  decimalDigits:
                      initData.experiments.miscConfig.amountPrecision,
                  showDelimeters: false,
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Row(children: _widgets);
  }

  getRibbonTextWidget(BuildContext context, String title) {
    if (title.length != 0)
      return Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          Image.asset(
            "images/template/middle_ribbon.png",
            height: 20.0,
          ),
          Container(
            height: Platform.isIOS ? 18.0 : 20.0,
            padding: EdgeInsets.only(left: Platform.isIOS ? 1.0 : 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style:
                        Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                              color: Color.fromRGBO(192, 3, 35, 1),
                              fontWeight: FontWeight.bold,
                            ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    return Container();
  }

  startTimer() {
    new Timer(new Duration(seconds: 2), () {
      setState(() {
        isButtonEnabled = true;
      });
    });
  }

  Widget getSpotsLeftBar() {
    InitData initData = Provider.of(context, listen: false);
    if (initData.experiments.miscConfig.spotsLeft == false) {
      return Container();
    }
    DatabaseManager _databaseManager =
        Provider.of<DatabaseManager>(context, listen: false);

    MyMatches matches = _databaseManager.getMyMatches();

    List<MyMatch> inProgressMatches = matches.getInProgressMatches();
    bool sameContestRunning = false;
    for (int i = 0; i < inProgressMatches.length; i++) {
      if (inProgressMatches[i].templateId == widget.asyncH2H.templateId) {
        sameContestRunning = true;
        break;
      }
    }
    return Padding(
      padding: const EdgeInsets.only(left: 18.0, right: 14),
      child: Column(
        children: [
          LinearPercentIndicator(
            lineHeight: 5.0,
            animation: true,
            animateFromLastPercent: true,
            animationDuration: 700,
            padding: EdgeInsets.all(0.0),
            backgroundColor: Colors.black26,
            linearGradient: LinearGradient(
              colors: [
                Color.fromRGBO(132, 198, 5, 1),
                Color.fromRGBO(70, 165, 13, 1)
              ],
              stops: [0, 0.9],
            ),
            percent: sameContestRunning
                ? 0
                : widget.asyncH2H.joinedCount / widget.asyncH2H.maxPlayers,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 2.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  sameContestRunning
                      ? widget.asyncH2H.maxPlayers.toString() + " spots left"
                      : (widget.asyncH2H.maxPlayers -
                                  widget.asyncH2H.joinedCount ==
                              1
                          ? "1 spot left"
                          : (widget.asyncH2H.maxPlayers -
                                      widget.asyncH2H.joinedCount)
                                  .toString() +
                              " spots left"),
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                        color: Color.fromRGBO(153, 101, 40, 1),
                        fontWeight: FontWeight.w900,
                      ),
                ),
                Text(
                  widget.asyncH2H.maxPlayers.toString() + " spots",
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                        color: Color.fromRGBO(153, 101, 40, 1),
                        fontWeight: FontWeight.w900,
                      ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);
    return Row(
      children: <Widget>[
        Expanded(
          child: Card(
            borderOnForeground: false,
            shape: RoundedRectangleBorder(
              side: BorderSide(
                width: 3.0,
                color: Color.fromRGBO(255, 218, 179, 1),
              ),
              borderRadius: BorderRadius.circular(12.0),
            ),
            margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
            child: InkWell(
              onTap: widget.onClick,
              child: ChangeNotifierProvider<AsyncHeadsUp>.value(
                value: widget.asyncH2H,
                child: Consumer<AsyncHeadsUp>(
                  builder: (context, _asyncH2H, child) {
                    return Tooltip(
                      message: _asyncH2H.h2hTemplateId.toString(),
                      child: Stack(
                        overflow: Overflow.visible,
                        alignment: Alignment.topCenter,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  width: (86 * 347) / 222,
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: <Widget>[
                                      Image.asset(
                                        "images/template/prize.png",
                                        width: (86 * 347) / 222,
                                        height: 90,
                                        fit: BoxFit.fill,
                                      ),
                                      FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: Container(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 8.0),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              getPrizePoolWidget(
                                                context,
                                                _asyncH2H.rewardType,
                                                _asyncH2H.prizePool,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    "PRIZE",
                                                    style: Theme.of(context)
                                                        .primaryTextTheme
                                                        .headline6
                                                        .copyWith(
                                                          fontFamily: 'FagoCo',
                                                          fontWeight:
                                                              FontWeight.w900,
                                                        ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 4.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Expanded(
                                              child: Column(
                                                children: <Widget>[
                                                  Text(
                                                    "Entry Fee",
                                                    style: Theme.of(context)
                                                        .primaryTextTheme
                                                        .subtitle2
                                                        .copyWith(
                                                          color: Color.fromRGBO(
                                                              153, 101, 40, 1),
                                                        ),
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        bottom: !initData
                                                                .experiments
                                                                .miscConfig
                                                                .spotsLeft
                                                            ? 8
                                                            : 16.0),
                                                    child: FittedBox(
                                                      fit: BoxFit.scaleDown,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          getEntryFeeWidget(
                                                              context,
                                                              _asyncH2H
                                                                  .realCurrency,
                                                              _asyncH2H
                                                                  .virtualCurrency),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  initData.experiments
                                                          .miscConfig.spotsLeft
                                                      ? Container()
                                                      : FittedBox(
                                                          fit: BoxFit.scaleDown,
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: Color
                                                                  .fromRGBO(
                                                                      241,
                                                                      229,
                                                                      199,
                                                                      1),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          4.0),
                                                            ),
                                                            padding:
                                                                const EdgeInsets
                                                                        .symmetric(
                                                                    vertical:
                                                                        4.0,
                                                                    horizontal:
                                                                        8.0),
                                                            margin:
                                                                const EdgeInsets
                                                                        .only(
                                                                    bottom:
                                                                        4.0),
                                                            child: Row(
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: <
                                                                  Widget>[
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          4.0),
                                                                  child: Image
                                                                      .asset(
                                                                    "images/icons/players.png",
                                                                    height:
                                                                        16.0,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  "${_asyncH2H.maxPlayers} Players",
                                                                  style: Theme.of(
                                                                          context)
                                                                      .primaryTextTheme
                                                                      .subtitle1
                                                                      .copyWith(
                                                                        color: Color.fromRGBO(
                                                                            153,
                                                                            101,
                                                                            40,
                                                                            1),
                                                                        fontWeight:
                                                                            FontWeight.w900,
                                                                      ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                bottom: 6.0,
                                                right: 10,
                                              ),
                                              child: CupertinoButton(
                                                padding: EdgeInsets.all(0.0),
                                                child: Stack(
                                                  alignment: Alignment.center,
                                                  children: <Widget>[
                                                    Image.asset(
                                                      "images/template/play.png",
                                                      height: 40.0,
                                                    ),
                                                    Text(
                                                      "PLAY",
                                                      style: Theme.of(context)
                                                          .primaryTextTheme
                                                          .headline5
                                                          .copyWith(
                                                            fontWeight:
                                                                FontWeight.w800,
                                                          ),
                                                    ),
                                                  ],
                                                ),
                                                pressedOpacity: 0.7,
                                                onPressed: isButtonEnabled
                                                    ? () {
                                                        startTimer();
                                                        setState(() {
                                                          isButtonEnabled =
                                                              false;
                                                        });
                                                        widget.onJoin();
                                                      }
                                                    : null,
                                              ),
                                            )
                                          ],
                                        ),
                                        getSpotsLeftBar(),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            top: -5,
                            right: 0,
                            left: 0,
                            child: getRibbonTextWidget(
                                context, _asyncH2H.ribbonText),
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
