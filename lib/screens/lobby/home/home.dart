import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/api/banner/banner.dart';
import 'package:solitaire_gold/api/banner/banner_zone.dart';
import 'package:solitaire_gold/api/lobby/lobbyAPI.dart';
import 'package:solitaire_gold/commonwidgets/carousel.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/banners/banners.dart';
import 'package:solitaire_gold/models/databasemanager.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/lobby_data.dart';
import 'package:solitaire_gold/models/lobby/mymatches.dart';
import 'package:solitaire_gold/models/lobby/prizestructure_details.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/lobby/home/async_h2h_card.dart';
import 'package:solitaire_gold/screens/lobby/home/home_tab.dart';
import 'package:solitaire_gold/screens/lobby/home/joincontest.dart';
import 'package:solitaire_gold/screens/lobby/home/leagueskeletoncard.dart';
import 'package:solitaire_gold/screens/lobby/home/small_league_card.dart';
import 'package:solitaire_gold/screens/prizestructure/prizestructure.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/utils/deeplink_cta.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/websocket/request.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class Home extends StatefulWidget {
  final Function(Map<String, dynamic>) onBannerClick;

  Home({this.onBannerClick});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  int lastLaunchTime = 0;
  TabController _tabController;

  LobbyAPI _lobbyAPI = LobbyAPI();
  BannerAPI _bannerAPI = BannerAPI();

  List<String> _tabs = ["HEAD TO HEAD", "LEAGUES"];

  // int _selectedIndex = 0;
  List<String> bannerIds = [];

  double animationValue = 0.0;
  final double tabHeight = 40.0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (mounted) requestBanners(context);
    });
    super.initState();
  }

  void requestBanners(BuildContext context) async {
    BannerList carousel = Provider.of<BannerList>(context, listen: false);

    Map<String, dynamic> response =
        await _bannerAPI.getBanners(context, zone: BannerZone.LOBBY_TOP);
    try {
      if (response["error"] == false) {
        carousel.setItems(BannerZone.LOBBY_TOP, response["data"]);
        response["data"].forEach((element) {
          bannerIds.add(element["id"]);
        });
        LobbyAnalytics().onLobbyLoaded(
          context,
          source: "l0",
          selectedTab: _tabController.index,
          banners: bannerIds,
        );
      }
    } catch (e) {}
  }

  void onTabChange(BuildContext context) {
    LobbyAnalytics().onLobbyTabSwitch(
      context,
      source: "l0",
      selectedTab: _tabController.index,
      banners: bannerIds,
    );
    // setState(() {
    //   _selectedIndex = _tabController.index;
    // });
  }

  void createTabControllerIfNotExist(List<String> _tabs) {
    if (_tabController == null) {
      _tabController = TabController(
        vsync: this,
        length: _tabs.length,
        initialIndex: 0,
      );
      _tabController.addListener(() {
        if (!_tabController.indexIsChanging) this.onTabChange(context);
      });
    }
  }

  List<AsyncHeadsUp> filterH2h(List<AsyncHeadsUp> leagues) {
    List<AsyncHeadsUp> asyncH2H = [];
    DatabaseManager _databaseManager =
        Provider.of<DatabaseManager>(context, listen: false);

    MyMatches matches = _databaseManager.getMyMatches();
    List<MyMatch> inProgressMatches = matches.getAllMatches();

    for (int j = 0; j < leagues.length; j++) {
      bool sameContestRunning = false;
      for (int i = 0; i < inProgressMatches.length; i++) {
        if (inProgressMatches[i].templateId == leagues[j].templateId &&
            leagues[j].noOfClones <= 1) {
          sameContestRunning = true;
          break;
        }
      }
      if (!sameContestRunning) {
        asyncH2H.add(leagues[j]);
      }
    }
    return asyncH2H;
  }

  List<SmallLeague> filterSmallLeagues(List<SmallLeague> leagues) {
    List<SmallLeague> asyncH2H = [];
    DatabaseManager _databaseManager =
        Provider.of<DatabaseManager>(context, listen: false);

    MyMatches matches = _databaseManager.getMyMatches();
    List<MyMatch> inProgressMatches = matches.getAllMatches();

    for (int j = 0; j < leagues.length; j++) {
      bool sameContestRunning = false;
      for (int i = 0; i < inProgressMatches.length; i++) {
        if (inProgressMatches[i].templateId == leagues[j].templateId &&
            leagues[j].noOfClones <= 1) {
          sameContestRunning = true;
          break;
        }
      }
      if (!sameContestRunning) {
        asyncH2H.add(leagues[j]);
      }
    }
    return asyncH2H;
  }

  ListView _getH2HCards(List<AsyncHeadsUp> _asyncH2H) {
    // List<AsyncHeadsUp> _asyncH2H = filterH2h(asyncH2H);

    return _asyncH2H == null
        ? ListView.builder(
            itemCount: 5,
            itemBuilder: (context, index) => index == 0
                ? SizedBox(
                    height: 8.0,
                  )
                : LeagueSkeletonCard(),
          )
        : ListView.builder(
            itemCount: _asyncH2H.length,
            itemBuilder: (context, index) {
              return AsyncH2HCard(
                asyncH2H: _asyncH2H[index],
                onJoin: () async {
                  onH2HJoin(context, _asyncH2H[index]);
                },
                onClick: () {},
              );
            },
          );
  }

  ListView _getTournamentCards(List<SmallLeague> _smallLeagues) {
    //List<SmallLeague> _smallLeagues = filterSmallLeagues(smallLeagues);

    return _smallLeagues == null
        ? ListView.builder(
            itemCount: 4,
            itemBuilder: (context, index) => LeagueSkeletonCard(),
          )
        : ListView.builder(
            itemCount: _smallLeagues.length,
            itemBuilder: (context, index) {
              return SmallLeagueCard(
                smallLeague: _smallLeagues[index],
                onJoin: () async {
                  onSmallLeagueJoin(context, _smallLeagues[index], "l0");
                },
                onClick: () async {
                  Loader().showLoader(true, immediate: true);
                  await _showPrizeStructure(_smallLeagues[index]);
                },
              );
            },
          );
  }

  Future<dynamic> _showPrizeStructure(SmallLeague _league) async {
    if (lastLaunchTime + 1000 <= DateTime.now().millisecondsSinceEpoch) {
      lastLaunchTime = DateTime.now().millisecondsSinceEpoch;

      if (!WebSocket().isConnected()) {
        return showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          barrierDismissible: true,
          builder: (disconnContext) {
            return Disconnection();
          },
        );
      }

      Loader().showLoader(true, immediate: true);
      LobbyAnalytics().onContestCardClicked(
        context,
        source: "l0",
        banners: bannerIds,
        smallLeague: _league,
        selectedTab: 1,
      );
      Map<String, dynamic> _response = await _lobbyAPI.getPrizeStructure(
          context,
          prizestructureId: _league.prizeStructureId);
      PrizeStrucutreDetails _prizestructureDetails =
          PrizeStrucutreDetails.fromJson(_response["data"]);

      Loader().showLoader(false);
      Map<String, dynamic> analyticData = {
        "tid": _league.templateId,
        "cid": _league.tournamentId,
        "bannerIds": bannerIds,
      };
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (prizestructurecontext) {
          return PrizeStructure(
            prizeStructureId: _league.prizeStructureId,
            prizestructureDetails: _prizestructureDetails,
            onJoin: () {
              Navigator.of(prizestructurecontext).pop();
              onSmallLeagueJoin(context, _league, "prize_break_up");
            },
            maxPlayers: _league.maxPlayers,
            analyticData: analyticData,
            source: "l0",
          );
        },
      );
    }
  }

  onSmallLeagueJoin(
      BuildContext context, SmallLeague _smallLeague, String source) async {
    if (lastLaunchTime + 1000 <= DateTime.now().millisecondsSinceEpoch) {
      lastLaunchTime = DateTime.now().millisecondsSinceEpoch;
      if (!WebSocket().isConnected()) {
        return showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          barrierDismissible: true,
          builder: (disconnContext) {
            return Disconnection();
          },
        );
      }
      AnalyticsManager().setJourney("JoinContest");
      LobbyAnalytics().onPlayClicked(
        context,
        source: source,
        banners: bannerIds,
        smallLeague: _smallLeague,
        selectedTab: 1,
      );

      Loader().showLoader(true, immediate: true);
      int allowJoin = await JoinContest().requestJoin(
        context,
        bonusAllowed: _smallLeague.bounsPercentage,
        entryFee: _smallLeague.realCurrency,
        diamonds: _smallLeague.virtualCurrency,
        league: _smallLeague,
        banners: bannerIds,
        source: source,
      );
      Loader().showLoader(false);

      switch (allowJoin) {
        case 0:
          launchSolitaire(context, smallLeague: _smallLeague, source: source);
          break;
        case 2:
          await DeeplinkLaunch().onAddCash(context,
              source: "insuffiecient_balance", onPaymentComplete: (response) {
            print(response);
            //onH2HJoin(context, _asyncH2H);
          });
      }
    }
  }

  launchSolitaire(
    BuildContext context, {
    SmallLeague smallLeague,
    AsyncHeadsUp asyncHeadsUp,
    String source = "l0",
  }) {
    try {
      EventManager().closePushNotifFlash();
    } catch (e) {}
    Future.delayed(Duration(seconds: 5), () {
      MyMatchesRequest myMatchedRequest = MyMatchesRequest();
      WebSocket().sendMessage(myMatchedRequest.toJson());
    });

    if (smallLeague != null) {
      LobbyAnalytics().onJoinContest(
        context,
        source: source,
        banners: bannerIds,
        selectedTab: 1,
        smallLeague: smallLeague,
      );

      routeManager.launchSolitaire(
        context,
        league: smallLeague,
        selectedLobbyTabIndex: _tabController.index,
      );
    } else if (asyncHeadsUp != null) {
      LobbyAnalytics().onJoinContest(
        context,
        source: "l0",
        selectedTab: 0,
        banners: bannerIds,
        headsUp: asyncHeadsUp,
      );

      routeManager.launchSolitaire(
        context,
        asyncHeadsUp: asyncHeadsUp,
        selectedLobbyTabIndex: _tabController.index,
      );
    }
  }

  onH2HJoin(BuildContext context, AsyncHeadsUp _asyncH2H) async {
    if (lastLaunchTime + 1000 <= DateTime.now().millisecondsSinceEpoch) {
      lastLaunchTime = DateTime.now().millisecondsSinceEpoch;
      if (!WebSocket().isConnected()) {
        return showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          barrierDismissible: true,
          builder: (disconnContext) {
            return Disconnection();
          },
        );
      }
      AnalyticsManager().setJourney("JoinContest");
      Loader().showLoader(true, immediate: true);
      LobbyAnalytics().onPlayClicked(
        context,
        selectedTab: 0,
        source: "l0",
        banners: bannerIds,
        headsUp: _asyncH2H,
      );
      int allowJoin = await JoinContest().requestJoin(
        context,
        bonusAllowed: _asyncH2H.bounsPercentage,
        entryFee: _asyncH2H.realCurrency,
        diamonds: _asyncH2H.virtualCurrency,
        headsUp: _asyncH2H,
        banners: bannerIds,
        source: "l0",
      );

      Loader().showLoader(false);

      switch (allowJoin) {
        case 0:
          launchSolitaire(context, asyncHeadsUp: _asyncH2H);
          break;
        case 2:
          await DeeplinkLaunch().onAddCash(context,
              source: "insuffiecient_balance", onPaymentComplete: (response) {
            print(response);
            //onH2HJoin(context, _asyncH2H);
          });
      }
    }
  }

  updateUserDetails() async {
    AddCashAPI _addCashAPI = AddCashAPI();
    User user = Provider.of(context, listen: false);
    final result = await _addCashAPI.getUserDetails(context);
    if (!result["error"]) {
      user.copyFrom(result["data"]);
    }
  }

  @override
  Widget build(BuildContext context) {
    createTabControllerIfNotExist(_tabs);
    DatabaseManager _databaseManager =
        Provider.of<DatabaseManager>(context, listen: true);

    LobbyData _lobby = _databaseManager.getLobbyData();

    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        children: <Widget>[
          Consumer<BannerList>(
            builder: (context, carousels, child) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Carousel(
                  showDots: true,
                  showPlaceholder: true,
                  carousel: carousels.getBannersFor(BannerZone.LOBBY_TOP),
                  onClick: widget.onBannerClick,
                ),
              );
            },
          ),
          Expanded(
            child: Stack(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: tabHeight + 7),
                  child: Column(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 4.0, right: 4.0, bottom: 4.0),
                          child: Container(
                            padding: EdgeInsets.only(top: 12),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12.0),
                              border: Border.all(
                                color: Color.fromRGBO(167, 13, 31, 1),
                                width: 1.0,
                              ),
                              gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(74, 3, 11, 1),
                                  Color.fromRGBO(27, 1, 4, 1),
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                              ),
                            ),
                            child: ChangeNotifierProvider<LobbyData>.value(
                              value: _lobby,
                              child: Consumer<LobbyData>(
                                builder: (context, _lobby, child) {
                                  return _lobby == null
                                      ? Container()
                                      : TabBarView(
                                          controller: _tabController,
                                          children: [
                                            _lobby.asyncH2H != null &&
                                                    _lobby.asyncH2H.length == 0
                                                ? Center(
                                                    child: GradientText(
                                                      "No Games Available!",
                                                      style: Theme.of(context)
                                                          .primaryTextTheme
                                                          .headline6
                                                          .copyWith(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 28,
                                                            fontFamily:
                                                                'FagoCo',
                                                          ),
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Color.fromRGBO(249,
                                                                247, 237, 1),
                                                            Color.fromRGBO(235,
                                                                235, 207, 1)
                                                          ]),
                                                    ),
                                                  )
                                                : _getH2HCards(_lobby.asyncH2H),
                                            _lobby.smallLeagues != null &&
                                                    _lobby.smallLeagues
                                                            .length ==
                                                        0
                                                ? Center(
                                                    child: GradientText(
                                                      "No Games Available!",
                                                      style: Theme.of(context)
                                                          .primaryTextTheme
                                                          .headline6
                                                          .copyWith(
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 28,
                                                            fontFamily:
                                                                'FagoCo',
                                                          ),
                                                      gradient: LinearGradient(
                                                          colors: [
                                                            Color.fromRGBO(249,
                                                                247, 237, 1),
                                                            Color.fromRGBO(235,
                                                                235, 207, 1)
                                                          ]),
                                                    ),
                                                  )
                                                : _getTournamentCards(
                                                    _lobby.smallLeagues),
                                          ],
                                        );
                                },
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: HomeTabBar(
                    tabController: _tabController,
                    tabs: _tabs,
                    tabHeight: tabHeight + 5,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
