import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';

import 'package:solitaire_gold/api/geolocation/locationAPI.dart';
import 'package:solitaire_gold/api/lobby/lobbyAPI.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/join_contest_response.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/popups/common/generic_dialog.dart';
import 'package:solitaire_gold/popups/mobile_verification_popup.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/screens/withdraw/statedobdialog.dart';
import 'package:solitaire_gold/utils/loader.dart';

class JoinContest {
  LobbyAPI _lobbyAPI = LobbyAPI();

  JoinContest._internal();
  factory JoinContest() => joinContest;
  static final JoinContest joinContest = JoinContest._internal();

  requestJoin(
    BuildContext context, {
    @required double bonusAllowed,
    @required double entryFee,
    @required int diamonds,
    @required String source,
    SmallLeague league,
    AsyncHeadsUp headsUp,
    List<String> banners,
  }) async {
    LocationAPI _locationAPI = LocationAPI();
    Map<String, dynamic> locationResult;
    bool isCashGame = (league != null && league.rewardType == 1) ||
        (headsUp != null && headsUp.rewardType == 1) ||
        entryFee > 0;

    if (isCashGame) {
      Loader().showLoader(true, immediate: true);
      locationResult = await _locationAPI.getUserLocation(
        context,
        isJoinContest: true,
      );
    }

    if (locationResult != null && locationResult["isError"]) {
      if (locationResult["isBlockedState"] != null &&
          locationResult["isBlockedState"]) {
        Loader().showLoader(false);
        await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) => GenericDialog(
            contentText: Text(
              "Cash transactions are restricted in your State. Reach out to our support for further assistance.",
              textAlign: TextAlign.center,
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.white,
                  ),
            ),
            buttonText: "Ok".toUpperCase(),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        );
      }
      return 1;
    } else {
      Map<String, dynamic> _response = await _lobbyAPI.joinContest(
        context,
        bonusAllowed: bonusAllowed,
        entryFee: entryFee,
        diamonds: diamonds,
        locationData: locationResult,
      );
      Loader().showLoader(false);

      if (_response["error"] == true) {
        if (_response["isSocketError"] == true) {
          await showDialog(
            barrierColor: Color.fromARGB(175, 0, 0, 0),
            context: context,
            barrierDismissible: false,
            builder: (context) => Disconnection(),
          );
        } else {
          if (_response["message"] != null) {
            await showDialog(
              barrierColor: Color.fromARGB(175, 0, 0, 0),
              context: context,
              barrierDismissible: false,
              builder: (context) => GenericDialog(
                contentText: Text(
                  _response["message"],
                  textAlign: TextAlign.center,
                  style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                        color: Colors.white,
                      ),
                ),
                buttonText: "ok".toUpperCase(),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
          }
        }
      } else {
        JoinContestResponse _joinContestRespons =
            JoinContestResponse.fromJson(_response["data"]["joinContestData"]);
        LobbyAnalytics().onPlayResponse(
          context,
          source: source,
          headsUp: headsUp,
          smallLeague: league,
          error: _joinContestRespons.error,
          banners: banners,
        );
        if (_joinContestRespons.errorDetails.isBlockedUser && isCashGame) {
          await showDialog(
              barrierColor: Color.fromARGB(175, 0, 0, 0),
              context: context,
              builder: (context) {
                return GenericDialog(
                  contentText: Text(
                    _joinContestRespons.errorDetails.errorMessage,
                    textAlign: TextAlign.center,
                    style:
                        Theme.of(context).primaryTextTheme.headline4.copyWith(
                              color: Colors.white,
                            ),
                  ),
                  buttonText: "ok".toUpperCase(),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                );
              });
          return 1;
        } else if (_joinContestRespons.error.indexOf(101) != -1) {
          var result = await showDialog(
            barrierColor: Color.fromARGB(175, 0, 0, 0),
            context: context,
            builder: (context) => GenericDialog(
              title: "images/titles/insufficient-balance.png",
              buttonText: "Deposit".toUpperCase(),
              contentText: Text(
                "You don't have sufficient Cash balance to join this contest.",
                textAlign: TextAlign.center,
                style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                      color: Colors.white,
                    ),
              ),
              onPressed: () {
                Navigator.pop(context, {"action": "deposit"});
              },
            ),
          );
          if (result != null && result["action"] == "deposit") return 2;
          return -1;
        } else if (_joinContestRespons.error.indexOf(102) != -1) {
          await showDialog(
              barrierColor: Color.fromARGB(175, 0, 0, 0),
              context: context,
              builder: (context) => GenericDialog(
                    title: "images/titles/insufficient-balance.png",
                    buttonText: "ok".toUpperCase(),
                    contentText: Text(
                      "You don't have sufficient Gems balance to join this contest.",
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).primaryTextTheme.headline4.copyWith(
                                color: Colors.white,
                              ),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ));
          return 3;
        } else if (_joinContestRespons.error.indexOf(3) != -1 && isCashGame) {
          final result = await showDialog(
            barrierColor: Color.fromARGB(175, 0, 0, 0),
            context: context,
            builder: (BuildContext context) {
              return StateDobDialog(
                source: "join_contest",
              );
            },
          );
          if (result != null && result["success"]) {
            return await requestJoin(
              context,
              bonusAllowed: bonusAllowed,
              entryFee: entryFee,
              diamonds: diamonds,
              headsUp: headsUp,
              league: league,
              source: source,
            );
          } else if (result != null && !result["success"]) {
            return 4;
          }
        } else if (_joinContestRespons.error.indexOf(103) != -1 && isCashGame) {
          final result = await showDialog(
            barrierColor: Color.fromARGB(175, 0, 0, 0),
            context: context,
            builder: (BuildContext context) {
              return MobileVerificationPopup();
            },
          );
          if (result != null && result["success"] == true) {
            return await requestJoin(
              context,
              bonusAllowed: bonusAllowed,
              entryFee: entryFee,
              diamonds: diamonds,
              headsUp: headsUp,
              league: league,
              source: source,
            );
          } else if (result != null && !result["success"]) {
            return 5;
          }
        } else {
          return Future.value(0);
        }
      }
    }
  }
}
