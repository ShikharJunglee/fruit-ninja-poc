import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/utils/deeplink_cta.dart';
import 'package:solitaire_gold/utils/loader.dart';

class WelcomePopup extends StatelessWidget {
  final Map<String, dynamic> popupData;

  WelcomePopup({this.popupData});

  bannerAction(BuildContext context) async {
    LobbyAnalytics().onLoginPopupClicked(context, source: "l0");
    Loader().showLoader(true);
    switch (popupData["CTA"]) {
      case DeeplinkCTA.PROFILE:
        await DeeplinkLaunch().onProfile(context);
        break;
      case DeeplinkCTA.KYC:
        await DeeplinkLaunch().onKYC(context);
        break;
      case DeeplinkCTA.ACCOUNT:
        await DeeplinkLaunch().onAccountSummary(context);
        break;
      case DeeplinkCTA.WITHDRAW:
        await DeeplinkLaunch().onWithdraw(context,source: "welcome_popup");
        break;
      case DeeplinkCTA.RAF:
        await DeeplinkLaunch().onRAF(context);
        break;
      case DeeplinkCTA.HOW_TO_PLAY:
        break;
      case DeeplinkCTA.SCORING:
        await DeeplinkLaunch().onScoring(context);
        break;
      case DeeplinkCTA.HELP:
        await routeManager.launchHelpCenter(context);
        break;
    }
    Navigator.pop(context);
    Loader().showLoader(false);
  }

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
        dialog: Dialog(
          backgroundColor: Colors.transparent,
          child: Stack(
            alignment: Alignment.topRight,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.green,
                  ),
                  clipBehavior: Clip.hardEdge,
                  child: InkWell(
                    child: Image.network(popupData["url"]),
                    onTap: () {
                      bannerAction(context);
                    },
                  ),
                ),
              ),
              IconButton(
                padding: EdgeInsets.all(0.0),
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                icon: Image.asset(
                  "images/icons/close2.png",
                  height: 32.0,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
        padding: EdgeInsets.all(8));
  }
}
