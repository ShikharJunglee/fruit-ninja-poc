import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/banner/banner_zone.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/banners/banners.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/utils/deeplink_cta.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class Deals extends StatefulWidget {
  final Function(Map<String, dynamic>) onBannerClick;

  Deals({this.onBannerClick});

  @override
  _DealsState createState() => _DealsState();
}

class _DealsState extends State<Deals> {
  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline4.copyWith(
        fontWeight: FontWeight.w900,
        color: Colors.white,
        shadows: [
          Shadow(
            blurRadius: 0.0,
            color: Colors.grey.shade700,
            offset: Offset(0.0, 3.0),
          ),
        ],
      ),
      gradient: LinearGradient(
        colors: [
          Colors.yellow,
          Color.fromRGBO(253, 239, 171, 1),
          Color.fromRGBO(231, 181, 25, 1),
          Colors.red,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  void _onBannerClick(dynamic banner) async {
    Loader().showLoader(true, immediate: true);
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }

    switch (banner["CTA"]) {
      case DeeplinkCTA.PROFILE:
        DeeplinkLaunch().onProfile(context);
        break;
      case DeeplinkCTA.KYC:
        DeeplinkLaunch().onKYC(context);
        break;
      case DeeplinkCTA.ACCOUNT:
        DeeplinkLaunch().onAccountSummary(context);
        break;
      case DeeplinkCTA.WITHDRAW:
        DeeplinkLaunch().onWithdraw(context,source: "deals");
        break;
      case DeeplinkCTA.RAF:
        DeeplinkLaunch().onRAF(context);
        break;
      case DeeplinkCTA.HOW_TO_PLAY:
        DeeplinkLaunch().onFTUE(context);
        break;
      case DeeplinkCTA.SCORING:
        DeeplinkLaunch().onScoring(context);
        break;
      case DeeplinkCTA.HELP:
        routeManager.launchHelpCenter(context);
        break;
      default:
        if (widget.onBannerClick != null) {
          widget.onBannerClick(banner);
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<BannerList>(
      builder: (context, carousels, child) {
        List<dynamic> banners = carousels.getBannersFor(BannerZone.DEALS);
        if (banners.length != 0) {
          return ListView.builder(
            itemCount: banners.length,
            itemBuilder: (context, index) => Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      children: <Widget>[
                        getGradientText(
                            context, banners[index]["analyticEvent"]),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _onBannerClick(banners[index]);
                    },
                    borderRadius: BorderRadius.circular(4.0),
                    child: CachedNetworkImage(
                      imageUrl: banners[index]["banner"],
                    ),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Center(
            child: GradientText(
              "No Promotions Available!",
              style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'FagoCo',
                    fontSize: 28,
                  ),
              gradient: LinearGradient(colors: [
                Color.fromRGBO(249, 247, 237, 1),
                Color.fromRGBO(235, 235, 207, 1)
              ]),
            ),
          );
        }
      },
    );
  }
}
