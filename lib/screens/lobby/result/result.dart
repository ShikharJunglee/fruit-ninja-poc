import 'dart:async';
import 'dart:io';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/api/lobby/lobbyAPI.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/databasemanager.dart';
import 'package:solitaire_gold/models/lobby/leaderboard.dart';
import 'package:solitaire_gold/models/lobby/mymatches.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/lobby/result/leaderboard.dart';
import 'package:solitaire_gold/screens/lobby/result/resultcard.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:solitaire_gold/utils/websocket/request.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class Result extends StatefulWidget {
  final int matchId;
  final bool showLeaderBoardOnLaunch;

  Result({
    this.showLeaderBoardOnLaunch,
    this.matchId,
  });
  @override
  _ResultState createState() => _ResultState();
}

class _ResultState extends State<Result> with BasePage {
  int lastLaunchTime = 0;
  bool showMoreCurrentVisible = false;
  bool showMoreCompletedVisible = false;
  bool showMoreCurrentClicked = false;
  bool showMoreCompletedClicked = false;

  StreamSubscription _solitaireSubscription;
  StreamSubscription _mymatchesSubscription;
  StreamSubscription<dynamic> leaderboardSubscription;

  Flushbar flushbar;
  Timer matchPollTimer;
  Completer<bool> _myMatchesFetcher = Completer();

  void initState() {
    super.initState();
    leaderboardSubscription = EventManager()
        .subscribe(EventName.LEADERBOARD_LOADED, showLeaderboardDialog);
    _solitaireSubscription =
        EventManager().subscribe(EventName.SOLITAIRE_LOADING, closeFlushbar);
    _mymatchesSubscription =
        EventManager().subscribe(EventName.MYMATCH_ADDED, (_) {
      if (!_myMatchesFetcher.isCompleted) _myMatchesFetcher.complete(true);
    });

    LobbyAnalytics().onLobbyResultsLoaded(context, source: "footer");
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  setData(BuildContext context) async {
    fetchMyMatchesData();

    await _myMatchesFetcher.future;
    await showLeaderBoardViaDeepLinking(context);
  }

  closeFlushbar(_) {
    if (flushbar != null) flushbar.dismiss();
    flushbar = null;
  }

  fetchMyMatchesData() {
    matchPollTimer = Timer(Duration(seconds: 10), () {
      MyMatchesRequest myMatchedRequest = MyMatchesRequest();
      WebSocket().sendMessage(myMatchedRequest.toJson());

      fetchMyMatchesData();
    });
  }

  handleFCMMessage(dynamic fcmData) async {
    try {
      if (fcmData != null) {
        Map<dynamic, dynamic> fcmMapData = fcmData;
        String title = fcmMapData["title"] != null ? fcmMapData["title"] : "";
        String matchId =
            fcmMapData["matchId"] != null ? fcmMapData["matchId"] : "";
        int matchIdInt =
            (matchId != null && matchId.isNotEmpty) ? int.parse(matchId) : 0;
        String type = fcmMapData["type"] != null ? fcmMapData["type"] : "";
        String body = fcmMapData["body"] != null ? fcmMapData["body"] : "";
        if (title != null &&
            matchIdInt != null &&
            type != null &&
            body != null) {
          showFCMMessageToast(title, body, matchIdInt, type);
        }
      }
    } catch (e) {
      print(e);
    }
  }

  showFCMMessageToast(
      String title, String body, int matchIdInt, String type) async {
    flushbar = Flushbar(
      titleText: Text(
        title,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
            color: Colors.black,
            fontFamily: "ShadowsIntoLightTwo"),
      ),
      messageText: Text(
        body,
        style: TextStyle(
            fontSize: 18.0,
            color: Colors.black,
            fontFamily: "ShadowsIntoLightTwo"),
      ),
      mainButton: FlatButton(
        onPressed: () {
          if (flushbar != null) {
            closeFlushbar(null);
          }
        },
        child: Text(
          "Check Now",
          style: TextStyle(color: Colors.green),
        ),
      ),
      duration: Duration(seconds: 8),
      isDismissible: true,
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      backgroundColor: Colors.grey.shade200,
      margin: EdgeInsets.all(8),
      borderRadius: 8,
    );
    await flushbar.show(context);
    flushbar = null;
  }

  showLeaderBoardViaDeepLinking(BuildContext context) async {
    try {
      AppConfig config = Provider.of(context, listen: false);
      MyMatch currentMatch;

      DatabaseManager _databaseManager =
          Provider.of<DatabaseManager>(context, listen: false);
      MyMatches matches = _databaseManager.getMyMatches();
      matches.getAllMatches().forEach((myMatch) {
        if (myMatch.matchId == widget.matchId) {
          currentMatch = myMatch;
        }
      });

      if (currentMatch != null &&
          widget.showLeaderBoardOnLaunch != null &&
          widget.showLeaderBoardOnLaunch &&
          widget.matchId != null &&
          config.bShowLeaderBoardOnResultPageOnStart) {
        showLeaderBoard(currentMatch);
        config.setbShowLeaderBoardOnResultPageOnStart(false);
      }
    } catch (e) {
      print(e);
    }
  }

  showLeaderBoard(MyMatch match) async {
    if (lastLaunchTime + 1000 <= DateTime.now().millisecondsSinceEpoch) {
      Loader().showLoader(true, immediate: true);
      lastLaunchTime = DateTime.now().millisecondsSinceEpoch;
      try {
        dynamic _response = await LobbyAPI()
            .getLeaderBoardDataWithMatchId(context, match.matchId);
        if (_response != null &&
            !_response["error"] &&
            _response["data"] != null) {
          showLeaderboardDialog(_response["data"], myMatch: match);
        }
      } catch (e) {}
    }
  }

  getLeaderboardData(int matchId, int prizeStructureId) {
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }

    LeaderboardRequest leaderboardRequest =
        LeaderboardRequest(matchId, pId: prizeStructureId);
    WebSocket().sendMessage(leaderboardRequest.toJson());
  }

  void showLeaderboardDialog(dynamic data, {@required MyMatch myMatch}) async {
    Loader().showLoader(false);
    Navigator.of(context).popUntil((r) => r.isFirst);
    Leaderboard leaderboardData = Leaderboard.fromJson(data);
    Map<String, dynamic> result = await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      useRootNavigator: true,
      barrierDismissible: true,
      builder: (context) {
        return LeaderboardDetails(
          leaderboardData: leaderboardData,
          showDelayedReplay: false,
          source: "my_matches",
          analyticsData: null,
        );
      },
    );

    if (result != null && result["replay"] == true) {
      showReplay(myMatch);
    }
    if (result != null && result["tieBreaker"] == true) {
      sendTieBreakerMessageUnity(leaderboardData);
    }
  }

  sendTieBreakerMessageUnity(Leaderboard leaderboardData) async {
    leaderboardSubscription.cancel();

    await routeManager.launchSolitaire(
      context,
      leaderboard: leaderboardData,
      tieMatchId: leaderboardData.matchId,
      formatType: leaderboardData.formatType,
    );
    leaderboardSubscription = EventManager()
        .subscribe(EventName.LEADERBOARD_LOADED, showLeaderboardDialog);
  }

  Future<void> showReplay(MyMatch match) async {
    routeManager.launchSolitaireReplay(
      context,
      match: match,
    );
  }

  ListView getMatches(
      List<MyMatch> inProgressMatches, List<MyMatch> completedMatches) {
    completedMatches.removeWhere(
        (match) => (match.formatType == -101 || match.formatType == -201));

    inProgressMatches.removeWhere(
        (match) => (match.formatType == -100 || match.formatType == -200));

    if (completedMatches.length > 3) {
      if (!showMoreCompletedClicked) {
        showMoreCompletedVisible = true;
        completedMatches = completedMatches.sublist(0, 3);
      } else {
        showMoreCompletedVisible = false;
      }
    } else {
      showMoreCompletedVisible = false;
    }

    if (inProgressMatches.length > 3) {
      if (!showMoreCurrentClicked) {
        showMoreCurrentVisible = true;
        inProgressMatches = inProgressMatches.sublist(0, 3);
      } else {
        showMoreCurrentVisible = false;
      }
    } else {
      showMoreCurrentVisible = false;
    }

    if (inProgressMatches.length > 0) {
      inProgressMatches.insert(0, MyMatch(formatType: -100));
      if (showMoreCurrentVisible) {
        inProgressMatches.add(MyMatch(formatType: -200));
      }
    }
    if (completedMatches.length > 0) {
      completedMatches.insert(0, MyMatch(formatType: -101));
      if (showMoreCompletedVisible) {
        completedMatches.add(MyMatch(formatType: -201));
      }
    }

    List<MyMatch> matches = [...inProgressMatches, ...completedMatches];

    return ListView.builder(
      itemCount: matches.length,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.only(top: index == 0 ? 8.0 : 0.0),
          child: ResultCard(
            onTap:
                [-100, -101, -200, -201].contains(matches[index].formatType) ||
                        matches[index].status == MatchStatus.REFUND
                    ? null
                    : () {
                        showLeaderBoard(matches[index]);
                      },
            match: matches[index],
            showAllCompleted: () {
              setState(() {
                showMoreCompletedClicked = true;
                completedMatches
                    .removeWhere((element) => element.formatType == -201);
              });
            },
            showAllCurrent: () {
              setState(() {
                showMoreCurrentClicked = true;
                inProgressMatches
                    .removeWhere((element) => element.formatType == -200);
              });
            },
          ),
        );
      },
    );
  }

  @override
  void dispose() async {
    if (matchPollTimer != null) matchPollTimer.cancel();
    _solitaireSubscription.cancel();
    _mymatchesSubscription.cancel();
    leaderboardSubscription.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DatabaseManager _databaseManager =
        Provider.of<DatabaseManager>(context, listen: false);
    MyMatches matches = _databaseManager.getMyMatches();
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: ChangeNotifierProvider<MyMatches>.value(
        value: matches,
        child: Consumer<MyMatches>(
          builder: (context, matches, child) {
            List<MyMatch> currentMatches = [...matches.getInProgressMatches()];
            List<MyMatch> completedMatches = [...matches.getCompletedMatches()];
            List<MyMatch> allMatches = [...matches.getAllMatches()];

            return !matches.loaded
                ? Container(
                    child: Center(
                      child: GradientText(
                        "Loading...",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Colors.white,
                              fontFamily: 'FagoCo',
                              fontWeight: FontWeight.bold,
                              fontSize: 28,
                            ),
                        gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(249, 247, 237, 1),
                              Color.fromRGBO(235, 235, 207, 1)
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                      ),
                    ),
                  )
                : (matches == null || allMatches.length == 0
                    ? Container(
                        child: Center(
                          child: GradientText(
                            "No Results!",
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Colors.white,
                                  fontFamily: 'FagoCo',
                                  fontWeight: FontWeight.bold,
                                  fontSize: 28,
                                ),
                            gradient: LinearGradient(
                                colors: [
                                  Color.fromRGBO(249, 247, 237, 1),
                                  Color.fromRGBO(235, 235, 207, 1)
                                ],
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter),
                          ),
                        ),
                      )
                    : getMatches(currentMatches, completedMatches));
          },
        ),
      ),
    );
  }
}
