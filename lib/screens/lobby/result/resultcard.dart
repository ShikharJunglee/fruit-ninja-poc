import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/lobby/mymatches.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class ResultCard extends StatefulWidget {
  final MyMatch match;
  final VoidCallback showAllCompleted;
  final VoidCallback showAllCurrent;
  final Function onTap;

  ResultCard({
    @required this.onTap,
    @required this.match,
    @required this.showAllCompleted,
    @required this.showAllCurrent,
  });

  @override
  _ResultCardState createState() => _ResultCardState();
}

class _ResultCardState extends State<ResultCard> {
  Timer _timer;
  int elapsedDays = 0;
  int elapsedHours = 0;
  int elapsedMinutes = 0;
  int elapsedSeconds = 0;

  bool isTimerClosed = false;
  Duration elapsedTime;

  @override
  void initState() {
    super.initState();
    if (![-100, -101, -200, -201].contains(widget.match.formatType)) {
      if (widget.match.status != null &&
          widget.match.status > MatchStatus.IN_PROGRESS) {
        startTimerToCalculateEPOC();
      }
    }
  }

  @override
  void didUpdateWidget(ResultCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    if ([-100, -101, -200, -201].contains(widget.match.formatType)) {
      if (_timer != null) {
        _timer.cancel();
      }
    } else {
      if (_timer == null) {
        startTimerToCalculateEPOC();
      }
    }
  }

  startTimerToCalculateEPOC() {
    // elapsedTime = DateTime.fromMillisecondsSinceEpoch(
    //         DateTime.parse(widget.match.endTime).millisecondsSinceEpoch)
    //     .difference(DateTime.now());
    elapsedTime =
        DateTime.now().difference(DateTime.parse(widget.match.endTime));
    setState(() {
      elapsedDays = elapsedTime.inDays;
      elapsedHours = elapsedTime.inHours - (elapsedDays * 24);
      elapsedMinutes = elapsedTime.inMinutes -
          ((elapsedHours * 60) + (elapsedDays * 24 * 60));
      elapsedSeconds = elapsedTime.inSeconds -
          ((elapsedHours * 60 * 60) +
              (elapsedDays * 24 * 60 * 60) +
              (elapsedMinutes * 60));
    });

    _timer = Timer(Duration(seconds: 1), () {
      startTimerToCalculateEPOC();
      if (elapsedTime.inMilliseconds <= 0) {
        setState(() {
          isTimerClosed = true;
        });
      } else {
        isTimerClosed = false;
      }
    });
  }

  Widget getElapsedTime() {
    TextStyle style = TextStyle(
      color: Color.fromRGBO(153, 101, 40, 1),
      fontSize: 16.0,
    );
    if (elapsedTime.inDays >= 2) {
      return Text(
        "${elapsedDays.toString()} days ago",
        style: style,
      );
    } else if (elapsedTime.inHours >= 24) {
      return Text(
        "${elapsedDays.toString()} day ago",
        style: style,
      );
    } else if (elapsedTime.inHours >= 1) {
      return Text(
        elapsedTime.inHours == 1
            ? "${elapsedHours.toString()} hours ago"
            : "${elapsedHours.toString()} hour ago",
        style: style,
      );
    } else if (elapsedTime.inMinutes >= 1) {
      return Text(
        elapsedTime.inMinutes == 1
            ? "${elapsedMinutes.toString()} min ago"
            : "${elapsedMinutes.toString()} mins ago",
        style: style,
      );
    }
    return Container();
  }

  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline4.copyWith(
        fontWeight: FontWeight.w900,
        fontFamily: 'FagoCo',
        color: Colors.white,
        shadows: [
          Shadow(
            blurRadius: 0.0,
            color: Colors.grey.shade700,
            offset: Offset(0.0, 3.0),
          ),
        ],
      ),
      gradient: LinearGradient(
        colors: [
          Colors.yellow,
          Color.fromRGBO(253, 239, 171, 1),
          Color.fromRGBO(231, 181, 25, 1),
          Colors.red,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  getPrizePoolWidget(BuildContext context, int rewardType, double prizePool) {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> _widgets = [];
    if (rewardType == 2) {
      _widgets.add(
        Image.asset(
          "images/currency/diamond.png",
          height: 24.0,
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: getGradientText(
            context,
            prizePool.toInt().toString(),
          ),
        ),
      );
    }

    if (rewardType == 1) {
      _widgets.add(
        getGradientText(
          context,
          CurrencyFormat.format(
            prizePool,
            decimalDigits: initData.experiments.miscConfig.amountPrecision,
            showDelimeters: false,
          ),
        ),
      );
    }

    return Row(children: _widgets);
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);

    if (widget.match.formatType == -100) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Text(
          "Current Contests",
          style: TextStyle(
              color: Color.fromRGBO(255, 197, 0, 1),
              fontSize: 20.0,
              fontWeight: FontWeight.w700),
        ),
      );
    }

    if (widget.match.formatType == -101) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Text(
          "Completed Contests",
          style: TextStyle(
              color: Color.fromRGBO(255, 197, 0, 1),
              fontSize: 20.0,
              fontWeight: FontWeight.w700),
        ),
      );
    }
    if (widget.match.formatType == -201 || widget.match.formatType == -200) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: InkWell(
          onTap: () {
            widget.match.formatType == -200
                ? widget.showAllCurrent()
                : widget.showAllCompleted();
          },
          child: Text(
            "Show More",
            style: TextStyle(
                color: Color.fromRGBO(255, 197, 0, 1),
                fontSize: 20.0,
                fontWeight: FontWeight.w700),
            textAlign: TextAlign.right,
          ),
        ),
      );
    }

    bool ish2h = widget.match.size == 2;
    bool isCompleted = widget.match.status > MatchStatus.IN_PROGRESS;
    String matchStatus = "Better Luck \n Next time";
    if (widget.match.status == MatchStatus.REFUND) {
      matchStatus = "Abandoned";
    } else if (widget.match.status <= MatchStatus.IN_PROGRESS) {
      matchStatus = "Awaiting Result";
    } else if (widget.match.realCurrency > 0 ||
        widget.match.virtualCurrency > 0) {
      matchStatus = "You Won";
    }

    return Container(
      height: 72,
      padding: const EdgeInsets.all(2.0),
      margin: const EdgeInsets.only(bottom: 8.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(
          width: 2.0,
          color: Color.fromRGBO(189, 165, 139, 1),
        ),
      ),
      child: CupertinoButton(
        padding: EdgeInsets.all(0.0),
        onPressed: widget.onTap,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 4.0, horizontal: 4.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          widget.match.name,
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.black,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        if (isCompleted && elapsedTime.inMinutes >= 1)
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 4.0),
                                child: Image.asset(
                                  "images/icons/clock.png",
                                  color: Colors.grey.shade700,
                                  height: 16.0,
                                ),
                              ),
                              getElapsedTime(),
                            ],
                          ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: FittedBox(
                            alignment: Alignment.centerLeft,
                            fit: BoxFit.scaleDown,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Entry: ",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Color.fromRGBO(153, 101, 40, 1),
                                      fontSize: 14.0,
                                    ),
                                  ),
                                  if (widget.match.entryRealCurrency == 0 &&
                                      widget.match.entryVirtualCurrency == 0)
                                    Text(
                                      "Free",
                                      style: TextStyle(
                                        color: Color.fromRGBO(153, 101, 40, 1),
                                        fontSize: 14.0,
                                      ),
                                    ),
                                  if (widget.match.entryRealCurrency > 0)
                                    Text(
                                      CurrencyFormat.format(
                                          widget.match.entryRealCurrency
                                              .toDouble(),
                                          showDelimeters: false,
                                          decimalDigits: initData.experiments
                                              .miscConfig.amountPrecision),
                                      style: TextStyle(
                                        color: Color.fromRGBO(153, 101, 40, 1),
                                        fontSize: 14.0,
                                      ),
                                    ),
                                  if (widget.match.entryRealCurrency > 0 &&
                                      widget.match.entryVirtualCurrency > 0)
                                    Text(
                                      " + ",
                                      style: TextStyle(
                                        color: Color.fromRGBO(153, 101, 40, 1),
                                        fontSize: 14.0,
                                      ),
                                    ),
                                  if (widget.match.entryVirtualCurrency > 0)
                                    Image.asset(
                                      "images/icons/goldbar-icon.png",
                                      height: 12.0,
                                    ),
                                  if (widget.match.entryVirtualCurrency > 0)
                                    Text(
                                      "${widget.match.entryVirtualCurrency}",
                                      style: TextStyle(
                                        color: Color.fromRGBO(153, 101, 40, 1),
                                        fontSize: 14.0,
                                      ),
                                    ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: ish2h
                              ? Container()
                              : Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "Size: ${widget.match.joined}/${widget.match.size}",
                                      style: TextStyle(
                                        color: Color.fromRGBO(153, 101, 40, 1),
                                        fontSize: 14.0,
                                      ),
                                    ),
                                  ],
                                ),
                        ),
                        Expanded(
                          child: widget.match.status != MatchStatus.REFUND &&
                                  widget.match.status > MatchStatus.IN_PROGRESS
                              ? Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "Rank: ",
                                      style: TextStyle(
                                        color: Color.fromRGBO(153, 101, 40, 1),
                                        fontSize: 14.0,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                    Column(
                                      children: <Widget>[
                                        if (widget.match.rank == 1)
                                          Image.asset(
                                            "images/icons/crown-small.png",
                                            height: 8.0,
                                          ),
                                        Text(
                                          "${widget.match.rank}",
                                          style: TextStyle(
                                            color:
                                                Color.fromRGBO(153, 101, 40, 1),
                                            fontSize: 14.0,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    )
                                  ],
                                )
                              : Container(),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Container(
                    width: 86,
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: (widget.match.status <= MatchStatus.IN_PROGRESS)
                          ? Colors.white
                          : Color.fromRGBO(250, 243, 224, 1),
                      image: (widget.match.realCurrency > 0 ||
                              widget.match.virtualCurrency > 0)
                          ? DecorationImage(
                              image: AssetImage(
                                "images/template/prize.png",
                              ),
                              fit: BoxFit.fill,
                            )
                          : null,
                    ),
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              if (matchStatus == "You Won")
                                Text(
                                  matchStatus,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .headline6
                                      .copyWith(
                                        fontWeight: FontWeight.w900,
                                        fontSize: 16.0,
                                      ),
                                  textAlign: TextAlign.center,
                                )
                              else
                                Text(
                                  matchStatus,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .headline6
                                      .copyWith(
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.w400,
                                          color:
                                              Color.fromRGBO(196, 178, 138, 1)),
                                  textAlign: TextAlign.center,
                                ),
                            ],
                          ),
                          if (widget.match.realCurrency > 0)
                            getPrizePoolWidget(
                              context,
                              1,
                              widget.match.realCurrency,
                            ),
                          if (widget.match.virtualCurrency > 0)
                            getPrizePoolWidget(
                              context,
                              2,
                              widget.match.virtualCurrency.toDouble(),
                            ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
