import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/api/lobby/lobbyAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/lobby/leaderboard.dart';
import 'package:solitaire_gold/models/lobby/mymatches.dart';
import 'package:solitaire_gold/models/lobby/prizestructure_details.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/popups/reportdialog.dart';
import 'package:solitaire_gold/screens/auth/baseauth.dart';
import 'package:solitaire_gold/screens/lobby/result/leaderboardcard.dart';
import 'package:solitaire_gold/screens/prizestructure/prizestructure.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class LeaderboardDetails extends StatefulWidget {
  final Leaderboard leaderboardData;
  final int gameId;
  final bool showDelayedReplay;
  final Map<String, dynamic> analyticsData;
  final String source;
  LeaderboardDetails({
    this.leaderboardData,
    this.gameId,
    this.showDelayedReplay,
    @required this.analyticsData,
    this.source,
  });

  @override
  _LeaderboardDetailsState createState() => _LeaderboardDetailsState();
}

class _LeaderboardDetailsState extends State<LeaderboardDetails> with BaseAuth {
  Timer _timer;
  double loadingPercent;
  bool replayButtonEnabled;
  bool bPrizeStructureEnabled = true;
  double playerScore;
  double winAmountGems;
  double winAmountCash;
  bool replayClicked = false;

  LobbyAPI _lobbyAPI = LobbyAPI();

  @override
  void initState() {
    replayButtonEnabled = !widget.showDelayedReplay;
    if (widget.showDelayedReplay) {
      startTimer();
    }
    loadingPercent = 100;
    GamePlayAnalytics().onResultScreenLoaded(
      context,
      source: widget.source,
      leaderboardData: widget.leaderboardData,
    );
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  Future<dynamic> _showPrizeStructure(
      int prizeStructureId, int joinCount, int status, int maxPlayers) async {
    GamePlayAnalytics().onResultInfoClicked(context,
        source: widget.source, leaderboardData: widget.leaderboardData);
    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> _response = await _lobbyAPI.getPrizeStructure(context,
        prizestructureId: prizeStructureId,
        joinCount: joinCount,
        status: status);
    PrizeStrucutreDetails _prizestructureDetails =
        PrizeStrucutreDetails.fromJson(_response["data"]);

    Loader().showLoader(false);
    return showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return PrizeStructure(
          prizeStructureId: prizeStructureId,
          prizestructureDetails: _prizestructureDetails,
          onJoin: null,
          maxPlayers: maxPlayers,
          analyticData: widget.analyticsData == null
              ? {
                  "mid": widget.leaderboardData.matchId,
                }
              : widget.analyticsData,
          source: widget.source,
        );
      },
    );
  }

  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline5.copyWith(
        fontWeight: FontWeight.w600,
        color: Colors.white,
        shadows: [
          Shadow(
            blurRadius: 0.0,
            color: Colors.grey.shade700,
            offset: Offset(0.0, 2.0),
          ),
        ],
      ),
      gradient: LinearGradient(
        colors: [
          Colors.yellow,
          Color.fromRGBO(253, 239, 171, 1),
          Color.fromRGBO(231, 181, 25, 1),
          Colors.red,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  getEntryFeeWidget(
      BuildContext context, double realCurrency, int virtualCurrency) {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> _widgets = [];
    if (virtualCurrency != null && virtualCurrency > 0) {
      _widgets.add(
        Image.asset(
          "images/currency/diamond.png",
          height: 18.0,
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 2.0),
          child: getGradientText(
            context,
            virtualCurrency.toString(),
          ),
        ),
      );
    }

    if (virtualCurrency != null &&
        virtualCurrency > 0 &&
        realCurrency != null &&
        realCurrency > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4.0),
          child: getGradientText(
            context,
            "+",
          ),
        ),
      );
    }

    if (realCurrency != null && realCurrency > 0) {
      _widgets.add(
        getGradientText(
          context,
          CurrencyFormat.format(
            realCurrency.toDouble(),
            decimalDigits: initData.experiments.miscConfig.amountPrecision,
            showDelimeters: false,
          ),
        ),
      );
    }
    if ((realCurrency == null || realCurrency == 0) &&
        (virtualCurrency == null || virtualCurrency == 0)) {
      _widgets.add(
        getGradientText(
          context,
          "Free",
        ),
      );
    }

    return Container(
      height: 25,
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _widgets,
        ),
      ),
    );
  }

  getPrizePoolWidget(
      BuildContext context, double prizePool, double diamondPrizePool) {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> _widgets = [];
    if (diamondPrizePool > 0) {
      _widgets.add(
        Image.asset(
          "images/currency/diamond.png",
          height: 18.0,
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 2.0),
          child: getGradientText(
            context,
            diamondPrizePool
                .toInt()
                .toString()
                .replaceAll(".0", "")
                .replaceAll(".00", ""),
          ),
        ),
      );
    }

    if (diamondPrizePool > 0 && prizePool > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4.0),
          child: getGradientText(
            context,
            "+",
          ),
        ),
      );
    }

    if (prizePool > 0) {
      _widgets.add(
        getGradientText(
          context,
          CurrencyFormat.format(
            prizePool.toDouble(),
            decimalDigits: initData.experiments.miscConfig.amountPrecision,
            showDelimeters: false,
          ),
        ),
      );
    }

    return Container(
      height: 25,
      child: FittedBox(
        fit: BoxFit.scaleDown,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _widgets,
        ),
      ),
    );
  }

  _getHeader() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("images/header.png"),
          fit: BoxFit.fill,
        ),
      ),
      padding: const EdgeInsets.only(top: 30.0, bottom: 24.0),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 8.0,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/stats_rectangle.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: Text(
                      "Size",
                      style: Theme.of(context)
                          .primaryTextTheme
                          .headline6
                          .copyWith(fontSize: 17.0),
                    ),
                  ),
                  Container(
                    height: 25,
                    child: getGradientText(
                      context,
                      widget.leaderboardData.joinedCount.toString() +
                          "/" +
                          widget.leaderboardData.size.toString(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 8.0,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/stats_rectangle.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: Text(
                      "Entry Fee",
                      style: Theme.of(context)
                          .primaryTextTheme
                          .headline6
                          .copyWith(fontSize: 17.0),
                    ),
                  ),
                  getEntryFeeWidget(
                    context,
                    widget.leaderboardData.entryFeeRealCurrency,
                    widget.leaderboardData.entryFeevirtualCurrency,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 8.0,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/stats_rectangle.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Stack(
                children: <Widget>[
                  if (widget.leaderboardData.size > 2)
                    Positioned(
                      top: 0,
                      right: 4,
                      child: InkWell(
                        onTap: () async {
                          if (bPrizeStructureEnabled && !replayClicked) {
                            bPrizeStructureEnabled = false;
                            await _showPrizeStructure(
                                widget.leaderboardData.prizeStructureId,
                                widget.leaderboardData.joinedCount,
                                widget.leaderboardData.matchStatus,
                                widget.leaderboardData.size);

                            bPrizeStructureEnabled = true;
                          }
                        },
                        child: Image.asset(
                          "images/icons/info.png",
                          height: 20.0,
                        ),
                      ),
                    ),
                  Center(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom: 4.0),
                          child: Text(
                            "Prize",
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(fontSize: 17.0),
                          ),
                        ),
                        getPrizePoolWidget(
                          context,
                          widget.leaderboardData.rewardType == 1
                              ? widget.leaderboardData.prizePool
                              : 0,
                          widget.leaderboardData.rewardType == 2
                              ? widget.leaderboardData.prizePool
                              : 0,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 8.0,
          ),
        ],
      ),
    );
  }

  void onTieBreaker(Leaderboard leaderboardData) {
    Navigator.of(context).pop({"tieBreaker": true});
  }

  startTimer() {
    _timer = Timer.periodic(Duration(milliseconds: 100), (timer) {
      if (loadingPercent <= 0) {
        setState(() {
          replayButtonEnabled = true;
          loadingPercent = 100;
        });
        timer.cancel();
        return;
      }
      setState(() {
        loadingPercent -= 2;
      });
    });
  }

  getBody() {
    double verticalSpaceToLeft = 160.0 * 2;
    double screenHeight = MediaQuery.of(context).size.height;
    double availableheight = screenHeight - verticalSpaceToLeft;

    double maxLeaderboardHeight =
        widget.leaderboardData.matchStatus > MatchStatus.IN_PROGRESS
            ? (widget.leaderboardData.leaderboard.length <= 2
                ? 185.0
                : widget.leaderboardData.leaderboard.length == 3
                    ? 300.0
                    : 350.0)
            : widget.leaderboardData.size <= 2
                ? 185.0
                : 350.0;

    return Container(
      constraints: BoxConstraints(
        maxHeight: maxLeaderboardHeight > availableheight
            ? availableheight
            : maxLeaderboardHeight,
      ),
      child: Column(
        children: [
          Flexible(
            child: ListView.builder(
              itemCount:
                  widget.leaderboardData.matchStatus > MatchStatus.IN_PROGRESS
                      ? widget.leaderboardData.leaderboard.length
                      : widget.leaderboardData.size,
              itemBuilder: (context, index) {
                if (index < widget.leaderboardData.leaderboard.length) {
                  return LeaderboardCard(
                    rank: widget.leaderboardData.leaderboard[index].rank,
                    diamonds: widget
                        .leaderboardData.leaderboard[index].virtualCurrency,
                    prize:
                        widget.leaderboardData.leaderboard[index].realCurrency,
                    username:
                        widget.leaderboardData.leaderboard[index].userName,
                    userAvatarId:
                        widget.leaderboardData.leaderboard[index].avatarId,
                    score: widget.leaderboardData.leaderboard[index].score,
                    allowTieBreaker: widget.leaderboardData.allowTiebreaker,
                    gameStatus:
                        widget.leaderboardData.leaderboard[index].gameStatus,
                  );
                } else {
                  return LeaderboardCard(
                    rank: 0,
                    diamonds: 0,
                    prize: 0,
                    username: "",
                    userAvatarId: -1,
                    score: 0,
                    isPending: true,
                    gameStatus: -1,
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  onReplayPressed() async {
    if (!replayButtonEnabled || replayClicked) {
      return null;
    }
    setState(() {
      replayClicked = true;
    });
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }
    replayButtonEnabled = false;
    GamePlayAnalytics().onReplayClicked(
      context,
      source: widget.source,
      matchId: widget.leaderboardData.matchId,
    );

    Navigator.of(context).pop({"replay": true});
  }

  getFooterButton() {
    double width = MediaQuery.of(context).size.width;
    double dp = MediaQuery.of(context).devicePixelRatio;
    double leftMargin = 12 / 1080 * (width * dp);
    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }
    if (widget.leaderboardData.allowTiebreaker) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              height: width < 320 ? 56.0 : 60,
              margin: const EdgeInsets.symmetric(vertical: 8.0),
              child: Button(
                text: "Tie Breaker".toUpperCase(),
                style: ctaStyle,
                size: ButtonSize.medium,
                onPressed: () {
                  onTieBreaker(widget.leaderboardData);
                },
              ),
            ),
          ),
        ],
      );
    } else {
      bool isUserPlayed = false;
      if (widget.leaderboardData.size <= 2) {
        User user = Provider.of(
          context,
          listen: false,
        );
        widget.leaderboardData.leaderboard.forEach((element) {
          if (element.userId == user.id) {
            isUserPlayed = element.gameStatus == 1;
          }
        });
      }
      return (widget.leaderboardData.matchStatus > MatchStatus.IN_PROGRESS &&
              widget.leaderboardData.size > 2)
          ? Container()
          : Container(
              margin: EdgeInsets.symmetric(horizontal: leftMargin),
              child: Row(
                mainAxisAlignment: widget.leaderboardData.size == 2
                    ? MainAxisAlignment.spaceEvenly
                    : MainAxisAlignment.center,
                children: [
                  widget.leaderboardData.size == 2 && isUserPlayed
                      ? Expanded(
                          child: Container(
                            height: 56,
                            child: Center(
                              child: InkWell(
                                onTap: () async {
                                  await onReplayPressed();
                                },
                                child: Container(
                                  height: 56,
                                  child: Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Button(
                                        type: ButtonType.secondary,
                                        text: "",
                                        size: ButtonSize.medium,
                                        onPressed: replayButtonEnabled
                                            ? () async {
                                                await onReplayPressed();
                                              }
                                            : null,
                                      ),
                                      Container(
                                        alignment: Alignment.center,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  bottom: 8.0,
                                                  left: 0.0,
                                                  right: 2.0,
                                                  top: 4.0),
                                              child: Text(
                                                "Replay".toUpperCase(),
                                                textAlign: TextAlign.end,
                                                style: ctaStyle.copyWith(
                                                  color: replayButtonEnabled
                                                      ? Color.fromRGBO(
                                                          123, 61, 0, 1)
                                                      : Colors.grey.shade700,
                                                ),
                                              ),
                                            ),
                                            replayButtonEnabled &&
                                                    !replayClicked
                                                ? Container(
                                                    height: 30,
                                                    child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                bottom: 8.0,
                                                                left: 8.0,
                                                                top: 4),
                                                        child: Image.asset(
                                                          "images/icons/replay.png",
                                                          fit: BoxFit.scaleDown,
                                                        )),
                                                  )
                                                : Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 12.0,
                                                            bottom: 8),
                                                    child: Container(
                                                      width: 20.0,
                                                      height: 20.0,
                                                      child: Theme(
                                                        data: Theme.of(context)
                                                            .copyWith(
                                                                accentColor: Colors
                                                                    .grey
                                                                    .shade700),
                                                        child:
                                                            new CircularProgressIndicator(
                                                          strokeWidth: 2,
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  Expanded(
                    child: Container(
                      height: 56.0,
                      margin: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Button(
                        text: "Ok".toUpperCase(),
                        size: ButtonSize.medium,
                        style: ctaStyle,
                        onPressed: () {
                          if (replayClicked) return;
                          User user = Provider.of(context, listen: false);
                          widget.leaderboardData.leaderboard.forEach(
                            (item) {
                              if (item.userName == user.loginName) {
                                winAmountGems = item.virtualCurrency;
                                winAmountCash = item.realCurrency;
                                playerScore = item.score;
                              }
                            },
                          );
                          GamePlayAnalytics().onOkClicked(context,
                              source: widget.source,
                              leaderboardData: widget.leaderboardData,
                              winAmountGems: winAmountGems,
                              winAmountCash: winAmountCash,
                              score: playerScore);
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ),
                ],
              ),
            );
    }
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    bool isWinnerUser =
        widget.leaderboardData.matchStatus > MatchStatus.IN_PROGRESS &&
            widget.leaderboardData.leaderboard.any((item) =>
                item.userName == user.loginName &&
                (item.realCurrency > 0 || item.virtualCurrency > 0));
    String headerAsset = "images/titles/result-pending.png";
    if (widget.leaderboardData.matchStatus > MatchStatus.IN_PROGRESS) {
      if (widget.leaderboardData.size > 2)
        headerAsset = "images/titles/leaderboard.png";
      else
        headerAsset = "images/titles/results.png";
    }

    return CustomDialog(
      padding: EdgeInsets.zero,
      dialog: Dialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Stack(
          alignment: Alignment.center,
          children: [
            InkWell(
              onTap: () {
                if (!replayClicked) Navigator.of(context).pop();
              },
              splashColor: Colors.transparent,
              highlightColor: Colors.black12,
            ),
            Column(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.only(bottom: isWinnerUser ? 102.0 : 0.0),
                        child: Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            if (isWinnerUser)
                              Positioned(
                                top: 0.0,
                                child: Image.asset(
                                  "images/icons/crown.png",
                                  height: 140,
                                ),
                              ),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: isWinnerUser ? 112.0 : 0.0),
                              child: CommonDialogFrame(
                                onClose: () {
                                  GamePlayAnalytics().onResultCloseClicked(
                                    context,
                                    source: widget.source,
                                    leaderboardData: widget.leaderboardData,
                                    gameId: widget.gameId,
                                  );
                                },
                                titleAsset: headerAsset,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    _getHeader(),
                                    SizedBox(
                                      height: 4.0,
                                    ),
                                    getBody(),
                                    getFooterButton(),
                                  ],
                                ),
                                footer: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "ID #${widget.leaderboardData.matchId}",
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .bodyText1
                                          .copyWith(
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                          ),
                                    ),
                                    InkWell(
                                      onTap: () async {
                                        if (replayClicked) return;
                                        await showDialog(
                                          barrierColor:
                                              Color.fromARGB(175, 0, 0, 0),
                                          context: context,
                                          builder: (BuildContext context) {
                                            return ReportDialog(
                                              matchId: widget
                                                  .leaderboardData.matchId,
                                              gameId: widget.gameId,
                                              source: widget.showDelayedReplay
                                                  ? "game_table"
                                                  : "result",
                                            );
                                          },
                                        );
                                      },
                                      child: Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 4.0),
                                            child: Image.asset(
                                              "images/icons/report.png",
                                              height: 14.0,
                                              color:
                                                  Theme.of(context).errorColor,
                                            ),
                                          ),
                                          Text(
                                            "REPORT".toUpperCase(),
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .bodyText1
                                                .copyWith(
                                                  fontWeight: FontWeight.w700,
                                                  color: Colors.white,
                                                  decoration:
                                                      TextDecoration.underline,
                                                ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
