import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class LeaderboardCard extends StatelessWidget {
  final int rank;
  final double prize;
  final double diamonds;
  final double score;
  final String username;
  final int userAvatarId;
  final bool allowTieBreaker;
  final bool isPending;
  final int gameStatus;

  LeaderboardCard({
    @required this.rank,
    @required this.prize,
    @required this.diamonds,
    @required this.score,
    @required this.username,
    @required this.userAvatarId,
    @required this.gameStatus,
    this.allowTieBreaker = false,
    this.isPending = false,
  });

  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline4.copyWith(
            fontWeight: FontWeight.w800,
            color: Colors.white,
          ),
      gradient: LinearGradient(
        colors: [
          Colors.redAccent.shade100,
          Colors.redAccent.shade700,
          Colors.black,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  getPrizeWidget(
      BuildContext context, double realCurrency, double virtualCurrency) {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> _widgets = [];
    if (virtualCurrency != null && virtualCurrency > 0) {
      _widgets.add(
        Image.asset(
          "images/currency/diamond.png",
          height: 22.0,
        ),
      );

      _widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 2.0),
          child: getGradientText(
            context,
            virtualCurrency
                .toString()
                .replaceAll(".0", "")
                .replaceAll(".00", ""),
          ),
        ),
      );
    }

    if (virtualCurrency != null &&
        virtualCurrency > 0 &&
        realCurrency != null &&
        realCurrency > 0) {
      _widgets.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: getGradientText(context, "+"),
        ),
      );
    }

    if (realCurrency != null && realCurrency > 0) {
      _widgets.add(
        getGradientText(
          context,
          CurrencyFormat.format(
            realCurrency.toDouble(),
            decimalDigits: initData.experiments.miscConfig.amountPrecision,
            showDelimeters: false,
          ),
        ),
      );
    }

    return Row(children: _widgets);
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    InitData initDataProvider = Provider.of(context, listen: false);
    bool isCurrentUser = user.loginName == username;
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      shape: RoundedRectangleBorder(
        side: BorderSide(
          width: 2.0,
          color: Color.fromRGBO(255, 206, 21, 1),
        ),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Container(
        padding: const EdgeInsets.all(12.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          gradient: isCurrentUser
              ? LinearGradient(
                  colors: [
                    Color.fromRGBO(253, 239, 171, 1),
                    Color.fromRGBO(211, 145, 74, 1),
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(0.0, 1.0),
                )
              : null,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 4.0),
                  child: Row(
                    children: <Widget>[
                      if (rank > 3 || isPending)
                        Stack(
                          alignment: Alignment.center,
                          children: <Widget>[
                            Image.asset(
                              "images/icons/rank.png",
                              height: 36.0,
                            ),
                            if (isPending)
                              Text(
                                "-",
                                style: TextStyle(
                                  fontSize: 24.0,
                                ),
                              )
                            else
                              Text("$rank"),
                          ],
                        ),
                      if (rank <= 3 && rank >= 1)
                        Image.asset(
                          "images/icons/rank-$rank.png",
                          height: 46.0,
                        ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 10.0, right: 10.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    border: Border.all(
                      width: 2.0,
                      color: Color.fromRGBO(188, 185, 160, 1),
                    ),
                  ),
                  child: ClipRRect(
                    clipBehavior: Clip.hardEdge,
                    borderRadius: BorderRadius.circular(
                      50,
                    ),
                    child: CachedNetworkImage(
                      imageUrl: initDataProvider.serverData.avatarUrl +
                          userAvatarId.toString() +
                          ".png",
                      fit: BoxFit.fitHeight,
                      placeholder: (context, string) {
                        return Container(
                          padding: EdgeInsets.all(16.0),
                          child: CircularProgressIndicator(
                            strokeWidth: 2.0,
                          ),
                          width: 56,
                          height: 56,
                        );
                      },
                      errorWidget: (context, url, error) => Image.asset(
                        "images/icons/avatar-placeholder.png",
                        height: 56.0,
                      ),
                      height: 56,
                      width: 56,
                    ),
                  ),
                ),
                if (isPending)
                  Text(
                    "Yet to join",
                    style:
                        Theme.of(context).primaryTextTheme.headline6.copyWith(
                              color: Color.fromRGBO(54, 3, 12, 1),
                              fontSize: 18.0,
                              fontWeight: FontWeight.w700,
                            ),
                  )
                else
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        user.loginName == username ? "You" : "$username",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Color.fromRGBO(54, 3, 12, 1),
                              fontSize: 18.0,
                              fontWeight: FontWeight.w700,
                            ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 4.0,
                        ),
                        margin: const EdgeInsets.only(top: 8.0),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(248, 237, 227, 1),
                          borderRadius: BorderRadius.circular(4.0),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                              color: Colors.grey.shade500,
                              offset: Offset(0, -1),
                            ),
                            BoxShadow(
                              blurRadius: 1.0,
                              spreadRadius: 0.0,
                              color: Colors.grey.shade500,
                              offset: Offset(0, 1),
                            ),
                          ],
                        ),
                        child: gameStatus == 1
                            ? Row(
                                children: <Widget>[
                                  Text(
                                    "Score : ",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Color.fromRGBO(54, 3, 12, 1),
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w700,
                                        ),
                                  ),
                                  Text(
                                    "${score.toInt()}",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Color.fromRGBO(109, 6, 26, 1),
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w700,
                                        ),
                                  ),
                                ],
                              )
                            : (gameStatus == -1
                                ? Text(
                                    "Yet to join",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Color.fromRGBO(109, 6, 26, 1),
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w700,
                                        ),
                                  )
                                : Text(
                                    "Now playing",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Color.fromRGBO(109, 6, 26, 1),
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.w700,
                                        ),
                                  )),
                      ),
                    ],
                  ),
              ],
            ),
            if (isPending)
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Image.asset(
                  "images/icons/search.png",
                  height: 28.0,
                ),
              )
            else if (allowTieBreaker)
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(right: 28.0),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        getGradientText(context, "Tie".toUpperCase()),
                      ],
                    ),
                  ),
                ),
              )
            else if (prize > 0 || diamonds > 0)
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: FittedBox(
                    fit: BoxFit.fitWidth,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "Prize",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline6
                              .copyWith(
                                color: Colors.brown.shade900,
                              ),
                        ),
                        getPrizeWidget(context, prize, diamonds),
                      ],
                    ),
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
