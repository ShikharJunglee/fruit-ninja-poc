import 'dart:math';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';

class FTUE extends StatefulWidget {
  final String source;

  FTUE(this.source);

  @override
  _FTUEState createState() => _FTUEState();
}

class _FTUEState extends State<FTUE> {
  PageController _pageController;
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      initialPage: 0,
    );
    _pageController.addListener(() {
      print(_pageController.page);
      if (_selectedIndex != _pageController.page.floor()) {
        GamePlayAnalytics().onFTUESwiped(context,
            source: widget.source,
            screenNumber: _selectedIndex,
            direction: _selectedIndex < _pageController.page.floor()
                ? "left"
                : "right");
      }
      setState(() {
        _selectedIndex = _pageController.page.floor();
      });
    });
    GamePlayAnalytics().onFTUELoaded(context, source: widget.source);
  }

  bool isTablet(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var diagonal =
        sqrt((size.width * size.width) + (size.height * size.height));
    var isTablet = diagonal > 1100.0;
    return isTablet;
  }

  @override
  void dispose() {
    super.dispose();
  }

  getPageWith(
    BuildContext context, {
    String titleImagePath,
    String contentImagePath,
    FlareActor contentFlare,
    String line1Text,
    String line2Text,
    RichText line2,
    Widget bottomLine1,
    Widget bottomLine2,
  }) {
    return Column(
      children: [
        Expanded(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AspectRatio(
                aspectRatio: isTablet(context)
                    ? (9 / 16)
                    : MediaQuery.of(context).size.aspectRatio,
                child: Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Positioned(
                          left: 0.0,
                          right: 0.0,
                          top: 42.0,
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 16.0),
                            child: Image.asset("images/ftue/popup-top.png"),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              bottom: 24.0, left: 32.0, right: 32.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: Image.asset(
                                  titleImagePath,
                                  height: 68.0,
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: line1Text,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .subtitle1
                                      .copyWith(
                                        color: Colors.white,
                                      ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          if (line2 != null)
                            Expanded(
                              child: line2,
                            ),
                          if (line2 == null)
                            Expanded(
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: line2Text,
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .subtitle1
                                          .copyWith(
                                            color: Colors.white,
                                          ),
                                    )
                                  ],
                                ),
                              ),
                            )
                        ],
                      ),
                    ),
                    if (contentFlare != null)
                      AspectRatio(
                        child: contentFlare,
                        aspectRatio: 1080 / 1030,
                      ),
                    if (contentFlare == null)
                      Image.asset(
                        contentImagePath,
                        fit: BoxFit.contain,
                      ),
                    if (bottomLine1 != null) bottomLine1,
                    if (bottomLine2 != null) bottomLine2,
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("images/ftue/background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          bottom: false,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                    icon: Image.asset("images/icons/close2.png"),
                    onPressed: () {
                      GamePlayAnalytics().onFTUECloseClicked(
                        context,
                        source: widget.source,
                        screenNumber: _selectedIndex,
                      );
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
              Expanded(
                child: PageView(
                  controller: _pageController,
                  children: <Widget>[
                    getPageWith(
                      context,
                      contentFlare: FlareActor(
                        "images/ftue/page1.flr",
                        alignment: Alignment.center,
                        animation: "page1",
                      ),
                      titleImagePath: "images/ftue/how-to-play.png",
                      line1Text: "Fill these 4 sections with cards from",
                      line2: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.white,
                              ),
                          children: [
                            TextSpan(
                              text: "Ace",
                              style: TextStyle(
                                color: Colors.yellow,
                              ),
                            ),
                            TextSpan(
                              text: " to ",
                            ),
                            TextSpan(
                              text: "King ",
                              style: TextStyle(
                                color: Colors.yellow,
                              ),
                            ),
                            TextSpan(
                              text: "of each suit",
                            )
                          ],
                        ),
                      ),
                    ),
                    getPageWith(
                      context,
                      contentImagePath: "images/ftue/page2.jpg",
                      titleImagePath: "images/ftue/organizing-cards.png",
                      line1Text: "In this section, arrange cards from",
                      line2: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.white,
                              ),
                          children: [
                            TextSpan(
                              text: "King",
                              style: TextStyle(
                                color: Colors.yellow,
                              ),
                            ),
                            TextSpan(
                              text: " to ",
                            ),
                            TextSpan(
                              text: "Ace ",
                              style: TextStyle(
                                color: Colors.yellow,
                              ),
                            ),
                            TextSpan(
                              text: "in alternating colors",
                            ),
                          ],
                        ),
                      ),
                    ),
                    getPageWith(
                      context,
                      contentFlare: FlareActor(
                        "images/ftue/page3.flr",
                        alignment: Alignment.center,
                        animation: "page3",
                        fit: BoxFit.fitHeight,
                      ),
                      titleImagePath: "images/ftue/closed-card.png",
                      line1Text: "Tap on Closed Deck",
                      line2Text: "to get required cards",
                    ),
                    getPageWith(
                      context,
                      contentImagePath: "images/ftue/page4.jpg",
                      titleImagePath: "images/ftue/time-bonus.png",
                      line1Text: "Each game is 5 min",
                      line2Text: "Finish early to get Time Bonus",
                      bottomLine1: FittedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              "Not all games can be solved, use",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .headline6
                                  .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 3.0, right: 6.0),
                              padding: EdgeInsets.all(6.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.green.shade800,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(1, 2.0),
                                    )
                                  ]),
                              child: Icon(
                                Icons.exit_to_app,
                                color: Colors.white,
                                size: 18.0,
                              ),
                            ),
                            Text(
                              "button",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .headline6
                                  .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                  ),
                            )
                          ],
                        ),
                      ),
                      bottomLine2: Text(
                        "and finish the game",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                            ),
                      ),
                    ),
                    getPageWith(
                      context,
                      contentImagePath: "images/ftue/page5.jpg",
                      titleImagePath: "images/ftue/fair-play.png",
                      line1Text: "All players in a contest",
                      line2Text: "get the same cards",
                      bottomLine1: FittedBox(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset(
                                "images/ftue/fair-play-icon.png",
                                height: 24.0,
                              ),
                            ),
                            Text(
                              "Equal chance for all players",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .headline6
                                  .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w400,
                                  ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 24.0,
                ),
                child: CupertinoButton(
                  padding: EdgeInsets.all(0.0),
                  child: Container(
                    height: 48.0,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            "images/buttons/button-secondary-medium.png"),
                      ),
                    ),
                    child: Text(
                      _selectedIndex == 4 ? "LETS PLAY" : "NEXT",
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.brown.shade800,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  pressedOpacity: 0.7,
                  onPressed: () {
                    GamePlayAnalytics().onFTUENextClicked(
                      context,
                      source: widget.source,
                      screenNumber: _selectedIndex,
                    );
                    if (_selectedIndex == 4) {
                      Navigator.of(context).pop();
                    } else {
                      _pageController.nextPage(
                        curve: Curves.ease,
                        duration: Duration(milliseconds: 350),
                      );
                    }
                  },
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 32.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 8.0,
                        width: 8.0,
                        decoration: BoxDecoration(
                          color: _selectedIndex == 0
                              ? Colors.white
                              : Colors.white12,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 8.0,
                        width: 8.0,
                        decoration: BoxDecoration(
                          color: _selectedIndex == 1
                              ? Colors.white
                              : Colors.white12,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 8.0,
                        width: 8.0,
                        decoration: BoxDecoration(
                          color: _selectedIndex == 2
                              ? Colors.white
                              : Colors.white12,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 8.0,
                        width: 8.0,
                        decoration: BoxDecoration(
                          color: _selectedIndex == 3
                              ? Colors.white
                              : Colors.white12,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        height: 8.0,
                        width: 8.0,
                        decoration: BoxDecoration(
                          color: _selectedIndex == 4
                              ? Colors.white
                              : Colors.white12,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
