import 'dart:async';
import 'dart:io';

import 'package:flash/flash.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission/permission.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/api/banner/banner.dart';
import 'package:solitaire_gold/api/banner/banner_zone.dart';
import 'package:solitaire_gold/api/geolocation/locationAPI.dart';
import 'package:solitaire_gold/api/geolocation/mylocationservice.dart';
import 'package:solitaire_gold/api/lobby/lobbyAPI.dart';
import 'package:solitaire_gold/api/user/accountAPI.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/commonwidgets/lobby_appbar.dart';
import 'package:solitaire_gold/models/addcash/deposit.dart';
import 'package:solitaire_gold/models/addcash/paymentdetails.dart';
import 'package:solitaire_gold/models/banners/banners.dart';
import 'package:solitaire_gold/models/databasemanager.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/generic_dialog.dart';
import 'package:solitaire_gold/providers/deeplinkroute.dart';
import 'package:solitaire_gold/providers/fcmDataRoute.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/lobby/account/account.dart';
import 'package:solitaire_gold/screens/lobby/addcash/add_cash.dart';
import 'package:solitaire_gold/screens/lobby/addcash/add_cash_helper.dart';
import 'package:solitaire_gold/screens/lobby/addcash/transactionsuccess.dart';
import 'package:solitaire_gold/screens/lobby/bottomnavigationpanel.dart';
import 'package:solitaire_gold/screens/lobby/closedialog.dart';
import 'package:solitaire_gold/screens/lobby/crash_test.dart';
import 'package:solitaire_gold/screens/lobby/deals/deals.dart';
import 'package:solitaire_gold/screens/lobby/ftue/ftue_dialog.dart';
import 'package:solitaire_gold/screens/lobby/home/home.dart';
import 'package:solitaire_gold/screens/lobby/notification_popup.dart';
import 'package:solitaire_gold/screens/lobby/result/result.dart';
import 'package:solitaire_gold/screens/lobby/welcomepopup.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/screens/solitairegold/solitairegold.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/deeplink_cta.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';
import 'package:solitaire_gold/utils/location_permission.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedprefkeys.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:solitaire_gold/utils/websocket/request.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class Lobby extends StatefulWidget {
  @override
  _LobbyState createState() => _LobbyState();
}

class _LobbyState extends State<Lobby>
    with SingleTickerProviderStateMixin, BasePage {
  bool isAddCash = false;
  bool isDisconnectionPopupOpen = false;

  TabController _pageTabController;
  StreamSubscription _solitaireSubscription;

  final BannerAPI _bannerAPI = BannerAPI();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  Widget _addCash = Container();

  Result _resultScreen = Result(
    matchId: null,
    showLeaderBoardOnLaunch: false,
  );

  @override
  void initState() {
    SolitaireGold.isLaunched = false;
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
    _pageTabController = TabController(length: 5, vsync: this, initialIndex: 2);
    _pageTabController.addListener(() {
      if (!_pageTabController.indexIsChanging) {
        setState(() {});
      }
    });

    super.initState();
  }

  setData(BuildContext context) async {
    connectWebsocket(context);
    getDeepLinkingRoutingData(context);
    addWebEngageScreenData();
    subscribeToNativeToFlutterEventsListener(context);
    _solitaireSubscription =
        EventManager().subscribe(EventName.SOLITAIRE_LOADING, closeFlash);

    fetchMyMatchesData();
    InitData initData = Provider.of(context, listen: false);
    int index = initData.experiments.getLobbyIndex();
    if (index != _pageTabController.index) {
      setState(() {
        _pageTabController.index = index;
      });
    }

    User user = Provider.of(context, listen: false);
    bool loadInteractive = (await SharedPrefHelper()
            .getFromSharedPref(SharedPrefKey.interactiveDemo)) ==
        "true";

    if (user.newUser || loadInteractive) {
      await showFTUEDialog();
    }

    if (user.newUser) {
      if (user.welcomePopup != null) {
        if (initData.experiments.welcomeBackConfig.welcomeBonusEnabled) {
          LobbyAnalytics().onLoginPopupLoaded(context, source: "l0");
          await showDialog(
              barrierColor: Color.fromARGB(175, 0, 0, 0),
              context: context,
              builder: (BuildContext context) {
                return WelcomePopup(popupData: user.welcomePopup);
              });
          LobbyAnalytics().onLoginPopupClosed(context, source: "l0");
        }
      }
    }

    Timer(Duration(milliseconds: 400), () {
      checkForGPS(context);
    });
  }

  showFTUEDialog() async {
    Map<String, dynamic> result = await showDialog(
      context: context,
      builder: (context) => FTUEDialog(),
    );

    if (result["allow"] == true) {
      SharedPrefHelper()
          .saveToSharedPref(SharedPrefKey.interactiveDemo, "true");
      await routeManager.launchInteractiveFTUE(context);
    }
  }

  closeFlash(_) {
    EventManager().closePushNotifFlash();
  }

  wsConnectionStatusChanged(var connected) async {
    if (!connected && !isDisconnectionPopupOpen) {
      isDisconnectionPopupOpen = true;
      await showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
      isDisconnectionPopupOpen = false;
    } else if (connected && isDisconnectionPopupOpen) {
      startTimerToCloseDisconnetion();
    }
  }

  startTimerToCloseDisconnetion() {
    new Timer(new Duration(milliseconds: 500), () {
      if (WebSocket().isConnected() && isDisconnectionPopupOpen) {
        isDisconnectionPopupOpen = false;
        Navigator.pop(context);
      }
    });
  }

  fetchMyMatchesData() {
    MyMatchesRequest myMatchedRequest = MyMatchesRequest();
    WebSocket().sendMessage(myMatchedRequest.toJson());
  }

  checkAndUpdateAccountData() async {
    InitData initData = Provider.of(context, listen: false);
    String timeStampStr = await SharedPrefHelper()
        .getFromSharedPref(SharedPrefKey.accountUpdateTimestamp);

    int timeStampMs = 0;
    if (timeStampStr != null && timeStampStr != "") {
      timeStampMs = int.parse(timeStampStr);
    }

    int currentTsInMs = DateTime.now().millisecondsSinceEpoch;

    if (currentTsInMs - timeStampMs >
        initData.experiments.miscConfig.updateAccountDurationInMs) {
      SharedPrefHelper().saveToSharedPref(
          SharedPrefKey.accountUpdateTimestamp, currentTsInMs.toString());
      updateAccountData();
    }
  }

  checkForGPS(BuildContext context) async {
    InitData initData = Provider.of(context, listen: false);
    PermissionStatus _status;
    if (await LocationAPI.getStoredLocation(initData) == null) {
      if (initData.locationStateRestrictions["askForPermissionOnLobby"] ==
          true) {
        if (Platform.isIOS) {
          MyLocationService.askForLocationPermission(context, false, "lobby");
        } else {
          LocationPermissionHandler locationPermissionHandler =
              LocationPermissionHandler();
          _status = await locationPermissionHandler.checkForPermission(
            context,
            source: "lobby",
          );

          if (_status == null ||
              _status == PermissionStatus.deny ||
              _status == PermissionStatus.notAgain) {
            GamePlayAnalytics()
                .onGeoLocationAccessGiven(context, access: "deny");
          } else {
            GamePlayAnalytics()
                .onGeoLocationAccessGiven(context, access: "allow");
          }
        }
      }
      if ((_status == PermissionStatus.allow ||
              _status == PermissionStatus.whenInUse) &&
          initData.locationStateRestrictions["turnOnGPSOnLobby"] == true) {
        await ChannelManager.GPS_STREAM_CHANNEL.invokeMethod('checkAndOnGPS');
      }
    }
  }

  addWebEngageScreenData() async {
    Map<String, dynamic> screendata = new Map();
    screendata["screenName"] = "lobby";
    MyWebEngageService().webengageAddScreenData(data: screendata);
  }

  subscribeToNativeToFlutterEventsListener(BuildContext context) {
    if (EventManager().nativeToFlutterStreamSubscription == null) {
      EventManager().nativeToFlutterStreamSubscription = ChannelManager
          .NATIVE_TO_FLUTTER_EVENT_STREAM_CHANNEL
          .receiveBroadcastStream()
          .listen((data) {
        if (data != null && !SolitaireGold.isLaunched) {
          try {
            Map<dynamic, dynamic> eventDataMap = {};
            eventDataMap = data;
            if (eventDataMap["eventName"] == "fcm_msg_recieved") {
              if (eventDataMap["data"] != null) {
                try {
                  FCMDataRoute routesData =
                      Provider.of<FCMDataRoute>(context, listen: false);
                  routesData
                      .setData(FCMDataRoute.fromJson(eventDataMap["data"]));
                  showFCMMessageToast(context);
                } catch (e) {
                  print(e);
                }
              }
            } else if (eventDataMap["eventName"] ==
                "deeplinking_data_recieved") {
              if (eventDataMap["data"] != null) {
                dynamic deepLinkingData = eventDataMap["data"];
                if (deepLinkingData["activateDeepLinkingNavigation"] != null &&
                    deepLinkingData["activateDeepLinkingNavigation"] == true) {
                  try {
                    DeepLinkRoute routesData =
                        Provider.of<DeepLinkRoute>(context, listen: false);
                    routesData.setData(DeepLinkRoute.fromJson(deepLinkingData));
                    launchDeepLinkRoute(context);
                  } catch (e) {
                    print(e);
                  }
                }
              }
            } else if (eventDataMap["eventName"] ==
                "webengageinapp_data_recieved") {
              print(eventDataMap["data"]);
            }
          } catch (e) {}
        }
      });
    }
  }

  showFCMMessageToast(BuildContext context) async {
    AppConfig config = Provider.of(context, listen: false);
    config.setbShowLeaderBoardOnResultPageOnStart(false);
    FCMDataRoute routesData = Provider.of<FCMDataRoute>(context, listen: false);
    String title = routesData.sg_title;
    int matchIdInt = routesData.sg_matchId;
    String type = routesData.sg_type;
    String body = routesData.sg_body;

    if (title != null && matchIdInt != null && type != null && body != null) {
      closeFlash(null);
      await showFlash(
        context: context,
        duration: Duration(seconds: 8),
        builder: (context, controller) {
          EventManager().pushNotifflashController = controller;
          return NotificationPopup(
            title: title,
            body: body,
            controller: controller,
            onCheck: () async {
              controller.dismiss();
              EventManager().pushNotifflashController.dismiss();
              if (_pageTabController.index == getResultPageIndex()) {
                /*When user is on Result page ,this part is called.*/
                const String TIE_BREAKER = "1";
                const String MATCH_END = "6";
                const String ABANDONED = "5";
                if (type == ABANDONED) {
                  AppConfig config = Provider.of(context, listen: false);
                  config.setbShowLeaderBoardOnResultPageOnStart(false);
                } else if (type == TIE_BREAKER || type == MATCH_END) {
                  dynamic _response = await LobbyAPI()
                      .getLeaderBoardDataWithMatchId(
                          context, routesData.sg_matchId);
                  if (_response != null &&
                      !_response["error"] &&
                      _response["data"] != null) {
                    EventManager().publish(EventName.LEADERBOARD_LOADED,
                        data: _response["data"]);
                  }
                }
              } else {
                Navigator.of(context).popUntil((r) => r.isFirst);
                onResultScreenWithLeaderBoardPopup(matchIdInt, type);
              }
            },
          );
        },
      );

      EventManager().pushNotifflashController = null;
    }
  }

  updateAccountData() async {
    try {
      Map<String, dynamic> response =
          await AccountAPI().getAccountDetails(context);

      User user = Provider.of(context, listen: false);
      if (response != null &&
          response["data"] != null &&
          response["data"]["balance"] != null) {
        if (response["data"]["balance"]["bonusAmount"] != null)
          user.userBalance.setBonusAmount(
              response["data"]["balance"]["bonusAmount"].toDouble());

        if (response["data"]["balance"]["withdrawable"] != null)
          user.userBalance.setWithdrawable(
              response["data"]["balance"]["withdrawable"].toDouble());

        if (response["data"]["balance"]["depositBucket"] != null)
          user.userBalance.setDepositBucket(
              response["data"]["balance"]["depositBucket"].toDouble());

        if (response["data"]["balance"]["lockedBonusAmount"] != null)
          user.userBalance.setLockedBonusAmount(
              response["data"]["balance"]["lockedBonusAmount"].toDouble());

        if (response["data"]["balance"]["goldBars"] != null)
          user.userBalance.setGoldBars(response["data"]["balance"]["goldBars"]);

        setState(() {});
      }
    } catch (e) {}
  }

  onResultScreenWithLeaderBoardPopup(int matchId, String type) async {
    try {
      const String TIE_BREAKER = "1";
      const String MATCH_END = "6";
      const String ABANDONED = "5";
      if (type == MATCH_END) {
        updateAccountData();
      }
      if (type == ABANDONED) {
        AppConfig config = Provider.of(context, listen: false);
        config.setbShowLeaderBoardOnResultPageOnStart(false);
        onResultScreen(getResultPageIndex());
      } else if (type == TIE_BREAKER || type == MATCH_END) {
        AppConfig config = Provider.of(context, listen: false);
        config.setbShowLeaderBoardOnResultPageOnStart(true);
        setState(() {
          _resultScreen = Result(
            showLeaderBoardOnLaunch: true,
            matchId: matchId,
          );
          onResultScreen(getResultPageIndex());
        });
      }
    } catch (e) {}
  }

  Future<void> getDeepLinkingRoutingData(BuildContext context) async {
    Map<dynamic, dynamic> deepLinkingRoutingData = new Map();
    try {
      if (Platform.isIOS) {
        final Map<String, dynamic> value = (await ChannelManager
                .FLUTTER_TO_NATIVE_CHANNEL
                .invokeMethod('_deepLinkingRoutingHandler'))
            .cast<String, dynamic>();

        if (value != null) {
          deepLinkingRoutingData = value;
          if (deepLinkingRoutingData != null) {
            DeepLinkRoute routesData =
                Provider.of<DeepLinkRoute>(context, listen: false);
            routesData.setData(DeepLinkRoute.fromJson(deepLinkingRoutingData));
            launchDeepLinkRoute(context);
          }
        }
      } else {
        final value = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
            .invokeMethod('_deepLinkingRoutingHandler')
            .timeout(Duration(seconds: 10));
        if (value != null) {
          deepLinkingRoutingData = value;
          if (deepLinkingRoutingData != null) {
            DeepLinkRoute routesData =
                Provider.of<DeepLinkRoute>(context, listen: false);
            routesData.setData(DeepLinkRoute.fromJson(deepLinkingRoutingData));
            launchDeepLinkRoute(context);
          }
        }
      }
    } catch (e) {
      print(e);
    }
  }

  launchDeepLinkRoute(BuildContext context) async {
    DeepLinkRoute routeData =
        Provider.of<DeepLinkRoute>(context, listen: false);
    if (routeData.pageRoute == null || routeData.pageRoute.isEmpty) {
      return;
    }
    switch (routeData.pageRoute.toLowerCase()) {
      case "earncash":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        DeeplinkLaunch().onRAF(context);
        Loader().showLoader(false);
        break;
      case "addcash":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        int promoamount = routeData.promoamount;
        String promocode = routeData.promocode;
        routeData.setToDefault();
        DeeplinkLaunch().onAddCash(context,
            source: "deeplink",
            prefilledAmount: promoamount,
            prefilledPromoCode: promocode);
        Loader().showLoader(false);
        break;
      case "verification":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        DeeplinkLaunch().onKYC(context);
        routeData.setToDefault();
        Loader().showLoader(false);
        break;
      case "withdraw":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        DeeplinkLaunch().onWithdraw(context, source: "deeplink");
        Loader().showLoader(false);
        break;
      case "support":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        RouteManager().launchContactUs(context);
        Loader().showLoader(false);
        break;
      case "gameresults":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        if (routeData.sg_matchId != null && routeData.sg_type != null) {
          if (_pageTabController.index == getResultPageIndex()) {
            const String TIE_BREAKER = "1";
            const String MATCH_END = "6";
            const String ABANDONED = "5";
            if (routeData.sg_type == ABANDONED) {
              AppConfig config = Provider.of(context, listen: false);
              config.setbShowLeaderBoardOnResultPageOnStart(false);
            } else if (routeData.sg_type == TIE_BREAKER ||
                routeData.sg_type == MATCH_END) {
              dynamic _response = await LobbyAPI()
                  .getLeaderBoardDataWithMatchId(context, routeData.sg_matchId);
              if (_response != null &&
                  !_response["error"] &&
                  _response["data"] != null) {
                EventManager().publish(EventName.LEADERBOARD_LOADED,
                    data: _response["data"]);
              }
            }
          } else {
            onResultScreenWithLeaderBoardPopup(
                routeData.sg_matchId, routeData.sg_type);
          }
        } else {
          onResultScreen(getResultPageIndex());
        }
        Loader().showLoader(false);
        break;
      case "promotion":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        try {
          int index = 1;
          InitData initData = Provider.of(context, listen: false);
          for (FooterConfig footer in initData.experiments.footers) {
            if (footer.name == "DEALS") {
              index = footer.index - 1;
            }
          }
          _onNavigationChange(index);
        } catch (e) {}
        Loader().showLoader(false);
        break;
      case "accounts":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        DeeplinkLaunch().onAccountSummary(context);
        Loader().showLoader(false);
        break;
      case "myprofile":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        DeeplinkLaunch().onProfile(context);
        Loader().showLoader(false);
        break;
      case "help":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        routeData.setToDefault();
        RouteManager().launchHelpCenter(context);
        Loader().showLoader(false);
        break;
      case "staticpage":
        Loader().showLoader(true, immediate: true);
        Navigator.of(context).popUntil((r) => r.isFirst);
        String pageLocation = routeData.pageLocation;
        String pageTitle = routeData.pageTitle;
        routeData.setToDefault();
        if (pageLocation != null && pageTitle != null) {
          RouteManager()
              .launchStaticPage(context, url: pageLocation, title: pageTitle);
        }
        Loader().showLoader(false);
        break;
    }
  }

  int getResultPageIndex() {
    int index = 1;
    InitData initData = Provider.of(context, listen: false);
    for (FooterConfig footer in initData.experiments.footers) {
      if (footer.name == "RESULTS") {
        index = footer.index - 1;
      }
    }
    return index;
  }

  int getLobbyPageIndex() {
    InitData initData = Provider.of(context, listen: false);
    int index = initData.experiments.getLobbyIndex();
    return index;
  }

  void connectWebsocket(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    DatabaseManager dbManager =
        Provider.of<DatabaseManager>(context, listen: false);
    dbManager.connectToServerWith(config.wsUrl);
  }

  Widget getUserBalance(String image, String balance,
      {Function onClick, Widget suffix, String symbol = ""}) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            constraints: BoxConstraints(maxWidth: 140.0),
            child: InkWell(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onTap: onClick,
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.centerLeft,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 4.0),
                    height: 23.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4.0),
                        color: Colors.black),
                    clipBehavior: Clip.hardEdge,
                    child: Stack(
                      children: [
                        Container(
                          height: 22.0,
                          margin: EdgeInsets.only(top: 1.0, right: 1.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4.0),
                            color: Color.fromRGBO(80, 5, 15, 1),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.transparent,
                              ),
                              BoxShadow(
                                color: Colors.grey[900],
                                spreadRadius: -1.0,
                                blurRadius: 18.0,
                              ),
                            ],
                          ),
                          padding: EdgeInsets.only(left: 18.0, right: 8.0),
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: 24.0, right: 18.0),
                                    child: FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 2.0),
                                            child: Text(
                                              symbol,
                                              style: Theme.of(context)
                                                  .primaryTextTheme
                                                  .headline6
                                                  .copyWith(
                                                    fontFamily: 'FagoCo-bold',
                                                    color: Colors.white,
                                                  ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          Text(
                                            balance,
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .headline6
                                                .copyWith(
                                                  fontFamily: 'FagoCo',
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white,
                                                ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: -6,
                    child: Image.asset(
                      image,
                      height: 44.0,
                    ),
                  ),
                  if (suffix != null) Positioned(right: 2, child: suffix)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  _onNavigationChange(int index) async {
    checkAndUpdateAccountData();
    isAddCash = false;
    InitData initData = Provider.of(context, listen: false);
    AnalyticsManager().setJourney(null);
    for (FooterConfig footer in initData.experiments.footers) {
      if (footer.index - 1 == index) {
        switch (footer.name) {
          case "ADDCASH":
            bool success = await onAddCash();
            if (!success) {
              Loader().showLoader(false);
              return;
            }
            break;
          case "RESULTS":
            onResultScreen(index);
            break;
          case "DEALS":
            requestBanners(context);
            break;
          default:
            _pageTabController.animateTo(index);
        }
        _pageTabController.animateTo(index);
      }
    }
  }

  Future<bool> onAddCash() async {
    if (!WebSocket().isConnected()) {
      await showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );

      return false;
    }
    AnalyticsManager().setJourney("Deposit");
    GamePlayAnalytics().onAddCashClicked(context, source: "header");
    Map<String, dynamic> result = await AddCashHelper().authAddcash(context);
    if (result == null || result["error"] == null) return false;
    if (result["error"] == true) {
      if (result["message"] != null) {
        await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) => GenericDialog(
            contentText: Text(
              "${result["message"]}",
              textAlign: TextAlign.center,
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                    color: Colors.white,
                  ),
            ),
            buttonText: "Ok".toUpperCase(),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        );
      }
      return false;
    } else {
      Map<String, dynamic> arguments = result["data"];
      InitData initData = Provider.of(context, listen: false);
      isAddCash = true;
      setState(() {
        _addCash = AddCash(
          source: "Lobby",
          promoCodes: arguments["promoCodes"],
          paymentConfig: arguments["paymentConfig"],
          depositData: Deposit(
            paymentDetails: PaymentDetails.fromJson(arguments["paymentData"]),
            bAllowRepeatDeposit: true,
            showLockedAmount: true,
          ),
          paymentComplete: (payResponse) {
            _pageTabController.index = initData.experiments.getLobbyIndex();
            onPaymentSuccess(payResponse);
          },
          availableHeight: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top -
              MediaQuery.of(context).padding.bottom -
              kToolbarHeight -
              70.0,
        );
      });

      Future.delayed(Duration(milliseconds: 100), () {
        _pageTabController.index = initData.experiments.getAddCashIndex();
      });
      return true;
    }
  }

  onResultScreen(int index) {
    fetchMyMatchesData();
    Future.delayed(Duration(milliseconds: 100), () {
      _pageTabController.index = index;
    });
  }

  onPaymentSuccess(Map<String, dynamic> payload) async {
    User user = Provider.of(context, listen: false);
    isAddCash = false;
    user.userBalance.setBonusAmount(payload["bonusAmount"].toDouble());
    user.userBalance.setWithdrawable(payload["withdrawable"].toDouble());
    user.userBalance.setDepositBucket(payload["depositBucket"].toDouble());

    await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      builder: (BuildContext context) {
        return TransactionSuccess(payload, 0, 0);
      },
    );
  }

  void requestBanners(BuildContext context) async {
    BannerList carousel = Provider.of<BannerList>(context, listen: false);

    Map<String, dynamic> response =
        await _bannerAPI.getBanners(context, zone: BannerZone.DEALS);
    if (response["error"] == false) {
      carousel.setItems(BannerZone.DEALS, response["data"]);
    }
  }

  void onBannerClick(Map<String, dynamic> _banner) {
    switch (_banner["CTA"]) {
      case DeeplinkCTA.DEPOSIT:
        onAddCash();
        break;
    }
  }

  Future<bool> _onWillPop() {
    InitData initData = Provider.of(context, listen: false);
    if (_pageTabController.index != initData.experiments.getLobbyIndex()) {
      _pageTabController.index = initData.experiments.getLobbyIndex();
      isAddCash = false;
      return Future.value(false);
    }

    return showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      barrierDismissible: false,
      builder: (context) => CloseDialog(),
    );
  }

  List<String> getBottomNavigationItems() {
    InitData initData = Provider.of(context, listen: false);
    List<String> tabItems = [];

    for (int i = 0; i <= 5; i++) {
      for (FooterConfig footer in initData.experiments.footers) {
        if (footer.index == i) {
          switch (footer.name) {
            case "ADDCASH":
              tabItems.add("ADD CASH");
              break;
            case "RESULTS":
              tabItems.add("RESULTS");
              break;
            case "LOBBY":
              tabItems.add("LOBBY");
              break;
            case "DEALS":
              tabItems.add("DEALS");
              break;
            case "ACCOUNT":
              tabItems.add("MENU");
              break;
          }
        }
      }
    }
    return tabItems;
  }

  List<Widget> getTabViewFromFooterConfig() {
    InitData initData = Provider.of(context, listen: false);
    List<Widget> tabViewPages = [];
    for (int i = 0; i < 10; i++) {
      for (FooterConfig footer in initData.experiments.footers) {
        if (footer.index == i) {
          switch (footer.name) {
            case "ADDCASH":
              tabViewPages.add(_addCash);
              break;
            case "RESULTS":
              tabViewPages.add(_resultScreen);
              break;
            case "LOBBY":
              tabViewPages.add(Home(onBannerClick: onBannerClick));
              break;
            case "DEALS":
              tabViewPages.add(Deals(onBannerClick: onBannerClick));
              break;
            case "ACCOUNT":
              tabViewPages.add(Account());
              break;
          }
        }
      }
    }

    return tabViewPages;
  }

  @override
  void dispose() {
    _solitaireSubscription.cancel();
    closeFlash(null);
    EventManager().cancleNativeToFlutterStreamSubscriptionEvent();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    return WillPopScope(
      onWillPop: _onWillPop,
      child: LoaderContainer(
        child: Stack(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/lobby/background.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Scaffold(
              backgroundColor: Colors.transparent,
              key: scaffoldKey,
              appBar: LobbyAppBar(
                children: <Widget>[
                  Consumer<User>(
                    builder: (context, user, child) {
                      return Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            getUserBalance(
                              "images/currency/diamond.png",
                              user.userBalance.goldBars.toString(),
                            ),
                            getUserBalance(
                              config.channelId < 200
                                  ? "images/currency/cash.png"
                                  : "images/currency/cash-dollar.png",
                              user.userBalance
                                  .getTotalBalance()
                                  .toStringAsFixed(2)
                                  .replaceAll(".00", ""),
                              symbol:
                                  CurrencyFormat.format(0).replaceAll("0", ""),
                              suffix: Container(
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                  color: Colors.green.shade800,
                                  borderRadius: BorderRadius.circular(4.0),
                                ),
                                child: Center(
                                  child: Container(
                                    child: Text(
                                      "+",
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6
                                          .copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w700,
                                          ),
                                    ),
                                  ),
                                ),
                              ),
                              onClick: () {
                                if (!isAddCash) {
                                  onAddCash();
                                }
                              },
                            ),
                          ],
                        ),
                      );
                    },
                  )
                ],
                automaticallyImplyLeading: false,
              ),
              body: Column(
                children: <Widget>[
                  Expanded(
                    child: TabBarView(
                      controller: _pageTabController,
                      physics: NeverScrollableScrollPhysics(),
                      children: getTabViewFromFooterConfig(),
                    ),
                  ),
                ],
              ),
              resizeToAvoidBottomInset: false,
              bottomNavigationBar: BottomNavigationPanel(
                onItemSelect: (int index) {
                  if (!_pageTabController.indexIsChanging) {
                    _onNavigationChange(index);
                    LobbyAnalytics().onLobbyFooterItemClicked(
                      context,
                      source: "footer",
                      index: index,
                    );
                  }
                },
                items: getBottomNavigationItems(),
                selectedIndex: _pageTabController.index,
              ),
              // floatingActionButton: FloatingActionButton(
              //   child: Icon(Icons.broken_image),
              //   onPressed: () {
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //         builder: (context) => CrashTest(),
              //       ),
              //     );
              //   },
              // ),
            ),
          ],
        ),
      ),
    );
  }
}
