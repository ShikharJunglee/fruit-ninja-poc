import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/models/dataloader.dart';
import 'package:solitaire_gold/screens/solitairegold/solitairegold.dart';
import 'package:solitaire_gold/services/myhelperclass.dart';

class NotificationPopup extends StatefulWidget {
  final String title;
  final String body;
  final Function onCheck;
  final FlashController<dynamic> controller;

  NotificationPopup({this.controller, this.title, this.body, this.onCheck});

  @override
  _NotificationPopupState createState() => _NotificationPopupState();
}

class _NotificationPopupState extends State<NotificationPopup> {
  bool isChecked = false;

  onCheck() {
    if (widget.onCheck != null && !isChecked) {
      isChecked = true;
      widget.onCheck();
    }
  }

  @override
  Widget build(BuildContext context) {
    DataLoader loader = Provider.of<DataLoader>(context, listen: false);

    return ChangeNotifierProvider.value(
      value: loader,
      child: Consumer<DataLoader>(
        builder: (context, loader, child) {
          return Flash(
            controller: widget.controller,
            style: FlashStyle.floating,
            position: FlashPosition.top,
            boxShadows: kElevationToShadow[4],
            horizontalDismissDirection: HorizontalDismissDirection.horizontal,
            backgroundColor: Colors.transparent,
            child: Container(
              width: MyHelperClass.getScreenWidth(context) * 0.95,
              height: 80,
              child: Card(
                color: Color.fromRGBO(255, 255, 255, 1),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                child: FlatButton(
                  onPressed: () {
                    if (!loader.isLoading && !SolitaireGold.isLaunched)
                      onCheck();
                  },
                  child: Row(
                    children: [
                      Expanded(
                          flex: 2,
                          child: Image.asset(
                            "images/logo.png",
                            height: 30.0,
                            width: 30.0,
                          )),
                      Expanded(
                          flex: 6,
                          child: Padding(
                              padding: const EdgeInsets.only(top: 9.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.title,
                                    textAlign: TextAlign.start,
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16.0,
                                          decorationStyle:
                                              TextDecorationStyle.solid,
                                        ),
                                  ),
                                  Text(
                                    widget.body,
                                    textAlign: TextAlign.start,
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Colors.black,
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.normal,
                                          decorationStyle:
                                              TextDecorationStyle.solid,
                                        ),
                                  )
                                ],
                              ))),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 12.0),
                          child: FlatButton(
                            color: Colors.transparent,
                            textColor: Color.fromRGBO(227, 153, 29, 1),
                            padding: EdgeInsets.all(8.0),
                            onPressed: () async {
                              if (!loader.isLoading &&
                                  !SolitaireGold.isLaunched) onCheck();
                            },
                            child: Text(
                              "Check",
                              style: TextStyle(fontSize: 14.0),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
