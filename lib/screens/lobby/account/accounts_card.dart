import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AccountsCard extends StatelessWidget {
  final Map<String, dynamic> item;
  final Function onTap;
  final bool showElevation;

  AccountsCard(
      {this.showElevation = false, @required this.item, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Card(
      clipBehavior: Clip.hardEdge,
      elevation: showElevation ? 4.0 : 0.0,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          width: 2.0,
          color: showElevation
              ? Colors.transparent
              : Color.fromRGBO(193, 162, 132, 1),
        ),
        borderRadius: BorderRadius.circular(showElevation ? 8.0 : 12.0),
      ),
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 52.0,
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: item["icon"].toString().startsWith("http")
                    ? CachedNetworkImage(
                        imageUrl: item["icon"],
                        placeholder: (context, string) {
                          return Container(
                            padding: EdgeInsets.all(4.0),
                            child: CircularProgressIndicator(
                              strokeWidth: 2.0,
                            ),
                            width: 24,
                            height: 24,
                          );
                        },
                        width: 24,
                      )
                    : Image.asset(
                        item["icon"],
                        width: 24.0,
                      ),
              ),
              Text(
                item["title"].toUpperCase(),
                style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
