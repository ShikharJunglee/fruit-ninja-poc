import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/commonwidgets/gradienttext.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/generic_dialog.dart';
import 'package:solitaire_gold/popups/scoring_system.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/lobby/account/accounts_card.dart';
import 'package:solitaire_gold/screens/lobby/addcash/transactionsuccess.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/deeplink_cta.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/models/initdata.dart';

class Account extends StatefulWidget {
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> with BasePage {
  List<dynamic> _items = List();
  @override
  void initState() {
    super.initState();
    InitData initData = Provider.of(context, listen: false);
    User user = Provider.of(context, listen: false);

    _items = List();
    initData.experiments.menuItems.forEach((element) {
      if (element.visibilityMod == null ||
          element.visibilityRemainders
              .contains(user.id % element.visibilityMod)) {
        _items.add(element.toJson());
      }
    });
    LobbyAnalytics().onLobbyAccountLoaded(context, source: "footer");
  }

  getGradientText(BuildContext context, String text) {
    return GradientText(
      text,
      alignment: TextAlign.center,
      style: Theme.of(context).primaryTextTheme.headline5.copyWith(
            fontWeight: FontWeight.w600,
            color: Colors.white,
            // shadows: [
            //   Shadow(
            //     blurRadius: 0.0,
            //     color: Colors.grey.shade700,
            //     offset: Offset(0.0, 3.0),
            //   ),
            // ],
          ),
      gradient: LinearGradient(
        colors: [
          Colors.yellow,
          Color.fromRGBO(253, 239, 171, 1),
          Color.fromRGBO(231, 181, 25, 1),
          Colors.red,
        ],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    );
  }

  void onItemClicked(String cta) async {
    LobbyAnalytics().onMenuItemClicked(
      context,
      source: "app_drawer",
      title: cta.toLowerCase(),
    );
    switch (cta) {
      case DeeplinkCTA.DEPOSIT:
        Map<String, dynamic> result =
            await DeeplinkLaunch().onAddCash(context, source: "app_drawer");
        if (result != null) onPaymentSuccess(result);
        break;
      case DeeplinkCTA.PROFILE:
        DeeplinkLaunch().onProfile(context);
        break;
      case DeeplinkCTA.KYC:
        DeeplinkLaunch().onKYC(context);
        break;
      case DeeplinkCTA.ACCOUNT:
        DeeplinkLaunch().onAccountSummary(context);
        break;
      case DeeplinkCTA.WITHDRAW:
        DeeplinkLaunch().onWithdraw(
          context,
          source: "app_drawer",
          onError: (msg) {
            showDialog(
                barrierColor: Color.fromARGB(175, 0, 0, 0),
                context: context,
                builder: (context) {
                  return GenericDialog(
                    contentText: Text(
                      msg,
                      textAlign: TextAlign.center,
                      style:
                          Theme.of(context).primaryTextTheme.headline4.copyWith(
                                color: Colors.white,
                              ),
                    ),
                    buttonText: "Ok".toUpperCase(),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  );
                });
          },
        );
        break;
      case DeeplinkCTA.RAF:
        DeeplinkLaunch().onRAF(context);
        break;
      case DeeplinkCTA.HOW_TO_PLAY:
        DeeplinkLaunch().onFTUE(context);
        break;
      case DeeplinkCTA.SCORING:
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return ScoringSystem();
          },
        );
        break;
      case DeeplinkCTA.HELP:
        routeManager.launchHelpCenter(context);
        break;
      case DeeplinkCTA.LOGOUT:
        DeeplinkLaunch().onLogout(context);
        break;
      case DeeplinkCTA.CHARGE_BACK:
        DeeplinkLaunch().onChargeBack(context);
        break;
    }
  }

  onPaymentSuccess(Map<String, dynamic> payload) async {
    User user = Provider.of(context, listen: false);

    user.userBalance.setBonusAmount(payload["bonusAmount"].toDouble());
    user.userBalance.setWithdrawable(payload["withdrawable"].toDouble());
    user.userBalance.setDepositBucket(payload["depositBucket"].toDouble());

    return await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      builder: (BuildContext context) {
        return TransactionSuccess(
            payload, payload["bonusAmount"], payload["goldCount"]);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);
    return Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            Positioned(
              left: 0.0,
              right: 0.0,
              bottom: 0.0,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Image.asset("images/header.png"),
                  ),
                ],
              ),
            ),
            Consumer<User>(
              builder: (context, user, child) {
                return Padding(
                  padding: const EdgeInsets.only(top: 16.0, bottom: 24.0),
                  child: Row(
                    children: <Widget>[
                      SizedBox(
                        width: 16.0,
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            onItemClicked(DeeplinkCTA.DEPOSIT);
                          },
                          child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Image.asset("images/stats_rectangle.png"),
                              FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Deposit",
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6,
                                    ),
                                    getGradientText(
                                      context,
                                      CurrencyFormat.format(
                                        user.userBalance.depositBucket,
                                        decimalDigits: initData.experiments
                                            .miscConfig.amountPrecision,
                                      ).replaceAll(",", ""),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 16.0,
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            onItemClicked(DeeplinkCTA.WITHDRAW);
                          },
                          child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Image.asset("images/stats_rectangle.png"),
                              FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Winnings",
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6,
                                    ),
                                    getGradientText(
                                      context,
                                      CurrencyFormat.format(
                                        user.userBalance.withdrawable,
                                        decimalDigits: initData.experiments
                                            .miscConfig.amountPrecision,
                                      ).replaceAll(",", ""),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 16.0,
                      ),
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            onItemClicked(DeeplinkCTA.RAF);
                          },
                          child: Stack(
                            alignment: Alignment.center,
                            children: <Widget>[
                              Image.asset("images/stats_rectangle.png"),
                              FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Column(
                                  children: <Widget>[
                                    Text(
                                      "Bonus",
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6,
                                    ),
                                    getGradientText(
                                      context,
                                      CurrencyFormat.format(
                                        user.userBalance.bonusAmount,
                                        decimalDigits: initData.experiments
                                            .miscConfig.amountPrecision,
                                      ).replaceAll(",", ""),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 16.0,
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
            child: ListView.builder(
              itemCount: _items.length,
              itemBuilder: (context, index) => AccountsCard(
                item: _items[index],
                onTap: () {
                  onItemClicked(_items[index]["cta"]);
                },
              ),
            ),
          ),
        ),
      ],
    );
  }
}
