import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class CloseDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      dialog: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4.0),
        ),
        backgroundColor: Colors.transparent,
        child: CommonDialogFrame(
          titleAsset: "images/titles/exit-game.png",
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 42.0, left: 36.0, right: 36.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "Do you want to exit?",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline4
                            .copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 32.0, bottom: 16.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 8.0),
                        child: Button(
                          text: "YES".toUpperCase(),
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline5
                              .copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                          size: ButtonSize.medium,
                          onPressed: () {
                            while (Navigator.canPop(context)) {
                              Navigator.of(context).pop();
                            }
                            SystemNavigator.pop();
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 16.0),
                        child: Button(
                          text: "NO".toUpperCase(),
                          size: ButtonSize.medium,
                          type: ButtonType.secondary,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline5
                              .copyWith(
                                color: Colors.brown.shade800,
                                fontWeight: FontWeight.bold,
                              ),
                          onPressed: () {
                            Navigator.of(context).pop(false);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
