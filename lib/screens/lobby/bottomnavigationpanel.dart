import 'dart:io';
import 'package:flutter/material.dart';

class BottomNavigationPanel extends StatelessWidget {
  final List<dynamic> drawerItems;
  final List<String> items;
  final Function onItemSelect;
  final int selectedIndex;

  BottomNavigationPanel({
    this.drawerItems,
    @required this.items,
    this.selectedIndex,
    @required this.onItemSelect,
  });

  List<BottomNavigationBarItem> getNavigationItems(
      BuildContext context, bool showNotification) {
    double iconHeight = 32.0;
    List<BottomNavigationBarItem> bottomItems = [];

    items.forEach((item) {
      bottomItems.add(
        BottomNavigationBarItem(
          backgroundColor: Colors.transparent,
          icon: Stack(
            children: <Widget>[
              Container(
                child: Image.asset(
                  "images/bottombar/" +
                      item
                          .toLowerCase()
                          .replaceAll(" & ", " ")
                          .replaceAll(" ", "-") +
                      ".png",
                  height: iconHeight,
                ),
              ),
            ],
          ),
          activeIcon: Image.asset(
            "images/bottombar/" +
                item.toLowerCase().replaceAll(" & ", " ").replaceAll(" ", "-") +
                "-select.png",
            height: iconHeight,
            fit: BoxFit.contain,
          ),
          title: Padding(
            padding: const EdgeInsets.only(top: 2.0),
            child: Text(
              item,
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .primaryTextTheme
                  .subtitle1
                  .copyWith(color: Colors.white, fontWeight: FontWeight.bold),
              maxLines: 2,
            ),
          ),
        ),
      );
    });

    return bottomItems;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(84, 2, 19, 1),
      padding: EdgeInsets.only(top: 4.0),
      height: 70.0,
      child: BottomNavigationBar(
        unselectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w600,
          color: Colors.white,
          fontSize: Platform.isIOS ? 12 : 16,
        ),
        selectedLabelStyle: TextStyle(
          fontWeight: FontWeight.w600,
          color: Colors.white,
          fontSize: Platform.isIOS ? 12 : 16,
        ),
        backgroundColor: Color.fromRGBO(121, 3, 20, 1),
        currentIndex: selectedIndex,
        type: BottomNavigationBarType.fixed,
        items: getNavigationItems(context, false),
        onTap: (int index) {
          onItemSelect(index);
        },
      ),
    );
  }
}
