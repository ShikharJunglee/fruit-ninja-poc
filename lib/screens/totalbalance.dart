import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class TotalBalance extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);
    User user = Provider.of(context, listen: false);
    return CustomDialog(
      padding: EdgeInsets.symmetric(horizontal: 16),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: Center(
          child: Container(
            padding: EdgeInsets.only(left: 12.0, right: 12.0, bottom: 12.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(4.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Stack(
                  children: [
                    Align(
                      alignment: Alignment(1.1, 0),
                      child: IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(
                            flex: 14,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: Text(
                                "Total Balance " +
                                    CurrencyFormat.format(
                                        user.userBalance.getTotalBalance(),
                                        decimalDigits: initData.experiments
                                            .miscConfig.amountPrecision,
                                        showDelimeters: false),
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .subtitle1
                                    .copyWith(
                                      color: Colors.black,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Column(
                          children: [
                            Text(
                              "Deposit",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade700,
                                    fontSize: 22.0,
                                  ),
                            ),
                            Text(
                              CurrencyFormat.format(
                                  user.userBalance.depositBucket,
                                  decimalDigits: initData
                                      .experiments.miscConfig.amountPrecision,
                                  showDelimeters: false),
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade700,
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Center(
                            child: Container(
                              width: 1,
                              height: 50,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Column(
                          children: [
                            Text(
                              "Winnings",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade700,
                                    fontSize: 22.0,
                                  ),
                            ),
                            Text(
                              CurrencyFormat.format(
                                  user.userBalance.withdrawable,
                                  decimalDigits: initData
                                      .experiments.miscConfig.amountPrecision,
                                  showDelimeters: false),
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade700,
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: Center(
                            child: Container(
                              width: 1,
                              height: 50,
                              decoration: BoxDecoration(
                                color: Colors.grey[200],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Column(
                          children: [
                            Text(
                              "Bonus",
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade700,
                                    fontSize: 22.0,
                                  ),
                            ),
                            Text(
                              CurrencyFormat.format(
                                  user.userBalance.bonusAmount,
                                  decimalDigits: initData
                                      .experiments.miscConfig.amountPrecision,
                                  showDelimeters: false),
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade700,
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
