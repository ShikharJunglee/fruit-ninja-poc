import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/api/user/profileAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/commonwidgets/overlaymanager.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/screens/profile/widgets/statelist.dart';
import 'package:solitaire_gold/utils/loader.dart';

class WithdrawAddress extends StatefulWidget {
  final String source;

  WithdrawAddress({this.source});

  @override
  WithdrawAddressState createState() => WithdrawAddressState();
}

class WithdrawAddressState extends State<WithdrawAddress> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();

  ProfileAPI _profileAPI = ProfileAPI();

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController stateController = TextEditingController();
  TextEditingController zipcodeController = TextEditingController();
  OverlayManager _overlayManager = OverlayManager();
  List<dynamic> states = List();
  Map<String, dynamic> selectedState;
  GlobalKey _key = GlobalKey();
  bool showStateError = false;

  Map<String, dynamic> getUserProfileObject() {
    User user = Provider.of(context, listen: false);
    Map<String, dynamic> payload = {
      "address": {
        if (user.address.firstName != firstNameController.text)
          "first_name": firstNameController.text,
        if (user.address.lastName != lastNameController.text)
          "last_name": lastNameController.text,
        if (user.address.address1 != addressController.text)
          "add_line_1": addressController.text,
        if (user.address.city != cityController.text)
          "city": cityController.text,
        if (user.address.pincode.toString() != zipcodeController.text)
          "pin": zipcodeController.text,
      },
      "info": {
        if (user.address.state != stateController.text)
          "state": selectedState["code"],
      },
    };
    return payload;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  void setData(BuildContext context) async {
    User user = Provider.of(context, listen: false);
    if (user.address.firstName != null) {
      firstNameController.text = user.address.firstName;
    }
    if (user.address.lastName != null) {
      lastNameController.text = user.address.lastName;
    }
    if (user.address.address1 != null) {
      addressController.text = user.address.address1;
    }
    if (user.address.city != null) {
      cityController.text = user.address.city;
    }
    if (user.address.pincode != null) {
      zipcodeController.text = user.address.pincode.toString();
    }
    await getStates(context);
    Loader().showLoader(false);
    setState(() {});
  }

  getStateFromCode(String code) {
    Map<String, dynamic> state =
        states.firstWhere((state) => state["code"] == code, orElse: () => null);
    if (state != null) return state;
    return null;
  }

  getStates(BuildContext context) async {
    User user = Provider.of(context, listen: false);

    Map<String, dynamic> result = await _profileAPI.getStateList(context);
    if (result["error"] == false) {
      states = result["data"];
      selectedState = user.address.state == null
          ? null
          : getStateFromCode(user.address.state);
      stateController.text =
          selectedState != null ? selectedState["value"] : "";
    }
  }

  showStateSelection() {
    FocusScope.of(context).unfocus();
    double height = 400.0;

    int delay = 0;
    if (FocusScope.of(context).focusedChild != null) {
      delay = 50;
    }

    Future.delayed(Duration(milliseconds: delay), () {
      double width = MediaQuery.of(context).size.width - 140;
      Offset offset = Offset(MediaQuery.of(context).size.width / 2 - width / 2,
          MediaQuery.of(context).size.height / 2 - height / 2);

      _overlayManager.showOverlay(
        context,
        offset,
        Padding(
          padding: const EdgeInsets.only(right: 20.0),
          child: Container(
            width: width,
            height: height,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(6.0),
            ),
            clipBehavior: Clip.hardEdge,
            child: StateList(
              stateData: states,
              onSelect: (Map<String, dynamic> state) {
                _overlayManager.closeAll();
                setState(() {
                  selectedState = state;
                });
              },
            ),
          ),
        ),
      );
    });
  }

  Widget getListItemTextBox({
    String label,
    String placeholder,
    TextEditingController controller,
    TextInputType keyboardType = TextInputType.text,
    int maxLength,
    Widget suffixIcon,
    BoxConstraints suffixIconConstraints,
    bool enabled = true,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 120.0,
            height: 32.0,
            alignment: Alignment.centerLeft,
            child: Text(
              label,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.grey.shade700,
                    fontWeight: FontWeight.w600,
                  ),
            ),
          ),
          Expanded(
            child: SimpleTextBox(
              controller: controller,
              keyboardType: keyboardType,
              suffixIcon: suffixIcon,
              suffixIconConstraints: BoxConstraints(maxHeight: 20.0),
              labelText: placeholder != null ? placeholder : label,
              enabled: enabled,
              contentPadding: EdgeInsets.symmetric(
                horizontal: 16.0,
                vertical: 8.0,
              ),
              borderColor: Colors.grey.shade500,
              focusedBorderColor: Colors.grey.shade700,
              isDense: true,
              validator: (value) {
                if (value.isEmpty) {
                  return "*Required";
                } else if (maxLength != null && value.length > maxLength) {
                  return "Maximum " +
                      maxLength.toString() +
                      " characters allowed";
                }
                return null;
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget getDropdown({String label, bool enabled, String placeholder}) {
    Widget dropdown = Padding(
      padding: const EdgeInsets.symmetric(vertical: 0.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          showStateSelection();
                        },
                        child: Container(
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Flexible(
                                child: Text(
                                  selectedState != null
                                      ? selectedState["value"]
                                      : placeholder,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .headline6
                                      .copyWith(
                                        color: Colors.grey.shade700,
                                        fontWeight: FontWeight.w400,
                                      ),
                                ),
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                color: enabled
                                    ? Colors.black
                                    : Colors.grey.shade300,
                                key: _key,
                              ),
                            ],
                          ),
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 3.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4.0),
                            border: Border.all(
                              width: 1.0,
                              color: enabled
                                  ? Colors.grey.shade500
                                  : Colors.grey.shade300,
                            ),
                            color: Colors.white,
                          ),
                        ),
                      ),
                      showStateError
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "  *Required",
                                style: TextStyle(
                                    color: Theme.of(context).errorColor,
                                    fontSize: 12),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    return dropdown;
  }

  getStateNameFromCode(String code) {
    Map<String, dynamic> state = getStateFromCode(code);
    if (state != null) return state["value"];
    return "";
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of(context, listen: false);
    return LoaderContainer(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CommonAppBar(
          children: [
            Text(
              "Address Info".toUpperCase(),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              children: [
                Row(
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 30, bottom: 12),
                      child: Text(
                        "Address",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 18.0,
                            ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SimpleTextBox(
                    hintText: "First Name",
                    labelText: "First Name",
                    controller: firstNameController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Required";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SimpleTextBox(
                    hintText: "Last Name",
                    labelText: "Last Name",
                    controller: lastNameController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Required";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SimpleTextBox(
                    hintText: "Address",
                    labelText: "Address",
                    controller: addressController,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Required";
                      }
                      return null;
                    },
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SimpleTextBox(
                          hintText: "City Name",
                          labelText: "City Name",
                          controller: cityController,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Required";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: getDropdown(
                          label: "",
                          placeholder: user.address.state == null
                              ? "State"
                              : getStateNameFromCode(user.address.state),
                          enabled: user.address.state == null,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SimpleTextBox(
                          hintText: "Zipcode",
                          labelText: "Zipcode",
                          controller: zipcodeController,
                          maxLength: 5,
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Required";
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Button(
                      text: "update".toUpperCase(),
                      onPressed: () async {
                        if (formKey.currentState.validate()) {
                          GamePlayAnalytics().onWithdrawUpdateAddressClicked(
                              context,
                              source: "withdraw_page");
                          Loader().showLoader(true, immediate: true);
                          Map<String, dynamic> body = getUserProfileObject();
                          Map<String, dynamic> result =
                              await _profileAPI.updateProfile(context, body);

                          Loader().showLoader(false);
                          if (!result["error"]) {
                            User user = Provider.of(
                              context,
                              listen: false,
                            );
                            user.copyFrom(result["data"]);
                            Navigator.of(context).pop(result["data"]);
                          }
                        }
                      }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
