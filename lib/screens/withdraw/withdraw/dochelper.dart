class DocVerificationMessages {
  static const DOC_SUBMITTED =
      "Documents have been uploaded successfully. The status will be updated within 24-48 hours.";
  static const VERIFIED = "You can now make cash withdrawal requests.";
}

class PanVerificationMessages {
  static const UNDER_REVIEW =
      "PAN card has been declined. Please send your clear and valid image of your PAN at support@mailssolitairegold.in.";
  static const DOC_REJECTED =
      "Your PAN verification request has been rejected.";
  static const VERIFIED = "Your PAN has been successfully verified.";
  static const MAX_ATTEMPTS =
      "You have exceeded maximum number of attempts to upload PAN documents";
}

class AddressVerificationMessages {
  static const UNDER_REVIEW =
      "Address proof has been declined. Please send your complete address proof(front/back copy) at support@mailssolitairegold.in.";
  static const DOC_REJECTED =
      "Your address verification request has been rejected.";
  static const VERIFIED = "Your KYC has been successfully verified.";
  static const MAX_ATTEMPTS =
      "You have exceeded maximum number of attempts to upload KYC documents";
}
