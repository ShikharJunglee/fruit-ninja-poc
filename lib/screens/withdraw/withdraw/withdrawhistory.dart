import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/userbalance.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/withdraw/payoutstatuspopup.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class WithdrawHistory extends StatefulWidget {
  final String source;
  WithdrawHistory({@required this.source});

  @override
  WithdrawHistoryState createState() => WithdrawHistoryState();
}

class WithdrawHistoryState extends State<WithdrawHistory> {
  String cookie = "";
  List<dynamic> recents;

  @override
  void initState() {
    super.initState();
    GamePlayAnalytics().onWithdrawHistoryLoaded(
      context,
      source: widget.source,
    );
    WidgetsBinding.instance.addPostFrameCallback((_) => getRecentWithdraws());
  }

  getRecentWithdraws() async {
    Loader().showLoader(true);
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.WITHDRAW_HISTORY.toString(),
      ),
    );
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);

        if (response["type"] == "success") {
          setState(() {
            recents = response["data"];
          });
        } else {
          setState(() {
            recents = [];
          });
        }
      },
    ).whenComplete(() {
      Loader().showLoader(false);
    });
  }

  onCancelTransaction(Map<String, dynamic> transaction) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    User user = Provider.of(context, listen: false);
    UserBalance userBalance = user.userBalance;

    http.Request req = http.Request(
        "POST",
        Uri.parse(config.apiUrl +
            AppUrl.CANCEL_WITHDRAW.toString() +
            transaction["id"].toString()));
    req.body = json.encode({});
    await HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          Map<String, dynamic> cancelledTransaction = response["data"];
          double totalCancelledWithdrawAmount = 0;
          recents.forEach((transaction) {
            if (transaction["id"] == cancelledTransaction["id"]) {
              setState(() {
                transaction["status"] = cancelledTransaction["status"];
              });
              totalCancelledWithdrawAmount +=
                  (transaction["amount"] + transaction["processingFee"]);
            }
          });
          userBalance.updateWithdrawable(
              userBalance.withdrawable + totalCancelledWithdrawAmount);
        }
      },
    );
  }

  onClaimTransaction(Map<String, dynamic> transaction) async {
    await routeManager.launchPayoutMode(
      context,
      source: widget.source,
      withdrawRequest: transaction,
    );

    showPayoutStatus(transaction);
  }

  showPayoutStatus(Map<String, dynamic> payoutTransaction) async {
    getRecentWithdraws();

    // await showDialog(
    //   barrierColor: Color.fromARGB(175, 0, 0, 0),
    //   context: context,
    //   barrierDismissible: true,
    //   builder: (payoutContext) {
    //     return TsevoPayoutStatusPopup(payoutTransaction: payoutTransaction);
    //   },
    // );
  }

  @override
  Widget build(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    InitData initData = Provider.of(context, listen: false);
    return LoaderContainer(
      child: Scaffold(
        appBar: CommonAppBar(
          children: [
            Text(
              "Recent withdrawals".toUpperCase(),
            ),
          ],
        ),
        body: recents == null || recents.length == 0
            ? Center(
                child: Text(
                  recents == null ? "Loading..." : "No recent transactions.",
                  style: Theme.of(context).primaryTextTheme.headline5.copyWith(
                        color: Colors.red,
                      ),
                ),
              )
            : ListView(
                children: recents.map((recentWithdraw) {
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: ListTile(
                      onTap: () {},
                      leading: CircleAvatar(
                        maxRadius: 28.0,
                        backgroundColor: Colors.blue.shade100,
                        child: CurrencyText(
                          showCurrencyDelimiters: false,
                          amount: double.tryParse(
                              recentWithdraw["amount"].toString()),
                          decimalDigits:
                              initData.experiments.miscConfig.amountPrecision,
                          style: TextStyle(
                            color: Colors.blue.shade900.withOpacity(0.6),
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      title: Row(
                        children: [
                          Text(recentWithdraw["status"]),
                          Text(
                            recentWithdraw["processingFee"] > 0
                                ? (config.channelId > 200
                                        ? "  (+ \$"
                                        : "    (+ \u20b9") +
                                    "${recentWithdraw["processingFee"]} Fee)"
                                : "",
                            style: TextStyle(
                                color: Colors.grey[600],
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(recentWithdraw["createdDate"]),
                          Text("#" + recentWithdraw["id"].toString()),
                        ],
                      ),
                      isThreeLine: true,
                      trailing: recentWithdraw["status"] == "REQUESTED"
                          ? RaisedButton(
                              onPressed: () {
                                onCancelTransaction(recentWithdraw);
                              },
                              child: Text(
                                "CANCEL".toUpperCase(),
                              ),
                              textColor: Colors.white70,
                              color: Theme.of(context).primaryColorDark,
                            )
                          : (recentWithdraw["status"] == "APPROVED" &&
                                  recentWithdraw["type"] == 202
                              ? RaisedButton(
                                  onPressed: () {
                                    onClaimTransaction(recentWithdraw);
                                  },
                                  child: Text(
                                    "CLAIM".toUpperCase(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.w900,
                                        fontSize: Theme.of(context)
                                            .primaryTextTheme
                                            .subtitle1
                                            .fontSize),
                                  ),
                                  textColor: Colors.white70,
                                  color: Colors.green.shade500,
                                )
                              : Container(
                                  width: 0,
                                )),
                    ),
                  );
                }).toList(),
              ),
      ),
    );
  }
}
