import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/userbalance.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class WithdrawDetail extends StatefulWidget {
  final bool bankDetailsRequired;
  final bool bIsVPAIDRequired;
  final String firstName;
  final String lastName;
  final String ifscCode;
  final int minWithdraw;
  final int maxWithdraw;
  final String accountNumber;
  final Function onSubmit;
  final TextEditingController amountController;
  final String vpaID;

  WithdrawDetail({
    @required this.bankDetailsRequired,
    @required this.bIsVPAIDRequired,
    @required this.firstName,
    @required this.lastName,
    @required this.ifscCode,
    @required this.vpaID,
    @required this.minWithdraw,
    @required this.maxWithdraw,
    @required this.accountNumber,
    @required this.onSubmit,
    this.amountController,
  });

  @override
  _WithdrawDetailState createState() => _WithdrawDetailState();
}

class _WithdrawDetailState extends State<WithdrawDetail> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController amountController;
  TextEditingController lastNameController = TextEditingController();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController ifscCodeController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController vpaIDIdController = TextEditingController();

  @override
  void initState() {
    amountController = widget.amountController != null
        ? widget.amountController
        : TextEditingController();
    firstNameController.text = widget.firstName;
    lastNameController.text = widget.lastName;

    if (widget.bankDetailsRequired) {
      ifscCodeController.text = widget.ifscCode;
      accountNumberController.text = widget.accountNumber;
    }
    if (widget.bIsVPAIDRequired) {
      vpaIDIdController.text = widget.vpaID;
    }

    super.initState();
  }

  Widget getBankDetailsRequiredUI() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: SimpleTextBox(
                  controller: accountNumberController,
                  isDense: true,
                  enabled: widget.accountNumber == null,
                  labelText: "Account Number",
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter Account Number";
                    }
                    return null;
                  },
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: SimpleTextBox(
                  controller: ifscCodeController,
                  enabled: widget.ifscCode == null,
                  isDense: true,
                  labelText: "IFSC Code",
                  keyboardType: TextInputType.text,
                  validator: (value) {
                    if (RegExp(r'^[A-Za-z]{4}0[A-Z0-9a-z]{6}$')
                            .allMatches(value)
                            .length ==
                        0) {
                      return "Please enter valid IFSC Code";
                    }
                    return null;
                  },
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: RichText(
                  text: TextSpan(
                    style: TextStyle(
                      color: Colors.black54,
                      fontSize:
                          Theme.of(context).primaryTextTheme.caption.fontSize,
                    ),
                    text:
                        "*Account Name is auto populated as per your KYC. If not matching with the name of your bank account please write to us ",
                    children: [
                      TextSpan(
                        text: "here",
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            FocusScope.of(context).unfocus();
                            routeManager.launchContactUs(context);
                          },
                        style: TextStyle(
                          color: Colors.blue,
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),

                // child: Text(
                //   "*Account Name is auto populated as per your KYC. If not matching with the name of your bank account please write to us here",
                //   textAlign: TextAlign.justify,
                //   style: TextStyle(
                //     color: Colors.black54,
                //     fontSize:
                //         Theme.of(context).primaryTextTheme.caption.fontSize,
                //   ),
                // ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget getUPIDetailsRequiredUI() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: SimpleTextBox(
                  controller: vpaIDIdController,
                  isDense: true,
                  enabled: !(widget.vpaID != null && widget.vpaID != ""),
                  labelText: "Enter UPI Id",
                  keyboardType: TextInputType.text,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 16.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  "*Make sure that you have entered correct details for UPI. Your money will be transferred to the added UPI ID if bank accepts this",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize:
                        Theme.of(context).primaryTextTheme.caption.fontSize,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: SimpleTextBox(
                      controller: amountController,
                      isDense: true,
                      labelText:
                          "Enter Amount (${CurrencyFormat.format(widget.minWithdraw.toDouble())} Min)",
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        User user = Provider.of(context, listen: false);
                        UserBalance userBalance = user.userBalance;

                        if (value.isEmpty) {
                          return "Please enter amount to withdraw";
                        } else {
                          double amount = double.parse(value);
                          if (amount < widget.minWithdraw) {
                            return "You can not withdraw amount less than " +
                                CurrencyFormat.format(
                                    widget.minWithdraw.toDouble());
                          } else if (amount > userBalance.withdrawable) {
                            return "You can not withdraw more than " +
                                CurrencyFormat.format(userBalance.withdrawable);
                          } else if (amount > widget.maxWithdraw) {
                            return "You can not withdraw more than " +
                                CurrencyFormat.format(
                                    widget.maxWithdraw.toDouble());
                          }
                          return null;
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(right: 4.0),
                      child: SimpleTextBox(
                        controller: firstNameController,
                        isDense: true,
                        labelText: "First Name",
                        enabled: widget.firstName == null ||
                            widget.firstName.isEmpty,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Account Name is required for bank withdraw.";
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 4.0),
                      child: SimpleTextBox(
                        isDense: true,
                        controller: lastNameController,
                        labelText: "Last Name",
                        enabled: widget.firstName == null ||
                            widget.firstName.isEmpty,
                        keyboardType: TextInputType.text,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            widget.bankDetailsRequired
                ? getBankDetailsRequiredUI()
                : widget.bIsVPAIDRequired
                    ? getUPIDetailsRequiredUI()
                    : Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: 16.0,
                          horizontal: 8.0,
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Consumer<User>(
                                builder: (context, user, child) {
                                  return RichText(
                                    textAlign: TextAlign.left,
                                    text: TextSpan(
                                        style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: Theme.of(context)
                                              .primaryTextTheme
                                              .caption
                                              .fontSize,
                                        ),
                                        children: [
                                          TextSpan(
                                              text:
                                                  "*Make sure your mobile number registered with Paytm is KYC verified"),
                                        ]),
                                  );
                                },
                              ),
                            )
                          ],
                        ),
                      ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Button(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        widget.onSubmit(
                            double.tryParse(amountController.text),
                            {
                              "account_number": accountNumberController.text,
                              "ifsc_code": ifscCodeController.text,
                              "bank_name": firstNameController.text +
                                  " " +
                                  lastNameController.text,
                            },
                            firstNameController.text,
                            vpaIDIdController.text);
                      }
                    },
                    text: "Request Withdrawal".toUpperCase(),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
