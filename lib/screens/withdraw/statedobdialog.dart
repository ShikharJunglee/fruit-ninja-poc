import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:solitaire_gold/api/user/profileAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';

const double _pickerSheetHeight = 216.0;

class StateDobDialog extends StatefulWidget {
  final String source;

  StateDobDialog({@required this.source});

  @override
  _StateDobDialogState createState() => _StateDobDialogState();
}

class _StateDobDialogState extends State<StateDobDialog> {
  List<DropdownMenuItem> states = List();
  List<dynamic> listCategories = List();
  Map<String, dynamic> selectedState;
  String _defaultText = "Date of Birth";

  DateTime _date = DateTime(DateTime.now().year - 18);

  bool isDobError = false;
  bool isStateError = false;

  ProfileAPI _profileAPI = ProfileAPI();

  @override
  void initState() {
    super.initState();
    GamePlayAnalytics().onStateDobPopupLoaded(context, source: widget.source);

    WidgetsBinding.instance.addPostFrameCallback((_) => _setData(context));
  }

  @override
  void dispose() {
    super.dispose();
  }

  _setData(BuildContext context) {
    getStates(context);
  }

  getStates(BuildContext context) async {
    final result = await _profileAPI.getStateList(context);
    if (result["error"] == false) {
      listCategories = result["data"];
      for (var i = 0; i < listCategories.length; i++) {
        Map<String, dynamic> category = listCategories[i];
        states.add(DropdownMenuItem(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 16),
              decoration: BoxDecoration(
                border: Border.symmetric(
                    vertical: BorderSide(color: Colors.red[700], width: 1)),
                color: Color.fromRGBO(69, 4, 15, 1),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    category["value"],
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            ),
            value: i.toString()));
      }
      setState(() {});
    }
  }

  Widget _buildBottomPicker(Widget picker) {
    return Container(
      height: _pickerSheetHeight,
      padding: const EdgeInsets.only(top: 6.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: GestureDetector(
          onTap: () {},
          child: SafeArea(
            top: false,
            child: picker,
          ),
        ),
      ),
    );
  }

  Future<Null> _selectDate(BuildContext context) async {
    FocusScope.of(context).unfocus();
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return _buildBottomPicker(
          CupertinoDatePicker(
            maximumDate: DateTime.now(),
            mode: CupertinoDatePickerMode.date,
            initialDateTime: _date,
            onDateTimeChanged: (DateTime newDateTime) {
              setState(() {
                _defaultText = null;
                _date = newDateTime;
                isDobError = false;
              });
            },
          ),
        );
      },
    );
  }

  String getDate() {
    return (_date.year.toString() +
        "-" +
        (_date.month >= 10
            ? _date.month.toString()
            : ("0" + _date.month.toString())) +
        "-" +
        (_date.day >= 10
            ? _date.day.toString()
            : ("0" + _date.day.toString())));
  }

  void onSubmit(BuildContext context) async {
    GamePlayAnalytics().onStateDobSubmitClicked(
      context,
      source: widget.source,
    );
    if (selectedState == null) {
      setState(() {
        isStateError = true;
      });
    }
    if (_defaultText != null) {
      setState(() {
        isDobError = true;
      });
    }
    if (_defaultText == null) {
      Loader().showLoader(true, immediate: true);

      final result = await _profileAPI.updateStateDob(
        context,
        state: selectedState["code"],
        dob: getDate(),
      );

      Loader().showLoader(false);

      if (result != null && result["success"]) {
        GamePlayAnalytics().onStateDobSubmitted(
          context,
          source: widget.source,
          dob: getDate(),
          state: selectedState["code"],
          error: false,
        );
        Navigator.pop(context, {
          "success": true,
        });
      } else {
        GamePlayAnalytics().onStateDobSubmitted(
          context,
          source: widget.source,
          dob: getDate(),
          state: selectedState["code"],
          error: true,
        );
        Navigator.pop(context, {
          "success": false,
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }
    return CustomDialog(
      padding: EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: CommonDialogFrame(
          onClose: () {
            GamePlayAnalytics()
                .stateDobCancelClicked(context, source: widget.source);
          },
          titleAsset: "images/titles/details.png",
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 32.0, vertical: 16.0),
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 24.0),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(bottom: 32.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(3.0),
                                            color: Color.fromRGBO(69, 4, 15, 1),
                                            border: Border.all(
                                              width: 1.0,
                                              color: Color.fromRGBO(
                                                  214, 123, 16, 1),
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                blurRadius: 1.0,
                                                spreadRadius: 1.0,
                                                color: Color.fromRGBO(
                                                    214, 123, 16, 1),
                                              ),
                                            ],
                                          ),
                                          child: DropdownButton(
                                            isExpanded: true,
                                            underline: Container(),
                                            hint: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 14.0,
                                                      horizontal: 12.0),
                                              child: Text(
                                                selectedState == null
                                                    ? "State"
                                                    : selectedState["value"],
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .headline6
                                                    .copyWith(
                                                      color: Color.fromRGBO(
                                                          214, 123, 16, 1),
                                                    ),
                                              ),
                                            ),
                                            dropdownColor:
                                                Color.fromRGBO(69, 4, 15, 1),
                                            items: states,
                                            onChanged: (dynamic state) {
                                              setState(() {
                                                selectedState = listCategories[
                                                    int.parse(state)];
                                                isStateError = false;
                                              });
                                            },
                                            iconSize: 30,
                                            iconEnabledColor: Colors.white,
                                            icon:
                                                Icon(Icons.keyboard_arrow_down),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  if (isStateError)
                                    Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: Text(
                                        "Please enter state",
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .headline6
                                            .copyWith(
                                                fontWeight: FontWeight.normal,
                                                fontSize: 18,
                                                color: Theme.of(context)
                                                    .errorColor),
                                      ),
                                    ),
                                ],
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 12.0, vertical: 12.0),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(3.0),
                                          color: Color.fromRGBO(69, 4, 15, 1),
                                          border: Border.all(
                                            width: 1.0,
                                            color:
                                                Color.fromRGBO(214, 123, 16, 1),
                                          ),
                                          boxShadow: [
                                            BoxShadow(
                                              blurRadius: 1.0,
                                              spreadRadius: 1.0,
                                              color: Color.fromRGBO(
                                                  214, 123, 16, 1),
                                            ),
                                          ],
                                        ),
                                        child: InkWell(
                                          onTap: () {
                                            _selectDate(context);
                                          },
                                          child: _defaultText != null
                                              ? Text(
                                                  _defaultText,
                                                  style: Theme.of(context)
                                                      .primaryTextTheme
                                                      .headline6
                                                      .copyWith(
                                                        color: Color.fromRGBO(
                                                            214, 123, 16, 1),
                                                      ),
                                                )
                                              : Text(
                                                  getDate(),
                                                  style: Theme.of(context)
                                                      .primaryTextTheme
                                                      .headline6
                                                      .copyWith(
                                                        color: Color.fromRGBO(
                                                            214, 123, 16, 1),
                                                      ),
                                                ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                if (isDobError)
                                  Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Text(
                                      "Please enter date of birth",
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .headline6
                                          .copyWith(
                                              fontWeight: FontWeight.normal,
                                              fontSize: 18,
                                              color:
                                                  Theme.of(context).errorColor),
                                    ),
                                  ),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 24.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 80.0),
                        constraints: BoxConstraints(maxWidth: 180.0),
                        child: Button(
                          text: "SUBMIT",
                          style: ctaStyle,
                          onPressed: () {
                            onSubmit(context);
                          },
                          size: ButtonSize.medium,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
