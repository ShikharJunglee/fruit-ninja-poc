import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/kyc/kycverificationAPI.dart';
import 'package:solitaire_gold/api/kyc/withdrawAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/commonwidgets/customexpansionpanbel.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/userbalance.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:solitaire_gold/models/withdraw/withdrawdata.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/screens/withdraw/verification/kycverification.dart';
import 'package:solitaire_gold/screens/withdraw/verification/mobileverification.dart';
import 'package:solitaire_gold/screens/withdraw/verification/panverification.dart';
import 'package:solitaire_gold/screens/withdraw/verification/verificationtitle.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/dochelper.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/withdrawdetail.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/withdrawsuccess.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class Withdraw extends StatefulWidget {
  final String source;
  final Map<String, dynamic> data;

  Withdraw({this.data, this.source});

  @override
  WithdrawState createState() => WithdrawState();
}

class WithdrawState extends State<Withdraw>
    with SingleTickerProviderStateMixin, BasePage {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  WithdrawData _withdrawData;
  int _selectedItemIndex = 0;
  TabController _tabController;
  Map<String, dynamic> _withdrawModes;
  TextEditingController amountController = TextEditingController();

  WithdrawAPI _withdrawAPI = WithdrawAPI();
  KYCVerificationAPI _kycVerificationAPI = KYCVerificationAPI();

  @override
  void initState() {
    super.initState();
    _withdrawData = WithdrawData.fromJson(widget.data);
    _withdrawModes = _withdrawData.withdrawModes;
    if (_withdrawModes != null) {
      _tabController = TabController(
        length: _withdrawModes.keys.length,
        vsync: this,
      );
    }
    _tabController.addListener(() {
      setState(() {});
    });
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  void setData(BuildContext context) async {
    User user = Provider.of<User>(context, listen: false);
    UserBalance userBalance = user.userBalance;
    VerificationStatus verificationStatus = user.verificationStatus;
    userBalance.updateWithdrawable(userBalance.withdrawable);
    verificationStatus
        .updateMobileVerificationStatus(verificationStatus.mobileVerified);
    verificationStatus.updateDocStatus(verificationStatus.panVerificationStatus,
        verificationStatus.addressVerificationStatus);

    GamePlayAnalytics().onWithdrawLoaded(
      context,
      source: widget.source != null ? widget.source : "app_drawer",
      ifsc: _withdrawData.ifscCode != null ? _withdrawData.ifscCode : "",
      accountNo: _withdrawData.accountNumber != null
          ? _withdrawData.accountNumber
          : "",
      upiId: _withdrawData.vpaID != null ? _withdrawData.vpaID : "",
      mode: _withdrawModes.keys.toList()[0],
    );

    Loader().showLoader(true, immediate: true);

    _kycVerificationAPI.getVerificationStatus(context);

    Loader().showLoader(false);
  }

  onWithdrawRequest(double amount, Map<String, dynamic> bankDetails,
      String name, int withdrawType, String vpaID) async {
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }
    User user = Provider.of(context, listen: false);
    UserBalance userBalance = user.userBalance;

    GamePlayAnalytics().onRequestWithdrawClicked(
      context,
      source: "withdraw_page",
      fName: user.address.firstName,
      lName: user.address.lastName,
      withdrawMode: _withdrawModes.keys.toList()[_tabController.index],
      ifsc: _withdrawData.ifscCode != null ? _withdrawData.ifscCode : "",
      accountNo: _withdrawData.accountNumber != null
          ? _withdrawData.accountNumber
          : "",
      upiId: _withdrawData.vpaID != null ? _withdrawData.vpaID : "",
      withdrawAmount: amount,
    );

    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> response = await _withdrawAPI.makeWithdrawRequest(
        context,
        amount: amount,
        bankDetails: bankDetails,
        hasBankDetails: (_withdrawData.accountNumber != null &&
            _withdrawData.accountNumber != "" &&
            user.address.firstName != null &&
            user.address.firstName != "" &&
            user.address.lastName != null &&
            user.address.lastName != "" &&
            _withdrawData.ifscCode != "" &&
            _withdrawData.ifscCode != null),
        name: name,
        hasVPAID: (_withdrawData.vpaID != null &&
            _withdrawData.vpaID != "" &&
            user.address.firstName != null &&
            user.address.firstName != "" &&
            user.address.lastName != null &&
            user.address.lastName != ""),
        withdrawType: withdrawType,
        vpaID: vpaID);

    Loader().showLoader(false);

    if (response["error"]) {
      showMessageOnTop(context, msg: response["errorMessage"]);
    } else {
      GamePlayAnalytics().onWithdrawRaised(
        context,
        source: "withdraw_page",
        fName: user.address.firstName,
        lName: user.address.lastName,
        withdrawMode: _withdrawModes.keys.toList()[_tabController.index],
        ifsc: _withdrawData.ifscCode != null ? _withdrawData.ifscCode : "",
        accountNo: _withdrawData.accountNumber != null
            ? _withdrawData.accountNumber
            : "",
        upiId: _withdrawData.vpaID != null ? _withdrawData.vpaID : "",
        withdrawAmount: amount,
      );
      amountController.clear();
      FocusScope.of(context).unfocus();
      userBalance
          .updateWithdrawable(userBalance.withdrawable - response["amount"]);
      await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return WithdrawSuccess(
              withdrawResponse: response,
              withdrawType: withdrawType,
            );
          });
      await launchWithdrawHistory(context);
    }
  }

  launchWithdrawHistory(BuildContext context) {
    User user = Provider.of(context, listen: false);

    GamePlayAnalytics().onWithdrawHistoryClicked(
      context,
      source: "withdraw_page",
      fName: user.address.firstName,
      lName: user.address.lastName,
      withdrawMode: _withdrawModes.keys.toList()[_tabController.index],
      ifsc: _withdrawData.ifscCode != null ? _withdrawData.ifscCode : "",
      accountNo: _withdrawData.accountNumber != null
          ? _withdrawData.accountNumber
          : "",
      upiId: _withdrawData.vpaID != null ? _withdrawData.vpaID : "",
    );
    return routeManager.launchWithdrawHistory(context, source: "withdraw_page");
  }

  Widget getTabByWithdrawMode(BuildContext context, String mode) {
    switch (mode) {
      case "paytm":
        return getPaytmWithDrawWidget(context);
      case "bank":
        return getBankWithdrawWidget(context);
      case "upi":
        return getUPIWithdrawWidget(context);
    }
    return Container();
  }

  String getStringForStatus(String status, int source) {
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;
    if (status != "DEFAULT_MSG" && status != KYCVerificationStatus.VERIFIED) {
      return verificationStatus.messages[status] ?? "";
    } else {
      String msg = "";
      if (source == 0) {
        String addressStatus = verificationStatus.addressVerificationStatus;
        if (addressStatus == KYCVerificationStatus.SUBMITTED) {
          msg = DocVerificationMessages.DOC_SUBMITTED;
        } else if (addressStatus == KYCVerificationStatus.UNDER_REVIEW) {
          msg = AddressVerificationMessages.UNDER_REVIEW;
        } else if (addressStatus == KYCVerificationStatus.REJECTED) {
          msg = AddressVerificationMessages.DOC_REJECTED;
        } else if (addressStatus == KYCVerificationStatus.VERIFIED) {
          msg = AddressVerificationMessages.VERIFIED;
        }
      } else {
        String panStatus = verificationStatus.panVerificationStatus;
        if (panStatus == KYCVerificationStatus.SUBMITTED) {
          msg = DocVerificationMessages.DOC_SUBMITTED;
        } else if (panStatus == KYCVerificationStatus.UNDER_REVIEW) {
          msg = PanVerificationMessages.UNDER_REVIEW;
        } else if (panStatus == KYCVerificationStatus.REJECTED) {
          msg = PanVerificationMessages.DOC_REJECTED;
        } else if (panStatus == KYCVerificationStatus.VERIFIED) {
          msg = PanVerificationMessages.VERIFIED;
        } else if (panStatus == KYCVerificationStatus.MAX_ATTEMPTS) {
          msg = PanVerificationMessages.MAX_ATTEMPTS;
        }
      }
      return msg;
    }
  }

  Widget getPaytmWithDrawWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Consumer<User>(
        builder: (context, user, child) {
          return ChangeNotifierProvider.value(
            value: user.verificationStatus,
            child: Consumer<VerificationStatus>(
              builder: (context, verificationStatus, child) {
                bool isKYCVerified =
                    verificationStatus.addressVerificationStatus ==
                            "VERIFIED" &&
                        verificationStatus.panVerificationStatus ==
                            KYCVerificationStatus.VERIFIED;
                return ((_withdrawModes["paytm"] as List).indexOf("mobile") !=
                                -1 &&
                            !verificationStatus.mobileVerified) ||
                        (_withdrawModes["paytm"].indexOf("pan card") != -1 &&
                            !isKYCVerified)
                    ? Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Consumer<User>(
                          builder: (context, user, child) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    getMobileVerificationWidget(context),
                                    getKYCVerificationWidget(context),
                                    getPANVerificationWidget(context),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    : Column(
                        children: <Widget>[
                          WithdrawDetail(
                            bankDetailsRequired: false,
                            bIsVPAIDRequired: false,
                            vpaID: _withdrawData.vpaID,
                            accountNumber: _withdrawData.accountNumber,
                            firstName: user.address.firstName,
                            ifscCode: _withdrawData.ifscCode,
                            lastName: user.address.lastName,
                            maxWithdraw: _withdrawData.paytmMaxWithdraw,
                            minWithdraw: _withdrawData.paytmMinWithdraw,
                            onSubmit: (double amount,
                                Map<String, dynamic> bankDetails,
                                String name,
                                String vpaID) {
                              onWithdrawRequest(
                                  amount, bankDetails, name, 4, vpaID);
                            },
                            amountController: amountController,
                          ),
                        ],
                      );
              },
            ),
          );
        },
      ),
    );
  }

  Widget getBankWithdrawWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Consumer<User>(
        builder: (context, user, child) {
          return ChangeNotifierProvider.value(
            value: user.verificationStatus,
            child: Consumer<VerificationStatus>(
              builder: (context, verificationStatus, child) {
                bool isKYCVerified =
                    verificationStatus.addressVerificationStatus ==
                            "VERIFIED" &&
                        verificationStatus.panVerificationStatus ==
                            KYCVerificationStatus.VERIFIED;
                return ((_withdrawModes["bank"] as List).indexOf("mobile") !=
                                -1 &&
                            !verificationStatus.mobileVerified) ||
                        (_withdrawModes["bank"].indexOf("pan card") != -1 &&
                            !isKYCVerified)
                    ? Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Consumer<User>(
                          builder: (context, user, child) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    getMobileVerificationWidget(context),
                                    getKYCVerificationWidget(context),
                                    getPANVerificationWidget(context),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    : Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: WithdrawDetail(
                          bankDetailsRequired: true,
                          bIsVPAIDRequired: false,
                          vpaID: _withdrawData.vpaID,
                          accountNumber: _withdrawData.accountNumber,
                          firstName: user.address.firstName,
                          ifscCode: _withdrawData.ifscCode,
                          lastName: user.address.lastName,
                          maxWithdraw: _withdrawData.maxWithdraw,
                          minWithdraw: _withdrawData.minWithdraw,
                          onSubmit: (double amount,
                              Map<String, dynamic> bankDetails,
                              String name,
                              String vpaID) {
                            onWithdrawRequest(
                                amount, bankDetails, name, 1, vpaID);
                          },
                          amountController: amountController,
                        ),
                      );
              },
            ),
          );
        },
      ),
    );
  }

  Widget getUPIWithdrawWidget(BuildContext context) {
    return SingleChildScrollView(
      child: Consumer<User>(
        builder: (context, user, child) {
          return ChangeNotifierProvider.value(
            value: user.verificationStatus,
            child: Consumer<VerificationStatus>(
              builder: (context, verificationStatus, child) {
                bool isKYCVerified =
                    verificationStatus.addressVerificationStatus ==
                            "VERIFIED" &&
                        verificationStatus.panVerificationStatus ==
                            KYCVerificationStatus.VERIFIED;
                return ((_withdrawModes["upi"] as List).indexOf("mobile") !=
                                -1 &&
                            !verificationStatus.mobileVerified) ||
                        (_withdrawModes["upi"].indexOf("pan card") != -1 &&
                            !isKYCVerified)
                    ? Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Consumer<User>(
                          builder: (context, user, child) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 0),
                              child: SingleChildScrollView(
                                child: Column(
                                  children: <Widget>[
                                    getMobileVerificationWidget(context),
                                    getKYCVerificationWidget(context),
                                    getPANVerificationWidget(context),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    : Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                        child: WithdrawDetail(
                          bankDetailsRequired: false,
                          bIsVPAIDRequired: true,
                          vpaID: _withdrawData.vpaID,
                          accountNumber: _withdrawData.accountNumber,
                          firstName: user.address.firstName,
                          ifscCode: _withdrawData.ifscCode,
                          lastName: user.address.lastName,
                          maxWithdraw: _withdrawData.maxWithdraw,
                          minWithdraw: _withdrawData.minWithdraw,
                          onSubmit: (double amount,
                              Map<String, dynamic> bankDetails,
                              String name,
                              String vpaID) {
                            onWithdrawRequest(
                                amount, bankDetails, name, 5, vpaID);
                          },
                          amountController: amountController,
                        ),
                      );
              },
            ),
          );
        },
      ),
    );
  }

  void _setSelectedItem(int i) {
    setState(() {
      if (_selectedItemIndex == i)
        _selectedItemIndex = -1;
      else
        _selectedItemIndex = i;
    });
  }

  CustomExpansionTile getMobileVerificationWidget(BuildContext context) {
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;
    return CustomExpansionTile(
      isExpanded: _selectedItemIndex == 0,
      canExpand: !verificationStatus.mobileVerified,
      child: MobileVerification(onVerificationError: (String error) {
        showMessageOnTop(context, msg: error);
      }),
      onExpansionChanged: () {
        _setSelectedItem(0);
      },
      subtitle: Text(user.mobile ?? ""),
      title: VerificationTitle(
        heading: "Mobile",
        status: verificationStatus.mobileVerified ? 1 : 0,
        notVerifiedStatusText: "Verify",
      ),
    );
  }

  int getKycStatus(VerificationStatus verificationStatus) {
    switch (verificationStatus.addressVerificationStatus) {
      case KYCVerificationStatus.NOT_SUBMITTED:
        return 0;
      case KYCVerificationStatus.VERIFIED:
        return 1;
      case KYCVerificationStatus.UNDER_REVIEW:
        return 2;
      case KYCVerificationStatus.REJECTED:
      case KYCVerificationStatus.MAX_ATTEMPTS:
        return 3;
      case KYCVerificationStatus.SUBMITTED:
        return 4;
    }
    return 0;
  }

  int getPanStatus(VerificationStatus verificationStatus) {
    switch (verificationStatus.panVerificationStatus) {
      case KYCVerificationStatus.NOT_SUBMITTED:
      case KYCVerificationStatus.PAN_REQUIRED:
        return 0;
      case KYCVerificationStatus.VERIFIED:
        return 1;
      case KYCVerificationStatus.UNDER_REVIEW:
        return 2;
      case KYCVerificationStatus.REJECTED:
      case KYCVerificationStatus.MAX_ATTEMPTS:
        return 3;
      case KYCVerificationStatus.SUBMITTED:
        return 4;
    }
    return 0;
  }

  CustomExpansionTile getPANVerificationWidget(BuildContext context) {
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;
    return CustomExpansionTile(
      isExpanded: _selectedItemIndex == 3,
      canExpand: !(verificationStatus.panVerificationStatus ==
          KYCVerificationStatus.VERIFIED),
      child: PanVerification(onVerificationError: (String error) {
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) {
            return SimpleDialog(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        error,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 78.0),
                  child: Button(
                    size: ButtonSize.medium,
                    text: "OK",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          },
        );
      }),
      onExpansionChanged: () {
        GamePlayAnalytics().onKycPanClicked(context, source: "withdraw_page");
        _setSelectedItem(3);
      },
      subtitle: Text(
        getStringForStatus(verificationStatus.panRemark, 1),
      ),
      title: VerificationTitle(
        heading: "PAN",
        status: getPanStatus(verificationStatus),
        notVerifiedStatusText: "Submit",
      ),
    );
  }

  CustomExpansionTile getKYCVerificationWidget(BuildContext _context) {
    User user = Provider.of(_context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;
    return CustomExpansionTile(
      isExpanded: _selectedItemIndex == 1,
      canExpand: !(verificationStatus.addressVerificationStatus ==
          KYCVerificationStatus.VERIFIED),
      child: KycVerification(
        onVerificationError: (String error) {
          showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        error,
                        textAlign: TextAlign.center,
                        softWrap: true,
                        maxLines: 3,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 78.0),
                      child: Button(
                        size: ButtonSize.medium,
                        text: "OK",
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ],
                );
              });
        },
      ),
      onExpansionChanged: () {
        GamePlayAnalytics().onKycClicked(
          context,
          source: "withdraw_page",
        );
        _setSelectedItem(1);
      },
      subtitle: Text(
        getStringForStatus(verificationStatus.kycRemark, 0),
      ),
      title: VerificationTitle(
        heading: "Address Verification",
        status: getKycStatus(verificationStatus),
        notVerifiedStatusText: "Upload",
      ),
    );
  }

  getCurrentActiveWithDrawMode() {
    _withdrawModes.keys.forEach((element) => {
          //return element
        });
    return 1;
  }

  Widget getWithdrawBalanceView() {
    InitData initData = Provider.of(context, listen: false);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 2.0,
            spreadRadius: 1.0,
            color: Colors.grey.shade300,
          ),
        ],
      ),
      child: Card(
        elevation: 0.0,
        margin: EdgeInsets.zero,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 2, bottom: 2),
                  child: Text(
                    "Withdrawable balance",
                    style:
                        Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 18.0,
                            ),
                  ),
                ),
                Consumer<User>(
                  builder: (context, user, child) {
                    return ChangeNotifierProvider.value(
                      value: user.userBalance,
                      child: Consumer<UserBalance>(
                        builder: (context, userBalance, child) {
                          return CurrencyText(
                            showCurrencyDelimiters: false,
                            amount: userBalance.withdrawable,
                            decimalDigits:
                                initData.experiments.miscConfig.amountPrecision,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline5
                                .copyWith(
                                  color: Colors.grey.shade800,
                                  fontSize: 28.0,
                                  fontWeight: FontWeight.bold,
                                ),
                          );
                        },
                      ),
                    );
                  },
                ),
              ]),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> getWithdrawModes() {
    int i = 0;
    List<Widget> items = [];

    for (String withdrawType in _withdrawModes.keys) {
      int curIndex = i;
      items.add(
        Expanded(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: InkWell(
                  radius: 6.0,
                  onTap: () {
                    GamePlayAnalytics().onWithdrawTypeSelected(
                      context,
                      source: "withdraw_page",
                      withdrawMode: _withdrawModes.keys.toList()[curIndex],
                    );
                    setState(() {
                      _tabController.index = curIndex;
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 4.0),
                    margin:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 4.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(6.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2.0,
                          spreadRadius: 1.0,
                          color: Colors.grey.shade300,
                        ),
                      ],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Image.asset(
                                  "images/withdraw/" +
                                      withdrawType.toLowerCase() +
                                      ".png",
                                  height: 20.0,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 2.0, right: 2.0, top: 2.0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        withdrawType.toUpperCase(),
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .subtitle2
                                            .copyWith(
                                                color: Colors.grey.shade600,
                                                fontSize: 18.0),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              if (_tabController.index == i)
                Positioned(
                  bottom: 2.2,
                  child: Container(
                    child: Image.asset(
                      "images/misc/white_arrow.png",
                      height: 16.0,
                    ),
                  ),
                ),
            ],
          ),
        ),
      );
      i++;
    }

    return items;
  }

  @override
  void didUpdateWidget(Withdraw oldWidget) {
    _tabController.index = 0;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return LoaderContainer(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CommonAppBar(
          children: [
            Text(
              "Withdrawals".toUpperCase(),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: IconButton(
                padding: EdgeInsets.symmetric(vertical: 4.0),
                icon: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      "images/withdraw/history_icon.png",
                      height: 28.0,
                    ),
                    Text(
                      "History",
                      style:
                          Theme.of(context).primaryTextTheme.caption.copyWith(
                                color: Colors.white,
                              ),
                    )
                  ],
                ),
                onPressed: () {
                  launchWithdrawHistory(context);
                },
              ),
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            getWithdrawBalanceView(),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 16.0),
            //   child: Text(
            //     "Withdraw options",
            //     style: Theme.of(context).primaryTextTheme.headline6.copyWith(
            //           color: Colors.grey.shade700,
            //         ),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: Row(
                children: getWithdrawModes(),
              ),
            ),
            Expanded(
              child: _tabController != null
                  ? TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: _tabController,
                      children: _withdrawModes.keys.map((k) {
                        return getTabByWithdrawMode(context, k);
                      }).toList(),
                    )
                  : Center(
                      child: Text(
                        "Withdraw is not available right now.\n\nPlease try after sometime.",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .bodyText1
                            .copyWith(
                              color: Colors.red,
                            ),
                        textAlign: TextAlign.center,
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
