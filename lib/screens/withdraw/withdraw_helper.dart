import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/api/kyc/withdrawAPI.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/withdraw/statedobdialog.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';

class WithdrawHelper {
  WithdrawHelper._internal();
  factory WithdrawHelper() => withdrawHelper;
  static final WithdrawHelper withdrawHelper = WithdrawHelper._internal();

  WithdrawAPI _withdrawAPI = WithdrawAPI();

  launchWithdraw(BuildContext context,
      {@required String source, @required Function onErrorMessage}) async {
    AnalyticsManager().setJourney("Withdraw");

    Loader().showLoader(true, immediate: true);
    AppConfig config = Provider.of(context, listen: false);
    dynamic result;
    if (config.channelId < 200) {
      result = await _withdrawAPI.authWithdraw(context);
    } else {
      result = await _withdrawAPI.authWithdrawUS(context);
    }

    Loader().showLoader(false);

    if (result["error"] != true && result["data"] != null) {
      User user = Provider.of(context, listen: false);
      user.copyFrom(result["data"]["user"]);

      if (result["data"]["error"].length > 0) {
        if (result["data"]["errorDetails"]["isBlockedUser"]) {
          if (onErrorMessage != null)
            onErrorMessage(result["data"]["errorDetails"]["errorMessage"]);
        } else {
          List errorCodes = result["data"]["error"];
          int errorCode = _getErrorCode(errorCodes);
          switch (errorCode) {
            case AuthStatusCode.MISSING_DOB:
              final result = await showDialog(
                barrierColor: Color.fromARGB(175, 0, 0, 0),
                context: context,
                builder: (BuildContext context) {
                  return StateDobDialog(
                    source: "withdraw_page",
                  );
                },
              );
              if (result != null && result["success"]) {
                launchWithdraw(context,
                    source: source, onErrorMessage: onErrorMessage);
              } else if (result != null && !result["success"]) {
                if (onErrorMessage != null)
                  onErrorMessage("Getting error while updating details.");
              }
              break;
            case AuthStatusCode.NOT_VERIFIED:
            case AuthStatusCode.REG_CLOSED:
              routeManager.launchWithdraw(
                context,
                withdrawData: result["data"]["withdrawData"],
                source: source,
              );
              break;
            case AuthStatusCode.PAN_REQUIRED:
              routeManager.launchWithdraw(
                context,
                withdrawData: result["data"]["withdrawData"],
                source: source,
              );
              break;
          }
        }
      } else {
        await routeManager.launchWithdraw(context,
            withdrawData: result["data"]["withdrawData"], source: source);
        AnalyticsManager().setJourney(null);
      }
    }
  }

  int _getErrorCode(errorCodes) {
    if (errorCodes.indexOf(AuthStatusCode.INSUFFICIENT_FUND) != -1 ||
        errorCodes.indexOf(AuthStatusCode.INSUFFICIENT_CASH_FUND) != -1) {
      return AuthStatusCode.INSUFFICIENT_FUND;
    } else if (errorCodes.indexOf(AuthStatusCode.MISSING_DOB) != -1) {
      return AuthStatusCode.MISSING_DOB;
    } else if (errorCodes.indexOf(AuthStatusCode.NOT_VERIFIED) != -1 ||
        errorCodes.indexOf(7) != -1 ||
        errorCodes.indexOf(8) != -1 ||
        errorCodes.indexOf(9) != -1) {
      return AuthStatusCode.NOT_VERIFIED;
    } else if (errorCodes.indexOf(AuthStatusCode.CONTEST_ALREADY_JOINED) !=
        -1) {
      return AuthStatusCode.CONTEST_ALREADY_JOINED;
    } else {
      return AuthStatusCode.REG_CLOSED;
    }
  }
}
