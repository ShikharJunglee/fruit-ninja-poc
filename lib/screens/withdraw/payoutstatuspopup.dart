import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/kyc/withdrawAPI.dart';

class TsevoPayoutStatusPopup extends StatefulWidget {
  final Map<String, dynamic> payoutTransaction;

  TsevoPayoutStatusPopup({this.payoutTransaction});

  @override
  _TsevoPayoutStatusPopupState createState() => _TsevoPayoutStatusPopupState();
}

class _TsevoPayoutStatusPopupState extends State<TsevoPayoutStatusPopup> {
  WithdrawAPI _withdrawAPI = WithdrawAPI();
  bool dataLoaded = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  setData(BuildContext context) {
    _withdrawAPI.getTsevoWithdrawStatus(
        context, widget.payoutTransaction["id"]);
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Row(
        children: [Text("User Info".toUpperCase())],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      children: [
        !dataLoaded
            ? Container()
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Text("")],
              ),
      ],
    );
  }
}
