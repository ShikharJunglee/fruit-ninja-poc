import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/api/kyc/withdrawAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/userbalance.dart';
import 'package:solitaire_gold/models/withdraw/withdrawdata.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/totalbalance.dart';
import 'package:solitaire_gold/screens/withdraw/ssnpopup.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/withdrawsuccess.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';

class WithdrawUS extends StatefulWidget {
  final String source;
  final Map<String, dynamic> data;

  WithdrawUS({this.data, this.source});

  @override
  WithdrawUSState createState() => WithdrawUSState();
}

class WithdrawUSState extends State<WithdrawUS>
    with SingleTickerProviderStateMixin, BasePage {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  WithdrawData _withdrawData;
  Map<String, dynamic> _withdrawModes;
  String _currentWithdrawMode = "";

  TextEditingController amountController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController routingNumberController = TextEditingController();

  WithdrawAPI _withdrawAPI = WithdrawAPI();
  bool addressPresent = true;
  bool addressError = false;
  bool hasBankDetails = false;

  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _withdrawData = WithdrawData.fromJson(widget.data);
    _withdrawModes = _withdrawData.withdrawModes;

    if (_withdrawModes != null) {
      _tabController = TabController(
        length: _withdrawModes.keys.length,
        vsync: this,
      );
    }

    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  void setData(BuildContext context) async {
    User user = Provider.of<User>(context, listen: false);
    UserBalance userBalance = user.userBalance;
    userBalance.updateWithdrawable(userBalance.withdrawable);

    fillData();

    GamePlayAnalytics().onWithdrawUSLoaded(
      context,
      source: widget.source != null ? widget.source : "app_drawer",
      routingNumber: _withdrawData.routingNumber != null
          ? _withdrawData.routingNumber
          : "",
      bAddressPresent: addressPresent,
      accountNo: _withdrawData.accountNumber != null
          ? _withdrawData.accountNumber
          : "",
      mode: "201",
    );
  }

  fillData() {
    if (_withdrawData.routingNumber != null) {
      routingNumberController.text = _withdrawData.routingNumber;
    }
    if (_withdrawData.accountNumber != null) {
      accountNumberController.text = _withdrawData.accountNumber;
      setState(() {
        hasBankDetails = true;
      });
    }
  }

  onWithdrawRequestTsevo(double amount, int withdrawType) async {
    User user = Provider.of(context, listen: false);
    UserBalance userBalance = user.userBalance;

    if (_withdrawData.ssnEnabled != null && _withdrawData.ssnEnabled) {
      if (!await checkForSSN(amount)) {
        return;
      }
    }
    if (_withdrawData.processingFee["202"] >= amount) {
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Withdraw amount cannot be less than",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.grey.shade700,
                              ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "processing fee.",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.grey.shade700,
                              ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
      return;
    }

    Loader().showLoader(true, immediate: true);
    amount = (((amount * 100).toInt()) / 100);
    Map<String, dynamic> response = await _withdrawAPI.makeWithdrawRequestTsevo(
      context,
      amount: amount,
      withdrawType: withdrawType,
    );

    Loader().showLoader(false);

    if (response["error"]) {
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  response["errorMessage"],
                  textAlign: TextAlign.center,
                  softWrap: true,
                  maxLines: 3,
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                        color: Colors.grey.shade700,
                      ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
    } else {
      amountController.clear();
      FocusScope.of(context).unfocus();
      setState(() {
        double processingFee = 0;
        if (response["amount"] < _withdrawData.processingMin) {
          processingFee = _withdrawData.processingFee["201"].toDouble();
        }
        userBalance.updateWithdrawable(
            userBalance.withdrawable - response["amount"] - processingFee);
        hasBankDetails = true;
      });
      await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return WithdrawSuccess(
              withdrawResponse: response,
              withdrawType: withdrawType,
            );
          });
      await launchWithdrawHistory(context);
    }
  }

  onWithdrawRequest(
    double amount,
    Map<String, dynamic> bankDetails,
    int withdrawType,
  ) async {
    User user = Provider.of(context, listen: false);
    UserBalance userBalance = user.userBalance;
    GamePlayAnalytics().onRequestWithdrawUSClicked(
      context,
      source: "withdraw_page",
      fName: user.address.firstName,
      lName: user.address.lastName,
      withdrawMode: "201",
      routingNumber: _withdrawData.routingNumber != null
          ? _withdrawData.routingNumber
          : routingNumberController.text,
      processingFee:
          _withdrawData.processingFee[withdrawType.toString()].toDouble(),
      accountNo: _withdrawData.accountNumber != null
          ? _withdrawData.accountNumber
          : accountNumberController.text,
      withdrawAmount: amount,
      bAddressPresent: addressPresent,
    );
    if (_withdrawData.ssnEnabled != null && _withdrawData.ssnEnabled) {
      if (!await checkForSSN(amount)) {
        return;
      }
    }
    if (_withdrawData.processingFee[withdrawType.toString()] >= amount) {
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Withdraw amount cannot be less than",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.grey.shade700,
                              ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "processing fee.",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.grey.shade700,
                              ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
      return;
    }

    Loader().showLoader(true, immediate: true);
    amount = (((amount * 100).toInt()) / 100);
    Map<String, dynamic> response = await _withdrawAPI.makeWithdrawRequestUS(
      context,
      amount: amount,
      bankDetails: bankDetails,
      hasBankDetails: (_withdrawData.accountNumber != null &&
          _withdrawData.accountNumber != "" &&
          user.address.firstName != null &&
          user.address.firstName != "" &&
          user.address.lastName != null &&
          user.address.lastName != "" &&
          _withdrawData.routingNumber != "" &&
          _withdrawData.routingNumber != null),
      withdrawType: withdrawType,
    );

    Loader().showLoader(false);

    if (response["error"]) {
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  response["errorMessage"],
                  textAlign: TextAlign.center,
                  softWrap: true,
                  maxLines: 3,
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                        color: Colors.grey.shade700,
                      ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
    } else {
      GamePlayAnalytics().onWithdrawUSRaised(
        context,
        source: "withdraw_page",
        fName: user.address.firstName,
        lName: user.address.lastName,
        withdrawMode: "201",
        processingFee:
            _withdrawData.processingFee[withdrawType.toString()].toDouble(),
        routingNumber: _withdrawData.routingNumber != null
            ? _withdrawData.routingNumber
            : routingNumberController.text,
        accountNo: _withdrawData.accountNumber != null
            ? _withdrawData.accountNumber
            : accountNumberController.text,
        withdrawAmount: amount,
      );
      amountController.clear();
      FocusScope.of(context).unfocus();
      setState(() {
        double processingFee = 0;
        if (response["amount"] < _withdrawData.processingMin) {
          processingFee = _withdrawData.processingFee["201"].toDouble();
        }
        userBalance.updateWithdrawable(
            userBalance.withdrawable - response["amount"] - processingFee);
        hasBankDetails = true;
      });
      await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return WithdrawSuccess(
              withdrawResponse: response,
              withdrawType: withdrawType,
            );
          });
      await launchWithdrawHistory(context);
    }
  }

  Future<bool> checkForSSN(double amount) async {
    if (_withdrawData.ssnPresent) {
      return Future.value(true);
    }
    Loader().showLoader(true);

    Map<String, dynamic> result = await _withdrawAPI.getTotalWithdraw(context);

    Loader().showLoader(false);
    if (amount + result["data"] > _withdrawData.ssnValue) {
      bool success = await showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (context) {
          return SSNPopup(ssnAmount: _withdrawData.ssnValue);
        },
      );
      if (success) {
        return Future.value(true);
      }
    } else {
      return Future.value(true);
    }
    return Future.value(false);
  }

  launchWithdrawHistory(BuildContext context) {
    return routeManager.launchWithdrawHistory(context, source: "withdraw_page");
  }

  Widget getTabByWithdrawMode(BuildContext context, String mode) {
    switch (mode) {
      case "bank":
        return getBankWithdrawWidget(context);
      case "paypal":
        return getPaypalWithdrawWidget(context);
    }
    return Container();
  }

  Widget getPaypalWithdrawWidget(BuildContext context) {
    final formKey = new GlobalKey<FormState>();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Form(
        key: formKey,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SimpleTextBox(
                maxLength: 10,
                controller: amountController,
                isDense: true,
                labelText: "Enter Amount",
                contentPadding: EdgeInsets.all(0),
                prefixIcon: Container(
                  width: 30,
                  child: Center(
                    child: Text(
                      "\$",
                      style:
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                color: Colors.grey.shade700,
                                fontWeight: FontWeight.w400,
                              ),
                    ),
                  ),
                ),
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                validator: (value) {
                  User user = Provider.of(context, listen: false);
                  if (double.tryParse(value) == null) {
                    return "*Required";
                  }
                  if (double.parse(value) >
                      double.parse(
                          user.userBalance.withdrawable.toStringAsFixed(2))) {
                    return "Insufficient balance";
                  }
                  if (double.parse(value) < _withdrawData.minWithdraw)
                    return "Minimum withdrawal amount is \$${_withdrawData.minWithdraw}";
                  if (double.parse(value) > _withdrawData.maxWithdraw) {
                    return "Maximum withdrawal amount is \$${_withdrawData.maxWithdraw}";
                  }
                  return null;
                },
                onChanged: (String amount) {
                  // setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: [
                            Text(
                              "Processing Fee",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade800,
                                    fontSize: 22.0,
                                  ),
                            ),
                            IconButton(
                              icon: Image.asset(
                                "images/icons/info-icon.png",
                                height: 15,
                              ),
                              onPressed: () {
                                showDialog(
                                  barrierColor: Color.fromARGB(175, 0, 0, 0),
                                  context: context,
                                  builder: (context) {
                                    return SimpleDialog(
                                      titlePadding: EdgeInsets.all(0),
                                      contentPadding: EdgeInsets.all(8),
                                      title: Stack(
                                        children: [
                                          Align(
                                            alignment: Alignment(1, -1),
                                            child: IconButton(
                                              icon: Icon(Icons.close),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 20.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text("Processing fee"
                                                    .toUpperCase()),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        Column(
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                  "Withdrawal under \$${_withdrawData.processingMin} are subject"),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                  "to a \$${_withdrawData.processingFee["201"]} procesing fee."),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "\$",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      double.tryParse(amountController.text) != null &&
                              double.tryParse(amountController.text) <=
                                  _withdrawData.processingMin
                          ? _withdrawData.processingFee["201"]
                              .toStringAsFixed(2)
                          : "0.00",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Text(
                      "Total Withdraw Amount",
                      style:
                          Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 22.0,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "\$",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerRight,
                      child: Text(
                        double.tryParse(amountController.text) != null
                            ? double.tryParse(amountController.text) <=
                                    _withdrawData.processingMin
                                ? (double.tryParse(amountController.text) -
                                        _withdrawData.processingFee["201"])
                                    .toStringAsFixed(2)
                                : double.tryParse(amountController.text)
                                    .toStringAsFixed(2)
                            : "0.00",
                        textAlign: TextAlign.right,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline5
                            .copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 28.0,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Button(
                text: "Request Withdrawal".toUpperCase(),
                onPressed: () {
                  if (!addressPresent) {
                    setState(() {
                      addressError = true;
                    });
                    return;
                  }
                  if (formKey.currentState.validate()) {
                    double amount = double.parse(amountController.text);
                    onWithdrawRequestTsevo(
                      amount,
                      202,
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getBankWithdrawWidget(BuildContext context) {
    final formKey = new GlobalKey<FormState>();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SimpleTextBox(
                maxLength: 10,
                controller: amountController,
                isDense: true,
                labelText: "Enter Amount",
                contentPadding: EdgeInsets.all(0),
                prefixIcon: Container(
                  width: 30,
                  child: Center(
                    child: Text(
                      "\$",
                      style:
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                color: Colors.grey.shade700,
                                fontWeight: FontWeight.w400,
                              ),
                    ),
                  ),
                ),
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                validator: (value) {
                  User user = Provider.of(context, listen: false);
                  if (value.split(".").length > 2) {
                    return "Please enter valid Amount";
                  }
                  if (double.tryParse(value) == null) {
                    return "*Required";
                  }
                  if (double.parse(
                          user.userBalance.withdrawable.toStringAsFixed(2)) <
                      _withdrawData.minWithdraw) {
                    return "You don't have sufficient balance to withdraw";
                  }
                  if (((double.parse(value) > _withdrawData.maxWithdraw) ||
                      double.parse(value) < _withdrawData.minWithdraw)) {
                    if ((double.parse(
                            user.userBalance.withdrawable.toStringAsFixed(2)) <
                        _withdrawData.maxWithdraw)) {
                      return "Enter an amount between \$${_withdrawData.minWithdraw.toStringAsFixed(2)} and \$${user.userBalance.withdrawable.toStringAsFixed(2)}";
                    } else {
                      return "Enter an amount between \$${_withdrawData.minWithdraw.toStringAsFixed(2)} and \$${_withdrawData.maxWithdraw.toStringAsFixed(2)}";
                    }
                  }
                  return null;
                },
                onChanged: (String amount) {
                  //setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SimpleTextBox(
                prefixIcon: Container(
                  width: 30,
                ),
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                ],
                enabled: !hasBankDetails,
                controller: accountNumberController,
                isDense: true,
                labelText: "Account Number",
                maxLength: 17,
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value.isEmpty) {
                    return "*Required";
                  }
                  if (value.length < 4) {
                    return "Invalid";
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SimpleTextBox(
                enabled: !hasBankDetails,
                inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                prefixIcon: Container(
                  width: 30,
                ),
                controller: routingNumberController,
                isDense: true,
                maxLength: 9,
                labelText: "Routing Number",
                keyboardType: TextInputType.number,
                validator: (value) {
                  if (value.isEmpty) {
                    return "*Required";
                  }
                  if (value.length < 9) {
                    return "Invalid";
                  }
                  if (!value.startsWith(new RegExp(
                      r"^((0[0-9])|(1[0-2])|(2[1-9])|(3[0-2])|(6[1-9])|(7[0-2])|80)([0-9]{7})$"))) {
                    return "Invalid";
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: [
                            Text(
                              "Processing Fee",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade800,
                                    fontSize: 22.0,
                                  ),
                            ),
                            IconButton(
                              icon: Image.asset(
                                "images/icons/info-icon.png",
                                height: 15,
                              ),
                              onPressed: () {
                                showDialog(
                                  barrierColor: Color.fromARGB(175, 0, 0, 0),
                                  context: context,
                                  builder: (context) {
                                    return SimpleDialog(
                                      titlePadding: EdgeInsets.all(0),
                                      contentPadding: EdgeInsets.all(8),
                                      title: Stack(
                                        children: [
                                          Align(
                                            alignment: Alignment(1, -1),
                                            child: IconButton(
                                              icon: Icon(Icons.close),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 20.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text("Processing fee"
                                                    .toUpperCase()),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        Column(
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                  "Withdrawal under \$${_withdrawData.processingMin} are subject"),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                  "to a \$${_withdrawData.processingFee["201"]} procesing fee."),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "\$",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      double.tryParse(amountController.text) != null &&
                              double.tryParse(amountController.text) <=
                                  _withdrawData.processingMin
                          ? _withdrawData.processingFee["201"]
                              .toStringAsFixed(2)
                          : "0.00",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Text(
                      "Total Withdraw Amount",
                      style:
                          Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 22.0,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "\$",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerRight,
                      child: Text(
                        double.tryParse(amountController.text) != null
                            ? double.tryParse(amountController.text) <=
                                    _withdrawData.processingMin
                                ? (double.tryParse(amountController.text) -
                                        _withdrawData.processingFee["201"])
                                    .toStringAsFixed(2)
                                : double.tryParse(amountController.text)
                                    .toStringAsFixed(2)
                            : "0.00",
                        textAlign: TextAlign.right,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline5
                            .copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 28.0,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Button(
                text: "Request Withdrawal".toUpperCase(),
                onPressed: () {
                  if (!addressPresent) {
                    setState(() {
                      addressError = true;
                    });
                    return;
                  }
                  if (formKey.currentState.validate()) {
                    double amount = double.parse(amountController.text);
                    onWithdrawRequest(
                      amount,
                      {
                        "account_number": accountNumberController.text,
                        "routing_number": routingNumberController.text,
                      },
                      201,
                    );
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget primaryAddressWidget(BuildContext context) {
    User user = Provider.of(context, listen: false);
    addressPresent = true;
    if (user.address.address1 == null ||
        user.address.firstName == null ||
        user.address.lastName == null ||
        user.address.city == null ||
        user.address.state == null ||
        user.address.pincode == null) {
      addressPresent = false;
    }
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Address",
                style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                      color: Colors.grey.shade700,
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                    ),
              ),
              addressPresent
                  ? InkWell(
                      child:
                          Image.asset("images/icons/edit-icon.png", height: 20),
                      onTap: () async {
                        GamePlayAnalytics().onWithdrawAddAddressClicked(
                          context,
                          source: "witdraw_page",
                          bAdressPresent: addressPresent,
                        );
                        Loader().showLoader(true, immediate: true);
                        await launchAddressDetailsPage();
                      },
                    )
                  : Container(),
            ],
          ),
          !addressPresent
              ? Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "(Please submit an address)",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.red,
                              fontSize: 22.0,
                            ),
                      ),
                      Container(
                        height: 30,
                        child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Button(
                              text: "Add",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                      fontSize: 72.0,
                                      fontWeight: FontWeight.bold),
                              size: ButtonSize.medium,
                              onPressed: () async {
                                GamePlayAnalytics().onWithdrawAddAddressClicked(
                                  context,
                                  source: "witdraw_page",
                                  bAdressPresent: addressPresent,
                                );
                                Loader().showLoader(true, immediate: true);
                                await launchAddressDetailsPage();
                                setState(() {
                                  addressError = false;
                                });
                              }),
                        ),
                      ),
                    ],
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "${user.address.firstName} ${user.address.lastName}",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade800,
                                    fontSize: 22.0,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "${user.address.address1} ${user.address.address2 ?? ""} ${user.address.city} ${user.address.state} ${user.address.pincode}",
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context)
                                .primaryTextTheme
                                .subtitle1
                                .copyWith(
                                  color: Colors.grey.shade800,
                                  fontSize: 22.0,
                                ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
        ],
      ),
    );
  }

  launchAddressDetailsPage() async {
    await routeManager.launchWithdrawAddressDetails(context,
        source: "withdraw-page");
    setState(() {
      addressError = false;
    });
  }

  Widget getWithdrawBalanceView() {
    User user = Provider.of(
      context,
      listen: false,
    );
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 8.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: [
                          Text(
                            "Withdrawable Balance",
                            style: Theme.of(context)
                                .primaryTextTheme
                                .subtitle1
                                .copyWith(
                                  color: Colors.grey.shade800,
                                  fontSize: 22.0,
                                ),
                          ),
                          IconButton(
                              icon: Image.asset(
                                "images/icons/info-icon.png",
                                height: 15,
                              ),
                              onPressed: () {
                                GamePlayAnalytics().onTotalBalanceInfoClicked(
                                    context,
                                    source: "withdraw_page");
                                showDialog(
                                  barrierColor: Color.fromARGB(175, 0, 0, 0),
                                  context: context,
                                  builder: (context) {
                                    return TotalBalance();
                                  },
                                );
                              })
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    "\$",
                    textAlign: TextAlign.right,
                    style:
                        Theme.of(context).primaryTextTheme.headline5.copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 28.0,
                              fontWeight: FontWeight.bold,
                            ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Consumer<User>(
                    builder: (context, user, child) {
                      return ChangeNotifierProvider.value(
                        value: user.userBalance,
                        child: Consumer<UserBalance>(
                          builder: (context, userBalance, child) {
                            return FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerRight,
                              child: Text(
                                userBalance.withdrawable.toStringAsFixed(2),
                                textAlign: TextAlign.right,
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline5
                                    .copyWith(
                                      color: Colors.grey.shade800,
                                      fontSize: 28.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            );
                          },
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LoaderContainer(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CommonAppBar(
          bottom: _tabController != null
              ? TabBar(
                  onTap: (int index) {
                    GamePlayAnalytics().onWithdrawTypeSelected(
                      context,
                      source: "withdraw_page",
                      withdrawMode: _withdrawModes.keys.toList()[index],
                    );
                    setState(() {
                      _currentWithdrawMode =
                          _withdrawModes.keys.toList()[index];
                    });
                  },
                  controller: _tabController,
                  indicatorColor: Colors.white,
                  indicatorWeight: 4.0,
                  labelStyle: Theme.of(context).primaryTextTheme.title.copyWith(
                        fontWeight: FontWeight.w600,
                      ),
                  tabs: _withdrawModes.keys.map((k) {
                    return Tab(
                      child: Text(k.toUpperCase()),
                    );
                  }).toList(),
                )
              : null,
          children: [
            Text(
              "Withdraw".toUpperCase(),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: IconButton(
                padding: EdgeInsets.symmetric(vertical: 4.0),
                icon: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      "images/withdraw/history_icon.png",
                      height: 28.0,
                    ),
                    Text(
                      "History",
                      style:
                          Theme.of(context).primaryTextTheme.caption.copyWith(
                                color: Colors.white,
                              ),
                    )
                  ],
                ),
                onPressed: () {
                  GamePlayAnalytics().onWithdrawHistoryUSClicked(
                    context,
                    source: "withdraw_page",
                  );
                  launchWithdrawHistory(context);
                },
              ),
            )
          ],
        ),
        body: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            primaryAddressWidget(context),
            getWithdrawBalanceView(),
            Expanded(
              child: _tabController != null
                  ? TabBarView(
                      controller: _tabController,
                      children: _withdrawModes.keys.map((k) {
                        return Padding(
                          padding: EdgeInsets.only(top: 12.0),
                          child: getTabByWithdrawMode(context, k),
                        );
                      }).toList(),
                    )
                  : Center(
                      child: Text(
                        "Withdraw is not available right now.\n\nPlease try after sometime.",
                        style:
                            Theme.of(context).primaryTextTheme.title.copyWith(
                                  color: Colors.red,
                                ),
                        textAlign: TextAlign.center,
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
