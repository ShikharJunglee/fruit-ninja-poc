import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/customexpansionpanbel.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:solitaire_gold/screens/withdraw/verification/emailverification.dart';
import 'package:solitaire_gold/screens/withdraw/verification/kycverification.dart';
import 'package:solitaire_gold/screens/withdraw/verification/mobileverification.dart';
import 'package:solitaire_gold/screens/withdraw/verification/panverification.dart';
import 'package:solitaire_gold/screens/withdraw/verification/verificationtitle.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/dochelper.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:provider/provider.dart';

class Verification extends StatefulWidget {
  Verification();

  @override
  _VerificationState createState() => _VerificationState();
}

class _VerificationState extends State<Verification> with BasePage {
  User user;
  int _selectedItemIndex = -1;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  setData(BuildContext context) async {
    setDefaultExpansion(context);
  }

  setDefaultExpansion(BuildContext context) {
    user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;
    setState(() {
      if (!verificationStatus.emailVerified) {
        _selectedItemIndex = 0;
      } else if (!verificationStatus.mobileVerified) {
        _selectedItemIndex = 1;
      } else if (verificationStatus.addressVerificationStatus !=
          KYCVerificationStatus.VERIFIED) {
        _selectedItemIndex = 2;
      } else if (verificationStatus.panVerificationStatus !=
          KYCVerificationStatus.VERIFIED) {
        _selectedItemIndex = 3;
      }
    });
  }

  void _setSelectedItem(int i) {
    setState(() {
      if (_selectedItemIndex == i)
        _selectedItemIndex = -1;
      else
        _selectedItemIndex = i;
    });
  }

  int getKycStatus(VerificationStatus verificationStatus) {
    switch (verificationStatus.addressVerificationStatus) {
      case KYCVerificationStatus.NOT_SUBMITTED:
        return 0;
      case KYCVerificationStatus.VERIFIED:
        return 1;
      case KYCVerificationStatus.UNDER_REVIEW:
        return 2;
      case KYCVerificationStatus.REJECTED:
      case KYCVerificationStatus.MAX_ATTEMPTS:
        return 3;
      case KYCVerificationStatus.SUBMITTED:
        return 4;
    }
    return 0;
  }

  int getPanStatus(VerificationStatus verificationStatus) {
    switch (verificationStatus.panVerificationStatus) {
      case KYCVerificationStatus.NOT_SUBMITTED:
      case KYCVerificationStatus.PAN_REQUIRED:
        return 0;
      case KYCVerificationStatus.VERIFIED:
        return 1;
      case KYCVerificationStatus.UNDER_REVIEW:
        return 2;
      case KYCVerificationStatus.REJECTED:
      case KYCVerificationStatus.MAX_ATTEMPTS:
        return 3;
      case KYCVerificationStatus.SUBMITTED:
        return 4;
    }
    return 0;
  }

  String getStringForStatus(String status, int source) {
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;

    if (status != "DEFAULT_MSG" && status != KYCVerificationStatus.VERIFIED) {
      return verificationStatus.messages[status] ?? "";
    } else {
      String msg = "";
      if (source == 0) {
        String addressStatus = verificationStatus.addressVerificationStatus;
        if (addressStatus == KYCVerificationStatus.SUBMITTED) {
          msg = DocVerificationMessages.DOC_SUBMITTED;
        } else if (addressStatus == KYCVerificationStatus.UNDER_REVIEW) {
          msg = AddressVerificationMessages.UNDER_REVIEW;
        } else if (addressStatus == KYCVerificationStatus.REJECTED) {
          msg = AddressVerificationMessages.DOC_REJECTED;
        } else if (addressStatus == KYCVerificationStatus.VERIFIED) {
          msg = AddressVerificationMessages.VERIFIED;
        } else if (addressStatus == KYCVerificationStatus.MAX_ATTEMPTS) {
          msg = AddressVerificationMessages.MAX_ATTEMPTS;
        }
      } else {
        String panStatus = verificationStatus.panVerificationStatus;
        if (panStatus == KYCVerificationStatus.SUBMITTED) {
          msg = DocVerificationMessages.DOC_SUBMITTED;
        } else if (panStatus == KYCVerificationStatus.UNDER_REVIEW) {
          msg = PanVerificationMessages.UNDER_REVIEW;
        } else if (panStatus == KYCVerificationStatus.REJECTED) {
          msg = PanVerificationMessages.DOC_REJECTED;
        } else if (panStatus == KYCVerificationStatus.VERIFIED) {
          msg = PanVerificationMessages.VERIFIED;
        } else if (panStatus == KYCVerificationStatus.MAX_ATTEMPTS) {
          msg = PanVerificationMessages.MAX_ATTEMPTS;
        }
      }
      return msg;
    }
  }

  getEmailWidget(VerificationStatus verificationStatus, User user) {
    return CustomExpansionTile(
      isExpanded: _selectedItemIndex == 0,
      canExpand: !verificationStatus.emailVerified,
      child: EmailVerification(),
      onExpansionChanged: () {
        _setSelectedItem(0);
      },
      subtitle: Text(user.email ?? ""),
      title: VerificationTitle(
        heading: "Email",
        status: verificationStatus.emailVerified ? 1 : 0,
        notVerifiedStatusText: "Verify",
      ),
    );
  }

  getPhoneWidget(VerificationStatus verificationStatus, User user) {
    return CustomExpansionTile(
      isExpanded: _selectedItemIndex == 1,
      canExpand: !verificationStatus.mobileVerified,
      child: MobileVerification(onVerificationError: (String error) {
        showMessageOnTop(context, msg: error);
      }),
      onExpansionChanged: () {
        _setSelectedItem(1);
      },
      subtitle: Text(user.mobile ?? ""),
      title: VerificationTitle(
        heading: "Mobile",
        status: verificationStatus.mobileVerified ? 1 : 0,
        notVerifiedStatusText: "Verify",
      ),
    );
  }

  getKYCWidget(VerificationStatus verificationStatus, User user) {
    return CustomExpansionTile(
      isExpanded: _selectedItemIndex == 2,
      canExpand: !(verificationStatus.addressVerificationStatus ==
          KYCVerificationStatus.VERIFIED),
      child: KycVerification(
        onVerificationError: (String error) {
          showDialog(
              context: context,
              builder: (context) {
                return SimpleDialog(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        error,
                        textAlign: TextAlign.center,
                        softWrap: true,
                        maxLines: 3,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 78.0),
                      child: Button(
                        size: ButtonSize.medium,
                        text: "OK",
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                  ],
                );
              });
        },
      ),
      onExpansionChanged: () {
        GamePlayAnalytics().onKycClicked(
          context,
          source: "kyc",
        );
        _setSelectedItem(2);
      },
      subtitle: Text(getStringForStatus(verificationStatus.kycRemark, 0) ?? ""),
      title: VerificationTitle(
        heading: "Address Verification",
        status: getKycStatus(verificationStatus),
        notVerifiedStatusText: "Upload",
      ),
    );
  }

  getPANWidget(VerificationStatus verificationStatus, User user) {
    return CustomExpansionTile(
      isExpanded: _selectedItemIndex == 3,
      canExpand: !(verificationStatus.panVerificationStatus ==
          KYCVerificationStatus.VERIFIED),
      child: PanVerification(onVerificationError: (String error) {
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) {
            return SimpleDialog(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        error,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 78.0),
                  child: Button(
                    size: ButtonSize.medium,
                    text: "OK",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          },
        );
      }),
      onExpansionChanged: () {
        GamePlayAnalytics().onKycPanClicked(context, source: "kyc");
        _setSelectedItem(3);
      },
      subtitle: Text(getStringForStatus(verificationStatus.panRemark, 1) ?? ""),
      title: VerificationTitle(
        heading: "PAN",
        status: getPanStatus(verificationStatus),
        notVerifiedStatusText: "Submit",
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return LoaderContainer(
      child: Scaffold(
        appBar: CommonAppBar(
          children: [
            Text(
              "KYC".toUpperCase(),
            ),
          ],
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 8),
            ),
            Consumer<User>(
              builder: (context, user, child) {
                return ChangeNotifierProvider.value(
                  value: user.verificationStatus,
                  child: Consumer<VerificationStatus>(
                    builder: (context, verificationStatus, child) {
                      return Expanded(
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 8),
                          child: SingleChildScrollView(
                            child: Column(
                              children: <Widget>[
                                getEmailWidget(verificationStatus, user),
                                getPhoneWidget(verificationStatus, user),
                                getKYCWidget(verificationStatus, user),
                                getPANWidget(verificationStatus, user)
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
