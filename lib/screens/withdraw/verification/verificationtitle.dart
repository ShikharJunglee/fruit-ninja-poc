import 'package:flutter/material.dart';

class VerificationTitle extends StatelessWidget {
  final String heading;
  final String subheading;
  final int status;
  final String notVerifiedStatusText;

  VerificationTitle({
    this.heading,
    this.subheading,
    this.status,
    this.notVerifiedStatusText = "Not Verified",
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                child: Text(
                  subheading != null ? "$heading\n$subheading" : "$heading",
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                        color: Colors.grey.shade800,
                        fontSize: 18.0,
                      ),
                ),
              ),
            ],
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  status == 1
                      ? "Verified"
                      : status == 2
                          ? "Under Review"
                          : status == 3
                              ? "Rejected"
                              : status == 0
                                  ? notVerifiedStatusText
                                  : "Submitted",
                  style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontStyle: FontStyle.italic,
                    decoration: TextDecoration.underline,
                    fontSize: 18.0,
                    color: status == 1
                        ? Color.fromRGBO(0, 157, 4, 1)
                        : status == 2
                            ? Color.fromRGBO(255, 138, 0, 1)
                            : Colors.red,
                  ),
                ),
                status == 1
                    ? Container(
                        padding: EdgeInsets.symmetric(horizontal: 8),
                      )
                    : Container(),
                status == 1
                    ? Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(0, 157, 4, 1),
                          borderRadius: BorderRadius.circular(16),
                        ),
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 20,
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
