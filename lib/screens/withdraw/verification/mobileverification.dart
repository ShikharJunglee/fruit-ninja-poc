import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/api/kyc/mobileverificationAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:provider/provider.dart';

const int OTP_RESEND_TIME = 30;

class MobileVerification extends StatefulWidget {
  final Function onVerificationError;

  MobileVerification({this.onVerificationError});

  @override
  _MobileVerificationState createState() => _MobileVerificationState();
}

class _MobileVerificationState extends State<MobileVerification> {
  Timer _timer;
  int _currentTimeLapse;
  bool _bIsOTPSent = false;

  FocusNode _mobileFocusNode;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController _otpController = TextEditingController();
  final TextEditingController _mobileController = TextEditingController();

  MobileVerificationAPI _mobileVerificationAPI = MobileVerificationAPI();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
    _mobileFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    _mobileFocusNode.dispose();
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  void startTimer() {
    _currentTimeLapse = OTP_RESEND_TIME;
    const oneSec = const Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_currentTimeLapse < 1) {
            timer.cancel();
          } else {
            _currentTimeLapse = _currentTimeLapse - 1;
          }
        },
      ),
    );
  }

  void setData(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    if (user.mobile != null) {
      _mobileController.text = user.mobile;
    }
  }

  void onSendOTP(BuildContext ccontext, String mobile) async {
    Map<String, dynamic> result = await _mobileVerificationAPI.sendOTP(
      context,
      mobile: _mobileController.text,
      shouldUpdate: _mobileController.text != mobile,
    );

    if (!result["error"]) {
      startTimer();
      setState(() {
        _bIsOTPSent = true;
      });
    } else {
      widget.onVerificationError(result["message"]);
    }
  }

  void onResendOTP(BuildContext context) async {
    Map<String, dynamic> result = await _mobileVerificationAPI
        .resendOTP(context, mobile: _mobileController.text);

    if (!result["error"]) {
      startTimer();
      setState(() {
        _bIsOTPSent = true;
      });
    } else {
      widget.onVerificationError("Getting error while resending OTP.");
    }
  }

  void onVerifyOTP() async {
    User user = Provider.of<User>(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;

    Map<String, dynamic> result = await _mobileVerificationAPI
        .verifyOTP(context, otp: _otpController.text);

    if (!result["error"]) {
      user.mobile = _mobileController.text;
      verificationStatus.updateMobileVerificationStatus(true);
    } else {
      widget.onVerificationError(result["message"]);
    }
  }

  void editNumber() {
    setState(() {
      _bIsOTPSent = false;
      _otpController.clear();
      _currentTimeLapse = OTP_RESEND_TIME;
      _timer.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!_bIsOTPSent) _mobileFocusNode.requestFocus();
    return Column(
      children: <Widget>[
        Form(
          key: formKey,
          child: Consumer<User>(
            builder: (context, user, child) {
              return ChangeNotifierProvider.value(
                value: user.verificationStatus,
                child: Consumer<VerificationStatus>(
                  builder: (ctx, verificationStatus, child) {
                    return Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(bottom: 16.0),
                              child: Stack(
                                children: <Widget>[
                                  SimpleTextBox(
                                    focusNode: _mobileFocusNode,
                                    controller: _mobileController,
                                    isDense: true,
                                    labelText: "Enter mobile number",
                                    keyboardType: TextInputType.phone,
                                    enabled:
                                        !verificationStatus.mobileVerified &&
                                            !_bIsOTPSent,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(
                                        10,
                                      ),
                                      FilteringTextInputFormatter.deny(
                                        RegExp("[\/,. -/#\$]"),
                                      ),
                                    ],
                                    validator: (value) {
                                      if (value.isEmpty || value.length < 10) {
                                        return "Provide valid mobile number for verification";
                                      }
                                      return null;
                                    },
                                  ),
                                  _bIsOTPSent
                                      ? Align(
                                          alignment: Alignment(1, 0),
                                          child: IconButton(
                                            padding: EdgeInsets.only(top: 4.0),
                                            icon: Icon(Icons.edit),
                                            onPressed: editNumber,
                                          ),
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                            _bIsOTPSent
                                ? SimpleTextBox(
                                    controller: _otpController,
                                    keyboardType: TextInputType.number,
                                    isDense: true,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(
                                        6,
                                      ),
                                      FilteringTextInputFormatter.deny(
                                        RegExp("[\/,. -/#\$]"),
                                      ),
                                    ],
                                    labelText: "Enter OTP",
                                    suffixIcon: _currentTimeLapse > 0
                                        ? FlatButton(
                                            padding: EdgeInsets.only(top: 4.0),
                                            child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                Text("RETRY "),
                                                Text(
                                                  "${_currentTimeLapse}S",
                                                  style: TextStyle(
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            onPressed: null,
                                          )
                                        : FlatButton(
                                            padding: EdgeInsets.only(top: 4.0),
                                            child: Text("RESEND"),
                                            onPressed: () {
                                              onResendOTP(context);
                                            },
                                          ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter OTP.';
                                      }
                                      return null;
                                    },
                                  )
                                : Container(),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 8.0,
                                left: 0.0,
                                right: 0.0,
                              ),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      "You will receive an OTP on this number.",
                                      style: TextStyle(color: Colors.black54),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Button(
                                    text: !_bIsOTPSent
                                        ? "Send OTP".toUpperCase()
                                        : "VERIFY".toUpperCase(),
                                    onPressed: () {
                                      User user = Provider.of<User>(context,
                                          listen: false);
                                      if (formKey.currentState.validate()) {
                                        if (_bIsOTPSent) {
                                          onVerifyOTP();
                                        } else {
                                          onSendOTP(context, user.mobile);
                                        }
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    );
                  },
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
