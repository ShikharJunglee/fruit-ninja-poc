import 'dart:io';
import 'package:flutter/material.dart';
import 'package:permission/permission.dart';
import 'package:solitaire_gold/api/kyc/kycverificationAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/dropdown.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/dochelper.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import 'package:path/path.dart' as path;
import 'package:solitaire_gold/utils/storagepermission.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class KycVerification extends StatefulWidget {
  final Function onVerificationError;

  KycVerification({this.onVerificationError});

  @override
  _KycVerificationState createState() => _KycVerificationState();
}

class _KycVerificationState extends State<KycVerification> {
  File _addressImage;
  File _addressBackImage;
  bool reUpload = false;
  bool upLoaded = false;
  StoragePermission _permission = StoragePermission();

  List<dynamic> _addressList = [];
  bool _bShowImageUploadError = false;
  String _selectedAddressDocType = "";

  bool bAllowUpload = true;
  int allowdDocSizeInMB = 5;

  KYCVerificationAPI _kycVerificationAPI = KYCVerificationAPI();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));

    super.initState();
  }

  setData(BuildContext context) {
    setAddressList(context);
  }

  String getStringForStatus(String status) {
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;

    if (status != "DEFAULT_MSG" && status != KYCVerificationStatus.VERIFIED) {
      return verificationStatus.messages[status] ?? "";
    } else {
      String addressStatus = verificationStatus.addressVerificationStatus;
      String msg = "";
      if (addressStatus == KYCVerificationStatus.SUBMITTED) {
        msg = DocVerificationMessages.DOC_SUBMITTED;
      } else if (addressStatus == KYCVerificationStatus.UNDER_REVIEW) {
        msg = AddressVerificationMessages.UNDER_REVIEW;
      } else if (addressStatus == KYCVerificationStatus.REJECTED) {
        msg = AddressVerificationMessages.DOC_REJECTED;
      } else if (addressStatus == KYCVerificationStatus.VERIFIED) {
        msg = AddressVerificationMessages.VERIFIED;
      } else if (addressStatus == KYCVerificationStatus.MAX_ATTEMPTS) {
        msg = AddressVerificationMessages.MAX_ATTEMPTS;
      }
      return msg;
    }
  }

  setAddressList(BuildContext context) async {
    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> result =
        await _kycVerificationAPI.getAddressList(context);

    Loader().showLoader(false);

    if (result["error"] == false) {
      setState(() {
        _addressList = result["addressList"];
        _selectedAddressDocType = _addressList[0]["name"];
      });
    } else {
      widget.onVerificationError(
          "Getting error while getting address document list.");
    }
  }

  Future getImage(Function callback) async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, maxWidth: 1200);
    bool isDocsValidSize = true;
    var docLength = await image.length();
    if ((docLength) >= (allowdDocSizeInMB * 1024 * 1024)) {
      isDocsValidSize = false;
    }
    if (image != null) {
      if (isDocsValidSize) {
        callback(image);
      } else {
        widget.onVerificationError("Doc size should be less than " +
            allowdDocSizeInMB.toString() +
            "MB");
      }
    }
  }

  Future getAddressImage() async {
    getImage((File image) {
      setState(() {
        _addressImage = image;
        _bShowImageUploadError = false;
      });
      GamePlayAnalytics().onKycAddFrontSelected(
        context,
        source: "kyc",
        addType: _selectedAddressDocType,
        frontFile:
            _addressImage != null ? _addressImage.path.split("/").last : "",
        backFile: _addressBackImage != null
            ? _addressBackImage.path.split("/").last
            : "",
      );
    });
  }

  Future getAddressBackCopyImage() async {
    getImage((File image) {
      setState(() {
        _addressBackImage = image;
        _bShowImageUploadError = false;
      });
      GamePlayAnalytics().onKycAddBackSelected(
        context,
        source: "kyc",
        addType: _selectedAddressDocType,
        frontFile:
            _addressImage != null ? _addressImage.path.split("/").last : "",
        backFile: _addressBackImage != null
            ? _addressBackImage.path.split("/").last
            : "",
      );
    });
  }

  _onUploadDocuments() async {
    if (!WebSocket().isConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection();
        },
      );
    }

    GamePlayAnalytics().onKycAddUploadClicked(
      context,
      source: "kyc",
      addType: _selectedAddressDocType,
      frontFile:
          _addressImage != null ? _addressImage.path.split("/").last : "",
      backFile: _addressBackImage != null
          ? _addressBackImage.path.split("/").last
          : "",
    );
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;

    if (_addressImage == null) {
      setState(() {
        widget.onVerificationError("No file has been selected");
      });
    } else {
      Loader().showLoader(true, immediate: true);
      upLoaded = true;
      setState(() {});
      Map<String, dynamic> result = await _kycVerificationAPI.onUploadDocuments(
        context,
        addressImage: _addressImage,
        addressBackImage: _addressBackImage,
        addressDocType: _selectedAddressDocType,
      );
      if (!result["error"]) {
        verificationStatus.updateAddress(result["body"]["addressVerification"],
            result["body"]["address_verification_remark"]);
        GamePlayAnalytics().onKycAddUploaded(
          context,
          source: "kyc",
          addType: _selectedAddressDocType,
          frontFile:
              _addressImage != null ? _addressImage.path.split("/").last : "",
          backFile: _addressBackImage != null
              ? _addressBackImage.path.split("/").last
              : "",
        );
      } else {
        if (result["message"] != null)
          widget.onVerificationError(result["message"]);

        _kycVerificationAPI.getVerificationStatus(context);
      }
      Loader().showLoader(false);
      setState(() {
        reUpload = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Consumer<User>(
          builder: (context, user, child) {
            return ChangeNotifierProvider.value(
              value: user.verificationStatus,
              child: Consumer<VerificationStatus>(
                builder: (context, verificationStatus, child) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ((verificationStatus.addressVerificationStatus ==
                                      KYCVerificationStatus.NOT_SUBMITTED ||
                                  reUpload) ||
                              (verificationStatus.addressVerificationStatus ==
                                      KYCVerificationStatus.UNDER_REVIEW) &&
                                  !reUpload)
                          ? (!reUpload &&
                                  verificationStatus
                                          .addressVerificationStatus !=
                                      KYCVerificationStatus.NOT_SUBMITTED)
                              ? Column(children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Button(
                                          text: "reupload".toUpperCase(),
                                          onPressed: () {
                                            setState(() {
                                              reUpload = true;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Flexible(
                                        child: Text(
                                          getStringForStatus(
                                              verificationStatus.kycRemark),
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  255, 138, 0, 1),
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  )
                                ])
                              : Form(
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        0.0, 0.0, 0.0, 0.0),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 8.0),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                child: Dropdown(
                                                  dropDownItems: _addressList,
                                                  valueKey: "value",
                                                  label: _addressList.length > 0
                                                      ? _addressList[0]["value"]
                                                      : "Document Type",
                                                  onSelectCallback:
                                                      (Map<String, dynamic>
                                                          selectedItem) {
                                                    GamePlayAnalytics()
                                                        .onKycAddTypeSelected(
                                                      context,
                                                      source: "kyc",
                                                      addType:
                                                          selectedItem["value"],
                                                    );
                                                    setState(() {
                                                      _selectedAddressDocType =
                                                          selectedItem["name"];
                                                    });
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: 4.0, bottom: 4.0),
                                          child: Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  Flexible(
                                                    fit: FlexFit.loose,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        InkWell(
                                                          onTap: () async {
                                                            GamePlayAnalytics()
                                                                .onKycAddFrontClicked(
                                                              context,
                                                              source: "kyc",
                                                              addType:
                                                                  _selectedAddressDocType,
                                                              frontFile: _addressImage !=
                                                                      null
                                                                  ? _addressImage
                                                                      .path
                                                                      .split(
                                                                          "/")
                                                                      .last
                                                                  : "",
                                                              backFile: _addressBackImage !=
                                                                      null
                                                                  ? _addressBackImage
                                                                      .path
                                                                      .split(
                                                                          "/")
                                                                      .last
                                                                  : "",
                                                            );
                                                            if (!(PermissionStatus
                                                                    .allow ==
                                                                await _permission
                                                                    .checkForPermission())) {
                                                              return;
                                                            }
                                                            getAddressImage();
                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: Colors.grey
                                                                  .shade200,
                                                              border:
                                                                  Border.all(
                                                                width: 1.0,
                                                                color: Colors
                                                                    .grey
                                                                    .shade500,
                                                              ),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8.0),
                                                            ),
                                                            padding: EdgeInsets
                                                                .symmetric(
                                                                    horizontal:
                                                                        8.0,
                                                                    vertical:
                                                                        8.0),
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              8.0),
                                                                  child: Image
                                                                      .asset(
                                                                    "images/kyc/image-icon.png",
                                                                    height:
                                                                        20.0,
                                                                  ),
                                                                ),
                                                                FittedBox(
                                                                  child: Text(
                                                                    "Select Front",
                                                                    style: Theme.of(
                                                                            context)
                                                                        .primaryTextTheme
                                                                        .subtitle1
                                                                        .copyWith(
                                                                          color: Colors
                                                                              .grey
                                                                              .shade800,
                                                                          fontSize:
                                                                              18.0,
                                                                          fontWeight:
                                                                              FontWeight.w500,
                                                                        ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 20,
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 4.0),
                                                          child:
                                                              _addressImage !=
                                                                      null
                                                                  ? Text(
                                                                      path.basename(
                                                                          _addressImage
                                                                              .path),
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    )
                                                                  : Container(),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 8.0,
                                                  ),
                                                  Flexible(
                                                    fit: FlexFit.loose,
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        InkWell(
                                                          onTap: () async {
                                                            GamePlayAnalytics()
                                                                .onKycAddBackClicked(
                                                              context,
                                                              source: "kyc",
                                                              addType:
                                                                  _selectedAddressDocType,
                                                              frontFile: _addressImage !=
                                                                      null
                                                                  ? _addressImage
                                                                      .path
                                                                      .split(
                                                                          "/")
                                                                      .last
                                                                  : "",
                                                              backFile: _addressBackImage !=
                                                                      null
                                                                  ? _addressBackImage
                                                                      .path
                                                                      .split(
                                                                          "/")
                                                                      .last
                                                                  : "",
                                                            );
                                                            if (!(PermissionStatus
                                                                    .allow ==
                                                                await _permission
                                                                    .checkForPermission())) {
                                                              return;
                                                            }
                                                            getAddressBackCopyImage();
                                                            setState(() {});
                                                          },
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: Colors.grey
                                                                  .shade200,
                                                              border:
                                                                  Border.all(
                                                                width: 1.0,
                                                                color: Colors
                                                                    .grey
                                                                    .shade500,
                                                              ),
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          8.0),
                                                            ),
                                                            padding: EdgeInsets
                                                                .symmetric(
                                                                    horizontal:
                                                                        8.0,
                                                                    vertical:
                                                                        8.0),
                                                            child: Row(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          right:
                                                                              8.0),
                                                                  child: Image
                                                                      .asset(
                                                                    "images/kyc/image-icon.png",
                                                                    height:
                                                                        20.0,
                                                                  ),
                                                                ),
                                                                FittedBox(
                                                                  child: Text(
                                                                    "Select Back",
                                                                    style: Theme.of(
                                                                            context)
                                                                        .primaryTextTheme
                                                                        .subtitle1
                                                                        .copyWith(
                                                                          color: Colors
                                                                              .grey
                                                                              .shade800,
                                                                          fontSize:
                                                                              18.0,
                                                                          fontWeight:
                                                                              FontWeight.w500,
                                                                        ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          height: 20,
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  top: 4.0),
                                                          child:
                                                              _addressBackImage !=
                                                                      null
                                                                  ? Text(
                                                                      path.basename(
                                                                          _addressBackImage
                                                                              .path),
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    )
                                                                  : Container(),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        _bShowImageUploadError
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 8.0, bottom: 8.0),
                                                child: Row(
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Text(
                                                        "Please select address proof document both bothsides.",
                                                        style: TextStyle(
                                                            color: Theme.of(
                                                                    context)
                                                                .errorColor),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : Container(),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 8.0, bottom: 8.0),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                child: Text(
                                                  verificationStatus
                                                              .addressVerificationStatus ==
                                                          KYCVerificationStatus
                                                              .NOT_SUBMITTED
                                                      ? "Upload both sides of aadhaar card or any other address proof where your name, date of birth & address is clearly visible."
                                                      : "",
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .indicatorColor),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 0.0, bottom: 8.0),
                                                child: Button(
                                                  text: "UPLOAD".toUpperCase(),
                                                  onPressed: () {
                                                    _onUploadDocuments();
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        verificationStatus
                                                    .addressVerificationStatus !=
                                                KYCVerificationStatus
                                                    .NOT_SUBMITTED
                                            ? Row(
                                                children: <Widget>[
                                                  Flexible(
                                                    child: Text(
                                                      getStringForStatus(
                                                              verificationStatus
                                                                  .kycRemark) ??
                                                          "",
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              255, 138, 0, 1),
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            : Container()
                                      ],
                                    ),
                                  ),
                                )
                          : Row(
                              children: <Widget>[
                                Flexible(
                                  child: Text(
                                    getStringForStatus(
                                        verificationStatus.kycRemark),
                                    style: TextStyle(
                                        color: Color.fromRGBO(255, 138, 0, 1),
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                      (verificationStatus.addressVerificationStatus ==
                                  KYCVerificationStatus.REJECTED ||
                              verificationStatus.addressVerificationStatus ==
                                  KYCVerificationStatus.MAX_ATTEMPTS)
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: FlatButton(
                                    padding: EdgeInsets.all(0),
                                    child: RichText(
                                      text: TextSpan(
                                        text:
                                            "If your account is not verified please contact ",
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 11),
                                        children: <TextSpan>[
                                          TextSpan(
                                            text:
                                                "support@mailssolitairegold.in",
                                            style: TextStyle(
                                                color: Colors.blue,
                                                fontStyle: FontStyle.italic,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                    onPressed: () async {
                                      await routeManager
                                          .launchContactUs(context);
                                    },
                                  ),
                                ),
                              ],
                            )
                          : Container(),
                    ],
                  );
                },
              ),
            );
          },
        ),
      ],
    );
  }
}

class KYCVerificationStatus {
  static const SUBMITTED = "DOC_SUBMITTED";
  static const NOT_SUBMITTED = "DOC_NOT_SUBMITTED";
  static const UNDER_REVIEW = "UNDER_REVIEW";
  static const VERIFIED = "VERIFIED";
  static const REJECTED = "DOC_REJECTED";
  static const PAN_REQUIRED = "PAN_REQUIRED";
  static const MAX_ATTEMPTS = "MAX_ATTEMPTS";
}
