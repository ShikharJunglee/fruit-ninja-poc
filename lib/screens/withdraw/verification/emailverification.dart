import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/kyc/emailverificationAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:provider/provider.dart';

class EmailVerification extends StatefulWidget {
  @override
  _EmailVerificationState createState() => _EmailVerificationState();
}

class _EmailVerificationState extends State<EmailVerification> {
  bool _bIsMailSent = false;
  String _emailVerificationError;
  final TextEditingController _emailController = TextEditingController();

  EmailVerificationAPI _emailVerificationAPI = EmailVerificationAPI();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
    super.initState();
  }

  void setData(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    if (user.email != null) {
      _emailController.text = user.email;
    }
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  void onVerify(String email) async {
    if (_emailController.text == "" || _emailController.text == null) {
      setState(() {
        _emailVerificationError = "Please enter email address.";
      });
    } else if (!validateEmail(_emailController.text)) {
      setState(() {
        _emailVerificationError = "Please enter valid email address.";
      });
    } else {
      Map<String, dynamic> result =
          await _emailVerificationAPI.sendVerificationEmail(
        context,
        email: _emailController.text,
        shouldUpdate: _emailController.text != email,
      );

      setState(() {
        if (result["error"] == true) {
          _emailVerificationError = result["message"];
        } else {
          _bIsMailSent = true;
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Consumer<User>(
        builder: (context, user, child) {
          return ChangeNotifierProvider.value(
            value: user.verificationStatus,
            child: Consumer<VerificationStatus>(
              builder: (context, verificationStatus, child) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    !verificationStatus.emailVerified
                        ? _bIsMailSent
                            ? Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Text(
                                      "Verification mail sent successfully. Please check your mail and visit verification link to verify your email.",
                                      style: TextStyle(fontSize: 14),
                                      maxLines: 3,
                                    ),
                                  ),
                                ],
                              )
                            : Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    child: SimpleTextBox(
                                      controller: _emailController,
                                      labelText: "Enter email address",
                                      isDense: true,
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                  ),
                                  _emailVerificationError == null
                                      ? Container()
                                      : Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 8.0, 0.0, 8.0),
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                _emailVerificationError,
                                                style: TextStyle(
                                                  color: Theme.of(context)
                                                      .errorColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 8.0,
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "You will receive verification link on this email address.",
                                          style:
                                              TextStyle(color: Colors.black54),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Expanded(
                                        child: Button(
                                          text: "VERIFY".toUpperCase(),
                                          onPressed: () {
                                            onVerify(user.email);
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              )
                        : Container(),
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }
}
