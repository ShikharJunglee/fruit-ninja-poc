import 'package:flutter/material.dart';
import 'package:solitaire_gold/api/kyc/kycverificationAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/verificationstatus.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/withdraw/withdraw/dochelper.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';

import 'kycverification.dart';

class PanVerification extends StatefulWidget {
  final Function onVerificationError;
  PanVerification({this.onVerificationError});
  @override
  _PanVerificationState createState() => _PanVerificationState();
}

class _PanVerificationState extends State<PanVerification> {
  final TextEditingController _panController = TextEditingController();
  final KYCVerificationAPI _kycVerificationAPI = KYCVerificationAPI();

  @override
  void initState() {
    super.initState();
  }

  bool validatePan(String value) {
    RegExp regex = RegExp("([A-Z]){5}([0-9]){4}([A-Z]){1}");
    if (!regex.hasMatch(value)) {
      widget.onVerificationError("Enter A Valid Pan Number");
      return false;
    } else
      return true;
  }

  String getStringForStatus(String status) {
    User user = Provider.of(context, listen: false);
    VerificationStatus verificationStatus = user.verificationStatus;

    if (status != "DEFAULT_MSG" && status != KYCVerificationStatus.VERIFIED) {
      return verificationStatus.messages[status] ?? "";
    } else {
      String panStatus = verificationStatus.panVerificationStatus;
      String msg = "";
      if (panStatus == KYCVerificationStatus.SUBMITTED) {
        msg = DocVerificationMessages.DOC_SUBMITTED;
      } else if (panStatus == KYCVerificationStatus.UNDER_REVIEW) {
        msg = PanVerificationMessages.UNDER_REVIEW;
      } else if (panStatus == KYCVerificationStatus.REJECTED) {
        msg = PanVerificationMessages.DOC_REJECTED;
      } else if (panStatus == KYCVerificationStatus.VERIFIED) {
        msg = PanVerificationMessages.VERIFIED;
      } else if (panStatus == KYCVerificationStatus.MAX_ATTEMPTS) {
        msg = PanVerificationMessages.MAX_ATTEMPTS;
      }
      return msg;
    }
  }

  void onVerify(String pan) async {
    GamePlayAnalytics().onKycPanSubmit(
      context,
      source: "kyc",
      pan: _panController.text,
    );
    if (validatePan(pan)) {
      Loader().showLoader(true, immediate: true);
      Map<String, dynamic> result = await _kycVerificationAPI.onSendPan(
        context,
        panNumber: _panController.text.toUpperCase(),
      );

      if (result["error"] == true) {
        await _kycVerificationAPI.getVerificationStatus(context);

        widget.onVerificationError(result["msg"]);
      } else {
        VerificationStatus verificationStatus =
            Provider.of(context, listen: false);
        verificationStatus.updatePanStatus(
          result["pan_status"],
          result["pan_remark"],
        );
        GamePlayAnalytics().onKycPanSubmitted(
          context,
          source: "kyc",
          pan: _panController.text,
        );
      }

      Loader().showLoader(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Consumer<User>(
        builder: (context, user, child) {
          return ChangeNotifierProvider.value(
            value: user.verificationStatus,
            child: Consumer<VerificationStatus>(
              builder: (context, verificationStatus, child) {
                return Column(mainAxisSize: MainAxisSize.min, children: <
                    Widget>[
                  !(verificationStatus.panVerificationStatus ==
                          KYCVerificationStatus.VERIFIED)
                      ? (verificationStatus.panVerificationStatus ==
                                      KYCVerificationStatus.UNDER_REVIEW ||
                                  verificationStatus.panVerificationStatus ==
                                      KYCVerificationStatus.PAN_REQUIRED) ||
                              verificationStatus.panVerificationStatus ==
                                  KYCVerificationStatus.NOT_SUBMITTED
                          ? Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Container(
                                  child: SimpleTextBox(
                                    controller: _panController,
                                    isDense: true,
                                    maxLength: 10,
                                    textCapitalization:
                                        TextCapitalization.characters,
                                    labelText: 'Enter PAN Number',
                                  ),
                                ),
                                verificationStatus.panVerificationStatus == null
                                    ? Container()
                                    : Padding(
                                        padding: EdgeInsets.fromLTRB(
                                            0.0, 2.0, 0.0, 2.0),
                                      ),
                                Row(
                                  children: <Widget>[
                                    Text(
                                      "Submit your PAN for verification",
                                      style: TextStyle(color: Colors.black54),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      child: Button(
                                        text: (verificationStatus
                                                    .panVerificationStatus !=
                                                KYCVerificationStatus
                                                    .UNDER_REVIEW)
                                            ? "VERIFY".toUpperCase()
                                            : "Resubmit".toUpperCase(),
                                        onPressed: () {
                                          onVerify(_panController.text);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          : Container()
                      : Container(),
                  verificationStatus.panVerificationStatus !=
                              KYCVerificationStatus.PAN_REQUIRED &&
                          verificationStatus.panVerificationStatus !=
                              KYCVerificationStatus.NOT_SUBMITTED
                      ? Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                  child: Text(
                                getStringForStatus(
                                        verificationStatus.panRemark) ??
                                    "",
                                style: TextStyle(
                                    color: Color.fromRGBO(255, 138, 0, 1),
                                    fontWeight: FontWeight.bold),
                              )),
                            ),
                          ],
                        )
                      : Container(),
                  (verificationStatus.panVerificationStatus ==
                              KYCVerificationStatus.REJECTED ||
                          verificationStatus.panVerificationStatus ==
                              KYCVerificationStatus.MAX_ATTEMPTS)
                      ? Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  child: RichText(
                                    text: TextSpan(
                                      text:
                                          "If your account is not verified please contact ",
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 11),
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: "support@mailssolitairegold.in",
                                          style: TextStyle(
                                              color: Colors.blue,
                                              fontStyle: FontStyle.italic,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onPressed: () async {
                                    await routeManager.launchContactUs(context);
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      : Container()
                ]);
              },
            ),
          );
        },
      ),
    );
  }
}
