import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/api/kyc/withdrawAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';

class SSNPopup extends StatefulWidget {
  final int ssnAmount;

  SSNPopup({this.ssnAmount});

  @override
  _SSNPopupState createState() => _SSNPopupState();
}

class _SSNPopupState extends State<SSNPopup> {
  WithdrawAPI _withdrawAPI = WithdrawAPI();

  final formKey = new GlobalKey<FormState>();

  TextEditingController ssnController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Row(
        children: [Text("User Info".toUpperCase())],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("SSN verification is mandatory as your"),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("withdrawal crosses \$${widget.ssnAmount}"),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
              child: Form(
                key: formKey,
                child: SimpleTextBox(
                  controller: ssnController,
                  hintText: "Enter 9 digit SSN",
                  inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                  keyboardType: TextInputType.number,
                  maxLength: 9,
                  validator: (value) {
                    if (value.length < 9) {
                      return "Invalid";
                    }
                    if (value.startsWith(new RegExp(r"[000,666]"))) {
                      return "Invalid";
                    }
                    return null;
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 64.0),
              child: Button(
                size: ButtonSize.medium,
                text: "submit".toUpperCase(),
                onPressed: () async {
                  if (formKey.currentState.validate()) {
                    String ssn = ssnController.text.substring(0, 3) +
                        "-" +
                        ssnController.text.substring(3, 5) +
                        "-" +
                        ssnController.text.substring(5);
                    Map<String, dynamic> result =
                        await _withdrawAPI.submitSSN(context, ssn);
                    if (result["error"]) {
                      showDialog(
                        barrierColor: Color.fromARGB(175, 0, 0, 0),
                        context: context,
                        builder: (BuildContext context) {
                          return SimpleDialog(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  result["message"],
                                  textAlign: TextAlign.center,
                                  softWrap: true,
                                  maxLines: 3,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .subtitle1
                                      .copyWith(
                                        color: Colors.grey.shade700,
                                      ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 78.0),
                                child: Button(
                                  size: ButtonSize.medium,
                                  text: "OK",
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ),
                            ],
                          );
                        },
                      );
                    } else {
                      Navigator.pop(context, true);
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
