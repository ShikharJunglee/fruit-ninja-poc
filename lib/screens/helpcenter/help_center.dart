import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/lobby/account/accounts_card.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:provider/provider.dart';

class HelpCenter extends StatelessWidget {
  final List<dynamic> _items = [
    {
      "icon": "images/appdrawer/legal.png",
      "title": "Legal",
      "cta": "legal",
    },
    {
      "icon": "images/appdrawer/contact_us.png",
      "title": "Contact Us",
      "cta": "rap",
    },
    {
      "icon": "images/appdrawer/faq.png",
      "title": "FAQ",
      "cta": "faq",
    }
  ];

  void onItemClicked(BuildContext context, String cta) {
    switch (cta) {
      case "legal":
        _onLegal(context);
        break;
      case "rap":
        _onContactUs(context);
        break;
      case "faq":
        _onFAQ(context);
        break;
    }
  }

  _onLegal(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    config.staticPageConfig.forEach((conf) {
      if (conf["name"].toLowerCase() == "legal".toLowerCase()) {
        routeManager.launchStaticPage(context,
            url: conf["data"]["url"], title: conf["data"]["title"]);
      }
    });
  }

  _onContactUs(BuildContext context) {
    routeManager.launchContactUs(context);
  }

  _onFAQ(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    config.staticPageConfig.forEach((conf) {
      if (conf["name"].toLowerCase() == "faq".toLowerCase()) {
        routeManager.launchStaticPage(context,
            url: conf["data"]["url"], title: conf["data"]["title"]);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonAppBar(children: [
        Text(
          "HELP CENTER",
        ),
      ]),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
          itemCount: _items.length,
          itemBuilder: (context, index) => AccountsCard(
            item: _items[index],
            showElevation: true,
            onTap: () {
              onItemClicked(context, _items[index]["cta"]);
            },
          ),
        ),
      ),
    );
  }
}
