import 'dart:io';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/analytics/referandearnevents.dart';
import 'package:solitaire_gold/api/banner/banner.dart';
import 'package:solitaire_gold/api/banner/banner_zone.dart';
import 'package:solitaire_gold/commonwidgets/carousel.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/leadingbutton.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/raf/bonus_distribution.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:permission/permission.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/loader.dart';

class EarnCashV2 extends StatefulWidget {
  final Map<String, dynamic> data;

  EarnCashV2({this.data});

  @override
  EarnCashV2State createState() {
    return EarnCashV2State();
  }
}

class EarnCashV2State extends State<EarnCashV2> with BasePage {
  int refAAmount = 0;
  int refBAmount = 0;
  String cookie = "";
  String refCode = "";
  Map<String, dynamic> inviteUrls;
  String inviteMsg = "";
  List<dynamic> topBanners = [];
  List<dynamic> bottomBanners = [];

  BannerAPI _bannerAPI = BannerAPI();

  @override
  void initState() {
    super.initState();

    _setReferralDetails();
    if (Platform.isIOS) {
      initSocialShareChannel();
    }

    WidgetsBinding.instance.addPostFrameCallback((_) => _init(context));
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFLoaded(context, amount: refAAmount, referalCode: refCode);
  }

  void _init(BuildContext context) async {
    Map<String, dynamic> banners =
        await _bannerAPI.getBanners(context, zone: BannerZone.RAF_BANNERS);
    Loader().showLoader(false);
    Map<String, dynamic> testimonials = await _bannerAPI.getBanners(context,
        zone: BannerZone.RAF_TESTINONIAL_BANNERS);

    if (banners["error"] == false)
      setState(() {
        topBanners = banners["data"];
      });
    if (testimonials["error"] == false)
      setState(() {
        bottomBanners = testimonials["data"];
      });
    try {
      MyWebEngageService().webengageAddScreenData(context: context, data: {
        "screenName": "earncash",
        "data": {
          "refCode": refCode,
        },
      });
      MyWebEngageService().webengageRAFClickedEvent({
        "refCode": refCode,
      });
    } catch (e) {}
  }

  _setReferralDetails() {
    refCode = widget.data["refDetails"]["refCode"];
    refAAmount = widget.data["refDetails"]["amountUserA"];
    refBAmount = widget.data["refDetails"]["amountUserB"];
    inviteUrls = widget.data["refDetails"]["refLink"];
  }

  _copyCode() {
    ReferAndEarnEvents.referAndEarnEvents.onRAFCopyCodeClicked(context,
        amount: refAAmount, referalCode: refCode);
    Clipboard.setData(
      ClipboardData(text: refCode),
    );
    showMessageOnTop(context, msg: "COPIED");
  }

  Future<void> initSocialShareChannel() async {
    try {
      await ChannelManager.SOCIAL_SHARE.invokeMethod('initSocialShareChannel');
    } catch (e) {}
  }

  Future<void> _shareNowViaWhatsAppApplication(String msg) async {
    try {
      await ChannelManager.SOCIAL_SHARE.invokeMethod('shareViaWhatsApp', msg);
    } catch (e) {
      _shareNowViaSystemApplication(msg);
    }
  }

  Future<void> _shareNowViaSystemApplication(String msg) async {
    try {
      await ChannelManager.SOCIAL_SHARE.invokeMethod('shareText', msg);
    } catch (e) {}
  }

  _shareNowWhatsApp() {
    ReferAndEarnEvents.referAndEarnEvents.onRAFWhatsAppClicked(context,
        amount: refAAmount, referalCode: refCode);
    inviteMsg =
        "Hi! Let's play Solitaire. It's Fast, Fun and also we can win Cash. Use this link to download and get a free bonus of ${CurrencyFormat.format(refBAmount.toDouble())} \n ${inviteUrls["whatsapp"].replaceAll("%3d", "=")}";

    _shareNowViaWhatsAppApplication(inviteMsg);
  }

  _shareNow() {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFMoreClicked(context, amount: refAAmount, referalCode: refCode);
    inviteMsg =
        "Hi! Let's play Solitaire. It's Fast, Fun and also we can win Cash. Use this link to download and get a free bonus of ${CurrencyFormat.format(refBAmount.toDouble())} \n ${inviteUrls["whatsapp"].replaceAll("%3d", "=")}";

    _shareNowViaSystemApplication(inviteMsg);
  }

  checkForPermission() async {
    List<Permissions> permissions =
        await Permission.getPermissionsStatus([PermissionName.Contacts]);

    if (permissions[0].permissionStatus != PermissionStatus.allow) {
      ReferAndEarnEvents.referAndEarnEvents.onSmsContactLoaded(context);
      return await askForPermission();
    }
    return true;
  }

  askForPermission() async {
    final result =
        await Permission.requestPermissions([PermissionName.Contacts]);

    if (result != null) {
      return result[0].permissionStatus == PermissionStatus.allow;
    }
    return false;
  }

  checkForPermissionAndOpenContacts(
      BuildContext context, String refCode, int refAAmount) async {
    if (Platform.isAndroid) {
      bool permissionGranted = await checkForPermission();
      if (!permissionGranted) {
        showMessageOnTop(context,
            msg:
                "Please grant permissions to your contacts to send Free Invites.");
        return null;
      } else {
        ReferAndEarnEvents.referAndEarnEvents.onSmsContactAllowed(context);
      }
    }

    Iterable<Contact> contacts =
        await ContactsService.getContacts(withThumbnails: false);
    if (contacts != null) {
      routeManager.launchSelectContacts(context,
          contacts: contacts.toList(growable: false),
          refCode: refCode,
          refAmount: refAAmount);
    }
  }

  _shareNowSMS() async {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFSmsClicked(context, amount: refAAmount, referalCode: refCode);
    // String smsRestriction = await SharedPrefHelper()
    //     .getFromSharedPref(URLS.SHARED_PREFERENCE_SMS_RESTRICTION);
    // int lastSendSMS = 0;
    // if (smsRestriction != null) {
    //   lastSendSMS = int.parse(smsRestriction);
    // }

    // if (widget.data["smsRestrictionDuration"] <
    //     DateTime.now().millisecondsSinceEpoch) {
    checkForPermissionAndOpenContacts(context, refCode, refAAmount);
    // } else {
    //   showMessageOnTop(
    //     context,
    //     "You have reached the max Free SMS limit for the day. Please try again later.",
    //   );
    // }
  }

  showBonusDistribution() async {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFHelpClicked(context, amount: refAAmount, referalCode: refCode);
    await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      builder: (BuildContext context) {
        return BonusDistribution(
          amount: refAAmount,
          bonusDistribution: widget.data["bonusDistribution"],
        );
      },
    );
    ReferAndEarnEvents.referAndEarnEvents.onRAFHelpPopupClosed(context);
  }

  Future<bool> backEvent() {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFBackClicked(context, amount: refAAmount, referalCode: refCode);
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => backEvent(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: CommonAppBar(
          leading: LeadingButton(
            onPressed: () {
              backEvent();
              Navigator.of(context).pop();
            },
          ),
          children: <Widget>[
            Text(
              "Refer & Earn".toUpperCase(),
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width * 490 / 885,
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: Carousel(
                carousel: topBanners,
                aspectRatio: 885 / 490,
              ),
            ),
            widget.data["refDetails"]["howItWorksEnabled"]
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 0.0),
                        child: InkWell(
                          onTap: () {
                            showBonusDistribution();
                          },
                          child: Text(
                            "How it works?",
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromRGBO(4, 99, 177, 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : Container(),
            Expanded(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      "Invite Friends Via",
                      style: TextStyle(
                          fontSize: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .fontSize),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: getShareIconItems(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        if (widget.data["showCopyCode"])
                          Column(
                            children: <Widget>[
                              InkWell(
                                child: Image.asset(
                                  "images/rafv2/copy.png",
                                  height: 36.0,
                                ),
                                onTap: () {
                                  _copyCode();
                                },
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text("Copy Code"),
                              )
                            ],
                          ),
                        if (widget.data["showCopyCode"])
                          Container(
                            width: 48.0,
                          ),
                        Column(
                          children: <Widget>[
                            IconButton(
                              icon: Image.asset(
                                "images/rafv2/more.png",
                                height: 36.0,
                              ),
                              onPressed: () {
                                _shareNow();
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Text("More"),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              margin: const EdgeInsets.only(bottom: 48.0),
              child: Carousel(
                carousel: bottomBanners,
              ),
            ),
          ],
        ),
      ),
    );
  }

  getShareIconItems() {
    return [
      Expanded(
        child: FittedBox(
          alignment: Alignment.centerRight,
          fit: BoxFit.scaleDown,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              getWhatsAppIcon(),
            ],
          ),
        ),
      ),
      Container(
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        child: Image.asset(
          "images/rafv2/or-icon.png",
          height: 24.0,
        ),
      ),
      Expanded(
        child: FittedBox(
          fit: BoxFit.scaleDown,
          alignment: Alignment.centerLeft,
          child: Row(
            children: <Widget>[
              getFreeSMSIcon(),
            ],
          ),
        ),
      ),
    ];
  }

  Widget getMoreIcon() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 12.0, right: 12.0),
          child: IconButton(
            padding: EdgeInsets.all(0.0),
            iconSize: 48.0,
            onPressed: () {
              _shareNow();
            },
            icon: Image.asset(
              "images/rafv2/more.png",
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text("More"),
        )
      ],
    );
  }

  Widget getWhatsAppIcon() {
    return Container(
      constraints: BoxConstraints(minWidth: 140.0, minHeight: 44.0),
      child: RaisedButton(
        onPressed: () {
          _shareNowWhatsApp();
        },
        elevation: 0.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24.0),
        ),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Image.asset(
                "images/rafv2/whatsapp.png",
                height: 24.0,
              ),
            ),
            Text(
              "WhatsApp",
              style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                    color: Colors.white,
                    fontWeight: FontWeight.w500,
                  ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getFreeSMSIcon() {
    return Container(
      constraints: BoxConstraints(minWidth: 140.0, minHeight: 44.0),
      child: OutlineButton(
        onPressed: () {
          _shareNowSMS();
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24.0),
        ),
        highlightedBorderColor: Theme.of(context).buttonColor,
        borderSide: BorderSide(
          width: 2.0,
          color: Theme.of(context).buttonColor,
        ),
        child: Row(
          children: <Widget>[
            Text(
              "Free SMS",
              style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                    color: Theme.of(context).buttonColor,
                    fontWeight: FontWeight.w500,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
