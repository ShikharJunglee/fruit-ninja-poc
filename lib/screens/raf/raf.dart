import 'dart:io';
import 'package:contacts_service/contacts_service.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/analytics/referandearnevents.dart';
import 'package:solitaire_gold/api/banner/banner.dart';
import 'package:solitaire_gold/api/banner/banner_zone.dart';
import 'package:solitaire_gold/commonwidgets/carousel.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/leadingbutton.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/raf/bonus_distribution.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:permission/permission.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/loader.dart';

class EarnCash extends StatefulWidget {
  final Map<String, dynamic> data;

  EarnCash({this.data});

  @override
  EarnCashState createState() {
    return EarnCashState();
  }
}

class EarnCashState extends State<EarnCash> with BasePage {
  int refAAmount = 0;
  int refBAmount = 0;
  String cookie = "";
  String refCode = "";
  Map<String, dynamic> inviteUrls;
  String inviteMsg = "";
  List<dynamic> topBanners = [];
  List<dynamic> bottomBanners = [];

  BannerAPI _bannerAPI = BannerAPI();

  @override
  void initState() {
    super.initState();
    Loader().showLoader(false);
    _setReferralDetails();
    if (Platform.isIOS) {
      initSocialShareChannel();
    }

    WidgetsBinding.instance.addPostFrameCallback((_) => _init(context));
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFLoaded(context, amount: refAAmount, referalCode: refCode);
  }

  void _init(BuildContext context) async {
    Map<String, dynamic> banners =
        await _bannerAPI.getBanners(context, zone: BannerZone.RAF_BANNERS);
    Map<String, dynamic> testimonials = await _bannerAPI.getBanners(context,
        zone: BannerZone.RAF_TESTINONIAL_BANNERS);

    if (banners["error"] == false)
      setState(() {
        topBanners = banners["data"];
      });
    if (testimonials["error"] == false)
      setState(() {
        bottomBanners = testimonials["data"];
      });

    try {
      MyWebEngageService().webengageAddScreenData(context: context, data: {
        "screenName": "earncash",
        "data": {
          "refCode": refCode,
        },
      });
      MyWebEngageService().webengageRAFClickedEvent({
        "refCode": refCode,
      });
    } catch (e) {}
  }

  _setReferralDetails() {
    refCode = widget.data["refDetails"]["refCode"];
    refAAmount = widget.data["refDetails"]["amountUserA"];
    refBAmount = widget.data["refDetails"]["amountUserB"];
    inviteUrls = widget.data["refDetails"]["refLink"];
  }

  _copyCode() {
    ReferAndEarnEvents.referAndEarnEvents.onRAFCopyCodeClicked(context,
        amount: refAAmount, referalCode: refCode);
    Clipboard.setData(
      ClipboardData(text: refCode),
    );
    showMessageOnTop(context, msg: "COPIED");
  }

  Future<void> initSocialShareChannel() async {
    try {
      await ChannelManager.SOCIAL_SHARE.invokeMethod('initSocialShareChannel');
    } catch (e) {}
  }

  Future<void> _shareNowViaWhatsAppApplication(String msg) async {
    try {
      await ChannelManager.SOCIAL_SHARE.invokeMethod('shareViaWhatsApp', msg);
    } catch (e) {
      _shareNowViaSystemApplication(msg);
    }
  }

  Future<void> _shareNowViaSystemApplication(String msg) async {
    try {
      await ChannelManager.SOCIAL_SHARE.invokeMethod('shareText', msg);
    } catch (e) {}
  }

  _shareNowWhatsApp() {
    ReferAndEarnEvents.referAndEarnEvents.onRAFWhatsAppClicked(context,
        amount: refAAmount, referalCode: refCode);
    inviteMsg =
        "Hi! Let's play Solitaire. It's Fast, Fun and also we can win Cash. Use this link to download and get a free bonus of ${CurrencyFormat.format(refBAmount.toDouble())} \n ${inviteUrls["whatsapp"].replaceAll("%3d", "=")}";

    _shareNowViaWhatsAppApplication(inviteMsg);
  }

  _shareNow() {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFMoreClicked(context, amount: refAAmount, referalCode: refCode);
    inviteMsg =
        "Hi! Let's play Solitaire. It's Fast, Fun and also we can win Cash. Use this link to download and get a free bonus of ${CurrencyFormat.format(refBAmount.toDouble())} \n ${inviteUrls["whatsapp"].replaceAll("%3d", "=")}";

    _shareNowViaSystemApplication(inviteMsg);
  }

  _shareNowTelegram() {
    ReferAndEarnEvents.referAndEarnEvents.onRAFTelegramClicked(context,
        amount: refAAmount, referalCode: refCode);
    inviteMsg =
        "Hi! Let's play Solitaire. It's Fast, Fun and also we can win Cash. Use this link to download and get a free bonus of ${CurrencyFormat.format(refBAmount.toDouble())} \n ${inviteUrls["whatsapp"].replaceAll("%3d", "=")}";

    _shareNowViaTelegramApplication(inviteMsg);
  }

  Future<void> _shareNowViaTelegramApplication(String msg) async {
    try {
      await ChannelManager.SOCIAL_SHARE.invokeMethod('shareViaTelegram', msg);
    } catch (e) {
      _shareNowViaSystemApplication(msg);
    }
  }

  checkForPermission() async {
    List<Permissions> permissions =
        await Permission.getPermissionsStatus([PermissionName.Contacts]);

    if (permissions[0].permissionStatus != PermissionStatus.allow) {
      ReferAndEarnEvents.referAndEarnEvents.onSmsContactLoaded(context);
      return await askForPermission();
    }
    return true;
  }

  askForPermission() async {
    final result =
        await Permission.requestPermissions([PermissionName.Contacts]);

    if (result != null) {
      return result[0].permissionStatus == PermissionStatus.allow;
    }
    return false;
  }

  checkForPermissionAndOpenContacts(
      BuildContext context, String refCode, int refAAmount) async {
    if (Platform.isAndroid) {
      bool permissionGranted = await checkForPermission();
      if (!permissionGranted) {
        showMessageOnTop(context,
            msg:
                "Please grant permissions to your contacts to send Free Invites.");
        return null;
      } else {
        ReferAndEarnEvents.referAndEarnEvents.onSmsContactAllowed(context);
      }
    }

    Iterable<Contact> contacts =
        await ContactsService.getContacts(withThumbnails: false);

    List<Contact> lstContacts = contacts.toList();

    if (contacts != null) {
      routeManager.launchSelectContacts(
        context,
        contacts: lstContacts,
        refCode: refCode,
        refAmount: refAAmount,
      );
    }
  }

  _shareNowSMS() async {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFSmsClicked(context, amount: refAAmount, referalCode: refCode);
    // String smsRestriction = await SharedPrefHelper()
    //     .getFromSharedPref(URLS.SHARED_PREFERENCE_SMS_RESTRICTION);
    // int lastSendSMS = 0;
    // if (smsRestriction != null) {
    //   lastSendSMS = int.parse(smsRestriction);
    // }

    // if (widget.data["smsRestrictionDuration"] <
    //     DateTime.now().millisecondsSinceEpoch) {
    checkForPermissionAndOpenContacts(context, refCode, refAAmount);
    // } else {
    //   showMessageOnTop(
    //     context,
    //     "You have reached the max Free SMS limit for the day. Please try again later.",
    //   );
    // }
  }

  showBonusDistribution() async {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFHelpClicked(context, amount: refAAmount, referalCode: refCode);
    await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      builder: (BuildContext context) {
        return BonusDistribution(
          amount: refAAmount,
          bonusDistribution: widget.data["bonusDistribution"],
        );
      },
    );
    ReferAndEarnEvents.referAndEarnEvents.onRAFHelpPopupClosed(context);
  }

  Future<bool> backEvent() {
    ReferAndEarnEvents.referAndEarnEvents
        .onRAFBackClicked(context, amount: refAAmount, referalCode: refCode);
    return Future.value(true);
  }

  Widget getCopyCodeWidget() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(
            "Share Code",
            style: TextStyle(
                fontSize:
                    Theme.of(context).primaryTextTheme.subtitle1.fontSize),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 32.0, right: 32.0, top: 8.0),
          child: getCopyCode(),
        ),
      ],
    );
  }

  Widget getCopyCode() {
    return DottedBorder(
      color: Color.fromRGBO(49, 116, 171, 1),
      dashPattern: [6.0, 2.0],
      borderType: BorderType.RRect,
      padding: EdgeInsets.all(0.0),
      radius: Radius.circular(8.0),
      strokeWidth: 1.0,
      child: Container(
        width: 220.0,
        height: 36.0,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Color.fromRGBO(240, 250, 242, 1)),
        child: InkWell(
          onTap: () {
            _copyCode();
          },
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      refCode,
                      style:
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                letterSpacing: 2.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600,
                              ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(3.0),
                    child: Image.asset(
                      "images/raf/copy-icon.png",
                      height: 32.0,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  getShareIconItems() {
    bool showFreeSMSInviteFeature = false;
    if (showFreeSMSInviteFeature) {
      return [
        getWhatsAppIcon(),
        getTelegramIcon(),
        getMoreIcon(),
      ];
    }

    return [
      getWhatsAppIcon(),
      getTelegramIcon(),
      getFreeSMSIcon(),
      getMoreIcon(),
    ];
  }

  Widget getMoreIcon() {
    return Column(
      children: <Widget>[
        IconButton(
          iconSize: 48.0,
          onPressed: () {
            _shareNow();
          },
          icon: Container(
            child: Image.asset(
              "images/raf/more-icon.png",
            ),
          ),
        ),
        Text("More"),
      ],
    );
  }

  Widget getWhatsAppIcon() {
    return Column(
      children: <Widget>[
        IconButton(
          onPressed: () {
            _shareNowWhatsApp();
          },
          iconSize: 48.0,
          icon: Container(
            child: Image.asset(
              "images/raf/whatsapp-icon.png",
            ),
          ),
        ),
        Text("WhatsApp"),
      ],
    );
  }

  Widget getTelegramIcon() {
    return Column(
      children: <Widget>[
        IconButton(
          onPressed: () {
            _shareNowTelegram();
          },
          iconSize: 48.0,
          icon: Container(
            child: Image.asset(
              "images/raf/telegram-icon.png",
            ),
          ),
        ),
        Text("Telegram"),
      ],
    );
  }

  Widget getFreeSMSIcon() {
    return Column(
      children: <Widget>[
        IconButton(
          onPressed: () {
            _shareNowSMS();
          },
          iconSize: 48.0,
          icon: Container(
            child: Image.asset(
              "images/raf/message-icon.png",
            ),
          ),
        ),
        Text("Free SMS"),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => backEvent(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: CommonAppBar(
          leading: LeadingButton(
            onPressed: () {
              backEvent();
              Navigator.of(context).pop();
            },
          ),
          children: <Widget>[
            Text(
              "Refer & Earn".toUpperCase(),
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width * 490 / 885,
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: Carousel(
                carousel: topBanners,
                aspectRatio: 885 / 490,
                customPlaceholder: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width * 490 / 885,
                ),
              ),
            ),
            widget.data["refDetails"]["howItWorksEnabled"]
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 0.0),
                        child: InkWell(
                          onTap: () {
                            showBonusDistribution();
                          },
                          child: Text(
                            "How it works?",
                            style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromRGBO(4, 99, 177, 1),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : Container,
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  widget.data["showCopyCode"]
                      ? getCopyCodeWidget()
                      : Container(),
                  Padding(
                    padding: EdgeInsets.only(bottom: 8.0, top: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: getShareIconItems(),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              margin: const EdgeInsets.only(bottom: 48.0),
              child: Carousel(
                carousel: bottomBanners,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
