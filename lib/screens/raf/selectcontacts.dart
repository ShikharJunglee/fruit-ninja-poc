import 'dart:io';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_package_manager/flutter_package_manager.dart';
import 'package:solitaire_gold/analytics/referandearnevents.dart';
import 'package:solitaire_gold/api/raf/earncashAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/leadingbutton.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/screens/raf/contactlistitem.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/loader.dart';

class SelectContacts extends StatefulWidget {
  final List<Contact> contacts;
  final String refCode;
  final int bonusAmount;

  SelectContacts({this.contacts, this.refCode, this.bonusAmount});

  @override
  SelectContactsState createState() {
    return new SelectContactsState();
  }
}

class SelectContactsState extends State<SelectContacts> with BasePage {
  List<String> arrSelectedNumbers = [];
  List<String> arrInviteSent = [];
  String searchString = "";
  bool isIos;
  bool bSent = false;
  TextEditingController textController = TextEditingController();
  bool isSearchEnabled = true;

  int allContactCount = 0;

  List<Contact> contacts = [];
  List<Contact> filteredContacts = [];
  List<dynamic> arrAccountHolders = [];

  EarnCashAPI _earnCashAPI = EarnCashAPI();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setData());
  }

  setData() {
    if (Platform.isIOS) {
      isIos = true;
    }

    filterContacts();

    textController.addListener(() {
      setState(() {
        searchString = textController.text;
      });
    });

    saveInstalledAppList();
    saveContacts(widget.contacts);
  }

  Future<bool> _onWillPop() async {
    ReferAndEarnEvents().onFreeInviteBackLoaded(context);
    return (await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (context) => new AlertDialog(
            contentPadding: EdgeInsets.all(0),
            title: new Text(
                'Are you sure you do not wish to invite more friends?'),
            content: Container(
              width: 0,
              height: 0,
            ),
            actions: <Widget>[
              new FlatButton(
                onPressed: () {
                  ReferAndEarnEvents().onFreeInviteNoClicked(context);
                  Navigator.of(context).pop(false);
                },
                child: new Text(
                  'No',
                  style: TextStyle(
                      fontSize: Theme.of(context)
                          .primaryTextTheme
                          .headline6
                          .fontSize),
                ),
              ),
              new FlatButton(
                onPressed: () {
                  ReferAndEarnEvents().onFreeInviteYesClicked(context);
                  setState(() {
                    isSearchEnabled = false;
                  });
                  Navigator.of(context).pop(true);
                },
                child: new Text(
                  'Yes',
                  style: TextStyle(
                      fontSize: Theme.of(context)
                          .primaryTextTheme
                          .headline6
                          .fontSize),
                ),
              ),
            ],
          ),
        )) ??
        false;
  }

  getTrimmedNumber(String number) {
    return number.replaceAll(" ", "").replaceAll("+91", "");
  }

  filterContacts() async {
    List<String> arrPhoneNumbersToFilter = [];
    for (var i = 0; i < widget.contacts.length; i++) {
      for (var j = 0; j < widget.contacts[i].phones.length; j++) {
        List<Item> phones = widget.contacts[i].phones.toList();
        String strPhoneNumber = getTrimmedNumber(phones[j].value);
        if (!arrPhoneNumbersToFilter.contains(strPhoneNumber))
          arrPhoneNumbersToFilter.add(strPhoneNumber);
      }
    }

    Loader().showLoader(true, immediate: true);

    arrAccountHolders = await _earnCashAPI.getExistingContacts(
        context, arrPhoneNumbersToFilter);

    Loader().showLoader(false);

    filteredContacts = widget.contacts;
    selectAllNumbers();
    setState(() {});
    ReferAndEarnEvents.referAndEarnEvents.onFreeSmsLoaded(context,
        selectedAll: allContactCount == arrSelectedNumbers.length,
        totalBonus: widget.bonusAmount * allContactCount,
        noOfContatcs: allContactCount,
        existingHzContacts: arrAccountHolders.length);
  }

  saveInstalledAppList() async {
    if (!Platform.isAndroid) return null;
    Loader().showLoader(true, immediate: true);

    List packages = await FlutterPackageManager.getInstalledPackages();
    _earnCashAPI.saveInstalledApps(context, packages);

    Loader().showLoader(false);
  }

  saveContacts(List<Contact> contacts) {
    Loader().showLoader(true, immediate: true);

    List<dynamic> arrContact = [];
    for (int i = 0; i < contacts.length; i++) {
      List<Item> phones = contacts[i].phones.toList();
      if (phones.length > 0) {
        var obj = {};
        obj["contactName"] = contacts[i].displayName;
        obj["mobile"] = [];
        for (int j = 0; j < phones.length; j++) {
          if (!obj["mobile"].contains(getTrimmedNumber(phones[j].value)))
            obj["mobile"].add(getTrimmedNumber(phones[j].value));
        }
        arrContact.add(obj);
      }
    }
    _earnCashAPI.saveContactsApi(context, arrContact, widget.refCode);

    Loader().showLoader(false);
  }

  selectAllNumbers() {
    for (var i = 0; i < filteredContacts.length; i++) {
      for (var j = 0; j < filteredContacts[i].phones.length; j++) {
        List<Item> phones = filteredContacts[i].phones.toList();
        String strPhoneNumber = getTrimmedNumber(phones[j].value);
        if (!arrSelectedNumbers.contains(strPhoneNumber) &&
            !arrInviteSent.contains(strPhoneNumber) &&
            !arrAccountHolders.contains(strPhoneNumber))
          arrSelectedNumbers.add(strPhoneNumber);
      }
    }

    allContactCount = arrSelectedNumbers.length;
  }

  showLoader(BuildContext context, bool bShow) {
    Loader().showLoader(bShow);
  }

  List<Contact> searchTheContacts() {
    return filteredContacts.where((contact) {
      if (searchString == null || searchString.length == 0)
        return true;
      else if (contact.displayName != null &&
          contact.displayName
                  .toLowerCase()
                  .indexOf(searchString.toLowerCase()) >=
              0) {
        return true;
      }
      for (Item phone in contact.phones.toList()) {
        if (phone.value.indexOf(searchString) >= 0) {
          return true;
        }
      }
      return false;
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    contacts = searchTheContacts();
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: CommonAppBar(
          leading: LeadingButton(
            onPressed: () async {
              ReferAndEarnEvents().onFreeInviteBackButton(
                context,
                noOfContatcs: allContactCount,
                totalBonus: arrSelectedNumbers.length * widget.bonusAmount,
                selectedAll: arrSelectedNumbers.length == allContactCount,
                existingHzContacts: arrAccountHolders.length,
                selectedContactCount: arrSelectedNumbers.length,
              );
              var result = await _onWillPop();
              if (result) {
                Navigator.of(context).pop();
              }
            },
          ),
          children: getMainTitleBar(),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Column(
                children: <Widget>[
                  PreferredSize(
                    preferredSize: Size.fromHeight(32.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.grey.shade600,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.shade800,
                                  blurRadius: 1,
                                  spreadRadius: 0.5,
                                  offset: Offset(0, 1))
                            ],
                          ),
                          height: 32,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Container(
                                  padding: const EdgeInsets.only(left: 8),
                                  child: Row(
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "Name",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: Theme.of(context)
                                                    .primaryTextTheme
                                                    .subtitle2
                                                    .fontSize),
                                          ),
                                          Text(
                                            "(${arrSelectedNumbers.length.toString()})",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: Theme.of(context)
                                                    .primaryTextTheme
                                                    .subtitle2
                                                    .fontSize),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 8),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "Bonus",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: Theme.of(context)
                                                    .primaryTextTheme
                                                    .subtitle2
                                                    .fontSize),
                                          ),
                                          Text(
                                            "(${CurrencyFormat.format((arrSelectedNumbers.length * widget.bonusAmount).toDouble())})",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: Theme.of(context)
                                                    .primaryTextTheme
                                                    .subtitle2
                                                    .fontSize),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 4),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Checkbox(
                                        materialTapTargetSize:
                                            MaterialTapTargetSize.shrinkWrap,
                                        checkColor: Colors.white,
                                        tristate: true,
                                        activeColor:
                                            Theme.of(context).buttonColor,
                                        value: arrSelectedNumbers.length ==
                                                allContactCount
                                            ? true
                                            : (arrSelectedNumbers.length > 0
                                                ? null
                                                : false),
                                        onChanged: (bChecked) {
                                          setState(() {
                                            if (bChecked == null) {
                                              arrSelectedNumbers = [];
                                            } else if (bChecked) {
                                              selectAllNumbers();

                                              ReferAndEarnEvents().onSelectAll(
                                                context,
                                                noOfContatcs: allContactCount,
                                                totalBonus: allContactCount *
                                                    widget.bonusAmount,
                                                existingHzContacts:
                                                    arrAccountHolders.length,
                                              );
                                            } else {
                                              arrSelectedNumbers = [];
                                              ReferAndEarnEvents()
                                                  .onDeSelectAll(
                                                context,
                                                noOfContatcs: allContactCount,
                                                totalBonus: 0,
                                                existingHzContacts:
                                                    arrAccountHolders.length,
                                              );
                                            }
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    child: getContactListWidget(),
                  ),
                ],
              ),
            ),
          ],
        ),
        bottomNavigationBar: Container(
          width: 0,
          height: 0,
        ),
      ),
    );
  }

  Widget getBottomNavigationBar() {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(vertical: 8),
          width: MediaQuery.of(context).size.width * 0.7,
          height: 50,
          child: Button(
            onPressed: arrSelectedNumbers.length == 0 || bSent
                ? null
                : () {
                    sendInvites();
                  },
            text: "FREE INVITE",
            style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                ),
          ),
        ),
      ],
    );
  }

  Widget getContactListWidget() {
    return contacts.length == 0 && searchString.length > 0
        ? Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Text(
                      'No contacts found'.toUpperCase(),
                      style: TextStyle(
                          fontSize: Theme.of(context)
                              .primaryTextTheme
                              .headline6
                              .fontSize),
                    ),
                  ),
                ],
              ),
            ],
          )
        : (contacts.length == 0 && searchString.length == 0
            ? Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          'Fetching contacts'.toUpperCase(),
                          style: TextStyle(
                              fontSize: Theme.of(context)
                                  .primaryTextTheme
                                  .headline6
                                  .fontSize),
                        ),
                      ),
                    ],
                  ),
                ],
              )
            : Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: contacts.length,
                      itemBuilder: (context, index) {
                        return contacts[index].phones.length > 0
                            ? Container(
                                child: ContactListItem(
                                  accountHolders: arrAccountHolders,
                                  selectedNumbers: arrSelectedNumbers,
                                  sentInvites: arrInviteSent,
                                  bonusAmount: widget.bonusAmount,
                                  contact: contacts[index],
                                  onChanged: (strPhoneNumber, selected) {
                                    if (selected &&
                                        !arrSelectedNumbers
                                            .contains(strPhoneNumber)) {
                                      arrSelectedNumbers.add(strPhoneNumber);
                                      ReferAndEarnEvents()
                                          .onSingleCheckBoxSelected(
                                        context,
                                        noOfContatcs: allContactCount,
                                        totalBonus: arrSelectedNumbers.length *
                                            widget.bonusAmount,
                                        selectedAll:
                                            arrSelectedNumbers.length ==
                                                allContactCount,
                                        existingHzContacts:
                                            arrAccountHolders.length,
                                        selectedContactCount:
                                            arrSelectedNumbers.length,
                                      );
                                    }
                                    if (!selected) {
                                      arrSelectedNumbers.remove(strPhoneNumber);
                                      ReferAndEarnEvents()
                                          .onSingleCheckBoxDeSelected(
                                        context,
                                        noOfContatcs: arrSelectedNumbers.length,
                                        totalBonus: arrSelectedNumbers.length *
                                            widget.bonusAmount,
                                        selectedAll:
                                            arrSelectedNumbers.length ==
                                                allContactCount,
                                        existingHzContacts:
                                            arrAccountHolders.length,
                                        selectedContactCount:
                                            arrSelectedNumbers.length,
                                      );
                                    }

                                    setState(() {});
                                  },
                                ),
                              )
                            : Container();
                      },
                    ),
                  ),
                  getBottomNavigationBar(),
                ],
              ));
  }

  List<Widget> getMainTitleBar() {
    return [
      Text(
        "Invite".toUpperCase(),
      ),
      Container(
        height: 36,
        width: MediaQuery.of(context).size.width * 0.5,
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: SimpleTextBox(
          enabled: isSearchEnabled,
          isDense: true,
          prefixIcon: Icon(Icons.search),
          style: TextStyle(
            fontSize: Theme.of(context).primaryTextTheme.headline6.fontSize,
            color: Colors.grey.shade900,
          ),
          //labelText: "Search",
          labelStyle: TextStyle(
            fontSize: Theme.of(context).primaryTextTheme.subtitle1.fontSize,
          ),
          color: Colors.white,
          borderWidth: 1,
          borderColor: Colors.grey.shade500,
          focusedBorderColor: Theme.of(context).buttonColor,
          controller: textController,
          contentPadding: EdgeInsets.all(0.0),
        ),
      ),
    ];
  }

  Widget getInviteInfo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "You can earn ",
              style: TextStyle(
                color: Colors.grey.shade700,
                fontSize: Theme.of(context).primaryTextTheme.subtitle1.fontSize,
              ),
            ),
            Text(
              CurrencyFormat.format(
                      (arrSelectedNumbers.length * widget.bonusAmount)
                          .toDouble()) +
                  "/- ",
              style: TextStyle(
                color: Colors.grey.shade700,
                fontWeight: FontWeight.bold,
                fontSize: Theme.of(context).primaryTextTheme.headline5.fontSize,
              ),
            ),
            Text(
              "for ",
              style: TextStyle(
                color: Colors.grey.shade700,
                fontSize: Theme.of(context).primaryTextTheme.subtitle1.fontSize,
              ),
            ),
            Text(
              arrSelectedNumbers.length.toString(),
              style: TextStyle(
                color: Colors.grey.shade700,
                fontWeight: FontWeight.bold,
                fontSize: Theme.of(context).primaryTextTheme.headline6.fontSize,
              ),
            ),
            Text(
              " Contacts ",
              style: TextStyle(
                color: Colors.grey.shade700,
                fontSize: Theme.of(context).primaryTextTheme.subtitle1.fontSize,
              ),
            ),
          ],
        ),
      ],
    );
  }

  sendInvites() async {
    if (arrSelectedNumbers.length == allContactCount) {
      bSent = true;
      saveSMSRestrictionPreference();
      setState(() {});
    }
    ReferAndEarnEvents().onFreeInviteClicked(
      context,
      noOfContatcs: allContactCount,
      totalBonus: widget.bonusAmount * arrSelectedNumbers.length,
      selectedAll: arrSelectedNumbers.length == allContactCount,
      existingHzContacts: arrAccountHolders.length,
      selectedContactCount: arrSelectedNumbers.length,
    );

    Loader().showLoader(true, immediate: true);

    bool bSuccess = await _earnCashAPI.sendInvitesApi(
        context, arrSelectedNumbers, widget.refCode);

    Loader().showLoader(false);

    if (bSuccess) {
      showMessageOnTop(context, msg: "Invite sent successfully");
      arrInviteSent.addAll(arrSelectedNumbers);
      arrSelectedNumbers = [];
      setState(() {});
    } else {
      showMessageOnTop(context, msg: "Invite failed");
    }
  }

  saveSMSRestrictionPreference() async {
    // await SharedPrefHelper().saveToSharedPref(
    //     URLS.SHARED_PREFERENCE_SMS_RESTRICTION,
    //     new DateTime.now().millisecondsSinceEpoch.toString());
  }
}
