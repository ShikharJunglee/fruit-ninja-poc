import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class ContactListItem extends StatefulWidget {
  final List<String> selectedNumbers;
  final List<dynamic> accountHolders;
  final List<String> sentInvites;
  final int bonusAmount;
  final Contact contact;
  final Function onChanged;

  ContactListItem({
    this.contact,
    this.onChanged,
    this.selectedNumbers,
    this.accountHolders = const [],
    this.bonusAmount = 0,
    this.sentInvites = const [],
  });

  @override
  State<StatefulWidget> createState() {
    return new ContactListItemState();
  }
}

class ContactListItemState extends State<ContactListItem> {
  List<String> arrSelectedNumbers = [];

  getTrimmedNumber(String number) {
    return number.replaceAll(" ", "").replaceAll("+91", "");
  }

  @override
  Widget build(BuildContext context) {
    List<String> phones = [];
    widget.contact.phones.forEach((phone) {
      if (!phones.contains(getTrimmedNumber(phone.value)))
        phones.add(getTrimmedNumber(phone.value));
    });

    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: Colors.grey.shade500),
        ),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.only(left: 8, top: 18),
              child: Text(
                widget.contact.displayName ?? "",
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, top: 18),
              child: Text(
                CurrencyFormat.format(widget.bonusAmount.toDouble()),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: phones.map((phone) {
                  if (widget.accountHolders.contains(phone)) {
                    return getAlreadyHaveAccountMobileItem(phone);
                  }
                  return getMobileItem(phone);
                }).toList()),
          ),
        ],
      ),
    );
  }

  Widget getAlreadyHaveAccountMobileItem(String phone) {
    AppConfig config = Provider.of(context, listen: false);
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  phone,
                  style: TextStyle(color: Colors.grey.shade500),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Text(
                    "Already on ${config.appName}",
                    style: TextStyle(
                        color: Colors.green.shade500,
                        fontStyle: FontStyle.italic,
                        fontSize: 12.0),
                  ),
                ),
              ],
            ),
          ),
          Checkbox(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            activeColor: Theme.of(context).buttonColor,
            value: true,
            onChanged: null,
          ),
        ],
      ),
    );
  }

  Widget getMobileItem(String phone) {
    return InkWell(
      onTap: widget.sentInvites.contains(phone)
          ? null
          : () {
              setState(() {
                if (arrSelectedNumbers.contains(phone))
                  arrSelectedNumbers.remove(phone);
                else
                  arrSelectedNumbers.add(phone);
                widget.onChanged(phone, arrSelectedNumbers.contains(phone));
              });
            },
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              child: Text(
                phone,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.end,
                style: TextStyle(
                    color: widget.sentInvites.contains(phone)
                        ? Colors.grey.shade400
                        : Colors.black),
              ),
            ),
            Checkbox(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              activeColor: Theme.of(context).buttonColor,
              value: widget.selectedNumbers.contains(phone),
              onChanged: widget.sentInvites.contains(phone)
                  ? null
                  : (bChecked) {
                      setState(() {
                        if (bChecked && !arrSelectedNumbers.contains(phone))
                          arrSelectedNumbers.add(phone);

                        if (!bChecked && arrSelectedNumbers.contains(phone))
                          arrSelectedNumbers.remove(phone);

                        widget.onChanged(
                            phone, arrSelectedNumbers.contains(phone));
                      });
                    },
            ),
          ],
        ),
      ),
    );
  }
}
