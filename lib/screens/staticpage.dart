import 'dart:async';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:webview_flutter/webview_flutter.dart';

class StaticPage extends StatefulWidget {
  final String url;
  final String title;

  StaticPage({@required this.url, this.title});

  @override
  _StaticPageState createState() => _StaticPageState();
}

class _StaticPageState extends State<StaticPage> {
  final Completer<WebViewController> _futureWebviewController =
      Completer<WebViewController>();
  WebViewController webController;
  bool closeAllowed = false;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setData();
    });
    _futureWebviewController.future.then((webController) async {
      this.webController = webController;
    });
    super.initState();
  }

  setData() {
    Loader().showLoader(true, immediate: true);
  }

  @override
  void didUpdateWidget(StaticPage oldWidget) {
    Loader().showLoader(true, immediate: true);
    this.webController.loadUrl(widget.url);
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(closeAllowed);
      },
      child: LoaderContainer(
        child: Scaffold(
          appBar: widget.title == null
              ? PreferredSize(
                  child: Container(),
                  preferredSize: Size.fromHeight(0.0),
                )
              : CommonAppBar(
                  children: <Widget>[
                    Text(
                      widget.title.toUpperCase(),
                    ),
                  ],
                ),
          body: WebView(
            initialUrl: widget.url,
            debuggingEnabled: true,
            onWebViewCreated: (WebViewController webViewController) {
              _futureWebviewController.complete(webViewController);
            },
            javascriptMode: JavascriptMode.unrestricted,
            onPageStarted: (String url) {
              print('Page started loading: $url');
            },
            onPageFinished: (String url) {
              closeAllowed = true;
              Loader().showLoader(false);
            },
          ),
        ),
      ),
    );
  }
}
