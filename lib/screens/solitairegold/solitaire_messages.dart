enum MessageType {
  AnalyticsEvent,
  UnityOpen,
  UnityClose,
  UnityReady,
//Game config: url, cookie etc
  Init,
  GameEnd,
  GameStart,
  GameTableFTUEComplete,
  GameTableFTUESkipped,
  ShowUI,
}

extension MessageTypeString on MessageType {
  String getString() {
    return this.toString().replaceAll("MessageType.", "");
  }
}
