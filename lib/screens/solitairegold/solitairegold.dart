import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/screens/solitairegold/solitire_manager.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';

import '../../services/mywebengageservice.dart';

class SolitaireGold extends StatefulWidget {
  final SmallLeague league;
  final AsyncHeadsUp asyncHeadsUp;
  final bool isReplay;
  final int selectedTabIndex;
  final bool isFTUE;
  final int matchId;
  final int gameId;
  final int tieMatchId;
  final int formatType;

  SolitaireGold({
    this.isFTUE,
    this.league,
    this.matchId,
    this.gameId,
    this.tieMatchId = 0,
    this.formatType,
    this.asyncHeadsUp,
    this.isReplay = false,
    this.selectedTabIndex = 0,
  });

  static bool isLaunched = false;

  @override
  _SolitaireGoldState createState() => _SolitaireGoldState();
}

class _SolitaireGoldState extends State<SolitaireGold>
    with WidgetsBindingObserver, BasePage {
  SolitaireManager _solitaireManager = SolitaireManager();
  Completer<bool> _unityViewRendered = Completer();

  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      ModalRoute.of(context)
          .animation
          .addStatusListener(onAnimationStatusChange);
    });
    EventManager().publish(EventName.SOLITAIRE_LOADING);
    addWebEngageScreenData();
    try {
      EventManager().pauseNativeToFlutterStreamSubscriptionEvent();
      EventManager().closePushNotifFlash();
    } catch (e) {}
  }

  _onUnityController(UnityWidgetController _controller) async {
    _solitaireManager.registerController(_controller);

    _solitaireManager.setAppConfig(context);

    if (widget.isReplay) {
      _solitaireManager.startReplay(context,
          matchId: widget.matchId, gameId: widget.gameId);
    } else if (widget.isFTUE == true) {
      _solitaireManager.startInteractiveGame(context);
    } else {
      if (!_unityViewRendered.isCompleted) await _unityViewRendered.future;
      _solitaireManager.startNewGame(
        context,
        league: widget.league,
        tieMatchId: widget.tieMatchId,
        asyncHeadsUp: widget.asyncHeadsUp,
        selectedTabIndex: widget.selectedTabIndex,
        formatType: widget.formatType,
      );
    }
  }

  updateUserDetails() async {
    User user = Provider.of(context, listen: false);
    final result = await AddCashAPI().getUserDetails(context);
    if (!result["error"]) {
      user.copyFrom(result["data"]);
    }
  }

  void onAnimationStatusChange(status) {
    if (status == AnimationStatus.completed) {
      ModalRoute.of(context)
          .animation
          .removeStatusListener(onAnimationStatusChange);
      _unityViewRendered.complete(true);
    }
  }

  addWebEngageScreenData() {
    try {
      Map<String, dynamic> screendata = new Map();
      screendata["screenName"] = "gametable";
      MyWebEngageService().webengageAddScreenData(data: screendata);
    } catch (e) {}
  }

  @override
  void dispose() {
    SolitaireGold.isLaunched = false;
    WidgetsBinding.instance.removeObserver(this);
    EventManager().resumeNativeToFlutterStreamSubscriptionEvent();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused ||
        state == AppLifecycleState.inactive) {
      _solitaireManager.pause();
    } else if (state == AppLifecycleState.resumed) {
      _solitaireManager.resume();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        ModalRoute.of(context)
            .animation
            .removeStatusListener(onAnimationStatusChange);
        return Future.value(false);
      },
      child: SafeArea(
        child: Stack(
          children: [
            UnityWidget(
              isARScene: false,
              onUnityViewCreated: (controller) {
                _onUnityController(controller);
              },
              onUnityMessage: (controller, message) {
                if (mounted) {
                  _solitaireManager.onUnityMessage(context, message);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
