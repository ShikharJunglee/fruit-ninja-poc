import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';
import 'package:solitaire_gold/utils/websocket/websocket.dart';

class Disconnection extends StatefulWidget {
  final bool useWebSocketForDisconnectionDetection;

  Disconnection({this.useWebSocketForDisconnectionDetection = true});

  @override
  _DisconnectionState createState() => _DisconnectionState();
}

class _DisconnectionState extends State<Disconnection> {
  StreamSubscription<dynamic> wsSubscription;

  @override
  void initState() {
    super.initState();
    wsSubscription =
        EventManager().subscribe(EventName.WEBSOCKET_CONNECTION, (data) {
      closeDialog();
    });
  }

  @override
  void dispose() {
    wsSubscription.cancel();
    super.dispose();
  }

  void closeDialog() async {
    if (widget.useWebSocketForDisconnectionDetection &&
        WebSocket().isConnected()) {
      Navigator.of(context).pop();
    } else if (!widget.useWebSocketForDisconnectionDetection) {
      var connectivityResult = await Connectivity().checkConnectivity();
      if (connectivityResult == ConnectivityResult.mobile ||
          connectivityResult == ConnectivityResult.wifi) {
        // connected
        Navigator.of(context).pop();
      }
    }
  }

  Widget getRetryButton(BuildContext context, Function onPressed) {
    String buttonType = ButtonType.primary.toString().split(".")[1];
    String buttonSize = ButtonSize.medium.toString().split(".")[1];
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }
    return CupertinoButton(
      onPressed: onPressed,
      borderRadius: BorderRadius.circular(6.0),
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              child: onPressed != null
                  ? Image.asset(
                      // "images/splash.png",
                      "images/buttons/button-$buttonType-$buttonSize.png",
                      fit: BoxFit.fill,
                    )
                  : Image.asset(
                      "images/buttons/button-disabled-$buttonSize.png",
                      fit: BoxFit.fill,
                    ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Text(
                "RETRY".toUpperCase(),
                style: ctaStyle,
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: CommonDialogFrame(
          canClose: false,
          titleAsset: "images/titles/disconnected.png",
          child: Padding(
            padding: const EdgeInsets.only(top: 48.0, left: 48.0, right: 48.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Check your internet connection and retry.",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                        color: Colors.white,
                      ),
                ),
                SizedBox(
                  height: 16.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0, bottom: 12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        constraints: BoxConstraints(maxWidth: 180.0),
                        child: getRetryButton(
                          context,
                          () {
                            closeDialog();
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 8.0),
    );
  }
}
