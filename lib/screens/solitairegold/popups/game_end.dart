import 'dart:async';

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/leaderboard.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class GameEnd extends StatefulWidget {
  final int score;
  final int timeBonus;
  final int matchId;
  final SmallLeague league;
  final AsyncHeadsUp headsUp;
  final Leaderboard leaderboard;
  final int gameId;

  GameEnd({
    this.score,
    this.timeBonus,
    this.matchId,
    this.league,
    this.headsUp,
    this.leaderboard,
    this.gameId,
  });

  @override
  GameEndState createState() => GameEndState();
}

class GameEndState extends State<GameEnd> {
  int loadingPercent;
  Timer timer;

  @override
  void initState() {
    loadingPercent = 0;
    this.startTimer();
    GamePlayAnalytics().onGameOverLoaded(
      context,
      source: "gt",
      gameId: widget.gameId,
      matchId: widget.matchId,
      headsUp: widget.headsUp,
      league: widget.league,
      leaderboard: widget.leaderboard,
      score: widget.score,
      timeBonus: widget.timeBonus,
    );
    super.initState();
  }

  @override
  void dispose() {
    if (timer != null) {
      timer.cancel();
    }
    super.dispose();
  }

  startTimer() {
    timer = new Timer.periodic(new Duration(milliseconds: 100), (timer) {
      setState(() {
        loadingPercent += 2;
      });
      if (loadingPercent >= 100) {
        loadingPercent = 0;
        submit();
        timer.cancel();
      }
    });
  }

  submit() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;

    TextStyle style = Theme.of(context).primaryTextTheme.headline4.copyWith(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        );

    if (width < 320) {
      style = Theme.of(context).primaryTextTheme.headline6.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          );
    }

    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }

    return CustomDialog(
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            CommonDialogFrame(
              canClose: false,
              titleAsset: "images/titles/gameover.png",
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 48,
                  right: 48,
                  top: 60,
                  bottom: 20,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Your Score",
                          style: style,
                        ),
                        Text(
                          widget.score.toString(),
                          style: style,
                        ),
                      ],
                    ),
                    SizedBox(height: 16.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Time Bonus",
                          style: style,
                        ),
                        Text(
                          widget.timeBonus.toString(),
                          style: style,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Divider(
                      height: 2.0,
                      color: Colors.brown.shade100,
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Total Score",
                          style: style,
                        ),
                        Text(
                          (widget.score + widget.timeBonus).toString(),
                          style: style,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 16.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 24.0, bottom: 0.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            constraints: BoxConstraints(
                                maxWidth: width < 320 ? 180.0 : 200),
                            child: Stack(
                              children: <Widget>[
                                ClipRRect(
                                  clipBehavior: Clip.hardEdge,
                                  borderRadius: BorderRadius.circular(4.0),
                                  child: LinearPercentIndicator(
                                    lineHeight: 60.0,
                                    animation: true,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    animateFromLastPercent: true,
                                    animationDuration: 100,
                                    padding: EdgeInsets.only(
                                      top: 4.0,
                                      right: 6,
                                      left: 4,
                                    ),
                                    backgroundColor: Colors.green.shade500,
                                    percent: loadingPercent / 100,
                                    linearGradient: LinearGradient(
                                      colors: [
                                        Colors.grey.shade900,
                                        Colors.grey.shade900,
                                        Colors.grey.shade900,
                                        Colors.grey.shade900,
                                        Colors.grey.shade900,
                                      ],
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                    ),
                                  ),
                                ),
                                Opacity(
                                  opacity: 0.8,
                                  child: Button(
                                    size: ButtonSize.medium,
                                    text: "SUBMIT SCORE".toUpperCase(),
                                    style: ctaStyle,
                                    onPressed: () {
                                      GamePlayAnalytics().onSubmitScoreClicked(
                                        context,
                                        source: "gt",
                                        headsUp: widget.headsUp,
                                        league: widget.league,
                                        matchId: widget.matchId,
                                        score: widget.score,
                                        timeBonus: widget.timeBonus,
                                        leaderboard: widget.leaderboard,
                                        gameId: widget.gameId,
                                      );
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              footer: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    "ID #${widget.matchId}",
                    style:
                        Theme.of(context).primaryTextTheme.bodyText1.copyWith(
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 8.0),
    );
  }
}
