import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/leaderboard.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';

class GamePause extends StatelessWidget {
  final int matchId;
  final AsyncHeadsUp asyncHeadsUp;
  final SmallLeague smallLeague;
  final int gameId;
  final Leaderboard leaderboard;
  GamePause({
    this.matchId,
    this.asyncHeadsUp,
    this.smallLeague,
    this.gameId,
    this.leaderboard,
  });
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 12.0;
    double maxWidth = 400.0;
    double width = screenWidth > maxWidth ? maxWidth : screenWidth;
    double paddingLeft = (screenWidth - width) / 2 + 12;
    TextStyle style = Theme.of(context).primaryTextTheme.headline4.copyWith(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        );

    if (width < 320) {
      style = Theme.of(context).primaryTextTheme.headline6.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          );
    }

    TextStyle ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
          fontSize: 30,
          color: Colors.white,
          fontWeight: FontWeight.w700,
        );
    if (width < 320) {
      ctaStyle = Theme.of(context).primaryTextTheme.headline5.copyWith(
            color: Colors.white,
            fontWeight: FontWeight.w700,
          );
    }

    return CustomDialog(
      dialog: Dialog(
        backgroundColor: Colors.transparent,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CommonDialogFrame(
              titleAsset: "images/titles/paused.png",
              child: Padding(
                padding: const EdgeInsets.only(top: 48.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 32.0, vertical: 20),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "Game paused, do you want to \nEnd the Game?",
                              style: style,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 32.0, bottom: 16.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Button(
                                size: ButtonSize.medium,
                                text: "Resume".toUpperCase(),
                                style: ctaStyle.copyWith(
                                    color: Colors.brown.shade800),
                                type: ButtonType.secondary,
                                onPressed: () {
                                  GamePlayAnalytics().onPlayOnClicked(
                                    context,
                                    source: "gt",
                                    league: smallLeague,
                                    headsUp: asyncHeadsUp,
                                    matchId: matchId,
                                    gameId: gameId,
                                    leaderboard: leaderboard,
                                  );
                                  Navigator.of(context).pop({
                                    "action": "resume",
                                  });
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Button(
                                size: ButtonSize.medium,
                                text: "End Now".toUpperCase(),
                                style: ctaStyle,
                                onPressed: () {
                                  GamePlayAnalytics().onEndNowClicked(
                                    context,
                                    source: "gt",
                                    league: smallLeague,
                                    headsUp: asyncHeadsUp,
                                    matchId: matchId,
                                    gameId: gameId,
                                    leaderboard: leaderboard,
                                  );
                                  Navigator.of(context).pop({
                                    "action": "end",
                                  });
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              footer: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: paddingLeft),
                    child: Text(
                      "Match Id: " + matchId.toString(),
                      style:
                          Theme.of(context).primaryTextTheme.bodyText1.copyWith(
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      padding: EdgeInsets.symmetric(horizontal: 8.0),
    );
  }
}
