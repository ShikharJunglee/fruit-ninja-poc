import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/event.dart';
import 'package:solitaire_gold/models/lobby/async_h2h.dart';
import 'package:solitaire_gold/models/lobby/small_league.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/solitairegold/game_start_data.dart';
import 'package:solitaire_gold/models/solitairegold/replay_data.dart';
import 'package:solitaire_gold/models/solitairegold/solitaire_initdata.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/popups/reportdialog.dart';
import 'package:solitaire_gold/screens/solitairegold/solitaire_messages.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedprefkeys.dart';

class SolitaireManager {
  SmallLeague _league;
  AsyncHeadsUp _asyncHeadsUp;
  UnityWidgetController _controller;
  int _selectedTabIndex;

  registerController(UnityWidgetController controller) {
    _controller = controller;
  }

  onUnityMessage(BuildContext context, message) async {
    Map<String, dynamic> unityMessage = {};
    try {
      unityMessage = json.decode(message);
    } catch (e) {
      print(e);
    }

    String type = unityMessage["type"].toString();

    if (type == MessageType.GameEnd.getString() ||
        type == MessageType.GameTableFTUESkipped.getString()) {
      Navigator.of(context).popUntil((r) => r.isFirst);
    } else if (type == MessageType.GameTableFTUESkipped.getString()) {
      Navigator.of(context).popUntil((r) => r.isFirst);
    } else if (type == MessageType.GameTableFTUEComplete.getString()) {
      SharedPrefHelper()
          .saveToSharedPref(SharedPrefKey.interactiveDemo, "false");
      Navigator.of(context).popUntil((r) => r.isFirst);
    } else if (type == MessageType.ShowUI.getString()) {
      onShowUI(context, json.decode(unityMessage["data"]));
    } else if (type == MessageType.AnalyticsEvent.getString()) {
      try {
        onAnalyticsEvent(context, json.decode(unityMessage["data"]));
      } catch (e) {
        print("Getting error while sending analytics event");
      }
    }
  }

  onShowUI(BuildContext context, Map<String, dynamic> dataObject) async {
    switch (dataObject["name"]) {
      case "RAP":
        await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return ReportDialog(
              matchId: _league != null
                  ? _league.tournamentId
                  : _asyncHeadsUp.h2hTemplateId,
              gameId: int.tryParse(dataObject["meta"]["game_id"]),
              source: "game_table",
            );
          },
        );
        break;
      default:
    }
  }

  onAnalyticsEvent(BuildContext context, Map<String, dynamic> metadata) {
    User user = Provider.of(context, listen: false);
    metadata = {
      ...metadata,
      "src": "game_table",
      "fu": user.isFirstDeposit,
      "tblid": _asyncHeadsUp != null
          ? 0
          : _league != null
              ? _league.tableId
              : 0,
      "eftype":
          _asyncHeadsUp != null ? _asyncHeadsUp.entryType : _league.entryType,
      "frmtype":
          _asyncHeadsUp != null ? _asyncHeadsUp.formatType : _league.formatType,
      "slt": _selectedTabIndex,
      "cef": _asyncHeadsUp != null
          ? _asyncHeadsUp.realCurrency
          : _league.realCurrency,
      "gbef": _asyncHeadsUp != null
          ? _asyncHeadsUp.virtualCurrency
          : _league.virtualCurrency,
      "size":
          _asyncHeadsUp != null ? _asyncHeadsUp.maxPlayers : _league.maxPlayers,
    };

    AnalyticsManager().addEvent(
      Event(name: metadata["type"], eventMetadata: metadata["meta"]),
    );
  }

  pause() {
    _controller.pause();
  }

  resume() {
    _controller.resume();
  }

  sendMessageToUnity(Map<String, dynamic> message) {
    return _controller.postMessage(
        'UnityMessageManager', 'onMessage', json.encode(message));
  }

  setAppConfig(BuildContext context) {
    User user = Provider.of(context, listen: false);
    InitData initData = Provider.of<InitData>(context, listen: false);
    AppConfig _config = Provider.of(context, listen: false);

    SolitaireInitData gameInitData = SolitaireInitData(
      avatarUrl: initData.serverData.avatarUrl,
      cookie: HttpManager.cookie,
      csBaseUrl: _config.gameUrl,
      replayServerBaseUrl: _config.replyURL,
      context: "",
      userId: user.id,
      channelId: _config.channelId,
      lobbyBaseUrl: _config.apiUrl,
      gameTableConfig: initData.experiments.gameTableConfig,
    );

    sendMessageToUnity({
      "type": MessageType.Init.getString(),
      "data": json.encode(gameInitData.toJson())
    });
  }

  startInteractiveGame(BuildContext context) async {
    InitData _initData = Provider.of(context, listen: false);

    GameStartData _gameStartData = GameStartData(
      isFTUE: true,
      isReplay: false,
      musicEnabled: _initData.experiments.miscConfig.musicEnabled,
    );

    SharedPrefHelper().saveToSharedPref(SharedPrefKey.interactiveDemo, "false");
    sendMessageToUnity({
      "type": MessageType.GameStart.getString(),
      "data": json.encode(_gameStartData.toJson())
    });
    sendMessageToUnity({
      "type": MessageType.UnityOpen.getString(),
    });
  }

  startReplay(BuildContext context, {int matchId, int gameId}) async {
    ReplayGame _replayData = ReplayGame(
      isReplay: true,
      gameId: gameId,
      matchId: matchId,
    );

    SharedPrefHelper().saveToSharedPref(SharedPrefKey.interactiveDemo, "false");
    sendMessageToUnity({
      "type": MessageType.GameStart.getString(),
      "data": json.encode(_replayData.toJson())
    });
    sendMessageToUnity({
      "type": MessageType.UnityOpen.getString(),
    });
  }

  startNewGame(
    BuildContext context, {
    AsyncHeadsUp asyncHeadsUp,
    SmallLeague league,
    int selectedTabIndex,
    int tieMatchId,
    int formatType,
  }) async {
    InitData _initData = Provider.of(context, listen: false);
    _selectedTabIndex = selectedTabIndex;
    _league = league;
    _asyncHeadsUp = asyncHeadsUp;
    bool loadInteractive = (await SharedPrefHelper()
            .getFromSharedPref(SharedPrefKey.interactiveDemo)) ==
        "true";

    GameStartData _gameStartData = GameStartData(
      isFTUE: loadInteractive,
      isReplay: false,
      tournamentId: _league != null
          ? _league.tournamentId
          : _asyncHeadsUp == null
              ? 0
              : _asyncHeadsUp.h2hTemplateId,
      tieMatchId: tieMatchId,
      gameType: _league != null
          ? _league.formatType
          : _asyncHeadsUp == null
              ? formatType
              : _asyncHeadsUp.formatType,
      musicEnabled: _initData.experiments.miscConfig.musicEnabled,
    );

    sendMessageToUnity({
      "type": MessageType.GameStart.getString(),
      "data": json.encode(_gameStartData.toJson())
    });
    sendMessageToUnity({
      "type": MessageType.UnityOpen.getString(),
    });
  }
}
