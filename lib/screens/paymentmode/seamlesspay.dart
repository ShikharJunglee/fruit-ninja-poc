import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/screens/paymentmode/deposit_tile_checkbox.dart';

class SeamLessPay extends StatelessWidget {
  final int amount;
  final String logo;
  final String label;
  final Function() onPay;
  final Map<String, dynamic> paymentTypeDetail;

  SeamLessPay({
    @required this.amount,
    this.paymentTypeDetail,
    @required this.logo,
    @required this.label,
    @required this.onPay,
  });

  getPayNowButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Button(
              onPressed: () {
                onPay();
              },
              isCurrencyText: true,
              showCurrencyDelimiters: false,
              amount: amount.toDouble(),
              prefix: "Pay Now ".toUpperCase(),
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w800,
                fontSize: 28.0,
                fontFamily: 'roboto',
                shadows: [
                  Shadow(
                    offset: Offset(-1.5, -1),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(1.5, -1),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(1.5, 2.5),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(-1.5, 2.5),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Card(
                elevation: 2.0,
                clipBehavior: Clip.antiAlias,
                child: DepositTileCheckBox(
                  isChecked: true,
                  label: Row(
                    children: <Widget>[
                      logo.endsWith("svg")
                          ? SvgPicture.network(logo, width: 24.0)
                          : Image.network(
                              logo.trim(),
                              height: 24.0,
                            ),
                      Padding(
                        padding: EdgeInsets.only(left: 16.0),
                        child: Text(
                          label,
                          style: Theme.of(context)
                              .primaryTextTheme
                              .headline6
                              .copyWith(
                                color: Colors.grey.shade700,
                              ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: getPayNowButton(context),
        ),
      ],
    );
  }
}
