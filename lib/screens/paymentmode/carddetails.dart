class CardDetails {
  List<String> discoverCards = [
    "65",
    "644",
    "645",
    "648",
    "649",
    "6011",
    "622126",
    "622925",
  ];
  List<String> masterCards = [
    "23",
    "26",
    "51",
    "52",
    "54",
    "55",
    "223",
    "229",
    "270",
    "271",
    "2221",
    "2229",
    "2720",
  ];
  List<String> visaCards = [
    "4",
  ];
  List<String> amexCard = [
    "34",
    "37",
  ];

  final String visaCardIcon =
      "https://d2cbroser6kssl.cloudfront.net/images/howzat/imgs/visa.png";
  final String amexCardIcon =
      "https://d2cbroser6kssl.cloudfront.net/images/howzat/imgs/amex.png";
  final String masterCardIcon =
      "https://d2cbroser6kssl.cloudfront.net/images/howzat/imgs/mastercad.png";
  final String discoverCardIcon =
      "https://d2cbroser6kssl.cloudfront.net/images/howzat/imgs/discovery.png";
  final String otherCardIcon =
      "https://d2cbroser6kssl.cloudfront.net/images/howzat/imgs/othercards.png";

  String getCardIcon(String cardNumber) {
    String cardIcon;
    discoverCards.forEach((number) {
      if (cardNumber.startsWith(number)) {
        cardIcon = discoverCardIcon;
      }
    });

    masterCards.forEach((number) {
      if (cardNumber.startsWith(number)) {
        cardIcon = masterCardIcon;
      }
    });

    visaCards.forEach((number) {
      if (cardNumber.startsWith(number)) {
        cardIcon = visaCardIcon;
      }
    });

    amexCard.forEach((number) {
      if (cardNumber.startsWith(number)) {
        cardIcon = amexCardIcon;
      }
    });

    if (cardIcon == null) {
      cardIcon = otherCardIcon;
    }

    return cardIcon;
  }
}
