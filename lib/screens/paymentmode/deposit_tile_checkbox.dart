import 'package:flutter/material.dart';

class DepositTileCheckBox extends StatelessWidget {
  final Widget label;
  final Function onChecked;
  final bool isChecked;

  DepositTileCheckBox({this.label, this.onChecked, this.isChecked});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey.shade100,
          width: 1.0,
        ),
      ),
      child: InkWell(
        onTap: () {
          onChecked();
        },
        child: Card(
          elevation: 0.0,
          margin: EdgeInsets.all(0.0),
          child: Container(
            height: 40,
            padding:
                const EdgeInsets.symmetric(horizontal: 24.0, vertical: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                label,
                Container(
                  width: 20.0,
                  height: 20.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      width: 1.0,
                      color: Colors.grey.shade400,
                    ),
                  ),
                  alignment: Alignment.center,
                  child: Container(
                    height: 16.0,
                    width: 16.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: isChecked ? Colors.green : Colors.transparent,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
