import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class InitPay extends StatefulWidget {
  final String url;
  final Function onTransactionComplete;

  InitPay({this.url, this.onTransactionComplete});

  @override
  InitPayState createState() => InitPayState();
}

class InitPayState extends State<InitPay> {
  Map<String, String> depositResponse;
  FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();
  StreamSubscription stateChangeListener;

  @override
  void initState() {
    setWebview();
    super.initState();
  }

  setWebview() async {
    stateChangeListener = flutterWebviewPlugin.onStateChanged.listen(
      (WebViewStateChanged state) {
        Uri uri = Uri.dataFromString(state.url);
        if (uri.path.indexOf(AppUrl.PAYMENT_SUCCESS.toString()) != -1 &&
            uri.hasQuery) {
          if (depositResponse == null) {
            depositResponse = uri.queryParameters;
            if (stateChangeListener != null) {
              stateChangeListener.cancel();
            }
            flutterWebviewPlugin.close();
            Navigator.of(context).pop(json.encode(depositResponse));
          }
        } else if (uri.path.indexOf(AppUrl.TSEVO_PAYMENT_SUCCESS.toString()) !=
            -1) {
          depositResponse = uri.queryParameters;
          if (stateChangeListener != null) {
            stateChangeListener.cancel();
          }

          flutterWebviewPlugin.close();
          Navigator.of(context).pop(json.encode(depositResponse));
        }
      },
    );

    flutterWebviewPlugin
        .evalJavascript("document.cookie='" + HttpManager.cookie + "'");
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0),
        child: Container(),
      ),
      url: Uri.encodeFull(widget.url),
      withJavascript: true,
      enableAppScheme: true,
      withLocalStorage: true,
      debuggingEnabled: true,
    );
  }
}
