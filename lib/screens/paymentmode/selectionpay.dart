import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/dropdown.dart';
import 'package:solitaire_gold/screens/paymentmode/deposit_tile_checkbox.dart';

class SelectionPay extends StatefulWidget {
  final int amount;
  final String logo;
  final String label;
  final String type;
  final bool expanded;
  final List<dynamic> bankNames;
  final Function(bool) onExpanded;
  final Function(Map<String, dynamic>) onPay;
  final Function(Map<String, dynamic>) onOptionSelect;

  SelectionPay({
    @required this.amount,
    this.bankNames,
    @required this.logo,
    @required this.label,
    @required this.type,
    @required this.onPay,
    @required this.onExpanded,
    @required this.onOptionSelect,
    this.expanded = false,
  });

  @override
  _SelectionPayState createState() => _SelectionPayState();
}

class _SelectionPayState extends State<SelectionPay> {
  Map<String, dynamic> selectedOtherBank;
  Map<String, dynamic> selectedPreferredBank;
  List<Map<String, dynamic>> otherBanks = List();
  List<Map<String, dynamic>> preferredBanks = List();

  @override
  void initState() {
    widget.bankNames.forEach((bank) {
      bank["label"] = bank["info"]["label"];
      if (bank["info"]["isPreminum"] == true) {
        preferredBanks.add(bank);
      } else {
        otherBanks.add(bank);
      }
    });
    if (preferredBanks.length == 0) {
      preferredBanks = otherBanks;
      otherBanks = [];
    }
    selectedPreferredBank = preferredBanks[0];
    super.initState();
  }

  getPayNowButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Button(
              onPressed: () {
                widget.onPay(selectedOtherBank == null
                    ? selectedPreferredBank
                    : selectedOtherBank);
              },
              isCurrencyText: true,
              showCurrencyDelimiters: false,
              amount: widget.amount.toDouble(),
              prefix: "Pay Now ".toUpperCase(),
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w800,
                fontSize: 28.0,
                fontFamily: 'roboto',
                shadows: [
                  Shadow(
                    offset: Offset(-1.5, -1),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(1.5, -1),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(1.5, 2.5),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(-1.5, 2.5),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 0.0, bottom: 16.0, right: 0.0),
      child: Column(
        children: <Widget>[
          Card(
            elevation: 2.0,
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: <Widget>[
                for (var bank in preferredBanks)
                  DepositTileCheckBox(
                    label: bank["info"]["logoUrl"] != null
                        ? (bank["info"]["logoUrl"] as String).endsWith(".svg")
                            ? SvgPicture.network(
                                bank["info"]["logoUrl"],
                                height: 24.0,
                              )
                            : Image.network(
                                (bank["info"]["logoUrl"] as String).trim(),
                                height: 24.0,
                              )
                        : Text(
                            bank["info"]["label"],
                            style: Theme.of(context)
                                .primaryTextTheme
                                .headline6
                                .copyWith(
                                  color: Colors.grey.shade700,
                                ),
                          ),
                    onChecked: () {
                      setState(() {
                        selectedOtherBank = null;
                        selectedPreferredBank = bank;
                      });
                    },
                    isChecked: selectedPreferredBank != null &&
                        selectedPreferredBank["name"] == bank["name"],
                  ),
              ],
            ),
          ),
          if (otherBanks.length > 0)
            Padding(
              padding: const EdgeInsets.only(
                  top: 10.0, bottom: 0.0, left: 4.0, right: 4.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Dropdown(
                      label: widget.type == "WALLET"
                          ? "Other Wallets"
                          : "Other Banks",
                      dropDownItems: otherBanks,
                      valueKey: "label",
                      onSelectCallback: (Map<String, dynamic> selectedBank) {
                        widget.onOptionSelect(selectedBank);
                        setState(() {
                          selectedOtherBank = selectedBank;
                          selectedPreferredBank = null;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: getPayNowButton(),
          )
        ],
      ),
    );
  }
}
