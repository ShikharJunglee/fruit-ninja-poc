import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/commonwidgets/textbox/MaskedTextInputFormatter.dart';
import 'package:solitaire_gold/screens/paymentmode/carddetails.dart';

class SeamLessWithDetailsPay extends StatefulWidget {
  final int amount;
  final String logo;
  final String label;
  final bool expanded;
  final Function(bool) onExpanded;
  final Map<String, dynamic> payModeDetails;
  final Function(Map<String, dynamic>) onPay;

  SeamLessWithDetailsPay({
    @required this.amount,
    @required this.logo,
    @required this.label,
    @required this.onPay,
    this.expanded = false,
    this.onExpanded,
    @required this.payModeDetails,
  });

  @override
  _SeamLessWithDetailsPayState createState() => _SeamLessWithDetailsPayState();
}

class _SeamLessWithDetailsPayState extends State<SeamLessWithDetailsPay> {
  final CardDetails _cardDetails = CardDetails();
  final GlobalKey<FormState> _formState = GlobalKey<FormState>();
  final TextEditingController _cardNumberController = TextEditingController();

  String cvv;
  String expiry;
  String cardNumber;
  String cardHolderName;

  String cardIcon;

  @override
  void initState() {
    addCardNumberListener();
    super.initState();
  }

  addCardNumberListener() {
    cardIcon = _cardDetails.otherCardIcon;
    _cardNumberController.addListener(() {
      setState(() {
        cardIcon = _cardDetails.getCardIcon(_cardNumberController.text);
      });
    });
  }

  getPayNowButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Button(
              isCurrencyText: true,
              showCurrencyDelimiters: false,
              amount: widget.amount.toDouble(),
              prefix: "Pay Now ".toUpperCase(),
              onPressed: () {
                if (_formState.currentState.validate()) {
                  _formState.currentState.save();
                  widget.onPay({
                    "cvv": cvv,
                    "expiry": expiry,
                    "cardHolderName":
                        cardHolderName == null ? "" : cardHolderName,
                    "cardNumber":
                        cardNumber.replaceAll(RegExp(r"\s\b|\b\s"), ""),
                  });
                }
              },
              style: Theme.of(context).primaryTextTheme.headline4.copyWith(
                color: Colors.white,
                fontWeight: FontWeight.w800,
                fontSize: 28.0,
                fontFamily: 'roboto',
                shadows: [
                  Shadow(
                    offset: Offset(-1.5, -1),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(1.5, -1),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(1.5, 2.5),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                  Shadow(
                    offset: Offset(-1.5, 2.5),
                    blurRadius: 4.0,
                    color: Color.fromRGBO(70, 135, 12, 1),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Form(
        key: _formState,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 16.0, top: 4.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: SimpleTextBox(
                      labelText: "Card number",
                      isDense: true,
                      controller: _cardNumberController,
                      style:
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                color: Colors.grey.shade700,
                              ),
                      labelStyle:
                          Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                                color: Colors.grey.shade600,
                              ),
                      suffixIcon: Padding(
                        padding: EdgeInsets.only(right: 4.0),
                        child: Image.network(
                          cardIcon,
                          width: 8.0,
                        ),
                      ),
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.deny(
                          RegExp("[-,./#\$]"),
                        ),
                        MaskedTextInputFormatter(
                          mask: 'xxxx xxxx xxxx xxxx',
                          separator: ' ',
                        ),
                      ],
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Card number is required.";
                        }
                        return null;
                      },
                      onSaved: (value) {
                        cardNumber = value;
                      },
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 16.0, top: 4.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(right: 8.0),
                      child: SimpleTextBox(
                        labelText: "Expiry Date",
                        hintText: "MM/YY",
                        isDense: true,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                        labelStyle: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade600,
                            ),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.deny(
                            RegExp("[-,.#\$]"),
                          ),
                          MaskedTextInputFormatter(
                            mask: 'xx/xx',
                            separator: '/',
                          ),
                        ],
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Expiry date required.";
                          } else {
                            List<String> expiryDate = value.split("/");
                            int month = int.tryParse(expiryDate[0]);
                            int year = int.tryParse(expiryDate.length == 1
                                ? ""
                                : "20" + expiryDate[1]);
                            if (month == null || !(month > 0 && month <= 12)) {
                              return "Invalid month";
                            }
                            if (year == null || year == 0) {
                              return "Invalid year";
                            } else {
                              int currentYear = DateTime.now().year;
                              int currentMonth = DateTime.now().month;
                              if (year < currentYear - 1 ||
                                  (year == currentYear &&
                                      currentMonth < month)) {
                                return "Invalid expiry date";
                              }
                            }
                          }
                          return null;
                        },
                        onSaved: (value) {
                          expiry = value;
                        },
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: SimpleTextBox(
                        isDense: true,
                        labelText: "CVV",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Colors.grey.shade700,
                            ),
                        labelStyle: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade600,
                            ),
                        obscureText: true,
                        maxLines: 1,
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          WhitelistingTextInputFormatter.digitsOnly,
                        ],
                        validator: (value) {
                          if (value.isEmpty) {
                            return "CVV number required.";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          cvv = value;
                        },
                        maxLength: 4,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.only(bottom: 16.0, top: 4.0),
            //   child: Row(
            //     children: <Widget>[
            //       Expanded(
            //         child: SimpleTextBox(
            //           labelText: "Card Holder's Name",
            //           isDense: true,
            //           style:
            //               Theme.of(context).primaryTextTheme.headline6.copyWith(
            //                     color: Colors.grey.shade700,
            //                   ),
            //           labelStyle:
            //               Theme.of(context).primaryTextTheme.subtitle1.copyWith(
            //                     color: Colors.grey.shade600,
            //                   ),
            //           keyboardType: TextInputType.text,
            //           validator: (value) {
            //             if (value.isEmpty) {
            //               return "Cardholder's name is required.";
            //             }
            //             return null;
            //           },
            //           onSaved: (value) {
            //             cardHolderName = value;
            //           },
            //         ),
            //       ),
            //     ],
            //   ),
            // ),
            Container(
              padding: EdgeInsets.only(bottom: 12.0, top: 0.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: getPayNowButton(),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
