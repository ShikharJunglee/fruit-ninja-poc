import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/models/initdata.dart';

class DepositDetails extends StatelessWidget {
  final int amount;
  final int bonusAmount;
  final int goldCount;
  final Map<String, dynamic> promo;
  DepositDetails({this.amount, this.bonusAmount, this.goldCount, this.promo});

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);
    return Card(
      elevation: 5.0,
      margin: EdgeInsets.symmetric(horizontal: 2.0, vertical: 0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: (promo == null)
              ? MainAxisAlignment.center
              : MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "Deposit",
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 4.0),
                  child: CurrencyText(
                    showCurrencyDelimiters: false,
                    amount: amount.toDouble(),
                    decimalDigits:
                        initData.experiments.miscConfig.amountPrecision,
                    style:
                        Theme.of(context).primaryTextTheme.headline6.copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                            ),
                  ),
                ),
              ],
            ),
            if (promo != null)
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        "Bonus Code",
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 4.0),
                    child: DottedBorder(
                      strokeWidth: 1,
                      color: Theme.of(context).buttonColor,
                      padding:
                          EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
                      child: Text(
                        promo["promoCode"],
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline6
                            .copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 16.0,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
            if (promo != null)
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "Bonus",
                                style: TextStyle(fontSize: 16.0),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 4.0),
                            child: Row(
                              children: <Widget>[
                                CurrencyText(
                                  showCurrencyDelimiters: false,
                                  amount: bonusAmount.toDouble(),
                                  decimalDigits: initData
                                      .experiments.miscConfig.amountPrecision,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .headline6
                                      .copyWith(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                      ),
                                ),
                                // if (goldCount > 0)
                                //   Row(
                                //     children: <Widget>[
                                //       Text(
                                //         " + ",
                                //         style: Theme.of(context)
                                //             .primaryTextTheme
                                //             .headline6
                                //             .copyWith(
                                //               color: Colors.black,
                                //               fontWeight: FontWeight.w700,
                                //             ),
                                //       ),
                                //       Image.asset(
                                //         "images/misc/goldbar-icon.png",
                                //         package: "common_utils",
                                //         height: 14.0,
                                //       ),
                                //       Text(
                                //         "$goldCount",
                                //         style: Theme.of(context)
                                //             .primaryTextTheme
                                //             .headline6
                                //             .copyWith(
                                //               color: Colors.black,
                                //               fontWeight: FontWeight.w700,
                                //             ),
                                //       ),
                                //     ],
                                //   ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
          ],
        ),
      ),
    );
  }
}
