import 'package:flutter_svg/svg.dart';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/screens/paymentmode/seamlesspay.dart';
import 'package:solitaire_gold/screens/paymentmode/seamlesswithdetailspay.dart';
import 'package:solitaire_gold/screens/paymentmode/selectionpay.dart';

class OtherPayments extends StatefulWidget {
  final int amount;
  final Function(Map<String, dynamic>) onPay;
  final Map<String, dynamic> paymentModeDetails;
  final Function(bool, Map<String, dynamic>) payModeSelect;
  final Function(Map<String, dynamic>) onOptionSelect;

  OtherPayments({
    @required this.amount,
    @required this.paymentModeDetails,
    @required this.onPay,
    @required this.payModeSelect,
    @required this.onOptionSelect,
  });

  @override
  _OtherPaymentsState createState() => _OtherPaymentsState();
}

class _OtherPaymentsState extends State<OtherPayments>
    with SingleTickerProviderStateMixin {
  int selectedIndex = 0;
  TabController _tabController;
  Map<String, dynamic> lastPaymentInfo;

  @override
  void initState() {
    List<dynamic> lastPaymentArray = getLastPaymentInfo();
    lastPaymentInfo = lastPaymentArray.length > 0 ? lastPaymentArray[0] : {};

    _tabController = TabController(
        vsync: this,
        length:
            widget.paymentModeDetails["paymentInfo"]["paymentTypes"].length);
    _tabController.addListener(() {
      if (!_tabController.indexIsChanging) {
        setState(() {
          selectedIndex = _tabController.index;
        });
      }
    });
    super.initState();
  }

  getLastPaymentInfo() {
    return widget.paymentModeDetails["userInfo"] == null ||
            widget.paymentModeDetails["userInfo"]["lastPaymentArray"] == null
        ? []
        : widget.paymentModeDetails["userInfo"]["lastPaymentArray"];
  }

  List<Widget> getPaymentList() {
    int i = 0;
    List<Widget> items = [];

    for (var paymentType in widget.paymentModeDetails["paymentInfo"]
        ["paymentTypes"]) {
      int curIndex = i;
      items.add(
        Expanded(
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(bottom: 16.0),
                child: InkWell(
                  radius: 6.0,
                  onTap: () {
                    setState(() {
                      _tabController.index = curIndex;
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    margin: EdgeInsets.all(2.0),
                    decoration: BoxDecoration(
                      color: _tabController.index == curIndex
                          ? Colors.white
                          : Colors.grey.shade100,
                      borderRadius: BorderRadius.circular(6.0),
                      boxShadow: _tabController.index == curIndex
                          ? [
                              BoxShadow(
                                color: Colors.grey.shade400,
                                blurRadius: 3.0,
                                spreadRadius: 0.0,
                                offset: Offset(0, 2),
                              ),
                            ]
                          : null,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: (paymentType["logo"] as String)
                                        .endsWith(".svg")
                                    ? SvgPicture.network(
                                        paymentType["logo"],
                                        height: 24.0,
                                      )
                                    : Image.network(
                                        paymentType["logo"],
                                        height: 24.0,
                                      ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 3.0, right: 3.0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        paymentType["label"],
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .subtitle2
                                            .copyWith(
                                              color: Colors.grey.shade600,
                                            ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              if (selectedIndex == i)
                Positioned(
                  bottom: 2.2,
                  child: Container(
                    child: Image.asset(
                      "images/misc/white_arrow.png",
                      height: 16.0,
                    ),
                  ),
                ),
            ],
          ),
        ),
      );
      i++;
    }

    return items;
  }

  getTabView(int curIndex) {
    final paymentType =
        widget.paymentModeDetails["paymentInfo"]["paymentTypes"][curIndex];
    List<dynamic> bankNames =
        widget.paymentModeDetails["paymentInfo"][paymentType["type"]];
    Map<String, dynamic> payModeDetails =
        bankNames.length > 0 ? bankNames[0]["info"] : null;

    bool isSeamless = payModeDetails["isSeamless"] || bankNames.length == 1;
    bool cardDetailsRequired = payModeDetails["detailRequired"];

    return isSeamless
        ? cardDetailsRequired
            ? SeamLessWithDetailsPay(
                amount: widget.amount,
                payModeDetails: payModeDetails,
                logo: paymentType["logo"],
                label: paymentType["label"],
                onExpanded: (expanded) {
                  setState(() {
                    if (selectedIndex != curIndex) {
                      selectedIndex = curIndex;
                      widget.payModeSelect(true, payModeDetails);
                    } else {
                      selectedIndex = null;
                    }
                  });
                },
                expanded: selectedIndex == curIndex,
                onPay: (userDetails) {
                  payModeDetails["cvv"] = userDetails["cvv"];
                  payModeDetails["expiry"] = userDetails["expiry"];
                  payModeDetails["cardNumber"] = userDetails["cardNumber"];
                  payModeDetails["cardHolderName"] =
                      userDetails["cardHolderName"];
                  payModeDetails["paymentOption"] = bankNames[0]["name"];
                  payModeDetails["paymentType"] = paymentType["type"];
                  widget.onPay(payModeDetails);
                },
              )
            : SeamLessPay(
                amount: widget.amount,
                logo: paymentType["logo"],
                label: paymentType["label"],
                paymentTypeDetail: payModeDetails,
                onPay: () {
                  payModeDetails["paymentOption"] = bankNames[0]["name"];
                  payModeDetails["paymentType"] = paymentType["type"];
                  widget.payModeSelect(true, payModeDetails);
                  widget.onPay(payModeDetails);
                },
              )
        : SelectionPay(
            amount: widget.amount,
            bankNames: bankNames,
            logo: paymentType["logo"],
            label: paymentType["label"],
            type: paymentType["type"],
            expanded: selectedIndex == curIndex,
            onExpanded: (expanded) {
              setState(() {
                if (selectedIndex != curIndex) {
                  selectedIndex = curIndex;
                } else {
                  selectedIndex = null;
                }
              });
              widget.payModeSelect(expanded, paymentType);
            },
            onOptionSelect: (optionDetails) {
              widget.onOptionSelect(optionDetails);
            },
            onPay: (selectedBank) {
              payModeDetails = selectedBank["info"];
              payModeDetails["paymentType"] = paymentType["type"];
              payModeDetails["paymentOption"] = selectedBank["name"];
              widget.payModeSelect(true, payModeDetails);
              widget.onPay(payModeDetails);
            },
          );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              IntrinsicHeight(
                child: Row(
                  children: getPaymentList(),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TabBarView(
                    controller: _tabController,
                    children: List.generate(
                      _tabController.length,
                      (index) {
                        return SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              getTabView(index),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
