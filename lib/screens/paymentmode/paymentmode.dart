import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/analyticsevents.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/screens/lobby/addcash/paymentpartners.dart';
import 'package:solitaire_gold/screens/lobby/addcash/transactionfailed.dart';
import 'package:solitaire_gold/screens/paymentmode/depositdetail.dart';
import 'package:solitaire_gold/screens/paymentmode/otherpayments.dart';
import 'package:solitaire_gold/services/mybranchioservice.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/gateways/cashfreepayment.dart';
import 'package:solitaire_gold/utils/gateways/paytmnative.dart';
import 'package:solitaire_gold/utils/gateways/razorpay.dart';
import 'package:solitaire_gold/utils/gateways/stripe.dart';
import 'package:solitaire_gold/utils/gateways/tsevo.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';

class PaymentMode extends StatefulWidget {
  final String source;
  final int amount;
  final int bonusAmount;
  final int goldCount;
  final Map<String, dynamic> promo;
  final Map<String, dynamic> paymentModeDetails;
  final Map<String, dynamic> stripe;
  final String disclaimerText;

  PaymentMode({
    @required this.source,
    this.amount,
    this.bonusAmount,
    this.goldCount,
    this.promo,
    this.paymentModeDetails,
    @required this.disclaimerText,
    this.stripe,
  });

  @override
  _PaymentModeState createState() => _PaymentModeState();
}

class _PaymentModeState extends State<PaymentMode> with BasePage {
  final Razorpay _razorpay = Razorpay();
  final StripePay _stripePayment = StripePay();
  final CashFreePayment _cashFreePayment = CashFreePayment();
  final Tsevo _tsevoPayment = Tsevo();
  final PaytmNativePayment _paytmNativePayment = PaytmNativePayment();

  final AnalyticsEvents _analyticsEvents = AnalyticsEvents();

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      User user = Provider.of(context, listen: false);
      _analyticsEvents.paymentModeLaunch(
        source: widget.source,
        amount: widget.amount,
        promoCode: widget.promo != null ? widget.promo["promoCode"] : "",
        firstDeposit: user.isFirstDeposit,
      );
    });
  }

  Map<String, dynamic> getUserDetails() {
    User user = Provider.of(context, listen: false);

    Map<String, dynamic> userResponse = {
      if (user.email != null) "email": user.email,
      if (user.mobile != null) "phone": user.mobile,
      if (user.address.lastName != null) "lastName": user.address.lastName,
      if (user.address.firstName != null) "firstName": user.address.firstName,
    };

    return userResponse
      ..addAll({
        "updateName": false,
        "updateEmail": false,
        "updateMobile": false,
      });
  }

  addWebEngagePaymnetInitEvent(Map<String, dynamic> payModeDetails) {
    User user = Provider.of(context, listen: false);
    Map<String, dynamic> data = payModeDetails;
    data["channelId"] = HttpManager.channelId;
    data["pageSource"] = "paymentmode_page";
    data["isFirstDeposit"] = user.isFirstDeposit;
    MyWebEngageService().webengageAddDepositIntiatedEvent(data);
  }

  Future<bool> isPaytmAppInstalled(BuildContext context) async {
    bool isPaytmAppInstalled = false;
    try {
      String result =
          await ChannelManager.PAYTMNATIVE.invokeMethod('isPayTmAppInstalled');
      if (result == "true") {
        isPaytmAppInstalled = true;
      }
      return isPaytmAppInstalled;
    } catch (e) {
      return isPaytmAppInstalled;
    }
  }

  onPaySecurely(Map<String, dynamic> payModeDetails) async {
    User user = Provider.of(context, listen: false);
    try {
      addWebEngagePaymnetInitEvent(payModeDetails);
    } catch (e) {}

    _analyticsEvents.validatePaySecurely(
      source: widget.source,
      amount: widget.amount,
      gatewayId: int.tryParse(payModeDetails["gatewayId"].toString()),
      modeOptionId: payModeDetails["modeOptionId"],
      firstDeposit: user.isFirstDeposit,
      promoCode: widget.promo == null ? null : widget.promo["promoCode"],
      message: "Success",
    );
    bool bisPaytmAppInstalled = false;
    String clientPaymentType = "";
    if (payModeDetails["gateway"] == "PAYTMWALLET") {
      bisPaytmAppInstalled = await isPaytmAppInstalled(context);
    } else {
      clientPaymentType = payModeDetails["paymentOption"];
    }

    _analyticsEvents.paySecurely(
        source: widget.source,
        amount: widget.amount,
        gatewayId: int.tryParse(payModeDetails["gatewayId"].toString()),
        modeOptionId: payModeDetails["modeOptionId"],
        firstDeposit: user.isFirstDeposit,
        promoCode: widget.promo == null ? null : widget.promo["promoCode"],
        clientPaymentType: clientPaymentType != ""
            ? clientPaymentType
            : (bisPaytmAppInstalled ? "paytm_native" : "paytm_web"));

    if (payModeDetails["gateway"] == "PAYTMWALLET" && bisPaytmAppInstalled) {
      _paytmNativePayment.openPaytmNativePayment(context,
          amount: widget.amount,
          paymentModeDetails: payModeDetails,
          promoCode: widget.promo == null ? null : widget.promo["promoCode"],
          userDetails: getUserDetails(),
          isFirstDeposit: user.isFirstDeposit, onComplete: (payload, response) {
        onPaymentResponse(
            {}..addAll(payload)..addAll(payModeDetails), response);
      });
    } else if (payModeDetails["gateway"] == "STRIPE") {
      _stripePayment.openStripe(
        context,
        amount: widget.amount,
        paymentModeDetails: payModeDetails,
        promoCode: widget.promo == null ? null : widget.promo["promoCode"],
        userDetails: getUserDetails(),
        isFirstDeposit: user.isFirstDeposit,
        onComplete: (response) {
          onPaymentResponse(payModeDetails, response);
        },
        stripeConfig: widget.stripe,
      );
    } else if (payModeDetails["gateway"] == "TSEVO") {
      Map<String, dynamic> position = await AddCashAPI().getLatLong(context);
      Map<String, dynamic> result = await _tsevoPayment.openTsevoPayment(
        context,
        amount: widget.amount,
        paymentModeDetails: payModeDetails,
        promoCode: widget.promo == null ? null : widget.promo["promoCode"],
        userDetails: getUserDetails(),
        isFirstDeposit: user.isFirstDeposit,
        latitude: position["location"]["lat"],
        longitude: position["location"]["lng"],
        radius: position["accuracy"],
        altitude: 0,
      );
      onPaymentResponse({}..addAll(payModeDetails), result);
    } else if (payModeDetails["isSeamless"]) {
      _razorpay.openRazorpay(
        context,
        amount: widget.amount,
        paymentModeDetails: payModeDetails,
        promoCode: widget.promo == null ? null : widget.promo["promoCode"],
        userDetails: getUserDetails(),
        isFirstDeposit: user.isFirstDeposit,
        onComplete: (payload, response) {
          onPaymentResponse(
              {}..addAll(payload)..addAll(payModeDetails), response);
        },
      );
    } else {
      Map<String, dynamic> result = await _cashFreePayment.openCashFreePayment(
        context,
        amount: widget.amount,
        paymentModeDetails: payModeDetails,
        promoCode: widget.promo == null ? null : widget.promo["promoCode"],
        userDetails: getUserDetails(),
        isFirstDeposit: user.isFirstDeposit,
      );
      onPaymentResponse({}..addAll(payModeDetails), result);
    }
  }

  addWebEngagePaymentFailedEvent(Map<String, dynamic> response) {
    User user = Provider.of(context, listen: false);
    Map<String, dynamic> data = response;
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = user.isFirstDeposit;
    data["pageSource"] = "paymentmode_page";
    MyWebEngageService().webengageAddTransactionFailedEvent(data);
  }

  addWebEngagePaymentSuccessEvent(Map<String, dynamic> response) {
    Map<String, dynamic> data = response;
    User user = Provider.of(context, listen: false);
    data["channelId"] = HttpManager.channelId;
    data["isFirstDeposit"] = user.isFirstDeposit;
    data["pageSource"] = "paymentmode_page";
    MyWebEngageService().webengageAddTransactionSuccessEvent(data);
  }

  onPaymentResponse(
      Map<String, dynamic> payload, Map<String, dynamic> response) async {
    User user = Provider.of(context, listen: false);
    if (response != null && response["error"]) {
      try {
        addWebEngagePaymentFailedEvent(response["data"]);
        MyBranchIoService().branchEventTransactionFailed(response);
      } catch (e) {}

      _analyticsEvents.paymentFailed(
        source: widget.source,
        amount: widget.amount,
        paymentPayload: payload,
        paymentResponse: response["data"],
        promoCode: widget.promo == null ? "" : widget.promo["promoCode"],
        firstDeposit: user.isFirstDeposit,
      );

      if (response["message"] != null) {
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                  child: Text(
                    response["message"],
                    textAlign: TextAlign.center,
                    softWrap: true,
                    style:
                        Theme.of(context).primaryTextTheme.headline5.copyWith(
                              color: Colors.grey.shade700,
                            ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 78.0),
                  child: Button(
                    size: ButtonSize.medium,
                    text: "OK",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          },
        );
      } else {
        Map<String, dynamic> result = await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return TransactionFailed(response["data"]);
          },
        );

        if (result != null && result["cancel"]) {
          _analyticsEvents.paymentFailedCancel(
            source: widget.source,
            amount: widget.amount,
            transactionResult: response["data"],
            promoCode: widget.promo == null ? "" : widget.promo["promoCode"],
          );
          if (result != null && result["cancel"]) {
            Navigator.of(context).pop({
              "cancel": true,
            });
          }
        } else {
          _analyticsEvents.paymentFailedRetry(
            source: widget.source,
            amount: widget.amount,
            transactionResult: response["data"],
            promoCode: widget.promo == null ? "" : widget.promo["promoCode"],
          );
        }
      }
    } else if (response != null) {
      try {
        user.userBalance.setWithdrawable(response["data"]["withdrawable"]);
        user.userBalance.setDepositBucket(response["data"]["depositBucket"]);
      } catch (e) {}

      try {
        addWebEngagePaymentSuccessEvent(response["data"]);
        MyBranchIoService().branchEventTransactionSuccess(response["data"]);
      } catch (e) {}

      try {
        _analyticsEvents.paymentSuccess(
          amount: widget.amount,
          paymentPayload: payload,
          paymentResponse: response["data"],
          promoCode: widget.promo == null ? "" : widget.promo["promoCode"],
          firstDeposit: user.isFirstDeposit,
        );
      } catch (e) {}
      Navigator.of(context).pop(
        response["data"],
      );
    }
  }

  void onPaymentModeSelect(expanded, paymentType) {
    User user = Provider.of(context, listen: false);
    _analyticsEvents.payModeSelect(
      source: widget.source,
      expanded: expanded,
      amount: widget.amount,
      promoCode: widget.promo == null ? "" : widget.promo["promoCode"],
      firstDeposit: user.isFirstDeposit,
      paymentType: paymentType["paymentType"],
    );
  }

  void onPaymentOptionChange(optionDetails) {
    User user = Provider.of(context, listen: false);

    _analyticsEvents.payOptionSelect(
      source: widget.source,
      amount: widget.amount,
      promoCode: widget.promo == null ? "" : widget.promo["promoCode"],
      firstDeposit: user.isFirstDeposit,
      gatewayId: int.tryParse(optionDetails["info"]["gatewayId"].toString()),
      modeOptionId: optionDetails["info"]["modeOptionId"],
      paymentOptionType: optionDetails["name"],
    );
  }

  bool showPreferredPaymentMode() {
    return !false &&
        widget.paymentModeDetails != null &&
        widget.paymentModeDetails["userInfo"] != null &&
        widget.paymentModeDetails["userInfo"]["lastPaymentArray"] != null;
  }

  @override
  Widget build(BuildContext context) {
    return LoaderContainer(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: CommonAppBar(
          children: <Widget>[
            Text(
              "Payment options".toUpperCase(),
            ),
          ],
        ),
        body: Padding(
          padding: EdgeInsets.only(left: 4.0, right: 4.0, top: 12.0),
          child: Column(
            children: <Widget>[
              DepositDetails(
                promo: widget.promo,
                amount: widget.amount,
                bonusAmount: widget.bonusAmount,
                goldCount: widget.goldCount,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.only(top: 16.0),
                  child: OtherPayments(
                    amount: widget.amount,
                    paymentModeDetails: widget.paymentModeDetails,
                    onOptionSelect: (optionDetails) {
                      onPaymentOptionChange(optionDetails);
                    },
                    onPay: (payModeDetails) {
                      onPaySecurely(payModeDetails);
                    },
                    payModeSelect: onPaymentModeSelect,
                  ),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: PaymentPartners(
          disclaimerText: widget.disclaimerText,
        ),
      ),
    );
  }
}
