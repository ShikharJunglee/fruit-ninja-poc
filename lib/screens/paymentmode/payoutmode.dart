import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/api/addcash/addcashAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/screens/lobby/addcash/transactionfailed.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/gateways/tsevo.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';

class PayoutMode extends StatefulWidget {
  final String source;
  final Map<String, dynamic> withdrawRequest;

  PayoutMode({
    @required this.source,
    this.withdrawRequest,
  });

  @override
  _PayoutModeState createState() => _PayoutModeState();
}

class _PayoutModeState extends State<PayoutMode> with BasePage {
  final Tsevo _tsevoPayment = Tsevo();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  setData(BuildContext context) {
    Loader().showLoader(true, immediate: true);
    Future.delayed(Duration(seconds: 1), () {
      Loader().showLoader(false);
      onPaySecurely();
    });
  }

  Map<String, dynamic> getUserDetails() {
    User user = Provider.of(context, listen: false);

    Map<String, dynamic> userResponse = {
      if (user.email != null) "email": user.email,
      if (user.mobile != null) "phone": user.mobile,
      if (user.address.lastName != null) "lastName": user.address.lastName,
      if (user.address.firstName != null) "firstName": user.address.firstName,
    };

    return userResponse
      ..addAll({
        "updateName": false,
        "updateEmail": false,
        "updateMobile": false,
      });
  }

  addWebEngagePaymnetInitEvent(Map<String, dynamic> payModeDetails) {
    User user = Provider.of(context, listen: false);
    Map<String, dynamic> data = payModeDetails;
    data["channelId"] = HttpManager.channelId;
    data["pageSource"] = "paymentmode_page";
    data["isFirstDeposit"] = user.isFirstDeposit;
    MyWebEngageService().webengageAddDepositIntiatedEvent(data);
  }

  Future<bool> isPaytmAppInstalled(BuildContext context) async {
    bool isPaytmAppInstalled = false;
    try {
      String result =
          await ChannelManager.PAYTMNATIVE.invokeMethod('isPayTmAppInstalled');
      if (result == "true") {
        isPaytmAppInstalled = true;
      }
      return isPaytmAppInstalled;
    } catch (e) {
      return isPaytmAppInstalled;
    }
  }

  onPaySecurely() async {
    Loader().showLoader(false);
    User user = Provider.of(context, listen: false);

    Map<String, dynamic> position = await AddCashAPI().getLatLong(context);
    Map<String, dynamic> result = await _tsevoPayment.openTsevoPayout(
      context,
      withdrawRequest: widget.withdrawRequest,
      userDetails: getUserDetails(),
      isFirstDeposit: user.isFirstDeposit,
      latitude: position["location"]["lat"],
      longitude: position["location"]["lng"],
      radius: position["accuracy"],
      altitude: 0,
    );
    onPaymentResponse(result);
  }

  onPaymentResponse(Map<String, dynamic> response) async {
    if (response != null && response["error"]) {
      if (response["message"] != null) {
        Navigator.of(context).pop({
          "cancel": true,
        });
        showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                  child: Text(
                    response["message"],
                    textAlign: TextAlign.center,
                    softWrap: true,
                    style:
                        Theme.of(context).primaryTextTheme.headline5.copyWith(
                              color: Colors.grey.shade700,
                            ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 78.0),
                  child: Button(
                    size: ButtonSize.medium,
                    text: "OK",
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              ],
            );
          },
        );
      } else {
        Map<String, dynamic> result = await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          builder: (BuildContext context) {
            return TransactionFailed(response["data"]);
          },
        );

        if (result != null && result["cancel"]) {
          if (result != null && result["cancel"]) {
            Navigator.of(context).pop({
              "cancel": true,
            });
          }
        }
      }
    } else if (response != null) {
      Navigator.of(context).pop(
        response["data"],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return LoaderContainer(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.only(left: 4.0, right: 4.0, top: 12.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                children: [],
              )
            ],
          ),
        ),
      ),
    );
  }
}
