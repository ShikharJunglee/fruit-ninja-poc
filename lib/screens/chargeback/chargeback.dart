import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/chargeback/chargebackdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/userbalance.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/chargeback/chargeback_success.dart';
import 'package:solitaire_gold/screens/chargeback/chargebackapi.dart';
import 'package:solitaire_gold/screens/totalbalance.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';

class Chargeback extends StatefulWidget {
  final String source;
  final Map<String, dynamic> data;

  Chargeback({this.data, this.source});

  @override
  ChargebackState createState() => ChargebackState();
}

class ChargebackState extends State<Chargeback>
    with SingleTickerProviderStateMixin, BasePage {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();

  ChargebackData _chargebackData;
  TextEditingController amountController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController routingNumberController = TextEditingController();

  ChargebackAPI _chargeBackAPI = ChargebackAPI();

  @override
  void initState() {
    super.initState();
    _chargebackData = ChargebackData.fromJson(widget.data);
    WidgetsBinding.instance.addPostFrameCallback((_) => setData(context));
  }

  void setData(BuildContext context) async {
    User user = Provider.of<User>(context, listen: false);
    UserBalance userBalance = user.userBalance;
    userBalance.setDepositBucket(userBalance.depositBucket);
    GamePlayAnalytics().onChargeBackLoaded(
      context,
      source: "app_drawer",
      minAmount: _chargebackData.minChargeback,
      maxAmount: _chargebackData.maxChargeback,
      processingFee: _chargebackData.processingFee,
    );
  }

  onChargebackRequest(double amount) async {
    User user = Provider.of(context, listen: false);
    UserBalance userBalance = user.userBalance;
    GamePlayAnalytics().onChargeBackRequestClicked(
      context,
      source: "chrgbck_page",
      minAmount: _chargebackData.minChargeback,
      maxAmount: _chargebackData.maxChargeback,
      processingFee: _chargebackData.processingFee,
      amountEntered: amount,
    );
    if (_chargebackData.processingFee >= amount) {
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Chargeback amount cannot be less than",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.grey.shade700,
                              ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "processing fee.",
                          style: Theme.of(context)
                              .primaryTextTheme
                              .subtitle1
                              .copyWith(
                                color: Colors.grey.shade700,
                              ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
      return;
    }

    Loader().showLoader(true, immediate: true);
    amount = (((amount * 100).toInt()) / 100);
    Map<String, dynamic> response = await _chargeBackAPI.makeChargebackRequest(
      context,
      amount: amount,
    );

    Loader().showLoader(false);

    if (response["error"]) {
      showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(
                    response["errorMessage"],
                    textAlign: TextAlign.center,
                    softWrap: true,
                    style:
                        Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                              color: Colors.grey.shade700,
                            ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 78.0),
                child: Button(
                  size: ButtonSize.medium,
                  text: "OK",
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ],
          );
        },
      );
    } else {
      GamePlayAnalytics().onChargeBackRaised(
        context,
        source: "chrgbck_page",
        minAmount: _chargebackData.minChargeback,
        maxAmount: _chargebackData.maxChargeback,
        processingFee: _chargebackData.processingFee,
        amountEntered: amount,
      );
      amountController.clear();
      FocusScope.of(context).unfocus();
      setState(() {
        userBalance.setDepositBucket(userBalance.depositBucket -
            response["changebackConfig"]["amount"] -
            response["changebackConfig"]["processingFee"]);
      });
      await showDialog(
          barrierColor: Color.fromARGB(175, 0, 0, 0),
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return ChargebackSuccess(
              chargebackResponse: response,
            );
          });
      await launchChargebackHistory(context);
    }
  }

  launchChargebackHistory(BuildContext context) {
    return routeManager.launchChargebackHistory(context,
        source: "chargeback_page");
  }

  Widget getChargebackWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Form(
        key: formKey,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SimpleTextBox(
                maxLength: 10,
                controller: amountController,
                isDense: true,
                hintText:
                    "Enter Amount (Min: \$${_chargebackData.minChargeback} - Max: \$${_chargebackData.maxChargeback})",
                labelText: "Enter Amount",
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp("[0-9,.]")),
                ],
                validator: (value) {
                  User user = Provider.of(context, listen: false);
                  if (value.split(".").length > 2) {
                    return "Please enter valid Amount";
                  }
                  if (double.tryParse(value) == null) {
                    return "*Required";
                  }
                  if (double.parse(
                          user.userBalance.depositBucket.toStringAsFixed(2)) <
                      _chargebackData.minChargeback) {
                    return "You don't have sufficient balance to request refund";
                  }
                  if (((double.parse(value) > _chargebackData.maxChargeback) ||
                      double.parse(value) < _chargebackData.minChargeback)) {
                    if ((double.parse(
                            user.userBalance.depositBucket.toStringAsFixed(2)) <
                        _chargebackData.maxChargeback)) {
                      return "Enter an amount between \$${_chargebackData.minChargeback.toStringAsFixed(2)} and \$${user.userBalance.depositBucket.toStringAsFixed(2)}";
                    } else {
                      return "Enter an amount between \$${_chargebackData.minChargeback.toStringAsFixed(2)} and \$${_chargebackData.maxChargeback.toStringAsFixed(2)}";
                    }
                  }
                  return null;
                },
                onChanged: (String amount) {
                  setState(() {});
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: [
                            Text(
                              "Processing Fee",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .subtitle1
                                  .copyWith(
                                    color: Colors.grey.shade800,
                                    fontSize: 18.0,
                                  ),
                            ),
                            IconButton(
                              icon: Image.asset(
                                "images/icons/info-icon.png",
                                height: 15,
                              ),
                              onPressed: () {
                                showDialog(
                                  barrierColor: Color.fromARGB(175, 0, 0, 0),
                                  context: context,
                                  builder: (context) {
                                    return SimpleDialog(
                                      titlePadding: EdgeInsets.all(0),
                                      contentPadding: EdgeInsets.all(8),
                                      title: Stack(
                                        children: [
                                          Align(
                                            alignment: Alignment(1, -1),
                                            child: IconButton(
                                              icon: Icon(Icons.close),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 20.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text("Processing fee"
                                                    .toUpperCase()),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        Column(
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                  "Deposit Refunds are subjected to a"),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(
                                                  "\$${_chargebackData.processingFee} procesing fee."),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "\$",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      (_chargebackData.processingFee ?? 0).toStringAsFixed(2),
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    flex: 7,
                    child: Text(
                      "Net Refundable Amount",
                      style:
                          Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 18.0,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text(
                      "\$",
                      textAlign: TextAlign.right,
                      style:
                          Theme.of(context).primaryTextTheme.headline5.copyWith(
                                color: Colors.grey.shade800,
                                fontSize: 28.0,
                                fontWeight: FontWeight.bold,
                              ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerRight,
                      child: Text(
                        double.tryParse(amountController.text) != null
                            ? (double.tryParse(amountController.text) -
                                    _chargebackData.processingFee)
                                .toStringAsFixed(2)
                            : "0.00",
                        textAlign: TextAlign.right,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .headline5
                            .copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 28.0,
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Button(
                text: "Request Refund".toUpperCase(),
                onPressed: () {
                  if (formKey.currentState.validate()) {
                    double amount = double.parse(amountController.text);
                    onChargebackRequest(
                      amount,
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getChargebackBalanceView() {
    User user = Provider.of(context, listen: false);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Row(
                    children: [
                      Text(
                        "Deposit Balance",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 18.0,
                            ),
                      ),
                      IconButton(
                          icon: Image.asset(
                            "images/icons/info-icon.png",
                            height: 15,
                          ),
                          onPressed: () {
                            GamePlayAnalytics().onTotalBalanceInfoClicked(
                                context,
                                source: "chrgbck_page");
                            showDialog(
                              barrierColor: Color.fromARGB(175, 0, 0, 0),
                              context: context,
                              builder: (context) {
                                return TotalBalance();
                              },
                            );
                          }),
                    ],
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    "\$",
                    textAlign: TextAlign.right,
                    style:
                        Theme.of(context).primaryTextTheme.headline5.copyWith(
                              color: Colors.grey.shade800,
                              fontSize: 28.0,
                              fontWeight: FontWeight.bold,
                            ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Consumer<User>(
                    builder: (context, user, child) {
                      return ChangeNotifierProvider.value(
                        value: user.userBalance,
                        child: Consumer<UserBalance>(
                          builder: (context, userBalance, child) {
                            return FittedBox(
                              fit: BoxFit.scaleDown,
                              alignment: Alignment.centerRight,
                              child: Text(
                                userBalance.depositBucket.toStringAsFixed(2),
                                textAlign: TextAlign.right,
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline5
                                    .copyWith(
                                      color: Colors.grey.shade800,
                                      fontSize: 28.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                              ),
                            );
                          },
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        setState(() {});
        return Future.value(true);
      },
      child: LoaderContainer(
        child: Scaffold(
          key: _scaffoldKey,
          appBar: CommonAppBar(
            children: [
              Text(
                "Deposit Refund".toUpperCase(),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: IconButton(
                  padding: EdgeInsets.symmetric(vertical: 4.0),
                  icon: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Image.asset(
                        "images/withdraw/history_icon.png",
                        height: 28.0,
                      ),
                      Text(
                        "History",
                        style:
                            Theme.of(context).primaryTextTheme.caption.copyWith(
                                  color: Colors.white,
                                ),
                      )
                    ],
                  ),
                  onPressed: () {
                    GamePlayAnalytics().onChargebackHistoryClicked(
                      context,
                      source: "chargeback_page",
                    );
                    launchChargebackHistory(context);
                  },
                ),
              )
            ],
          ),
          body: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    getChargebackBalanceView(),
                    getChargebackWidget(context),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  "Your deposit balance can be refunded only to your deposit instrument.",
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                        color: Colors.grey.shade800,
                        fontSize: 18.0,
                      ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
