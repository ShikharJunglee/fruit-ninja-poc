import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:http/http.dart' as http;

class ChargebackAPI {
  Future<Map<String, dynamic>> getChargebackData(BuildContext context) {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    http.Request req = http.Request(
        "GET", Uri.parse(config.apiUrl + AppUrl.CHARGEBACK_DATA.toString()));
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> result = json.decode(res.body);
        if (result["type"] == "error") {
          return {"error": true, "message": result["message"]};
        } else {
          return {
            "error": false,
            "data": result["data"],
          };
        }
      },
    );
  }

  Future<Map<String, dynamic>> makeChargebackRequest(
    BuildContext context, {
    @required double amount,
  }) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request("POST",
        Uri.parse(config.apiUrl + AppUrl.CHARGEBACK_REQUEST.toString()));
    req.body = json.encode({"amount": amount});

    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) async {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          response["error"] = false;
          return {
            "error": false,
            ...response["data"],
          };
        } else {
          Map<String, dynamic> response = json.decode(res.body);
          response["errorMessage"] = response["data"]["errorMessage"];
          response["error"] = true;

          return {
            "errorMessage": response["data"]["errorMessage"],
            "error": true,
          };
        }
      },
    );
  }
}
