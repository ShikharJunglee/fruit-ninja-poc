import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';

class ChargebackSuccess extends StatelessWidget {
  final Map<String, dynamic> chargebackResponse;

  ChargebackSuccess({this.chargebackResponse});

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "Charge-Back Requested".toUpperCase(),
                      style:
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                color: Colors.black,
                              ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 16.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "Requested ID :",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .caption
                                  .copyWith(
                                    color: Colors.black,
                                  ),
                            ),
                            TextSpan(
                              text: chargebackResponse["changebackConfig"]["id"]
                                      .toString() +
                                  ".",
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .bodyText2
                                  .copyWith(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w800,
                                  ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        "It will be processed within 7-14 days",
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(
                              color: Colors.black,
                            ),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 48.0,
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: Button(
                        size: ButtonSize.medium,
                        text: "OK".toUpperCase(),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
