import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/currencytext.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/models/profile/userbalance.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class ChargebackHistory extends StatefulWidget {
  final String source;
  ChargebackHistory({@required this.source});

  @override
  ChargebackHistoryState createState() => ChargebackHistoryState();
}

class ChargebackHistoryState extends State<ChargebackHistory> {
  String cookie = "";
  List<dynamic> recents;

  @override
  void initState() {
    super.initState();
    GamePlayAnalytics().onChargebackHistoryLoaded(
      context,
      source: widget.source,
    );
    WidgetsBinding.instance.addPostFrameCallback((_) => getRecentChargebacks());
  }

  getRecentChargebacks() async {
    Loader().showLoader(true);
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    http.Request req = http.Request(
      "GET",
      Uri.parse(
        config.apiUrl + AppUrl.CHARGEBACK_HISTORY.toString(),
      ),
    );
    return HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          setState(() {
            recents = response["data"];
          });
        } else {
          setState(() {
            recents = [];
          });
        }
      },
    ).whenComplete(() {
      Loader().showLoader(false);
    });
  }

  onCancelTransaction(Map<String, dynamic> transaction) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    User user = Provider.of(context, listen: false);
    UserBalance userBalance = user.userBalance;

    http.Request req = http.Request(
        "POST",
        Uri.parse(config.apiUrl +
            AppUrl.CANCEL_CHARGEBACK.toString() +
            transaction["id"].toString()));
    req.body = json.encode({});
    await HttpManager(http.Client()).sendRequest(req).then(
      (http.Response res) {
        Map<String, dynamic> response = json.decode(res.body);
        if (response["type"] == "success") {
          Map<String, dynamic> cancelledTransaction = response["data"];
          double totalCancelledChargebackAmount = 0;
          recents.forEach((transaction) {
            if (transaction["id"] == cancelledTransaction["id"]) {
              setState(() {
                transaction["status"] = cancelledTransaction["status"];
              });
              totalCancelledChargebackAmount +=
                  (transaction["amount"] + transaction["processingFee"]);
            }
          });
          userBalance.setDepositBucket(
              userBalance.depositBucket + totalCancelledChargebackAmount);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);

    return LoaderContainer(
      child: Scaffold(
        appBar: CommonAppBar(
          children: [
            Text(
              "Recent Refunds".toUpperCase(),
            ),
          ],
        ),
        body: recents == null || recents.length == 0
            ? Center(
                child: Text(
                  recents == null ? "Loading..." : "No recent transactions.",
                  style: Theme.of(context).primaryTextTheme.headline5.copyWith(
                        color: Colors.red,
                      ),
                ),
              )
            : ListView(
                children: recents.map((recentChargebacks) {
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: ListTile(
                      onTap: () {},
                      leading: CircleAvatar(
                        maxRadius: 28.0,
                        backgroundColor: Colors.blue.shade100,
                        child: CurrencyText(
                          showCurrencyDelimiters: false,
                          amount: double.tryParse(
                              recentChargebacks["amount"].toString()),
                          decimalDigits:
                              initData.experiments.miscConfig.amountPrecision,
                          style: TextStyle(
                            color: Colors.blue.shade900.withOpacity(0.6),
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                      title: Row(
                        children: [
                          Text(recentChargebacks["status"]),
                          Text(
                            recentChargebacks["processingFee"] > 0
                                ? "  (+ \$${recentChargebacks["processingFee"]} Fee)"
                                : "",
                            style: TextStyle(
                              color: Colors.grey[600],
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(DateTime.parse(recentChargebacks["createdDate"])
                              .toLocal()
                              .toString()
                              .split(".")[0]),
                          Text("#" + recentChargebacks["id"].toString()),
                        ],
                      ),
                      isThreeLine: true,
                      trailing: recentChargebacks["status"] == "REQUESTED"
                          ? RaisedButton(
                              onPressed: () {
                                onCancelTransaction(recentChargebacks);
                              },
                              child: Text(
                                "CANCEL".toUpperCase(),
                              ),
                              textColor: Colors.white70,
                              color: Theme.of(context).primaryColorDark,
                            )
                          : Container(
                              width: 0.0,
                            ),
                    ),
                  );
                }).toList(),
              ),
      ),
    );
  }
}
