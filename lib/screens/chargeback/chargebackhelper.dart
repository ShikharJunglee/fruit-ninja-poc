import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/chargeback/chargebackAPI.dart';
import 'package:solitaire_gold/utils/loader.dart';

class ChargebackHelper {
  ChargebackHelper._internal();
  factory ChargebackHelper() => withdrawHelper;
  static final ChargebackHelper withdrawHelper = ChargebackHelper._internal();

  ChargebackAPI _chargebackAPI = ChargebackAPI();
  launchChargeback(
    BuildContext context, {
    @required String source,
  }) async {
    AnalyticsManager().setJourney("Chargeback");
    Loader().showLoader(true, immediate: true);

    final result = await _chargebackAPI.getChargebackData(context);

    Loader().showLoader(false);

    if (result["error"] != true && result["data"] != null) {
      User user = Provider.of(context, listen: false);
      user.copyFrom(result["data"]["user"]);

      await routeManager.launchChargeback(
        context,
        source: "",
        withdrawData: result["data"]["changebackConfig"],
      );
      AnalyticsManager().setJourney(null);
    }
  }
}
