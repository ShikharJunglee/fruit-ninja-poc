import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/services.dart';
import 'package:solitaire_gold/api/contactus/contactusAPI.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/commonappbar.dart';
import 'package:solitaire_gold/commonwidgets/dropdown.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/basepage.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:provider/provider.dart';

class ContactUs extends StatefulWidget {
  @override
  ContactUsState createState() => ContactUsState();
}

class ContactUsState extends State<ContactUs> with BasePage {
  final ContactUsAPI _contactUsAPI = ContactUsAPI();

  final FocusNode emailFocusNode = FocusNode();
  final FocusNode mobileFocusNode = FocusNode();
  final FocusNode descriptionFocusNode = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _description = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _mobileController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool showSubCategory = false;
  List<dynamic> categoriesData = [];
  List<dynamic> subCategoriesData = [];

  Map<String, dynamic> selectedCategory;
  Map<String, dynamic> selectedSubCategory;

  int descriptionMinChar = 10;
  bool selectedCategoryError = false;

  @override
  void initState() {
    super.initState();
    GamePlayAnalytics().onContactUsPageLoaded(context, source: "app_drawer");
    WidgetsBinding.instance.addPostFrameCallback((_) => setData());
  }

  setData() {
    User user = Provider.of(context, listen: false);
    _emailController.text = user.email;
    _mobileController.text = user.mobile;

    _getFormDetails(context);
  }

  _getFormDetails(BuildContext context) async {
    Loader().showLoader(true, immediate: true);
    final result = await _contactUsAPI.getContactUsData(context);

    Loader().showLoader(false);

    if (result["error"] != true) {
      setState(() {
        categoriesData = result['data'] != null ? result['data'] : [];
      });
    }
  }

  submitForm(BuildContext context) async {
    GamePlayAnalytics().onContactUsSubmit(
      context,
      source: "contact_us",
    );
    if (selectedCategory != null) {
      setState(() {
        selectedCategoryError = false;
      });
    }
    if (selectedCategory == null) {
      _formKey.currentState.validate();
      setState(() {
        selectedCategoryError = true;
      });
    } else if (_formKey.currentState.validate()) {
      Loader().showLoader(true, immediate: true);

      Map<String, dynamic> result = await _contactUsAPI.submitContactUs(
        context,
        category: selectedCategory["id"],
        subCategory:
            selectedSubCategory != null ? selectedSubCategory["id"] : "",
        comments: _description.text,
        email: _emailController.text,
        mobile: _mobileController.text,
      );

      Loader().showLoader(false);

      if (result["error"] == true) {
        showMessageOnTop(context,
            msg: "Unable to process request. Please try again...!");
      } else {
        FocusScope.of(context).unfocus();
        await showAlertOnTop(
          context,
          title: "Request received",
          msg: "Allow us to revert in 48 hrs.",
        );

        Navigator.of(context).pop();
      }
    }
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  @override
  void dispose() {
    _emailController.dispose();
    _mobileController.dispose();
    _description.dispose();
    emailFocusNode.dispose();
    mobileFocusNode.dispose();
    descriptionFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of(context, listen: false);
    AppConfig config = Provider.of(context, listen: false);

    return LoaderContainer(
      child: Scaffold(
        key: _scaffoldKey,
        appBar: CommonAppBar(
          children: <Widget>[
            Text(
              "Contact Us".toUpperCase(),
            ),
          ],
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                margin: EdgeInsets.all(8.0),
                padding: EdgeInsets.only(top: 24.0, left: 8.0, right: 8.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6.0),
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 4.0,
                      spreadRadius: 1.0,
                      color: Colors.grey.shade500,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.only(top: 4.0),
                                child: SimpleTextBox(
                                  focusNode: emailFocusNode,
                                  controller: _emailController,
                                  isDense: true,
                                  labelText: "Email ID",
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please enter email address.";
                                    } else if (!validateEmail(value)) {
                                      return "Please enter valid email address.";
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: SimpleTextBox(
                                  focusNode: mobileFocusNode,
                                  controller: _mobileController,
                                  isDense: true,
                                  enabled: !(user.mobile != null &&
                                      user.mobile.length > 0),
                                  labelText: "Mobile Number",
                                  inputFormatters: [
                                    LengthLimitingTextInputFormatter(
                                      10,
                                    )
                                  ],
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return "Please enter mobile number.";
                                    } else if (!isNumeric(value) ||
                                        value.length != 10) {
                                      return "Please enter valid mobile number.";
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.phone,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Dropdown(
                                            label: "Select issue category",
                                            dropDownItems: categoriesData,
                                            valueKey: "id",
                                            onSelectCallback:
                                                (Map<String, dynamic>
                                                    selectedCategory) {
                                              GamePlayAnalytics()
                                                  .onContactUsCategorySelected(
                                                context,
                                                source: "contact_us",
                                                category:
                                                    selectedCategory["id"],
                                              );
                                              setState(() {
                                                this.selectedCategory =
                                                    selectedCategory;
                                                this.selectedSubCategory = null;
                                                this.subCategoriesData =
                                                    selectedCategory[
                                                        "subcategories"];
                                              });
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                    if (selectedCategoryError)
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          "Please select issue category",
                                          style: TextStyle(
                                              fontSize: 12.0,
                                              color:
                                                  Theme.of(context).errorColor),
                                        ),
                                      ),
                                  ],
                                ),
                              ),
                              if (subCategoriesData.length > 0)
                                Padding(
                                  padding: const EdgeInsets.only(top: 16.0),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Dropdown(
                                          label: "Select issue type",
                                          dropDownItems: subCategoriesData,
                                          valueKey: "id",
                                          onSelectCallback:
                                              (Map<String, dynamic>
                                                  selectedSubCategory) {
                                            GamePlayAnalytics()
                                                .onContactUsSubcategorySelected(
                                              context,
                                              source: "contact_us",
                                              subcategory:
                                                  selectedSubCategory["id"],
                                            );
                                            this.selectedSubCategory =
                                                selectedSubCategory;
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              Padding(
                                padding: EdgeInsets.only(top: 16.0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: SimpleTextBox(
                                        controller: _description,
                                        isDense: true,
                                        minLines: 5,
                                        maxLines: 5,
                                        focusNode: descriptionFocusNode,
                                        validator: (value) {
                                          if (value.isEmpty) {
                                            return "Please explain your issue";
                                          }
                                          if (value.length <
                                              descriptionMinChar) {
                                            return "Description minimum length should be $descriptionMinChar characters";
                                          }
                                          return null;
                                        },
                                        hintText: "Type your message",
                                        keyboardType: TextInputType.multiline,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 32.0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    "For most asked doubt and concerns,",
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .headline6
                                        .copyWith(
                                          color: Colors.grey,
                                        ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 4.0),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          "please refer to ",
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline6
                                              .copyWith(
                                                color: Colors.grey,
                                              ),
                                          textAlign: TextAlign.center,
                                        ),
                                        InkWell(
                                          onTap: () {
                                            config.staticPageConfig
                                                .forEach((conf) {
                                              if (conf["name"].toLowerCase() ==
                                                  "faq".toLowerCase()) {
                                                routeManager.launchStaticPage(
                                                    context,
                                                    url: conf["data"]["url"],
                                                    title: conf["data"]
                                                        ["title"]);
                                              }
                                            });
                                          },
                                          child: Text(
                                            "FAQ ",
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .headline6
                                                .copyWith(
                                                  color: Colors.blue,
                                                  fontStyle: FontStyle.italic,
                                                ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Text(
                                          " section.",
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .headline6
                                              .copyWith(
                                                color: Colors.grey,
                                              ),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Button(
                                text: "SEND MESSAGE",
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .headline5
                                    .copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                onPressed: () {
                                  submitForm(context);
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
