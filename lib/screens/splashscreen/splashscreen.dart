import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flare_flutter/flare_cache.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/analytics/gameplayevents.dart';
import 'package:solitaire_gold/api/auth/auth.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/solitairegold/solitaire_messages.dart';
import 'package:solitaire_gold/screens/splashscreen/downloadapk.dart';
import 'package:solitaire_gold/services/myfirebasecrashlytics.dart';
import 'package:solitaire_gold/services/myhelperclass.dart';
import 'package:solitaire_gold/services/mywebengageservice.dart';
import 'package:solitaire_gold/services/fluttertonativeservice.dart';
import 'package:solitaire_gold/services/mybranchioservice.dart';
import 'package:solitaire_gold/services/myfirebasemanager.dart';
import 'package:solitaire_gold/utils/sharedpref/sharedpref.dart';
import 'package:solitaire_gold/utils/storagepermission.dart';
import 'package:http/http.dart' as http;
import 'package:solitaire_gold/models/apiresponse.dart';
import 'package:solitaire_gold/screens/auth/baseauth.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:package_info/package_info.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/appconfig.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> with BaseAuth {
  bool isAppLoaded = false;
  double loadingPercent = 5.0;
  AuthAPI _authAPI = AuthAPI();
  StoragePermission _permission = StoragePermission();

  bool isIPad = false;

  final Completer<bool> _unityReady = Completer<bool>();
  // final Completer<UnityWidgetController> _controllerCompleter =
  //     Completer<UnityWidgetController>();
  // final Completer<String> cookieCompleter = Completer<String>();
  final FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => initApp(context));
    super.initState();
  }

  initApp(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    isIPad = await MyHelperClass.isIpad();
    await getRequiredData(context, config);
    GamePlayAnalytics().onAppLaunch(context);
  }

  getRequiredData(BuildContext context, AppConfig config) async {
    AppConfig config = Provider.of(context, listen: false);

    setLoadingPercentage(5.0);
    isAppLoaded = true;
    setLoadingPercentage(10.0);
    await initWebview(context);

    await firebaseCrashAnalytics.initializeFlutterFire();
    await MyBranchIoService.initBranchIoPlugin(config, context);
    await MyFirebaseMessagingService.getFirebaseToken(config);
    if (Platform.isAndroid) {
      await FlutterToNativeService.getAdvertisingId(config);
    }
    if (config.fcmSubscribeId != null) {
      await MyFirebaseMessagingService.subscribeToFirebaseTopic(
          config.fcmSubscribeId);
    }
    final initData = await _getInitData(config);
    await setLoadingPercentage(30.0);
    _setInitData(config, initData);
    await setLoadingPercentage(40.0);

    if (!isIPad) {
      if (kDebugMode) {
        await _unityReady.future.timeout(
          Duration(seconds: 6),
          onTimeout: () => Future.value(false),
        );
      } else {
        await _unityReady.future;
      }
    }
    // if (!isIPad) {
    //   await _controllerCompleter.future;
    //   await copyUnityCookieForBackwardCompatibility();
    // }

    await checkForUpdate();

    _startAnalytics(context);

    await setLoadingPercentage(70.0);

    bool isUserLoggedIn = await _isSessionExists(context);

    await setLoadingPercentage(100.0);

    if (isUserLoggedIn) {
      MyWebEngageService.onUserInfoRefreshed(context);
      routeManager.launchLobbyScreen(context);
    } else {
      await precacheFlare();
      routeManager.launchSignupScreen(context);
    }
  }

  // copyUnityCookieForBackwardCompatibility() async {
  //   String cookie = await cookieCompleter.future.timeout(
  //     Duration(seconds: 2),
  //     onTimeout: () => null,
  //   );
  //   if (cookie != null) {
  //     HttpManager.cookie = cookie;
  //     await SharedPrefHelper().saveCookieToStorage(cookie);
  //   }

  //   return Future.value(null);
  // }

  initWebview(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    try {
      await flutterWebviewPlugin
          .launch(
            config.apiUrl + AppUrl.COOKIE_PAGE.value,
            hidden: true,
          )
          .timeout(Duration(seconds: 4));
      await FlutterWebviewPlugin()
          .evalJavascript("document.cookie='" + HttpManager.cookie + "'; ");
    } catch (e) {}
  }

  _startAnalytics(BuildContext context) {
    AppConfig config = Provider.of(context, listen: false);
    InitData initData = Provider.of(context, listen: false);

    AnalyticsManager().init(
      config: config,
      url: initData.analyticsConfig.analyticsURL,
      timeout: initData.analyticsConfig.visitTimeout,
      enabled: initData.analyticsConfig.analyticsEnabled,
      duration: initData.analyticsConfig.analyticsSendInterval,
    );
    AnalyticsManager().statAnalytics();
  }

  precacheFlare() async {
    final page1AssetProvider = AssetFlare(
      bundle: rootBundle,
      name: "images/ftue/page3.flr",
    );
    await cachedActor(page1AssetProvider);

    final page3AssetProvider = AssetFlare(
      bundle: rootBundle,
      name: "images/ftue/page3.flr",
    );
    await cachedActor(page3AssetProvider);

    return null;
  }

  Future<bool> _isSessionExists(BuildContext context) async {
    final result = await _authAPI.getLoginStatus(context);
    if (result["error"] == false) {
      var userProvider = Provider.of<User>(context, listen: false);
      userProvider.copyFrom(result["user"]);

      User user = Provider.of(context, listen: false);
      AnalyticsManager().setUser(user);
      await firebaseCrashAnalytics
          .setCrashlyticsUserIdentity(user.id.toString());

      return true;
    }
    return false;
  }

  Future<void> checkForUpdate() async {
    InitData initData = Provider.of(context, listen: false);
    if (initData.updatePolicy.update && !Platform.isIOS) {
      if (await validUpdateCount() || initData.updatePolicy.forcedUpdate) {
        await _permission.checkForPermission();
        return await _showUpdatingAppDialog(
          initData.updatePolicy.updateUrl,
          logs: initData.updatePolicy.updatePopup.logs,
          isForceUpdate: initData.updatePolicy.forcedUpdate,
          header: initData.updatePolicy.updatePopup.header,
        );
      }
    }
  }

  _showUpdatingAppDialog(String url,
      {bool isForceUpdate, List<dynamic> logs, String header}) async {
    return await showDialog(
      barrierColor: Color.fromARGB(175, 0, 0, 0),
      context: context,
      builder: (BuildContext context) {
        return DownloadAPK(
          url: url,
          logs: logs,
          isForceUpdate: isForceUpdate,
          title: header,
        );
      },
      barrierDismissible: false,
    );
  }

  Future<dynamic> _getInitData(AppConfig config) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = "0.0";
    HttpManager.appVersion = packageInfo.version;

    if (Platform.isAndroid) {
      version = packageInfo.version;
    }
    http.Request req =
        http.Request("POST", Uri.parse(config.apiUrl + AppUrl.INIT_DATA.value));

    req.headers["version"] = version.toString();
    req.headers["isIos"] = Platform.isIOS.toString();

    req.body = json.encode(
        {"version": version.toString(), "code": "ind", "build": "1.0.0"});

    Map<String, dynamic> response;
    try {
      response = await HttpManager(http.Client())
          .sendRequest(req)
          .then((http.Response res) {
        if (res.statusCode >= 200 && res.statusCode <= 299) {
          ApiResponse response = ApiResponse.fromJson(json.decode(res.body));
          if (response.success) {
            return response.data;
          } else {
            return null;
          }
        }
        return null;
      });
    } on SocketException {
      await Future.delayed(Duration(milliseconds: 500), () async {
        response = await _getInitData(config);
      });
    }
    return response;
  }

  _setInitData(AppConfig config, Map<String, dynamic> initData) {
    if (initData == null) {
    } else {
      InitData initDataProvider = Provider.of(context, listen: false);
      initDataProvider.copyFrom(initData);
      config.setWSUrl(initData["wsUrl"]);
      config.setOtpResendTime(
          initData["experiments"]["signUpData"]["resendTime"]);
      config.setResendPasswordCount(
          initData["experiments"]["signUpData"]["resendPasswordCount"]);
      config.setOtpMaxResendCount(
          initData["experiments"]["signUpData"]["maxResendCount"]);
      config.setOtpMaxResendTime(
          initData["experiments"]["signUpData"]["maxResendTime"]);
      config
          .setEmailSwitch(initData["experiments"]["signUpData"]["emailSwitch"]);
      config.setStaticPageConfig(initData["experiments"]["staticPages"]);
    }
  }

  setLoadingPercentage(double percent) {
    setState(() {
      loadingPercent = percent;
    });
    return Future.delayed(Duration(milliseconds: 100));
  }

  Future<bool> validUpdateCount() async {
    InitData initData = Provider.of(context, listen: false);
    int updateTime = int.tryParse(
        await SharedPrefHelper().getFromSharedPref("updateTime") ?? "0");
    int counter = int.tryParse(
        await SharedPrefHelper().getFromSharedPref("updateCounter") ?? "0");
    if (updateTime <
        (DateTime.now().millisecondsSinceEpoch) -
            (initData.updatePolicy.updatePopup.interval * 60 * 60 * 1000)) {
      SharedPrefHelper().saveToSharedPref(
          "updateTime", DateTime.now().millisecondsSinceEpoch.toString());
      SharedPrefHelper().saveToSharedPref("updateCounter", "1");
      return true;
    } else if (counter < initData.updatePolicy.updatePopup.counter) {
      SharedPrefHelper()
          .saveToSharedPref("updateCounter", (counter + 1).toString());
      return true;
    }
    return false;
  }

  onMessageFromUnity(String message) {
    Map<String, dynamic> unityMessage = {};
    try {
      unityMessage = json.decode(message);
    } catch (e) {
      print(e);
    }

    String type = unityMessage["type"].toString();

    if (type == MessageType.UnityReady.getString()) {
      _registerUnityReady();
    }
  }

  _registerUnityReady() {
    _unityReady.complete(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            if (isAppLoaded && !isIPad)
              UnityWidget(
                onUnityViewCreated: (controller) {},
                onUnityMessage: (controller, message) {
                  onMessageFromUnity(message);
                  // Map<String, dynamic> cookieMessage = json.decode(message);
                  // if (!cookieCompleter.isCompleted &&
                  //     cookieMessage["type"] == -4 &&
                  //     cookieMessage["cookie"] != null &&
                  //     cookieMessage["cookie"].toString().isNotEmpty) {
                  //   cookieCompleter.complete(cookieMessage["cookie"]);
                  // } else if (!cookieCompleter.isCompleted) {
                  //   cookieCompleter.complete(null);
                  // }
                },
              ),
            Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(75, 10, 12, 1),
                            image: DecorationImage(
                              image: AssetImage("images/splash.png"),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.only(bottom: 128.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.black,
                                      width: 1.0,
                                    ),
                                    color: Color.fromRGBO(80, 5, 15, 1),
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                  child: ClipRRect(
                                    clipBehavior: Clip.hardEdge,
                                    borderRadius: BorderRadius.circular(4.0),
                                    child: LinearPercentIndicator(
                                      lineHeight: 20.0,
                                      animation: true,
                                      linearStrokeCap: LinearStrokeCap.butt,
                                      animateFromLastPercent: true,
                                      animationDuration: 100,
                                      padding: EdgeInsets.all(0.0),
                                      backgroundColor: Colors.transparent,
                                      percent: loadingPercent / 100,
                                      linearGradient: LinearGradient(
                                        colors: [
                                          Colors.yellow.shade800,
                                          Colors.yellow.shade600,
                                          Colors.yellow.shade200,
                                          Colors.yellow.shade600,
                                          Colors.yellow.shade800,
                                        ],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter,
                                      ),
                                    ),
                                  ),
                                  width:
                                      MediaQuery.of(context).size.width * 0.70,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
