import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:solitaire_gold/popups/common/customdialog.dart';
import 'package:solitaire_gold/popups/common/common_dialog_frame.dart';
import 'package:solitaire_gold/commonwidgets/colorbutton.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/storagepermission.dart';
import 'package:provider/provider.dart';
import 'package:permission/permission.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';

class DownloadAPK extends StatefulWidget {
  final String url;
  final bool isForceUpdate;
  final List<dynamic> logs;
  final String title;

  DownloadAPK({
    this.url,
    this.logs,
    this.isForceUpdate = false,
    this.title,
  });

  @override
  DownloadAPKState createState() => DownloadAPKState();
}

class DownloadAPKState extends State<DownloadAPK> with StoragePermission {
  var taskId;
  int currentStatus = 0;
  int downloadProgress = 0;
  ReceivePort _port = ReceivePort();

  @override
  void initState() {
    super.initState();
    IsolateNameServer.registerPortWithName(_port.sendPort, 'test');

    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];

      onDownloadStatusChange(id, status, progress);
    }, onDone: () {
      print("Closed");
    }, onError: (err) {
      print("Error");
    });

    WidgetsBinding.instance
        .addPostFrameCallback((_) => _checkForUpdate(context));
  }

  _checkForUpdate(BuildContext context) async {
    await FlutterDownloader.initialize();
    FlutterDownloader.registerCallback(downloadUpdate);
    PermissionStatus status = await checkForPermission();
    if (status != null) {
      setState(() {
        permissionStatus = status;
      });
    }
    if (widget.logs == null) {
      _startDownload(context);
    }
  }

  void _startDownload(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    Directory appDocDir = await getExternalStorageDirectory();
    String appDocPath = appDocDir.path;

    taskId = await FlutterDownloader.enqueue(
      url: widget.url,
      savedDir: appDocPath,
      showNotification: true,
      openFileFromNotification: true,
      fileName: config.appName + ".apk",
    );

    setState(() {
      currentStatus = 1;
    });
  }

  void onDownloadStatusChange(
      String id, DownloadTaskStatus status, int progress) {
    if (taskId == id) {
      setState(() {
        downloadProgress = progress;
      });
      if (status == DownloadTaskStatus.complete) {
        setState(() {
          currentStatus = 2;
        });
      } else if (status == DownloadTaskStatus.failed) {
        setState(() {
          currentStatus = 3;
        });
      }
    }
  }

  static downloadUpdate(String id, DownloadTaskStatus status, int progress) {
    final SendPort send = IsolateNameServer.lookupPortByName('test');
    send.send([id, status, progress]);
  }

  void _installAPK(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    if (taskId != null) {
      FlutterDownloader.open(taskId: taskId);
    } else {
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      Directory appDocDir = await getExternalStorageDirectory();
      String fileUrl = appDocDir.path + "/" + config.appName + ".apk";
      AndroidIntent(
        action: "action_view",
        apkUrl: fileUrl,
        package: packageInfo.packageName,
      ).openFile();
    }
  }

  void _onGivePermission() async {
    PermissionStatus status = await askForPermission();
    if (status != null) {
      setState(() {
        permissionStatus = status;
      });
    }
  }

  List<Widget> getTitle() {
    return [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: Image.asset(
              "images/update-icon.png",
              height: 64.0,
            ),
          ),
        ],
      ),
      Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Text(
                widget.title ?? "We are upgrading!",
                style: Theme.of(context).primaryTextTheme.headline5.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      )
    ];
  }

  Widget getDownloadingWidget() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 32.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(bottom: 8.0),
                    child: Row(
                      children: <Widget>[
                        Text(
                          downloadProgress.toString() + "%",
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                  ),
                  LinearProgressIndicator(
                    backgroundColor: Colors.black12,
                    valueColor: AlwaysStoppedAnimation<Color>(
                        Theme.of(context).buttonColor),
                    value: (downloadProgress / 100),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getDownloadFailedWidget() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 32.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Text(
              "Download Failed !",
              textAlign: TextAlign.center,
              style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                    color: Colors.yellow,
                    fontStyle: FontStyle.italic,
                  ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: widget.isForceUpdate
          ? () => Future.value(false)
          : () => Future.value(true),
      child: permissionStatus == PermissionStatus.allow
          ? CustomDialog(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              dialog: Dialog(
                backgroundColor: Colors.transparent,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: CommonDialogFrame(
                  canClose: !widget.isForceUpdate,
                  titleAsset: "images/titles/upgrading.png",
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [Color(0xFF1F0105), Color(0xFF570711)],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                      borderRadius: BorderRadius.circular(6.0),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                ...getTitle(),
                                Padding(
                                  padding: EdgeInsets.only(
                                    left: 32.0,
                                    right: 32.0,
                                    top: 8.0,
                                    bottom: 8.0,
                                  ),
                                  child: Column(
                                    children: widget.logs.map((text) {
                                      return Container(
                                        padding: EdgeInsets.only(bottom: 8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        right: 8.0, top: 3.0),
                                                    child: Container(
                                                      height: 6.0,
                                                      width: 6.0,
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        shape: BoxShape.circle,
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Text(
                                                      text,
                                                      style: Theme.of(context)
                                                          .primaryTextTheme
                                                          .subtitle1
                                                          .copyWith(
                                                            color: Colors.white,
                                                          ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                                if (currentStatus == 0)
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16.0),
                                    child: Row(
                                      children: <Widget>[
                                        if (!widget.isForceUpdate)
                                          Expanded(
                                            child: Container(
                                              height: 56.0,
                                              padding: EdgeInsets.all(4.0),
                                              child: ColorButton(
                                                color: Colors.grey.shade600,
                                                child: Text(
                                                  "CANCEL",
                                                  style: Theme.of(context)
                                                      .primaryTextTheme
                                                      .bodyText1
                                                      .copyWith(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.w800,
                                                      ),
                                                ),
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                              ),
                                            ),
                                          ),
                                        Expanded(
                                          child: Container(
                                            height: 56.0,
                                            padding: EdgeInsets.all(4.0),
                                            child: ColorButton(
                                              elevation: 0.0,
                                              child: Text(
                                                "Update now".toUpperCase(),
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .bodyText1
                                                    .copyWith(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w800,
                                                    ),
                                                textAlign: TextAlign.center,
                                              ),
                                              onPressed: () {
                                                _startDownload(context);
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                            if (currentStatus == 1)
                              Column(
                                children: <Widget>[
                                  getDownloadingWidget(),
                                ],
                              ),
                            if (currentStatus == 2)
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 16.0, left: 16.0, right: 16.0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        height: 56.0,
                                        padding: EdgeInsets.all(4.0),
                                        child: ColorButton(
                                          elevation: 0.0,
                                          child: Text(
                                            "Install now".toUpperCase(),
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .bodyText1
                                                .copyWith(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w800,
                                                ),
                                          ),
                                          onPressed: () {
                                            _installAPK(context);
                                          },
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            if (currentStatus == 3)
                              Column(
                                children: <Widget>[
                                  getDownloadFailedWidget(),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 16.0, left: 16.0, right: 16.0),
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            height: 56.0,
                                            padding: EdgeInsets.all(4.0),
                                            child: ColorButton(
                                              elevation: 0.0,
                                              child: Text(
                                                "Retry".toUpperCase(),
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .bodyText1
                                                    .copyWith(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.w800,
                                                    ),
                                              ),
                                              onPressed: () {
                                                _startDownload(context);
                                              },
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          : AlertDialog(
              title: Text("Download update"),
              titlePadding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
              contentPadding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                            "Please allow storage permission to download an update."),
                      ),
                    ],
                  )
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    "Give permission".toUpperCase(),
                  ),
                  onPressed: () {
                    _onGivePermission();
                  },
                ),
              ],
            ),
    );
  }
}
