import 'dart:async';
import 'package:flutter/services.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:sms/sms.dart';
import 'package:flutter/material.dart';

import 'package:solitaire_gold/screens/auth/login/otpcounter.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';

class OtpInput extends StatefulWidget {
  final Function onResend;
  final Function onVerify;
  OtpInput({@required this.onResend, @required this.onVerify});

  @override
  _OtpInputState createState() => _OtpInputState();
}

class _OtpInputState extends State<OtpInput> {
  TextEditingController otpController = TextEditingController();

  final otpFormKey = new GlobalKey<FormState>();
  int otpLength = 4;
  bool shouldSubmit = false;
  SmsReceiver receiver = SmsReceiver();
  StreamSubscription<SmsMessage> smsAutoReadSubscription;
  StreamSubscription<dynamic> otpSubmitError;
  bool bShowError;
  bool bHideSpacing = false;
  String errorMessage;

  @override
  void initState() {
    smsAutoReadSubscription = receiver.onSmsReceived.listen((SmsMessage msg) {
      otpController.text = msg.body.substring(0, 4);
      widget.onVerify(otpController.text);
    });
    otpSubmitError =
        EventManager().subscribe(EventName.VERIFY_OTP_ERROR, showError);
    errorMessage = "";
    bShowError = false;
    super.initState();
  }

  @override
  void dispose() {
    smsAutoReadSubscription.cancel();
    otpSubmitError.cancel();
    super.dispose();
  }

  void showError(var error) {
    setState(() {
      bShowError = true;
      errorMessage = error["message"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: SingleChildScrollView(
        child: Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Form(
                  key: otpFormKey,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 32.0, bottom: 8.0),
                        child: Stack(
                          alignment: Alignment.topRight,
                          children: <Widget>[
                            SimpleTextBox(
                              onChanged: (String text) => {
                                if (bShowError)
                                  {
                                    setState(() {
                                      bShowError = false;
                                      errorMessage = "";
                                    })
                                  }
                              },
                              controller: otpController,
                              labelText: "Enter OTP",
                              maxLength: otpLength,
                              maxLines: 1,
                              keyboardType: TextInputType.number,
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly,
                              ],
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Required";
                                } else if (value.length != otpLength) {
                                  return "Invalid OTP";
                                }
                                setState(() {
                                  bHideSpacing = false;
                                });
                                return null;
                              },
                            ),
                            OTPCounter(
                              onResend: widget.onResend,
                            ),
                          ],
                        ),
                      ),
                      bShowError
                          ? Container(
                              margin: EdgeInsets.only(
                                left: 12,
                                bottom: 6,
                              ),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    errorMessage,
                                    style: TextStyle(
                                        color: Color.fromRGBO(206, 61, 61, 1)),
                                  ),
                                ],
                              ))
                          : Container(
                              height: 0,
                            ),
                      Button(
                        text: "Submit".toUpperCase(),
                        onPressed: () {
                          if (otpFormKey.currentState.validate()) {
                            widget.onVerify(otpController.text);
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
