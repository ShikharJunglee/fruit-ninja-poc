import 'dart:async';

import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:provider/provider.dart';

class OTPCounter extends StatefulWidget {
  final Function onResend;
  OTPCounter({this.onResend});

  @override
  _OTPCounterState createState() => _OTPCounterState();
}

class _OTPCounterState extends State<OTPCounter> {
  Timer _timer;
  int _currentTimeLapse = 0;

  @override
  void initState() {
    startTimer();
    super.initState();
  }

  void startTimer() {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    setState(() {
      _currentTimeLapse = config.otpResendTime;
    });
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) => setState(
        () {
          if (_currentTimeLapse < 1) {
            timer.cancel();
          } else {
            _currentTimeLapse = _currentTimeLapse - 1;
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            if (_currentTimeLapse == 0)
              Text(
                "Resend",
                style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                      color: Theme.of(context).buttonColor,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w400,
                    ),
              ),
            if (_currentTimeLapse > 0)
              Text(
                "Resend in ",
                style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                      color: Colors.grey,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w400,
                    ),
              ),
            if (_currentTimeLapse > 0)
              Text(
                _currentTimeLapse.toString(),
                style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                      color: Colors.blue,
                      fontWeight: FontWeight.w400,
                    ),
              ),
          ],
        ),
      ),
      onTap: _currentTimeLapse == 0
          ? () {
              _timer.cancel();
              _currentTimeLapse = 30;
              widget.onResend();
              startTimer();
            }
          : null,
    );
  }
}
