class SocialSignInMethods {
  final int _value;
  const SocialSignInMethods._internal(this._value);

  int get value => _value;

  get hashCode => _value.hashCode;

  operator ==(status) => status._value == this._value;

  toString() => _value.toString();

  static SocialSignInMethods from(int value) =>
      SocialSignInMethods._internal(value);
  static const NO_SOCIAL = const SocialSignInMethods._internal(0);
  static const GOOGLE = const SocialSignInMethods._internal(1);
  static const FACEBOOK = const SocialSignInMethods._internal(2);
  static const BOTH = const SocialSignInMethods._internal(3);
}
