import 'dart:async';
import 'dart:io';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/api/auth/auth.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/carousel.dart';
import 'package:solitaire_gold/models/initdata.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/screens/auth/login/passwordinput.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/screens/auth/baseauth.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:solitaire_gold/commonwidgets/loadercontainer.dart';
import 'package:solitaire_gold/screens/auth/login/otpinput.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/screens/auth/login/social_signin_methods.dart';
import 'package:solitaire_gold/screens/solitairegold/popups/disconnection.dart';
import 'package:solitaire_gold/services/myhelperclass.dart';
import 'package:solitaire_gold/services/myuserloginservice.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/channelmanager/channelmanager.dart';
import 'package:solitaire_gold/utils/eventmanager.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:http/http.dart' as http;

class Signin extends StatefulWidget {
  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> with BaseAuth, AuthAPI {
  String _mobileNumber;
  String emailMatchError = "";
  String otpSentFor;
  int noOfOTPSent = 0;
  bool passwordPresent = false;
  bool showOTPError = false;
  String errorMessage = "";
  bool isContinueEnabled = true;
  Timer timer;
  var user;

  int otpCount = 0;
  int otpMaxCount = 0;
  int resendTimer = 0;
  int firstOtpTime = 0;

  SmsAutoFill _autoFill = SmsAutoFill();

  TextEditingController mobileController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  final formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  List<dynamic> topBanners = [];
  List<dynamic> bottomBanners = [];

  bool bConsiderClosingKeyboard = false;

  bool bAskedForHint = false;
  FocusNode currentFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    currentFocus.addListener(() async {
      if (currentFocus.hasFocus && !bAskedForHint) {
        bAskedForHint = true;
        scheduleMicrotask(() {
          _askPhoneHint();
        });
      }
    });

    mobileController.addListener(() {
      bool bConsider = true;

      if (mobileController.text.length == 13 &&
          mobileController.text.startsWith("+91")) {
        mobileController.text = mobileController.text.substring(3, 13);
        mobileController.selection = TextSelection.collapsed(offset: 10);
      } else if (mobileController.text.length == 12 &&
          mobileController.text.startsWith("+91")) {
        mobileController.text = mobileController.text.substring(3, 12);
        mobileController.selection = TextSelection.collapsed(offset: 10);
      } else if (mobileController.text.startsWith("+1")) {
        mobileController.text = mobileController.text.substring(2);
        mobileController.selection = TextSelection.collapsed(offset: 10);
      }

      if (bConsiderClosingKeyboard && mobileController.text.length == 10) {
        bConsider = false;
        FocusScope.of(context).unfocus();
      }
      setState(() {
        _mobileNumber = mobileController.text;
      });
      bConsiderClosingKeyboard = bConsider;
    });
    WidgetsBinding.instance
        .addPostFrameCallback((_) => onWidgetLoaded(context));
  }

  Future<void> _askPhoneHint() async {
    String hint = await _autoFill.hint;
    mobileController.value = TextEditingValue(text: hint ?? '');
  }

  onWidgetLoaded(BuildContext context) async {
    List topBannners = await getBanners(context, zone: 2);
    List bottomBannners = await getBanners(context, zone: 1);
    setState(() {
      topBanners = topBannners;
      bottomBanners = bottomBannners;
    });
  }

  onTournament(dynamic tournament) {
    routeManager.launchLobbyScreen(context);
  }

  _requestOTP({bool isFirstInput = true}) async {
    if (!await HttpManager(http.Client()).isInternetConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection(
            useWebSocketForDisconnectionDetection: false,
          );
        },
      );
    }

    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    Widget child = OtpInput(
      onResend: () {
        _requestOTP(isFirstInput: false);
      },
      onVerify: (String otp) async {
        var data = await _doSignIn(otp);
        if (data == null || !data["error"]) {
        } else {
          EventManager().publish(EventName.VERIFY_OTP_ERROR, data: data);
        }
      },
    );
    bool showOtp = false;
    bool showPassword = false;
    if (isFirstInput) {
      showOtp = true;
      showPassword = false;
      noOfOTPSent = 0;
    } else if (noOfOTPSent == 0 && firstOtpTime == 0) {
      showOtp = true;
      showPassword = false;
    } else if (noOfOTPSent <= config.maxOtpResendCount &&
        DateTime.now().millisecondsSinceEpoch - firstOtpTime <
            config.maxOtpResendTime * 1000) {
      if (passwordPresent && noOfOTPSent > config.resendPasswordCount) {
        showPassword = true;
        showOtp = false;
      } else {
        showOtp = true;
        showPassword = false;
      }
    } else if (DateTime.now().millisecondsSinceEpoch - firstOtpTime >
        config.maxOtpResendTime * 1000) {
      firstOtpTime = DateTime.now().millisecondsSinceEpoch;
      otpCount = 1;
      showOtp = true;
      showPassword = false;
    } else {
      showOtp = false;
      showPassword = false;
    }
    Map<String, dynamic> result;
    if (showOtp) {
      if (otpSentFor == _mobileNumber && !isFirstInput) {
        Loader().showLoader(true, immediate: true);
        result =
            await resendOTPForLogin(context: context, mobile: _mobileNumber);
        noOfOTPSent++;
        Loader().showLoader(false);
      } else {
        result = await requestOTPForLogin(
          context: context,
          mobile: _mobileNumber,
          email: emailController.text,
        );
        timer.cancel();
        setState(() {
          isContinueEnabled = true;
        });
        if (result["error"]) {
          if (result["message"] != null) {
            Loader().showLoader(false);
            setState(() {
              emailMatchError = result["message"];
            });
          }
        } else {
          Loader().showLoader(false);
          otpSentFor = _mobileNumber;
          noOfOTPSent++;
          if (firstOtpTime == 0) {
            firstOtpTime = DateTime.now().millisecondsSinceEpoch;
          }
          if (result["data"]["data"]["userId"].toString() == "false" ||
              (result["data"]["data"]["passwordPresent"] != null &&
                  result["data"]["data"]["passwordPresent"])) {
            passwordPresent = true;
            user = result["data"]["data"]["userId"];
          } else {
            passwordPresent = false;
            user = false;
          }
        }
      }
    }
    if (showPassword) {
      if (!isFirstInput) {
        Navigator.of(context).pop();
      }
      isFirstInput = true;
      Loader().showLoader(false);
      child = Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          PasswordInput(
            onVerify: (String password) async {
              if (user.toString() != "false") {
                var data = await _doPasswordSignIn(password);
                if (data == null || !data["error"]) {
                } else {
                  EventManager()
                      .publish(EventName.VERIFY_OTP_ERROR, data: data);
                }
              } else {
                var data = await _doPasswordSignUp(password);
                if (data == null || !data["error"]) {
                } else {
                  EventManager()
                      .publish(EventName.VERIFY_OTP_ERROR, data: data);
                }
              }
            },
          ),
        ],
      );
    }

    if (isFirstInput && (result == null || !result["error"])) {
      Loader().showLoader(false);
      showModalBottomSheet<void>(
        context: context,
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        builder: (context) {
          return Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              Container(
                color: Colors.transparent,
                padding:
                    const EdgeInsets.only(top: 20.0, left: 4.0, right: 4.0),
                child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        topRight: Radius.circular(8.0),
                      ),
                    ),
                    child: child),
              ),
              if (!showOtp)
                Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: Text(
                    "Not getting OTP? Try Password",
                    style:
                        Theme.of(context).primaryTextTheme.headline6.copyWith(
                              fontWeight: FontWeight.w400,
                              color: Colors.black,
                            ),
                  ),
                ),
              Positioned(
                top: 0,
                right: -8,
                child: InkWell(
                  highlightColor: Colors.transparent,
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Image.asset(
                      "images/icons/close.png",
                      height: 32.0,
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                    FocusScope.of(context).unfocus();
                  },
                ),
              ),
            ],
          );
        },
      );
    }
  }

  dynamic _doSignIn(String otp) async {
    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> contextPayload = await getPayloadObject(context);
    Map<String, dynamic> response = await doOTPSignin(
      context: context,
      mobile: _mobileNumber,
      email: emailController.text,
      otp: otp,
      payloadContext: contextPayload,
    );

    if (response["error"] == false) {
      User user = Provider.of<User>(context, listen: false);
      Navigator.of(context).pop();
      processResult(context, response["data"]);
      await onLoginAuthenticate(
          context, response["data"], UserSelectedLoginType.OTP_LOGIN, user);
      if (response["data"]["newUser"]) {
        LobbyAnalytics()
            .onSignUp(context, source: "auth", type: "OTP", user: user);
      } else {
        LobbyAnalytics()
            .onLogin(context, source: "auth", type: "OTP", user: user);
      }
    } else {
      Loader().showLoader(false);
      return response;
    }
    Loader().showLoader(false);
  }

  _doPasswordSignIn(String password) async {
    Loader().showLoader(true, immediate: true);

    Map<String, dynamic> contextPayload = await getPayloadObject(context);
    Map<String, dynamic> response = await doPasswordSignIn(
      context: context,
      mobile: _mobileNumber,
      email: emailController.text,
      password: password,
      payloadContext: contextPayload,
    );

    if (response["error"] == false) {
      User user = Provider.of<User>(context, listen: false);
      processResult(context, response["data"]);
      String loginType = _mobileNumber != null
          ? UserSelectedLoginType.MOBILE_LOGIN
          : UserSelectedLoginType.EMAIL_LOGIN;
      await onLoginAuthenticate(context, response["data"], loginType, user);
      LobbyAnalytics()
          .onLogin(context, source: "auth", type: "Password", user: user);
    } else {
      return response;
    }
    Loader().showLoader(false);
  }

  _doPasswordSignUp(String password) async {
    Loader().showLoader(true, immediate: true);
    Map<String, dynamic> contextPayload = await getPayloadObject(context);
    Map<String, dynamic> response = await doPasswordSignUp(
      context: context,
      mobile: _mobileNumber,
      password: password,
      payloadContext: contextPayload,
    );

    if (response["error"] == false) {
      User user = Provider.of<User>(context, listen: false);
      processResult(context, response["data"]);
      await onLoginAuthenticate(
          context, response["data"], UserSelectedLoginType.MOBILE_LOGIN, user);
      LobbyAnalytics()
          .onSignUp(context, source: "auth", type: "Password", user: user);
    } else {
      return response;
    }
    Loader().showLoader(false);
  }

  doGoogleLogin(BuildContext context,
      {@required Function onError, @required String page}) async {
    if (!await HttpManager(http.Client()).isInternetConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection(
            useWebSocketForDisconnectionDetection: false,
          );
        },
      );
    }

    try {
      Map<String, dynamic> loginResult =
          await MyUserLoginService.getGoogleLoginToken();
      print(loginResult);

      if (loginResult != null && loginResult["status"] == "success") {
        Map<String, dynamic> contextPayload = await getPayloadObject(context);
        Map<String, dynamic> response = await socialNativeLogin(
            context: context,
            token: loginResult["token"],
            loginType: "googlefirebase",
            payloadContext: contextPayload);
        if (response["error"] == false) {
          User user = Provider.of<User>(context, listen: false);
          processResult(context, response["data"]);
          await onLoginAuthenticate(context, response["data"],
              UserSelectedLoginType.GOOGLE_LOGIN, user);
        } else {
          return response;
        }
        Loader().showLoader(false);
      }
    } catch (e) {}
  }

  doFacebookLogin(BuildContext context,
      {@required Function onError, @required String page}) async {
    if (!await HttpManager(http.Client()).isInternetConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection(
            useWebSocketForDisconnectionDetection: false,
          );
        },
      );
    }

    try {
      final Map<dynamic, dynamic> loginResult =
          await MyUserLoginService.getFacebookLoginToken();
      if (loginResult["status"] == "success") {
        Map<String, dynamic> contextPayload = await getPayloadObject(context);
        Map<String, dynamic> response = await socialNativeLogin(
            context: context,
            token: loginResult["token"],
            loginType: "facebook",
            payloadContext: contextPayload);
        if (response["error"] == false) {
          User user = Provider.of<User>(context, listen: false);
          processResult(context, response["data"]);
          await onLoginAuthenticate(context, response["data"],
              UserSelectedLoginType.FACEBOOK_LOGIN, user);
        } else {
          return response;
        }
        Loader().showLoader(false);
      }
    } catch (e) {}
  }

  onAppleLoginButtonPressed(BuildContext buildContext) async {
    if (!await HttpManager(http.Client()).isInternetConnected()) {
      return showDialog(
        barrierColor: Color.fromARGB(175, 0, 0, 0),
        context: context,
        barrierDismissible: true,
        builder: (disconnContext) {
          return Disconnection(
            useWebSocketForDisconnectionDetection: false,
          );
        },
      );
    }
    bool bShowNativeAppleLogin = true;
    if (Platform.isIOS) {
      bShowNativeAppleLogin = true;
      try {
        bShowNativeAppleLogin = await ChannelManager.FLUTTER_TO_NATIVE_CHANNEL
            .invokeMethod('bShowNativeAppleLogin');
      } catch (e) {}
    } else {
      bShowNativeAppleLogin = false;
    }
    if (bShowNativeAppleLogin) {
      final Map<String, dynamic> loginResult =
          await MyUserLoginService.getAppleLoginToken();
      if (loginResult["status"] == "success") {
        Map<String, dynamic> contextPayload = await getPayloadObject(context);
        Map<String, dynamic> response = await appleSocialNativeLogin(
            context: buildContext,
            authorizationCode: loginResult["authorizationCode"].toString(),
            identityToken: loginResult["identityToken"].toString(),
            userIdentifier: loginResult["userIdentifier"].toString(),
            payloadContext: contextPayload);
        if (response["error"] == false) {
          User user = Provider.of<User>(context, listen: false);
          processResult(context, response["data"]);
          await onLoginAuthenticate(context, response["data"],
              UserSelectedLoginType.APPLE_LOGIN, user);
        } else {
          return response;
        }
        Loader().showLoader(false);
      }
    }
  }

  Widget getRectangleAppleLoginWidget(BuildContext buildContext) {
    return Container(
      padding: EdgeInsets.only(left: 56.0, right: 56.0, bottom: 24.0),
      child: Center(
        child: SignInWithAppleButton(
          onPressed: () async {
            onAppleLoginButtonPressed(buildContext);
          },
        ),
      ),
    );
  }

  Widget getAppleCircleLoginWidget(BuildContext buildContext) {
    return Container(
      height: 46.0,
      width: 46.0,
      margin: EdgeInsets.only(top: 2.0),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("images/icons/apple-login.png"),
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: FlatButton(
        child: Container(),
        onPressed: () async {
          Loader().showLoader(true, immediate: true);

          onAppleLoginButtonPressed(buildContext);

          Loader().showLoader(false);
        },
      ),
    );
  }

  Widget getSocialLoginWidget(BuildContext buildContext) {
    InitData initData = Provider.of(context, listen: false);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 56.0),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              if (Platform.isIOS &&
                  initData.experiments.signUpConfig.appleLoginEnabled == true)
                getAppleCircleLoginWidget(buildContext),
              if (initData.experiments.signUpConfig.socialSwitch ==
                      SocialSignInMethods.GOOGLE.value ||
                  initData.experiments.signUpConfig.socialSwitch ==
                      SocialSignInMethods.BOTH.value)
                Container(
                  width: 56,
                  height: 56,
                  child: FlatButton(
                    color: Colors.transparent,
                    padding: EdgeInsets.all(0),
                    child: Image.asset(
                      "images/icons/google-icon.png",
                      height: 56.0,
                      width: 56,
                    ),
                    onPressed: () async {
                      Loader().showLoader(true, immediate: true);

                      await doGoogleLogin(buildContext, page: "login",
                          onError: (String msg) {
                        showMessageOnTop(context, msg);
                      });
                      Loader().showLoader(false);
                    },
                  ),
                ),
              if (initData.experiments.signUpConfig.socialSwitch ==
                      SocialSignInMethods.FACEBOOK.value ||
                  initData.experiments.signUpConfig.socialSwitch ==
                      SocialSignInMethods.BOTH.value)
                Container(
                  width: 56,
                  height: 56,
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    color: Colors.transparent,
                    child: Image.asset(
                      "images/icons/fb-icon.png",
                      height: 56.0,
                      width: 56,
                    ),
                    onPressed: () async {
                      Loader().showLoader(true, immediate: true);

                      await doFacebookLogin(buildContext, page: "login",
                          onError: (String msg) {
                        showMessageOnTop(context, msg);
                      });
                      Loader().showLoader(false);
                    },
                  ),
                ),
            ],
          ),
          // if (Platform.isIOS &&
          //     initData.experiments.signUpConfig.appleLoginEnabled == true)
          //   getRectangleAppleLoginWidget(buildContext),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    InitData initData = Provider.of(context, listen: false);
    AppConfig config = Provider.of<AppConfig>(context, listen: false);

    return LoaderContainer(
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,
        body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            gradient: RadialGradient(
              colors: [
                Color.fromRGBO(160, 3, 14, 1),
                Color.fromRGBO(80, 3, 14, 1),
              ],
              radius: 1,
              stops: [0.3, 0.8],
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: const EdgeInsets.only(top: 64.0, bottom: 16.0),
                      child: Center(
                        child: Image.asset(
                          "images/logo-vertical.png",
                          height: 80,
                        ),
                      ),
                    ),
                    Flexible(
                      child: SingleChildScrollView(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            if (topBanners.length > 0)
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Carousel(
                                  carousel: topBanners,
                                  showPlaceholder: true,
                                  customPlaceholder: Container(
                                    alignment: Alignment.center,
                                    height: (MediaQuery.of(context).size.width /
                                            4.5) -
                                        4.0,
                                  ),
                                ),
                              ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 56.0, vertical: 24.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Form(
                                          key: formKey,
                                          child: Column(
                                            children: <Widget>[
                                              SimpleTextBox(
                                                maxLength: 10,
                                                controller: mobileController,
                                                focusNode: currentFocus,
                                                hintText: "Mobile Number",
                                                prefixIcon: Padding(
                                                  padding:
                                                      const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    width: 24.0,
                                                    padding: EdgeInsets.only(
                                                        bottom: 1.0),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                      border: Border(
                                                        right: BorderSide(
                                                          color: Colors
                                                              .grey.shade300,
                                                        ),
                                                      ),
                                                    ),
                                                    child: Text(
                                                      config.channelId <= 200
                                                          ? "+91"
                                                          : "+1",
                                                      style: Theme.of(context)
                                                          .primaryTextTheme
                                                          .headline6
                                                          .copyWith(
                                                            color: Colors.black,
                                                          ),
                                                    ),
                                                  ),
                                                ),
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .headline5
                                                    .copyWith(
                                                      color: Color.fromRGBO(
                                                          63, 60, 60, 1),
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                errorStyle: TextStyle(
                                                  fontSize: 16.0,
                                                  color: Colors.white,
                                                ),
                                                keyboardType:
                                                    TextInputType.number,
                                                inputFormatters: [
                                                  WhitelistingTextInputFormatter
                                                      .digitsOnly,
                                                ],
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return "Required";
                                                  } else if (value.length !=
                                                          10 ||
                                                      (value.startsWith(
                                                              new RegExp(
                                                                  r'[0-4]'),
                                                              0) &&
                                                          config.channelId <
                                                              200)) {
                                                    return "Invalid";
                                                  }
                                                  return null;
                                                },
                                              ),
                                              if (config.emailSwitch == 2)
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 24.0),
                                                  child: SimpleTextBox(
                                                    controller: emailController,
                                                    hintText:
                                                        "Enter Email Address",
                                                    style: Theme.of(context)
                                                        .primaryTextTheme
                                                        .headline6
                                                        .copyWith(
                                                          color: Color.fromRGBO(
                                                              63, 60, 60, 1),
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                    errorStyle: TextStyle(
                                                      fontSize: 16.0,
                                                      color: Colors.white,
                                                    ),
                                                    keyboardType: TextInputType
                                                        .emailAddress,
                                                    validator: (value) {
                                                      if (value.isEmpty) {
                                                        return "Required";
                                                      } else if (!MyHelperClass
                                                          .isEmailValid(
                                                              value)) {
                                                        return "Invalid";
                                                      }
                                                      return null;
                                                    },
                                                  ),
                                                ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      if (emailMatchError.isNotEmpty)
                                        Text(
                                          emailMatchError,
                                          style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                      Row(
                                        children: <Widget>[
                                          Expanded(
                                            child: Container(
                                              constraints: BoxConstraints(
                                                  maxHeight: 56.0),
                                              margin:
                                                  EdgeInsets.only(top: 24.0),
                                              child: Button(
                                                text: "Continue".toUpperCase(),
                                                onPressed: isContinueEnabled
                                                    ? () {
                                                        if (formKey.currentState
                                                            .validate()) {
                                                          setState(() {
                                                            isContinueEnabled =
                                                                false;
                                                            timer = Timer(
                                                                Duration(
                                                                    seconds: 1),
                                                                () {
                                                              setState(() {
                                                                isContinueEnabled =
                                                                    true;
                                                              });
                                                              Loader()
                                                                  .showLoader(
                                                                      true);
                                                            });
                                                            emailMatchError =
                                                                "";
                                                          });
                                                          formKey.currentState
                                                              .save();
                                                          _requestOTP();
                                                        }
                                                      }
                                                    : null,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            if (initData.experiments.signUpConfig
                                        .socialSwitch !=
                                    SocialSignInMethods.NO_SOCIAL.value ||
                                initData.experiments.signUpConfig
                                        .appleLoginEnabled ==
                                    true)
                              getSocialLoginWidget(context),
                            if (bottomBanners.length > 0)
                              Container(
                                padding: const EdgeInsets.only(
                                  top: 32.0,
                                  bottom: 24.0,
                                ),
                                child: Carousel(
                                  carousel: bottomBanners,
                                  showPlaceholder: true,
                                  customPlaceholder: Container(
                                    alignment: Alignment.center,
                                    height: (MediaQuery.of(context).size.width /
                                            4.5) -
                                        4.0,
                                  ),
                                ),
                              ),
                            SizedBox(
                              height: 72.0,
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(),
                  ],
                ),
              ),
            ],
          ),
        ),
        extendBody: true,
        bottomNavigationBar: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Image.asset(
                  "images/icons/fair-play.png",
                  height: 48,
                ),
                Image.asset(
                  "images/icons/trusted.png",
                  height: 48,
                ),
                Image.asset(
                  config.channelId < 200
                      ? "images/icons/instant-withdrawal.png"
                      : "images/icons/instant-withdrawal-dollar.png",
                  height: 48,
                ),
              ],
            ),
            Container(
              padding: EdgeInsets.only(
                  top: 4.0,
                  bottom: (MediaQuery.of(context).padding.bottom / 2) + 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Center(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  'By registering, you accept you are 18+ and agree to our ',
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .caption
                                  .copyWith(
                                    color: Colors.yellowAccent,
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                            TextSpan(
                              text: 'Terms & Conditions',
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .caption
                                  .copyWith(
                                    color: Colors.orange,
                                    fontWeight: FontWeight.w400,
                                  ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  InitData initData =
                                      Provider.of(context, listen: false);

                                  initData.experiments.staticPages.forEach(
                                    (conf) {
                                      if (conf.name.toLowerCase() ==
                                          "legal".toLowerCase()) {
                                        routeManager.launchStaticPage(context,
                                            url: conf.data.url,
                                            title: conf.data.title);
                                      }
                                    },
                                  );
                                },
                            ),
                            TextSpan(
                              text: ' & ',
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .caption
                                  .copyWith(
                                    color: Colors.yellowAccent,
                                    fontWeight: FontWeight.w400,
                                  ),
                            ),
                            TextSpan(
                              text: 'Privacy Policy',
                              style: Theme.of(context)
                                  .primaryTextTheme
                                  .caption
                                  .copyWith(
                                    color: Colors.orange,
                                    fontWeight: FontWeight.w400,
                                  ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  InitData initData =
                                      Provider.of(context, listen: false);

                                  initData.experiments.staticPages.forEach(
                                    (conf) {
                                      if (conf.name.toLowerCase() ==
                                          "privacypolicy".toLowerCase()) {
                                        routeManager.launchStaticPage(context,
                                            url: conf.data.url,
                                            title: conf.data.title);
                                      }
                                    },
                                  );
                                },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
