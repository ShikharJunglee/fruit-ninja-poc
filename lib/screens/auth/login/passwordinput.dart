import 'package:flutter/services.dart';
import 'package:solitaire_gold/commonwidgets/button.dart';
import 'package:solitaire_gold/commonwidgets/textbox.dart';
import 'package:flutter/material.dart';

class PasswordInput extends StatefulWidget {
  final Function onVerify;
  PasswordInput({@required this.onVerify});

  @override
  _PasswordInputState createState() => _PasswordInputState();
}

class _PasswordInputState extends State<PasswordInput> {
  TextEditingController passwordController = TextEditingController();

  final passwordFormKey = new GlobalKey<FormState>();
  bool shouldSubmit = false;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: SingleChildScrollView(
        child: Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 32.0, vertical: 16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Form(
                  key: passwordFormKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.only(top: 32.0, bottom: 16.0),
                        height: 120.0,
                        child: SimpleTextBox(
                          controller: passwordController,
                          obscureText: true,
                          labelText: "Enter Password",
                          maxLines: 1,
                          keyboardType: TextInputType.text,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Required";
                            } else if (value.length < 6) {
                              return "Min. 6 characters";
                            }
                            return null;
                          },
                        ),
                      ),
                      Button(
                        text: "Submit".toUpperCase(),
                        onPressed: () {
                          if (passwordFormKey.currentState.validate()) {
                            widget.onVerify(passwordController.text);
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
