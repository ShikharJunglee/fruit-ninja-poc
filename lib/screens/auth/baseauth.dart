import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:package_info/package_info.dart';
import 'package:solitaire_gold/analytics/analyticsmanager.dart';
import 'package:solitaire_gold/models/dataloader.dart';
import 'package:solitaire_gold/services/fluttertonativeservice.dart';
import 'package:solitaire_gold/services/mybranchioservice.dart';
import 'package:solitaire_gold/services/myfirebasemanager.dart';
import 'package:solitaire_gold/services/myuserloginservice.dart';
import 'package:solitaire_gold/utils/appurl.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:provider/provider.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/models/profile/user.dart';
import 'package:solitaire_gold/routes/routemanager.dart';

class BaseAuth {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  String userSelectedLoginType = "";
  @protected
  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  @protected
  bool validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  @protected
  bool isMobileNumber(String input) {
    if (input.length == 10 && isNumeric(input)) {
      return true;
    }
    return false;
  }

  @protected
  initServices(BuildContext context) async {}

  @protected
  processResult(BuildContext context, Map<String, dynamic> userData) async {
    var userProvider = Provider.of<User>(context, listen: false);
    userProvider.copyFrom(userData);

    User user = Provider.of(context, listen: false);
    AnalyticsManager().setUser(user);

    await initWebView(context);

    return RouteManager().launchLobbyScreen(context);
  }

  initWebView(BuildContext context) async {
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    try {
      await FlutterWebviewPlugin()
          .launch(
            config.apiUrl + AppUrl.COOKIE_PAGE.value,
            hidden: true,
          )
          .timeout(Duration(seconds: 4));
      await FlutterWebviewPlugin()
          .evalJavascript("document.cookie='" + HttpManager.cookie + "'; ");
    } catch (e) {}
  }

  onLoginResponse(BuildContext context, Map<String, dynamic> response) {
    User user = Provider.of(context, listen: false);
    user.copyFrom(response);
  }

  Future<void> onLoginAuthenticate(BuildContext context,
      Map<String, dynamic> loginData, String loginType, User user) async {
    await MyUserLoginService.onLoginAuthenticate(
        context, loginData, loginType, user);
    AnalyticsManager().setUser(user);
  }

  showLoader(BuildContext context, bool bShow) {
    DataLoader dataLoader = Provider.of(context, listen: false);
    dataLoader.setLoadingStatus(bShow);
  }

  showMessageOnTop(BuildContext context, String msg) {
    Flushbar flushbar;
    flushbar = Flushbar(
      boxShadows: [
        BoxShadow(
          blurRadius: 15.0,
          spreadRadius: 15.0,
          color: Colors.black12,
        )
      ],
      flushbarStyle: FlushbarStyle.FLOATING,
      messageText: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              msg,
              style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: Colors.black,
                  ),
            ),
          ),
          InkWell(
            child: Icon(
              Icons.close,
              color: Colors.black,
            ),
            onTap: () {
              flushbar.dismiss(true);
            },
          ),
        ],
      ),
      backgroundColor: Colors.white,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
    );
    flushbar.show(context);
  }

  Future<Map<String, dynamic>> getPayloadObject(BuildContext context) async {
    Map<String, dynamic> payloadContext = new Map();
    String model = "";
    String serial = "";
    String manufacturer = "";
    String locationLatitude = "";
    String locationLongitude = "";
    String platformType = "";
    bool isPhysicalDevice = true;
    String uid = "";
    AppConfig config = Provider.of<AppConfig>(context, listen: false);
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String packageName = packageInfo.packageName;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    Map<dynamic, dynamic> deviceInfoFromNative =
        await FlutterToNativeService.getDeviceInfoFromNative(config);
    await MyFirebaseMessagingService.getFirebaseToken(config);
    await MyBranchIoService.getInstallReferringLink(config);
    await FlutterToNativeService.getAdvertisingId(config);
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      model = androidInfo.model;
      manufacturer = androidInfo.manufacturer;
      serial = androidInfo.androidId;
      isPhysicalDevice = androidInfo.isPhysicalDevice;
      uid = deviceInfoFromNative["uid"] != null
          ? deviceInfoFromNative["uid"]
          : "";
    }
    if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      model = iosInfo.utsname.machine;
      manufacturer = "apple";
      serial = "";
      platformType = iosInfo.utsname.version;
      isPhysicalDevice = iosInfo.isPhysicalDevice;
      uid = iosInfo.identifierForVendor;
    }
    payloadContext = {
      "model": model,
      "serial": serial,
      "refCode": config.branchReferralCode,
      "deviceId": config.firebaseToken,
      "manufacturer": manufacturer,
      "channel_id": HttpManager.channelId,
      "location_latitude": locationLatitude,
      "location_longitude": locationLongitude,
      "app_version_flutter": packageInfo.version,
      "branchinstallReferringlink": config.branchInviteUrl,
      "googleaddid": config.googleaddid,
      "platformType": platformType,
      "uid": uid,
      "isPhysicalDevice": isPhysicalDevice,
      "packageName": packageName
    };
    try {
      if (config.branchReferringParams != null) {
        payloadContext["branchReferringParams"] =
            config.branchReferringParams.toString();
        Map<dynamic, dynamic> refParmsdata = config.branchReferringParams;
        refParmsdata.forEach((k, v) => payloadContext[k] = v);
      }
    } catch (e) {
      print(e);
    }
    try {
      /*Install Parms*/
      Map<dynamic, dynamic> installparms =
          await MyBranchIoService.getBranchInstallParams(config);
      if (installparms != null && installparms.length > 0) {
        payloadContext["branchInstallParams"] = installparms.toString();
        installparms.forEach((k, v) => payloadContext[k] = v);
      }
    } catch (e) {
      print(e);
    }
    try {
      /*Latest Referring parms */
      Map<dynamic, dynamic> latestReferringParms =
          await MyBranchIoService.getBranchSessionParams(config);
      if (latestReferringParms != null && latestReferringParms.length > 0) {
        payloadContext["branchSessionParms"] = latestReferringParms.toString();
        latestReferringParms.forEach((k, v) => payloadContext[k] = v);
      }
    } catch (e) {
      print(e);
    }

    try {
      payloadContext["network_operator"] =
          deviceInfoFromNative["network_operator"];
      payloadContext["network_type"] = deviceInfoFromNative["network_type"];
    } catch (e) {}

    bool disableBranchIOAttribution = false;
    if (!disableBranchIOAttribution) {
      if (config.branchInviteUrl != null && config.branchInviteUrl.length > 0) {
        try {
          var uri = Uri.parse(config.branchInviteUrl);
          if (uri.queryParameters != null && uri.queryParameters.length > 0) {
            uri.queryParameters.forEach((k, v) {
              if (k != null && v != null) {
                payloadContext[k] = v;
              }
            });
          }
        } catch (e) {
          print(e);
        }
        AnalyticsManager().setContext(payloadContext);
      }
    }
    return payloadContext;
  }
}
