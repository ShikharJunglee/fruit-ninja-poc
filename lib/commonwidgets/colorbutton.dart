import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class ColorButton extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  final Color color;
  final ShapeBorder shape;
  final Color disabledColor;
  final double elevation;
  final Key key;
  final BorderSide borderSide;
  final BorderRadius borderRadius;
  final EdgeInsetsGeometry padding;

  ColorButton({
    @required this.child,
    @required this.onPressed,
    this.color,
    this.disabledColor,
    this.elevation = 3.0,
    this.borderRadius,
    this.shape,
    this.key,
    this.borderSide,
    this.padding = const EdgeInsets.all(0.0),
  });

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: child,
      key: key,
      onPressed: onPressed,
      color: color ?? Theme.of(context).buttonColor,
      disabledColor: Colors.black26,
      shape: shape ??
          RoundedRectangleBorder(
            borderRadius: borderRadius ?? BorderRadius.circular(4.0),
          ),
      padding: padding,
      elevation: elevation,
    );
  }
}
