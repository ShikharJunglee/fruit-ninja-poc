import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:solitaire_gold/commonwidgets/currencytext.dart';

enum ButtonSize { large, medium }
enum ButtonType { primary, secondary }

class Button extends StatelessWidget {
  final String text;
  final TextStyle style;
  final Function onPressed;
  final ButtonSize size;
  final ButtonType type;

  // currency variables
  final bool isCurrencyText;
  final bool showCurrencyDelimiters;
  final double amount;
  final int decimalDigits;
  final String prefix;
  final String suffix;
  final bool isChips;

  Button({
    this.text,
    this.style,
    this.onPressed,
    this.size = ButtonSize.large,
    this.type = ButtonType.primary,

    // currency params
    this.isCurrencyText = false,
    this.showCurrencyDelimiters = true,
    this.amount,
    this.isChips = false,
    this.decimalDigits = 2,
    this.prefix = "",
    this.suffix = "",
  })  : assert(isCurrencyText != null),
        assert(!(isCurrencyText && text != null),
            "Both isCurrencyText and text must not be set"),
        assert(isCurrencyText || text != null,
            "Either text or isCurrencyText must be set"),
        assert(!isCurrencyText || amount != null, "amount is required"),
        assert(isCurrencyText || amount == null,
            "amount is only available when isCurrencyText is true"),
        assert(decimalDigits != null && decimalDigits > 0),
        // assert(
        //     (isCurrencyText && showCurrencyDelimiters != null) ||
        //         (!isCurrencyText && !showCurrencyDelimiters),
        //     "showCurrencyDelimiters is only available when isCurrencyText is true"),
        assert(
            (isCurrencyText && isChips != null) ||
                (!isCurrencyText && !isChips),
            "isChips is only available when isCurrencyText is true"),
        assert(
            (isCurrencyText && prefix != null) ||
                (!isCurrencyText && prefix == ""),
            "prefix is only available when isCurrencyText is true"),
        assert(
            (isCurrencyText && suffix != null) ||
                (!isCurrencyText && suffix == ""),
            "suffix is only available when isCurrencyText is true");

  @override
  Widget build(BuildContext context) {
    TextStyle _style = style;
    String buttonType = type.toString().split(".")[1];
    String buttonSize = size.toString().split(".")[1];
    return CupertinoButton(
      padding: EdgeInsets.all(0),
      onPressed: onPressed,
      borderRadius: BorderRadius.circular(6.0),
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              constraints: BoxConstraints(
                maxWidth: size == ButtonSize.medium ? 320.0 : 540.0,
              ),
              child: onPressed != null
                  ? Image.asset(
                      // "images/splash.png",
                      "images/buttons/button-$buttonType-$buttonSize.png",
                      fit: BoxFit.fill,
                    )
                  : Image.asset(
                      "images/buttons/button-disabled-$buttonSize.png",
                      fit: BoxFit.fill,
                    ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: isCurrencyText
                  ? CurrencyText(
                      amount: amount.toDouble(),
                      isChips: isChips,
                      decimalDigits: 0,
                      prefix: prefix,
                      suffix: suffix,
                      showCurrencyDelimiters: showCurrencyDelimiters,
                      style:
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                color: type == ButtonType.primary
                                    ? Colors.white
                                    : Color.fromRGBO(123, 61, 0, 1),
                                fontWeight: FontWeight.w900,
                                letterSpacing: 1.2,
                              ),
                    )
                  : Text(
                      text,
                      style: _style ??
                          Theme.of(context).primaryTextTheme.headline6.copyWith(
                                color: type == ButtonType.primary
                                    ? Colors.white
                                    : Color.fromRGBO(123, 61, 0, 1),
                                fontWeight: FontWeight.w900,
                                letterSpacing: 1.2,
                              ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
