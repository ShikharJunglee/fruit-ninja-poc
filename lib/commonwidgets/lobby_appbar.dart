import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/leadingbutton.dart';

class LobbyAppBar extends PreferredSize {
  final List<Widget> children;
  final bool automaticallyImplyLeading;
  final MainAxisAlignment mainAxisAlignment;
  final PreferredSizeWidget bottom;
  final Color backgroundColor;
  final Widget leading;

  LobbyAppBar({
    @required this.children,
    this.automaticallyImplyLeading = true,
    this.mainAxisAlignment = MainAxisAlignment.spaceBetween,
    this.bottom,
    this.backgroundColor,
    this.leading,
  }) : super(
            preferredSize: Size.fromHeight(
                kToolbarHeight + (bottom?.preferredSize?.height ?? 0.0)),
            child: null);

  @override
  Widget build(BuildContext context) {
    Widget leadingWidget;
    if (leading != null) {
      leadingWidget = leading;
    } else if (automaticallyImplyLeading) {
      leadingWidget = LeadingButton();
    }
    return Stack(
      children: <Widget>[
        Row(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(121, 3, 20, 1),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16.0),
                    bottomRight: Radius.circular(16.0),
                  ),
                ),
                padding: const EdgeInsets.only(bottom: 2.0),
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color.fromRGBO(154, 16, 33, 1),
                        Color.fromRGBO(74, 11, 22, 1),
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(16.0),
                      bottomRight: Radius.circular(16.0),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.only(bottom: 4.0),
          child: AppBar(
            elevation: 0.0,
            titleSpacing: 0.0,
            backgroundColor: Colors.transparent,
            leading: leadingWidget,
            actions: <Widget>[
              Container(),
            ],
            title: Padding(
              padding:
                  EdgeInsets.only(left: automaticallyImplyLeading ? 0.0 : 8.0),
              child: Row(
                mainAxisAlignment: mainAxisAlignment,
                children: children,
              ),
            ),
            bottom: bottom,
            automaticallyImplyLeading: automaticallyImplyLeading,
          ),
        ),
      ],
    );
  }
}
