import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:solitaire_gold/models/dataloader.dart';

class LoaderContainer extends StatelessWidget {
  final Widget child;
  LoaderContainer({this.child});

  Widget getLoader(BuildContext context) {
    return Container(
      color:  Color.fromARGB(175, 0, 0, 0),
      child: Center(
        child: Image.asset(
          "images/misc/loader.gif",
          height: 36.0,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    DataLoader loader = Provider.of<DataLoader>(context, listen: false);

    return Container(
      child: Stack(
        children: <Widget>[
          child,
          Container(
            child: ChangeNotifierProvider.value(
              value: loader,
              child: Consumer<DataLoader>(
                builder: (context, loader, child) {
                  if (loader.isLoading) {
                    return getLoader(context);
                  }
                  return Container();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
