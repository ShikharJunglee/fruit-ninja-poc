import 'package:flutter/material.dart';

class LeadingButton extends StatelessWidget {
  final bool isSinglePage;
  final Function onPressed;
  LeadingButton({this.isSinglePage = true, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        if (onPressed != null) {
          onPressed();
        } else {
          Navigator.of(context).pop();
        }
      },
    );
  }
}
