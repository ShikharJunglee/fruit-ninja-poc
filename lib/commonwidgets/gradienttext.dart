import 'package:flutter/material.dart';

class GradientText extends StatelessWidget {
  GradientText(
    this.text, {
    this.style,
    this.alignment = TextAlign.left,
    @required this.gradient,
  });

  final String text;
  final Gradient gradient;
  final TextStyle style;
  final TextAlign alignment;

  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle = TextStyle(color: Colors.white);
    TextStyle effectiveTextStyle = defaultStyle;
    if (style != null) effectiveTextStyle = defaultStyle.merge(style);
    return ShaderMask(
      shaderCallback: (bounds) => gradient.createShader(
        Rect.fromLTWH(0, 0, bounds.width, bounds.height),
      ),
      child: Text(
        text,
        style: effectiveTextStyle,
        textAlign: alignment,
      ),
    );
  }
}
