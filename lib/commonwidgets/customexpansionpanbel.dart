import 'package:flutter/material.dart';

class CustomExpansionTile extends StatefulWidget {
  final Key key;
  final bool showTrailingArrow;
  final Widget title;
  final Widget child;
  final Widget subtitle;
  final bool isExpanded;
  final bool canExpand;
  final Color color;
  final Function onExpansionChanged;

  CustomExpansionTile({
    @required this.child,
    @required this.title,
    @required this.isExpanded,
    @required this.onExpansionChanged,
    this.showTrailingArrow = true,
    this.canExpand = true,
    this.key,
    this.color = Colors.white,
    this.subtitle,
  });

  @override
  _CustomExpansionTileState createState() => _CustomExpansionTileState();
}

class _CustomExpansionTileState extends State<CustomExpansionTile>
    with SingleTickerProviderStateMixin {
  Widget getTrailingWidget() {
    return Icon(Icons.arrow_drop_down);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 4.0, vertical: 4.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
            blurRadius: 2.0,
            spreadRadius: 1.0,
            color: Colors.grey.shade300,
          ),
        ],
      ),
      child: Card(
        elevation: 0.0,
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InkWell(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(4.0),
                topRight: Radius.circular(4.0),
              ),
              onTap: widget.onExpansionChanged,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: widget.title,
                    ),
                    if (!widget.canExpand)
                      Container(
                        padding: EdgeInsets.only(top: 8.0),
                        child: widget.subtitle,
                      ),
                  ],
                ),
              ),
            ),
            if (widget.canExpand)
              AnimatedSize(
                  duration: Duration(milliseconds: 250),
                  vsync: this,
                  child: widget.isExpanded
                      ? Container(
                          padding: EdgeInsets.only(
                              left: 16.0, right: 16.0, bottom: 12.0, top: 4.0),
                          child: widget.child,
                        )
                      : Container())
          ],
        ),
      ),
    );
  }
}
