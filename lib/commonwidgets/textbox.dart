import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SimpleTextBox extends StatelessWidget {
  final bool enabled;
  final TextStyle style;
  final String hintText;
  final String labelText;
  final bool obscureText;
  final Widget prefixIcon;
  final Widget suffixIcon;
  final BoxConstraints suffixIconConstraints;
  final Color color;
  final bool isDense;
  final int minLines;
  final int maxLines;
  final int maxLength;
  final int errorMaxLines;
  final Color borderColor;
  final Color disabledBorderColor;
  final double borderWidth;
  final FocusNode focusNode;
  final TextStyle labelStyle;
  final TextStyle hintStyle;
  final TextStyle errorStyle;
  final Color focusedBorderColor;
  final FloatingLabelBehavior floatingLabelBehavior;
  final TextInputType keyboardType;
  final FormFieldSetter<String> onSaved;
  final TextEditingController controller;
  final EdgeInsetsGeometry contentPadding;
  final FormFieldValidator<String> validator;
  final List<TextInputFormatter> inputFormatters;
  final TextCapitalization textCapitalization;
  final Function onChanged;

  SimpleTextBox({
    this.style,
    this.enabled,
    this.onSaved,
    this.hintText,
    this.focusNode,
    this.labelText,
    this.validator,
    this.prefixIcon,
    this.suffixIcon,
    this.suffixIconConstraints,
    this.color,
    this.minLines,
    this.maxLines,
    this.errorMaxLines,
    this.controller,
    this.labelStyle,
    this.hintStyle,
    this.errorStyle,
    this.borderColor,
    this.disabledBorderColor,
    this.borderWidth,
    this.keyboardType,
    this.contentPadding,
    this.inputFormatters,
    this.focusedBorderColor,
    this.maxLength,
    this.obscureText = false,
    this.floatingLabelBehavior = FloatingLabelBehavior.auto,
    this.textCapitalization,
    this.isDense = false,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    final InputBorder enabledBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: borderColor != null ? borderColor : Colors.grey.shade400,
        width: borderWidth != null ? borderWidth : 1,
      ),
    );

    final InputBorder disabledBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: disabledBorderColor != null
            ? disabledBorderColor
            : Colors.grey.shade200,
      ),
    );

    final InputBorder focusedBorder = OutlineInputBorder(
      borderSide: BorderSide(
        width: borderWidth != null ? borderWidth : 1,
        color: focusedBorderColor != null
            ? focusedBorderColor
            : Theme.of(context).primaryColor,
      ),
    );

    final InputBorder errorBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: Theme.of(context).errorColor,
      ),
    );

    return TextFormField(
      style: style == null
          ? Theme.of(context).primaryTextTheme.headline6.copyWith(
                color: Colors.grey.shade700,
                fontWeight: FontWeight.w400,
              )
          : style,
      enabled: enabled,
      onSaved: onSaved,
      focusNode: focusNode,
      validator: validator,
      controller: controller,
      obscureText: obscureText,
      keyboardType: keyboardType,
      inputFormatters: inputFormatters,
      onChanged: onChanged,
      textCapitalization: textCapitalization != null
          ? textCapitalization
          : TextCapitalization.none,
      textAlignVertical: TextAlignVertical.center,
      minLines: minLines,
      maxLines: maxLines,
      maxLength: maxLength,
      autofocus: false,
      decoration: InputDecoration(
        filled: true,
        errorMaxLines: errorMaxLines,
        hintText: hintText,
        alignLabelWithHint: true,
        hintStyle: hintStyle == null ? style : hintStyle,
        labelText: labelText,
        suffixIcon: suffixIcon,
        suffixIconConstraints: suffixIconConstraints,
        prefixIcon: prefixIcon,
        labelStyle: labelStyle,
        errorBorder: errorBorder,
        errorStyle: errorStyle,
        fillColor: color != null ? color : Colors.white,
        focusedBorder: focusedBorder,
        helperMaxLines: 0,
        counterText: "",
        enabledBorder: enabledBorder,
        contentPadding: contentPadding == null
            ? EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0)
            : contentPadding,
        isDense: isDense,
        disabledBorder: disabledBorder,
        floatingLabelBehavior: floatingLabelBehavior,
        focusedErrorBorder: errorBorder,
      ),
    );
  }
}
