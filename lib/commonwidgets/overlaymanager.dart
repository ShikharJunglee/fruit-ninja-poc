import 'package:flutter/material.dart';

class OverlayManager {
  List<OverlayEntry> overlayEntries = [];

  getBackgroundOverlay(bool withBackground, bool closeOnOutsideClick) {
    return OverlayEntry(
      builder: (context) => Positioned(
        left: 0,
        top: 0,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: closeOnOutsideClick
                ? () {
                    closeAll();
                  }
                : null,
            child: Container(
              color: withBackground ? Colors.black26 : Colors.transparent,
            ),
          ),
        ),
      ),
    );
  }

  showOverlay(BuildContext context, Offset offset, Widget child,
      {bool withBackground = true, bool closeOnOutsideClick = true}) {
    OverlayEntry overlayChild = OverlayEntry(
      builder: (context) => Positioned(
        left: offset.dx,
        top: offset.dy,
        child: child,
      ),
    );

    if (withBackground || closeOnOutsideClick) {
      overlayEntries
          .add(getBackgroundOverlay(withBackground, closeOnOutsideClick));
    }

    overlayEntries.add(overlayChild);
    Overlay.of(context).insertAll(overlayEntries);

    return overlayChild;
  }

  closeAll() {
    overlayEntries.forEach((overlay) {
      if (overlay != null) {
        overlay.remove();
      }
    });
    overlayEntries = [];
  }
}
