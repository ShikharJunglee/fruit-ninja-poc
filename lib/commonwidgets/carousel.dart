import 'package:flutter/material.dart';
import 'package:solitaire_gold/analytics/lobbyevents.dart';
import 'package:solitaire_gold/routes/routemanager.dart';
import 'package:solitaire_gold/utils/deeplink_cta.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';

class Carousel extends StatefulWidget {
  final List<dynamic> carousel;
  final double aspectRatio;
  final bool showPlaceholder;
  final Widget customPlaceholder;
  final Function(Map<String, dynamic>) onClick;
  final bool showDots;

  Carousel({
    @required this.carousel,
    this.aspectRatio = 16 / 3.5,
    this.showPlaceholder = false,
    this.onClick,
    this.customPlaceholder,
    this.showDots = false,
  });

  @override
  _CarouselState createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  int _curPageIndex = 0;
  void _onBannerClick(dynamic banner) async {
    LobbyAnalytics().onBannerClicked(context,
        source: "banner", cta: banner["CTA"], zoneId: banner["zone"]);
    switch (banner["CTA"]) {
      case DeeplinkCTA.PROFILE:
        DeeplinkLaunch().onProfile(context);
        break;
      case DeeplinkCTA.KYC:
        DeeplinkLaunch().onKYC(context);
        break;
      case DeeplinkCTA.ACCOUNT:
        DeeplinkLaunch().onAccountSummary(context);
        break;
      case DeeplinkCTA.WITHDRAW:
        DeeplinkLaunch().onWithdraw(context);
        break;
      case DeeplinkCTA.RAF:
        DeeplinkLaunch().onRAF(context);
        break;
      case DeeplinkCTA.HOW_TO_PLAY:
        DeeplinkLaunch().onFTUE(context);
        break;
      case DeeplinkCTA.SCORING:
        DeeplinkLaunch().onScoring(context);
        break;
      case DeeplinkCTA.HELP:
        routeManager.launchHelpCenter(context);
        break;
      case DeeplinkCTA.LINK:
        routeManager.launchStaticPage(context,
            url: banner["link"], title: banner["pageTitle"]);
        break;
      default:
        if (widget.onClick != null) {
          widget.onClick(banner);
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.carousel == null || widget.carousel.length == 0
        ? widget.showPlaceholder && widget.customPlaceholder == null
            ? Container(
                alignment: Alignment.center,
                height: (MediaQuery.of(context).size.width / 4.5) - 4.0,
                child: Image.asset(
                  "images/lobby/solitaire_gold.png",
                  width: MediaQuery.of(context).size.width * 0.6,
                ),
              )
            : widget.showPlaceholder ? widget.customPlaceholder : Container()
        : Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(6.0),
                child: CarouselSlider(
                  enlargeCenterPage: false,
                  aspectRatio: widget.aspectRatio,
                  viewportFraction: 1.0,
                  enableInfiniteScroll: widget.carousel.length > 1,
                  autoPlayInterval: Duration(seconds: 10),
                  items: widget.carousel.map<Widget>(
                    (dynamic banner) {
                      return FlatButton(
                        padding: EdgeInsets.symmetric(horizontal: 1.0),
                        onPressed: () {
                          _onBannerClick(banner);
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: CachedNetworkImage(
                            imageUrl: banner["banner"],
                            placeholder: (context, url) => AspectRatio(
                              aspectRatio: widget.aspectRatio,
                              child: Center(
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(),
                                ),
                              ),
                            ),
                            fit: BoxFit.fitWidth,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                      );
                    },
                  ).toList(),
                  onPageChanged: (int index) {
                    setState(() {
                      _curPageIndex = index;
                    });
                  },
                  autoPlay: widget.carousel.length < 2 ? false : true,
                  reverse: false,
                ),
              ),
              if (widget.showDots && widget.carousel.length > 1)
                Padding(
                  padding: EdgeInsets.only(
                    top: 8.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: widget.carousel
                        .asMap()
                        .map((index, f) => MapEntry(
                            index,
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 4.0),
                              child: Container(
                                width: 8.0,
                                height: 8.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: index == _curPageIndex
                                      ? Colors.white
                                      : Colors.black45,
                                ),
                              ),
                            )))
                        .values
                        .toList(),
                  ),
                ),
            ],
          );
  }
}
