import 'package:flutter/material.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';

class CurrencyText extends StatelessWidget {
  final bool isChips;
  final bool showCurrencyDelimiters;
  final double amount;
  final TextStyle style;
  final int decimalDigits;
  final String prefix;
  final String suffix;
  CurrencyText({
    @required this.amount,
    this.style,
    this.isChips = false,
    this.showCurrencyDelimiters = true,
    this.decimalDigits = 2,
    this.prefix = "",
    this.suffix = "",
  });

  @override
  Widget build(BuildContext context) {
    String amountToShow = showCurrencyDelimiters
        ? CurrencyFormat.format(amount, decimalDigits: decimalDigits)
        : CurrencyFormat.format(amount, decimalDigits: decimalDigits)
            .replaceAll(",", "");

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        isChips
            ? Image.asset(
                "images/chips.png",
                width: 16.0,
                height: 12.0,
                fit: BoxFit.contain,
              )
            : Container(),
        Text(
          prefix + amountToShow + suffix,
          style: style,
        )
      ],
    );
  }
}
