import 'dart:math';
import 'package:flutter/material.dart';
import 'package:solitaire_gold/commonwidgets/overlaymanager.dart';

class Dropdown extends StatefulWidget {
  final String label;
  final String valueKey;
  final List<dynamic> dropDownItems;
  final Function onSelectCallback;

  Dropdown({
    this.label,
    this.valueKey,
    @required this.dropDownItems,
    this.onSelectCallback,
  });

  @override
  _DropdownState createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> with RouteAware {
  GlobalKey _key = GlobalKey();
  OverlayManager _overlayManager = OverlayManager();
  Map<String, dynamic> selectedItem;
  double singleItemHeight = 55.0;
  double maxHeight = 400.0;
  double height = 0;

  @override
  void didUpdateWidget(Dropdown oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.dropDownItems != widget.dropDownItems) {
      selectedItem = null;
    }
  }

  showDropdownOptions() {
    FocusScope.of(context).unfocus();
    maxHeight = 400.0;
    height = widget.dropDownItems.length * singleItemHeight;
    if (height > maxHeight) {
      height = maxHeight;
    }
    double width = MediaQuery.of(context).size.width - 140;

    Offset offset = Offset(MediaQuery.of(context).size.width / 2 - width / 2,
        MediaQuery.of(context).size.height / 2 - height / 2);

    _overlayManager.showOverlay(
      context,
      offset,
      Padding(
        padding: const EdgeInsets.only(right: 20.0),
        child: Container(
          width: width,
          // height: height,
          constraints: BoxConstraints(maxHeight: maxHeight),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(6.0),
          ),
          clipBehavior: Clip.hardEdge,
          child: DropdownList(
            singleItemHeight: singleItemHeight,
            height: height,
            items: widget.dropDownItems,
            valueKey: widget.valueKey,
            onSelect: (Map<String, dynamic> item) {
              _overlayManager.closeAll();
              setState(() {
                selectedItem = item;
              });
              if (widget.onSelectCallback != null)
                widget.onSelectCallback(selectedItem);
            },
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        _overlayManager.closeAll();
        return Future.value(true);
      },
      child: InkWell(
        onTap: () {
          showDropdownOptions();
        },
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: Text(
                  selectedItem != null
                      ? selectedItem[widget.valueKey]
                      : widget.label,
                  style: Theme.of(context).primaryTextTheme.headline6.copyWith(
                        color: Colors.grey.shade700,
                        fontWeight: FontWeight.w400,
                      ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Transform.rotate(
                angle: pi / 2,
                child: Icon(
                  Icons.chevron_right,
                  color: Colors.grey.shade500,
                  key: _key,
                ),
              ),
            ],
          ),
          padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(3.0),
            border: Border.all(
              width: 1.0,
              color: Colors.grey.shade300,
            ),
            color: Colors.white,
            // boxShadow: [
            //   BoxShadow(
            //     blurRadius: 1.0,
            //     spreadRadius: 1.0,
            //     color: Colors.grey.shade200,
            //   ),
            // ],
          ),
        ),
      ),
    );
  }
}

class DropdownList extends StatefulWidget {
  final List<dynamic> items;
  final String valueKey;
  final Function onSelect;
  final double singleItemHeight;
  final double height;
  DropdownList(
      {this.items,
      this.valueKey,
      this.onSelect,
      this.singleItemHeight,
      this.height});
  @override
  _DropdownListState createState() => _DropdownListState();
}

class _DropdownListState extends State<DropdownList> {
  ScrollController scrollController = ScrollController();
  double offset = 0;

  List<Widget> getList() {
    int i = 0;
    List<Widget> itemWidgets = [];
    widget.items.forEach((item) {
      itemWidgets.add(
        InkWell(
          onTap: () {
            widget.onSelect(item);
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 20.0),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey.shade300, width: 1.0),
                  top: i == 0
                      ? BorderSide(color: Colors.grey.shade300, width: 1.0)
                      : BorderSide(color: Colors.grey.shade300, width: 0.0),
                ),
              ),
              child: Text(
                item[widget.valueKey],
              ),
            ),
          ),
        ),
      );
      i++;
    });
    return itemWidgets;
  }

  void scrollDropdownDown() {
    scrollController.animateTo(scrollController.offset + 55,
        curve: Curves.linear, duration: Duration(milliseconds: 300));
    offset += 55;
  }

  void scrollDropdownUp() {
    scrollController.animateTo(scrollController.offset - 55,
        curve: Curves.linear, duration: Duration(milliseconds: 300));
    offset -= 55;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          FlatButton(
            onPressed: offset > 0
                ? () {
                    scrollDropdownUp();
                    setState(() {});
                  }
                : null,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Transform.rotate(
                    angle: pi * 1.5,
                    child: Container(
                      child: Icon(Icons.chevron_right),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: ListView(
              controller: scrollController,
              children: getList(),
              shrinkWrap: true,
              padding: EdgeInsets.zero,
            ),
          ),
          FlatButton(
            onPressed: offset <
                    ((widget.items.length * widget.singleItemHeight) -
                        widget.height)
                ? () {
                    scrollDropdownDown();
                    setState(() {});
                  }
                : null,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Transform.rotate(
                    angle: pi * 0.5,
                    child: Container(
                      child: Icon(Icons.chevron_right),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
