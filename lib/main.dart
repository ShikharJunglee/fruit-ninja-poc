import 'dart:async';
import 'dart:io';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart' as intl;
import 'package:provider/provider.dart';
import 'package:solitaire_gold/environments/environments.dart';
import 'package:solitaire_gold/routes/approutes.dart';
import 'package:solitaire_gold/screens/splashscreen/splashscreen.dart';
import 'package:solitaire_gold/utils/appconfig.dart';
import 'package:solitaire_gold/utils/currencyformat.dart';
import 'package:solitaire_gold/utils/httpmanager.dart';
import 'package:solitaire_gold/utils/loader.dart';
import 'package:solitaire_gold/utils/multitouch_disabler.dart';
import 'package:solitaire_gold/utils/providers.dart';

// Release commands:
// flutter build apk --release --flavor usastage
// flutter build apk --release --flavor usaprod
// flutter build apk --release --flavor indiastage
// flutter build apk --release --flavor indiaprod
// Development commands:
// flutter run --flavor indiaprod
// flutter run --flavor usaprod
// flutter run --flavor usastage
// flutter run --flavor indiastage
//Upload DSYM File:
// /Users/theoffice/Projects/grs_client/ios/Pods/FirebaseCrashlytics/upload-symbols -gsp /Users/theoffice/Projects/grs_client/ios/GoogleService-Info.plist -p ios PATH TO DSYM

applySystemSettings() {
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
}

AppConfig getConfig(Environments _environment) {
  int channelId = 101;
  var appconfig = AppConfig();
  appconfig.setAppName("Solitaire Gold");

  switch (_environment) {
    case Environments.LOCAL_ANDROID:
      channelId = 201;
      appconfig.setApiUrl("http://192.168.1.79:4005/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://goldrush.howzat.com/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "en_US",
        name: "USD",
        decimalDigits: 0,
      );
      break;
    case Environments.STAGE_ANDROID:
      channelId = 101;
      appconfig.setApiUrl("https://stg-gr.howzat.com/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://goldrush.howzat.com/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "hi_IN",
        name: "INR",
        decimalDigits: 0,
      );
      break;
    case Environments.STAGE_IOS:
      channelId = 102;
      appconfig.setApiUrl("https://stg-gr.howzat.com/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://goldrush.howzat.com/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "hi_IN",
        name: "INR",
        decimalDigits: 0,
      );
      break;
    case Environments.STAGE_ANDROID_US:
      channelId = 201;
      appconfig.setApiUrl("https://stg-gr.howzat.com/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://goldrush.howzat.com/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "en_US",
        name: "USD",
        decimalDigits: 0,
      );
      break;
    case Environments.STAGE_IOS_US:
      channelId = 202;
      appconfig.setApiUrl("https://stg-gr.howzat.com/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://goldrush.howzat.com/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "en_US",
        name: "USD",
        decimalDigits: 0,
      );
      break;
    case Environments.PROD_ANDROID:
      channelId = 101;
      appconfig.setApiUrl("https://beta-lobby.solitairegold.in/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://ws-beta-lobby.solitairegold.in/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "hi_IN",
        name: "INR",
        decimalDigits: 0,
      );
      break;
    case Environments.PROD_IOS:
      channelId = 102;
      appconfig.setApiUrl("https://lobby.solitairegold.in/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://connection.solitairegold.in/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "hi_IN",
        name: "INR",
        decimalDigits: 0,
      );
      break;
    case Environments.PROD_ANDROID_US:
      channelId = 201;
      appconfig.setApiUrl("https://lobby.solitairegold.com/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://connection.solitairegold.in/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "en_US",
        name: "USD",
        decimalDigits: 0,
      );
      break;
    case Environments.PROD_IOS_US:
      channelId = 202;
      appconfig.setApiUrl("https://lobby.solitairegold.com/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://connection.solitairegold.in/");
      appconfig.setDisableBranchIOAttributionValue(false);
      appconfig.setDisableSocialLoginValue(false);
      appconfig.setAttributionNumber(0);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "en_US",
        name: "USD",
        decimalDigits: 0,
      );
      break;
    case Environments.MI_INDIA:
      channelId = 101;
      appconfig.setApiUrl("https://lobby.solitairegold.in/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://connection.solitairegold.in/");
      appconfig.setDisableBranchIOAttributionValue(true);
      appconfig.setDisableSocialLoginValue(true);
      appconfig.setAttributionNumber(4);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "hi_IN",
        name: "INR",
        decimalDigits: 0,
      );
      break;
    case Environments.OPPO_INDIA:
      channelId = 101;
      appconfig.setApiUrl("https://lobby.solitairegold.in/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://connection.solitairegold.in/");
      appconfig.setDisableBranchIOAttributionValue(true);
      appconfig.setDisableSocialLoginValue(true);
      appconfig.setAttributionNumber(1);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "hi_IN",
        name: "INR",
        decimalDigits: 0,
      );
      break;
    case Environments.VIVO_INDIA:
      channelId = 101;
      appconfig.setApiUrl("https://lobby.solitairegold.in/api/");
      appconfig.setReplyURL(
          "https://jnkjwnrfnwnefnwkefkwebkhbwh3-gamereplay-stage.solitairegold.in/");
      appconfig.setGameUrl("https://connection.solitairegold.in/");
      appconfig.setDisableBranchIOAttributionValue(true);
      appconfig.setDisableSocialLoginValue(true);
      appconfig.setAttributionNumber(3);
      CurrencyFormat.formatCurrency = intl.NumberFormat.simpleCurrency(
        locale: "hi_IN",
        name: "INR",
        decimalDigits: 0,
      );
      break;
    default:
  }

  appconfig.setChannelId(channelId);
  HttpManager.channelId = channelId.toString();

  return appconfig;
}

void main() async {
  Environments _environment = Environments.STAGE_ANDROID;

  List<Environments> _stageEnvironments = [
    Environments.STAGE_ANDROID,
    Environments.STAGE_ANDROID_US,
    Environments.STAGE_IOS,
    Environments.STAGE_IOS_US
  ];

  WidgetsFlutterBinding.ensureInitialized();
  final GlobalKey<NavigatorState> navigatorkey = GlobalKey();

  Loader().setGlobalContextKey(navigatorkey);

  applySystemSettings();

  AppRoutes routes = AppRoutes();
  AppConfig appConfig = getConfig(_environment);
  Providers providers = Providers(appConfig);

  Widget _materialApp = MultitouchDisabler(
    child: MaterialApp(
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context)
              .copyWith(textScaleFactor: Platform.isAndroid ? 0.8 : 0.70),
        );
      },
      navigatorKey: navigatorkey,
      home: SplashScreen(),
      theme: ThemeData(
        primaryColor: Color.fromRGBO(101, 4, 3, 1),
        primaryColorLight: Color.fromRGBO(188, 69, 53, 1),
        primaryColorDark: Color.fromRGBO(84, 0, 0, 1),
        accentColor: Color.fromRGBO(211, 37, 24, 1),
        buttonColor: Color.fromRGBO(37, 133, 41, 1),
      ),
      onGenerateRoute: (settings) {
        return routes.getRouteFor(settings);
      },
    ),
  );

  var configuredApp = MultiProvider(
    providers: providers.getAll(),
    child: _stageEnvironments.indexOf(_environment) == -1
        ? _materialApp
        : Banner(
            location: BannerLocation.topStart,
            message: "Channel: " + appConfig.channelId.toString(),
            textDirection: TextDirection.ltr,
            layoutDirection: TextDirection.ltr,
            textStyle: TextStyle(fontSize: 10.0),
            child: _materialApp,
          ),
  );
  runZonedGuarded(() {
    runApp(configuredApp);
  }, (error, stackTrace) {
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}
