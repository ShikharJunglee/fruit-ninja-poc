
import Foundation;
import FirebaseInstanceID;
import FirebaseMessaging;
import Firebase;
import FirebaseCore;


class FCMService: NSObject, UNUserNotificationCenterDelegate,MessagingDelegate{
    
    
    private var PF_FCM_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    let gcmMessageIDKey = "gcm.message_id";
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        super.init();
        flutterViewcontroller=controller;
        PF_FCM_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.FCM_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService();
    }
    
    
    public func registerForPushNotifications(_ application: UIApplication,appDelegate: AppDelegate) {
        FirebaseApp.configure();
        Messaging.messaging().delegate = self;
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications();
    }
    
    
    private func initFlutterChannelsService(){
        PF_FCM_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "_getFirebaseToken"){
                var  channelResult:String="";
                if(MyGlobalVariables.firebaseToken.count>4){
                    channelResult=MyGlobalVariables.firebaseToken;
                }
                else{
                    if let fcmToken = self?.getFireBaseToken(){
                        channelResult=fcmToken;
                    }
                }
                result(channelResult)
            }
            else if(call.method == "_subscribeToFirebaseTopic"){
                let  channelResult:String = "";
                if let topic =  call.arguments as? String{
                    self?.subscribeToFirebaseTopic(topicName:topic);
                }
                result(channelResult)
            }
            else{
                result(FlutterMethodNotImplemented)
            }
        });
        
    }
    
    
    private func  getFireBaseToken()-> String{
        var token:String = "";
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print(error);
            } else if let result = result {
                token=result.token;
            }
        }
        return token;
    }
    
    public func subscribeToFirebaseTopic(topicName:String) {
        Messaging.messaging().subscribe(toTopic: topicName) { error in
        }
    }
    
    public func updateTheFCMToken(){
        
    }
    
    public func didRegisterForRemoteNotificationsWithDeviceTokenCalled(){
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                MyGlobalVariables.firebaseToken=result.token;
            }
        })
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        /*This is MessagingDelegate method */
        let dataDict:[String: String] = ["token": fcmToken];
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict);
        Messaging.messaging().subscribe(toTopic: "news") { error in
        }
        Messaging.messaging().subscribe(toTopic: "ios_news") { error in
        }
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        /*If APP is Forground It is called*/
        /*If App is in Background this is fired when notification arrives*/
        let userInfo = notification.request.content.userInfo;
        if let source=userInfo["source"] as? String{
            if(source == MyConstants.FCM_GAME_MSG_SOURCE){
                sendFCMDataReceivedEventAlert(userInfo:userInfo);
            }
        }
        let isForeground = UIApplication.shared.applicationState == .active;
        if(!isForeground){
            completionHandler([[.alert, .sound]]);
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        /*This is fired  only when notification is clicked by user*/
        /*When App is killed only this methos is called*/
        let userInfo = response.notification.request.content.userInfo
        Messaging.messaging().appDidReceiveMessage(userInfo);
        if let source=userInfo["source"] as? String{
            if(source == MyConstants.FCM_GAME_MSG_SOURCE){
                DeepLinkingManager.setDeepLinkingDataFromFCMData(dataLabel:userInfo);
            }
        }
        completionHandler();
    }
    
    public  func sendFCMDataReceivedEventAlert(userInfo:[AnyHashable : Any]){
        var fcmEventData:[String: Any]=[:];
        fcmEventData["eventName"]=StreamEventLables.FCM_MESSAGE_RECIEVED;
        fcmEventData["data"]=userInfo;
        if( MyGlobalVariables.nativeToFlutterAlertsEventSink != nil){
            MyGlobalVariables.nativeToFlutterAlertsEventSink(fcmEventData);
        }
    }
    
}






