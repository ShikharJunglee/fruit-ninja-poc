
import Foundation;
import Firebase;
import CoreLocation;

class MyLocationManager: NSObject, CLLocationManagerDelegate {
    let locationManager = CLLocationManager();
    private var LOCATION_MANAGER_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    private var flutterAppDelegate: AppDelegate!;
    private var location_permission_result: FlutterResult!;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController) {
        super.init();
        locationManager.delegate = self;
        flutterAppDelegate=appDelegate;
        flutterViewcontroller=controller;
        LOCATION_MANAGER_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.LOCATION_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService();
    }
    
    private func initFlutterChannelsService(){
        LOCATION_MANAGER_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "getLocationLongLat"){
                self?.location_permission_result=result;
                self?.getLocationLongLat();
            }
        });
    }
    
    
    func getLocationLongLat(){
        locationManager.delegate = self;
        var currentLocation: CLLocation!
        var response = [String : String]()
        response["bAccessGiven"] = "false";
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            guard let latitude: CLLocationDegrees = (locationManager.location?.coordinate.latitude) else {
                response["latitudeFailed"] = "true";
                location_permission_result(response);
                return
            }
            guard let longitude: CLLocationDegrees = (locationManager.location?.coordinate.longitude) else {
                response["longitudeFailed"] = "true";
                location_permission_result(response);
                return
            }
            let location = CLLocation(latitude: latitude, longitude: longitude)
            
            response["bAccessGiven"] = "true";
            let lo: Double = location.coordinate.latitude;
            let la: Double = location.coordinate.longitude;
            response["longitude"] = String(format: "%f", la);
            response["latitude"] = String(format: "%f", lo);
            location_permission_result(response);
        }else{
            response["bAccessGiven"] = "false";
            location_permission_result(response);
        }
    }
    
    func enableTheLocationService(_ application: UIApplication,appDelegate: AppDelegate) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            locationManager.startUpdatingLocation()
        } else if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        } else if CLLocationManager.authorizationStatus() == .denied {
            print("User denied location permissions.")
        }
    }
}
