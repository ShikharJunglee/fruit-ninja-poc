import UIKit;
import Flutter;
import UserNotifications;
import Branch;
import WebEngage;
import Firebase;
import CoreLocation;
import FirebaseInstanceID;
import FBSDKCoreKit;
import GoogleSignIn 


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate{
    
    private var controller : FlutterViewController!;
    private var  branchIoService:BranchIoService!;
    private var  fCMService:FCMService!;
    private var  webEngageService:WebEngageService!;
    private var  utilsService:UtilsService!;
    private var  browserLaunchService:BrowserLaunch!;
    private var  myLoginService:MyLoginService!;
    private var socialShareService:SocialShareService!;
    private var   razorpayPaymentService:RazorpayPaymentService!;
    private var locationManager:MyLocationManager!;
    private var flutterStreamHandlerService:FlutterStreamHandlerService!;
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        controller = window?.rootViewController as? FlutterViewController;
        initTheHighPriorityServices(application,didFinishLaunchingWithOptions: launchOptions);
        initFlutterChannelsAndEvents(application,didFinishLaunchingWithOptions: launchOptions);
        initOtherServices(application,didFinishLaunchingWithOptions: launchOptions);
        InitArgs(CommandLine.argc, CommandLine.unsafeArgv);
        var flutter_native_splash = 1
        UIApplication.shared.isStatusBarHidden = false
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    
    
    private func initTheHighPriorityServices(_ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        DeepLinkingManager.setDeepLinkingDataToDefault();
        branchIoService=BranchIoService(appDelegate:self,controller:controller);
        branchIoService.initBranchPlugin(didFinishLaunchingWithOptions:launchOptions);
    }
    private func initOtherServices(_ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        locationManager = MyLocationManager(appDelegate:self,controller:controller);
        initFBLaunchOptions(application,didFinishLaunchingWithOptions: launchOptions);
    }
    
    private func initFBLaunchOptions(_ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions);
    }
    
    
    private func initFlutterChannelsAndEvents(_ application: UIApplication,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        /* High Priority*/
        fCMService=FCMService(appDelegate:self,controller:controller);
        fCMService.registerForPushNotifications(application,appDelegate:self);
        webEngageService=WebEngageService(appDelegate:self,controller:controller);
        webEngageService.initWebengage(application,appDelegate:self,didFinishLaunchingWithOptions:launchOptions);
        utilsService=UtilsService(appDelegate:self,controller:controller);
        browserLaunchService=BrowserLaunch(appDelegate:self,controller:controller);
        socialShareService=SocialShareService(appDelegate:self,controller:controller!);
        myLoginService=MyLoginService(appDelegate:self,controller:controller!);
        flutterStreamHandlerService=FlutterStreamHandlerService(appDelegate:self,controller:controller!);
        razorpayPaymentService=RazorpayPaymentService(appDelegate:self,controller:controller);
        setGoogleAddId();
    }
    
    func setGoogleAddId(){
        MyGlobalVariables.googleaddid=MyDeviceInfo.identifierForAdvertising();
    }
    
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
    }
    
    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                              fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        /*If APP is Forground It is called*/
        /*If APP is Backgroung also  It is called*/
        /*  content_available: true key is added then this methos will be called*/
        completionHandler(UIBackgroundFetchResult.newData);
    }
    
    override func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        InstanceID.instanceID().instanceID(handler: { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)");
                MyGlobalVariables.firebaseToken=result.token;
            }
        })
        
    }
    

    override func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
      -> Bool {
        if #available(iOS 9.0, *) {
                  return GIDSignIn.sharedInstance().handle(url)
        }
        return Branch.getInstance().application(application, open: url, options: options);
        
    }
    
    override func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        /* handler for Universal Links*/
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            let url = userActivity.webpageURL!
            print(url.absoluteString);
        }
        return Branch.getInstance().continue(userActivity);
    }
    
    public func onUserInfoRefreshed(data:[String:Any])-> String{
        if(webEngageService != nil){
            webEngageService.onUserInfoRefreshed(data:data);
        }
        return "";
    }
    
}

struct MyGlobalVariables {
    static  var bBranchLodead:Bool = false;
    static  var firebaseToken:String="";
    static  var installReferring_link:String="";
    static  var googleaddid:String="";
    static  var deepLinkingDataObject:[String: Any]=[:];
    static  var actionEIdWhichHasDL:String = "";
    static  var deepLinkingDataModelFromWEbengageInapp:DeepLinkingDataModel=DeepLinkingDataModel();
    public  static var nativeToFlutterAlertsEventSink:FlutterEventSink!;
}


extension AppDelegate: WEGAppDelegate {
    func wegHandleDeeplink(_ deeplink: String!, userData data: [AnyHashable: Any]!) {
        if let urlFromWE = deeplink{
            DeepLinkingManager.handleDeepLinkingURLFromWebEngage(webengageURL: urlFromWE);
        }
    }
    func didReceiveAnonymousID(_ anonymousID: String!, for reason: WEGReason) {
    }
}

extension AppDelegate: WEGInAppNotificationProtocol {
    func notificationPrepared(_ inAppNotificationData: [String : Any]!, shouldStop stopRendering: UnsafeMutablePointer<ObjCBool>!) -> [AnyHashable : Any]!{
        return WebEngageService.getModifiedinAppNotificationData(inappdata:inAppNotificationData);
    }
    
    func notificationShown(_ inAppNotificationData: [String : Any]!){
    }
    
    func notification(_ inAppNotificationData: [String : Any]!, clickedWithAction actionId: String!){
        if let actionId=actionId{
            if(actionId == MyGlobalVariables.actionEIdWhichHasDL){
                let deepLinkingDataObject:[String: Any]=DeepLinkingHelper.getDeepLinkingDictFromStruct(deepLinkStructdata:MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp);
                MyGlobalVariables.deepLinkingDataObject=deepLinkingDataObject;
                if( MyGlobalVariables.nativeToFlutterAlertsEventSink != nil){
                    var eventDataToSend:[String: Any]=[:];
                    eventDataToSend["eventName"]="deeplinking_data_recieved";
                    eventDataToSend["data"]=MyGlobalVariables.deepLinkingDataObject;
                    MyGlobalVariables.nativeToFlutterAlertsEventSink(eventDataToSend);
                    DeepLinkingManager.setDeepLinkingDataToDefault();
                }
            }
        }
    }
    
    func notificationDismissed(_ inAppNotificationData: [String : Any]!){
    }
}





