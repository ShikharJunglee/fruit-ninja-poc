class DeepLinkingDataModel {
    var activateDeepLinkingNavigation = false;
    var dl_page_route: String="";
    var dl_ac_promocode: String="";
    var dl_ac_promoamount: String="";
    var dl_sp_pageLocation: String="";
    var dl_sp_pageTitle: String="";
    var dl_unique_id: String="";
    var sg_body: String="";
    var sg_matchId: String="";
    var sg_title: String = "";
    var sg_type: String="";
}

class DeepLinkingHelper{
    static func getDeepLinkingDictFromStruct(deepLinkStructdata:DeepLinkingDataModel?)->[String: Any]{
        var deepLinkingDataObject:[String: Any]=[:];
        if let deepLinkStructdata=deepLinkStructdata{
            deepLinkingDataObject["activateDeepLinkingNavigation"]=deepLinkStructdata.activateDeepLinkingNavigation;
            deepLinkingDataObject["dl_page_route"]=deepLinkStructdata.dl_page_route;
            deepLinkingDataObject["dl_ac_promocode"]=deepLinkStructdata.dl_ac_promocode;
            deepLinkingDataObject["dl_ac_promoamount"]=deepLinkStructdata.dl_ac_promoamount;
            deepLinkingDataObject["dl_sp_pageLocation"]=deepLinkStructdata.dl_sp_pageLocation;
            deepLinkingDataObject["dl_sp_pageTitle"]=deepLinkStructdata.dl_sp_pageTitle;
            deepLinkingDataObject["dl_unique_id"]=deepLinkStructdata.dl_unique_id;
            deepLinkingDataObject["sg_body"]=deepLinkStructdata.sg_body;
            deepLinkingDataObject["sg_title"]=deepLinkStructdata.sg_title;
            deepLinkingDataObject["sg_matchId"]=deepLinkStructdata.sg_matchId;
            deepLinkingDataObject["sg_type"]=deepLinkStructdata.sg_type;
        }
        return deepLinkingDataObject;
    }
}
