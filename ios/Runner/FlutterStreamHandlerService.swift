

import Foundation

class FlutterStreamHandlerService : NSObject,FlutterStreamHandler {
    
    private var eventSink: FlutterEventSink?;
    private var EVENTALERTS_STREAM_CHANNEL:FlutterEventChannel!;
    private var AUTHALERT_STREAM_CHANNEL:FlutterEventChannel!;
    private var AUTHEVENTS_STREAM_CHANNEL:FlutterEventChannel!;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController){
        super.init();
        EVENTALERTS_STREAM_CHANNEL=FlutterEventChannel(name: MyMethodConstants.NATIVE_TO_FLUTTER_EVENT_STREAM_CHANNEL, binaryMessenger: controller.binaryMessenger);
        EVENTALERTS_STREAM_CHANNEL.setStreamHandler(EventAlertStreamClass());
        AUTHEVENTS_STREAM_CHANNEL=FlutterEventChannel(name: MyMethodConstants.AUTH_EVENTS_STREAM_CHANNEL, binaryMessenger: controller.binaryMessenger);
        AUTHEVENTS_STREAM_CHANNEL.setStreamHandler(AuthEventsStreamClass());
    }
    
    
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        eventSink=events;
        return nil
    }
    
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        return nil
    }
    
}

class  EventAlertStreamClass: NSObject, FlutterStreamHandler{
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        MyGlobalVariables.nativeToFlutterAlertsEventSink = events
        return nil
    }
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        MyGlobalVariables.nativeToFlutterAlertsEventSink = nil
        return nil
    }
}

class  AuthEventsStreamClass: NSObject, FlutterStreamHandler{
    func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
        MyGlobalVariables.nativeToFlutterAlertsEventSink = events
        return nil
    }
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        MyGlobalVariables.nativeToFlutterAlertsEventSink = nil
        return nil
    }
}

