

import Foundation


struct MyMethodConstants{
    static let  FCM_SERVICE_MNAME : String="com.silvercenturion.fcm";
    static let  MY_LOGIN_MCHANNEL : String="com.silvercenturion.loginservice";
    static let  LOCATION_SERVICE_MNAME : String="com.silvercenturion.locationmanager";
    static let  BROWSER_LAUNCH_SERVICE_MNAME : String="com.silvercenturion.browser";
    static let  RAZORPAY_SERVICE_MNAME : String="com.silvercenturion.razorpay";
    static let  BRANCHIO_SERVICE_MNAME : String="com.silvercenturion.branch";
    static let  WEBENGAGE_SERVICE_MNAME : String="com.silvercenturion.webengage";
    static let  SOCIALSHARE_SERVICE_MNAME : String="com.silvercenturion.socialshare";
    static let  UTILS_SERVICE_MNAME : String="com.silvercenturion.utils";
    static let  NATIVE_TO_FLUTTER_EVENT_STREAM_CHANNEL : String="com.silvercenturion.eventalertsstream";
    static let  AUTH_EVENTS_STREAM_CHANNEL : String="com.silvercenturion.authevents";
}
