

import Foundation;
import Razorpay;

class RazorpayPaymentService: NSObject,RazorpayPaymentCompletionProtocolWithData{
    
    private var RAZORPAY_IO_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    private var razorpay_arguments:NSDictionary!
    private var razorpay_result: FlutterResult!;
    private var razorpay: RazorpayCheckout!;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        super.init();
        flutterViewcontroller=controller;
        RAZORPAY_IO_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.RAZORPAY_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService();
    }
    
    private func initFlutterChannelsService(){
        RAZORPAY_IO_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "initRazorpayNativePlugin"){
                result("Razorpay Init done");
            }
            else if(call.method == "_openRazorpayNative"){
                self?.razorpay_result=result;
                let razorpayInitArgue = call.arguments as? [String: AnyObject] ;
                if(razorpayInitArgue != nil){
                    let product_name = razorpayInitArgue!["name"] as? String ;
                    let prefill_email = razorpayInitArgue!["email"] as? String;
                    let prefill_phone = razorpayInitArgue!["phone"] as? String;
                    let product_amount = razorpayInitArgue!["amount"] as? String;
                    let product_orderId = razorpayInitArgue!["orderId"] as? String;
                    let product_method = razorpayInitArgue!["method"] as? String;
                    let product_image = razorpayInitArgue!["image"] as? String;
                    let razorpayRegId = razorpayInitArgue!["razorpayRegId"] as? String;
                    self?.showPaymentForm(name: product_name!, email:prefill_email!, phone:prefill_phone!, amount:product_amount!,orderId:product_orderId!, method:product_method!,image:product_image!,razorpayRegId:razorpayRegId!);
                }
            }
            else{
                result(FlutterMethodNotImplemented)
            }
        });
    }
    
    internal func showPaymentForm(name: String, email:String, phone:String, amount:String,orderId:String, method:String,image:String,razorpayRegId:String){
        /* Function Usecase : Open  Razorpay Payment Window*/
        razorpay = RazorpayCheckout.initWithKey(razorpayRegId, andDelegateWithData: self);
        let options: [String:Any] = [
            "amount" :amount ,
            "description": "Add Cash",
            "image": image,
            "name": name,
            "currency":"INR",
            "order_id":orderId,
            "prefill": [
                "contact": phone,
                "email": email,
                "method":method
            ],
            "theme": [
                "color": "#d32518"
            ]
        ]
        razorpay.open(options)
    }
    
    public func onPaymentError(_ code: Int32, description str: String,andData response: [AnyHashable : Any]?){
        /* Function Usecase : Call on Razorpay Payment Failed*/
        var successResponse = [String : Any]();
        successResponse["status"]="failed";
        successResponse["data"]="Payment failed";
    }
    
    public func onPaymentSuccess(_ payment_id: String,andData response: [AnyHashable : Any]?){
        /* Function Usecase : Call on Razorpay Payment success*/
        var dict : Dictionary = Dictionary<AnyHashable,Any>();
        if(response != nil){
            dict=response!;
        }
        let razorpay_signature : String = dict["razorpay_signature"] as? String ?? "";
        let razorpay_order_id : String = dict["razorpay_order_id"] as? String ?? "";
        var successResponse = [String : Any]();
        let jsonObject: NSDictionary = [
            "paymentId": payment_id,
            "signature": razorpay_signature,
            "orderId": razorpay_order_id,
            "status": "paid",
        ];
        successResponse["status"]="success";
        successResponse["data"]=jsonObject;
        razorpay_result(successResponse);
    }
}

