
import Foundation;
import Branch;

class BranchIoService: NSObject {
    private var BRANCH_IO_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    private var device_info_result: FlutterResult!;
    private var branchDataObject:[String: AnyObject]?;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        super.init();
        flutterViewcontroller=controller;
        BRANCH_IO_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.BRANCHIO_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService();
    }
    
    private func initFlutterChannelsService(){
        BRANCH_IO_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "_initBranchIoPlugin"){
                let data:[String: Any]=[:];
                if  let tempData = self?.branchDataObject {
                    result(tempData);
                }else{
                    result(data);
                }
            }
            else if(call.method == "_getInstallReferringLink"){
                let  channelResult:String=MyGlobalVariables.installReferring_link;
                result(channelResult)
            }
            else if(call.method == "trackAndSetBranchUserIdentity"){
                let  channelResult:String="";
                if let userId = call.arguments {
                    Branch.getInstance().setIdentity(userId as? String)
                }
                result(channelResult);
            }else if(call.method == "branchUserLogout"){
                Branch.getInstance().logout();
                result("Branch IO Logout Done");
            }
            else if(call.method == "branchLifecycleEventSigniup"){
                let  channelResult:String="";
                if let argue =  call.arguments as? NSDictionary{
                    if let data = argue["data"] as? [String: Any]{
                        let event = BranchEvent.standardEvent(.completeRegistration)
                        event.transactionID = argue["transactionID"] as? String;
                        event.eventDescription = argue["description"] as? String;
                        event.customData["registrationID"] = argue["registrationID"] as? String;
                        event.customData["login_name"] = data["login_name"] as? String;
                        event.customData["channelId"] =  data["channelId"] as? String;
                        for (key , value) in data {
                            event.customData[key+""] = value as? String;
                        }
                        event.logEvent();
                    }
                }
                result(channelResult);
            }
            else if(call.method == "branchEventTransactionFailed"){
                let  channelResult:String="";
                if let argue = call.arguments as? NSDictionary{
                    if let data =   argue["data"] as? NSDictionary{
                        var  isfirstDepositor:Bool=false;
                        var eventName:String="FIRST_DEPOSIT_FAILED";
                        isfirstDepositor = argue["firstDepositor"] as? Bool ?? false;
                        if(!isfirstDepositor){
                            eventName = "REPEAT_DEPOSIT_FAILED";
                        }
                        let event = BranchEvent.customEvent(withName:eventName)
                        for (key, value) in data {
                            event.customData[key as! String ] = value as? String;
                        }
                        event.logEvent();
                    }
                }
                result(channelResult)
            }
            else if(call.method == "branchEventTransactionSuccess"){
                let  channelResult:String="";
                if let argue = call.arguments as? NSDictionary{
                    if let data = argue["data"]! as? NSDictionary{
                        var  isfirstDepositor:Bool=false;
                        var eventName:String="FIRST_DEPOSIT_SUCCESS";
                        isfirstDepositor = argue["firstDepositor"] as? Bool ?? false;
                        if(!isfirstDepositor){
                            eventName = "REPEAT_DEPOSIT_SUCCESS";
                        }
                        let event = BranchEvent.customEvent(withName:eventName)
                        for (key, value) in data {
                            event.customData[key as! String ] = value as? String ;
                        }
                        event.logEvent();
                    }
                }
                result(channelResult)
            }
            else if(call.method=="getBranchInstallParams"){
                let installParams = Branch.getInstance().getFirstReferringParams();
                if let installParams=installParams{
                    result(installParams);
                }else{
                    let emptydict:[String: Any]=[:]
                    result(emptydict);
                }
            }else if(call.method=="getLatestReferringParms"){
                let sessionParams = Branch.getInstance().getLatestReferringParams();
                if let sessionParams=sessionParams{
                    result(sessionParams);
                }else{
                    let emptydict:[String: Any]=[:]
                    result(emptydict);
                }
                result(sessionParams);
            }
            else if(call.method == "_getGoogleAddId"){
                result("")
            }
            else{
                result(FlutterMethodNotImplemented)
            }
        });
    }
    
    public   func initBranchPlugin(didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        let branch = Branch.getInstance();
       branch.delayInitToCheckForSearchAds();
       branch.useLongerWaitForAppleSearchAds();
       branch.ignoreAppleSearchAdsTestData();
        branch.initSession(launchOptions: launchOptions) { (params, error) in
            if let error = error {
                print(error);
            } else if let params = params {
                self.initBranchSession(branchResultData:params as? [String: AnyObject]);
                self.branchDataObject=params as? [String: AnyObject];
                DeepLinkingManager.sendDeepLinkingDataReceivedEventAlert();
            }
            MyGlobalVariables.bBranchLodead = true;
        }
    }
    
    
    private func getDeviceInfo(deviceinfoResult: FlutterResult){
        let deviceInfoDict: NSDictionary = [
            "versionCode":MyDeviceInfo.getVersionCode(),
            "versionName":MyDeviceInfo.getVersionName(),
            "uid":MyDeviceInfo.getUID(),
            "model":MyDeviceInfo.getModel(),
            "serial":"",
            "manufacturer":"Apple",
            "version":MyDeviceInfo.getOSVersion(),
            "network_operator":MyDeviceInfo.getNetworkOperator(),
            "packageName":MyDeviceInfo.getPackageName(),
            "baseRevisionCode":"",
            "firstInstallTime":"",
            "lastUpdateTime":"",
            "device_ip_":MyDeviceInfo.getDeviceIPAddress(),
            "network_type":MyDeviceInfo.getNetworkType(),
            "googleaddid":MyDeviceInfo.identifierForAdvertising()
        ]
        device_info_result(deviceInfoDict)
    }
    

    private   func initBranchSession(branchResultData branchData:[String: AnyObject]?){
        var installReferring_link0:String = "";
        var installReferring_link1:String = "";
        var installReferring_link2:String = "";
        let installParams = Branch.getInstance().getFirstReferringParams();
        let sessionParams = Branch.getInstance().getLatestReferringParams();
        DeepLinkingManager.setDeepLinkingUsingBranchData(deepLinkingDataLabel:branchData);
        DeepLinkingManager.setDeepLinkingUsingBranchData(deepLinkingDataLabel:sessionParams as? [String: AnyObject]);
        if let  branchData = branchData {
            if(branchData["+clicked_branch_link"] != nil){
                if(branchData["+clicked_branch_link"]! as? Int == 1){
                    installReferring_link0=branchData["~referring_link"]! as? String ?? "";
                }
            }
        }
        if  let installParams = installParams {
            if(installParams["+clicked_branch_link"] != nil){
                if(installParams["+clicked_branch_link"]! as? Int == 1){
                    installReferring_link1=installParams["~referring_link"]! as? String ?? "";
                }
            }
        }
        if let sessionParams = sessionParams {
            if(sessionParams["+clicked_branch_link"] != nil){
                if(sessionParams["+clicked_branch_link"]! as? Int == 1){
                    installReferring_link2=sessionParams["~referring_link"]! as? String ?? "";
                }
            }
        }
        if (installReferring_link0.count > 2 ) {
            MyGlobalVariables.installReferring_link = installReferring_link0;
        } else if (installReferring_link1.count > 2) {
            MyGlobalVariables.installReferring_link = installReferring_link1;
        } else if (installReferring_link2.count > 2) {
            MyGlobalVariables.installReferring_link = installReferring_link2;
        }
    }
}

