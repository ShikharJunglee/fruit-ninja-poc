import Foundation;
import WebEngage;

class WebEngageService : NSObject, WEGInAppNotificationProtocol, WEGAppDelegate{
    
    var weUser: WEGUser!;
    var weAnalytics: WEGAnalytics!;
    var deepLinkingDataObject:[String: Any]=[:];
    
    private var WEBENGAGE_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        super.init();
        flutterViewcontroller=controller;
        WEBENGAGE_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.WEBENGAGE_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        weUser = WebEngage.sharedInstance().user;
        weAnalytics = WebEngage.sharedInstance().analytics;
        initFlutterChannelsService();
    }
    
    private func initFlutterChannelsService(){
        WEBENGAGE_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method=="webEngageEventSigniup"){
                if let argue = call.arguments as? [String : Any]{
                    self?.webEngageEventSigniup(signupData:argue);
                }
                result("");
            }
            if(call.method=="webEngageEventLogin"){
                let argue = call.arguments as? [String : Any];
                var  channelResult:String="";
                if(argue != nil){
                    channelResult = self!.webEngageEventLogin(loginData:argue!);
                }
                result(channelResult);
            }
            if(call.method=="webEngageTransactionFailed"){
                let argue = call.arguments as? [String : Any];
                var  channelResult:String="";
                if(argue != nil){
                    channelResult = self!.webEngageTransactionFailed(data:argue!);
                }
                result(channelResult);
            }
            if(call.method=="webEngageTransactionSuccess"){
                let argue = call.arguments as? [String : Any];
                var  channelResult:String="";
                if(argue != nil){
                    channelResult = self!.webEngageTransactionSuccess(data:argue!);
                }
                result(channelResult);
            }
            if(call.method == "webengageTrackUser"){
                let argue = call.arguments as? [String : Any];
                var  channelResult:String="";
                if(argue != nil){
                    if(argue != nil ){
                        channelResult = self!.webengageTrackUser(data:argue!);
                    }
                }
                result(channelResult)
            }
            else if (call.method == "webengageCustomAttributeTrackUser"){
                let argue = call.arguments as? [String : Any];
                var  channelResult:String="";
                if(argue != nil){
                    channelResult = self!.webengageCustomAttributeTrackUser(data:argue!);
                }
                result(channelResult);
            }
            else if(call.method == "trackEventsWithAttributes"){
                let argue = call.arguments as? [String : Any];
                var  channelResult:String="";
                if(argue != nil){
                    channelResult = self!.trackEventsWithAttributes(data:argue!);
                }
                result(channelResult);
            }
            else if(call.method == "webengageAddScreenData"){
                let argue = call.arguments as? [String : Any];
                var screenName:String = "";
                if(argue != nil){
                    if(argue!["screenName"] != nil){
                        screenName = argue!["screenName"] as! String;
                    }
                }
                self?.weAnalytics.navigatingToScreen(withName: screenName);
                let  channelResult:String="Added Scrren Data";
                result(channelResult);
            }
            else{
                result(FlutterMethodNotImplemented)
            }
        });
        
    }
    
    
    public  func initWebengage(_ application: UIApplication,appDelegate: AppDelegate,didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        WebEngage.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions,notificationDelegate:appDelegate,autoRegister:true);
    }
    
    private func  webengageTrackUser(data:[String:Any]?)-> String{
        /* Function Usecase : to track Web Engage User */
        var  trackType = "";
        var  value = "";
        if let data=data{
            if let trackTypeTemp=data["trackingType"] as? String{
                trackType=trackTypeTemp;
            }
            if let valueTemp=data["value"] as? String{
                value=valueTemp;
            }
            switch trackType{
            case "login":
                weUser.login(value);
                return "Login Track added";
            case "logout":
                weUser.logout();
                return "Logout To Tracking event done";
            case "setEmail":
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:value);
                weUser.setHashedEmail(encodeValue);
                return "Email track   added";
            case "setBirthDate":
                weUser.setBirthDateString(value);
                return "Birth Day track added";
            case "setPhoneNumber":
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:value);
                weUser.setHashedPhone(encodeValue);
                return "Phone Number track added";
            case "setFirstName":
                weUser.setFirstName(value);
                return "Login Track added";
            case "setGender":
                let gendel:String=value;
                if(gendel=="male"){
                    weUser.setGender(gendel);
                }
                else if (gendel=="female"){
                    weUser.setGender(gendel);
                }
                else{
                    weUser.setGender(gendel);
                }
                return "User Gender  added";
            case "setLastName":
                weUser.setLastName(value);
                return "Last Name  Track added";
            default:
                return "No Such tracking type found ";
            }
        }else{
            return "";
        }
    }
    
    private func  webengageCustomAttributeTrackUser(data:[String:Any]?)->String{
        /* Function Usecase : to track Web Engage User with custom attributes */
        var  value = "";
        var trackType="";
        if let data=data{
            if  let  type = data["trackType"] as? String {
                trackType = type;
            }
            if  let  valueTemp = data["value"] as? String {
                trackType = valueTemp;
            }
            weUser.setAttribute(trackType, withStringValue:value);
        }
        return "User tracking added";
    }
    
    
    private func trackEventsWithAttributes(data:[String:Any]?)-> String{
        /* Function Usecase : Track webengage Event with  Attributes*/
        if let  data=data{
            var eventName = "";
            if(data["eventName"] != nil){
                eventName = data["eventName"] as! String;
            }
            var addedAttributes:[String:Any] = [:];
            if let addedAttributesData=data["data"] as? [String:Any]{
                addedAttributes=addedAttributesData;
            }
            weAnalytics.trackEvent(withName: eventName, andValue:addedAttributes)
        }
        return "Event added";
    }
    
    private func webEngageEventSigniup(signupData:[String:Any]?) {
        /* Function Usecase : Web engage Event for Signup*/
        if let signupData=signupData{
            var email = "";
            var phone = "";
            var addedAttributes:[String:Any] = [:];
            if(signupData["email"] != nil){
                email = signupData["email"] as! String;
            }
            if(signupData["phone"] != nil){
                phone = signupData["phone"] as! String;
            }
            if(signupData["data"] != nil){
                addedAttributes = signupData["data"] as! [String:Any];
            }
            weAnalytics.trackEvent(withName: "COMPLETE_REGISTRATION", andValue:addedAttributes);
            if(email.count>3){
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:email);
                weUser.setHashedEmail(encodeValue);
            }
            if(phone.count>3){
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:phone);
                weUser.setHashedPhone(encodeValue);
            }
        }
    }
    
    private func webEngageEventLogin(loginData:[String:Any]?)->String{
        /* Function Usecase : Webengage event for login*/
        if let loginData=loginData{
            var email = "";
            var phone = "";
            var first_name = "";
            var last_name = "";
            var addedAttributes:[String:Any] = [:];
            if(loginData["email"] != nil){
                email = loginData["email"] as! String;
            }
            if(loginData["phone"] != nil){
                phone = loginData["phone"] as! String;
            }
            if(loginData["data"] != nil){
                addedAttributes = loginData["data"] as! [String:Any];
            }
            if(loginData["first_name"] != nil){
                first_name = loginData["first_name"] as! String;
            }
            if(loginData["last_name"] != nil){
                last_name = loginData["last_name"]! as! String ;
            }
            weAnalytics.trackEvent(withName: "COMPLETE_LOGIN", andValue:addedAttributes);
            if(email.count>3){
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:email);
                weUser.setHashedEmail(encodeValue);
            }
            if(phone.count>3){
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:phone);
                weUser.setHashedPhone(encodeValue);
            }
            if(loginData["first_name"] != nil){
                weUser.setFirstName(first_name);
            }
            if(loginData["last_name"] != nil){
                weUser.setLastName(last_name);
            }
        }
        return "Web engage Login  Event added";
    }
    
    private func webEngageTransactionFailed(data:[String:Any]?)-> String{
        /* Function Usecase : Webengage event for add cash transaction failed*/
        if let data=data{
            var isfirstDepositor:Bool = false;
            var eventName = "FIRST_DEPOSIT_FAILED";
            var addedAttributes:[String:Any] = [:];
            if let firstDepositor=data["firstDepositor"] as? Bool{
                isfirstDepositor=firstDepositor;
                if(!isfirstDepositor){
                    eventName = "REPEAT_DEPOSIT_FAILED";
                }
            }
            if let addedAttributesData = data["data"] as? [String:Any]{
                addedAttributes = addedAttributesData;
            }
            weAnalytics.trackEvent(withName: eventName, andValue:addedAttributes);
        }
        return "Web engage Transaction Failed  Event added";
    }
    
    private func webEngageTransactionSuccess(data:[String:Any]?)-> String{
        /* Function Usecase : Webengage event for add cash transaction success*/
        if let data=data{
            let eventName = "DEPOSIT_SUCCESS";
            var addedAttributes:[String:Any] = [:];
            if(data["data"] != nil){
                addedAttributes = data["data"] as! [String:Any];
            }
            weAnalytics.trackEvent(withName: eventName, andValue:addedAttributes);
        }
        return "Web engage Transaction Success  Event added";
    }
    
    public func onUserInfoRefreshed(data:[String:Any]?)-> String{
        if let data=data{
            if let userId = data["userId"] as? String{
                weUser.login(userId);
            }
            if let first_name = data["first_name"] {
                weUser.setFirstName(first_name as? String);
                weUser.setAttribute("first_name", withStringValue:first_name as? String);
            }
            if let email = data["email"] {
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:email as? String);
                weUser.setHashedEmail(encodeValue);
            }
            if let mobile = data["mobile"] {
                let encodeValue=MyHelperClass.doSHA256Encoding(stringData:mobile as? String);
                weUser.setHashedPhone(encodeValue);
            }
            if let lastName = data["lastName"] {
                weUser.setLastName(lastName as? String);
                weUser.setAttribute("last_name", withStringValue:lastName as? String);
            }
            if let login_name = data["login_name"] {
                weUser.setAttribute("userName", withStringValue:login_name as? String);
            }
            if let channelId = data["channelId"] {
                weUser.setAttribute("channelId", withStringValue:channelId as? String);
            }
            if let withdrawable = data["withdrawable"] {
                var valueinD:Double = withdrawable as! Double;
                valueinD = Double(round(100*valueinD)/100);
                let valueinNum = NSNumber(value: valueinD);
                weUser.setAttribute("withdrawable",  withValue:valueinNum);
            }
            if let balance = data["balance"] {
                var valueinD:Double = balance as! Double;
                valueinD = Double(round(100*valueinD)/100);
                let valueinNum = NSNumber(value: valueinD);
                weUser.setAttribute("balance",  withValue:valueinNum);
            }
            if let depositBucket = data["depositBucket"] {
                var valueinD:Double = depositBucket as! Double;
                valueinD = Double(round(100*valueinD)/100);
                let valueinNum = NSNumber(value: valueinD);
                weUser.setAttribute("depositBucket",  withValue:valueinNum);
            }
            if let nonWithdrawable = data["nonWithdrawable"] {
                var valueinD:Double = nonWithdrawable as! Double;
                valueinD = Double(round(100*valueinD)/100);
                let valueinNum = NSNumber(value: valueinD);
                weUser.setAttribute("nonWithdrawable",  withValue:valueinNum);
            }
            
            if let nonPlayableBucket=data["nonPlayableBucket"] {
                var valueinD:Double = nonPlayableBucket as! Double;
                valueinD = Double(round(100*valueinD)/100);
                let value = NSNumber(value: valueinD);
                weUser.setAttribute("nonPlayableBucket",  withValue:value);
            }
            if let pan_verification = data["pan_verification"] {
                if(pan_verification as? String == "DOC_NOT_SUBMITTED"){
                    weUser.setAttribute("idProofStatus", withValue:0);
                }else if(pan_verification as? String == "DOC_SUBMITTED"){
                    weUser.setAttribute("idProofStatus", withValue:1);
                }else if(pan_verification as? String == "UNDER_REVIEW"){
                    weUser.setAttribute("idProofStatus", withValue:2);
                }else if(pan_verification as? String == "DOC_REJECTED"){
                    weUser.setAttribute("idProofStatus", withValue:3);
                }else if(pan_verification as? String == "VERIFIED") {
                    weUser.setAttribute("idProofStatus", withValue:4);
                }
            }
            if let mobile_verification = data["mobile_verification"] {
                if(mobile_verification as! Bool){
                    weUser.setAttribute("mobileVerified", withValue:true);
                }else{
                    weUser.setAttribute("mobileVerified", withValue:false);
                }
            }
            if let address_verification = data["address_verification"] {
                if(address_verification as? String=="DOC_NOT_SUBMITTED"){
                    weUser.setAttribute("addressProofStatus", withValue:0);
                }else if(address_verification as? String=="DOC_SUBMITTED"){
                    weUser.setAttribute("addressProofStatus", withValue:1);
                }else if(address_verification as? String=="UNDER_REVIEW"){
                    weUser.setAttribute("addressProofStatus", withValue:2);
                }else if(address_verification as? String=="DOC_REJECTED"){
                    weUser.setAttribute("addressProofStatus", withValue:3);
                }else if(address_verification as? String=="VERIFIED") {
                    weUser.setAttribute("addressProofStatus", withValue:4);
                }
            }
            
            if let email_verification = data["email_verification"] {
                let value:Bool = email_verification as! Bool;
                if(value){
                    weUser.setAttribute("emailVerified", withValue:true);
                }else{
                    weUser.setAttribute("emailVerified", withValue:false);
                }
            }
            if let dob = data["dob"] {
                weUser.setBirthDateString(dob as? String);
            }
            if let state = data["state"] {
                weUser.setAttribute("State", withStringValue:state as? String);
            }
            if let user_balance_webengage = data["user_balance_webengage"] {
                var valueinD:Double = user_balance_webengage as! Double;
                valueinD = Double(round(100*valueinD)/100);
                let valueinNum = NSNumber(value: valueinD);
                weUser.setAttribute("balance", withValue:valueinNum);
            }
            if let accountStatus = data["accountStatus"] {
                weUser.setAttribute("accountStatus", withStringValue:accountStatus as? String);
            }
            if let appVersion = data["appVersion"] {
                weUser.setAttribute("appVersion", withStringValue:appVersion as? String);
            }
            if let state = data["state"] {
                weUser.setAttribute("state", withStringValue:state as? String);
            }
            if let chipBalance = data["chipBalance"] as? NSNumber{
                weUser.setAttribute("chipBalance", withValue:chipBalance);
            }
        }
        return "Data is used";
    }
    
    
    public  static func   getModifiedinAppNotificationData(inappdata:[String : Any]?)->[String : Any]{
        var resultDictionary = [String: Any]();
        var modifiedActionsArray:Array<Dictionary<String, Any>> = Array();
        /*We are modifaying the "action" array  value present in "inappdata" data */
        if let inappdata=inappdata{
            resultDictionary=inappdata;
            if let originalActionsArray=inappdata["actions"] as? Array<Dictionary<String, Any>>{
                resultDictionary["actions"] = [];
                for item in (originalActionsArray){
                    /*We are modifaying the "action item"   Dictionary value present in "originalActionsArray" data */
                    var modifiedActionItemDict:Dictionary<String, Any> = [:];
                    if let orginalActionItemDict = item as? [String : Any]{
                        modifiedActionItemDict = orginalActionItemDict;
                        DeepLinkingManager.setDeepLinkingDataModelFromWebengageInapp(actionItemFromSource:orginalActionItemDict);
                        modifiedActionItemDict["actionCategory"]="CLOSE";
                        modifiedActionItemDict["type"]="CLOSE";
                        modifiedActionItemDict["actionLink"]=" ";
                        modifiedActionsArray.append(modifiedActionItemDict)
                    }
                }
            }
        }
        resultDictionary["actions"] = modifiedActionsArray;
        return resultDictionary;
    }
    
    
    
    
    
}


