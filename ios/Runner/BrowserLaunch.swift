
import Foundation

class BrowserLaunch{
    
    private var BROWSER_LAUNCH_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        flutterViewcontroller=controller;
        BROWSER_LAUNCH_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.BROWSER_LAUNCH_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService();
    }
    
    
    private func initFlutterChannelsService(){
        
        BROWSER_LAUNCH_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "launchInBrowser"){
                if let weburl = call.arguments as? String{
                    guard let url = URL(string: weburl) else {
                        result(FlutterMethodNotImplemented)
                        return //be safe
                    }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                result("Broser Opened");
            }
            else{
                result(FlutterMethodNotImplemented)
            }
        });
        
    }
}

