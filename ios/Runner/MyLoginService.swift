//
//  SocialShareService.swift
//  Runner
//
//  Created by Subrahmanyam Pampana on 7/27/20.
//

import Foundation;
import GoogleSignIn;
import Firebase;
class MyLoginService: NSObject,GIDSignInDelegate{
    private var MY_LOGIN_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    private var googleLoginResult:FlutterResult!;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        super.init();
        flutterViewcontroller=controller;
        MY_LOGIN_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.MY_LOGIN_MCHANNEL,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService(appDelegate:appDelegate,controller:controller);
    }
    
    
    private func initFlutterChannelsService(appDelegate: AppDelegate,controller:FlutterViewController){
        MY_LOGIN_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "doGoogleLogin"){
                self?.googleLoginResult=result;
                GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID;
                GIDSignIn.sharedInstance().delegate = self;
                GIDSignIn.sharedInstance()?.presentingViewController = controller
                GIDSignIn.sharedInstance().signIn();
            }
            else{
                result(FlutterMethodNotImplemented)
            }
        });
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if let error = error {
            var successResponse = [String : Any]();
            successResponse["status"]="fail";
            googleLoginResult(successResponse);
            print(error);
            return
          }
        let idToken = user.authentication.accessToken
        var successResponse = [String : Any]();
        successResponse["status"]="success";
        successResponse["token"]=idToken;
        googleLoginResult(successResponse);
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            var successResponse = [String : Any]();
            successResponse["status"]="fail";
            googleLoginResult(successResponse);
            print(error);
            return
          }
    }
}


