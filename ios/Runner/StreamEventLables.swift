//
//  StreamEventLables.swift
//  Runner
//
//  Created by Subrahmanyam Pampana on 10/18/20.
//  Copyright © 2020 The Chromium Authors. All rights reserved.
//

import Foundation

struct StreamEventLables{
    static let  FCM_MESSAGE_RECIEVED : String="fcm_msg_recieved";
    static let  DEEPLINKING_DATA_RECIEVED : String="deeplinking_data_recieved";
}
