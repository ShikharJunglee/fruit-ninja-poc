//
//  DeepLinkingService.swift
//  Runner
//
//  Created by Subrahmanyam Pampana on 7/27/20.
//

import Foundation;

class DeepLinkingManager{
    
    
    public static  func setDeepLinkingUsingBranchData(deepLinkingDataLabel data:[String: AnyObject]?){
        if let dlData=data{
            if let clicked_branch_link=dlData["+clicked_branch_link"]{
                if(clicked_branch_link as? Int == 1){
                    setDeepLinkingDataModelFromDict(dataLabel:dlData)
                }else{
                    guard let non_branch_link = dlData["+non_branch_link"] else {
                        return
                    }
                    let str = non_branch_link as? String ?? "";
                    let validURL = MyHelperClass.isValidUrl(urlString:str);
                    if(validURL){
                        DeepLinkingManager.setDeepLinkingDataModelFromURL(dataLabel:str);
                    }
                }
            }
        }
    }
    
    
    
    public static func  setDeepLinkingDataModelFromURL(dataLabel deepLinkingURL:String?){
        if let deepLinkingURL=deepLinkingURL{
            if let dlURLFormat=URL(string: deepLinkingURL){
                let deepLinkingURLParms = MyHelperClass.getQueryParametersDict(from: dlURLFormat);
                if(!deepLinkingURLParms.isEmpty){
                    setDeepLinkingDataModelFromDict(dataLabel:deepLinkingURLParms);
                }
            }
        }
    }
    
    
    
    public static func setDeepLinkingDataFromFCMData(dataLabel userInfo:[AnyHashable : Any]?){
        if let userInfo=userInfo{
            let deepLinkingDataModel = DeepLinkingDataModel();
            if let body = userInfo["body"] {
                deepLinkingDataModel.sg_body = body as! String;
            }
            if let matchId = userInfo["matchId"] {
                deepLinkingDataModel.sg_matchId = matchId as! String;
            }
            if let title = userInfo["title"] {
                deepLinkingDataModel.sg_title = title as! String;
            }
            if let type = userInfo["type"] {
                deepLinkingDataModel.sg_type = type as! String;
            }
            deepLinkingDataModel.activateDeepLinkingNavigation=true;
            deepLinkingDataModel.dl_page_route="gameresults";
            let deepLinkingDataObject:[String: Any]=DeepLinkingHelper.getDeepLinkingDictFromStruct(deepLinkStructdata:deepLinkingDataModel);
            MyGlobalVariables.deepLinkingDataObject=deepLinkingDataObject;
        }
    }
    
    
    public static func sendDeepLinkingDataReceivedEventAlert(){
        if(MyGlobalVariables.nativeToFlutterAlertsEventSink != nil){
            var eventDataToSend:[String: Any]=[:];
            eventDataToSend["eventName"]=StreamEventLables.DEEPLINKING_DATA_RECIEVED;
            eventDataToSend["data"]=MyGlobalVariables.deepLinkingDataObject;
            MyGlobalVariables.nativeToFlutterAlertsEventSink(eventDataToSend);
            setDeepLinkingDataToDefault();
        }
    }
    
    
    public static func  setDeepLinkingDataModelFromDict(dataLabel deepLinkingData:[String: Any]?){
        let deepLinkingDataModel = DeepLinkingDataModel();
        if let data=deepLinkingData{
            if let dl_page_route = data["dl_page_route"] {
                deepLinkingDataModel.dl_page_route = dl_page_route as! String;
                
                print(deepLinkingDataModel.dl_page_route);
                print(deepLinkingDataModel.dl_page_route);
                if(deepLinkingDataModel.dl_page_route.count>2){
                    deepLinkingDataModel.activateDeepLinkingNavigation=true;
                }
            }
            if let sg_type = data["sg_type"] {
                deepLinkingDataModel.sg_type = sg_type as! String;
            }
            if let body = data["body"] {
                deepLinkingDataModel.sg_body = body as! String;
            }
            if let matchId = data["matchId"] {
                deepLinkingDataModel.sg_matchId = matchId as! String;
            }
            if let sg_matchId = data["sg_matchId"] {
                deepLinkingDataModel.sg_matchId = sg_matchId as! String;
            }
            if let title = data["title"] {
                deepLinkingDataModel.sg_title = title as! String;
            }
            if let type = data["type"] {
                deepLinkingDataModel.sg_type = type as! String;
            }
            if let dl_ac_promoamount = data["dl_ac_promoamount"] {
                deepLinkingDataModel.dl_ac_promoamount = dl_ac_promoamount as! String;
            }
            if let dl_ac_promocode = data["dl_ac_promocode"] {
                deepLinkingDataModel.dl_ac_promocode = dl_ac_promocode as! String;
            }
            if let dl_sp_pageLocation = data["dl_sp_pageLocation"] {
                deepLinkingDataModel.dl_sp_pageLocation = dl_sp_pageLocation as! String;
            }
            if let dl_sp_pageTitle = data["dl_sp_pageTitle"] {
                deepLinkingDataModel.dl_sp_pageTitle = dl_sp_pageTitle as! String;
            }
            let deepLinkingDataObject:[String: Any]=DeepLinkingHelper.getDeepLinkingDictFromStruct(deepLinkStructdata:deepLinkingDataModel);
            MyGlobalVariables.deepLinkingDataObject=deepLinkingDataObject;
        }
    }
    
    
    public static func handleDeepLinkingURLFromWebEngage(webengageURL:String?){
        if let urlFromWE = webengageURL{
            let validURL = MyHelperClass.isValidUrl(urlString:urlFromWE);
            if(validURL){
                let deepLinkingURL = URL(string: urlFromWE);
                let deepLinkingURLParms = MyHelperClass.getQueryParametersDict(from: deepLinkingURL!);
                if let enableDeepLinking=deepLinkingURLParms["enableDeepLinking"] as? String{
                    if(enableDeepLinking == "true"){
                        setDeepLinkingDataModelFromDict(dataLabel:deepLinkingURLParms);
                    }
                    if( MyGlobalVariables.nativeToFlutterAlertsEventSink != nil){
                        var eventDataToSend:[String: Any]=[:];
                        eventDataToSend["eventName"]="deeplinking_data_recieved";
                        eventDataToSend["data"]=MyGlobalVariables.deepLinkingDataObject;
                        MyGlobalVariables.nativeToFlutterAlertsEventSink(eventDataToSend);
                        setDeepLinkingDataToDefault();
                    }
                }else{
                    let weburl = urlFromWE;
                    guard let url = URL(string: weburl) else {
                        return //be safe
                    }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }
    
    
    public static func setDeepLinkingDataModelFromWebengageInapp(actionItemFromSource:Dictionary<String, Any>?){
        if let actionItemFromSource=actionItemFromSource{
            if let deepLinkingURL=actionItemFromSource["actionLink"] as? String{
                let deepLinkingURLArray = deepLinkingURL.components(separatedBy: "?");
                if(deepLinkingURLArray.count>1){
                    let queryParmetersArray = deepLinkingURLArray[1].components(separatedBy: "&")
                    if(queryParmetersArray.count>1){
                        for queryParmeter in queryParmetersArray{
                            let queryParmAndValueArray = queryParmeter.components(separatedBy: "=");
                            if(MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp != nil){
                                if(queryParmAndValueArray[0] == "dl_page_route"){
                                    MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp.dl_page_route = queryParmAndValueArray[1];
                                    MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp.activateDeepLinkingNavigation=true;
                                    MyGlobalVariables.actionEIdWhichHasDL = actionItemFromSource["actionEId"] as! String;
                                }
                                if(queryParmAndValueArray[0] == "dl_sp_pageTitle"){
                                    MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp.dl_sp_pageTitle = queryParmAndValueArray[1];
                                }
                                if(queryParmAndValueArray[0] == "dl_ac_promocode"){
                                    MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp.dl_ac_promocode = queryParmAndValueArray[1];
                                }
                                if(queryParmAndValueArray[0] == "dl_ac_promoamount"){
                                    MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp.dl_ac_promoamount = queryParmAndValueArray[1];
                                }
                                if(queryParmAndValueArray[0] == "dl_sp_pageLocation"){
                                    MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp.dl_sp_pageLocation = queryParmAndValueArray[1];
                                }
                                if(queryParmAndValueArray[0] == "dl_unique_id"){
                                    MyGlobalVariables.deepLinkingDataModelFromWEbengageInapp.dl_unique_id = queryParmAndValueArray[1];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    public static func setDeepLinkingDataToDefault(){
        MyGlobalVariables.deepLinkingDataObject["activateDeepLinkingNavigation"] = false;
    }
    public static func setDeepLinkingDataToTrue(){
        MyGlobalVariables.deepLinkingDataObject["activateDeepLinkingNavigation"] = true;
    }
    
    
}
