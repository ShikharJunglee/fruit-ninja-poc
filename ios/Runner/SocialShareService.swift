//
//  SocialShareService.swift
//  Runner
//
//  Created by Subrahmanyam Pampana on 7/27/20.
//

import Foundation
class SocialShareService{
    
    private var SOCIAL_SHARE_CHANNEL:FlutterMethodChannel!;
    private var flutterViewcontroller : FlutterViewController!;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        flutterViewcontroller=controller;
        SOCIAL_SHARE_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.SOCIALSHARE_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService();
    }
    
    
    private func initFlutterChannelsService(){
        SOCIAL_SHARE_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "initSocialShareChannel"){
                result("Social Share");
            }
            if(call.method == "shareViaWhatsApp"){
                if let texttoShare=call.arguments as? String{
                    self?.shareViaWhatsApp(msg:texttoShare);
                }
                result("Social Share");
            }
            if(call.method == "shareText"){
                if let texttoShare=call.arguments as? String{
                    self?.shareText(viewController:self?.flutterViewcontroller,msg:texttoShare);
                }
                result("Social Share");
            }
            if(call.method == "shareViaFacebook"){
                if let texttoShare=call.arguments as? String{
                    self?.shareText(viewController:self?.flutterViewcontroller,msg:texttoShare);
                }
                result("Social Share");
            }
            if(call.method == "shareViaGmail"){
                if let texttoShare=call.arguments as? String{
                    self?.shareText(viewController:self?.flutterViewcontroller,msg:texttoShare);
                }
                result("Social Share");
            }
            
            if(call.method == "shareViaTelegram"){
                if let texttoShare=call.arguments as? String{
                    self?.shareViaTelegram(msg:texttoShare);
                }
                result("Social Share");
            }
            else{
                result(FlutterMethodNotImplemented)
            }
        });
    }
    
    func shareViaWhatsApp(msg:String){
        let msg = msg
        let urlWhats = "whatsapp://send?text=\(msg)";
        var isOpened=false;
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL)
                    isOpened=true;
                } else {
                    isOpened=false;
                }
            }
        }
    }
    func shareViaTelegram(msg:String){
        let msg = msg;
        let urlWhats = "tg://msg?text=\(msg)"
        var isOpened=false;
        if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
            if let whatsappURL = NSURL(string: urlString) {
                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
                    UIApplication.shared.open(whatsappURL as URL)
                    isOpened=true;
                } else {
                    isOpened=false;
                }
            }
        } 
    }
    
    func shareText(viewController: UIViewController!,msg:String){
        let text = msg;
        let textToShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [.airDrop]
        
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
            popoverController.sourceView = viewController.view
            popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }
        viewController.present(activityViewController, animated: true, completion: nil);
    }
    
}

