//
//  UtilsService.swift
//  Runner
//
//  Created by Subrahmanyam Pampana on 7/27/20.
//

import Foundation;
import CoreLocation;

class UtilsService{
    
    
    private var location_permission_result: FlutterResult!;
    private var flutterViewcontroller : FlutterViewController!;
    private var flutterAppDelegate: AppDelegate!;
    let locationManager = CLLocationManager();
    
    private var UTILS_CHANNEL:FlutterMethodChannel!;
    
    init(appDelegate: AppDelegate,controller:FlutterViewController)  {
        flutterAppDelegate=appDelegate;
        flutterViewcontroller=controller;
        UTILS_CHANNEL = FlutterMethodChannel(name: MyMethodConstants.UTILS_SERVICE_MNAME,binaryMessenger: controller.binaryMessenger);
        initFlutterChannelsService();
    }
    
    
    private func initFlutterChannelsService(){
        
        UTILS_CHANNEL.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            if(call.method == "onUserInfoRefreshed"){
                let argue = call.arguments as? [String : Any];
                var  channelResult:String="";
                if(argue != nil){
                    channelResult = self?.flutterAppDelegate.onUserInfoRefreshed(data:argue!) as! String;
                }
                result(channelResult);
            }
            else if(call.method == "getLocationLongLat"){
                self?.location_permission_result=result;
                // self?.getLocationLongLat();
            }
            else  if(call.method == "_deepLinkingRoutingHandler"){
                self?.deepLinkingRoutingHandler(result: result);
                DeepLinkingManager.setDeepLinkingDataToDefault();
            }
            else if(call.method == "bShowNativeAppleLogin"){
                if #available(iOS 13.0, *) {
                    result(true);
                } else {
                    result(false);
                }
            }
            else if(call.method == "getDeviceInfoFromNative"){
                self?.getDeviceInfo(deviceinfoResult: result);
            }
            
        });
        
    }
    
    private func deepLinkingRoutingHandler(result: FlutterResult){
        result(MyGlobalVariables.deepLinkingDataObject);
    }
    
    private func getDeviceInfo(deviceinfoResult: FlutterResult){
        let deviceInfoDict: NSDictionary = [
            "versionCode":MyDeviceInfo.getVersionCode(),
            "versionName":MyDeviceInfo.getVersionName(),
            "uid":MyDeviceInfo.getUID(),
            "model":MyDeviceInfo.getModel(),
            "serial":"",
            "manufacturer":"Apple",
            "version":MyDeviceInfo.getOSVersion(),
            "network_operator":MyDeviceInfo.getNetworkOperator(),
            "packageName":MyDeviceInfo.getPackageName(),
            "baseRevisionCode":"",
            "firstInstallTime":"",
            "lastUpdateTime":"",
            "device_ip_":MyDeviceInfo.getDeviceIPAddress(),
            "network_type":MyDeviceInfo.getNetworkType(),
            "googleaddid":MyDeviceInfo.identifierForAdvertising()
        ]
        deviceinfoResult(deviceInfoDict)
    }

}

