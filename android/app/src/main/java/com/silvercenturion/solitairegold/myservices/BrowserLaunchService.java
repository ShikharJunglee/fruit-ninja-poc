package com.silvercenturion.solitairegold.myservices;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import com.silvercenturion.solitairegold.MainActivity;
import com.silvercenturion.solitairegold.MyApplicationClass;
import com.silvercenturion.solitairegold.mycommonutilcollection.MyDeviceInfo;
import com.silvercenturion.solitairegold.myconstants.ServiceConstants;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class BrowserLaunchService {

    private static final String BROWSER_LAUNCH_CHANNEL = ServiceConstants.BROWSER_LAUNCH_SERVICE_MNAME;

    public BrowserLaunchService(FlutterView flutterView, Activity activity, Context applicationContext) {
        initFlutterChannels(flutterView, activity, applicationContext);
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context context) {
        new MethodChannel(flutterView, BROWSER_LAUNCH_CHANNEL)
                .setMethodCallHandler(new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        if (methodCall.method.equals("launchInBrowser")) {
                            String linkUrl = methodCall.arguments();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkUrl));
                            activity.startActivity(browserIntent);
                        } else {
                            result.notImplemented();
                        }
                    }
                });
    }

}

