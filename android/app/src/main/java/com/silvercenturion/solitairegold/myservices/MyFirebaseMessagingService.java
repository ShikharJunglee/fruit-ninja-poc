package com.silvercenturion.solitairegold.myservices;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import android.os.Process;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.silvercenturion.solitairegold.MainActivity;
import com.silvercenturion.solitairegold.MyApplicationClass;
import com.silvercenturion.solitairegold.R;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.silvercenturion.solitairegold.myconstants.MyConstants;
import com.webengage.sdk.android.WebEngage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService{

    private static final String TAG = "MyFirebaseMsgService";
    Bitmap imageUriBitmap, pictureUrlBitmap, pictureBigBitmap;
    Intent resultIntent;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        boolean foregroud=false;
        try{
            foregroud=isApplicationForeground(this);
        }catch(Exception e){
            System.out.println(e);
        }
        try{
            Map<String, String> data = remoteMessage.getData();
            if (data != null){
                resultIntent=new Intent(this, MainActivity.class);
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                resultIntent.putExtra("isPushNotification","true");
                resultIntent.putExtra("fcmData",data.toString());

                if (data.containsKey("source") && "webengage".equals(data.get("source"))){
                    WebEngage.get().receive(data);
                }else{
                    if(foregroud){
                        sendDataToFlutter(remoteMessage);
                    }else{
                    if (data.containsKey("title") && data.containsKey("body") && data.containsKey("image_url") && data.containsKey("picture_url")) {
                        String pn_title = data.get("title");
                        String pn_body = data.get("body");
                        String pn_imageUri = data.get("image_url");
                        String pn_pictureUrl = data.get("picture_url");
                        showImageNotification(pn_title,pn_body,pn_imageUri,pn_pictureUrl);
                    }else if(data.containsKey("title") && data.containsKey("body")){
                        String pn_title = data.get("title");
                        String pn_body = data.get("body");
                        showNotificationWithBodyAndTitle(pn_title,pn_body);
                    }
                }}
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }

    @Override
    public void onNewToken(String token) {
        WebEngageService.setFCMKeyForWebEngage(token);
    }

    private  void sendDataToFlutter(RemoteMessage remoteMessage){
        try{
                Intent intent = new Intent("fcm_data_broadcast");
                intent.putExtra("fcmData", remoteMessage);
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }catch(Exception e){
            System.out.println(e);
        }
    }

    private  void showNotificationWithBodyAndTitle(String pn_title, String pn_body){
        try{
            /*Build Notification*/
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, MyConstants.FCM_CHANNEL_ID)
                    .setSmallIcon(R.drawable.push_icon)
                    .setContentTitle(pn_title)
                    .setContentText(pn_body)
                    .setAutoCancel(true)
                    .setColor(getResources().getColor((R.color.fcm_icon_bg)))
                    .setDefaults(Notification.DEFAULT_VIBRATE | Notification.FLAG_SHOW_LIGHTS | Notification.DEFAULT_SOUND)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentIntent(pendingIntent);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(MyConstants.FCM_CHANNEL_ID, MyConstants.FCM_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
                manager.createNotificationChannel(channel);
            }
            manager.notify(0, builder.build());
        }catch(Exception e){
            System.out.println(e);
        }

    }

    private void showImageNotification(String pn_title, String pn_body,String pn_imageUri,String pn_pictureUrl) {
        try {
            pictureUrlBitmap = getBitmapfromUrl(pn_pictureUrl);
            imageUriBitmap = getBitmapfromUrl(pn_imageUri);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
            bigPictureStyle.bigPicture(pictureUrlBitmap);
            bigPictureStyle.setBigContentTitle(pn_body);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, MyConstants.FCM_CHANNEL_ID)
                    .setSmallIcon(R.drawable.push_icon)
                    .setContentTitle(pn_title)
                    .setContentText(pn_body)
                    .setLargeIcon(imageUriBitmap)
                    .setDefaults(Notification.DEFAULT_VIBRATE | Notification.FLAG_SHOW_LIGHTS | Notification.DEFAULT_SOUND)
                    .setColor(getResources().getColor((R.color.fcm_icon_bg)))
                    .setSound(defaultSoundUri)
                    .setAutoCancel(true)
                    .setStyle(bigPictureStyle)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            builder.setContentIntent(pendingIntent);
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(MyConstants.FCM_CHANNEL_ID, MyConstants.FCM_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
                manager.createNotificationChannel(channel);
            }
            manager.notify(0, builder.build());
        } catch (Exception e) {
            System.out.print(e.toString());
        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    private boolean isAppOnForeground(Context context, String appPackageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        final String packageName = appPackageName;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {

                return true;
            }
        }
        return false;
    }
    private static boolean isApplicationForeground(Context context) {
        KeyguardManager keyguardManager =
                (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

        if (keyguardManager.isKeyguardLocked()) {
            return false;
        }
        int myPid = Process.myPid();

        ActivityManager activityManager =
                (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningAppProcessInfo> list;

        if ((list = activityManager.getRunningAppProcesses()) != null) {
            for (ActivityManager.RunningAppProcessInfo aList : list) {
                ActivityManager.RunningAppProcessInfo info;
                if ((info = aList).pid == myPid) {
                    return info.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
                }
            }
        }
        return false;
    }

}