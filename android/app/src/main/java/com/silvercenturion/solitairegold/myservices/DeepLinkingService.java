package com.silvercenturion.solitairegold.myservices;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.silvercenturion.solitairegold.MainActivity;
import com.silvercenturion.solitairegold.MyApplicationClass;
import com.silvercenturion.solitairegold.myconstants.MyConstants;
import com.silvercenturion.solitairegold.mydatamodels.DeepLinkingDataModel;
import com.silvercenturion.solitairegold.myconstants.StreamEventLables;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DeepLinkingService {

    public static void setDeepLinkingDataUsingIntentData(Intent intent, Activity activity) {
        /*Handling deep linking Data from push */
        if (intent != null && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            try {
                DeepLinkingDataModel deepLinkingDataModel = new DeepLinkingDataModel();
                if (extras.containsKey("dl_ac_promocode")) {
                    deepLinkingDataModel.setDlAcPromocode((String) extras.get("dl_ac_promocode"));
                }
                if (extras.containsKey("dl_ac_promoamount")) {
                    deepLinkingDataModel.setDlAcPromoamount((String) extras.get("dl_ac_promoamount"));
                }
                if (extras.containsKey("dl_sp_pageLocation")) {
                    deepLinkingDataModel.setDlSpPageLocation((String) extras.get("dl_sp_pageLocation"));
                }
                if (extras.containsKey("dl_sp_pageTitle")) {
                    deepLinkingDataModel.setDl_sp_pageTitle((String) extras.get("dl_sp_pageTitle"));
                }
                if (extras.containsKey("dl_unique_id")) {
                    deepLinkingDataModel.setDlUnique_id((String) extras.get("dl_unique_id"));
                }
                if (extras.containsKey("title")) {
                    String title = (String) extras.get("title");
                    deepLinkingDataModel.setGame_title(title);
                }
                if (extras.containsKey("matchId")) {
                    String matchId = (String) extras.get("matchId");
                    deepLinkingDataModel.setGame_matchId(matchId);
                }
                if (extras.containsKey("sg_matchId")) {
                    String matchId = (String) extras.get("sg_matchId");
                    deepLinkingDataModel.setGame_matchId(matchId);
                }
                if (extras.containsKey("type")) {
                    String type = (String) extras.get("type");
                    deepLinkingDataModel.setGame_type(type);
                }
                if (extras.containsKey("sg_type")) {
                    String type = (String) extras.get("sg_type");
                    deepLinkingDataModel.setGame_type(type);
                }
                if (extras.containsKey("body")) {
                    String body = (String) extras.get("body");
                    deepLinkingDataModel.setGame_body(body);
                }
                if (intent.getExtras().containsKey("enableDeepLinking")) {
                    String enableDeepLinking = extras.getString("enableDeepLinking");
                    if (enableDeepLinking.equals("true")) {
                        if (extras.containsKey("dl_page_route")) {
                            String dl_page_route = extras.getString("dl_page_route");
                            if (dl_page_route.length() > 2) {
                                deepLinkingDataModel.setDlPageRoute(dl_page_route);
                                deepLinkingDataModel.setActivateDeepLinkingNavigation(true);
                                setDeepLinkingDataObject(activity, deepLinkingDataModel);
                            }
                        }
                    }
                } else if (intent.getExtras() != null && intent.getExtras().containsKey("title")) {
                    deepLinkingDataModel.setDlPageRoute("gameresults");
                    deepLinkingDataModel.setActivateDeepLinkingNavigation(true);
                    setDeepLinkingDataObject(activity, deepLinkingDataModel);
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    public void setDeepLinkingDataUsingURL(Intent intent, Activity activity) {

    }

    public static void setDeepLinkingDataUsingJsonData(JSONObject referringParams, Activity activity) {
        String enableDeepLinking = referringParams.optString("enableDeepLinking", "");
        DeepLinkingDataModel deepLinkingDataModel = new DeepLinkingDataModel();
         if(referringParams.has("+clicked_branch_link")){
             try{
                 boolean clickedOnBranchLink=referringParams.getBoolean("+clicked_branch_link");
                 if(clickedOnBranchLink){
                     FlutterToNativeUtilsService.setAppLaunchSource(activity, MyConstants.APP_LAUNCH_SOURCE_DL);
                 }
             }catch (Exception e){

             }
         }
        if (enableDeepLinking.equals("true")) {
            deepLinkingDataModel.setActivateDeepLinkingNavigation(true);
            deepLinkingDataModel.setDlPageRoute(referringParams.optString("dl_page_route", ""));
            deepLinkingDataModel.setDlAcPromocode(referringParams.optString("dl_ac_promocode", ""));
            deepLinkingDataModel.setDlAcPromoamount(referringParams.optString("dl_ac_promoamount", ""));
            String pageLocation=referringParams.optString("dl_sp_pageLocation", "");
            deepLinkingDataModel.setDlSpPageLocation(pageLocation.toString());
            deepLinkingDataModel.setDl_sp_pageTitle(referringParams.optString("dl_sp_pageTitle", ""));
            deepLinkingDataModel.setDlUnique_id(referringParams.optString("dl_unique_id", ""));
            String matchId=referringParams.optString("matchId", "");
            String sg_matchId=referringParams.optString("sg_matchId", "");
            if(matchId!=null && !matchId.isEmpty()){
                deepLinkingDataModel.setGame_matchId(matchId);
            }
            if(sg_matchId!=null && !sg_matchId.isEmpty()){
                deepLinkingDataModel.setGame_matchId(sg_matchId);
            }
            String type=referringParams.optString("type", "");
            String sg_type=referringParams.optString("sg_type", "");
            if(type!=null && !type.isEmpty()){
                deepLinkingDataModel.setGame_type(type);
            }
            if(sg_type!=null && !sg_type.isEmpty()){
                deepLinkingDataModel.setGame_type(sg_type);
            }
            setDeepLinkingDataObject(activity, deepLinkingDataModel);
        }
    }

    public static void setDeepLinkingDataUsingMap(Map<String, Object> arguments, Activity activity) {
        DeepLinkingDataModel deepLinkingDataModel = new DeepLinkingDataModel();
        if (arguments.containsKey("enableDeepLinking")) {
            String enableDeepLinking = (String)arguments.get("enableDeepLinking");
            if (enableDeepLinking.equals("true")) {
                deepLinkingDataModel.setActivateDeepLinkingNavigation(true);
            }
        }
        if (arguments.containsKey("dl_page_route")) {
            String dl_page_route = (String) arguments.get("dl_page_route");
            deepLinkingDataModel.setDlPageRoute(dl_page_route);
        }
        if (arguments.containsKey("dl_ac_promocode")) {
            String dl_ac_promocode = (String) arguments.get("dl_ac_promocode");
            deepLinkingDataModel.setDlAcPromocode(dl_ac_promocode);
        }
        if (arguments.containsKey("dl_ac_promoamount")) {
            String dl_ac_promoamount = (String) arguments.get("dl_ac_promoamount");
            deepLinkingDataModel.setDlAcPromoamount(dl_ac_promoamount);
        }
        if (arguments.containsKey("dl_sp_pageLocation")) {
            String dl_sp_pageLocation = (String) arguments.get("dl_sp_pageLocation");
            deepLinkingDataModel.setDlSpPageLocation(dl_sp_pageLocation);
        }
        if (arguments.containsKey("dl_sp_pageTitle")) {
            String dl_sp_pageTitle = (String) arguments.get("dl_sp_pageTitle");
            deepLinkingDataModel.setDl_sp_pageTitle(dl_sp_pageTitle);
        }
        if (arguments.containsKey("dl_unique_id")) {
            String dl_unique_id = (String) arguments.get("dl_unique_id");
            deepLinkingDataModel.setDlUnique_id(dl_unique_id);
        }
        if (arguments.containsKey("title")) {
            String title = (String) arguments.get("title");
            deepLinkingDataModel.setGame_title(title);
        }
        if (arguments.containsKey("matchId")) {
            String matchId = (String) arguments.get("matchId");
            deepLinkingDataModel.setGame_matchId(matchId);
        }
        if (arguments.containsKey("sg_matchId")) {
            String matchId = (String) arguments.get("sg_matchId");
            deepLinkingDataModel.setGame_matchId(matchId);
        }
        if (arguments.containsKey("type")) {
            String type = (String) arguments.get("type");
            deepLinkingDataModel.setGame_type(type);
        }
        if (arguments.containsKey("sg_type")) {
            String type = (String) arguments.get("sg_type");
            deepLinkingDataModel.setGame_type(type);
        }
        if (arguments.containsKey("body")) {
            String body = (String) arguments.get("body");
            deepLinkingDataModel.setGame_body(body);
        }
        if (arguments.containsKey("type")&&arguments.containsKey("matchId")) {
            deepLinkingDataModel.setDlPageRoute("gameresults");
            deepLinkingDataModel.setActivateDeepLinkingNavigation(true);
            setDeepLinkingDataObject(activity, deepLinkingDataModel);
        }
    }


    public static  void setDeepLinkingDataObjectToDefault(Activity activity){
        ((MyApplicationClass) activity.getApplication()).setDeepLinkingDataObject(DeepLinkingDataModel.getDefaultDeepLinkingDataMap());
    }
    public static  void setDeepLinkingDataObject(Activity activity, DeepLinkingDataModel deepLinkingDataModel){
        ((MyApplicationClass) activity.getApplication()).setDeepLinkingDataObject(deepLinkingDataModel.getDeepLinkingDataMap());
        HashMap<String, Object> deepLinkingDataObject= ((MyApplicationClass) activity.getApplication()).getDeepLinkingDataObject();
        if(deepLinkingDataObject.containsKey("activateDeepLinkingNavigation")){
            try{
                boolean activateDeepLinkingNavigation = (boolean)deepLinkingDataObject.get("activateDeepLinkingNavigation");
                if(activateDeepLinkingNavigation){
                    if (MainActivity.eventAlertDataEventSink != null) {
                        setDeepLinkingDataEventSinkSuccess(deepLinkingDataObject);
                        setDeepLinkingDataObjectToDefault(activity);
                    }
                }
            }catch(Exception e){

            }
        }
    }

    public static void setDeepLinkingDataEventSinkSuccess(HashMap<String, Object> deepLinkingDataObject){
       if(MainActivity.eventAlertDataEventSink !=null){
           Map<String,Object> eventData=new HashMap<>();
           eventData.put("eventName", StreamEventLables.DEEPLINKING_DATA_RECIEVED);
           eventData.put("data",deepLinkingDataObject);
           MainActivity.eventAlertDataEventSink.success(eventData);
       }
    }

    public static void setDeepLinkingDataEventSinkSuccess(DeepLinkingDataModel deepLinkingDataModel) {
        if(MainActivity.eventAlertDataEventSink!=null){
            Map<String,Object> eventData=new HashMap<>();
            eventData.put("eventName", StreamEventLables.DEEPLINKING_DATA_RECIEVED);
            eventData.put("data",deepLinkingDataModel.getDeepLinkingDataMap());
            MainActivity.eventAlertDataEventSink.success(eventData);
        }
    }
}
