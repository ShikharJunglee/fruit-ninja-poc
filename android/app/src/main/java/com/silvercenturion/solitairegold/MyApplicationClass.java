package com.silvercenturion.solitairegold;


import com.silvercenturion.solitairegold.myconstants.MyConstants;
import com.silvercenturion.solitairegold.mydatamodels.DeepLinkingDataModel;
import com.webengage.sdk.android.WebEngage;
import com.webengage.sdk.android.WebEngageActivityLifeCycleCallbacks;
import com.webengage.sdk.android.WebEngageConfig;

import java.util.HashMap;

import io.branch.referral.Branch;
import io.flutter.app.FlutterApplication;

public class MyApplicationClass extends FlutterApplication {
    /*BranchClass extends FlutterApplication.To remove the Branch function from this project replace  android:name=".services.BranchClass" to io.flutter.app.FlutterApplication*/

    private String firebaseFCMToken = " ";
    private String googleAdId = "";
    private String installReferring_link = "";
    private String refCodeFromBranch = "";
    private HashMap<String, Object> deepLinkingDataObject;
    private String appLaunchSource = " ";


    @Override
    public void onCreate() {
        super.onCreate();
        deepLinkingDataObject = DeepLinkingDataModel.getDefaultDeepLinkingDataMap();
        /*Branch logging for debugging. Commint this line to disable the Branch Io logs*/
        Branch.enableLogging();
        /* Branch object initialization in apllication lavel*/
        Branch.getAutoInstance(this);
        initWebEngage();
    }

    private void initWebEngage() {
        WebEngageConfig config = new WebEngageConfig.Builder()
                .setWebEngageKey(MyConstants.getWebEngageKey(this))
                .setPushSmallIcon(R.drawable.push_icon)
                .setPushAccentColor(getResources().getColor((R.color.fcm_icon_bg)))
                .build();
        registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(this, config));
        FlutterApplication flutterApplication = new FlutterApplication();
        registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(flutterApplication, config));
        WebEngage.registerPushNotificationCallback(new MainActivity());
    }

    public String getFirebaseFCMToken() {
        return firebaseFCMToken;
    }

    public void setFirebaseFCMToken(String firebaseFCMToken) {
        this.firebaseFCMToken = firebaseFCMToken;
    }

    public String getGoogleAdId() {
        return googleAdId;
    }

    public void setGoogleAdId(String googleAdId) {
        this.googleAdId = googleAdId;
    }

    public String getInstallReferring_link() {
        return installReferring_link;
    }

    public void setInstallReferring_link(String installReferring_link) {
        this.installReferring_link = installReferring_link;
    }

    public String getRefCodeFromBranch() {
        return refCodeFromBranch;
    }

    public void setRefCodeFromBranch(String refCodeFromBranch) {
        this.refCodeFromBranch = refCodeFromBranch;
    }

    public HashMap<String, Object> getDeepLinkingDataObject() {
        return deepLinkingDataObject;
    }

    public void setDeepLinkingDataObject(HashMap<String, Object> deepLinkingDataObject) {
        this.deepLinkingDataObject = deepLinkingDataObject;
    }
    public String getAppLaunchSource() {
        return appLaunchSource;
    }

    public void setAppLaunchSource(String appLaunchSource) {
        this.appLaunchSource = appLaunchSource;
    }
}