package com.silvercenturion.solitairegold;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import io.branch.referral.Branch;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

import com.facebook.CallbackManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import com.silvercenturion.solitairegold.mycommonutilcollection.MyHelperClass;
import com.silvercenturion.solitairegold.myconstants.MyConstants;
import com.silvercenturion.solitairegold.myconstants.StreamEventLables;
import com.silvercenturion.solitairegold.myservices.BranchIoService;
import com.silvercenturion.solitairegold.myservices.BrowserLaunchService;
import com.silvercenturion.solitairegold.myservices.DeepLinkingService;
import com.silvercenturion.solitairegold.myservices.FCMServiceManager;
import com.silvercenturion.solitairegold.myservices.FlutterToNativeUtilsService;
import com.silvercenturion.solitairegold.myservices.MyLoginService;
import com.silvercenturion.solitairegold.myservices.PaytmNativeService;
import com.silvercenturion.solitairegold.myservices.RazorpayPaymentService;
import com.silvercenturion.solitairegold.myservices.ShareMessageService;
import com.silvercenturion.solitairegold.myservices.TechProcessService;
import com.silvercenturion.solitairegold.myservices.WebEngageService;
import com.silvercenturion.solitairegold.myservices.GPSService;
import com.silvercenturion.solitairegold.mycommonutilcollection.MyDeviceInfo;
import com.webengage.sdk.android.actions.render.InAppNotificationData;
import com.webengage.sdk.android.actions.render.PushNotificationData;
import com.webengage.sdk.android.callbacks.InAppNotificationCallbacks;
import com.webengage.sdk.android.callbacks.PushNotificationCallbacks;
import com.webengage.sdk.android.WebEngage;

import java.util.HashMap;
import java.util.Map;

import android.view.ViewTreeObserver;
import android.view.WindowManager;
public class MainActivity extends FlutterActivity implements PushNotificationCallbacks, InAppNotificationCallbacks, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static Context applicationContext;
    private static FlutterToNativeUtilsService flutterToNativeUtilsService;
    private static BranchIoService branchIoService;
    private static WebEngageService webEngageService;
    private static ShareMessageService shareMessageService;
    private static RazorpayPaymentService razorpayPaymentService;
    private static TechProcessService techProcessService;
    private static BrowserLaunchService browserLaunchService;
    private static FCMServiceManager fcmServiceManager;
    private static PaytmNativeService paytmNativeService;
    private static MyLoginService myLoginService;
    private CallbackManager fbCallbackManager;
    private static GPSService gpsService;
    private boolean bActivateIndiusOSAttribution = false;
    public static EventChannel.EventSink eventAlertDataEventSink;
    private static final String EVENTALERTS_STREAM_CHANNEL = "com.silvercenturion.eventalertsstream";
    private static final String AUTHEVENTS_STREAM_CHANNEL = "com.silvercenturion.authevents";
    private HashMap<String, Object> fcmPayloadMap;
    private static MainActivity currentActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    boolean flutter_native_splash = true;
    int originalStatusBarColor = 0;
    currentActivity = this;
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        originalStatusBarColor = getWindow().getStatusBarColor();
        getWindow().setStatusBarColor(0xff000000);
    }
    int originalStatusBarColorFinal = originalStatusBarColor;

        initHighPriorityServices();
        initTheAppServices();
        initTheSDKServices();
        GeneratedPluginRegistrant.registerWith(this);
    ViewTreeObserver vto = getFlutterView().getViewTreeObserver();
    vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        getFlutterView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
          getWindow().setStatusBarColor(originalStatusBarColorFinal);
        }
      }
    });

        initFlutterEvents();
        WebEngage.registerInAppNotificationCallback(this);
    }

    public void initHighPriorityServices() {
        applicationContext = getApplicationContext();
        MyDeviceInfo myDeviceInfo = new MyDeviceInfo();
        initPushNotifications();
        myDeviceInfo.setGoogleAdvertisingID(this);
        FlutterToNativeUtilsService.setAppLaunchSource(this, MyConstants.APP_LAUNCH_SOURCE_APP_ICON);
        checkForFcmData();
    }
    private void checkForFcmData(){
        try{
                 Bundle extras = getIntent().getExtras();
                 if(extras!=null){
                     fcmPayloadMap=new HashMap<>();
                     if (extras.containsKey("matchId")) {
                         String id =(String) extras.get("matchId");
                         fcmPayloadMap.put("matchId",id);
                         FlutterToNativeUtilsService.setAppLaunchSource(this,MyConstants.APP_LAUNCH_SOURCE_NOTIF);
                     }
                     if (extras.containsKey("type")) {
                         String id =(String) extras.get("type");
                         fcmPayloadMap.put("type",id);
                     }
                     if (extras.containsKey("isPushNotification")) {
                         FlutterToNativeUtilsService.setAppLaunchSource(this,MyConstants.APP_LAUNCH_SOURCE_NOTIF);
                     }
                 }
        }catch(Exception e){
        }
    }


    private void initTheAppServices() {
        flutterToNativeUtilsService = new FlutterToNativeUtilsService(getFlutterView(), MainActivity.this, this.applicationContext);
        razorpayPaymentService = new RazorpayPaymentService(getFlutterView(), MainActivity.this, applicationContext);
        branchIoService = new BranchIoService(getFlutterView(), MainActivity.this, this.applicationContext);
        webEngageService = new WebEngageService(getFlutterView(), MainActivity.this, this.applicationContext);
        fcmServiceManager = new FCMServiceManager(getFlutterView(), MainActivity.this, this.applicationContext);
        paytmNativeService=new PaytmNativeService(getFlutterView(), MainActivity.this, this.applicationContext);
        shareMessageService = new ShareMessageService(getFlutterView(), MainActivity.this, this.applicationContext);
        browserLaunchService=new BrowserLaunchService(getFlutterView(), MainActivity.this, this.applicationContext);
        /*Init myLoginService  only after fbCallbackManager init */
        fbCallbackManager = CallbackManager.Factory.create();
        myLoginService=new MyLoginService(getFlutterView(), MainActivity.this, this.applicationContext,fbCallbackManager);
        gpsService = new GPSService(getFlutterView(), MainActivity.this, this.applicationContext);
    }
    private void initTheSDKServices() {
        webEngageService.initWebEngage(MainActivity.this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("fcm_data_broadcast"));
    }

    @Override
    public void onStart() {
        super.onStart();
        DeepLinkingService.setDeepLinkingDataObjectToDefault(this);
        checkFCMDeepLinking();
        initBranchPlugin();
    }

    private void checkFCMDeepLinking(){
        if(fcmPayloadMap!=null){
            if(fcmPayloadMap.containsKey("matchId")&&fcmPayloadMap.containsKey("type")){
                DeepLinkingService.setDeepLinkingDataUsingMap(fcmPayloadMap,this);
                FlutterToNativeUtilsService.setAppLaunchSource(this,MyConstants.APP_LAUNCH_SOURCE_NOTIF);
            }
            if(fcmPayloadMap.containsKey("matchId")){
                fcmPayloadMap.remove("matchId");
            }
            if(fcmPayloadMap.containsKey("type")){
                fcmPayloadMap.remove("type");
            }
            if (fcmPayloadMap.containsKey("source") && "webengage".equals(fcmPayloadMap.get("source"))){
                FlutterToNativeUtilsService.setAppLaunchSource(this,MyConstants.APP_LAUNCH_SOURCE_NOTIF);
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public PushNotificationData onPushNotificationReceived(Context context, PushNotificationData notificationData) {
        return notificationData;
    }

    @Override
    public void onPushNotificationShown(Context context, PushNotificationData notificationData) {
    }

    @Override
    public boolean onPushNotificationClicked(Context context, PushNotificationData notificationData) {
        if(webEngageService !=null){
            return webEngageService.onWEPushNotificationCLicked(notificationData);
        }else{
            return false;
        }
    }

    @Override
    public boolean onPushNotificationActionClicked(Context context, PushNotificationData notificationData, String buttonID) {
        return false;
    }

    @Override
    public void onPushNotificationDismissed(Context context, PushNotificationData notificationData) {
    }

    @Override
    public InAppNotificationData onInAppNotificationPrepared(Context context, InAppNotificationData notificationData) {

        return notificationData;
    }

    @Override
    public void onInAppNotificationShown(Context context, InAppNotificationData notificationData) {

    }
    @Override
    public void onInAppNotificationDismissed(Context context, InAppNotificationData notificationData) {
    }

    @Override
    public boolean onInAppNotificationClicked(Context context, InAppNotificationData notificationData, String actionId) {
        if(webEngageService !=null){
            return webEngageService.onWEInnAppNotificationCLicked(notificationData, actionId);
        }else{
            return false;
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
        Branch.getInstance().reInitSession(this, branchIoService.branchReferralInitListener);
        DeepLinkingService.setDeepLinkingDataUsingIntentData(getIntent(), MainActivity.this);
    }


    private void initBranchPlugin() {
        Branch.getInstance().initSession(branchIoService.branchReferralInitListener, getIntent() != null ?
                getIntent().getData() : null, this);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try{
                RemoteMessage remoteMessage =
                        intent.getParcelableExtra("fcmData");
                Map<String, String> arguments = remoteMessage.getData();
                Map<String,Object> data=new HashMap<String,Object>();
                data.put("eventName", StreamEventLables.FCM_MESSAGE_RECIEVED);
                data.put("data",arguments);
                if (eventAlertDataEventSink != null) {
                    eventAlertDataEventSink.success(data);
                }
            }catch(Exception e){
            }
        }
    };

    private void initFlutterEvents() {
        new EventChannel(getFlutterView(), EVENTALERTS_STREAM_CHANNEL).setStreamHandler(
                new EventChannel.StreamHandler() {
                    @Override
                    public void onListen(Object args, final EventChannel.EventSink events) {
                        eventAlertDataEventSink=events;
                    }
                    @Override
                    public void onCancel(Object args) {

                    }
                }
        );
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PaytmNativeService.PAYTM_REQUEST_CODE && data != null) {
            paytmNativeService.onPaytmNativeActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RazorpayPaymentService.RAZORPAY_CUSTOME_REQUEST_CODE) {
            razorpayPaymentService.onRazoraPayActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == MyLoginService.RC_SIGN_IN) {
            myLoginService.onGoogleLoginActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == 1000) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                gpsService.onGPSResult(true);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                gpsService.onGPSResult(false);
            }
        }
    }

    public static void onHowzatUserInfoRefreshed(Map<String, Object> arguments,Activity activity) {
        webEngageService.onUserInfoRefreshed(arguments,activity);
    }

    private void initPushNotifications() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            /*Create channel to show notifications.*/
            String channelId = "fcm_default_channel";
            String channelName = "News";
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(
                    new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW));
        }
        String fmChannelName = "news";
        FirebaseMessaging.getInstance().subscribeToTopic(fmChannelName)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {

                        }
                    }
                });
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String token = task.getResult().getToken();
                        WebEngageService.setFCMKeyForWebEngage(token);
                        ((MyApplicationClass) getApplication()).setFirebaseFCMToken(token);
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {        
    }

    @Override
    public void onConnectionSuspended(int i) {        
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {        
    }
}