package com.silvercenturion.solitairegold.myservices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
//import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.login.LoginBehavior.NATIVE_WITH_FALLBACK;

public class MyLoginService {
    private static final String LOGIN_SERVICE_CHANNEL = "com.silvercenturion.loginservice";
    private FlutterView flutterView;
    public static final int RC_SIGN_IN = 98901;
    private  LoginManager facebookLoginManager;
    private CallbackManager fbCallbackManager;
    private MethodChannel.Result fbLoginResult;
    private MethodChannel.Result googleLoginResult;
    //private GoogleSignInClient mGoogleSignInClient;

    public MyLoginService(FlutterView flutterView, Activity activity, Context applicationContext,CallbackManager callbackManager) {
        this.flutterView = flutterView;
        initFlutterChannels(flutterView, activity, applicationContext);
        this.fbCallbackManager=callbackManager;
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context applicationContext) {

        new MethodChannel(flutterView, LOGIN_SERVICE_CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                if (methodCall.method.equals("doGoogleLogin")) {
                    String clientID = methodCall.arguments();
                    googleLoginResult=result;
                    doGoogleLogin(activity,clientID);
                } else if (methodCall.method.equals("doFacebookLogin")) {
                    fbLoginResult=result;
                    doFaceBookLogin(activity);
                }
            }
        });
    }


    private void  doFaceBookLogin(Activity activity){
        this.facebookLoginManager = LoginManager.getInstance();
        this.facebookLoginManager.registerCallback(fbCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        String accessToken = loginResult.getAccessToken()
                                .getToken();
                        Map<String, Object> result = new HashMap<>();
                        result.put("status", "success");
                        result.put("token", accessToken);
                        fbLoginResult.success(result);
                    }
                    @Override
                    public void onCancel() {
                        Map<String, Object> result = new HashMap<>();
                        result.put("status", "failed");
                        fbLoginResult.success(result);
                    }
                    @Override
                    public void onError(FacebookException exception) {
                        Map<String, Object> result = new HashMap<>();
                        result.put("status", "failed");
                        fbLoginResult.success(result);
                    }
        });
        this.facebookLoginManager.setLoginBehavior(NATIVE_WITH_FALLBACK);
        this.facebookLoginManager.logIn(activity,Arrays.asList("public_profile"));
    }

    private void  doGoogleLogin(Activity activity,String clientId){
//        String serverClientId = clientId;
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(serverClientId)
//                .requestServerAuthCode(serverClientId, false).requestEmail().build();
//        mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
//        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
//        Intent googleSignInIntent = mGoogleSignInClient.getSignInIntent();
//        activity.startActivityForResult(googleSignInIntent, RC_SIGN_IN);
    }

    public void onGoogleLoginActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == MyLoginService.RC_SIGN_IN) {
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            handleSignInResult(task);
//        }else{
//            Map<String, Object> result = new HashMap<>();
//            result.put("status", "failed");
//            googleLoginResult.success(result);
//        }

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
            String accessToken =acct.getIdToken();
            Map<String, Object> result = new HashMap<>();
            result.put("status", "success");
            result.put("token", accessToken);
            googleLoginResult.success(result);

        } catch (ApiException e) {

            Map<String, Object> result = new HashMap<>();
            result.put("status", "failed");
            googleLoginResult.success(result);

        }
    }



}

