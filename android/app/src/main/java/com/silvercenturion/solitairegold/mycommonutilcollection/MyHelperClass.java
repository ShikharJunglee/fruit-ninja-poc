package com.silvercenturion.solitairegold.mycommonutilcollection;


import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.Uri;
import android.util.Base64;

import com.silvercenturion.solitairegold.myconstants.MyConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class MyHelperClass {

    public String getQueryParmValueFromUrl(String url, String attribute) {
        Uri uri = Uri.parse(url);
        String value = uri.getQueryParameter(attribute);
        if (value == null || value.isEmpty()) {
            value = "";
        }
        return value;
    }

    public static String doSHA256Encoding(String input,Activity activity) {
        String encodedString = "";
        encodedString = encrypt(input, MyConstants.getWebEngageEncryptionKey(activity), MyConstants.getWebEngageEncryptionIVKey(activity));
        return encodedString;
    }

    private static String encrypt(String email, String secretKey, String ivParameterKey) {
        String encString = "";
        try {
            SecretKeySpec key = new SecretKeySpec(secretKey.getBytes(), "AES");
            IvParameterSpec iv = new IvParameterSpec(ivParameterKey.getBytes());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            byte[] encrypted = cipher.doFinal(email.getBytes());
            encString = Base64.encodeToString(encrypted, Base64.DEFAULT);
            return encString;
        } catch (Exception ex) {
        }

        return encString;
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    public static  void copyTextToClipBoard(Context context,String label,String text){
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(label, text);
        clipboard.setPrimaryClip(clip);
    }



}
