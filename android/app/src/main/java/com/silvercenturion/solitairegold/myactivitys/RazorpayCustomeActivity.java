
package com.silvercenturion.solitairegold.myactivitys;
import com.silvercenturion.solitairegold.R;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;

import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;
import com.razorpay.Razorpay;
import com.razorpay.RazorpayWebChromeClient;
import com.razorpay.RazorpayWebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RazorpayCustomeActivity extends AppCompatActivity {
    Razorpay razorpay;
    private WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razorpay_custome);
        try{
            Intent intent = getIntent();
            HashMap<String, Object> arguments = (HashMap<String, Object>)intent.getSerializableExtra("arguments");
            razorpay = new Razorpay(this,(String) arguments.get("razorpayRegId"));
            createWebView(this);
            razorpay.getPaymentMethods(new Razorpay.PaymentMethodsCallback() {
                @Override
                public void onPaymentMethodsReceived(String result) {
                    try{
                        JSONObject paymentMethods = new JSONObject(result);
                        System.out.println(paymentMethods);
                    }
                    catch(Exception e) {
                    }
                }
                @Override
                public void onError(String error){

                }
            });
            startPayment(arguments, this);
        }catch (Exception e){
            System.out.println(e);
        }
    }


    private void createWebView(Activity activity) {
        try{
            webview = (WebView) activity.findViewById(R.id.payment_webview);
            razorpay.setWebView(webview);
        }catch(Exception e){
            System.out.println(e);
        }
        /**
         * Override the RazorpayWebViewClient for your custom hooks
         */
        razorpay.setWebviewClient(new RazorpayWebViewClient(razorpay) {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d("TAG", "Custom client onPageStarted");
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.d("TAG", "Custom client onPageFinished");
            }
        });

        /**
         * Extend the RazorpayWebChromeClient for your custom hooks
         */
        razorpay.setWebChromeClient(new RazorpayWebChromeClient(razorpay) {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                // Make sure you don't forget to call the super method
                super.onProgressChanged(view, newProgress);
                //customProgressBar.setProgress(newProgress);
            }
        });
    }



    public void startPayment(Map<String, Object> arguments, final Activity activity) {
        System.out.println(arguments);
        try {
            String paymentMethod=(String) arguments.get("method");
            JSONObject data = new JSONObject();
            data.put("currency", "INR");
            data.put("order_id", (String) arguments.get("orderId"));
            data.put("amount", (String) arguments.get("amount"));
            data.put("email", (String) arguments.get("email"));
            data.put("contact", (String) arguments.get("phone"));
            data.put("method", paymentMethod);
            if(paymentMethod.equals("card")){
                String cardNo = (String) arguments.get("tp_cardNumber");
                String cvv = (String) arguments.get("tp_cvv");
                String nameOnCard = (String) arguments.get("tp_nameOnTheCard");
                String expiryYear = (String) arguments.get("tp_expireYear");
                String expiryYearSubstring = expiryYear.substring(Math.max(expiryYear.length() - 2, 0));
                int expiryYearSubstringInteger=Integer.parseInt(expiryYearSubstring);
                String expiryMonth = (String) arguments.get("tp_expireMonth");
                int expiryMonthInteger=Integer.parseInt(expiryMonth);

                boolean bSaveCard=(boolean) arguments.get("cardDataCapturingRequired");
                boolean isValidCard=razorpay.isValidCardNumber(cardNo);
                System.out.println(isValidCard);
                data.put("card[name]", nameOnCard);
                data.put("card[number]", cardNo);
                data.put("card[expiry_month]", expiryMonth);
                data.put("card[expiry_year]", expiryYearSubstring);
                data.put("card[cvv]", cvv);
            }else if(paymentMethod.equals("netbanking")){
                if((String) arguments.get("bank") !=null){
                    String bank = (String) arguments.get("bank");
                    data.put("bank", bank);
                }
            } else if(paymentMethod.equals("wallet")){
                data.put("wallet", (String) arguments.get("wallet"));
            } else if(paymentMethod.equals("upi")){
                String  upiPaymentFlow=(String) arguments.get("upiPaymentFlow");
                String  userSelectedUPIPaymentPkg=(String) arguments.get("userSelectedUPIPaymentPkg");
                String  upiID=(String) arguments.get("upiID");
                if(upiPaymentFlow.equals("intent")){
                    data.put("upi_app_package_name", userSelectedUPIPaymentPkg);
                    data.put("_[flow]", "intent");
                }else{
                    data.put("vpa", upiID);
                }
            }
            JSONObject notes = new JSONObject();
            notes.put("custom_field", "Add Cash");
            data.put("notes", notes);
            razorpay.validateFields(data, new Razorpay.ValidationListener() {
                @Override
                public void onValidationSuccess() {
                    try {
                        /* Make webview visible before submitting payment details*/
                        webview.setVisibility(View.VISIBLE);
                        razorpay.submit(data, new PaymentResultWithDataListener() {
                            @Override
                            public void onPaymentSuccess(String razorpayPaymentId, PaymentData paymentData) {
                                // Razorpay payment ID and PaymentData passed here after a successful payment
                                webview.setVisibility(View.GONE);
                                Map<String,Object> object= new HashMap<String,Object>();
                                object.put("paymentId", paymentData.getPaymentId());
                                object.put("signature", paymentData.getSignature());
                                object.put("orderId", paymentData.getOrderId());
                                object.put("status", "paid");
                                processRPPaymentSuccess(object);
                            }

                            @Override
                            public void onPaymentError(int code, String description,PaymentData paymentData) {
                                webview.setVisibility(View.GONE);
                                /*Error code and description is passed here*/
                                processRPPaymentFailed(description.toString());
                            }
                        });
                    } catch (Exception e) {
                        processRPPaymentFailed("Exception:"+e.toString());
                    }
                }

                @Override
                public void onValidationError(Map<String, String> error) {
                    processRPPaymentFailed("Exception:"+error.toString());
                }
            });

        } catch (Exception e) {
            Intent data = new Intent();
            data.putExtra("paymentResultData","Error in submitting payment details"+e.toString());
            this.setResult(0, data);
            this.finish();
        }
    }

    private void processRPPaymentFailed(String message){
        Intent data = new Intent();
        data.putExtra("paymentResultData",message);
        this.setResult(0, data);
        this.finish();
    }
    private void processRPPaymentSuccess(Map<String,Object> paymentData){
        Intent data = new Intent();
        data.putExtra("paymentResultData",(Serializable) paymentData);
        this.setResult(1, data);
        this.finish();
    }



    /* callback for permission requested from android */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (razorpay != null) {
            razorpay.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onBackPressed() {
        razorpay.onBackPressed();
        webview.setVisibility(View.GONE);
        processRPPaymentFailed("Back Pressed");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(razorpay!=null){
            razorpay.onActivityResult(requestCode,resultCode,data);
        }
    }

}
