package com.silvercenturion.solitairegold.mydatamodels;

import java.util.HashMap;

public class DeepLinkingDataModel {
    boolean activateDeepLinkingNavigation = false;
    String dl_page_route = " ";
    String dl_ac_promocode = " ";
    String dl_ac_promoamount = " ";
    String dl_sp_pageLocation = " ";
    String dl_sp_pageTitle = " ";
    String dl_unique_id = " ";
    String dl_event_type = " ";
    String sg_body="";
    String sg_matchId="";
    String sg_title="";
    String sg_type="";

    public String getGame_body() {
        return sg_body;
    }

    public void setGame_body(String game_body) {
        this.sg_body = game_body;
    }

    public String getGame_matchId() {
        return sg_matchId;
    }

    public void setGame_matchId(String game_matchId) {
        this.sg_matchId = game_matchId;
    }

    public String getGame_title() {
        return sg_title;
    }

    public void setGame_title(String game_title) {
        this.sg_title = game_title;
    }

    public String getGame_type() {
        return sg_type;
    }

    public void setGame_type(String game_type) {
        this.sg_type = game_type;
    }

    public void setActivateDeepLinkingNavigation(boolean value) {
        this.activateDeepLinkingNavigation = value;
    }

    public void setDlPageRoute(String name) {
        this.dl_page_route = name;
    }

    public void setDlAcPromocode(String name) {
        this.dl_ac_promocode = name;
    }

    public void setDlSpPageLocation(String name) {
        this.dl_sp_pageLocation = name;
    }

    public void setDlUnique_id(String name) {
        this.dl_unique_id = name;
    }

    public void setDlAcPromoamount(String name) {
        this.dl_ac_promoamount = name;
    }

    public boolean getActivateDeepLinkingNavigation() {
        return this.activateDeepLinkingNavigation;
    }
    public String getDlPageRoute() {
        return this.dl_page_route;
    }

    public String getDlAcPromocode() {
        return this.dl_ac_promocode;
    }

    public String getDlSpPageLocation() {
        return this.dl_sp_pageLocation;
    }

    public String getDl_event_type() {
        return dl_event_type;
    }

    public void setDl_event_type(String dl_event_type) {
        this.dl_event_type = dl_event_type;
    }

    public String getDlUnique_id() {
        return this.dl_unique_id;
    }


    public String getDl_sp_pageTitle() {
        return dl_sp_pageTitle;
    }

    public void setDl_sp_pageTitle(String dl_sp_pageTitle) {
        this.dl_sp_pageTitle = dl_sp_pageTitle;
    }


    public String getDlAcPromoamount() {
        return this.dl_ac_promoamount;
    }

    public HashMap getDeepLinkingDataMap() {
        HashMap<String, Object> deepLinkingDataObject = new HashMap<>();
        deepLinkingDataObject.put("activateDeepLinkingNavigation", activateDeepLinkingNavigation);
        deepLinkingDataObject.put("dl_page_route", dl_page_route);
        deepLinkingDataObject.put("dl_ac_promocode", dl_ac_promocode);
        deepLinkingDataObject.put("dl_ac_promoamount", dl_ac_promoamount);
        deepLinkingDataObject.put("dl_sp_pageLocation", dl_sp_pageLocation);
        deepLinkingDataObject.put("dl_sp_pageTitle", dl_sp_pageTitle);
        deepLinkingDataObject.put("dl_unique_id", dl_unique_id);
        deepLinkingDataObject.put("sg_body", sg_body);
        deepLinkingDataObject.put("sg_title", sg_title);
        deepLinkingDataObject.put("sg_matchId", sg_matchId);
        deepLinkingDataObject.put("sg_type", sg_type);
        return deepLinkingDataObject;
    }

    public static HashMap getDefaultDeepLinkingDataMap() {
        HashMap<String, Object> deepLinkingDataObject = new HashMap<>();
        deepLinkingDataObject.put("activateDeepLinkingNavigation", false);
        deepLinkingDataObject.put("dl_page_route", " ");
        deepLinkingDataObject.put("dl_ac_promocode", " ");
        deepLinkingDataObject.put("dl_ac_promoamount", " ");
        deepLinkingDataObject.put("dl_sp_pageLocation", " ");
        deepLinkingDataObject.put("dl_sp_pageTitle", " ");
        deepLinkingDataObject.put("dl_unique_id", " ");
        deepLinkingDataObject.put("sg_body", " ");
        deepLinkingDataObject.put("sg_title", " ");
        deepLinkingDataObject.put("sg_matchId", " ");
        deepLinkingDataObject.put("sg_type", " ");
        return deepLinkingDataObject;
    }

}


