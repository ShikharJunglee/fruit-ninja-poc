package com.silvercenturion.solitairegold.myservices;


import android.app.Activity;
import android.content.Context;

import androidx.annotation.Nullable;
import com.silvercenturion.solitairegold.MyApplicationClass;
import com.silvercenturion.solitairegold.mycommonutilcollection.MyDeviceInfo;
import com.silvercenturion.solitairegold.myconstants.ServiceConstants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.BRANCH_STANDARD_EVENT;
import io.branch.referral.util.BranchEvent;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class BranchIoService {
    private static final String BRANCH_IO_CHANNEL = ServiceConstants.BRANCHIO_SERVICE_MNAME;
    private FlutterView flutterView;
    MethodChannel.Result branchResultChannel;
    JSONObject branchDataObject;
    private Activity activity;
    private boolean bActivateIndiusOSAttribution = false;

    public BranchIoService(FlutterView flutterView, Activity activity, Context applicationContext) {
        this.flutterView = flutterView;
        initFlutterChannels(flutterView, activity, applicationContext);
        this.activity = activity;
    }


    public Branch.BranchReferralInitListener branchReferralInitListener =
            new Branch.BranchReferralInitListener() {
                @Override
                public void onInitFinished(@Nullable JSONObject referringParams, @Nullable BranchError error) {
                    if (error == null) {
                        initDeepLink();
                        if (branchResultChannel != null) {
                            branchResultChannel.success(referringParams.toString());
                        } else {
                            branchDataObject = referringParams;
                        }
                    }
                }
            };

    private void onBranchDataRequest(MethodChannel.Result result) {
        if (branchDataObject != null) {
            result.success(branchDataObject.toString());
        } else {
            branchResultChannel = result;
        }
    }

    private void initDeepLink() {
        JSONObject sessionParams = Branch.getInstance().getLatestReferringParams();
        DeepLinkingService.setDeepLinkingDataUsingJsonData(sessionParams, this.activity);
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context applicationContext) {
        new MethodChannel(flutterView, BRANCH_IO_CHANNEL)
                .setMethodCallHandler(new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        if (methodCall.method.equals("_initBranchIoPlugin")) {
                            onBranchDataRequest(result);
                        }
                        if (methodCall.method.equals("_getInstallReferringLink")) {
                            String installReferring_link = " ";
                            if (!bActivateIndiusOSAttribution) {
                                installReferring_link = (String) getInstallReferringLink(activity);
                            }
                            result.success(installReferring_link);
                        }

                        if (methodCall.method.equals("trackAndSetBranchUserIdentity")) {
                            String userId = methodCall.arguments();
                            trackAndSetBranchUserIdentity(userId);
                            result.success("Branch Io user Identity added.BranchIO Login done.");
                        }
                        if (methodCall.method.equals("branchUserLogout")) {
                            branchUserLogout();
                            result.success("Branch Io user Logout done");
                        }
                        if (methodCall.method.equals("branchLifecycleEventSigniup")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = branchLifecycleEventSigniup(arguments, applicationContext);
                            result.success(channelResult);
                        }
                        if (methodCall.method.equals("branchEventAddCustomEvent")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = branchEventAddCustomEvent(arguments, applicationContext);
                            result.success(channelResult);
                        }
                        if (methodCall.method.equals("branchEventTransactionFailed")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = branchEventTransactionFailed(arguments, applicationContext);
                            result.success(channelResult);
                        }
                        if (methodCall.method.equals("branchEventTransactionSuccess")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = branchEventTransactionSuccess(arguments, applicationContext);
                            result.success(channelResult);
                        }

                        if (methodCall.method.equals("_getGoogleAddId")) {
                            String googleAddId = (String) getGoogleAddId(activity);
                            result.success(googleAddId);
                        }
                        if (methodCall.method.equals("getBranchInstallParams")) {
                            JSONObject installParams = Branch.getInstance().getFirstReferringParams();
                            String installParmString = installParams.toString();
                            result.success(installParmString);
                        }
                        if (methodCall.method.equals("getLatestReferringParms")) {
                            JSONObject installParams = Branch.getInstance().getLatestReferringParams();
                            String installParmString = installParams.toString();
                            result.success(installParmString);
                        }
                    }
                });

    }

    public String getInstallReferringLink(Activity activity) {
        String installReferring_link = ((MyApplicationClass) activity.getApplication()).getInstallReferring_link();
        return installReferring_link;
    }

    public void trackAndSetBranchUserIdentity(String userId) {
        Branch.getInstance().setIdentity(userId);
    }

    public void branchUserLogout() {
        Branch.getInstance().logout();
    }


    public String branchLifecycleEventSigniup(Map<String, Object> arguments, Context context) {
        BranchEvent be = new BranchEvent(BRANCH_STANDARD_EVENT.COMPLETE_REGISTRATION)
                .setTransactionID("" + arguments.get("transactionID")).setDescription("HOWZAT SIGN UP");
        HashMap<String, Object> data = new HashMap();
        data = (HashMap) arguments.get("data");

        for (Map.Entry<String, Object> entry : data.entrySet()) {

            be.addCustomDataProperty((String) entry.getKey(), "" + entry.getValue());
        }
        be.logEvent(context);
        return "Sign Up Track event added";

    }

    public String branchEventTransactionFailed(Map<String, Object> arguments, Context context) {
        boolean isfirstDepositor = false;
        String eventName = "FIRST_DEPOSIT_FAILED";
        isfirstDepositor = Boolean.parseBoolean("" + arguments.get("firstDepositor"));
        if (!isfirstDepositor) {
            eventName = "REPEAT_DEPOSIT_FAILED";
        }
        BranchEvent be = new BranchEvent(eventName).setTransactionID("" + arguments.get("txnId"))
                .setDescription(("HOWZAT DEPOSIT FAILED"));
        be.addCustomDataProperty("txnTime", "" + arguments.get("txnTime"));
        be.addCustomDataProperty("txnDate", "" + arguments.get("txnDate"));
        be.addCustomDataProperty("appPage", "" + arguments.get("appPage"));

        HashMap<String, Object> data = new HashMap();
        data = (HashMap) arguments.get("data");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            be.addCustomDataProperty(entry.getKey(), "" + entry.getValue());
        }
        be.logEvent(context);

        return " Add Cash Failed Event added";

    }

    public String branchEventAddCustomEvent(Map<String, Object> arguments, Context context) {
        String eventName =  (String) arguments.get("eventName");
        BranchEvent be = new BranchEvent(eventName);
        HashMap<String, Object> data = new HashMap();
        data = (HashMap) arguments.get("data");
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            be.addCustomDataProperty(entry.getKey(), "" + entry.getValue());
        }
        be.logEvent(context);
        return eventName+"event added";
    }

    public String branchEventTransactionSuccess(Map<String, Object> arguments, Context context) {
        boolean isfirstDepositor = false;
        String eventName = "FIRST_DEPOSIT_SUCCESS";
        isfirstDepositor = Boolean.parseBoolean("" + arguments.get("firstDepositor"));
        if (!isfirstDepositor) {
            eventName = "REPEAT_DEPOSIT_SUCCESS";
        }

        BranchEvent be = new BranchEvent(eventName).setTransactionID("" + arguments.get("txnId"))
                .setDescription("HOWZAT DEPOSIT FAILED");
        be.addCustomDataProperty("txnTime", "" + arguments.get("txnTime"));
        be.addCustomDataProperty("txnDate", "" + arguments.get("txnDate"));
        be.addCustomDataProperty("appPage", "" + arguments.get("appPage"));

        HashMap<String, Object> data = new HashMap();
        data = (HashMap) arguments.get("data");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            be.addCustomDataProperty(entry.getKey(), "" + entry.getValue());
        }

        be.logEvent(context);
        return " Add Cash Success Event added";

    }

    private String getGoogleAddId(Activity activity) {
        String googleAdId = ((MyApplicationClass) activity.getApplication()).getGoogleAdId();
        return googleAdId;
    }

}
