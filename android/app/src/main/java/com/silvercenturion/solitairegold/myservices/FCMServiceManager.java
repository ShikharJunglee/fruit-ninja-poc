package com.silvercenturion.solitairegold.myservices;


import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.silvercenturion.solitairegold.MyApplicationClass;
import com.silvercenturion.solitairegold.myconstants.ServiceConstants;
import com.webengage.sdk.android.WebEngage;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class FCMServiceManager {
    private FlutterView flutterView;
    private static final String PF_FCM_CHANNEL = ServiceConstants.FCM_SERVICE_MNAME;

    public FCMServiceManager(FlutterView flutterView, Activity activity, Context applicationContext) {
        this.flutterView = flutterView;
        initFlutterChannels(flutterView, activity, applicationContext);
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context applicationContext) {
        new MethodChannel(flutterView, PF_FCM_CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                if (methodCall.method.equals("_getFirebaseToken")) {
                    String firebaseToken = getFireBaseToken(activity);
                    result.success(firebaseToken);
                } else if (methodCall.method.equals("_subscribeToFirebaseTopic")) {
                    String fcmTopic = methodCall.arguments();
                    subscribeToFirebaseTopic(fcmTopic);
                    result.success("Subscribed to PF fcm topic" + fcmTopic);
                } else {
                    result.notImplemented();
                }

            }
        });
    }


    private String getFireBaseToken(Activity activity) {
        String firebaseToken = ((MyApplicationClass) activity.getApplication()).getFirebaseFCMToken();
        if (firebaseToken != null && firebaseToken.length() > 0) {
            return firebaseToken;
        } else {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                return;
                            }
                            String token = task.getResult().getToken();
                            WebEngageService.setFCMKeyForWebEngage(token);
                            ((MyApplicationClass) activity.getApplication()).setFirebaseFCMToken(token);
                            WebEngage.get().setRegistrationID(token);
                        }
                    });

            return ((MyApplicationClass) activity.getApplication()).getFirebaseFCMToken();
        }


    }

    private void subscribeToFirebaseTopic(String topicName) {
        try {
            FirebaseMessaging.getInstance().subscribeToTopic(topicName);
        } catch (Exception e) {
        }
    }

}
