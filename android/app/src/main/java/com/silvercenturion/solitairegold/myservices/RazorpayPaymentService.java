package com.silvercenturion.solitairegold.myservices;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.WebView;

import com.silvercenturion.solitairegold.myactivitys.RazorpayCustomeActivity;
import com.silvercenturion.solitairegold.mycommonutilcollection.MyHelperClass;
import com.razorpay.ApplicationDetails;
import com.razorpay.Razorpay;
import com.razorpay.RzpUpiSupportedAppsCallback;
import com.razorpay.ValidateVpaCallback;
import com.silvercenturion.solitairegold.myconstants.ServiceConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class RazorpayPaymentService {

    private static final String RAZORPAY_IO_CHANNEL = ServiceConstants.RAZORPAY_SERVICE_MNAME;
    private FlutterView flutterView;
    private MethodChannel.Result razorpayPaymentResult;
    private MethodChannel.Result razorpayGetPaymentMethodsResult;
    Razorpay razorpay;
    private WebView webview;
    public static final int RAZORPAY_CUSTOME_REQUEST_CODE = 5690812;

    public RazorpayPaymentService(FlutterView flutterView, Activity activity, Context applicationContext) {
        this.flutterView = flutterView;
        initFlutterChannels(flutterView, activity);
    }


    protected void initFlutterChannels(FlutterView flutterView, Activity activity) {
        new MethodChannel(flutterView, RAZORPAY_IO_CHANNEL)
                .setMethodCallHandler(new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        if (methodCall.method.equals("_openRazorpayNative")) {
                            HashMap<String, Object> arguments = methodCall.arguments();
                            Intent intent = new Intent(activity, RazorpayCustomeActivity.class);
                            intent.putExtra("arguments", (Serializable) arguments);
                            activity.startActivityForResult(intent, RAZORPAY_CUSTOME_REQUEST_CODE);
                            razorpayPaymentResult = result;
                        } else if (methodCall.method.equals("getPaymentMethodData")) {
                            razorpayGetPaymentMethodsResult=result;
                            HashMap<String, Object> arguments = methodCall.arguments();
                            String paymentMethod = (String) arguments.get("paymentMethod");
                            HashMap razorpayPaymentMethodsData = new HashMap<>();
                            razorpayPaymentMethodsData.put("razorpayRegId", (String) arguments.get("razorpayRegId"));
                            razorpayPaymentMethodsData.put("paymentMethod", (String) arguments.get("paymentMethod"));
                            razorpay = new Razorpay(activity,(String) arguments.get("razorpayRegId"));

                            if(paymentMethod.equals("upi")){
                                HashMap installedUPIAppsDataMap = new HashMap<>();
                                Razorpay.getAppsWhichSupportUpi(activity, new RzpUpiSupportedAppsCallback() {
                                    @Override
                                    public void onReceiveUpiSupportedApps(List<ApplicationDetails> list) {
                                        /* List of upi supported app*/
                                        for (int i = 0; i < list.size(); i++) {
                                            HashMap appData = new HashMap<>();
                                            appData.put("packagename", list.get(i).getPackageName().toString());
                                            final String pureBase64Encoded = list.get(i).getIconBase64().substring(list.get(i).getIconBase64().indexOf(",") + 1);
                                            appData.put("icondata", pureBase64Encoded);
                                            installedUPIAppsDataMap.put(list.get(i).getAppName().toString(), appData);
                                        }
                                        razorpayPaymentMethodsData.put("installedUPIAppsDataMap", installedUPIAppsDataMap);
                                        razorpayGetPaymentMethodsResult.success(razorpayPaymentMethodsData);
                                    }
                                });
                            }else{
                                razorpay.getPaymentMethods(new Razorpay.PaymentMethodsCallback() {
                                    @Override
                                    public void onPaymentMethodsReceived(String result) {
                                        razorpayPaymentMethodsData.put("razorpayPaymentMethodsData", result);
                                        try {
                                            JSONObject jsonObject = new JSONObject(result);
                                            if (paymentMethod.equals("wallet")) {
                                                Map<String, String> razorpayWalletDataMap = new HashMap<>();
                                                JSONObject walletJsonObject = jsonObject.getJSONObject("wallet");
                                                Map<String, Object> walletJsonMap = MyHelperClass.jsonToMap(walletJsonObject);
                                                for (String key : walletJsonMap.keySet()) {
                                                    if ((boolean) walletJsonMap.get(key)) {
                                                        razorpayWalletDataMap.put(key, razorpay.getWalletLogoUrl(key));
                                                    }
                                                }
                                                razorpayPaymentMethodsData.put("razorpayWalletDataMap", razorpayWalletDataMap);
                                                onPaymentMethodsSuccess(razorpayPaymentMethodsData);
                                            } else if (paymentMethod.equals("netbanking")) {
                                                Map<String, Object> razorpayNetbankingDataMap = new HashMap<>();
                                                JSONObject netbankingJsonObject = jsonObject.getJSONObject("netbanking");
                                                Map<String, Object> netbankingJsonMap = MyHelperClass.jsonToMap(netbankingJsonObject);
                                                for (String key : netbankingJsonMap.keySet()) {
                                                    HashMap bankInfo = new HashMap<>();
                                                    bankInfo.put("logoURL", razorpay.getBankLogoUrl(key));
                                                    bankInfo.put("label", netbankingJsonMap.get(key));
                                                    razorpayNetbankingDataMap.put(key, bankInfo);
                                                }
                                                razorpayPaymentMethodsData.put("razorpayNetbankingDataMap", razorpayNetbankingDataMap);
                                                onPaymentMethodsSuccess(razorpayPaymentMethodsData);
                                            }
                                        } catch (JSONException err) {
                                            Log.d("Error", err.toString());
                                            onPaymentMethodsSuccess(razorpayPaymentMethodsData);
                                        }
                                    }
                                    @Override
                                    public void onError(String error) {
                                        onPaymentMethodsSuccess(razorpayPaymentMethodsData);
                                    }
                                });}
                        } else if (methodCall.method.equals("isValidVpa")) {
                            HashMap<String, Object> arguments = methodCall.arguments();
                            razorpay = new Razorpay(activity,(String) arguments.get("razorpayRegId"));
                            String vpa = (String) arguments.get("vpa");

                            razorpay.isValidVpa(vpa, new ValidateVpaCallback() {
                                @Override
                                public void onResponse(boolean b) {
                                    result.success(b);
                                }

                                @Override
                                public void onFailure() {
                                    result.success(false);
                                }
                            });
                        } else {
                            result.notImplemented();
                        }
                    }
                });
    }

    private void onPaymentMethodsSuccess(HashMap razorpayPaymentMethodsData){
        if(this.razorpayGetPaymentMethodsResult!=null){
            this.razorpayGetPaymentMethodsResult.success(razorpayPaymentMethodsData);
            this.razorpayGetPaymentMethodsResult = null;
        }
    }

    public void onRazoraPayActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RazorpayPaymentService.RAZORPAY_CUSTOME_REQUEST_CODE) {
            if (resultCode == 1) {
                Map<String,Object>  paymentData = (Map) data.getSerializableExtra("paymentResultData");
                onRazorPayPaymentSuccess(paymentData);
            } else {
                String errorMessage = (String) data.getSerializableExtra("paymentResultData");
                onRazorPayPaymentFail(errorMessage);
            }
        }

    }

    public void onRazorPayPaymentFail(String errorMsgData) {
        Map<String, Object> result = new HashMap<>();
        result.put("status", "failed");
        result.put("data", "Payment failed");
        razorpayPaymentResult.success(result);
    }

    public void onRazorPayPaymentSuccess(Map<String,Object> data) {
        Map<String, Object> result = new HashMap<>();
        result.put("status", "success");
        result.put("data", data);
        razorpayPaymentResult.success(result);
    }

}
