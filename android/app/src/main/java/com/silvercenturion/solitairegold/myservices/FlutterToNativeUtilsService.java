package com.silvercenturion.solitairegold.myservices;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import com.silvercenturion.solitairegold.MainActivity;
import com.silvercenturion.solitairegold.MyApplicationClass;
import com.silvercenturion.solitairegold.mycommonutilcollection.MyDeviceInfo;
import com.silvercenturion.solitairegold.myconstants.MyConstants;
import com.silvercenturion.solitairegold.myconstants.ServiceConstants;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class FlutterToNativeUtilsService {
    private static final String UTILS_CHANNEL = ServiceConstants.UTILS_SERVICE_MNAME;
     private MethodChannel.Result advertisingIdResult;


    public FlutterToNativeUtilsService(FlutterView flutterView, Activity activity, Context applicationContext) {
        initFlutterChannels(flutterView, activity, applicationContext);
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context context) {

        new MethodChannel(flutterView, UTILS_CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                if (methodCall.method.equals("deleteInternalStorageFile")) {
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {
                            String filename = methodCall.arguments();
                            try {
                                deleteIfFileExist(filename);
                            } catch (Exception e) {
                            }
                            try {
                                File internalFileDirectory = new File(
                                        Environment.getExternalStorageDirectory().getPath());
                                deleteFileRecursive(internalFileDirectory, filename);
                            } catch (Exception e) {
                                System.out.print(e);
                            }
                        }
                    });
                } else if (methodCall.method.equals("onUserInfoRefreshed")) {
                    Map<String, Object> arguments = methodCall.arguments();
                    MainActivity.onHowzatUserInfoRefreshed(arguments,activity);
                }
                else if (methodCall.method.equals("getAdvertisingId")) {
                    advertisingIdResult = result;
                    getGoogleAddId(activity);
                }
                else if (methodCall.method.equals("_getGoogleAddId")) {
                        String googleAddId = (String) getGoogleAddId(activity);
                        result.success(googleAddId);

                } else if (methodCall.method.equals("_deepLinkingRoutingHandler")) {
                    HashMap<String, Object> data = ((MyApplicationClass) activity.getApplication()).getDeepLinkingDataObject();
                    result.success(data);
                    DeepLinkingService.setDeepLinkingDataObjectToDefault(activity);
                } else if(methodCall.method.equals("getAppLaunchSource")){
                     String getAppLaunchSource=getAppLaunchSource(activity);
                     result.success(getAppLaunchSource);
                }else {
                    result.notImplemented();
                }
            }
        });
    }


    public boolean deleteIfFileExist(String fname) {
        boolean deleted = false;
        try {
            File applictionFile = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fname);
            if (applictionFile != null && applictionFile.exists()) {
                deleted = applictionFile.delete();
            }
        } catch (Exception e) {

        }
        return deleted;
    }

    private void deleteFileRecursive(File fileOrDirectory, String fileName) {
        try {
            if (fileOrDirectory.isDirectory()) {
                if (fileOrDirectory.exists()) {
                    for (File child : fileOrDirectory.listFiles()) {
                        deleteFileRecursive(child, fileName);
                    }
                }
            }
        } catch (Exception e) {
            System.out.print(e);
        }
        try {
            if (fileOrDirectory.isFile()) {
                if (fileOrDirectory.getName().endsWith(fileName)) {
                    Boolean deleted = fileOrDirectory.delete();
                }
            }
        } catch (Exception e) {

        }
    }
     private String getGoogleAddId(Activity activity) {
        String googleAdId = ((MyApplicationClass) activity.getApplication()).getGoogleAdId();
        if(googleAdId!=null && googleAdId.length()>3){
            advertisingIdResult.success(googleAdId);
        }else{
            MyDeviceInfo myDeviceInfo = new MyDeviceInfo();
            myDeviceInfo.setGoogleAdvertisingID(activity);
            String id = ((MyApplicationClass) activity.getApplication()).getGoogleAdId();
            advertisingIdResult.success(id);
        }
        return googleAdId;
    }

    private Map<String, Object> getDeviceInfo(Context context, Activity activity) {
        Map<String, Object> params = new HashMap<>();
        Map<String, String> emailList = new HashMap();
        final MyDeviceInfo deviceData = new MyDeviceInfo();
        final Map<String, String> deviceInfoList = deviceData.getDeviceInfoMap(activity);
        try {
            params.put("uid", deviceInfoList.get("device_ID"));
            params.put("model", deviceInfoList.get("model"));
            params.put("serial", deviceInfoList.get("android_Id"));
            params.put("manufacturer", deviceInfoList.get("manufacturer"));
            params.put("version", deviceInfoList.get("android_version"));
            params.put("network_operator", deviceInfoList.get("network_Operator"));
            params.put("packageName", deviceInfoList.get("packageName"));
            params.put("versionName", deviceInfoList.get("versionName"));
            params.put("baseRevisionCode", deviceInfoList.get("baseRevisionCode"));
            params.put("firstInstallTime", deviceInfoList.get("firstInstallTime"));
            params.put("lastUpdateTime", deviceInfoList.get("lastUpdateTime"));
            params.put("device_ip_", deviceInfoList.get("device_IPv4"));
            params.put("network_type", deviceData.getConnectionType(activity));
            String googleAddId = getGoogleAddId(activity);
            params.put("googleAdId", googleAddId);
            List email = deviceData.getGoogleEmailList(activity);
            int emailIndex = 1;
            for (Object s : email) {
                emailList.put("googleEmail" + emailIndex, s.toString());
                emailIndex++;
            }
            params.put("googleEmailList", emailList);
        } catch (Exception e) {

        }
        return params;
    }

    public static void setAppLaunchSource(Activity activity,String value){
        try{
            ((MyApplicationClass) activity.getApplication()).setAppLaunchSource(value);
        }catch(Exception e){

        }
    }
    public static String getAppLaunchSource(Activity activity){
        try{
            return ((MyApplicationClass) activity.getApplication()).getAppLaunchSource();
        }catch(Exception e){
            return MyConstants.APP_LAUNCH_SOURCE_APP_ICON;
        }
    }

}
