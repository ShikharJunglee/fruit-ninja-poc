package com.silvercenturion.solitairegold.myservices;



import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.paytm.pgsdk.TransactionManager;


import java.util.HashMap;
import java.util.Map;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class PaytmNativeService {

    private static final String PAYTM_NATIVE_CHANNEL = "com.silvercenturion.paytmnativechannel";
    private FlutterView flutterView;
    private Activity current_activity;
    public static final int PAYTM_REQUEST_CODE = 56908;
    private MethodChannel.Result paytmPaymentResult;


    public PaytmNativeService(FlutterView flutterView, Activity activity, Context applicationContext) {
        this.flutterView = flutterView;
        this.current_activity=activity;
        initFlutterChannels(flutterView, activity, applicationContext);
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context applicationContext) {
        new MethodChannel(flutterView, PAYTM_NATIVE_CHANNEL)
                .setMethodCallHandler(new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        if (methodCall.method.equals("_openPaytmNativePayment")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            openPaytmNativePayment(arguments,activity,applicationContext);
                            paytmPaymentResult=result;
                        } else if(methodCall.method.equals("isPaytmAppInstalled")) {
                            PackageManager pm = applicationContext.getPackageManager();
                            try {
                                pm.getPackageInfo("net.one97.paytm", PackageManager.GET_ACTIVITIES);
                                result.success("true");
                            } catch (PackageManager.NameNotFoundException var4) {
                                result.success("false");
                            }
                        }
                        else {
                            result.notImplemented();
                        }
                    }
                });

    }

    private void openPaytmNativePayment(Map<String, Object> arguments,Activity activity,Context applicationContext){
        String orderid=(String)arguments.get("orderid");
        String mid=(String)arguments.get("mid");
        String txyToken=(String) arguments.get("txyToken");
        String amount=(String) arguments.get("amount");
        String callbackurlHowzat=(String) arguments.get("callbackurl");
        String callbackurlStage="https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID="+orderid;
        String callbackurlProd="https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderid;
        String checkSumHash=(String) arguments.get("checkSumHash");
        String customerID=(String) arguments.get("customerID");
        String industrialTypeID=(String) arguments.get("industrialTypeID");
        String website=(String) arguments.get("website");


        PaytmOrder paytmOrder = new PaytmOrder(orderid, mid, txyToken, amount, callbackurlProd);

        TransactionManager transactionManager = new TransactionManager(paytmOrder, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionResponse(Bundle bundle) {
                System.out.println(bundle);
                // onPaytmPaymentSuccess(bundle);
            }
            @Override
            public void networkNotAvailable() {
                System.out.println("");
                onPaytmNativePaymentFail();
            }

            @Override
            public void clientAuthenticationFailed(String s) {
                System.out.println(s);
                onPaytmNativePaymentFail();
            }
            @Override
            public void someUIErrorOccurred(String s) {
                onPaytmNativePaymentFail();
            }
            @Override
            public void onErrorLoadingWebPage(int i, String s, String s1) {
                onPaytmNativePaymentFail();
            }
            @Override
            public void onBackPressedCancelTransaction() {
                System.out.println("");
                onPaytmNativePaymentFail();
            }
            @Override
            public void onTransactionCancel(String s, Bundle bundle) {
                onPaytmNativePaymentFail();
            }
        });
        try{
            transactionManager.startTransaction(activity, PAYTM_REQUEST_CODE);
        }catch(Exception e){
            System.out.println(e);
        }

    }

    public void onPaytmNativeActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode !=0){
            onPaytmPaymentSuccess(data.getStringExtra("response").toString());
        }else{
            onPaytmNativePaymentFail();
        }
    }

    private void  onPaytmPaymentSuccess(String  responseData){
        Map<String,Object> result=new HashMap<>();
        result.put("status","success");
        result.put("data",responseData);
        paytmPaymentResult.success(result);
    }

    public void onPaytmNativePaymentFail() {
        Map<String,Object> result=new HashMap<>();
        result.put("status","failed");
        result.put("data","Payment Payment failed");
        paytmPaymentResult.success(result);
    }

}
