package com.silvercenturion.solitairegold.myservices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.silvercenturion.solitairegold.myconstants.ServiceConstants;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class ShareMessageService {

    private static final String SOCIAL_SHARE_CHANNEL = ServiceConstants.SOCIALSHARE_SERVICE_MNAME;
    private FlutterView flutterView;


    public ShareMessageService(FlutterView flutterView, Activity activity, Context applicationContext) {
        this.flutterView = flutterView;
        initFlutterChannels(flutterView, activity, applicationContext);
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context applicationContext) {

        new MethodChannel(flutterView, SOCIAL_SHARE_CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                // Log.d("flutter", "on share via whatsapp");
                if (methodCall.method.equals("initSocialShareChannel")) {
                    result.success("Social Share init success");
                } else if (methodCall.method.equals("shareText")) {
                    String message = methodCall.arguments();
                    inviteFriend(message, activity);
                    result.success("Social Share init success");
                } else if (methodCall.method.equals("shareViaFacebook")) {
                    String message = methodCall.arguments();
                    inviteFriendViaFacebook(message, activity);
                    result.success("Social Share init success");
                } else if (methodCall.method.equals("shareViaWhatsApp")) {
                    String message = methodCall.arguments();
                    inviteFriendViaWhatsapp(message, activity);
                    result.success("Social Share init success");
                } else if (methodCall.method.equals("shareViaTelegram")) {
                    String message = methodCall.arguments();
                    inviteFriendViaTelegram(message, activity);
                    result.success("Social Share init success");
                } else if (methodCall.method.equals("shareViaGmail")) {
                    String message = methodCall.arguments();
                    inviteFriendViaGmail(message, activity);
                    result.success("Social Share init success");
                } else {
                    result.success("Social Share init success");
                }
            }
        });
    }

    public void inviteFriend(String message, Activity activity) {
        String shareMessage = message;
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            activity.startActivity(Intent.createChooser(shareIntent, "Share to"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public void inviteFriendViaWhatsapp(String message, Activity activity) {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message);
        try {
            activity.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            inviteFriend(message, activity);
        }
    }

    public void inviteFriendViaFacebook(String message, Activity activity) {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.facebook.katana");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message);
        try {
            activity.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            inviteFriend(message, activity);
        }
    }

    public void inviteFriendViaTelegram(String message, Activity activity) {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("org.telegram.messenger");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, message);
        try {
            activity.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            inviteFriend(message, activity);
        }
    }

    public void inviteFriendViaGmail(String message, Activity activity) {
        String shareMessage = message;
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.setPackage("com.google.android.gm");
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            activity.startActivity(Intent.createChooser(shareIntent, "Share to"));
        } catch (Exception e) {
            inviteFriend(message, activity);
        }
    }

}
