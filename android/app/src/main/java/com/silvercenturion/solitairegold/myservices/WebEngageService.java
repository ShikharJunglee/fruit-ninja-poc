package com.silvercenturion.solitairegold.myservices;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import com.google.gson.Gson;
import com.silvercenturion.solitairegold.MyApplicationClass;
import com.silvercenturion.solitairegold.R;
import com.silvercenturion.solitairegold.myconstants.ServiceConstants;
import com.silvercenturion.solitairegold.mydatamodels.DeepLinkingDataModel;
import com.silvercenturion.solitairegold.myconstants.MyConstants;
import com.silvercenturion.solitairegold.mycommonutilcollection.MyHelperClass;
import com.webengage.sdk.android.Analytics;
import com.webengage.sdk.android.User;
import com.webengage.sdk.android.WebEngage;
import com.webengage.sdk.android.WebEngageActivityLifeCycleCallbacks;
import com.webengage.sdk.android.WebEngageConfig;
import com.webengage.sdk.android.actions.render.InAppNotificationData;
import com.webengage.sdk.android.actions.render.PushNotificationData;
import com.webengage.sdk.android.utils.Gender;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class WebEngageService {

    private Analytics weAnalytics;
    private User weUser;
    private static final String WEBENGAGE_CHANNEL = ServiceConstants.WEBENGAGE_SERVICE_MNAME;
    private FlutterView flutterView;
    private Activity activity;


    public WebEngageService(FlutterView flutterView, Activity activity, Context applicationContext) {
        this.flutterView = flutterView;
        initFlutterChannels(flutterView, activity, applicationContext);
        this.activity = activity;
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context applicationContext) {
        new MethodChannel(flutterView, WEBENGAGE_CHANNEL)
                .setMethodCallHandler(new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {

                        if (methodCall.method.equals("webEngageEventSigniup")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = webEngageEventSigniup(arguments,activity);
                            result.success(channelResult);
                        }

                        if (methodCall.method.equals("webEngageEventLogin")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = webEngageEventLogin(arguments,activity);
                            result.success(channelResult);
                        }

                        if (methodCall.method.equals("webEngageTransactionFailed")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = webEngageTransactionFailed(arguments);
                            result.success(channelResult);
                        }
                        if (methodCall.method.equals("webEngageTransactionSuccess")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = webEngageTransactionSuccess(arguments);
                            result.success(channelResult);
                        } else if (methodCall.method.equals("webengageTrackUser")) {
                            Map<String, String> arguments = methodCall.arguments();
                            String channelResult = webengageTrackUser(arguments,activity);
                            result.success(channelResult);
                        } else if (methodCall.method.equals("webengageCustomAttributeTrackUser")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = webengageCustomAttributeTrackUser(arguments);
                            result.success(channelResult);
                        } else if (methodCall.method.equals("webengageTrackEvent")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = webengageTrackEvent(arguments);
                            result.success(channelResult);
                        } else if (methodCall.method.equals("trackEventsWithAttributes")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = trackEventsWithAttributes(arguments);
                            result.success(channelResult);
                        } else if (methodCall.method.equals("webengageAddScreenData")) {
                            Map<String, Object> arguments = methodCall.arguments();
                            String channelResult = webengageAddScreenData(arguments);
                            result.success(channelResult);
                        } else {
                            result.notImplemented();
                        }

                    }
                });

    }

    public void initWebEngage(Activity activity) {
        WebEngageConfig webEngageConfig = new WebEngageConfig.Builder().setWebEngageKey(MyConstants.getWebEngageKey(activity))
                 .setPushSmallIcon(R.color.fcm_icon_bg)
                .setPushAccentColor(activity.getResources().getColor((R.color.fcm_icon_bg)))
                .build();
        activity.getApplication()
                .registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(activity, webEngageConfig));
        FlutterApplication flutterApplication = new FlutterApplication();
        MyApplicationClass myApplicationClass = new MyApplicationClass();
        flutterApplication.registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(flutterApplication, webEngageConfig));
        myApplicationClass.registerActivityLifecycleCallbacks(new WebEngageActivityLifeCycleCallbacks(myApplicationClass, webEngageConfig));
        weAnalytics = WebEngage.get().analytics();
        weUser = WebEngage.get().user();
    }

    /**************** Web Engage Stuff ***************/
    public String webengageTrackUser(Map<String, String> arguments,Activity activity) {
        String trackType = arguments.get("trackingType");
        switch (trackType) {
            case "login":
                weUser.login(arguments.get("value"));
                return "Login Track added";
            case "logout":
                weUser.logout();
                return "Logout To Tracking event done";
            case "setEmail":
                String hashedEmail = MyHelperClass.doSHA256Encoding(arguments.get("value"),activity);
                weUser.setHashedEmail(hashedEmail);
                return "Email track   added";
            case "setBirthDate":
                weUser.setBirthDate(arguments.get("value"));
                return "Birth Day track added";
            case "setPhoneNumber":
                String hashedPhone = MyHelperClass.doSHA256Encoding(arguments.get("value"),activity);
                weUser.setHashedPhoneNumber(hashedPhone);
                return "Phone Number track added";
            case "setFirstName":
                weUser.setFirstName(arguments.get("value"));
                return "Login Track added";
            case "setGender":
                String gendel = arguments.get("value");
                if (gendel == "male") {
                    weUser.setGender(Gender.MALE);
                } else if (gendel == "female") {
                    weUser.setGender(Gender.FEMALE);
                } else {
                    weUser.setGender(Gender.OTHER);
                }
                return "User Gender  added";
            case "setLastName":
                weUser.setLastName(arguments.get("value"));
                return "Last Name  Track added";
            default:
                return "No Such tracking type found ";
        }
    }

    public String webengageCustomAttributeTrackUser(Map<String, Object> arguments) {
        String trackType = (String) arguments.get("trackingType");
        String value = (String) arguments.get("value");
        weUser.setAttribute(trackType, value);
        return "User " + trackType + " tracking added";
    }

    public String webengageTrackEvent(Map<String, Object> arguments) {
        /* Track Event without any Attributes */
        String eventName = "" + (String) arguments.get("eventName");
        boolean priority = true;
        weAnalytics.track(eventName, new Analytics.Options().setHighReportingPriority(priority));
        return "Event " + eventName + "" + " added";
    }

    public String trackEventsWithAttributes(Map<String, Object> arguments) {
        /* Track Event with Attributes */
        String eventName = (String) arguments.get("eventName");
        boolean priority = true;
        Map<String, Object> addedAttributes = new HashMap<>();
        addedAttributes = (Map) arguments.get("data");
        weAnalytics.track(eventName, addedAttributes, new Analytics.Options().setHighReportingPriority(priority));
        return "Event " + eventName + "" + "added";
    }

    public String webengageAddScreenData(Map<String, Object> arguments) {
        String screenName = (String) arguments.get("screenName");
        Map<String, Object> addedAttributes = new HashMap<>();
        addedAttributes = (Map) arguments.get("data");
        weAnalytics.screenNavigated(screenName,addedAttributes);
        return "Screen Data added for the screen " + screenName;
    }

    public String webEngageEventSigniup(Map<String, Object> arguments,Activity activity) {
        Map<String, Object> addCustomDataProperty = new HashMap<>();
        HashMap<String, Object> data = new HashMap();
        String encodedEmail = "";
        String encodedPhone = "";
        if((String) arguments.get("email") != null &&  ((String) arguments.get("email")).length()>3)
        {
            encodedEmail=MyHelperClass.doSHA256Encoding((String) arguments.get("email"),activity);
        }
        if((String) arguments.get("mobile") !=null && ((String) arguments.get("mobile")).length()>3){
            encodedPhone=MyHelperClass.doSHA256Encoding((String) arguments.get("mobile"),activity);;
        }
        data = (HashMap) arguments.get("data");
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            addCustomDataProperty.put(entry.getKey(), "" + entry.getValue());
        }
        weAnalytics.track("COMPLETE_REGISTRATION", addCustomDataProperty);
        if (encodedEmail != null && encodedEmail.length() > 3) {
            weUser.setHashedEmail(encodedEmail);

        }
        if (encodedPhone != null && encodedPhone.length() > 3) {
            weUser.setHashedPhoneNumber(encodedPhone);
        }
        weUser.setAttribute("loginType", (String) arguments.get("chosenloginTypeByUser"));
        return "Web engage Sign Up Track event added";
    }

    public String webEngageEventLogin(Map<String, Object> arguments,Activity activity) {
        Map<String, Object> addCustomDataProperty = new HashMap<>();
        HashMap<String, Object> data = new HashMap();
        String encodedEmail = "";
        String encodedPhone = "";
        data = (HashMap) arguments.get("data");
        if((String) arguments.get("email") != null &&  ((String) arguments.get("email")).length()>3)
        {
            encodedEmail=MyHelperClass.doSHA256Encoding((String) arguments.get("email"),activity);
        }
        if((String) arguments.get("mobile") !=null && ((String) arguments.get("mobile")).length()>3){
            encodedPhone=MyHelperClass.doSHA256Encoding((String) arguments.get("mobile"),activity);;
        }

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            addCustomDataProperty.put(entry.getKey(), "" + entry.getValue());
        }
        weAnalytics.track("COMPLETE_LOGIN", addCustomDataProperty);
        weUser.setAttribute("loginType", "" + (String) arguments.get("loginType"));

        if (encodedEmail != null && encodedEmail.length() > 3) {
            weUser.setHashedEmail(encodedEmail);
        }
        if (encodedPhone != null && encodedPhone.length() > 3) {
            weUser.setHashedPhoneNumber(encodedPhone);
        }
        if (data.get("first_name") != null) {
            weUser.setFirstName((String) data.get("first_name"));
        }
        if (data.get("last_name") != null) {
            weUser.setLastName((String) data.get("last_name"));
        }
        weUser.setAttribute("loginType", (String) arguments.get("chosenloginTypeByUser"));
        return "Web engage Login Track event added";
    }

    public String webEngageTransactionFailed(Map<String, Object> arguments) {

        boolean isfirstDepositor = false;
        String eventName = "FIRST_DEPOSIT_FAILED";
        isfirstDepositor = Boolean.parseBoolean("" + arguments.get("firstDepositor"));
        if (!isfirstDepositor) {
            eventName = "REPEAT_DEPOSIT_FAILED";
        }

        Map<String, Object> addCustomDataProperty = new HashMap<>();
        HashMap<String, Object> data = new HashMap();
        data = (HashMap) arguments.get("data");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            addCustomDataProperty.put(entry.getKey(), "" + entry.getValue());
        }
        weAnalytics.track(eventName, addCustomDataProperty);
        return " Add Cash Failed Event added";

    }

    public String webEngageTransactionSuccess(Map<String, Object> arguments) {
        boolean isfirstDepositor = false;
        String eventName = "DEPOSIT_SUCCESS";
        isfirstDepositor = Boolean.parseBoolean("" + arguments.get("firstDepositor"));
        if (!isfirstDepositor) {
            eventName = "DEPOSIT_SUCCESS";
        }
        Map<String, Object> addCustomDataProperty = new HashMap<>();
        HashMap<String, Object> data = new HashMap();
        data = (HashMap) arguments.get("data");

        for (Map.Entry<String, Object> entry : data.entrySet()) {
            addCustomDataProperty.put(entry.getKey(), "" + entry.getValue());
        }
        weAnalytics.track(eventName, addCustomDataProperty);
        return " Add Cash Success Event added";
    }


    public String onUserInfoRefreshed(Map<String, Object> arguments,Activity activity) {
        if (arguments.get("first_name") != null && ((String) arguments.get("first_name")).length() > 0) {
            weUser.setFirstName((String) arguments.get("first_name"));
            weUser.setAttribute("first_name", (String) arguments.get("first_name"));
        }
        if (arguments.get("email") != null && ((String) arguments.get("email")).length() > 0) {
            String email = MyHelperClass.doSHA256Encoding((String) arguments.get("email"),activity);
            weUser.setHashedEmail(email);
        }
        if (arguments.get("mobile") != null && ((String) arguments.get("mobile")).length() > 0) {
            String phone = MyHelperClass.doSHA256Encoding((String) arguments.get("mobile"),activity);
            weUser.setHashedPhoneNumber(phone);
        }
        if (arguments.get("lastName") != null && ((String) arguments.get("lastName")).length() > 0) {
            weUser.setLastName((String) arguments.get("lastName"));
            weUser.setAttribute("last_name", (String) arguments.get("lastName"));
        }
        if (arguments.get("login_name") != null && ((String) arguments.get("login_name")).length() > 0) {
            weUser.setAttribute("userName", (String) arguments.get("login_name"));
        }
        if (arguments.get("channelId") != null && ((String) arguments.get("channelId")).length() > 0) {
            weUser.setAttribute("channelId", (String) arguments.get("channelId"));
        }
        if (arguments.get("withdrawable") != null && (arguments.get("withdrawable")).toString().length() > 0) {
            double withdrawable = new Double(arguments.get("withdrawable").toString());
            weUser.setAttribute("withdrawable", Math.round(withdrawable * 100.0) / 100.0);
        }
        if (arguments.get("depositBucket") != null && (arguments.get("depositBucket")).toString().length() > 0) {
            double depositBucket = new Double(arguments.get("depositBucket").toString());
            weUser.setAttribute("depositBucket", Math.round(depositBucket * 100.0) / 100.0);
        }
        if (arguments.get("balance") != null && (arguments.get("balance")).toString().length() > 0) {
            double depositBucket = new Double(arguments.get("balance").toString());
            weUser.setAttribute("balance", Math.round(depositBucket * 100.0) / 100.0);
        }
        if (arguments.get("nonWithdrawable") != null && (arguments.get("nonWithdrawable")).toString().length() > 0) {
            double nonWithdrawable = new Double(arguments.get("nonWithdrawable").toString());
            weUser.setAttribute("nonWithdrawable", Math.round(nonWithdrawable * 100.0) / 100.0);
        }
        if (arguments.get("nonPlayableBucket") != null && (arguments.get("nonPlayableBucket")).toString().length() > 0) {
            double nonPlayableBucket = new Double(arguments.get("nonPlayableBucket").toString());
            weUser.setAttribute("nonPlayableBucket", Math.round(nonPlayableBucket * 100.0) / 100.0);
        }
        if (arguments.get("pan_verification") != null && ((String) arguments.get("pan_verification")).length() > 0) {
            if (arguments.get("pan_verification").toString().equals("DOC_NOT_SUBMITTED")) {
                weUser.setAttribute("idProofStatus", 0);
            } else if (arguments.get("pan_verification").toString().equals("DOC_SUBMITTED")) {
                weUser.setAttribute("idProofStatus", 1);
            } else if (arguments.get("pan_verification").toString().equals("UNDER_REVIEW")) {
                weUser.setAttribute("idProofStatus", 2);
            } else if (arguments.get("pan_verification").toString().equals("DOC_REJECTED")) {
                weUser.setAttribute("idProofStatus", 3);

            } else if (arguments.get("pan_verification").toString().equals("VERIFIED")) {
                weUser.setAttribute("idProofStatus", 4);
            }
        }
        if (arguments.get("mobile_verification") != null) {
            weUser.setAttribute("mobileVerified", (boolean) arguments.get("mobile_verification"));
        }
        if (arguments.get("address_verification") != null && ((String) arguments.get("address_verification")).length() > 0) {
            if (arguments.get("address_verification").toString().equals("DOC_NOT_SUBMITTED")) {
                weUser.setAttribute("addressProofStatus", 0);
            } else if (arguments.get("address_verification").toString().equals("DOC_SUBMITTED")) {
                weUser.setAttribute("addressProofStatus", 1);
            } else if (arguments.get("address_verification").toString().equals("UNDER_REVIEW")) {
                weUser.setAttribute("addressProofStatus", 2);
            } else if (arguments.get("address_verification").toString().equals("DOC_REJECTED")) {
                weUser.setAttribute("addressProofStatus", 3);
            } else if (arguments.get("address_verification").toString().equals("VERIFIED")) {
                weUser.setAttribute("addressProofStatus", 4);
            }
        }
        if (arguments.get("email_verification") != null) {
            weUser.setAttribute("emailVerified", (boolean) arguments.get("email_verification"));
        }
        if (arguments.get("dob") != null && ((String) arguments.get("dob")).length() > 0) {
            weUser.setBirthDate((String) arguments.get("dob"));
        }
        if (arguments.get("state") != null && ((String) arguments.get("state")).length() > 0) {
            weUser.setAttribute("State", (String) arguments.get("state"));
        }
        if (arguments.get("user_balance_webengage") != null) {
            double user_balance_webengage = new Double(arguments.get("user_balance_webengage").toString());
            weUser.setAttribute("balance", Math.round(user_balance_webengage * 100.0) / 100.0);
        }
        if (arguments.get("accountStatus") != null) {
            weUser.setAttribute("accountStatus", arguments.get("accountStatus").toString());
        }
        if (arguments.get("appVersion") != null) {
            weUser.setAttribute("appVersion", arguments.get("appVersion").toString());
        }
        if (arguments.get("state") != null) {
            weUser.setAttribute("state", arguments.get("state").toString());
        }
        if (arguments.get("chipBalance") != null) {
            weUser.setAttribute("chipBalance",(int)arguments.get("chipBalance"));
        }
        return "Refreshed user Date is used";
    }

    public boolean onWEPushNotificationCLicked(PushNotificationData notificationData){
        return false;
    }

    public boolean onWEInnAppNotificationCLicked(InAppNotificationData notificationData, String actionId) {
        DeepLinkingDataModel deepLinkingDataModel = new DeepLinkingDataModel();
        boolean enableDeepLinking = false;
        try {
            Gson gson = new Gson();
            String customData = gson.toJson(notificationData.getData());
            JSONObject cusTomeDataJson = new JSONObject(customData);
            JSONObject actionData = cusTomeDataJson.getJSONObject("nameValuePairs").getJSONObject("actions");
            JSONArray actionValuesArray = actionData.getJSONArray("values");
            for (int i = 0; i < actionValuesArray.length(); i++) {
                String actionEIdFromWe = actionValuesArray.getJSONObject(i).getJSONObject("nameValuePairs").getString("actionEId");
                if (actionEIdFromWe.equals(actionId)) {
                    String actionLinkFromWe = actionValuesArray.getJSONObject(i).getJSONObject("nameValuePairs").getString("actionLink");
                    String decodedURI = Uri.decode(actionLinkFromWe);
                    String[] parameters = decodedURI.split("\\?");
                    if (parameters.length > 0) {
                        String queryParmsPart = parameters[1];
                        String queryParmsMap[] = queryParmsPart.split("&");
                        for (int j = 0; j < queryParmsMap.length; j++) {
                            String[] nameValue = queryParmsMap[j].split("=");
                            if (nameValue[0].equals("dl_page_route")) {
                                deepLinkingDataModel.setDlPageRoute(nameValue[1]); }
                            if (nameValue[0].equals("dl_ac_promocode")) {
                                deepLinkingDataModel.setDlAcPromocode(nameValue[1]);
                            }
                            if (nameValue[0].equals("dl_ac_promoamount")) {
                                deepLinkingDataModel.setDlAcPromoamount(nameValue[1]);
                            }
                            if (nameValue[0].equals("dl_sp_pageLocation")) {
                                deepLinkingDataModel.setDlSpPageLocation(nameValue[1]);
                            }
                            if (nameValue[0].equals("dl_unique_id")) {
                                deepLinkingDataModel.setDlUnique_id(nameValue[1]);
                            }
                            if (nameValue[0].equals("dl_sp_pageTitle")) {
                                deepLinkingDataModel.setDl_sp_pageTitle(nameValue[1]);
                            }
                            if (nameValue[0].equals("enableDeepLinking")) {
                                if (nameValue[1].equals("true")) {
                                    deepLinkingDataModel.setActivateDeepLinkingNavigation(true);
                                    enableDeepLinking = true;
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            System.out.println(e);
        }
        if (enableDeepLinking) {
                DeepLinkingService.setDeepLinkingDataEventSinkSuccess(deepLinkingDataModel);
                DeepLinkingService.setDeepLinkingDataObjectToDefault(activity);
            return true;
        } else {
            return false;
        }
    }

    public static void setFCMKeyForWebEngage(String token){
        WebEngage.get().setRegistrationID(token);
    }

}
