package com.silvercenturion.solitairegold.myconstants;

public class ServiceConstants {

    public final static  String FCM_SERVICE_MNAME="com.silvercenturion.fcm";
    public final static  String LOCATION_SERVICE_MNAME="com.silvercenturion.locationmanager";
    public final static  String BROWSER_LAUNCH_SERVICE_MNAME="com.silvercenturion.browser";
    public final static  String RAZORPAY_SERVICE_MNAME="com.silvercenturion.razorpay";
    public final static  String BRANCHIO_SERVICE_MNAME="com.silvercenturion.branch";
    public final static  String WEBENGAGE_SERVICE_MNAME="com.silvercenturion.webengage";
    public final static  String SOCIALSHARE_SERVICE_MNAME="com.silvercenturion.socialshare";
    public final static  String UTILS_SERVICE_MNAME="com.silvercenturion.utils";
    public final static  String EVENTALERT_EVENT_ENAME="com.silvercenturion.eventalertsstream";

}
