package com.silvercenturion.solitairegold.myconstants;

import android.content.Context;
import com.silvercenturion.solitairegold.R;

public class MyConstants {
    public static  String  APP_LAUNCH_SOURCE_NOTIF="notification";
    public static  String  APP_LAUNCH_SOURCE_DL="deeplink";
    public static  String  APP_LAUNCH_SOURCE_APP_ICON="user_launch";
    public static final String FCM_CHANNEL_ID = "solitire_gold_channel";
    public static final String FCM_CHANNEL_NAME = "Solitire Gold Notifications";
    public static final String RESULT_PAGE_DEEPLINK_NAME = "gameresults";

    public static String  getWebEngageKey(Context context){
        String mystring = context.getResources().getString(R.string.webengage_key);
        return  mystring;
    }
    public static String getWebEngageEncryptionKey(Context context){
        String mystring = context.getResources().getString(R.string.webengage_enc_key);
        return  mystring;
    }
    public static String getWebEngageEncryptionIVKey(Context context){
        String mystring = context.getResources().getString(R.string.webengage_enc_iv);
        return  mystring;
    }


}


