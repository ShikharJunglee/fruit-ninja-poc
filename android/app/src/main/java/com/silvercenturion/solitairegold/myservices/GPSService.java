package com.silvercenturion.solitairegold.myservices;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.telephony.CellInfo;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.List;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;

public class GPSService {
    private Context appContext;
    private Activity appActivity;
    private MethodChannel.Result gpsResult;
    private MethodChannel.Result locationResult;
    private static final String GPS_STREAM_CHANNEL = "com.silvercenturion.gps";

    private LocationRequest mLocationRequest;
    private GoogleApiClient googleApiClient = null;
    private FusedLocationProviderClient mFusedLocationClient;

    LocationCallback callback;
    private long UPDATE_INTERVAL = 5 * 1000;
    private long FASTEST_INTERVAL = 2000;

    public GPSService(FlutterView flutterView, Activity activity, Context applicationContext) {
        initFlutterChannels(flutterView, activity, applicationContext);
    }

    protected void initFlutterChannels(FlutterView flutterView, Activity activity, Context context) {
        this.appContext = context;
        this.appActivity = activity;

        new MethodChannel(flutterView, GPS_STREAM_CHANNEL)
                .setMethodCallHandler(new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        if (methodCall.method.equals("checkAndOnGPS")) {
                            gpsResult = result;
                            EnableGPSAutomatically();
                        } else if (methodCall.method.equals("getGPSLocation")) {
                            locationResult = result;
                            getLocation();
                        } else {
                            result.notImplemented();
                        }
                    }
                });
    }

    private void EnableGPSAutomatically() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(appContext)
                    .addApi(LocationServices.API).addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) appActivity)
                    .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) appActivity).build();
            googleApiClient.connect();

            getGpsStatus();
        } else {
            getGpsStatus();
        }
    }

    private void getGpsStatus() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result
                        .getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        gpsResult.success(true);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(appActivity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        gpsResult.success(false);                        
                        break;
                }
            }
        });
    }

    private void getLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(appActivity);
        int permissionLocation = ContextCompat
                .checkSelfPermission(appActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(appActivity, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                sendLocationResult(location);
                            }
                        }
                    });
            startLocationUpdates();
        }
    }


    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(appActivity);
        int permissionLocation = ContextCompat
                .checkSelfPermission(appActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            callback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult _locationResult) {
                    Location location = _locationResult.getLastLocation();
                    if (location != null) {
                        sendLocationResult(location);

                        mFusedLocationClient.removeLocationUpdates(callback);
                    }
                }
            };
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, callback, Looper.myLooper());
        }
    }

    private void sendLocationResult(Location location) {
        if (location != null && locationResult != null) {
            HashMap<String, Double> loc = new HashMap<>();
            loc.put("latitude", location.getLatitude());
            loc.put("longitude", location.getLongitude());
            loc.put("accuracy", (double) location.getAccuracy());

            locationResult.success(loc);
            locationResult = null;
        }
    }

    private void toast(String message) {
        try {
            Toast.makeText(appContext, message, Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {

        }
    }

    public void onGPSResult(boolean isSuccess) {
        gpsResult.success(isSuccess);
    }
}
